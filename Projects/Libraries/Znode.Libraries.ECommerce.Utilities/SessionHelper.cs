﻿using System;
using System.Web;
using System.Web.SessionState;


namespace Znode.Libraries.ECommerce.Utilities
{
    public static class SessionHelper
    {
        public static void SaveDataInSession<T>(string key, T value)
        {
            switch (GetSessionStateMode())
            {
                case SessionStateMode.SQLServer:
                    ApplicationSessionConfiguration applicationSessionConfiguration = new ApplicationSessionConfiguration();
                    string sessionValue = applicationSessionConfiguration.GetSerializedData(value);
                    HttpContext.Current.Session[key] = sessionValue;
                    break;

                default:
                    if (HelperUtility.IsNotNull(HttpContext.Current.Session))
                        HttpContext.Current.Session[key] = value;
                    break;

            }
        }

        public static T GetDataFromSession<T>(string key)
        {
            switch (GetSessionStateMode())
            {
                case SessionStateMode.SQLServer:
                    ApplicationSessionConfiguration applicationSessionConfiguration = new ApplicationSessionConfiguration();
                    return applicationSessionConfiguration.GetDeSerializeData<T>(Convert.ToString(HttpContext.Current.Session[key]));

                default:
                    var o = HttpContext.Current?.Session?[key];
                    if (o is T)
                    {
                        return (T)o;
                    }
                    break;
            }

            return default(T);
        }

        public static void RemoveDataFromSession(string key)
        {
            var obj = GetDataFromSession<object>(key);
            if (obj == null) return;

            HttpContext.Current.Session.Remove(key);
        }

        public static SessionStateMode GetSessionStateMode() => HelperUtility.IsNull(HttpContext.Current?.Session) ? SessionStateMode.InProc : HttpContext.Current.Session.Mode;

    }
}
