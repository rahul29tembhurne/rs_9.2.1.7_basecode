﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;

namespace Znode.Libraries.ECommerce.Utilities
{
    public static class ZnodeWebstoreSettings
    {
        private static NameValueCollection settings = ConfigurationManager.AppSettings;

        public static void SetConfigurationSettingSource(NameValueCollection settingSource)
        {
            settings = settingSource;
        }

        public static string ZnodeApiRootUri
        {
            get
            {
                return Convert.ToString(settings["ZnodeApiRootUri"]);
            }
        }

        public static string PaymentApplicationUrl
        {
            get
            {
                return Convert.ToString(settings["PaymentApplicationUrl"]);
            }
        }

        public static string ZnodeApiUriItemSeparator
        {
            get
            {
                return Convert.ToString(settings["ZnodeApiUriItemSeparator"]);
            }
        }

        public static string ZnodeApiUriKeyValueSeparator
        {
            get
            {
                return Convert.ToString(settings["ZnodeApiUriKeyValueSeparator"]);
            }
        }

        public static string TotalBrandCount
        {
            get
            {
                return Convert.ToString(settings["TotalBrandsCount"]);
            }
        }

        public static string ZnodeWebStoreUri
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority);
            }
        }

        public static string ZnodeWebStoreUriSocialLogin
        {
            get
            {
                return Convert.ToString(settings["ZnodeWebStoreUriSocialLogin"]);
            }
        }

        public static string ZnodeGoogleMapKey
        {
            get
            {
                return Convert.ToString(settings["ZnodeGoogleMapKey"]);
            }
        }

        public static string ZnodeGocoderGoogleAPI
        {
            get
            {
                return Convert.ToString(settings["ZnodeGocoderGoogleAPI"]);
            }
        }

        public static string ZnodeGoogleAPIDomainKey
        {
            get
            {
                return Convert.ToString(settings["ZnodeGoogleAPIDomainKey"]);
            }
        }

        /// <summary>
        /// Disable the default startup page navigation to the portal selection page while executing in debug mode.
        /// NULL/"False" : Navigation to the portal selection page in DEBUG mode over Url:"/Dev/PortalSelection" or [Startup] will be enabled.
        /// "True" : Navigation to the portal selection page on [Startup] will be disabled. Default navigation to "/Home/Index" will be avaialble.
        /// </summary>
        public static string DisablePortalSelection
        {
            get
            {
                return Convert.ToString(settings["DisablePortalSelection"]);
            }
        }
        public static string SEOSlugToSkip
        {
            get
            {
                return Convert.ToString(settings["SEOSlugToSkip"]);
            }
        }

        public static bool RunAllManagedModules
        {
            get
            {
                return Convert.ToBoolean(settings["RunAllManagedModules"]);
            }
        }
        public static bool IsDataSeparationAllowedforAppType
        {
            get
            {
                return Convert.ToBoolean(settings["AllowDataSeparationforApptype"]);
            }
        }
        public static bool EnableTokenBasedAuthorization
        {
            get
            {
                return Convert.ToBoolean(settings["EnableTokenBasedAuthorization"]);
            }
        }

        public static string DefaultPageSizeForPLP
        {
            get
            {
                return Convert.ToString(settings["DefaultPageSizeForPLP"]);
            }
        }
    }
}