﻿namespace Znode.Libraries.ECommerce.Utilities
{
    public enum ZnodeTypeAheadEnum
    {
        StoreList,
        CatalogList,
        ProductList
    }
}
