﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Framework.Business;

namespace Znode.Libraries.ECommerce.Utilities
{
    public static class HelperUtility
    {
        #region Public Method
        /// <summary>
        /// Generated the unique Password string
        /// </summary>
        /// <param name="maxSize">Length for Max password size</param>
        /// <returns>Return the Unique key combination</returns>
        public static string GetUniqueKey(int maxSize)
        {
            char[] chars = new char[62];
            string strCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = strCharacters.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte bytedata in data)
            {
                result.Append(chars[bytedata % (chars.Length - 1)]);
            }
            SetNumberInUniqueKey(result);
            return result.ToString();
        }

        /// <summary>
        /// Generate XML string from data object 
        /// </summary>
        /// <typeparam name="T">The type of object</typeparam>
        /// <param name="t">Data object</param>
        /// <returns>XMLstring of object. and empty string is any Exception Occurs</returns>
        /// <exception cref="">Returns Empty string if exception occurs</exception>
        /// <example>string data = ToXML<List<PIMAttributeGroupViewModel>>(attributeFamilyDetails.Groups);</example>
        public static string ToXML<T>(T t) where T : class
        {
            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();

                //Omit Xml Declaration from xml as it was not required in stored Procedure
                settings.OmitXmlDeclaration = true;

                //Used to Indent the xml.
                settings.Indent = true;
                settings.CheckCharacters = false;

                System.IO.StringWriter stringwriter = new System.IO.StringWriter();
                XmlWriter xmlWriter = XmlWriter.Create(stringwriter, settings);

                //used to set the namespace in XML to empty string as it was not required in stored Procedure
                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);

                XmlSerializer serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(xmlWriter, t, namespaces);
                return Convert.ToString(stringwriter);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return string.Empty;
            }
        }

        //map Xml to Model 
        public static T ConvertXMLStringToModel<T>(string xmlString) where T : class
        {
            try
            {
                if (string.IsNullOrEmpty(xmlString)) return null;

                XmlSerializer serializer = new XmlSerializer(typeof(T));
                StringReader rdr = new StringReader(Convert.ToString(XDocument.Parse(xmlString)));
                T model = (T)serializer.Deserialize(rdr);
                return model;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return null;
            }
        }

        //Map list of Xml to list of Model.
        public static List<T> ConvertListOfXMLStringToListModel<T>(IEnumerable<string> xmlString) where T : class
        {
            try
            {
                List<T> xmlList = new List<T>();
                foreach (string xml in xmlString)
                {
                    if (!string.IsNullOrEmpty(xml))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(T));
                        StringReader rdr = new StringReader(Convert.ToString(XDocument.Parse(xml)));
                        T model = (T)serializer.Deserialize(rdr);
                        xmlList.Add(model);
                    }
                    else {
                        ZnodeLogging.LogMessage("While publishing catalog/category, some products xml getting null.", ZnodeLogging.Components.PIM.ToString(),TraceLevel.Error);
                    }
                }
                return xmlList;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return null;
            }
        }

        //XmlSerializer for product xml. 
        public static List<T> ConvertListOfXMLStringToListModelForProducts<T>(IEnumerable<string> xmlString) where T : class
        {
            try
            {
                List<T> xmlList = new List<T>();
                foreach (string xml in xmlString)
                {
                    if (!string.IsNullOrEmpty(xml))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(T));
                        string cleanXml = Regex.Replace(xml, @"<[a-zA-Z].[^(><.)]+/>",
                                       new MatchEvaluator(RemoveText));
                        StringReader rdr = new StringReader(Convert.ToString(XDocument.Parse(cleanXml)));
                        T model = (T)serializer.Deserialize(rdr);
                        xmlList.Add(model);
                    }
                    else
                    {
                        ZnodeLogging.LogMessage("While publishing catalog/category, some products xml getting null.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    }
                }
                return xmlList;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return null;
            }
        }

        //Remove empty tags from xml.
        static string RemoveText(Match m) { return ""; }

        //Split bulk records /  collections into chunks.Here we need to provide the list of collections and chunk size.
        public static List<List<T>> SplitCollectionIntoChunks<T>(List<T> collection, int chunkSize)
        {
            List<List<T>> chunks = new List<List<T>>();
            int chunkCount = collection.Count() / chunkSize;

            if (collection.Count % chunkSize > 0)
                chunkCount++;

            for (var index = 0; index < chunkCount; index++)
                chunks.Add(collection.Skip(index * chunkSize).Take(chunkSize).ToList());

            return chunks;
        }

        //Returns true if the passed value is not null, else return false.
        public static bool IsNotNull(object value)
            => !Equals(value, null);

        //Returns true if the passed value is null else false.
        public static bool IsNull(object value)
            => Equals(value, null);

        //Method to replace token with message text.
        public static string ReplaceTokenWithMessageText(string key, string replaceValue, string resourceText)
        {
            Regex rgx = new Regex(key, RegexOptions.IgnoreCase);
            return rgx.Replace(resourceText, string.IsNullOrEmpty(replaceValue) ? string.Empty : replaceValue);
        }


        /// <summary>
        /// Method to replace multiple tokens with respective message text.
        /// </summary>
        /// <param name="keyValueDictionary"></param>
        /// <param name="resourceText"></param>
        /// <returns>return message with updated values.</returns>
        public static string ReplaceMultipleTokenWithMessageText(IDictionary<string, string> keyValueDictionary, string resourceText)
        {
            foreach (var keyvaluePair in keyValueDictionary)
            {
                Regex rgx = new Regex(keyvaluePair.Key, RegexOptions.IgnoreCase);
                resourceText = rgx.Replace(resourceText, string.IsNullOrEmpty(keyvaluePair.Value) ? string.Empty : keyvaluePair.Value);
            }
            return resourceText;
        }

        /// <summary>
        /// Check number exists between the given range
        /// </summary>
        /// <param name="num">number to check between range</param>
        /// <param name="minNumber">lower limit</param>
        /// <param name="MaxNumber">upper limit</param>
        /// <param name="inclusive">true if want to include min and max number in comparison</param>
        /// <returns>true or false</returns>
        public static bool Between(int num, int minNumber, int MaxNumber, bool inclusive = false)
        {
            return inclusive
                ? minNumber <= num && num <= MaxNumber
                : minNumber < num && num < MaxNumber;
        }

        /// <summary>
        /// Check number exists between the given range.
        /// </summary>
        /// <param name="num">number to check between range</param>
        /// <param name="minNumber">lower limit</param>
        /// <param name="MaxNumber">upper limit</param>
        /// <param name="inclusive">true if want to include min and max number in comparison</param>
        /// <returns>true or false</returns>
        public static bool Between(decimal num, decimal minNumber, decimal MaxNumber, bool inclusive = false)
        {
            return inclusive
                ? minNumber <= num && num <= MaxNumber
                : minNumber < num && num < MaxNumber;
        }
        public enum StoreFeature
        {
            Taxes_In_Product_Price,
            Persistent_Cart,
            Address_Validation,
            Customer_Based_Pricing,
            Allow_multiple_coupons,
            Allow_Single_Page_Checkout,
            Allow_Global_Level_User_Creation,
            Require_Validated_Address,
            Enable_Profile_Based_Search,
            Enable_Product_Compare,
            Enable_Voice_Based_Search
        }

        //Convert datetime string to sqldatetime format.
        public static string ConvertStringToSqlDateFormat(string dateValue)
        {
            DateTime utcDateValue = new DateTime();
            if (DateTime.TryParse(dateValue, out utcDateValue))
                return utcDateValue.ToString(ZnodeConstant.SQLDateFormat);
            else
                return dateValue;
        }

        /// <summary>
        /// To get file path with url
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string GetFilePath(string file)
        {
            if (!string.IsNullOrEmpty(file))
            {
                StringBuilder filePath = new StringBuilder();
                filePath.Append(ZnodeWebstoreSettings.ZnodeApiRootUri);
                filePath.Append(file);
                return filePath.ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// Clear cache.
        /// </summary>
        /// <param name="domainId"></param>
        /// <returns>true or false</returns>
        public static bool ClearCache(int domainId)
        {
            try
            {
                //Get list of cache.
                IDictionaryEnumerator cacheEnumerator = HttpContext.Current.Cache.GetEnumerator();

                //Get domain data by domain id.
                ZnodeDomain domain = GetDomain(domainId);

                //Get cached keys.
                List<string> cacheItemsList = GetCachedItemsKeys(domain);
                while (cacheEnumerator.MoveNext())
                {
                    foreach (string item in cacheItemsList)
                        if (cacheEnumerator.Key.ToString().Contains(item))
                            HttpContext.Current.Cache.Remove(cacheEnumerator.Key.ToString());
                }

                //Remove domain cache .
                HttpContext.Current.Cache.Remove(domain?.DomainName);

                return true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Portal.ToString(), TraceLevel.Error);
                return false;
            }
        }

        /// <summary>
        /// Get current datetime.
        /// </summary>
        /// <returns>Returns current date and time.</returns>
        public static DateTime GetDateTime() => DateTime.Now.Date;

        /// <summary>
        /// Get current datetime.
        /// </summary>
        /// <returns>Returns current date and time.</returns>
        public static DateTime GetDateWithTime() => DateTime.Now;

        /// <summary>
        /// Get current date.
        /// </summary>
        /// <returns>Returns current date</returns>
        public static DateTime GetDate() => DateTime.Now.Date;
        /// <summary>
        /// Check Property Exist in dynamic object
        /// </summary>
        /// <param name="settings">dynamic object</param>
        /// <param name="name">propertyname</param>
        /// <returns>bool - true if object has property</returns>
        public static bool IsPropertyExist(dynamic settings, string name)
        {
            if (settings is ExpandoObject)
                return ((IDictionary<string, object>)settings).ContainsKey(name);

            return settings.GetType().GetProperty(name) != null;
        }

        /// <summary>
        /// Convert Data Table to Json.
        /// </summary>
        /// <param name="dataTable">dataTable</param>
        /// <returns>Return json string.</returns>
        public static string ToJson(this DataTable dataTable)
            => JsonConvert.SerializeObject(dataTable);

        /// <summary>
        /// To create HtppCookies with HttpOnly and Secure flag true
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static HttpCookie CreateHttpCookies(string name, string value = "")
        {
            return string.IsNullOrWhiteSpace(name)
                ? null
                : new HttpCookie(name, value)
                {
                    HttpOnly = ZnodeApiSettings.IsCookieHttpOnly,
                    Secure = ZnodeApiSettings.IsCookieSecure
    };
        }

        /// <summary>
        /// Method to get the http cookies
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static HttpCookie GetHttpCookies(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return null;
            }
            HttpCookie cookie = HttpContext.Current.Request.Cookies[name];

            //If cookie was not available in Request object, then check if it's being created in CURRENT response context and use that
            if (IsNull(cookie))
            {                
                cookie = HttpContext.Current.Response.Cookies[name];
            }

            if (cookie != null)
            {
                cookie.HttpOnly = ZnodeApiSettings.IsCookieHttpOnly;
                cookie.Secure = ZnodeApiSettings.IsCookieSecure;
            }
            return cookie;
        }

        /// <summary>
        /// Generate JSON string from data object 
        /// </summary>
        /// <typeparam name="T">The type of object</typeparam>
        /// <param name="t">Data object</param>
        /// <returns>JSONstring of object. and empty string if any Exception Occurs</returns>
        /// <exception cref="">Returns Empty string if exception occurs</exception>
        /// <example>string data = ToJSON(objectToSerialize);</example>
        public static string ToJSON(object t)
        {
            try
            {
                return JsonConvert.SerializeObject(t, Newtonsoft.Json.Formatting.Indented);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return string.Empty;
            }
        }

        //Get PortalId from SiteConfig
        public static int GetPortalId()
        {
            string domainName = HttpContext.Current.Request.ServerVariables.Get("HTTP_HOST")?.Trim() ?? GetPortalDomainName();
            return ZnodeConfigManager.GetSiteConfig(domainName)?.PortalId ?? 0;
        }

        //Get Domain Name by Request Headers
        public static string GetPortalDomainName()
        {
            const string headerDomainName = "Znode-DomainName";
            var headers = HttpContext.Current.Request.Headers;
            string domainName = headers[headerDomainName];
            return domainName;
        }

        #endregion

        #region Private Method
        /// <summary>
        /// Set the Number at the end of the autogenerated unique key.
        /// As for User, it is mandatory to have atlease one number in the password
        /// </summary>
        /// <param name="result"></param>
        private static void SetNumberInUniqueKey(StringBuilder result)
        {
            Random rnd = new Random();
            int dice = rnd.Next(1, 9);
            result.Append(Convert.ToString(dice));
        }

        //Get domain data by domain id
        private static ZnodeDomain GetDomain(int domainId)
        {
            ZnodeRepository<ZnodeDomain> _domainRepository = new ZnodeRepository<ZnodeDomain>();
            ZnodeDomain domain = _domainRepository.GetById(domainId);
            return domain;
        }

        //Get cached keys.
        private static List<string> GetCachedItemsKeys(ZnodeDomain domain)
        {
            List<string> cacheItemsToRemove = new List<string>();
            cacheItemsToRemove.Add("SliderBannerKey_" + domain?.PortalId);
            cacheItemsToRemove.Add("ProductListKey_" + domain?.PortalId);
            cacheItemsToRemove.Add("LinkKey_" + domain?.PortalId);
            cacheItemsToRemove.Add("CategoryListKey_" + domain?.PortalId);
            cacheItemsToRemove.Add("TextWidgetKey_" + domain?.PortalId);
            cacheItemsToRemove.Add("TagManager_" + domain?.PortalId);
            cacheItemsToRemove.Add("SpecialsNavigation" + domain?.PortalId);
            cacheItemsToRemove.Add("BrandDropDownNavigation" + domain?.PortalId);
            cacheItemsToRemove.Add("PriceNavigation" + domain?.PortalId);
            cacheItemsToRemove.Add("BrandListKey_" + domain?.PortalId);
            return cacheItemsToRemove;
        }
        #endregion

    }
}
