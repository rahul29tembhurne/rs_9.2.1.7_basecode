﻿using System;
using System.Collections.Specialized;
using System.Configuration;

namespace Znode.Libraries.ECommerce.Utilities
{
    public static class ZnodeAdminSettings
    {
        private static NameValueCollection settings = ConfigurationManager.AppSettings;

        public static void SetConfigurationSettingSource(NameValueCollection settingSource)
        {
            settings = settingSource;
        }

        public static string ZnodeApiRootUri
        {
            get
            {
                return Convert.ToString(settings["ZnodeApiRootUri"]);
            }
        }
        public static string ZnodeAdminUri
        {
            get
            {
                return Convert.ToString(settings["ZnodeAdminUri"]);
            }
        }
        public static string ZnodeApiUriItemSeparator
        {
            get
            {
                return Convert.ToString(settings["ZnodeApiUriItemSeparator"]);
            }
        }
        public static string CookieExpiresValue
        {
            get
            {
                return Convert.ToString(settings["CookieExpiresValue"]);
            }
        }
        public static string CookieExpiresValueForFilter
        {
            get
            {
                return Convert.ToString(settings["CookieExpiresValue"]);
            }
        }
        public static string ZnodeApiUriKeyValueSeparator
        {
            get
            {
                return Convert.ToString(settings["ZnodeApiUriKeyValueSeparator"]);
            }
        }
        public static string MediaFilePath
        {
            get
            {
                return Convert.ToString(settings["MediaFilePath"]);
            }
        }
        public static string FolderJsonPath
        {
            get
            {
                return Convert.ToString(settings["FolderJsonPath"]);
            }
        }
        public static string GridPagingStartValue
        {
            get
            {
                return Convert.ToString(settings["GridPagingStartValue"]);
            }

        }
       

        public static string SessionWarningTime
        {
            get
            {
                return Convert.ToString(settings["SessionWarningTime"]);
            }
        }
        public static string MediaUploadFolderName
        {
            get
            {
                return Convert.ToString(settings["MediaUploadFolderName"]);
            }
        }


        public static string PaymentApplicationUrl
        {
            get
            {
                return Convert.ToString(settings["PaymentApplicationUrl"]);
            }
        }

        public static string DefaultImagePath
        {
            get
            {
                return Convert.ToString(settings["DefaultImagePath"]);
            }
        }

        public static string MaxFileSize
        {
            get
            {
                return Convert.ToString(settings["MaxFileSize"]);
            }
        }

        public static string MaxSizeForVideo
        {
            get
            {
                return Convert.ToString(settings["MaxSizeForVideo"]);
            }
        }

        public static string NotificationMessagesIsFadeOut
        {
            get
            {
                return Convert.ToString(settings["NotificationMessagesIsFadeOut"]);
            }
        }

        public static string AccessType
        {
            get
            {
                return Convert.ToString(settings["AccessType"]);
            }
        }
        public static string ZnodeReportFolderName
        {
            get
            {
                return Convert.ToString(settings["ZnodeReportFolderName"]);
            }
        }

        public static string ReportServerDynamicReportFolderName
        {
            get
            {
                return Convert.ToString(settings["ReportServerDynamicReportFolderName"]);
            }
        }

        public static bool IsDebugMode
        {
            get
            {
                return Convert.ToBoolean(settings["IsDebugMode"]);
            }
        }

        public static bool EnableScriptOptimizations
        {
            get
            {
                return Convert.ToBoolean(settings["EnableScriptOptimizations"]);
            }
        }
        public static string ProductUpdateSampleCSVPath
        {
            get
            {
                return Convert.ToString(settings["ProductUpdateSampleCSVPath"]);
            }
        }

        public static int USPSWebRequestTimeOutMs
        {
            get
            {
                int uspsWebRequestTimeOut = 0;
                //Default 30 seconds if key is not available in config file.
                if (!(int.TryParse(settings["USPSWebRequestTimeOutMs"], out uspsWebRequestTimeOut)))
                    uspsWebRequestTimeOut = 30000;

                return uspsWebRequestTimeOut;
            }
        }

        public static bool IsDataSeparationAllowedforAppType
        {
            get
            {
                return Convert.ToBoolean(settings["AllowDataSeparationforApptype"]);
            }
        }
        public static bool EnableTokenBasedAuthorization
        {
            get
            {
                return Convert.ToBoolean(settings["EnableTokenBasedAuthorization"]);
            }
        }

        public static bool EnableCMSPreview
        {
            get
            {
                return Convert.ToBoolean(settings["IsEnableCMSPreview"]);
            }
        }

    }
}
