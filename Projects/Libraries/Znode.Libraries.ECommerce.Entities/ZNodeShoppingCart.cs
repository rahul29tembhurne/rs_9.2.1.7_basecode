﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Framework.Business;

namespace Znode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents Shopping cart and shopping cart items
    /// </summary>
    [Serializable()]
    public class ZnodeShoppingCart : ZnodeBusinessBase
    {
        #region Member Variables

        private StringBuilder _PromoDescription = new StringBuilder();
        protected StringBuilder _ErrorMessage = new StringBuilder();
        private decimal _GiftCardAmount = 0;
        private decimal _GiftCardBalance = 0;
        private decimal _OrderLevelShipping = 0;
        private decimal _OrderLevelTaxes = 0;
        private decimal _CSRDiscount = 0;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderDetailRepository;

        #endregion Member Variables

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ZNodeShoppingCart class
        /// </summary>
        public ZnodeShoppingCart()
        {
            ShoppingCartItems = new List<ZnodeShoppingCartItem>();
            Shipping = new ZnodeShipping();
            Payment = new ZnodePayment();
            Coupons = new List<ZnodeCoupon>();
            _orderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
        }

        #endregion Constructors

        #region Public Properties

        // Gets the error messages that have been created in this cart.
        public string ErrorMessage { get; }

        // Gets or sets a collection of shopping cart items
        public List<ZnodeShoppingCartItem> ShoppingCartItems { get; set; }

        // Gets or sets a value indicating whether any promotion applied
        public bool IsAnyPromotionApplied { get; set; }

        // Gets or sets the Shipping object
        public ZnodeShipping Shipping { get; set; }

        // Gets or sets the payment object
        public ZnodePayment Payment { get; set; }

        // Gets or sets the User Address
        public UserAddressModel UserAddress { get; set; }

        // Gets or sets the TotalPartialRefundAmount
        public virtual decimal TotalPartialRefundAmount
        {
            get
            {
                decimal? totalrefundAmount = ShoppingCartItems.Cast<ZnodeShoppingCartItem>().Sum(x => x.PartialRefundAmount);
                return totalrefundAmount ?? 0;
            }
        }

        // Gets the total shipping cost of line items in the shopping cart.
        public virtual decimal ShippingCost
        {
            get
            {
                decimal totalShippingCost = ShoppingCartItems.Cast<ZnodeShoppingCartItem>().Sum(x => x.ShippingCost) + OrderLevelShipping;
                decimal shippingDiscount = Shipping.ShippingDiscount;
                return !Equals(CustomShippingCost, null) ? CustomShippingCost.GetValueOrDefault() : totalShippingCost < shippingDiscount ? 0 : (totalShippingCost - shippingDiscount);
            }
        }

        // Gets the total shipping difference of line items in the shopping cart.
        public virtual decimal ShippingDifference
        {
            get
            {
                decimal _shippingDifference = 0;
                int? _orderId = this.OrderId;
                if (_orderId != null && _orderId > 0)
                {
                    ZnodeOmsOrderDetail orderDtls = _orderDetailRepository.Table.Where(x => x.OmsOrderId == _orderId && x.IsActive)?.FirstOrDefault();
                    if (orderDtls != null)
                    {
                        bool isShippingCostUpdated = orderDtls.IsShippingCostEdited ?? false;

                        if (isShippingCostUpdated || !Equals(CustomShippingCost, null))
                            return _shippingDifference;

                        if (IsLineItemReturned)
                        {
                            decimal _ordershippingCost = (orderDtls.ShippingCost ?? 0);
                            _ordershippingCost += (orderDtls.ShippingDifference ?? 0);
                            if (_ordershippingCost > 0 && ShippingCost > 0)
                            {
                                _shippingDifference = (_ordershippingCost - ShippingCost);
                                if (_shippingDifference > 0)
                                {
                                    _shippingDifference = _shippingDifference - CartItemShippingRefundAmount();
                                    if (_shippingDifference < 0)
                                        _shippingDifference = 0;
                                }
                            }
                        }
                        else
                        {
                            _shippingDifference = orderDtls.ShippingDifference ?? 0;
                        }
                    }
                }
                return _shippingDifference;
            }
        }

        // Gets or sets the discounted applied to this order.
        public decimal OrderLevelDiscount { get; set; }

        // Gets or sets the Gift Card Amount.
        public decimal GiftCardAmount
        {
            get { return _GiftCardAmount; }
            set { _GiftCardAmount = value; }
        }

        // Gets or sets the Gift Card Balance.
        public decimal GiftCardBalance
        {
            get { return _GiftCardBalance; }
            set { _GiftCardBalance = value; }
        }

        // Gets or sets the gift card number
        public string GiftCardNumber { get; set; }

        // Gets or sets the Gift Card Message.
        public string GiftCardMessage { get; set; }

        // Gets or sets a value indicating whether the gift card number is valid.
        public bool IsGiftCardValid { get; set; }

        // Gets or sets a value indicating whether the line item is returned or not.
        public bool IsLineItemReturned { get; set; }

        // Gets or sets a value indicating whether the gift card is applied to the shopping cart.
        public bool IsGiftCardApplied { get; set; }

        // Gets or sets the CSR Discount for internal calculation.
        public decimal CSRDiscount
        {
            get { return _CSRDiscount; }
            set { _CSRDiscount = value; }
        }

        // Gets or sets the CSR Discount Amount.
        public decimal CSRDiscountAmount { get; set; }

        // Gets or sets the CSR Discount Description.
        public string CSRDiscountDescription { get; set; }

        // Gets or sets the CSR Discount Message.
        public string CSRDiscountMessage { get; set; }

        // Gets or sets the CSR Discount Applied.
        public bool CSRDiscountApplied { get; set; }

        // Gets or sets the custom shipping cost amount.
        public decimal? CustomShippingCost { get; set; }

        // Get or sets the estimate shipping cost.
        public decimal? EstimateShippingCost { get; set; }

        // Gets or sets the custom tax cost amount.
        public decimal? CustomTaxCost { get; set; }

        // Gets or sets the Order Attribute to be save along with order submit.
        public string OrderAttribute { get; set; }

        //Gets or sets the externalId for ERP data.
        public string ExternalId { get; set; }

        // Gets or sets the shipping cost for this order.
        public decimal OrderLevelShipping
        {
            get
            {
                return _OrderLevelShipping + Shipping.ShippingHandlingCharge;
            }

            set
            {
                _OrderLevelShipping = value;
            }
        }

        // Gets or sets the tax cost for this order.
        public decimal OrderLevelTaxes
        {
            get
            {
                return !Equals(CustomTaxCost, null) ? CustomTaxCost.GetValueOrDefault() : _OrderLevelTaxes + SalesTax + VAT + HST + PST + GST;
            }

            set { _OrderLevelTaxes = value; }
        }

        // Gets or sets the total tax cost of items in the shopping cart.
        public virtual decimal TaxCost
        {
            get
            {
                decimal totalTaxCost = !Equals(CustomTaxCost, null) ? CustomTaxCost.GetValueOrDefault() : (ShoppingCartItems.Cast<ZnodeShoppingCartItem>().Sum(x => x.TaxCost) + TaxOnShipping);
                return totalTaxCost;
            }
        }

        // Gets or sets the total sales tax cost of items in the shopping cart
        public decimal SalesTax { get; set; }

        // Gets or sets the total tax cost of items in the shopping cart.
        public decimal VAT { get; set; }

        // Gets or sets the total tax cost of items in the shopping cart
        public decimal PST { get; set; }

        // Gets or sets the total tax cost of items in the shopping cart.
        public decimal HST { get; set; }

        // Gets or sets the total tax cost of items in the shopping cart.
        public decimal GST { get; set; }

        // Gets or sets the sales tax rate (%)
        public decimal TaxRate { get; set; }

        // Gets or sets the sales tax rate (%)
        public decimal TaxOnShipping { get; set; }

        // Gets the count of items in the shopping cart
        public int Count
        {
            get { return ShoppingCartItems.Count; }
        }

        // Gets the total cost of items in the shopping cart before shipping and taxes
        public virtual decimal SubTotal
        {
            get
            {
                return ShoppingCartItems.Cast<ZnodeShoppingCartItem>().Sum(x => x.ExtendedPrice);
            }
        }

        // Gets total discount of applied to the items in the shopping cart.
        public virtual decimal Discount
        {
            get
            {
                decimal totalDiscount = OrderLevelDiscount + ShoppingCartItems.Cast<ZnodeShoppingCartItem>().Sum(x => x.ExtendedPriceDiscount + x.DiscountAmount);

                totalDiscount = totalDiscount + TotalPartialRefundAmount;

                if (totalDiscount > SubTotal)
                    return SubTotal;

                return totalDiscount;
            }
        }

        // Gets the total cost after shipping, taxes and promotions
        public virtual decimal Total
        {
            get { return (SubTotal - Discount) + ShippingCost + ShippingDifference + OrderLevelTaxes - GiftCardAmount - CSRDiscount; }
        }

        public virtual decimal TotalAdditionalCost { get; set; }

        // Gets all of the promotion descriptions that have been added to the cart.
        public string PromoDescription
        {
            get
            {
                StringBuilder promoDesc = new StringBuilder(_PromoDescription.ToString());

                foreach (ZnodeShoppingCartItem item in ShoppingCartItems)
                {
                    if (promoDesc.Length > 0)
                        _PromoDescription.Append("<br/>");

                    promoDesc.Append(item.PromoDescription.ToString());
                }
                return promoDesc.ToString();
            }
        }

        // Gets or sets the promotion description
        public string AddPromoDescription
        {
            get { return _PromoDescription.ToString(); }

            set { _PromoDescription.Append(value); }
        }

        // Gets or sets a message to the shopping cart main error message.
        public string AddErrorMessage
        {
            get
            {
                if (_ErrorMessage.Length > 0)
                    _ErrorMessage.Append("<br/>");

                _ErrorMessage.Append(ErrorMessage);

                return _ErrorMessage.ToString();
            }

            set { _ErrorMessage.Append(value); }
        }

        // for paypal express checkout for Api
        public string Token { get; set; }

        public string Payerid { get; set; }

        //for setting PortalId in case of franchise admin
        public int? PortalId { get; set; }

        public int? UserId { get; set; }

        public string LoginUserName { get; set; }

        //depends on the first coupon applied.
        public bool CartAllowsMultiCoupon { get; set; }

        public int? OrderLineItemRelationshipId { get; set; }

        #endregion Public Properties

        /// <summary>
        /// Gets or sets Znode Coupons for applied in shopping cart.
        /// </summary>
        public List<ZnodeCoupon> Coupons { get; set; }

        /// <summary>
        /// Gets or sets Znode product description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Clear the previous error messages.
        /// </summary>
        public void ClearPreviousErrorMessges()
        {
            _ErrorMessage = new StringBuilder();
            _PromoDescription = new StringBuilder();
        }

        public int? ProfileId { get; set; }

        // Gets or sets a OrderId of shopping cart
        public int? OrderId { get; set; }

        // Gets or sets a OrderDate of shopping cart
        public DateTime? OrderDate { get; set; }

        // Gets or sets the discount description applied to this line item.
        public List<OrderDiscountModel> OrderLevelDiscountDetails { get; set; }

        public int LocalId { get; set; }
        public int PublishedCatalogId { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencySuffix { get; set; }
        public int PublishStateId { get; set; }
        //Get and set Personalise Value list
        public Dictionary<string, object> PersonaliseValuesList { get; set; }
        public List<PersonaliseValueModel> PersonaliseValueDetail { get; set; }

        public string ApproximateArrival { get; set; }
        public string CreditCardNumber { get; set; }
        public bool IsCchCalculate { get; set; }
        public List<ReturnOrderLineItemModel> ReturnItemList { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
        public string CardType { get; set; }
        public int? CreditCardExpMonth { get; set; }
        public int? CreditCardExpYear { get; set; }

        public bool IsAllowWithOtherPromotionsAndCoupons { get; set; }

        public string CultureCode { get; set; }

        #region Private Method

        //to get cart item refund amount applied for Shipping in case of IsReturnShipping
        private decimal CartItemShippingRefundAmount()
        {
            decimal refundAmount = 0;
            if (this?.ReturnItemList?.Count > 0 && this?.ShoppingCartItems?.Count > 0)
            {
                foreach (ReturnOrderLineItemModel item in this?.ReturnItemList)
                {
                    if (item.IsShippingReturn)
                    {
                        refundAmount += GetRefundAmountByQuantity(item);
                    }
                }
            }
            return refundAmount;
        }

        // to get refund amount by quantity for cart item
        private decimal GetRefundAmountByQuantity(ReturnOrderLineItemModel cartItem)
        {
            return cartItem.IsShippingReturn ? cartItem.ShippingCost : 0;
        }

        #endregion Private Method
    }
}