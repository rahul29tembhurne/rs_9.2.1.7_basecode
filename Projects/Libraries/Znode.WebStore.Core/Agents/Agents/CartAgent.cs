﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore.Models;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.WebStore.Agents
{
    public class CartAgent : BaseAgent, ICartAgent
    {
        #region Private member
        private readonly IShoppingCartClient _shoppingCartsClient;
        private readonly IPublishProductClient _publishProductClient;
        private readonly IAccountQuoteClient _accountQuoteClient;
        private readonly IUserClient _userClient;
        private static List<UpdatedProductQuantityModel> updatedProducts = new List<UpdatedProductQuantityModel>();
        #endregion

        #region Constructor
        public CartAgent(IShoppingCartClient shoppingCartsClient, IPublishProductClient publishProductClient, IAccountQuoteClient accountQuoteClient, IUserClient userClient)
        {
            _shoppingCartsClient = GetClient<IShoppingCartClient>(shoppingCartsClient);
            _publishProductClient = GetClient<IPublishProductClient>(publishProductClient);
            _accountQuoteClient = GetClient<IAccountQuoteClient>(accountQuoteClient);
            _userClient = GetClient<IUserClient>(userClient);
        }
        #endregion

        #region Public methods

        //Create Shopping Cart.
        public virtual CartViewModel CreateCart(CartItemViewModel cartItem)
        {
            if (IsNotNull(cartItem))
            {
                //Get shopping cart data from session.

                //Bind Portal related data to Shopping cart model.
                ShoppingCartModel shoppingCartModel = IsNull(GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)) && GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey)?.ShoppingCartItems?.Count > 0 ? GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) : new ShoppingCartModel();
                //shoppingCartModel.Coupons = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey)?.Coupons?.Count > 0 ? GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey)?.Coupons : new List<CouponModel>();
                shoppingCartModel = GetShoppingCart(cartItem, shoppingCartModel);
                //Create new cart.
                _shoppingCartsClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
                ShoppingCartModel shoppingCart = _shoppingCartsClient.CreateCart(shoppingCartModel);

                //Bind CartItemViewModel properties to ShoppingCartModel.
                BindCartItemData(cartItem, shoppingCart);

                //assign values which are required for the estimated shipping calculation.
                shoppingCart = GetEstimatedShippingDetails(shoppingCart);

                //if persistent cart disabled, we need not call below method, need to check with portal record.
                if ((IsNull(shoppingCart.UserDetails) || shoppingCart.UserDetails.UserId == 0) && PortalAgent.CurrentPortal.PersistentCartEnabled)
                {
                    SaveInCookie(WebStoreConstants.CartCookieKey, shoppingCart.CookieMappingId.ToString());
                }

                shoppingCart?.ShoppingCartItems.Where(x => x.SKU == cartItem.SKU).Select(x => { x.ProductType = cartItem.ProductType; return x; })?.ToList();

                SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCart);
                return shoppingCart?.ToViewModel<CartViewModel>();
            }
            return null;
        }

        //Add product in cart.
        public virtual AddToCartViewModel AddToCartProduct(AddToCartViewModel cartItem)
        {
            if (IsNotNull(cartItem))
            {
                cartItem.CookieMappingId = GetFromCookie(WebStoreConstants.CartCookieKey);

                ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey)?.ShoppingCartItems?.Count > 0 ? GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) : new ShoppingCartModel();

                cartItem.Coupons = shoppingCartModel?.Coupons?.Count > 0 ? shoppingCartModel?.Coupons : new List<CouponModel>();

                //Create new cart.
                cartItem = _shoppingCartsClient.AddToCartProduct(GetShoppingCartValues(cartItem)?.ToModel<AddToCartModel>()).ToViewModel<AddToCartViewModel>();

                SaveInCookie(WebStoreConstants.CartCookieKey, cartItem.CookieMappingId.ToString());

                cartItem?.ShoppingCartItems.Where(x => x.SKU == cartItem.SKU).Select(x => { x.ProductType = cartItem.ProductType; return x; })?.ToList();

                cartItem.ShippingId = (shoppingCartModel?.ShippingId).GetValueOrDefault();

                shoppingCartModel = MapAddToCartToShoppingCart(cartItem, shoppingCartModel);

                SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);

                return cartItem;
            }
            return null;
        }

        // Get Cart method to check Session or Cookie to get the existing shopping cart.
        public virtual CartViewModel GetCart(bool isCalculateTaxAndShipping = true, bool isCalculateCart = true)
        {
            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                                     GetCartFromCookie();

            if (IsNull(shoppingCartModel))
            {
                return new CartViewModel()
                {
                    HasError = true,
                    ErrorMessage = WebStore_Resources.OutofStockMessage
                };
            }

            if (IsNotNull(shoppingCartModel) && (IsNull(shoppingCartModel?.ShoppingCartItems) || shoppingCartModel?.ShoppingCartItems?.Count == 0))
            {
                var shoppingartItems = GetCartFromCookie()?.ShoppingCartItems;
                shoppingCartModel.ShoppingCartItems = (shoppingartItems?.Count > 0) ? shoppingartItems : new List<ShoppingCartItemModel>(); ;
            }

            if (shoppingCartModel.ShoppingCartItems?.Count == 0)
            {
                return new CartViewModel();
            }

            //Remove Shipping and Tax calculation From Cart.
            RemoveShippingTaxFromCart(shoppingCartModel);

            //To remove the tax getting calculated when we come back from checkout page to cart page
            shoppingCartModel.ShippingAddress = new AddressModel();
            shoppingCartModel.ShippingAddress.CountryName = "us";
            shoppingCartModel.ShippingAddress.PostalCode = GetFromSession<String>(ZnodeConstant.ShippingEstimatedZipCode);
            shoppingCartModel.CultureCode = PortalAgent.CurrentPortal.CultureCode;
            shoppingCartModel.CurrencyCode = PortalAgent.CurrentPortal.CurrencyCode;

            //Calculate cart
            shoppingCartModel = CalculateCart(shoppingCartModel, isCalculateTaxAndShipping, isCalculateCart);

            SaveInSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            //Set currency details.
            return SetCartCurrency(shoppingCartModel);
        }

        //Update quantity of cart item.
        public virtual CartViewModel UpdateCartItemQuantity(string guid, string quantity, int productId)
        {
            // Get shopping cart from session.
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);
            decimal newQuantity = ModifyQuantityValue(quantity);
            if (IsNotNull(shoppingCart))
            {
                //Get shopping cart item on the basis of guid.
                shoppingCart = GetShoppingCartItemByExternalId(shoppingCart, guid, newQuantity, productId);

                //Update shopping cart item quantity.
                if (IsNotNull(shoppingCart))
                {
                    return UpdatecartItemQuantity(shoppingCart, guid, newQuantity, productId);
                }
            }
            return new CartViewModel();
        }

        //Update quantity of cart item.
        public virtual AddToCartViewModel UpdateQuantityOfCartItem(string guid, string quantity, int productId)
        {
            // Get shopping cart from session.
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);
            decimal newQuantity = ModifyQuantityValue(quantity);

            if (IsNotNull(shoppingCart))
            {
                //Update quantity and update the cart.
                if (productId > 0)
                {
                    shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(x => x.GroupProducts?.Where(y => y.ProductId == productId)?.Select(z => { z.Quantity = newQuantity; return z; })?.FirstOrDefault()).ToList();
                }
                else
                {
                    shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(y => { y.Quantity = Convert.ToDecimal(newQuantity.ToInventoryRoundOff()); return y; }).ToList();
                }

                ShoppingCartItemModel shoppingCartItemModel = shoppingCart.ShoppingCartItems?.FirstOrDefault(w => w.ExternalId == guid);
                shoppingCartItemModel?.AssociatedAddOnProducts?.ForEach(x => x.Quantity = Convert.ToDecimal(newQuantity.ToInventoryRoundOff()));
                if (IsNotNull(shoppingCartItemModel))
                {
                    AddToCartViewModel addToCartViewModel = new AddToCartViewModel();
                    addToCartViewModel.CookieMappingId = GetFromCookie(WebStoreConstants.CartCookieKey);
                    addToCartViewModel.GroupId = shoppingCartItemModel.GroupId;
                    addToCartViewModel.AddOnProductSKUs = shoppingCartItemModel.AddOnProductSKUs;
                    addToCartViewModel.AutoAddonSKUs = shoppingCartItemModel.AutoAddonSKUs;
                    addToCartViewModel.SKU = shoppingCartItemModel.SKU;
                    addToCartViewModel.ParentOmsSavedcartLineItemId = shoppingCartItemModel.ParentOmsSavedcartLineItemId;
                    addToCartViewModel.AssociatedAddOnProducts = shoppingCartItemModel.AssociatedAddOnProducts;
                    if (!string.IsNullOrEmpty(shoppingCartItemModel.BundleProductSKUs))
                    {
                        addToCartViewModel.OmsSavedCartLineItemId = shoppingCartItemModel.OmsSavedcartLineItemId;
                        addToCartViewModel.Quantity = shoppingCartItemModel.Quantity;
                        GetSelectedBundleProductsForAddToCart(addToCartViewModel, shoppingCartItemModel.BundleProductSKUs);
                    }
                    else if (shoppingCartItemModel.GroupProducts?.Count > 0)
                    {
                        shoppingCartItemModel.Quantity = shoppingCartItemModel.GroupProducts.FirstOrDefault().Quantity;
                        addToCartViewModel.ShoppingCartItems.Add(shoppingCartItemModel);
                    }

                    else if (!string.IsNullOrEmpty(shoppingCartItemModel.ConfigurableProductSKUs))
                    {
                        addToCartViewModel.OmsSavedCartLineItemId = shoppingCartItemModel.OmsSavedcartLineItemId;
                        addToCartViewModel.Quantity = shoppingCartItemModel.Quantity;
                        GetSelectedConfigurableProductsForAddToCart(addToCartViewModel, shoppingCartItemModel.ConfigurableProductSKUs);
                    }

                    else
                    {
                        addToCartViewModel.ShoppingCartItems.Add(shoppingCartItemModel);
                    }

                    AddToCartModel addToCartModel = addToCartViewModel.ToModel<AddToCartModel>();
                    addToCartModel.PublishedCatalogId = shoppingCart.PublishedCatalogId;
                    addToCartModel.Coupons = shoppingCart.Coupons;
                    addToCartModel.ZipCode = shoppingCart?.ShippingAddress?.PostalCode;
                    addToCartModel.UserId = shoppingCart.UserId;
                    addToCartModel.PortalId = shoppingCart.PortalId;
                    addToCartModel = _shoppingCartsClient.AddToCartProduct(addToCartModel);
                    addToCartModel.ShippingId = shoppingCart.ShippingId;
                    //Save items updated quantities with sku in list.
                    UpdateQuantityItem(shoppingCartItemModel);

                    // ShoppingCartItems set null to get from cart from database in GetCart
                    shoppingCart.ShoppingCartItems = null;

                    SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCart);

                    SaveInCookie(WebStoreConstants.CartCookieKey, addToCartModel.CookieMappingId);
                    
                }
            }
            return new AddToCartViewModel();
        }

        private void UpdateQuantityItem(ShoppingCartItemModel shoppingCartItemModel)
        {
            UpdatedProductQuantityModel updatedProduct = updatedProducts.FirstOrDefault(x =>
                                    x.SKU == shoppingCartItemModel.SKU &&
                                    x.AddOnProductSKUs == shoppingCartItemModel.AddOnProductSKUs &&
                                    x.AutoAddonSKUs == shoppingCartItemModel.AutoAddonSKUs &&
                                    x.ConfigurableProductSKUs == shoppingCartItemModel.ConfigurableProductSKUs &&
                                    x.BundleProductSKUs == shoppingCartItemModel.BundleProductSKUs &&
                                    x.PersonaliseValuesDetail == shoppingCartItemModel.PersonaliseValuesDetail);
            if (IsNotNull(updatedProduct))
            {
                updatedProduct.Quantity = shoppingCartItemModel.Quantity;
            }
            else
            {
                updatedProducts.Add(new UpdatedProductQuantityModel()
                {
                    SKU = shoppingCartItemModel.SKU,
                    AddOnProductSKUs = shoppingCartItemModel.AddOnProductSKUs,
                    AutoAddonSKUs = shoppingCartItemModel.AutoAddonSKUs,
                    BundleProductSKUs = shoppingCartItemModel.BundleProductSKUs,
                    ConfigurableProductSKUs = shoppingCartItemModel.ConfigurableProductSKUs,
                    Quantity = shoppingCartItemModel.Quantity,
                    PersonaliseValuesDetail = shoppingCartItemModel.PersonaliseValuesDetail
                });
            }
        }

        //Remove cart item from shopping cart.
        public virtual bool RemoveCartItem(string guiId)
        {
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);

            if (cartModel?.ShoppingCartItems?.Count < 0) { return false; }

            if (cartModel.ShoppingCartItems.Count.Equals(1))
            {
                //Remove all cart items if shopping cart having only one item.              
                return RemoveAllCartItems();
            }

            var cartItem = cartModel.ShoppingCartItems.FirstOrDefault(x => x.ExternalId == guiId);
            if (Equals(cartItem, null)) { return false; }

            //to remove child item if deleting the parent product having autoaddon associated with it
            if (!string.IsNullOrEmpty(cartItem.AutoAddonSKUs))
            {
                cartModel.RemoveAutoAddonSKU = cartItem.AutoAddonSKUs;
                cartModel.IsParentAutoAddonRemoved = string.Equals(cartItem.SKU.Trim(), cartItem.AutoAddonSKUs.Trim(), StringComparison.OrdinalIgnoreCase) ? false : true;
            }

            // Remove item and update the cart in Session and API.
            cartModel.ShoppingCartItems.Remove(cartItem);

            _shoppingCartsClient.RemoveCartLineItem(Convert.ToInt32(cartItem.OmsSavedcartLineItemId));

            //Save shopping cart in session.
            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);
            return true;

        }
        //Remove cart items from shopping cart.
        public virtual bool RemoveCartItems(string[] guiId)
        {
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);

            if (cartModel?.ShoppingCartItems?.Count < 0) { return false; }

            if (cartModel.ShoppingCartItems.Count.Equals(1))
            {
                //Remove all cart items if shopping cart having only one item.              
                return RemoveAllCartItems();
            }

            var cartItems = cartModel.ShoppingCartItems.Where(x => guiId.Contains(x.ExternalId)).ToArray();
            if (Equals(cartItems, null) || cartItems.Count() != guiId.Length) { return false; }

            foreach (var cartItem in cartItems)
            {
                //to remove child item if deleting the parent product having autoaddon associated with it
                if (!string.IsNullOrEmpty(cartItem.AutoAddonSKUs))
                {
                    cartModel.RemoveAutoAddonSKU = cartItem.AutoAddonSKUs;
                    cartModel.IsParentAutoAddonRemoved = string.Equals(cartItem.SKU.Trim(), cartItem.AutoAddonSKUs.Trim(), StringComparison.OrdinalIgnoreCase) ? false : true;
                }

                // Remove item and update the cart in Session and API.
                cartModel.ShoppingCartItems.Remove(cartItem);
            }

            cartModel.IsMerged = true;
            //Create new cart.
            ShoppingCartModel shoppingCartModel = _shoppingCartsClient.CreateCart(cartModel);

            //Save shopping cart in session.
            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            return true;
        }

        //Remove all cart items from shoppingCart.
        public virtual bool RemoveAllCartItems(int OmsOrderId = 0)
        {
            // Get cart from Session.
            ShoppingCartModel cart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);
            if (IsNotNull(cart))
            {
                RemoveCookie(WebStoreConstants.CartCookieKey);
                if (OmsOrderId < 1)
                    _shoppingCartsClient.RemoveAllCartItem(new CartParameterModel { UserId = cart.UserId.GetValueOrDefault(), CookieMappingId = cart.CookieMappingId });
                SaveInSession(WebStoreConstants.CartModelSessionKey, new ShoppingCartModel());
            }
            return true;
        }

        //Get count of cart items in shopping cart.
        public virtual decimal GetCartCount()
        {
            string cookieValue = GetFromCookie(WebStoreConstants.CartCookieKey);

            string count = _shoppingCartsClient.GetCartCount(new CartParameterModel
            {
                CookieMappingId = cookieValue,
                LocaleId = PortalAgent.LocaleId,
                PortalId = PortalAgent.CurrentPortal.PortalId,
                PublishedCatalogId = GetCatalogId().GetValueOrDefault(),
                UserId = HttpContext.Current.User.Identity.IsAuthenticated ? GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId : null
            });
            if (string.IsNullOrEmpty(count)) count = "0";

            return Convert.ToDecimal(Helper.GetRoundOffQuantity(count));
        }




        //Get count of cart items in shopping cart.
        public virtual decimal GetCartCount(int productId)
        {
            //Get Shopping cart from session or cookie.
            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                       GetCartFromCookie();
            var count = shoppingCartModel?.ShoppingCartItems?.FirstOrDefault(x => x.ProductId == productId)?.Quantity;

            return count == null ? 0 : Convert.ToDecimal(Helper.GetRoundOffQuantity(count.Value));
        }

        //Get template cart model session.
        public virtual TemplateViewModel GetTemplateCartModelSession()
        {
            //Get template cart model session.
            TemplateViewModel cartModel = GetFromSession<TemplateViewModel>(GetTemplateCartModelSessionKey());

            //Return cartModel.
            return cartModel;
        }

        //Get cart info from cookie.
        public virtual ShoppingCartModel GetCartFromCookie()
        {
            ShoppingCartModel shoppingCartModel = null;
            string cookieValue = GetFromCookie(WebStoreConstants.CartCookieKey);
            if (!string.IsNullOrEmpty(cookieValue) || HttpContext.Current.User.Identity.IsAuthenticated)
            {
                try
                {
                    _shoppingCartsClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
                    shoppingCartModel = _shoppingCartsClient.GetShoppingCart(new CartParameterModel
                    {
                        CookieMappingId = cookieValue,
                        LocaleId = PortalAgent.LocaleId,
                        PortalId = PortalAgent.CurrentPortal.PortalId,
                        PublishedCatalogId = GetCatalogId().GetValueOrDefault(),
                        UserId = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId
                    });
                    
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);                 
                }
            }
          
            return shoppingCartModel;
        }

        //Calculate shipping.
        public virtual CartViewModel CalculateShipping(int shippingOptionId, int shippingAddressId, string shippingCode, string additionalInstruction = "")
        {
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                         GetCartFromCookie();

            if (IsNull(cartModel?.Shipping))
            {
                cartModel.Shipping = new OrderShippingModel();
            }

            //bind shipping related data to ShoppingCartModel.
            cartModel.AdditionalInstructions = additionalInstruction;
            cartModel.Shipping.ShippingId = shippingOptionId > 0 ? shippingOptionId : cartModel.Shipping.ShippingId;
            cartModel.ShippingId = shippingOptionId > 0 ? shippingOptionId : cartModel.ShippingId;
            cartModel.Shipping.ResponseCode = (IsNotNull(shippingCode) && shippingCode != string.Empty) ? shippingCode : cartModel.Shipping.ResponseCode;
            cartModel.ShippingAddress = IsNotNull(cartModel?.ShippingAddress) ? cartModel.ShippingAddress : DependencyResolver.Current.GetService<IUserAgent>().GetAddressList()?.AddressList?.FirstOrDefault(x => x.AddressId == shippingAddressId)?.ToModel<AddressModel>();
            cartModel.Shipping.ShippingCountryCode = IsNull(cartModel?.ShippingAddress?.CountryName) ? "us" : cartModel.ShippingAddress.CountryName;
            //Calculate cart.
            CartViewModel cartViewModel = CalculateCart(cartModel, true)?.ToViewModel<CartViewModel>();
            cartViewModel.AdditionalInstruction = cartModel.AdditionalInstructions;
            cartModel.Shipping.ShippingDiscount = Convert.ToDecimal(cartViewModel?.Shipping?.ShippingDiscount);
            cartModel.ShippingCost = cartViewModel.ShippingCost;
            cartModel.TaxCost = cartViewModel.TaxCost;
            cartModel.Total = cartViewModel.Total;
            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);
            if (PortalAgent.CurrentPortal.EnableApprovalManagement)
            {
                PortalApprovalModel portalApprovalModel = GetClient<PortalClient>().GetPortalApproverDetailsById(PortalAgent.CurrentPortal.PortalId);
                cartViewModel.ApprovalType = portalApprovalModel?.PortalApprovalTypeName;
            }

            if (IsNotNull(cartViewModel))
            {
                //cartViewModel.IsSinglePageCheckout = PortalAgent.CurrentPortal.IsEnableSinglePageCheckout;

                if (!string.IsNullOrEmpty(cartViewModel.ShippingResponseErrorMessage))
                {
                    cartViewModel.HasError = true;
                    cartViewModel.IsValidShippingSetting = false;
                    cartViewModel.ErrorMessage = cartViewModel.ShippingResponseErrorMessage;
                }
                else if (cartViewModel.ShoppingCartItems.Where(w => w.IsAllowedTerritories == false).ToList().Count > 0)
                {
                    cartViewModel.HasError = true;
                }

                //Get User details from session.
                UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

                if (cartModel.OmsQuoteId > 0)
                    cartViewModel.IsLastApprover = _accountQuoteClient.IsLastApprover(cartModel.OmsQuoteId);
                else
                    cartViewModel.IsLastApprover = cartModel?.IsLastApprover ?? false;

                cartViewModel.IsLevelApprovedOrRejected = cartModel?.IsLevelApprovedOrRejected ?? false;
                cartViewModel.OmsQuoteId = cartModel?.OmsQuoteId ?? 0;

                //Bind User details to ShoppingCartModel.
                cartViewModel.CustomerPaymentGUID = userViewModel?.CustomerPaymentGUID;
                cartViewModel.UserId = IsNotNull(userViewModel) ? userViewModel.UserId : 0;
                cartViewModel.OrderStatus = cartModel.OrderStatus;
                cartViewModel.BudgetAmount = (userViewModel?.BudgetAmount).GetValueOrDefault();
                cartViewModel.PermissionCode = userViewModel?.PermissionCode;
                cartViewModel.RoleName = userViewModel?.RoleName;

                if (IsNotNull(cartViewModel?.ShoppingCartItems) && cartViewModel.ShoppingCartItems.Count > 0)
                {
                    cartViewModel.ShoppingCartItems = cartViewModel.ShoppingCartItems.OrderBy(c => c.GroupSequence).ToList();
                }
                //Commented this code as it is a diplicate call to IsRequireApprovalRouting() method . 
                //if (cartModel.UserId > 0)
                //{
                //    cartViewModel.IsRequireApprovalRouting = IsRequireApprovalRouting('0', cartViewModel.Total.Value);
                //}
            }
            return cartViewModel;
        }


        public virtual bool MergeCart()
        {
            ShoppingCartModel cart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                           GetCartFromCookie();

            //GetCurrent user's Id.
            int userId = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey).UserId;

            if (HttpContext.Current.Request.Url.AbsolutePath.ToUpper() != "/CHECKOUT/INDEX"
               && cart?.UserId != 0
               && userId != cart?.UserId)
            {
                //Nullify edit mode cart
                cart = null;
            }
            //Get shopping cart by userId.
            _shoppingCartsClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
            ShoppingCartModel cartModel = _shoppingCartsClient.GetShoppingCart(new CartParameterModel
            {
                UserId = userId,
                PortalId = PortalAgent.CurrentPortal.PortalId,
                LocaleId = PortalAgent.LocaleId,
                PublishedCatalogId = GetCatalogId().GetValueOrDefault()
            });

            //Check if cart persistent.
            CheckCartPersistent(userId, cartModel, cart);

            //to set user profile Id in shopping cart 
            cartModel.ProfileId = Helper.GetProfileId();

            bool status = false;
            //Update cart
            if (cart?.ShoppingCartItems?.Count > 0)
            {
                status = UpdateCart(ref cartModel);
            }

            //Save cart in session.
            if (status)
            {
                SaveInSession(WebStoreConstants.CartMerged, true);
            }

            cartModel.ShippingId = (cart?.ShippingId).GetValueOrDefault();
            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);
            RemoveCookie(WebStoreConstants.CartCookieKey);
            return status;
        }

        /// <summary>
        /// Merge Cart after login
        /// </summary>
        /// <returns></returns>
        public virtual bool MergeGuestUserCart()
        {
            bool status = false;
            ShoppingCartModel cart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                                     GetCartFromCookie();
            //Merge cart
            if (cart?.ShoppingCartItems?.Count() > 0)
            {
                RemoveInSession(WebStoreConstants.CartModelSessionKey);
                status = _shoppingCartsClient.MergeGuestUsersCart(GetFiltersForMergeCart(cart.CookieMappingId, Convert.ToInt32(cart.ShoppingCartItems.FirstOrDefault()?.OmsSavedcartLineItemId)));
            }

            //Get shopping cart by userId.
            _shoppingCartsClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
            ShoppingCartModel cartModel = _shoppingCartsClient.GetShoppingCart(new CartParameterModel
            {
                UserId = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey).UserId,
                PortalId = PortalAgent.CurrentPortal.PortalId,
                LocaleId = PortalAgent.LocaleId,
                PublishedCatalogId = GetCatalogId().GetValueOrDefault()
            });

            cartModel.ShippingId = (cart?.ShippingId).GetValueOrDefault();
            cartModel.Coupons = cart?.Coupons;
            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);
            RemoveCookie(WebStoreConstants.CartCookieKey);

            return status;
        }

        FilterCollection GetFiltersForMergeCart(string cookieMappingId, int omsSavedCartLineItemId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add("UserId", FilterOperators.Equals, GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey).UserId.ToString());
            filters.Add(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, PortalAgent.CurrentPortal.PortalId.ToString());
            filters.Add("CookieMappingId", FilterOperators.Equals, cookieMappingId);
            filters.Add("OmsSavedCartLineItemId", FilterOperators.Equals, omsSavedCartLineItemId.ToString());
            return filters;
        }

        //Apply & validate Coupon code.
        public virtual CartViewModel ApplyDiscount(string discountCode, bool isGiftCard)
        {
            //Get shopping cart form session or through cookie.
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                           GetCartFromCookie();

            if (IsNull(cartModel))
            {
                return (CartViewModel)GetViewModelWithErrorMessage(new CartViewModel(), string.Empty);
            }

            cartModel.UserId = GetUserDetails()?.UserId;
            //Apply giftcard if "isGiftCard" is true else apply coupon if any. 
            return isGiftCard ? ApplyGiftCard(discountCode.Trim(), cartModel)
                              : ApplyCoupon(discountCode.Trim(), cartModel);
        }
        //Removes the applied coupon from the cart.
        public virtual CartViewModel RemoveCoupon(string couponCode)
        {
            ShoppingCartModel cart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                       GetCartFromCookie();

            if (IsNull(cart))
            {
                return (CartViewModel)GetViewModelWithErrorMessage(new CartViewModel(), string.Empty);
            }

            if (IsNotNull(cart.Coupons))
            {
                cart.Coupons.RemoveAll(x => x.Code == couponCode);
            }

            //Calculate coupon with cart.
            ShoppingCartModel _cartModel = _shoppingCartsClient.Calculate(cart);

            //Required for session state SQLServer.
            SaveInSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey, _cartModel);

            return _cartModel?.ToViewModel<CartViewModel>();
        }


        // Get Cart method to check Session or Cookie to get the existing shopping cart.
        public virtual CartViewModel SetQuoteCart(int omsQuoteId)
        {
            ShoppingCartModel availableCart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                     GetCartFromCookie();

            if (availableCart.OmsQuoteId != omsQuoteId)
            {
                FilterCollection filters = new FilterCollection();
                filters.Add(ZnodeOmsQuoteEnum.OmsQuoteId.ToString(), FilterOperators.Equals, omsQuoteId.ToString());
                filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, PortalAgent.LocaleId.ToString());

                //Get account quote.
                _accountQuoteClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
                AccountQuoteModel accountQuoteModel = _accountQuoteClient.GetAccountQuote(new ExpandCollection() { ZnodeOmsQuoteEnum.ZnodeOmsQuoteLineItems.ToString() }, filters);

                ShoppingCartModel shoppingCartModel = accountQuoteModel.ShoppingCart;

                UserViewModel userDetails = _userClient.GetUserAccountData(accountQuoteModel.UserId, new ExpandCollection { ExpandKeys.Profiles }).ToViewModel<UserViewModel>();

                AccountQuoteViewModel accountQuoteViewModel = BindDataToAccountQuoteViewModel(userDetails, accountQuoteModel);
                shoppingCartModel.IsLevelApprovedOrRejected = accountQuoteViewModel?.IsLevelApprovedOrRejected ?? false;
                shoppingCartModel.IsLastApprover = accountQuoteViewModel?.IsLastApprover ?? false;

                shoppingCartModel.OmsQuoteId = omsQuoteId;

                if (IsNull(shoppingCartModel))
                {
                    return new CartViewModel()
                    {
                        HasError = true,
                        ErrorMessage = WebStore_Resources.OutofStockMessage
                    };
                }

                if (shoppingCartModel.ShoppingCartItems?.Count == 0)
                {
                    return new CartViewModel();
                }

                shoppingCartModel.QuotePaymentSettingId = accountQuoteModel.PaymentSettingId;

                ShoppingCartModel shoppingCartModelForCreatecart = shoppingCartModel;
                _shoppingCartsClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
                _shoppingCartsClient.CreateCart(shoppingCartModelForCreatecart);

                SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);

                //Set currency details.
                return SetCartCurrency(shoppingCartModel);
            }
            else
            {
                return SetCartCurrency(availableCart);
            }
        }

        #region Template
        //Create Shopping Cart.
        public virtual TemplateViewModel AddToTemplate(TemplateCartItemViewModel cartItem)
        {
            if (IsNotNull(cartItem))
            {
                //Get shopping cart data from session.
                TemplateViewModel shoppingCartModel = GetFromSession<TemplateViewModel>(GetTemplateCartModelSessionKey());
                cartItem.IsQuickOrderPad = true;
                //Bind Portal related data to Shopping cart model.
                return GetTemplateCart(cartItem, shoppingCartModel);
            }
            return new TemplateViewModel();
        }

        //Set template cart model to null.
        public virtual void SetTemplateCartModelSessionToNull()
        {
            SaveInSession<TemplateViewModel>(GetTemplateCartModelSessionKey(), null);
        }

        //Create template.
        public virtual bool CreateTemplate(TemplateViewModel templateViewModel)
        {
            AccountTemplateModel accountTemplateModel = templateViewModel.ToModel<AccountTemplateModel>();
            if (IsNotNull(accountTemplateModel))
            {
                TemplateViewModel cartItems = GetFromSession<TemplateViewModel>(GetTemplateCartModelSessionKey());
                accountTemplateModel.TemplateCartItems = cartItems?.TemplateCartItems.ToModel<TemplateCartItemModel>()?.ToList();
                SetTemplateModel(accountTemplateModel);
                templateViewModel = _accountQuoteClient.CreateTemplate(accountTemplateModel)?.ToViewModel<TemplateViewModel>();
                return IsNotNull(templateViewModel);
            }
            return false;
        }

        //Get template.
        public virtual TemplateViewModel GetTemplate(int omsTemplateId)
        {

            if (omsTemplateId > 0)
            {
                try
                {
                    ExpandCollection expands = new ExpandCollection();
                    expands.Add(ZnodeOmsTemplateEnum.ZnodeOmsTemplateLineItems.ToString());
                    expands.Add(ExpandKeys.Pricing);

                    FilterCollection filters = GetRequiredFilters();

                    //Get the account template model.
                    _accountQuoteClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
                    AccountTemplateModel accountTemplateModel = _accountQuoteClient.GetAccountTemplate(omsTemplateId, expands, filters);
                    if (IsNotNull(accountTemplateModel))
                    {
                        //Maps the model to view model.
                        TemplateViewModel viewModel = accountTemplateModel.ToViewModel<TemplateViewModel>();
                        viewModel.CurrencyCode = PortalAgent.CurrentPortal?.CurrencyCode;
                        viewModel.CultureCode = PortalAgent.CurrentPortal?.CultureCode;
                        //Maps the cart items.
                        viewModel.TemplateCartItems = accountTemplateModel.TemplateCartItems?.ToViewModel<TemplateCartItemViewModel>()?.ToList();

                        SetTemplateCartItemModel(viewModel.TemplateCartItems);

                        TemplateViewModel TemplateViewModel = GetFromSession<TemplateViewModel>(GetTemplateCartModelSessionKey());

                        //Get the template line items from session.
                        if (IsNotNull(TemplateViewModel) && omsTemplateId == TemplateViewModel.OmsTemplateId && TemplateViewModel.IsQuickOrderPad)
                        {
                            viewModel.TemplateCartItems = TemplateViewModel.TemplateCartItems;
                        }

                        //Save in session.
                        SaveInSession<TemplateViewModel>(GetTemplateCartModelSessionKey(), viewModel);
                        return viewModel;
                    }
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                    return null;
                }
            }
            return new TemplateViewModel();
        }

        //Remove cart item from template.
        public virtual bool RemoveTemplateCartItem(string guiId)
        {
            TemplateViewModel templateViewModel = GetFromSession<TemplateViewModel>(GetTemplateCartModelSessionKey());

            if (templateViewModel?.TemplateCartItems?.Count > 0)
            {
                if (templateViewModel.TemplateCartItems.Count.Equals(1))
                {
                    //Remove all cart items if shopping cart having only one item.              
                    return RemoveAllTemplateCartItems();
                }

                TemplateCartItemViewModel cartItem = templateViewModel.TemplateCartItems.FirstOrDefault(x => x.ExternalId == guiId);

                if (IsNull(cartItem))
                {
                    return false;
                }

                // Remove item and update the cart in Session and API.
                templateViewModel.TemplateCartItems.Remove(cartItem);
                templateViewModel.CurrencyCode = PortalAgent.CurrentPortal.CurrencyCode;
                templateViewModel.CultureCode = PortalAgent.CurrentPortal.CultureCode;
                //Save shopping cart in session.
                SaveInSession(GetTemplateCartModelSessionKey(), templateViewModel);

                //Delete template cart item.
                if (cartItem.OmsTemplateId > 0 && cartItem.OmsTemplateLineItemId > 0)
                {
                    return _accountQuoteClient.DeleteCartItem(new AccountTemplateModel { OmsTemplateId = cartItem.OmsTemplateId, OmsTemplateLineItemId = cartItem.OmsTemplateLineItemId.ToString() });
                }

                return true;
            }
            return false;
        }

        //Remove all cart items from template.
        public virtual bool RemoveAllTemplateCartItems()
        {
            // Get cart from Session.
            TemplateViewModel templateViewModel = GetFromSession<TemplateViewModel>(GetTemplateCartModelSessionKey());
            if (IsNotNull(templateViewModel))
            {
                //Delete template cart item.
                DeleteCartItem(templateViewModel);
                SaveInSession(GetTemplateCartModelSessionKey(), new TemplateViewModel());
                return true;
            }
            return false;
        }

        //Get template list.
        public virtual TemplateListViewModel GetTemplateList(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            //Set Filter for template list.
            filters = SetFilterForTemplateList(filters);

            AccountTemplateListModel tempateList = _accountQuoteClient.GetTemplateList(null, filters, sortCollection, pageIndex, recordPerPage);
            TemplateListViewModel templateListViewModel = new TemplateListViewModel { List = tempateList?.AccountTemplates?.ToViewModel<TemplateViewModel>().ToList(), RoleName = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.RoleName };

            //Set tool menu for template on grid list view.
            if (string.Equals(templateListViewModel.RoleName, ZnodeRoleEnum.Administrator.ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                SetTemplateListToolMenu(templateListViewModel);
            }

            SetListPagingData(templateListViewModel, tempateList);

            return templateListViewModel?.List?.Count > 0 ? templateListViewModel : new TemplateListViewModel() { List = new List<TemplateViewModel>(), RoleName = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.RoleName };
        }

        //Method to delete template.
        public virtual bool DeleteTemplate(string omsTemplateId)
        {
            try
            {
                return _accountQuoteClient.DeleteTemplate(new ParameterModel { Ids = omsTemplateId.ToString() });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                return false;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return false;
            }
        }

        //Method to delete  template cart item.
        public virtual bool DeleteCartItem(TemplateViewModel templateViewModel)
        {
            string _omsTempleteLineItemIds = string.Empty;
            try
            {
                if (templateViewModel?.TemplateCartItems?.Count > 0)
                {
                    _omsTempleteLineItemIds = string.Join(",", templateViewModel.TemplateCartItems.Select(x => x.OmsTemplateLineItemId));
                }

                return _accountQuoteClient.DeleteCartItem(new AccountTemplateModel { OmsTemplateId = templateViewModel.OmsTemplateId, OmsTemplateLineItemId = _omsTempleteLineItemIds });
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                return false;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return false;
            }
        }

        //Add multiple products to the cart.
        public virtual string AddMultipleProductsToCartTemplate(List<TemplateCartItemViewModel> cartItems)
        {
            string errorMessage = string.Empty;
            try
            {
                foreach (TemplateCartItemViewModel cartItem in cartItems)
                {
                    cartItem.IsQuickOrderPad = true;
                    AddToTemplate(cartItem);
                }

                return errorMessage;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return ex.Message;
            }
        }

        //Update quantity of template cart item.
        public virtual TemplateViewModel UpdateTemplateItemQuantity(string guid, decimal quantity, int productId)
        {
            // Get shopping cart from session.
            TemplateViewModel templateCart = GetFromSession<TemplateViewModel>(GetTemplateCartModelSessionKey());

            if (IsNotNull(templateCart))
            {
                //Get shopping cart item on the basis of guid.
                templateCart = GetTemplateItemByExternalId(templateCart, guid, quantity, productId);

                //Update shopping cart item quantity.
                if (IsNotNull(templateCart))
                {
                    return UpdateTemplateItemQuantity(templateCart, guid, quantity, productId);
                }
            }
            return new TemplateViewModel();
        }

        //Add the template in cart.
        public virtual string AddTemplateToCart(int omsTemplateId)
        {
            string errorMessage = string.Empty;
            if (omsTemplateId > 0)
            {
                ExpandCollection expands = new ExpandCollection();
                expands.Add(ZnodeOmsTemplateEnum.ZnodeOmsTemplateLineItems.ToString());

                FilterCollection filters = GetRequiredFilters();
                //Get the account template model.
                _accountQuoteClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
                AccountTemplateModel accountTemplateModel = _accountQuoteClient.GetAccountTemplate(omsTemplateId, expands, filters);

                if (accountTemplateModel?.TemplateCartItems?.Count > 0)
                {
                    return AddMultipleProductsToCart(accountTemplateModel.TemplateCartItems.ToViewModel<CartItemViewModel>().ToList());
                }
            }
            return errorMessage;
        }
        #endregion

        //Add multiple products to the cart.
        public virtual string AddMultipleProductsToCart(List<CartItemViewModel> cartItems)
        {
            string errorMessage = string.Empty;
            try
            {
                foreach (CartItemViewModel cartItem in cartItems)
                {
                    CreateCart(cartItem);
                }

                return errorMessage;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return ex.Message;
            }
        }

        //Check quantity for cart item.
        public virtual void CheckCartQuantity(ProductViewModel viewModel, decimal? quantity)
        {
            if (IsNotNull(viewModel))
            {
                bool AllowBackOrder = false;
                bool TrackInventory = false;
                decimal selectedQuantity = quantity.GetValueOrDefault();

                List<AttributesSelectValuesViewModel> inventorySetting = viewModel.Attributes?.SelectAttributeList(ZnodeConstant.OutOfStockOptions);
                if (inventorySetting?.Count > 0)
                {
                    ProductAgent.TrackInventoryData(ref AllowBackOrder, ref TrackInventory, inventorySetting.FirstOrDefault().Code);

                    if (viewModel.Quantity < selectedQuantity && !AllowBackOrder && TrackInventory)
                    {
                        viewModel.InventoryMessage = !string.IsNullOrEmpty(viewModel.OutOfStockMessage) ? viewModel.OutOfStockMessage : WebStore_Resources.TextOutofStock;
                        viewModel.ShowAddToCart = false;
                        return;
                    }
                    else if (viewModel.Quantity < selectedQuantity && AllowBackOrder && TrackInventory)
                    {
                        viewModel.InventoryMessage = !string.IsNullOrEmpty(viewModel.BackOrderMessage) ? viewModel.BackOrderMessage : WebStore_Resources.TextBackOrderMessage;
                        viewModel.ShowAddToCart = true;
                        return;
                    }

                    if (!Between(selectedQuantity, Convert.ToDecimal(viewModel.Attributes?.Value(ZnodeConstant.MinimumQuantity)), Convert.ToDecimal(viewModel.Attributes?.Value(ZnodeConstant.MaximumQuantity)), true))
                    {
                        viewModel.InventoryMessage = string.Format(WebStore_Resources.WarningSelectedQuantity, viewModel.Attributes?.Value(ZnodeConstant.MinimumQuantity), viewModel.Attributes?.Value(ZnodeConstant.MaximumQuantity));
                        viewModel.ShowAddToCart = false;
                        return;
                    }
                    viewModel.InventoryMessage = !string.IsNullOrEmpty(viewModel.InStockMessage) ? viewModel.InStockMessage : WebStore_Resources.TextInstock;
                    viewModel.ShowAddToCart = true;
                }
            }
        }

        //Get Shipping estimates for the provided zip code.
        public virtual ShippingOptionListViewModel GetShippingEstimates(string zipCode)
        {
            ShoppingCartModel cart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                        GetCartFromCookie();

            if (IsNull(cart))
            {
                return (ShippingOptionListViewModel)GetViewModelWithErrorMessage(new ShippingOptionListViewModel(), WebStore_Resources.ErrorNoCartItemsFound);
            }

            cart.PublishStateId = DefaultSettingHelper.GetCurrentorDefaultAppType(PortalAgent.CurrentPortal.PublishState);
            ShippingListModel lstModel = _shoppingCartsClient.GetShippingEstimates(zipCode, cart);

            ShippingOptionListViewModel lstViewModel = new ShippingOptionListViewModel { ShippingOptions = lstModel?.ShippingList?.ToViewModel<ShippingOptionViewModel>().ToList() };

            if (cart.Shipping?.ShippingId > 0)
            {
                lstViewModel.ShippingOptions?.Where(x => x.ShippingId == cart.Shipping.ShippingId)?.Select(y => { y.IsSelected = true; return y; })?.ToList();
            }

            lstViewModel = GetCurrencyFormattedRates(lstViewModel);
            if (IsNotNull(cart.ShippingAddress))
            {
                cart.ShippingAddress.PostalCode = zipCode;
                SaveInSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey, cart);
            }
            SaveInSession<String>(ZnodeConstant.ShippingEstimatedZipCode, zipCode);
            return lstViewModel;
        }

        //Insert estimated shipping id in session
        public virtual void AddEstimatedShippingIdToCartViewModel(int shippingId)
        {
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                      GetCartFromCookie();
            cartModel.ShippingId = shippingId;
            cartModel.Shipping.ShippingId = shippingId;
            SaveInSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey, cartModel);
        }

        //Insert estimated shipping details in session
        public virtual void AddEstimatedShippingDetailsToCartViewModel(int shippingId, string zipCode)
        {
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                      GetCartFromCookie();
            cartModel.ShippingId = shippingId;
            cartModel.Shipping.ShippingId = shippingId;

            if (IsNotNull(cartModel.ShippingAddress))
            {
                cartModel.ShippingAddress.PostalCode = zipCode;
            }
            else
            {
                cartModel.ShippingAddress = new AddressModel { PostalCode = zipCode };
            }

            SaveInSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey, cartModel);
            SaveInSession<String>(ZnodeConstant.ShippingEstimatedZipCode, zipCode);
        }

        //Create cartItem for Group type product.
        public virtual void GetSelectedGroupedProducts(CartItemViewModel cartItem, ShoppingCartModel shoppingCart)
        {
            //Get sku's and quantity of associated group products.
            string[] groupProducts = string.IsNullOrEmpty(cartItem.GroupProductSKUs) ? cartItem.GroupProducts?.Select(x => x.Sku)?.ToArray() : cartItem.GroupProductSKUs?.Split(',');
            string[] groupProductsQuantity = string.IsNullOrEmpty(cartItem.GroupProductsQuantity) ? cartItem.GroupProducts?.Select(x => Convert.ToString(x.Quantity))?.ToArray() : cartItem.GroupProductsQuantity?.Split('_');

            for (int index = 0; index < groupProducts?.Length; index++)
            {
                bool isNewExtIdRequired = false;
                if (!Equals(index, 0))
                {
                    isNewExtIdRequired = true;
                }

                ShoppingCartItemModel cartItemModel = BindGroupProducts(groupProducts[index], groupProductsQuantity[index], cartItem, isNewExtIdRequired);
                if (IsNotNull(cartItemModel))
                {
                    shoppingCart.ShoppingCartItems.Add(cartItemModel);
                }
            }
        }

        //Create cartItem for Group type product.
        public virtual void GetSelectedGroupedProductsForAddToCart(AddToCartViewModel cartItem)
        {
            //Get sku's and quantity of associated group products.
            string[] groupProducts = string.IsNullOrEmpty(cartItem.GroupProductSKUs) ? cartItem.GroupProducts?.Select(x => x.Sku)?.ToArray() : cartItem.GroupProductSKUs?.Split(',');
            string[] groupProductsQuantity = string.IsNullOrEmpty(cartItem.GroupProductsQuantity) ? cartItem.GroupProducts?.Select(x => Convert.ToString(x.Quantity))?.ToArray() : cartItem.GroupProductsQuantity?.Split('_');

            cartItem.SKU = cartItem.SKU;
            cartItem.AddOnProductSKUs = cartItem.AddOnProductSKUs;
            string addOnProductSKUs = cartItem.AutoAddonSKUs;

            for (int index = 0; index < groupProducts?.Length; index++)
            {
                bool isNewExtIdRequired = !Equals(index, 0);
                cartItem.ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId;
                if (CheckConfigurableProductForAddToCart(cartItem))
                {
                    cartItem = BindConfigurableProductsForAddToCart(groupProducts[index], groupProductsQuantity[index], cartItem, isNewExtIdRequired);
                }
                else
                {
                    cartItem.GroupProducts = new List<AssociatedProductModel> { new AssociatedProductModel { Sku = groupProducts[index], Quantity = decimal.Parse(groupProductsQuantity[index]) } };
                    cartItem.Quantity = decimal.Parse(groupProductsQuantity[index]);
                }

                if (IsNotNull(cartItem))
                {
                    GetSelectedAddOnProductsForAddToCart(cartItem, addOnProductSKUs);
                }
            }
        }

        //Create cartItem for Group type product.
        public virtual void GetSelectedBundleProductsForAddToCart(AddToCartViewModel cartItem, string bundleProductSKUs = null)
        {
            //Get sku's and quantity of associated group products.
            string[] bundleProducts = !string.IsNullOrEmpty(bundleProductSKUs) ? bundleProductSKUs.Split(',') : !string.IsNullOrEmpty(cartItem.BundleProductSKUs) ? cartItem.BundleProductSKUs?.Split(',') : null;

            cartItem.SKU = cartItem.SKU;
            string addOnProductSKUs = cartItem.AddOnProductSKUs;
            cartItem.AutoAddonSKUs = cartItem.AutoAddonSKUs;

            for (int index = 0; index < bundleProducts?.Length; index++)
            {
                bool isNewExtIdRequired = !Equals(index, 0);
                cartItem.ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId;
                cartItem.BundleProductSKUs = bundleProducts[index];
                cartItem.Quantity = cartItem.Quantity;

                if (IsNotNull(cartItem))
                {
                    GetSelectedAddOnProductsForAddToCart(cartItem, addOnProductSKUs);
                }
            }
        }

        //Create cartItem for configurable type product.
        public virtual void GetSelectedConfigurableProductsForAddToCart(AddToCartViewModel cartItem, string configurableProductSkus = null)
        {
            //Get sku's and quantity of associated group products.
            string[] configurableProducts = !string.IsNullOrEmpty(configurableProductSkus) ? configurableProductSkus.Split(',') : !string.IsNullOrEmpty(cartItem.ConfigurableProductSKUs) ? cartItem.ConfigurableProductSKUs?.Split(',') : null;

            cartItem.SKU = cartItem.SKU;
            string addOnProductSKUs = cartItem.AddOnProductSKUs;
            cartItem.AutoAddonSKUs = cartItem.AutoAddonSKUs;

            for (int index = 0; index < configurableProducts?.Length; index++)
            {
                bool isNewExtIdRequired = !Equals(index, 0);
                cartItem.ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId;
                cartItem.ConfigurableProductSKUs = configurableProducts[index];
                cartItem.Quantity = cartItem.Quantity;

                if (IsNotNull(cartItem))
                {
                    GetSelectedAddOnProductsForAddToCart(cartItem, addOnProductSKUs);
                }
            }
        }

        //Get the list of add-ons for cart line item on the basis of savedCartLineItemId.
        private List<AssociatedProductModel> GetAddOnsValueCartLineItem(List<ShoppingCartItemModel> childShoppingCartItems, int? savedCartLineItemId)
        {
            List<AssociatedProductModel> list = new List<AssociatedProductModel>();
            var lineItem = childShoppingCartItems.Where(y => y.ParentOmsSavedcartLineItemId == savedCartLineItemId && y.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.AddOns)).ToList();
            foreach (var item in lineItem)
                list.Add(new AssociatedProductModel { Sku = item.SKU, Quantity = item.Quantity, OrderLineItemRelationshipTypeId = Convert.ToInt32(item.OrderLineItemRelationshipTypeId) });
            return list;
        }

        public virtual void GetSelectedAddOnProductsForAddToCart(AddToCartViewModel cartItem, string addOnSKUS = null)
        {
            string[] addOnProducts = !string.IsNullOrEmpty(addOnSKUS) ? addOnSKUS.Split(',') : cartItem.AddOnProductSKUs?.Split(',');

            if (IsNull(cartItem.AssociatedAddOnProducts) && IsNotNull(addOnProducts))
            {
                cartItem.AssociatedAddOnProducts = new List<AssociatedProductModel>();
                for (int index = 0; index < addOnProducts.Length; index++)
                {                   
                    cartItem.AssociatedAddOnProducts.Add(new AssociatedProductModel { Sku = addOnProducts[index], AddOnQuantity = cartItem.Quantity, Quantity = cartItem.Quantity, OrderLineItemRelationshipTypeId = 1 });
                }
            }
     
            if (IsNotNull(cartItem.AssociatedAddOnProducts) && cartItem.AssociatedAddOnProducts.Count > 0)
            {
                for (int index = 0; index < cartItem.AssociatedAddOnProducts.Count(); index++)
                {
                    bool isNewExtIdRequired = !Equals(index, 0);
                    cartItem.ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId;
                    cartItem.AddOnProductSKUs = cartItem.AssociatedAddOnProducts[index].Sku;
                    cartItem.Quantity = cartItem.Quantity;
                    cartItem.SKU = cartItem.SKU;
                    cartItem.AddOnQuantity = cartItem.AssociatedAddOnProducts[index].AddOnQuantity;

                    if (IsNotNull(cartItem))
                        cartItem.ShoppingCartItems.Add(cartItem.ToModel<ShoppingCartItemModel>());
                }
            }
            else
                cartItem.ShoppingCartItems.Add(cartItem.ToModel<ShoppingCartItemModel>());
        }

        //Get the Group products
        /// <summary>
        /// 
        /// </summary>
        /// <param name="simpleProduct"></param>
        /// <param name="simpleProductQty"></param>
        /// <param name="cartItem"></param>
        /// <param name="isNewExtIdRequired"></param>
        /// <returns></returns>
        public virtual ShoppingCartItemModel BindGroupProducts(string simpleProduct, string simpleProductQty, CartItemViewModel cartItem, bool isNewExtIdRequired)
        {
            return new ShoppingCartItemModel
            {
                ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId,
                SKU = cartItem.SKU,
                Quantity = decimal.Parse(simpleProductQty),
                AddOnProductSKUs = cartItem.AddOnProductSKUs,
                AutoAddonSKUs = cartItem.AutoAddonSKUs,
                GroupProducts = new List<AssociatedProductModel> { new AssociatedProductModel { Sku = simpleProduct, Quantity = decimal.Parse(simpleProductQty) } }
            };
        }

        //Bind CartItemViewModel properties to ShoppingCartModel.
        public virtual void BindCartItemData(CartItemViewModel cartItem, ShoppingCartModel shoppingCartModel)
        {
            shoppingCartModel.OmsQuoteId = cartItem.OmsQuoteId;
            shoppingCartModel.OrderStatus = cartItem.OrderStatus;
            shoppingCartModel.ShippingId = cartItem.ShippingId;
            shoppingCartModel.ShippingAddressId = cartItem.ShippingAddressId;
            shoppingCartModel.BillingAddressId = cartItem.BillingAddressId;
            shoppingCartModel.SelectedAccountUserId = cartItem.SelectedAccountUserId > 0 ? cartItem.SelectedAccountUserId : shoppingCartModel.UserId.GetValueOrDefault();
        }

        //Get attribute values and code.
        public virtual void PersonalisedItems(CartItemViewModel cartItem)
        {
            //Attribute Code And Value 
            string[] Codes = cartItem.PersonalisedCodes?.Split(',');
            string[] Values = cartItem.PersonalisedValues?.Split('`');

            //To DO

            //if (Convert.ToBoolean(cartItem.PersonalisedValues?.Contains('`')))
            //{
            //    Values = cartItem.PersonalisedValues?.Split('`');
            //}
            //else
            //{
            //    Values = cartItem.PersonalisedValues?.Split(',');
            //}

            Dictionary<string, object> SelectedAttributes = new Dictionary<string, object>();
            if (IsNotNull(Values))
            {
                //Add code and value pair
                for (int i = 0; i < Codes.Length; i++)
                    SelectedAttributes.Add(Codes[i], Values[i]);
            }
            cartItem.PersonaliseValuesList = SelectedAttributes;
        }

        //Get attribute values and code.
        public virtual void PersonalisedItemsForAddToCart(AddToCartViewModel cartItem)
        {
            string[] Codes = cartItem.PersonalisedCodes?.Split(',');
            string[] Values = cartItem.PersonalisedValues?.Split('`');

            if (Convert.ToBoolean(cartItem.PersonalisedValues?.Contains('`')))
                Values = cartItem.PersonalisedValues?.Split('`');
            else
                Values = cartItem.PersonalisedValues?.Split(',');

            Dictionary<string, object> SelectedAttributes = new Dictionary<string, object>();
            if (IsNotNull(Values))
            {
                //Add code and value pair
                for (int i = 0; i < Codes.Length; i++)
                {
                    SelectedAttributes.Add(Codes[i], Values[i]);
                }
            }
            cartItem.PersonaliseValuesList = SelectedAttributes;
        }

        //Check if cart persistent.
        public virtual void CheckCartPersistent(int userId, ShoppingCartModel cartModel, ShoppingCartModel sessionCart)
        {
            if (IsNotNull(cartModel) && sessionCart?.ShoppingCartItems?.Count > 0)
            {
                List<ShoppingCartItemModel> cartItems = sessionCart.ShoppingCartItems;
                List<ShoppingCartItemModel> groupProducts = new List<ShoppingCartItemModel>();
                foreach (ShoppingCartItemModel guestCartItem in cartItems)
                {
                    foreach (ShoppingCartItemModel loginCartModel in cartModel.ShoppingCartItems)
                    {
                        if (Equals(loginCartModel.ProductType, ZnodeConstant.GroupedProduct) && Equals(guestCartItem.ProductType, ZnodeConstant.GroupedProduct))
                        {
                            if (Equals(loginCartModel.GroupProducts.FirstOrDefault().Sku, guestCartItem.GroupProducts.FirstOrDefault().Sku)
                                && Equals(loginCartModel.AddOnProductSKUs, guestCartItem.AddOnProductSKUs))
                            {
                                loginCartModel.GroupProducts.FirstOrDefault().Quantity = guestCartItem.GroupProducts.FirstOrDefault().Quantity;
                            }
                            else
                            {
                                string sku = guestCartItem.GroupProducts.FirstOrDefault().Sku;
                                string addonSku = guestCartItem.AddOnProductSKUs;
                                bool isCartItemAvailable = CheckIfGrouProductAvailable(sku, addonSku, cartModel.ShoppingCartItems);
                                int availableInGroupProduct = groupProducts.Where(gp => gp.GroupProducts.FirstOrDefault().Sku == sku && gp.AddOnProductSKUs == addonSku).ToList().Count;
                                if (!isCartItemAvailable && availableInGroupProduct == 0)
                                {
                                    groupProducts.Add(guestCartItem);
                                }
                            }
                        }
                        else
                        {
                            if (guestCartItem.PersonaliseValuesDetail?.Count > 0 || loginCartModel.PersonaliseValuesDetail?.Count > 0)
                            {
                                //Check if personalized values are identical for guest user cart item and logged in user cart item model.
                                if (loginCartModel.ConfigurableProductSKUs == guestCartItem.ConfigurableProductSKUs && guestCartItem.PersonaliseValuesDetail.Any(x => loginCartModel.PersonaliseValuesDetail.Any(y => y.PersonalizeCode == x.PersonalizeCode) && loginCartModel.PersonaliseValuesDetail.Any(y => y.PersonalizeValue == x.PersonalizeValue)))
                                {
                                    loginCartModel.Quantity = guestCartItem.Quantity;
                                    loginCartModel.PersonaliseValuesList = guestCartItem.PersonaliseValuesList;
                                    loginCartModel.PersonaliseValuesDetail = guestCartItem.PersonaliseValuesDetail;
                                    break;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(loginCartModel.ConfigurableProductSKUs) && !string.IsNullOrEmpty(loginCartModel.ConfigurableProductSKUs))
                                {
                                    UpdateConfigurableProduct(guestCartItem, loginCartModel);
                                }
                                else if (Equals(loginCartModel.SKU, guestCartItem.SKU))
                                {
                                    UpdateProductDetails(guestCartItem, loginCartModel);
                                }
                            }
                        }
                    }


                    try
                    {
                        //If product with different SKU exist, then cartCount is 0 and in that case add the item to cart.
                        var cartCount = cartModel?.ShoppingCartItems?.Where(cartModelItem => ((!string.IsNullOrEmpty(cartModelItem.ConfigurableProductSKUs)
                            ? cartModelItem.ConfigurableProductSKUs
                            : (string.IsNullOrEmpty(cartModelItem.AddOnProductSKUs)
                                ? cartModelItem.SKU
                                : cartModelItem.AddOnProductSKUs)) == (!string.IsNullOrEmpty(guestCartItem.ConfigurableProductSKUs)
                                    ? guestCartItem.ConfigurableProductSKUs
                                    : (string.IsNullOrEmpty(guestCartItem.AddOnProductSKUs)
                                        ? guestCartItem.SKU
                                        : guestCartItem.AddOnProductSKUs))) && (cartModelItem.PersonaliseValuesDetail?.Any(x => (cartModelItem.PersonaliseValuesDetail?.Any(y => y.PersonalizeCode == x.PersonalizeCode)).GetValueOrDefault() && (guestCartItem.PersonaliseValuesDetail?.Any(y => y.PersonalizeValue == x.PersonalizeValue)).GetValueOrDefault())).GetValueOrDefault())?.Count();


                        if (cartCount == 0)
                        {
                            cartModel.ShoppingCartItems.Add(guestCartItem);
                        }
                    }
                    catch (Exception)
                    {

                    }
                }

                //check if group products are available if so insert it in cart model
                if (groupProducts?.Count > 0)
                {
                    cartModel.ShoppingCartItems.AddRange(groupProducts);
                }

                //Check if cart persistent.
                if (cartModel?.ShoppingCartItems?.Count > 0 && PortalAgent.CurrentPortal.PersistentCartEnabled)
                {
                    cartModel.UserDetails = new UserModel() { UserId = userId };
                }

                if (IsNull(cartModel.ShippingAddress))
                {
                    cartModel.ShippingAddress = new AddressModel { PostalCode = sessionCart.ShippingAddress?.PostalCode };
                }

                //Get coupons if alreary applied.
                if (sessionCart?.Coupons?.Count > 0)
                {
                    //Remove Invalid coupon code.
                    foreach (var coupon in sessionCart.Coupons)
                    {
                        if (coupon.CouponApplied)
                        {
                            cartModel.Coupons.Add(coupon);
                        }
                    }
                }
            }
        }

        //add cart items to cart.
        public virtual bool UpdateCart(ref ShoppingCartModel cartModel)
        {
            try
            {
                cartModel.IsMerged = true;
                cartModel.ShoppingCartItems.ForEach(x => x.IsProductEdit = true);
                _shoppingCartsClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
                cartModel = _shoppingCartsClient.CreateCart(cartModel);
                return IsNotNull(cartModel);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return false;
            }
        }

        //add cart items to cart.
        public virtual ShoppingCartModel UpdateCartDetails(ShoppingCartModel cartModel)
        {
            try
            {
                cartModel.IsMerged = true;
                _shoppingCartsClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
                return _shoppingCartsClient.CreateCart(cartModel);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return null;
            }
        }

        #endregion

        #region AmazonPay
        public virtual CartViewModel CalculateAmazonShipping(int shippingOptionId, int shippingAddressId, string shippingCode, AddressViewModel addressViewModel)
        {
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);

            if (IsNull(cartModel?.Shipping))
            {
                cartModel.Shipping = new OrderShippingModel();
            }

            //bind shipping related data to ShoppingCartModel.
            cartModel.Shipping.ShippingId = shippingOptionId;
            cartModel.Shipping.ResponseCode = shippingCode;
            cartModel.ShippingAddress = addressViewModel.ToModel<AddressModel>();

            //Calculate cart.
            CartViewModel cartViewModel = CalculateCart(cartModel)?.ToViewModel<CartViewModel>();

            if (IsNotNull(cartViewModel) && cartModel.Shipping.ShippingId > 0)
            {
                cartViewModel.IsSinglePageCheckout = PortalAgent.CurrentPortal.IsEnableSinglePageCheckout;

                if (!string.IsNullOrEmpty(cartViewModel.ShippingResponseErrorMessage))
                {
                    cartViewModel.HasError = true;
                    cartViewModel.IsValidShippingSetting = false;
                    cartViewModel.ErrorMessage = cartViewModel.ShippingResponseErrorMessage;
                }
                else if (cartViewModel.ShoppingCartItems.Where(w => w.IsAllowedTerritories == false).ToList().Count > 0)
                {
                    cartViewModel.HasError = true;
                }
            }
            //Get User details from session.
            UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

            //Bind User details to ShoppingCartModel.
            cartViewModel.CustomerPaymentGUID = userViewModel?.CustomerPaymentGUID;
            cartViewModel.UserId = IsNotNull(userViewModel) ? userViewModel.UserId : 0;
            cartViewModel.OrderStatus = cartModel.OrderStatus;
            cartViewModel.BudgetAmount = (userViewModel?.BudgetAmount).GetValueOrDefault();
            cartViewModel.PermissionCode = userViewModel?.PermissionCode;
            cartViewModel.RoleName = userViewModel?.RoleName;
            return cartViewModel;
        }
        #endregion

        #region Private Methods       

        //Bind shopping cart values.
        public ShoppingCartModel GetShoppingCart(CartItemViewModel cartItem, ShoppingCartModel shoppingCart)
        {
            if (!string.IsNullOrEmpty(cartItem.PersonalisedCodes))
            {
                PersonalisedItems(cartItem);
            }

            //Check if cartitem contains group product and add it to shopping cart.
            if (!string.IsNullOrEmpty(cartItem.GroupProductSKUs) && !string.IsNullOrEmpty(cartItem.GroupProductsQuantity) || cartItem?.GroupProducts?.Count > 0)
            {
                GetSelectedGroupedProducts(cartItem, shoppingCart);
            }
            else
            {
                shoppingCart.ShoppingCartItems.Add(cartItem?.ToModel<ShoppingCartItemModel>());
            }

            shoppingCart.PortalId = PortalAgent.CurrentPortal.PortalId;
            shoppingCart.LocaleId = PortalAgent.LocaleId;
            shoppingCart.PublishedCatalogId = GetCatalogId().GetValueOrDefault();
            shoppingCart.UserId = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId;
            shoppingCart.ProfileId = Helper.GetProfileId();
            shoppingCart = GetEstimatedShippingDetails(shoppingCart);
            return shoppingCart;
        }

        //Bind shopping cart values.
        public AddToCartViewModel GetShoppingCartValues(AddToCartViewModel cartItem)
        {
            if (!string.IsNullOrEmpty(cartItem.PersonalisedCodes))
            {
                PersonalisedItemsForAddToCart(cartItem);
            }

            //Check if cart item contains group product and add it to shopping cart.
            if (!string.IsNullOrEmpty(cartItem.GroupProductSKUs) && !string.IsNullOrEmpty(cartItem.GroupProductsQuantity) || cartItem?.GroupProducts?.Count > 0)
            {
                GetSelectedGroupedProductsForAddToCart(cartItem);
            }
            else if (!string.IsNullOrEmpty(cartItem.BundleProductSKUs))
            {
                GetSelectedBundleProductsForAddToCart(cartItem);
            }
            else
            {
                GetSelectedAddOnProductsForAddToCart(cartItem);
            }

            cartItem.PortalId = PortalAgent.CurrentPortal.PortalId;
            cartItem.LocaleId = PortalAgent.LocaleId;
            cartItem.PublishedCatalogId = GetCatalogId().GetValueOrDefault();
            cartItem.UserId = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId;
            return cartItem;
        }

        //To check cartitem has configurable product or not.
        private bool CheckConfigurableProductForAddToCart(AddToCartViewModel cartItem)
        {
            return (!string.IsNullOrEmpty(cartItem.GroupProductSKUs) &&
                           !string.IsNullOrEmpty(cartItem.GroupProductsQuantity) &&
                           string.Equals(cartItem.ProductType, ZnodeConstant.ConfigurableProduct, StringComparison.CurrentCultureIgnoreCase)) ? true : false;
        }

        //to bind configurable products
        private AddToCartViewModel BindConfigurableProductsForAddToCart(string configurableSKU, string quantity, AddToCartViewModel cartItem, bool isNewExtIdRequired)
        {
            cartItem.Quantity = Convert.ToDecimal(quantity);
            cartItem.ConfigurableProductSKUs = configurableSKU;
            return cartItem;
        }

        //Bind template cart values.
        private TemplateViewModel GetTemplateCart(TemplateCartItemViewModel cartItem, TemplateViewModel shoppingCart)
        {
            if (string.Equals(cartItem.ProductType, ZnodeConstant.GroupedProduct, StringComparison.CurrentCultureIgnoreCase))
            {
                GetGroupProducts(cartItem, shoppingCart);
            }

            //Check if Shopping cart is null.
            if (IsNull(shoppingCart))
            {
                shoppingCart = new TemplateViewModel() { TemplateCartItems = new List<TemplateCartItemViewModel>() };
            }

            shoppingCart.HasError = !Equals(shoppingCart.TemplateCartItems.Where(x => x.ProductId == cartItem.ProductId)?.FirstOrDefault(), null);

            if (shoppingCart.HasError)
            {
                return shoppingCart;
            }

            if (!string.IsNullOrEmpty(cartItem.PersonalisedCodes))
            {
                PersonalisedItems(cartItem);
            }

            cartItem.OmsTemplateId = shoppingCart.OmsTemplateId;

            //Check if cartitem contains group product and add it to shopping cart.
            if (!string.IsNullOrEmpty(cartItem.GroupProductSKUs) && !string.IsNullOrEmpty(cartItem.GroupProductsQuantity))
            {
                GetSelectedGroupedTemplateProducts(cartItem, shoppingCart);
            }
            else
            {
                ProductViewModel product = new ProductAgent(GetClient<CustomerReviewClient>(), GetClient<PublishProductClient>(), GetClient<WebStoreProductClient>(), GetClient<SearchClient>(), GetClient<HighlightClient>(), GetClient<PublishCategoryClient>()).GetProductPriceAndInventory(cartItem.SKU, cartItem.Quantity.ToString(), cartItem.AddOnProductSKUs);
                if (IsNotNull(product))
                {
                    //Set product details.
                    SetProductDetails(cartItem, product);
                    shoppingCart.TemplateCartItems?.Add(cartItem);
                }
            }

            shoppingCart.UserId = Convert.ToInt32(GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId);
            shoppingCart.CurrencyCode = PortalAgent.CurrentPortal.CurrencyCode;
            shoppingCart.CultureCode = PortalAgent.CurrentPortal.CultureCode;
            shoppingCart.IsQuickOrderPad = cartItem.IsQuickOrderPad;
            shoppingCart.TemplateName = cartItem.TemplateName;
            SaveInSession(GetTemplateCartModelSessionKey(), shoppingCart);
            return shoppingCart;
        }

        //Get Bundle Product list.
        private List<BundleProductViewModel> GetBundleProduct(int productId)
        {
            IProductAgent _productAgent = new ProductAgent(null, _publishProductClient, null, null, null, null);

            //Get group product list.
            List<BundleProductViewModel> bundleProductList = _productAgent.GetBundleProduct(productId);
            return bundleProductList;
        }

        //Get group product list.
        private void GetGroupProducts(TemplateCartItemViewModel cartItem, TemplateViewModel shoppingCart)
        {
            IProductAgent _productAgent = new ProductAgent(null, _publishProductClient, null, null, null, null);

            //Get group product list.
            List<GroupProductViewModel> groupProductList = _productAgent.GetGroupProductList(Convert.ToInt32(cartItem.ProductId));

            //Convert List<GroupProductViewModel> to List<AssociatedProductModel>.
            cartItem.GroupProducts = ToAssociatedProductListModel(groupProductList, cartItem.Quantity);

            shoppingCart?.TemplateCartItems?.Select(x => x.GroupProducts = cartItem.GroupProducts);

            //Calculate and set total price.
            foreach (AssociatedProductModel item in cartItem.GroupProducts)
            {
                cartItem.ExtendedPrice += item.UnitPrice.GetValueOrDefault() * (item.Quantity);
            }
        }

        //Convert List<GroupProductViewModel> to List<AssociatedProductModel>.
        private List<AssociatedProductModel> ToAssociatedProductListModel(List<GroupProductViewModel> groupProductList, decimal quantity)
        {
            List<AssociatedProductModel> groupProducts = new List<AssociatedProductModel>();
            foreach (GroupProductViewModel groupProduct in groupProductList)
            {
                groupProducts.Add(new AssociatedProductModel
                {
                    Quantity = quantity,
                    ProductId = groupProduct.PublishProductId,
                    Sku = groupProduct.SKU,
                    ProductName = groupProduct.Name,
                    UnitPrice = groupProduct.RetailPrice,
                    CurrencyCode = groupProduct.CurrencyCode,
                    CultureCode = groupProduct.CultureCode,
                    InStockMessage = groupProduct.InStockMessage,
                    InventoryMessage = groupProduct.InventoryMessage,
                    OutOfStockMessage = groupProduct.OutOfStockMessage,
                    BackOrderMessage = groupProduct.BackOrderMessage,
                    MaximumQuantity = Convert.ToDecimal(groupProduct.Attributes?.Value(ZnodeConstant.MaximumQuantity)),
                    MinimumQuantity = Convert.ToDecimal(groupProduct.Attributes?.Value(ZnodeConstant.MinimumQuantity))
                }
                    );
            }
            return groupProducts;
        }

        //Get cart description.
        private string GetCartDescription(ProductViewModel product)
        {
            string cartdescription = string.Empty;
            List<BundleProductViewModel> bundleProductList = null;

            if (string.Equals(product.ProductType, ZnodeConstant.BundleProduct, StringComparison.CurrentCultureIgnoreCase))
            {
                bundleProductList = GetBundleProduct(product.PublishProductId);
            }

            if (IsNotNull(bundleProductList))
            {
                foreach (BundleProductViewModel bundle in bundleProductList)
                {
                    cartdescription += $"{ bundle.SKU } - { bundle.Name } <br/>";
                }
            }
            //Binds the cart description.
            foreach (AddOnViewModel addon in product?.AddOns)
            {
                if (IsNotNull(addon))
                {
                    if (addon.IsRequired)
                    {
                        cartdescription += $"{ addon.GroupName } : { addon.AddOnValues?.FirstOrDefault()?.Name } <br />";
                    }
                }
            }
            return cartdescription;
        }


        //Check quantity of cartitem.
        private ShoppingCartModel GetShoppingCartItemByExternalId(ShoppingCartModel cartModel, string guid, decimal quantity, int productId)
        {
            //Get shopping cart item having guid.
            ShoppingCartItemModel cartItem = cartModel?.ShoppingCartItems?.FirstOrDefault(x => x.ExternalId == guid);

            if (IsNotNull(cartItem))
            {
                //Get selected sku.
                string sku = productId > 0 ? cartItem.GroupProducts?.Where(x => x.ProductId == productId)?.FirstOrDefault()?.Sku
                                    : !string.IsNullOrEmpty(cartItem.ConfigurableProductSKUs) ? cartItem.ConfigurableProductSKUs : cartItem.SKU;
                ProductViewModel ProductDetails = CheckQuantity(sku, quantity, cartItem.AddOnProductSKUs);
                if (!ProductDetails.ShowAddToCart)
                {
                    if (IsNotNull(cartModel.ShoppingCartItems.FirstOrDefault(c => c.SKU == sku)))
                    {
                        cartModel.ShoppingCartItems.FirstOrDefault(c => c.SKU == sku).InsufficientQuantity = true;
                    }
                }
            }
            return cartModel;
        }

        //Check quantity of cartitem.
        private TemplateViewModel GetTemplateItemByExternalId(TemplateViewModel cartModel, string guid, decimal quantity, int productId)
        {
            //Get shopping cart item having guid.
            TemplateCartItemViewModel cartItem = cartModel?.TemplateCartItems?.FirstOrDefault(x => x.ExternalId == guid);

            if (IsNotNull(cartItem))
            {
                int indexOfSelectedElement = cartModel.TemplateCartItems.IndexOf(cartItem);
                if (cartItem?.GroupProducts?.Count() > 0)
                {
                    cartItem.ExtendedPrice = 0;

                    //Calculate and set Extended Price of group product.
                    foreach (AssociatedProductModel item in cartItem.GroupProducts)
                    {
                        cartItem.ExtendedPrice += item.UnitPrice.GetValueOrDefault() * (item.ProductId == productId ? quantity : item.Quantity);
                    }

                    cartModel.TemplateCartItems.RemoveAll(x => x.ExternalId == guid);
                    cartModel.TemplateCartItems.Insert(indexOfSelectedElement, cartItem);
                    return cartModel;
                }

                //Get selected sku.
                string sku = productId > 0 ? cartItem.GroupProducts?.Where(x => x.ProductId == productId)?.FirstOrDefault()?.Sku
                                    : !string.IsNullOrEmpty(cartItem.ConfigurableProductSKUs) ? cartItem.ConfigurableProductSKUs : cartItem.SKU;

                //Get available quantity of cart item.
                ProductViewModel product = new ProductAgent(GetClient<CustomerReviewClient>(), GetClient<PublishProductClient>(), GetClient<WebStoreProductClient>(), GetClient<SearchClient>(), GetClient<HighlightClient>(), GetClient<PublishCategoryClient>()).GetProductPriceAndInventory(sku, quantity.ToString(), cartItem.AddOnProductSKUs);

                //check if available quantity against selected quantity.
                CheckCartQuantity(product, quantity);

                if (product.ShowAddToCart)
                {
                    cartItem.UnitPrice = IsNotNull(product.PromotionalPrice) ? Convert.ToDecimal(product.PromotionalPrice) : IsNotNull(product.UnitPrice) ? Convert.ToDecimal(product.UnitPrice.ToPriceRoundOff()) : 0;
                    cartItem.ExtendedPrice = IsNotNull(product.PromotionalPrice) ? Convert.ToDecimal((product.PromotionalPrice * quantity).ToPriceRoundOff()) : IsNotNull(product.UnitPrice) ? Convert.ToDecimal((product.UnitPrice * quantity).ToPriceRoundOff()) : 0;
                    cartModel.TemplateCartItems.RemoveAll(x => x.ExternalId == guid);
                    cartModel.TemplateCartItems.Insert(indexOfSelectedElement, cartItem);
                    return cartModel;
                }
            }
            return null;
        }

        //Set Filter for template list.
        private FilterCollection SetFilterForTemplateList(FilterCollection filters)
        {
            if (IsNull(filters))
            {
                filters = new FilterCollection();
            }

            filters.RemoveAll(x => x.Item1 == ZnodeUserEnum.UserId.ToString());
            filters.RemoveAll(x => x.Item1 == FilterKeys.PortalId);
            filters.Add(new FilterTuple(ZnodeUserEnum.UserId.ToString(), FilterOperators.Equals, GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId.ToString()));
            filters.Add(FilterKeys.PortalId, FilterOperators.Equals, PortalAgent.CurrentPortal.PortalId.ToString());
            return filters;
        }

        //Check available quantity.
        private ProductViewModel CheckQuantity(string sku, decimal quantity, string addOnSkus)
        {
            //Get available quantity of cart item.
            ProductViewModel product = new ProductAgent(GetClient<CustomerReviewClient>(), GetClient<PublishProductClient>(), GetClient<WebStoreProductClient>(), GetClient<SearchClient>(), GetClient<HighlightClient>(), GetClient<PublishCategoryClient>()).GetProductPriceAndInventory(sku, quantity.ToString(), addOnSkus);

            //check if available quantity against selected quantity.
            CheckCartQuantity(product, quantity);

            return product;
        }

        //Get total quantity on hand by sku.
        private decimal GetQuantityOnHandBySku(string sku, int portalId)
        {
            _publishProductClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
            ProductInventoryPriceListModel productInventory = _publishProductClient.GetProductPriceAndInventory(new ParameterInventoryPriceModel { Parameter = sku, PortalId = portalId });
            return (productInventory?.ProductList?.Where(w => w.SKU == sku).FirstOrDefault().Quantity).GetValueOrDefault();
        }

        //Update shopping cart item quantity.
        private CartViewModel UpdatecartItemQuantity(ShoppingCartModel shoppingCart, string guid, decimal quantity, int productId)
        {
            //Update quantity and update the cart.
            if (productId > 0)
            {
                shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(x => x.GroupProducts?.Where(y => y.ProductId == productId)?.Select(z => { z.Quantity = quantity; return z; })?.FirstOrDefault()).ToList();
            }
            else
            {
                shoppingCart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(y => { y.Quantity = Convert.ToDecimal(quantity.ToInventoryRoundOff()); return y; }).ToList();
            }

            shoppingCart.IsMerged = true;
            _shoppingCartsClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
            ShoppingCartModel shoppingCartModel = _shoppingCartsClient.CreateCart(shoppingCart);

            //assign values which are required for the estimated shipping calculation.
            if (IsNotNull(shoppingCartModel))
            {
                shoppingCartModel.Shipping = shoppingCart.Shipping?.ShippingId > 0 ? shoppingCart.Shipping : shoppingCartModel.Shipping;
                shoppingCartModel.ShippingAddress = !string.IsNullOrEmpty(shoppingCart.ShippingAddress?.PostalCode) ? shoppingCart.ShippingAddress : shoppingCartModel.ShippingAddress;
            }

            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);

            return SetCartCurrency(shoppingCartModel);
        }

        //Update template cart item quantity.
        private TemplateViewModel UpdateTemplateItemQuantity(TemplateViewModel shoppingCart, string guid, decimal quantity, int productId)
        {
            //Update quantity and update the cart.
            if (productId > 0)
            {
                shoppingCart.TemplateCartItems?.Where(w => w.ExternalId == guid)?.Select(x => x.GroupProducts?.Where(y => y.ProductId == productId)?.Select(z => { z.Quantity = quantity; return z; })?.FirstOrDefault()).ToList();
            }
            else
            {
                shoppingCart.TemplateCartItems?.Where(w => w.ExternalId == guid)?.Select(y => { y.Quantity = quantity; return y; }).ToList();
            }

            shoppingCart.IsQuickOrderPad = true;
            shoppingCart.CurrencyCode = PortalAgent.CurrentPortal.CurrencyCode;
            shoppingCart.CultureCode = PortalAgent.CurrentPortal.CultureCode;
            SaveInSession(GetTemplateCartModelSessionKey(), shoppingCart);
            return shoppingCart;
        }

        //Calculate cart.
        protected virtual ShoppingCartModel CalculateCart(ShoppingCartModel shoppingCartModel, bool isCalCulateTaxAndShipping = true, bool isCalculateCart= true)
        {
            if (IsNotNull(shoppingCartModel))
            {
                shoppingCartModel.LocaleId = PortalAgent.LocaleId;
                shoppingCartModel.IsCalCulateTaxAndShipping = isCalCulateTaxAndShipping;
                string billingEmail = shoppingCartModel.BillingEmail;
                int shippingId = shoppingCartModel.ShippingId;
                int billingAddressId = shoppingCartModel.BillingAddressId;
                int shippingAddressId = shoppingCartModel.ShippingAddressId;
                int selectedAccountUserId = shoppingCartModel.SelectedAccountUserId;
                shoppingCartModel.ProfileId = Helper.GetProfileId();
                if (isCalculateCart)
                    shoppingCartModel = _shoppingCartsClient.Calculate(shoppingCartModel);
                //Bind required data to ShoppingCartModel.
                BindShoppingCartData(shoppingCartModel, billingEmail, shippingId, shippingAddressId, billingAddressId, selectedAccountUserId);
            }
            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            return shoppingCartModel;
        }

        //Bind required data to ShoppingCartModel.
        private void BindShoppingCartData(ShoppingCartModel shoppingCartModel, string billingEmail, int shippingId, int shippingAddressId, int billingAddressId, int selectedAccountUserId)
        {
            shoppingCartModel.BillingEmail = billingEmail;
            shoppingCartModel.ShippingId = shippingId;
            shoppingCartModel.ShippingAddressId = shippingAddressId;
            shoppingCartModel.BillingAddressId = billingAddressId;
            shoppingCartModel.SelectedAccountUserId = selectedAccountUserId;
        }

        //Create cartItem for Group type product.
        private void GetSelectedGroupedTemplateProducts(TemplateCartItemViewModel cartItem, TemplateViewModel shoppingCart)
        {
            //Get sku's and quantity of associated group products.
            string[] groupProducts = cartItem.GroupProductSKUs.Split(',');
            string[] groupProductsQuantity = cartItem.GroupProductsQuantity.Split('_');
            if (groupProductsQuantity.Count() <= 0)
            {
                groupProductsQuantity = cartItem.GroupProductsQuantity.Split(',');
            }

            cartItem.GroupProducts = new List<AssociatedProductModel>();

            //Get the associated group product skus and their quantities.
            for (int index = 0; index < groupProducts.Length; index++)
            {
                if (!string.IsNullOrEmpty(groupProductsQuantity[index]))
                {
                    cartItem.GroupProducts.Add(new AssociatedProductModel
                    {
                        Quantity = Convert.ToDecimal(groupProductsQuantity[index]),
                        Sku = groupProducts[index]
                    });
                }
            }
            shoppingCart.TemplateCartItems.Add(cartItem);
        }

        //Set currecy details for shopping cart and cart items.
        protected virtual CartViewModel SetCartCurrency(ShoppingCartModel cartModel)
        {
            if (IsNotNull(cartModel))
            {
                CartViewModel cartViewModel = cartModel.ToViewModel<CartViewModel>();

                // cartViewModel.ShoppingCartItems?.ForEach(item => item.GroupProducts.ForEach(y => y.Quantity = (y.Quantity)));

                string currencyCode = PortalAgent.CurrentPortal.CurrencyCode;
                string cultureCode = PortalAgent.CurrentPortal.CultureCode;
                cartViewModel.CultureCode = cultureCode;
                cartViewModel.CurrencyCode = currencyCode;
                cartViewModel.ZipCode = cartModel.ShippingAddress?.PostalCode;
                cartViewModel.ShoppingCartItems.Select(x => { x.CurrencyCode = currencyCode; x.CultureCode = cultureCode; return x; })?.ToList();
                cartViewModel.TaxCost = 0;
                return cartViewModel;
            }
            return new CartViewModel();
        }
        //get calculated shopping cart
        public virtual CartViewModel CalculateCart()
        {
            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);

            var model = CalculateCart(shoppingCartModel, false, true);

            return SetCartCurrency(model);
        }

        //Check whether approval routing is required for the current user quote.
        private bool IsRequireApprovalRouting(int quoteId, decimal quoteTotal)
        {
            bool IsRequireApprovalRouting = false;

            UserApproverListViewModel model = GetUserApproverList(0, true);
            if (model?.UserApprover?.Count > 0)
            {
                int firstLevelOrder = model.UserApprover.Min(x => x.ApproverOrder);
                decimal? firstLevelBudgetStart = model.UserApprover.Where(x => x.ApproverOrder == firstLevelOrder)?.Select(x => x.FromBudgetAmount)?.FirstOrDefault();
                if (IsNotNull(firstLevelBudgetStart) && quoteTotal > firstLevelBudgetStart)
                {
                    IsRequireApprovalRouting = true;
                }
            }
            return IsRequireApprovalRouting;
        }

        //Get user view model from session.
        public virtual UserViewModel GetUserViewModelFromSession()
        {
            return GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey) ?? GetFromSession<UserViewModel>(WebStoreConstants.GuestUserKey);
        }

        //Get the list of user approvers.
        public virtual UserApproverListViewModel GetUserApproverList(int omsQuoteId, bool showAllApprovers)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodeOmsQuoteEnum.OmsQuoteId.ToString(), FilterOperators.Equals, omsQuoteId.ToString()));
            filters.Add(new FilterTuple(ZnodeOmsTemplateEnum.UserId.ToString(), FilterOperators.Equals, GetUserViewModelFromSession()?.UserId.ToString()));
            filters.Add(new FilterTuple(ZnodeConstant.ShowAllApprovers.ToString().ToLower(), FilterOperators.Equals, showAllApprovers.ToString()));

            UserApproverListModel listModel = _accountQuoteClient.GetUserApproverList(null, filters, null, null, null);
            return new UserApproverListViewModel { UserApprover = listModel?.UserApprovers?.ToViewModel<UserApproverViewModel>()?.ToList() };
        }

        //Update Quantity and personalise value of a product.
        private void UpdateProductDetails(ShoppingCartItemModel guestCartItem, ShoppingCartItemModel loginCartModel)
        {
            loginCartModel.PersonaliseValuesList = guestCartItem.PersonaliseValuesList;
        }

        //Update Quantity and Personalise Value for configurable product.
        private void UpdateConfigurableProduct(ShoppingCartItemModel guestCartItem, ShoppingCartItemModel loginCartModel)
        {
            if (Equals(loginCartModel.ConfigurableProductSKUs, guestCartItem.ConfigurableProductSKUs))
            {
                UpdateProductDetails(guestCartItem, loginCartModel);
            }
        }

        private bool CheckIfGrouProductAvailable(string sku, string addonSku, List<ShoppingCartItemModel> shoppingCartItems)
        {
            if (shoppingCartItems.Count > 0)
            {
                return Convert.ToBoolean(shoppingCartItems.Where(sc => sc?.GroupProducts?.FirstOrDefault()?.Sku == sku && sc.AddOnProductSKUs == addonSku).ToList().Count);
            }
            else
            {
                return false;
            }
        }

        //Apply coupon code.
        private CartViewModel ApplyCoupon(string couponCode, ShoppingCartModel cartModel)
        {
            //Get coupons that are applied to cart.
            List<CouponModel> promocode = GetCoupons(cartModel);

            List<CouponViewModel> invalidCoupons = new List<CouponViewModel>();

            //Bind new coupon to shopping cart model to apply.
            if (!string.IsNullOrEmpty(couponCode))
            {
                GetNewCouponToApply(cartModel, couponCode);
            }
            else
            {
                invalidCoupons.Add(new CouponViewModel { Code = couponCode, PromotionMessage = WebStore_Resources.ErrorEmptyCouponCode, CouponApplied = false, CouponValid = false });
            }

            RemoveInvalidCoupon(cartModel);
            //Calculate coupon for the cart.
            ShoppingCartModel calculatedCart = _shoppingCartsClient.Calculate(cartModel);
            cartModel.Coupons = calculatedCart?.Coupons;
            cartModel.Total = calculatedCart?.Total;
            cartModel.Discount = Convert.ToDecimal(calculatedCart?.Discount);
            cartModel.ShippingCost = Convert.ToDecimal(calculatedCart?.ShippingCost);
            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);

            CartViewModel cartViewModel = calculatedCart?.ToViewModel<CartViewModel>();
            //Bind User details to ShoppingCartModel.
            cartViewModel.CustomerPaymentGUID = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.CustomerPaymentGUID;
            return cartViewModel;
        }

        //Add to promoCodes when shopping cart having coupons already.
        private List<CouponModel> GetCoupons(ShoppingCartModel cartModel)
        {
            return cartModel.Coupons?.Where(x => x.CouponApplied)?.Select(y => y)?.ToList();
        }

        //Get new coupon code and added to cart.
        private void GetNewCouponToApply(ShoppingCartModel cartModel, string couponCode)
        {
            //Check if any coupon is already appled to cart and add new coupon.
            bool isCouponNotAvailableInCart = Equals(cartModel.Coupons?.Where(p => Equals(p.Code, couponCode)).ToList().Count, 0);

            //Add coupon code if not present in cart.
            if (isCouponNotAvailableInCart)
            {
                cartModel.Coupons.Add(new CouponModel { Code = couponCode });
            }
        }

        //Apply gift card.
        private CartViewModel ApplyGiftCard(string giftCardNumber, ShoppingCartModel cartModel)
        {
            cartModel.GiftCardNumber = giftCardNumber;
            //calculate price of shopping cart after gift card applied.
            ShoppingCartModel calculatedCart = _shoppingCartsClient.Calculate(cartModel);
            cartModel.Coupons = calculatedCart?.Coupons;
            cartModel.Total = calculatedCart?.Total;
            cartModel.Discount = Convert.ToDecimal(calculatedCart?.Discount);
            cartModel.ShippingCost = Convert.ToDecimal(calculatedCart?.ShippingCost);
            cartModel.GiftCardAmount=calculatedCart.GiftCardAmount;
            cartModel.GiftCardBalance=calculatedCart.GiftCardBalance;
            cartModel.GiftCardMessage = calculatedCart.GiftCardMessage;
            cartModel.GiftCardNumber = calculatedCart.GiftCardNumber;
            cartModel.GiftCardValid = calculatedCart.GiftCardValid;
            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);

            CartViewModel cartViewModel = calculatedCart?.ToViewModel<CartViewModel>();
            if (IsNotNull(cartViewModel))
            {
                cartViewModel.SuccessMessage = cartViewModel.GiftCardMessage;
            }
            //Bind User details to ShoppingCartModel.
            cartViewModel.CustomerPaymentGUID = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.CustomerPaymentGUID;
            return cartViewModel;           
        }

        //Set template model.
        private void SetTemplateModel(AccountTemplateModel accountTemplateModel)
        {
            accountTemplateModel.PortalId = PortalAgent.CurrentPortal.PortalId;
            accountTemplateModel.LocaleId = PortalAgent.LocaleId;
            accountTemplateModel.PublishedCatalogId = PortalAgent.CurrentPortal.PublishCatalogId;
            accountTemplateModel.UserId = Convert.ToInt32(GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId);
        }

        //Set the Tool Menus for Template List Grid View.
        private void SetTemplateListToolMenu(TemplateListViewModel model)
        {
            if (IsNotNull(model))
            {
                model.GridModel = new GridModel();
                model.GridModel.FilterColumn = new FilterColumnListModel();
                model.GridModel.FilterColumn.ToolMenuList = new List<ToolMenuModel>();
                model.GridModel.FilterColumn.ToolMenuList.Add(new ToolMenuModel { DisplayText = Admin_Resources.ButtonDelete, JSFunctionName = $"User.prototype.DeleteTemplate(this)", ControllerName = "User", ActionName = "DeleteTemplate" });
            }
        }

        //Sets the template cart item model.
        private void SetTemplateCartItemModel(List<TemplateCartItemViewModel> templateCartItems)
        {
            //Check for null check.
            if (templateCartItems?.Count > 0)
            {
                IProductAgent productAgent = new ProductAgent(GetClient<CustomerReviewClient>(), GetClient<PublishProductClient>(), GetClient<WebStoreProductClient>(), GetClient<SearchClient>(), GetClient<HighlightClient>(), GetClient<PublishCategoryClient>());
                foreach (var cartItem in templateCartItems)
                {
                    //Sets the properties.
                    ProductViewModel product = productAgent.GetProductPriceAndInventory(cartItem.SKU, cartItem.Quantity.ToString(), cartItem.AddOnProductSKUs);
                    if (IsNotNull(product))
                    {
                        SetProductDetails(cartItem, product);
                    }
                }
            }
        }

        //Maps the product details in TemplateCartItemViewModel.
        private void SetProductDetails(TemplateCartItemViewModel cartItem, ProductViewModel product)
        {
            if (IsNotNull(product))
            {
                cartItem.CartDescription = GetCartDescription(product);
                //Maps the data properly.
                cartItem.ProductId = product.ConfigurableProductId > 0 ? product.ConfigurableProductId.ToString() : product.PublishProductId.ToString();
                cartItem.UnitPrice = IsNotNull(product.PromotionalPrice) ? Convert.ToDecimal(product.PromotionalPrice) : IsNotNull(product.UnitPrice) ? Convert.ToDecimal(product.UnitPrice.ToPriceRoundOff()) : 0;
                cartItem.ExtendedPrice = cartItem?.GroupProducts?.Count() > 0 ? Convert.ToDecimal(cartItem.ExtendedPrice.ToPriceRoundOff()) : IsNotNull(product.PromotionalPrice) ? Convert.ToDecimal((product?.PromotionalPrice.GetValueOrDefault() * cartItem.Quantity).ToPriceRoundOff()) : Convert.ToDecimal((product?.UnitPrice.GetValueOrDefault() * cartItem.Quantity).ToPriceRoundOff());
                cartItem.ProductName = product.Name;
                cartItem.ImagePath = product.ImageSmallPath;
                cartItem.SEODescription = product.SEODescription;
                cartItem.SEOKeywords = product.SEOKeywords;
                cartItem.SEOTitle = product.SEOTitle;
                cartItem.SEOUrl = product.SEOUrl;
                cartItem.MaxQuantity = Convert.ToDecimal(product.Attributes?.Value(ZnodeConstant.MaximumQuantity));
                cartItem.MinQuantity = Convert.ToDecimal(product.Attributes?.Value(ZnodeConstant.MinimumQuantity));
                if (!string.IsNullOrEmpty(product?.ConfigurableProductSKU))
                {
                    cartItem.SKU = product.SKU;
                    cartItem.ConfigurableProductSKUs = string.Join(",", product?.Attributes?.Where(x => x.AttributeCode == "SKU").Select(x => x.AttributeValues).Distinct());
                    cartItem.CartDescription = string.Join("<br>", product?.Attributes?.Where(x => x.IsConfigurable).Select(x => x.AttributeName + " - " + x.AttributeValues));
                }
            }
        }

        //Remove Shipping and Tax calculation From Cart.
        protected virtual void RemoveShippingTaxFromCart(ShoppingCartModel shoppingCartModel)
        {
            shoppingCartModel.ShippingAddress = IsNotNull(shoppingCartModel.ShippingAddress) ? shoppingCartModel.ShippingAddress : null;
            shoppingCartModel.BillingAddress = null;
            shoppingCartModel.Shipping = new OrderShippingModel() { ShippingId = shoppingCartModel.ShippingId > 0 ? shoppingCartModel.ShippingId : 0, ShippingCountryCode = "us" };
            shoppingCartModel.Payment = new PaymentModel();
            shoppingCartModel.BillingEmail = null;
            RemoveTaxFromCart(shoppingCartModel);
        }

        //Remove Tax calculation From Cart.
        protected virtual void RemoveTaxFromCart(ShoppingCartModel shoppingCartModel)
        {
            shoppingCartModel.TaxCost = 0;
            shoppingCartModel.SalesTax = 0;
            shoppingCartModel.TaxRate = 0;
            shoppingCartModel.Vat = 0;
            shoppingCartModel.Pst = 0;
            shoppingCartModel.Hst = 0;
            shoppingCartModel.Gst = 0;
            shoppingCartModel.OrderLevelTaxes = 0;
        }

        //Convert the shipping rate to currency formatted rate.
        private ShippingOptionListViewModel GetCurrencyFormattedRates(ShippingOptionListViewModel lstViewModel)
        {
            ShippingOptionListViewModel model = new ShippingOptionListViewModel();
            List<ShippingOptionViewModel> lstModel = new List<ShippingOptionViewModel>();
            string cultureCode = PortalAgent.CurrentPortal.CultureCode;
            if (lstViewModel?.ShippingOptions?.Count > 0)
            {
                foreach (ShippingOptionViewModel shipping in lstViewModel?.ShippingOptions)
                {
                    shipping.FormattedShippingRate = Helper.FormatPriceWithCurrency(shipping.ShippingRate, cultureCode);
                    lstModel.Add(shipping);
                }
            }

            model.ShippingOptions = lstModel;
            return model;
        }

        //to remove invalid coupon form cart 
        private void RemoveInvalidCoupon(ShoppingCartModel cartModel)
        {
            if (cartModel.Coupons?.Count > 0)
            {
                cartModel.Coupons.RemoveAll(x => !x.CouponApplied && !string.IsNullOrEmpty(x.PromotionMessage));
            }
        }

        //Get template cart model session key.
        private string GetTemplateCartModelSessionKey()
        {
            //GetCurrent user's Id.
            int userId = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey).UserId;
            return WebStoreConstants.TemplateCartModelSessionKey + userId;
        }

        //Convert string type to decimal for quantity.
        private static decimal ModifyQuantityValue(string quantity)
        {
            decimal newQuantity = 0;
            if (quantity.Contains(","))
            {
                newQuantity = Convert.ToDecimal((quantity).Replace(",", "."));
            }
            else
            {
                newQuantity = decimal.Parse(quantity, CultureInfo.InvariantCulture);
            }

            return newQuantity;
        }

        //Map AddToCartViewModel to ShoppingCartModel
        protected virtual ShoppingCartModel MapAddToCartToShoppingCart(AddToCartViewModel addToCartViewModel, ShoppingCartModel shoppingCartModel)
        {
            if (IsNotNull(addToCartViewModel?.Coupons) && IsNotNull(shoppingCartModel?.ShoppingCartItems))
            {
                shoppingCartModel.Coupons = new List<CouponModel>();
                shoppingCartModel.Coupons.AddRange(addToCartViewModel.Coupons);
            }
            if (IsNotNull(addToCartViewModel?.ShoppingCartItems) && IsNotNull(shoppingCartModel?.ShoppingCartItems))
            {
                shoppingCartModel.ShoppingCartItems = null;
            }
            shoppingCartModel.ExternalId = addToCartViewModel.ExternalId;
            shoppingCartModel.PublishedCatalogId = addToCartViewModel.PublishedCatalogId;
            shoppingCartModel.LocaleId = addToCartViewModel.LocaleId;
            shoppingCartModel.UserId = addToCartViewModel.UserId;
            shoppingCartModel.PortalId = addToCartViewModel.PortalId;
            shoppingCartModel.CookieMappingId = addToCartViewModel.CookieMappingId;
            shoppingCartModel.ShippingId = addToCartViewModel.ShippingId;
            return shoppingCartModel;
        }


        //Get details from shoppincart session which are required to calculate Estimated shipping cost.
        private ShoppingCartModel GetEstimatedShippingDetails(ShoppingCartModel shoppingCart)
        {
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);
            if (IsNotNull(cartModel))
            {
                shoppingCart.ShippingAddress = IsNotNull(cartModel.ShippingAddress) ? cartModel.ShippingAddress : shoppingCart.ShippingAddress;
                shoppingCart.Shipping = cartModel.Shipping?.ShippingId > 0 ? cartModel.Shipping : shoppingCart.Shipping;
                shoppingCart.ShippingId = cartModel.ShippingId;
            }
            return shoppingCart;
        }


        //Bind user details to AccountQuoteViewModel.
        private AccountQuoteViewModel BindDataToAccountQuoteViewModel(UserViewModel userDetails, AccountQuoteModel accountQuoteModel)
        {
            AccountQuoteViewModel accountQuoteViewModel = accountQuoteModel?.ToViewModel<AccountQuoteViewModel>();
            if (IsNotNull(accountQuoteViewModel))
            {
                accountQuoteViewModel.CurrencyCode = DefaultSettingHelper.DefaultCurrency;
                accountQuoteViewModel.RoleName = userDetails?.RoleName;
                accountQuoteViewModel.PermissionCode = userDetails?.PermissionCode;
                accountQuoteViewModel.CartItemCount = Convert.ToString(GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey)?.ShoppingCartItems.Count()) ??
                          GetFromCookie(WebStoreConstants.CartCookieKey);
                PortalViewModel portal = PortalAgent.CurrentPortal;
                accountQuoteViewModel.CurrencyCode = portal?.CurrencyCode;

                if (accountQuoteModel.ShoppingCart?.ShoppingCartItems?.Count() > 0)
                {
                    //If inventory is out of stock set error message.
                    if (accountQuoteModel.ShoppingCart.ShoppingCartItems.Any(x => x.InsufficientQuantity))
                    {
                        accountQuoteViewModel.OutOfStockMessage = portal?.OutOfStockMessage;
                    }
                }
                accountQuoteViewModel.LoggedInUserId = GetUserDetails().UserId;
                accountQuoteViewModel.IsLastApprover = _accountQuoteClient.IsLastApprover(accountQuoteModel.OmsQuoteId);
                accountQuoteModel.ShoppingCart.Shipping.ShippingName = accountQuoteModel.ShippingName;
            }
            return accountQuoteViewModel;
        }

        #endregion
    }
}