﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Core.Agents.IAgent;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.WebStore
{
    public abstract class BaseAgent
    {
        public ExpandCollection Expands { get; set; }
        public FilterCollection Filters { get; set; }
        public SortCollection Sorts { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        int count = 1;

        #region Protected Methods
        /// <summary>
        /// Get API client object with current domain name and key.
        /// </summary>
        /// <typeparam name="T">The type of API client object.</typeparam>
        /// <returns>An API client object of type T.</returns>
        protected T GetClient<T>() where T : class
        {
            var obj = Activator.CreateInstance<T>();
            if (!(obj is BaseClient)) return obj;
            if (!(obj as BaseClient).IsGlobalAPIAuthorization)
            {
                // Need to use Url.Authority to set domin name and its API key for multi store
                var urlAuthority = HttpContext.Current.Request.Url.Authority;
                (obj as BaseClient).DomainName = urlAuthority;
                (obj as BaseClient).DomainKey = ConfigurationManager.AppSettings[urlAuthority];
            }
            else
            {
                (obj as BaseClient).DomainName = ConfigurationManager.AppSettings["ZnodeApiDomainName"];
                (obj as BaseClient).DomainKey = ConfigurationManager.AppSettings["ZnodeApiDomainKey"];
            }

            // Set account ID to get the profile based API response
            if (HelperUtility.IsNotNull(HttpContext.Current.User) && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                UserViewModel model = GetUserDetails();
                if (HelperUtility.IsNotNull(model))
                    (obj as BaseClient).UserId = model.UserId;
            }
                       
            (obj as BaseClient).PublishStateHeader = GetPublishState().ToString();
            (obj as BaseClient).LocaleHeader = GetStoreLocale().ToString();

            return obj;
        }

        /// <summary>
        /// Get API client object with current domain name and key.
        /// </summary>
        /// <typeparam name="T">The type of API client object.</typeparam>
        /// <returns>An API client object of type T.</returns>
        protected T GetClient<T>(T obj) where T : IBaseClient
        {
            if (!(obj is BaseClient)) return obj;
            if (!(obj as BaseClient).IsGlobalAPIAuthorization)
            {
                // Need to use Url.Authority to set domin name and its API key for multi store
                var urlAuthority = HttpContext.Current.Request.Url.Authority;
                (obj as BaseClient).DomainName = urlAuthority;
                (obj as BaseClient).DomainKey = ConfigurationManager.AppSettings[urlAuthority];
            }
            else
            {
                (obj as BaseClient).DomainName = ConfigurationManager.AppSettings["ZnodeApiDomainName"];
                (obj as BaseClient).DomainKey = ConfigurationManager.AppSettings["ZnodeApiDomainKey"];
            }
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var model = GetUserDetails();
                if (HelperUtility.IsNotNull(model))
                {
                    (obj as BaseClient).UserId = model.UserId;
                }
            }
                        
            (obj as BaseClient).PublishStateHeader = GetPublishState().ToString();
            (obj as BaseClient).LocaleHeader = GetStoreLocale().ToString();

            ICustomHeaders _headerAgent = DependencyResolver.Current.GetService<ICustomHeaders>();

            if (HelperUtility.IsNotNull(_headerAgent))
            {
                Dictionary<string, string> headers = _headerAgent.SetCustomHeaderOfClient();

                int? count = headers?.Count;

                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        switch (i.ToString())
                        {
                            case "0":
                                (obj as BaseClient).Custom1 = $"{headers.ElementAt(i).Key}:{headers.ElementAt(i).Value}";
                                break;
                            case "1":
                                (obj as BaseClient).Custom2 = $"{headers.ElementAt(i).Key}:{headers.ElementAt(i).Value}";
                                break;
                            case "2":
                                (obj as BaseClient).Custom3 = $"{headers.ElementAt(i).Key}:{headers.ElementAt(i).Value}";
                                break;
                            case "3":
                                (obj as BaseClient).Custom4 = $"{headers.ElementAt(i).Key}:{headers.ElementAt(i).Value}";
                                break;
                            case "4":
                                (obj as BaseClient).Custom5 = $"{headers.ElementAt(i).Key}:{headers.ElementAt(i).Value}";
                                break;
                        }
                    }
                }
            }

            return obj;
        }

        //Get currently logged in user details.
        public UserViewModel GetUserDetails()
        {
            UserViewModel model = null;
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    IUserClient client = new UserClient();

                    //get user details from session if session dat is not null.
                    if (Equals(GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey), null))
                    {
                        //Get the User Details.
                        //Don't Use the Agent here, it will cause the infinite looping. As this method gets called from the BaseAgent.
                        UserModel userModel = client.GetAccountByUser(HttpContext.Current.User.Identity.Name);
                        if (HelperUtility.IsNotNull(userModel))
                        {
                            SaveInSession<UserViewModel>(WebStoreConstants.UserAccountKey, userModel.ToViewModel<UserViewModel>());
                            model = userModel.ToViewModel<UserViewModel>();
                        }
                        client = null;
                    }
                    else
                        //Set value from Session Variable
                        model = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);
                }
            } catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            return model;
        }

        /// <summary>
        /// Gets a cookie value.
        /// </summary>
        /// <param name="key">The key for the value being retrieved.</param>
        /// <returns>The value for the key.</returns>
        protected string GetFromCookie(string key)
        {
            var cookie = HttpContext.Current.Request.Cookies.Get(key);
            if (cookie != null)
            {
                return cookie.Value;
            }

            return String.Empty;
        }

        /// <summary>
        /// Gets an object from session.
        /// </summary>
        /// <typeparam name="T">The type of object to retrieve.</typeparam>
        /// <param name="key">The key for the session object being retrieved.</param>
        /// <returns>The object of type T from session.</returns>
        protected T GetFromSession<T>(string key) => SessionHelper.GetDataFromSession<T>(key);

        /// <summary>
        /// Saves a cookie value.
        /// </summary>
        /// <param name="key">The key for the cookie value.</param>
        /// <param name="value">The cookie value.</param>
        protected void SaveInCookie(string key, string value) =>
            HttpContext.Current.Response.Cookies.Add(new HttpCookie(key)
            {
                Expires = DateTime.Now.AddYears(1),
                Name = key,
                Value = value
            });

        /// <summary>
        /// Removes a cookie value.
        /// </summary>
        /// <param name="key">The key for the cookie value being removed.</param>
        protected void RemoveCookie(string key)
        {
            var cookie = HttpContext.Current.Request.Cookies.Get(key);
            if (cookie == null) return;
            cookie.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Saves an object in session.
        /// </summary>
        /// <typeparam name="T">The type of object being saved.</typeparam>
        /// <param name="key">The key for the session object.</param>
        /// <param name="value">The value of the session object.</param>
        protected void SaveInSession<T>(string key, T value) => SessionHelper.SaveDataInSession<T>(key, value);

        /// <summary>
        /// Removes an object from session.
        /// </summary>
        /// <param name="key">The key of the session object.</param>
        protected void RemoveInSession(string key) => SessionHelper.RemoveDataFromSession(key);

        /// <summary>
        /// Set status message.
        /// </summary>
        /// <param name="model"></param>
        protected void SetStatusMessage(BaseViewModel model)
        {
            if (GetFromSession<string>("ErrorMessage") != null)
            {
                model.ErrorMessage = GetFromSession<string>("ErrorMessage");
                model.HasError = true;
                HttpContext.Current.Session.Remove("ErrorMessage");
            }
            else if (GetFromSession<string>("SuccessMessage") != null)
            {
                model.SuccessMessage = GetFromSession<string>("SuccessMessage");
                HttpContext.Current.Session.Remove("SuccessMessage");
            }
        }

        /// <summary>
        /// Replace filter key name
        /// </summary>
        /// <param name="filters">Reference of filter collection</param>
        /// <param name="keyName">Old Key Name</param>
        /// <param name="newKeyName">New key name</param>
        protected void ReplaceFilterKeyName(ref FilterCollection filters, string keyName, string newKeyName)
        {
            FilterCollection tempCollection = new FilterCollection();
            tempCollection = filters;
            FilterCollection newCollection = new FilterCollection();

            foreach (var tuple in filters)
            {
                if (Equals(tuple.Item1.ToLower(), keyName.ToLower()))
                    newCollection.Add(new FilterTuple(newKeyName, tuple.Item2, tuple.Item3));
            }
            foreach (var temp in tempCollection)
            {
                if (!Equals(temp.Item1.ToLower(), keyName.ToLower()))
                    newCollection.Add(temp);
            }
            filters = newCollection;
        }

        /// <summary>
        /// Replace sort key name
        /// </summary>
        /// <param name="sort">Sort collection</param>
        /// <param name="keyName">Old key name</param>
        /// <param name="newKeyName">New key name</param>
        protected void ReplaceSortKeyName(SortCollection sort, string keyName, string newKeyName)
        {
            if (!Equals(sort, null) && !Equals(sort.Count, 0))
            {
                if (sort["sort"].ToString().ToLower().Equals(keyName.ToLower()))
                {
                    sort["sort"] = newKeyName;
                }
            }
        }

        /// <summary>
        /// This method will log the general message in the admin log files
        /// </summary>
        /// <param name="message">string message to be logged</param>
        protected void LogMessage(string message)
        {
            //ZnodeLoggingBase.LogAdminMessage(message);
        }

        /// <summary>
        /// Get BaseViewModel with HasError and ErrorMessage set.
        /// </summary>
        /// <param name="viewModel">View model to set.</param>
        /// <param name="errorMessage">Error message to set.</param>
        /// <returns>Returns BaseViewModel with HasError and ErrorMessage set.</returns>
        protected BaseViewModel GetViewModelWithErrorMessage(BaseViewModel viewModel, string errorMessage)
        {
            viewModel.HasError = true;
            viewModel.ErrorMessage = errorMessage;
            return viewModel;
        }

        /// <summary>
        /// Set List Paging Details.
        /// </summary>
        /// <param name="listViewModel">List of View Model</param>
        /// <param name="listModel">Api List Model</param>
        protected void SetListPagingData(BaseViewModel listViewModel, BaseListModel listModel)
        {
            listViewModel.Page = Convert.ToInt32(listModel.PageIndex);
            listViewModel.RecordPerPage = Convert.ToInt32(listModel.PageSize);
            listViewModel.TotalPages = Convert.ToInt32(listModel.TotalPages);
            listViewModel.TotalResults = Convert.ToInt32(listModel.TotalResults);
        }

        //Get Mandatory Filters.
        protected FilterCollection GetRequiredFilters()
        {
            FilterCollection filters = new FilterCollection();

            filters.Add(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, Convert.ToString(GetCatalogId()));
            filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, PortalAgent.LocaleId.ToString());
            filters.Add(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, PortalAgent.CurrentPortal.PortalId.ToString());
            filters.Add(WebStoreEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue);

            SetProfileIdFilter(filters);

            return filters;
        }

        //Set Profile Id Filter.
        protected void SetProfileIdFilter(FilterCollection filters)
        {
            Dictionary<string, bool> PortalFeatureValues = PortalAgent.CurrentPortal.PortalFeatureValues;
            if (PortalFeatureValues?.Count > 0 && PortalFeatureValues.ContainsKey(HelperUtility.StoreFeature.Enable_Profile_Based_Search.ToString()) && PortalFeatureValues[HelperUtility.StoreFeature.Enable_Profile_Based_Search.ToString()])
            {
                IWebstoreHelper helper = GetService<IWebstoreHelper>();
                UserViewModel currentUser = helper.GetUserViewModelFromSession();
                //If catalog is not associated to account then profile base catalog work.
                if (HelperUtility.IsNull(currentUser?.PublishCatalogId))
                {
                    int? profileId = currentUser?.Profiles?.Count > 0 ? currentUser?.ProfileId > 0 ? currentUser.ProfileId : currentUser.Profiles?.Where(x => x.IsDefault.GetValueOrDefault())?.FirstOrDefault()?.ProfileId : PortalAgent.CurrentPortal.ProfileId;
                    filters.Add(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(profileId));
                }
            }
        }

        //Get Profile Id.
        protected static int? GetProfileId()
        {
            Dictionary<string, bool> PortalFeatureValues = PortalAgent.CurrentPortal.PortalFeatureValues;
            if (PortalFeatureValues?.Count > 0 && PortalFeatureValues.ContainsKey(HelperUtility.StoreFeature.Enable_Profile_Based_Search.ToString()) && PortalFeatureValues[HelperUtility.StoreFeature.Enable_Profile_Based_Search.ToString()])
            {
                IWebstoreHelper helper = GetService<IWebstoreHelper>();
                UserViewModel currentUser = helper.GetUserViewModelFromSession();
                //If catalog is not associated to account then profile base catalog work.
                if (HelperUtility.IsNull(currentUser?.PublishCatalogId))
                    return currentUser?.Profiles?.Count > 0 ? currentUser.ProfileId > 0 ? currentUser?.ProfileId : currentUser.Profiles?.FirstOrDefault(x => x.IsDefault.GetValueOrDefault())?.ProfileId : PortalAgent.CurrentPortal.ProfileId;
            }
            return null;
        }

        protected static int? GetCatalogId()
        {
            IWebstoreHelper helper = GetService<IWebstoreHelper>();
            UserViewModel currentUser = helper.GetUserViewModelFromSession();

            if (HelperUtility.IsNotNull(currentUser) && currentUser.AccountId > 0 && currentUser.PublishCatalogId > 0)
                return currentUser.PublishCatalogId;
            else
                return PortalAgent.CurrentPortal.PublishCatalogId;
        }

        // Removes keys from bind data model which are not Attributes of the Group.
        protected void RemoveNonAttributeKeys(BindDataModel bindDataModel)
        {
            foreach (var item in bindDataModel.ControlsData.Where(kvp => !kvp.Key.ToString().EndsWith("_attr")).ToList())
                bindDataModel.ControlsData.Remove(item.Key);
        }

        //Removes attributes from bind data model which are emplty or null value.
        protected void RemoveAttributeWithEmptyValue(BindDataModel bindDataModel)
        {
            foreach (var item in bindDataModel.ControlsData.Where(kvp => string.IsNullOrEmpty(kvp.Value.ToString())).ToList())
                bindDataModel.ControlsData.Remove(item.Key);
        }

        /// <summary>
        /// Get publish state from cookie, if cookie is null we set default 0 in cookie.
        /// </summary>
        /// <returns></returns>
        protected static ZnodePublishStatesEnum GetPublishState()
        {
             HttpCookie cookie = HelperUtility.GetHttpCookies("_WebStorePublishState");
             if (HelperUtility.IsNull(cookie) || HelperUtility.IsNull(cookie.Value))
             {
                 ZnodePublishStatesEnum Default = 0;
                 SetPublishState(Default);
                 cookie = HelperUtility.GetHttpCookies("_WebStorePublishState");
             }
            return  (ZnodePublishStatesEnum)Enum.Parse(typeof(ZnodePublishStatesEnum), cookie.Value, true);                      
        }

        /// <summary>
        /// Set publish state in cookie
        /// </summary>
        /// <param name="znodePublishStates"></param>
        protected static void SetPublishState(ZnodePublishStatesEnum znodePublishStates)
        {
            HttpCookie cookie = HelperUtility.GetHttpCookies("_WebStorePublishState");
            cookie.Value = znodePublishStates.ToString();
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Set Locale in cookie
        /// </summary>
        /// <param name="localeId"></param>
        protected static void SetStoreLocale(int localeId)
        {
             HttpCookie cookie = HelperUtility.GetHttpCookies("_WebStoreculture");
             cookie.Value = localeId.ToString();
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Get locale from cookie, if cookie is null we set store default locale in cookie.
        /// </summary>
        /// <returns></returns>
        protected static int GetStoreLocale()
        {
            HttpCookie cookie = HelperUtility.GetHttpCookies("_WebStoreculture");
            int localeId = 0;
            if (HelperUtility.IsNotNull(cookie))
            {
               Int32.TryParse(cookie.Value, out localeId);
            }            
            return localeId;
        }

        protected static string GetCurrentWebstoreDomain() => HttpContext.Current.Request.Url.Authority;

        #endregion
    }
}