﻿using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.WebStore.Core.Agents
{
    public class SiteMapAgent : BaseAgent, ISiteMapAgent
    {
        #region Private Variables
        private readonly ISiteMapClient _siteMapClient;

        #endregion

        #region Constructor
        public SiteMapAgent(ISiteMapClient siteMapClient)
        {
            _siteMapClient = GetClient<ISiteMapClient>(siteMapClient);
        }
        #endregion

        #region Public Methods.
        //Get Publish Product List.
        public virtual PublishProductListModel GetPublishProductList(int? pageIndex, int? pageSize)
        {
            FilterCollection filters = new FilterCollection();
            filters.RemoveAll(x => x.FilterName == FilterKeys.ZnodeCatalogId);
            filters.Add(FilterKeys.ZnodeCatalogId, FilterOperators.Equals,PortalAgent.CurrentPortal.PublishCatalogId.ToString());
            filters.Add(FilterKeys.PortalId, FilterOperators.Equals, PortalAgent.CurrentPortal.PortalId.ToString());
            filters.Add(FilterKeys.LocaleId, FilterOperators.Equals, PortalAgent.CurrentPortal.Locales?.FirstOrDefault(x => x.IsDefault == true)?.LocaleId.ToString());

            ExpandCollection expands = new ExpandCollection();
            return _siteMapClient.GetPublishProductList(expands, filters, null, pageIndex, pageSize);
        }

        #endregion
    }
}
