﻿using System;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Helpers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.WebStore.Agents
{
    public class PaymentAgent : BaseAgent, IPaymentAgent
    {
        #region Private Variables
        private readonly IPaymentClient _paymentClient;
        private readonly IOrderClient _orderClient;
        private readonly ICartAgent _cartAgent;
        #endregion

        #region Public Constructor
        public PaymentAgent(IPaymentClient paymentClient, IOrderClient orderClient)
        {
            _paymentClient = GetClient<IPaymentClient>(paymentClient);
            _cartAgent = new CartAgent(GetClient<ShoppingCartClient>(), GetClient<PublishProductClient>(), GetClient<AccountQuoteClient>(), GetClient<UserClient>());
            _orderClient = GetClient<IOrderClient>(orderClient);
        }
        #endregion

        #region Public Methods

        public virtual PaymentDetailsViewModel GetPaymentDetails(int paymentSettingId)
        {
            string gatwayname = string.Empty;
            FilterCollection filters = new FilterCollection();

            filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "1"));
            filters.Add(new FilterTuple(ZnodePaymentSettingEnum.PaymentSettingId.ToString(), FilterOperators.Equals, paymentSettingId.ToString()));

            PaymentSettingModel paymentSetting = _paymentClient.GetPaymentSettings(new Api.Client.Expands.ExpandCollection { ZnodePaymentSettingEnum.ZnodePaymentType.ToString() }, filters, null, null, null)?.PaymentSettings?.FirstOrDefault();
            PaymentDetailsViewModel model = new PaymentDetailsViewModel();

            if (HelperUtility.IsNotNull(paymentSetting))
            {
                model.IsPoDocUploadEnable = paymentSetting.IsPoDocUploadEnable;
                model.IsPoDocRequire = paymentSetting.IsPoDocRequire;
                model.IsBillingAddressOptional = paymentSetting.IsBillingAddressOptional;
                string totalAmount = GetOrderTotal();
                if (string.Equals(paymentSetting.PaymentTypeName, ZnodeConstant.PAYPAL_EXPRESS.ToString(), StringComparison.CurrentCultureIgnoreCase) ||
                          string.Equals(paymentSetting.PaymentTypeName, ZnodeConstant.Amazon_Pay.ToString(), StringComparison.CurrentCultureIgnoreCase))
                {
                    totalAmount = ConvertTotalToLocale(totalAmount);
                }
                else if (!string.IsNullOrEmpty(paymentSetting?.GatewayCode))
                {
                    totalAmount = Encryption.EncryptPaymentToken(ConvertTotalToLocale(totalAmount));
                }
                else
                {
                    model.HasError = string.IsNullOrEmpty(paymentSetting?.GatewayCode);
                }
                model.GatewayCode = paymentSetting.GatewayCode;
                model.PaymentCode = paymentSetting.PaymentCode;
                model.PaymentProfileId = paymentSetting.ProfileId;
                model.Total = totalAmount;
            }
            return model;
        }

        public virtual PaymentSettingViewModel GetPaymentSetting(int paymentSettingId, int portalId = 0)
        {
            try
            {
                PaymentSettingViewModel paymentSettingViewModel = null;
                PaymentSettingModel paymentSettingModel = _paymentClient.GetPaymentSetting(paymentSettingId, false, new ExpandCollection { ZnodePaymentSettingEnum.ZnodePaymentType.ToString() }, portalId);
                int? profileid = paymentSettingModel.ProfileId;
                if (paymentSettingModel.IsCallToPaymentAPI)
                {
                    string paymentDisplayName = paymentSettingModel.PaymentDisplayName;
                    paymentSettingViewModel = _paymentClient.GetPaymentSettingByPaymentCode(paymentSettingModel.PaymentCode)?.ToViewModel<PaymentSettingViewModel>();

                    if (HelperUtility.IsNotNull(paymentSettingViewModel))
                    {
                        paymentSettingViewModel.PaymentSettingId = paymentSettingId;
                        paymentSettingViewModel.ProfileId = profileid;
                        paymentSettingViewModel.PaymentTypeName = paymentSettingModel.PaymentTypeName;
                        paymentSettingViewModel.PaymentDisplayName = paymentDisplayName;
                        paymentSettingViewModel.PaymentCode = paymentSettingModel.PaymentCode;
                    }
                }
                return paymentSettingViewModel;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return null;
            }

        }

        // Call PayNow method in Payment Application
        public virtual GatewayResponseModel ProcessPayNow(SubmitPaymentModel model)
        {
            if (HelperUtility.IsNotNull(model))
                return _paymentClient.PayNow(model);
            return new GatewayResponseModel { HasError = true, };
        }

        // Call PayNow method in Payment Application
        public GatewayResponseModel ProcessPayPal(SubmitPaymentModel model)
        {
            if (HelperUtility.IsNotNull(model))
                return _paymentClient.PayPal(model);
            return new GatewayResponseModel { HasError = true, };
        }

        public string GetOrderTotal()
        {
            decimal? total = 0;
            //Get shopping Cart from Session or cookie
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                           _cartAgent.GetCartFromCookie();

            if (HelperUtility.IsNotNull(shoppingCart))
                total = shoppingCart.Total;

            string strTotal = ConvertTotalToLocale(Convert.ToString(total));
            return strTotal;
        }

        //Get saved credit card details by customer GUID
        public PaymentMethodCCDetailsListModel GetPaymentCreditCardDetails(string customersGUID)
        {
            try
            {
                return _paymentClient.GetSavedCardDetailsByCustomerGUID(customersGUID);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return new PaymentMethodCCDetailsListModel { HasError = true };
            }
        }

        //Get Count of saved credit card by customers GUID.
        public int GetSaveCreditCardCount(string customersGUID)
        {
            try
            {
                PaymentMethodCCDetailsListModel cards = _paymentClient.GetSavedCardDetailsByCustomerGUID(customersGUID);
                return cards?.PaymentMethodCCDetails?.Count ?? 0;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Customers.ToString(), TraceLevel.Error);
                return 0;
            }
        }

        //Delete saved credit card details 
        public bool DeleteSavedCreditCardDetail(string paymentGUID)
        {
            try
            {
                return _paymentClient.DeleteSavedCreditCardDetail(paymentGUID);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Customers.ToString(), TraceLevel.Error);
                return false;
            }
        }

        /// <summary>
        /// Call PayPal payment finalize method in Payment Application
        /// </summary>
        /// <param name="submitPaymentModel">Submit Payment Model</param>
        /// <returns>Gateway Response Model</returns>
        public GatewayResponseModel FinalizePayPalProcess(SubmitPaymentModel submitPaymentModel)
        {
            if (HelperUtility.IsNotNull(submitPaymentModel.PaymentToken))
                return _paymentClient.FinalizePayPalProcess(submitPaymentModel);
            return new GatewayResponseModel { HasError = true, };
        }


        #endregion

        #region AmazonPay

        //Get Amazon Pay address from amazon.
        public SubmitPaymentModel GetAmazonPayAddressDetails(SubmitPaymentModel model)
           => _paymentClient.GetAmazonPayAddressDetails(model);

        //Call AmazonPay method in payment application.
        public GatewayResponseModel ProcessAmazonPay(SubmitPaymentModel model)
        {
            if (HelperUtility.IsNotNull(model))
                return _paymentClient.AmazonPay(model);
            return new GatewayResponseModel { HasError = true, };
        }

        #endregion

        #region Private Method
        //to convert total amount to locale wise
        private string ConvertTotalToLocale(string total)
            => total.Replace(",", ".");
        #endregion
    }
}