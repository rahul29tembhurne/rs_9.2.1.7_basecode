﻿using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.ViewModels;

namespace Znode.Engine.WebStore.Agents
{
    public interface IPaymentAgent
    {

        /// <summary>
        /// Get PaymentGatway Name.
        /// </summary>
        /// <param name="paymentSettingId">Payment Setting Id/param>
        /// <returns></returns>
        PaymentDetailsViewModel GetPaymentDetails(int paymentSettingId);

        /// <summary>
        /// Get Payment Setting.
        /// </summary>
        /// <param name="paymenSettingId">Id To get Payment Setting</param>
        /// <param name="portalId">Optional portal Id</param>
        /// <returns>payment Setting View Model</returns>
        PaymentSettingViewModel GetPaymentSetting(int paymentSettingId, int portalId = 0);

        /// <summary>
        /// This method will use to call the payment and process the order
        /// </summary>
        /// <param name="submitPaymentModel">Submit Payment Model</param>
        /// <returns>GatewayResponseModel</returns>
        GatewayResponseModel ProcessPayNow(SubmitPaymentModel submitPaymentModel);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        string GetOrderTotal();

        /// <summary>
        /// This method will use to call the amazon payment checkout and process the order
        /// </summary>
        /// <param name="submitPaymentModel">Submit Payment Model</param>
        /// <returns>GatewayResponseModel</returns>
        GatewayResponseModel ProcessAmazonPay(SubmitPaymentModel submitPaymentModel);

        /// <summary>
        /// This method will use to call the Paypal Express Checkout payment and process the order
        /// </summary>
        /// <param name="submitPaymentModel">Submit Payment Model</param>
        /// <returns>GatewayResponseModel</returns>
        GatewayResponseModel ProcessPayPal(SubmitPaymentModel submitPaymentModel);

        /// <summary>
        /// Get saved credit card details by customers GUID
        /// </summary>        
        /// <param name="customersGUID">string customersGUID</param>
        /// <returns>PaymentMethodCCDetailsListModel</returns>
        PaymentMethodCCDetailsListModel GetPaymentCreditCardDetails(string customersGUID);

        /// <summary>
        /// Get saved credit card count by customers GUID  
        /// </summary>
        /// <param name="customersGUID">string customersGUID</param>
        /// <returns>Count of saved credit card.</returns>
        int GetSaveCreditCardCount(string customersGUID);

        /// <summary>
        /// Delete saved credit cards.
        /// </summary>
        /// <param name="paymentGUID">paymentGUID</param>
        /// <returns>true or false</returns>
        bool DeleteSavedCreditCardDetail(string paymentGUID);

        /// <summary>
        /// call PayPal payment finalize method in Payment Application
        /// </summary>
        /// <param name="submitPaymentModel"></param>
        /// <returns></returns>
        GatewayResponseModel FinalizePayPalProcess(SubmitPaymentModel submitPaymentModel);

        /// <summary>
        /// Get AmazonPay address details.
        /// </summary>
        /// <param name="model">SubmitPaymenyModel</param>
        /// <returns>SubmitPaymentModel</returns>
        SubmitPaymentModel GetAmazonPayAddressDetails(SubmitPaymentModel model);
    }
}
