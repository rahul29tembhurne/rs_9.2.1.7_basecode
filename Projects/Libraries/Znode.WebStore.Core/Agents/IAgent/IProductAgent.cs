﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.ViewModels;

namespace Znode.Engine.WebStore.Agents
{
    public interface IProductAgent
    {
        /// <summary>
        ///Create review for product.
        /// </summary>
        /// <param name="reviewModel">Model with customer reviw data.</param>
        /// <returns>View model with customer review data.</returns>
        ProductReviewViewModel CreateReview(ProductReviewViewModel reviewModel);

        /// <summary>
        /// Get Product details by product id.
        /// </summary>
        /// <param name="productId">Product Id.</param>
        /// <returns>Returns ProductViewModel.</returns>
        ProductViewModel GetProduct(int productID);

		/// <summary>
		/// This method only returns the brief details of a published product found in Mongo.
		/// </summary>
		/// <param name="productID"></param>
		/// <returns>Returns ProductViewModel.</returns>
		ProductViewModel GetProductBrief(int productID);

		/// <summary>
		/// This method only returns the extended details of a published product found in Mongo.
		/// </summary>
		/// <param name="productID"></param>
		/// <param name="expandKeys">Pass appropriate expand keys to get the corresponding details in response.</param>
		/// <returns>Returns ShortProductViewModel.</returns>
		ShortProductViewModel GetExtendedProductDetails(int productID, string[] expandKeys);

		/// <summary>
		/// Get product list by product sku.
		/// </summary>
		/// <param name="sku">Product SKU.</param>
		/// <returns>Returns ProductListViewModel.</returns>
		List<AutoComplete> GetProductList(string sku);

        /// <summary>
        /// get product price and inventory
        /// </summary>
        /// <param name="productSKU">product sku</param>
        /// <param name="quantity">quantity of product</param>
        /// <param name="message">inventory message.</param>
        /// <param name="parentProductSKU">parent product sku.</param>
        /// <returns>ProductViewModel</returns>
        ProductViewModel GetProductPriceAndInventory(string productSKU, string quantity, string addOnIds, string parentProductSKU = "", int parentProductId = 0);

        /// <summary>
        /// Get Product details by product id.
        /// </summary>
        /// <param name="productId">Product Id.</param>
        /// <returns>Returns ProductViewModel.</returns>
        ProductReviewViewModel GetProductForReview(int productId, string productName,decimal? rating);

        /// <summary>
        /// Get bunlde product list
        /// </summary>
        /// <param name="productId">product id.</param>
        /// <returns>list of bundle product</returns>
        List<BundleProductViewModel> GetBundleProduct(int productId);


        /// <summary>
        /// Get product highlights list.
        /// </summary>
        /// <param name="productId">publish product id.</param>
        /// <param name="highLightsCodes">Highlights codes.</param>
        /// <returns>list of highlights.</returns>
        List<HighlightsViewModel> GetProductHighlights(int productId, string highLightsCodes);

        /// <summary>
        /// Get Configurable product.
        /// </summary>
        /// <param name="productId">Product id</param>
        /// <param name="sku">Product SKU.</param>
        /// <param name="codes">Attribute Codes</param>
        /// <param name="values">Attribute values</param>
        /// <param name="selectedCode">selected Codes</param>
        /// <param name="selectedValue">selected values</param>
        /// <returns>Return product view model.</returns>

        /// <summary>
        ///Get group product list.
        /// </summary>
        /// <param name="productId">product id.</param>
        /// <returns>list of associated products.</returns>
        List<GroupProductViewModel> GetGroupProductList(int productId, bool checkInventory = true);

        /// <summary>
        /// Check Group Product Inventory
        /// </summary>
        /// <param name="productSKU">Product skus.</param>
        /// <param name="quantity">quantity selected</param>
        /// <returns>product view model</returns>
        GroupProductViewModel CheckGroupProductInventory(int mainProductId, string productSKU, string quantity);

        /// <summary>
        /// Get list of product which are available for compare
        /// </summary>
        /// <returns>true/false</returns>
        bool GetCompareProducts();

        /// <summary>
        /// Add Product to comparison list.
        /// </summary>
        /// <param name="productId">product id.</param>
        /// <param name="categoryId">category id.</param>
        /// <param name="message">Messege.</param>
        /// <param name="errorCode">Error code.</param>
        /// <returns>true ot false</returns>
        bool GlobalAddProductToCompareList(int productId, int categoryId, out string message, out int errorCode);

        /// <summary>
        /// Get list of comare produt list.
        /// </summary>
        /// <returns>list of product</returns>
        List<ProductViewModel> GetCompareProductsDetails(bool isProductDetail = false);

        /// <summary>
        /// Remove all the product from comparison
        /// </summary>
        void DeleteComparableProducts();

        /// <summary>
        /// Remove single product
        /// </summary>
        /// <param name="productId">Product id.</param>
        /// <returns>true or false</returns>
        bool RemoveProductFormSession(int productId);

       

        /// <summary>
        /// Send Compare Product Mail.
        /// </summary>
        /// <param name="productCompareModel">Model with product data.</param>
        /// <returns>True/False</returns>
        bool SendComparedProductMail(ProductCompareViewModel viewModel);

        /// <summary>
        /// Get product url by product sku.
        /// </summary>
        /// <param name="productSKU">SKUof product.</param>
        /// <param name="urlHelper">URLHelper.</param>
        /// <returns>Return product URL.</returns>
        string GetProductUrl(string productSKU, UrlHelper urlHelper);

        /// <summary>
        /// Check inventory of add on products associated to product.
        /// </summary>
        /// <param name="model">Product View Model.</param>
        /// <param name="addOnIds">add on skus.</param>
        /// <param name="quantity">selected quantity.</param>
        void CheckAddOnInvenTory(ProductViewModel model, string addOnIds, decimal quantity);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="quantity"></param>
        void CheckInventory(ProductViewModel viewModel, decimal? quantity);

        /// <summary>
        /// Send A Mail to a Friend.
        /// </summary>
        /// <param name="viewModel">viewModel</param>
        /// <returns>bool</returns>
        bool SendMailToFriend(EmailAFriendViewModel viewModel);

        /// <summary>
        /// Get message for group product.
        /// </summary>
        /// <param name="associatedProductList">associated product list.</param>
        /// <returns>price message.</returns>
        string GetGroupProductMessage(List<GroupProductViewModel> associatedProductList);

        /// <summary>
        /// Gets an highlight according to highlightId.
        /// </summary>
        /// <param name="highLightId">ID of the highlight.</param>
        /// <param name="productId">product id.</param>
        /// <param name="sku">SKU of the product.</param>
        /// <returns>Highlight of the specified ID.</returns>
        HighlightsViewModel GetHighlightInfo(int highLightId, int productId, string sku);

        /// <summary>
        /// Gets the breadcrumb of the product.
        /// </summary>
        /// <param name="categoryId">Category id of the last selected category ID.</param>
        /// <param name="checkFromSession">Bool value to check if categoryId is to be taken from session.</param>
        /// <returns>Breadcrumb string.</returns>
        string GetBreadCrumb(int categoryId, string[] productAssociatedCategoryIds, bool checkFromSession);

        /// <summary>
        /// Get string to generate see more url.
        /// </summary>
        /// <param name="breadCrumb">Breadcrum url through which see more url will be generated.</param>
        /// <returns>Returns see more string.</returns>
        string GetSeeMoreString(string breadCrumb);

        /// <summary>
        ///  Get price for products through ajax async call.
        /// </summary>
        /// <param name="products">parameters to get product price</param>
        /// <returns></returns>
        List<ProductPriceViewModel> GetProductPrice(List<ProductPriceViewModel> products);

        /// <summary>
        /// Get Configurable product.
        /// </summary>
        /// <returns>Return product view model.</returns>
        ProductViewModel GetConfigurableProduct(ParameterProductModel model);
        /// <summary>
        /// Get list of product reviews.
        /// </summary>
        /// <param name="id">Product id.</param>
        /// <returns>Returns list of reviews.</returns>
        ProductAllReviewListViewModel GetAllReviews(int id, string sortingChoice, int? pageSize, int pageNo);

        /// <summary>
        /// Gets property of auto complete item.
        /// </summary>
        /// <param name="productId">Seleted item product ID.</param>
        /// <returns>Auto complete Item.</returns>
        AutoComplete GetAutoCompleteProductProperties(int productId);
        /// <summary>
        /// Get Product SEO details
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        SEOViewModel GetSEODetails(int itemId,string seoCode);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        List<RecentViewModel> GetRecentProductList(int productId);
    }
}
