﻿using System.Web.Mvc;
using System.Web.Routing;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.WebStore
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            bool runAllManagedModules = ZnodeWebstoreSettings.RunAllManagedModules;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("bundles/{*bundle}");

            routes.MapMvcAttributeRoutes();



            routes.MapRoute("product-reviews-", "product/Allreviews/{id}", new { controller = "product", action = "AllReviews" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[\w- ]+$" });

            routes.MapRoute("product-writereviews-", "product/WriteReview/{id}", new { controller = "product", action = "WriteReview" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });

            routes.MapRoute("category-breadcrumb", "category/GetBreadCrumb/{categoryId}", new { controller = "category", action = "GetBreadCrumb" }, new { httpMethod = new HttpMethodConstraint("GET"), categoryId = @"[\w- ]+$" });

            routes.MapRoute("category-details", "category/{category}/{categoryId}", new { controller = "category", action = "Index" }, new { httpMethod = new HttpMethodConstraint("GET"), category = @"[\w- ]+$", categoryId = @"[\w- ]+$" });

            routes.MapRoute("brand-details", "Brand/{brand}/{brandId}", new { controller = "brand", action = "Index" }, new { httpMethod = new HttpMethodConstraint("GET"), brand = @"[\w- ]+$", brandId = @"[\w- ]+$" });

            routes.MapRoute("brand-detailsid", "brand/{brandId}", new { controller = "brand", action = "Index" }, new { httpMethod = new HttpMethodConstraint("GET"), brandId = @"[0-9]+$" });

            routes.MapRoute("blog-details", "blog/{blogNewsId}", new { controller = "blogNews", action = "Index" }, new { httpMethod = new HttpMethodConstraint("GET"), blogNewsId = @"[\w- ]+$" });

            routes.MapRoute("news-details", "news/{blogNewsId}", new { controller = "blogNews", action = "Index" }, new { httpMethod = new HttpMethodConstraint("GET"), blogNewsId = @"[\w- ]+$" });

            routes.MapRoute("seocategory-details", "category/{seo}/{category}", new { controller = "category", action = "Index" }, new { httpMethod = new HttpMethodConstraint("GET"), seo = @"[\w-]+$", category = @"[\w- ]+$" });

            routes.MapRoute("product-details", "product/{id}", new { controller = "product", action = "Details" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });

            //Setting route for product on edit mode. [Fetching product from cart on the basis of ParentOmsSavedCartLineItemId]
            routes.MapRoute("edit-product-details", "product/{id}/{parentOmsSavedCartLineItemId}", new { controller = "product", action = "Details" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });

            routes.MapRoute("product-details-sku", "product/{id}/{sku}", new { controller = "product", action = "Details" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$", sku = @"[\w-]+$" });

            routes.MapRoute("seoproduct-details", "product/{seo}/{product}", new { controller = "product", action = "Index" }, new { httpMethod = new HttpMethodConstraint("GET"), seo = @"[\w-]+$", product = @"[\w- ]+$" });

            routes.MapRoute("category-detailsId", "Category/{categoryId}", new { controller = "category", action = "Index" }, new { httpMethod = new HttpMethodConstraint("GET"), categoryId = @"[\w- ]+$" });

            routes.MapRoute("contentPage-pageId", "contentpage/{contentPageId}", new { controller = "ContentPage", action = "ContentPage" }, new { httpMethod = new HttpMethodConstraint("GET"), contentPageId = @"[\w- ]+$" });

            routes.MapRoute("ecert-inhandecert", "eCert/list", new { controller = "eCert", action = "AvailableECertList" }, new { httpMethod = new HttpMethodConstraint("GET"), contentPageId = @"[\w- ]+$" });

            routes.MapSEORoute(
                name: "SeoSlug",
                url: runAllManagedModules ? "{*slug}" : "{slug}",
                defaults: new { controller = "", action = "", slug = "", ElementId = "" });

            routes.MapRoute(
               name: "Default",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
           );

        }
    }
}
