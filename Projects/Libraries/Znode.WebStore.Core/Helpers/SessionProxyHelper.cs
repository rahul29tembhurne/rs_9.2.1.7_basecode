﻿using System;
using System.Diagnostics;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.WebStore
{
    public class SessionProxyHelper
    {
        // To Check whether Current login user.
        public static bool IsLoginUser() => Equals(HttpContext.Current.Session[WebStoreConstants.UserAccountKey], null) ? false : true;

        //Get user approver details.
        public static ApproverDetailsModel GetApproverDetails(int userId)
        {
            ApproverDetailsModel approverDetailsModel = new ApproverDetailsModel();
            try
            {
                if (Equals(HttpContext.Current.Session[WebStoreConstants.UserApproverKey], null))
                {
                    AccountQuoteClient client = new AccountQuoteClient();
                    ApproverDetailsModel userApproverDetailsModel = client.UserApproverDetails(userId);
                    if (!Equals(userApproverDetailsModel, null))
                    {
                        approverDetailsModel.IsApprover = userApproverDetailsModel.IsApprover;
                        approverDetailsModel.HasApprovers = userApproverDetailsModel.HasApprovers;
                    }
                    HttpContext.Current.Session[WebStoreConstants.UserApproverKey] = approverDetailsModel;
                }
                else
                    //Set value from Session Variable
                    approverDetailsModel = HttpContext.Current.Session[WebStoreConstants.UserApproverKey] as ApproverDetailsModel;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            return approverDetailsModel;
        }

        //Get billing account number.
        public static string GetBillingAccountNumber(int userId)
        {
            string billingAccountNumber = string.Empty;
            try
            {
                if (Equals(HttpContext.Current.Session[WebStoreConstants.BillingAccountNumber], null))
                {
                    AccountQuoteClient client = new AccountQuoteClient();
                    billingAccountNumber = client.GetBillingAccountNumber(userId);

                    HttpContext.Current.Session[WebStoreConstants.BillingAccountNumber] = billingAccountNumber;
                }
                else
                    //Set value from Session Variable
                    billingAccountNumber = HttpContext.Current.Session[WebStoreConstants.BillingAccountNumber] as string;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            return billingAccountNumber;
        }
    }
}