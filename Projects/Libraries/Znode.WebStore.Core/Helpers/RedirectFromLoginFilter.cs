﻿using System;
using System.Configuration;
using System.Web.Mvc;

using Znode.Engine.WebStore.Controllers;

using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.WebStore
{
    public class RedirectFromLogin : ActionFilterAttribute, IActionFilter
    {
        #region Properties
        public string HomeController
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("CustomHomePageUrl")))
                    return "Home";
                string[] controllerActionValue = ConfigurationManager.AppSettings.Get("CustomHomePageUrl").Split(',');
                return string.IsNullOrEmpty(controllerActionValue[0]) ? "Home" : controllerActionValue[0];
            }
        }
        public string HomeAction
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("CustomHomePageUrl")))
                    return "Index";
                string[] controllerActionValue = ConfigurationManager.AppSettings.Get("CustomHomePageUrl").Split(',');
                return string.IsNullOrEmpty(controllerActionValue[1]) ? "Index" : controllerActionValue[1];
            }
        }
        public string LoginPageUrl
        {
            get
            {
                return string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("CustomLoginPageUrl")) ?
                             "/User/Login" : ConfigurationManager.AppSettings.Get("CustomLoginPageUrl");
            }
        }
        #endregion

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = (BaseController)filterContext.Controller;
            if (IsRedirectFromLogin(filterContext) && IsLoginRequired())
                filterContext.Result = controller.RedirectToAction(HomeAction, HomeController);
            base.OnActionExecuting(filterContext);
        }

        private static bool IsLoginRequired()
        {
            return Convert.ToBoolean(GetService<IAuthenticationHelper>().IsAuthorizationMandatory());
        }

        public bool IsRedirectFromLogin(ActionExecutingContext filterContext)
        {
            return filterContext.HttpContext.Request.UrlReferrer?.AbsolutePath.Equals(LoginPageUrl) ?? false;
        }
    }
}
