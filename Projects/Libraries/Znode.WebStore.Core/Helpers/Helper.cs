﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.WebStore
{
    public static class Helper
    {
        #region Private Variables
        private static readonly IMessageAgent _messageAgent;
        #endregion
        #region Constructor
        static Helper()
        {
            IWebstoreHelper helper = GetService<IWebstoreHelper>();
            _messageAgent = helper.MessageAgent();
        }
        #endregion
        //Get message.
        public static string GetMessage(string key, string controller)
        => _messageAgent.GetMessage(key, controller);

        //For Price according to currency
        public static string FormatPriceWithCurrency(decimal? price, string cultureName, string UOM = "")
        {
            if (HelperUtility.IsNotNull(price))
            {
                string cultureValue;
                cultureName = string.IsNullOrEmpty(cultureName) ? PortalAgent.CurrentPortal?.CultureCode : cultureName;
                if (!string.IsNullOrEmpty(cultureName))
                {
                    CultureInfo info = new CultureInfo(cultureName);
                    info.NumberFormat.CurrencyDecimalDigits = Convert.ToInt32(DefaultSettingHelper.DefaultPriceRoundOff);
                    cultureValue = $"{price.GetValueOrDefault().ToString("c", info.NumberFormat)}";
                }
                else
                    cultureValue = Convert.ToString(price);

                return !string.IsNullOrEmpty(UOM) ? $"{cultureValue} / {UOM}" : cultureValue;
            }
            return null;
        }

        public static bool InfinityLoading
        {
            get
            {
                return (HelperUtility.IsNotNull(SessionHelper.GetDataFromSession<bool>("ViewAllMode"))) ? SessionHelper.GetDataFromSession<bool>("ViewAllMode") : Convert.ToBoolean(ConfigurationManager.AppSettings["InfinityLoading"]);
            }
        }

        public static bool IsAsyncPrice
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["IsAsyncPrice"]);

            }
        }

        //Set Cart parameters for Add To cart.
        public static void SetProductCartParameter(ProductViewModel product)
        {
            if (HelperUtility.IsNotNull(product))
            {
                string sku = !string.IsNullOrEmpty(product.ConfigurableProductSKU) ? product.SKU : product.Attributes.Value(ZnodeConstant.ProductSKU);
                product.CartParameter = new Dictionary<string, string>();
                product.ProductType = product.Attributes.SelectAttributeList(ZnodeConstant.ProductType)?.FirstOrDefault()?.Code;
                product.CartParameter.Add("ProductId", product.PublishProductId.ToString());
                product.CartParameter.Add("SKU", sku);
                product.CartParameter.Add("ProductType", product.ProductType);
                product.CartParameter.Add("Quantity", product.Attributes.Value(ZnodeConstant.MinimumQuantity));
                product.CartParameter.Add("ParentProductId", product.ParentProductId.ToString());
                product.CartParameter.Add("ConfigurableProductSKUs", product.ConfigurableProductSKU);
                product.CartParameter.Add("AddOnProductSKUs", string.Empty);
                product.CartParameter.Add("PersonalisedCodes", string.Empty);
                product.CartParameter.Add("PersonalisedValues", string.Empty);
                if (product.AddOns.Count > 0)
                    product.CartParameter.Add("AutoAddonSKUs", string.Join(",", product.AddOns.Where(x => x.IsAutoAddon)?.Select(y => y.AutoAddonSKUs)));
                switch (product.ProductType)
                {
                    case ZnodeConstant.GroupedProduct:
                        product.CartParameter.Add("GroupProductSKUs", string.Empty);
                        product.CartParameter.Add("GroupProductsQuantity", string.Empty);
                        break;
                    case ZnodeConstant.BundleProduct:
                        product.CartParameter.Add("BundleProductSKUs", string.Empty);
                        break;
                    default:
                        break;
                }
            }
        }

        //Set Cart parameters for Add To cart.
        public static void SetProductCartParameter(ShortProductViewModel product)
        {
            if (HelperUtility.IsNotNull(product))
            {
                string sku = !string.IsNullOrEmpty(product.ConfigurableProductSKU) ? product.SKU : product.Attributes.Value(ZnodeConstant.ProductSKU);
                product.CartParameter = new Dictionary<string, string>();
                product.MiscellaneousDetails.ProductType = product.Attributes.SelectAttributeList(ZnodeConstant.ProductType)?.FirstOrDefault()?.Code;
                product.CartParameter.Add("ProductId", product.PublishProductId.ToString());
                product.CartParameter.Add("SKU", sku);
                product.CartParameter.Add("ProductType", product.MiscellaneousDetails.ProductType);
                product.CartParameter.Add("Quantity", product.Attributes.Value(ZnodeConstant.MinimumQuantity));
                product.CartParameter.Add("ParentProductId", product.ParentProductId.ToString());
                product.CartParameter.Add("ConfigurableProductSKUs", product.ConfigurableProductSKU);
                product.CartParameter.Add("AddOnProductSKUs", string.Empty);
                product.CartParameter.Add("PersonalisedCodes", string.Empty);
                product.CartParameter.Add("PersonalisedValues", string.Empty);
                if (product.AddOns.Count > 0)
                    product.CartParameter.Add("AutoAddonSKUs", string.Join(",", product.AddOns.Where(x => x.IsAutoAddon)?.Select(y => y.AutoAddonSKUs)));
                switch (product.MiscellaneousDetails.ProductType)
                {
                    case ZnodeConstant.GroupedProduct:
                        product.CartParameter.Add("GroupProductSKUs", string.Empty);
                        product.CartParameter.Add("GroupProductsQuantity", string.Empty);
                        break;
                    case ZnodeConstant.BundleProduct:
                        product.CartParameter.Add("BundleProductSKUs", string.Empty);
                        break;
                    default:
                        break;
                }
            }
        }

        public static void AddIntoCache<T>(T model, string cacheKey, string CacheDurationSettingKey)
        {
            //Cache settings.
            CacheDurationSettings cacheDurationSetting = CacheDurationSettings.GetCacheDurationSettingsByMappingKey(CacheDurationSettingKey);

            HttpRuntime.Cache.Insert(cacheKey, model, null, DateTime.UtcNow.AddMinutes(Equals(cacheDurationSetting, null)
                ? 0 : Convert.ToDouble(cacheDurationSetting.Value)), Cache.NoSlidingExpiration);
        }

        public static void AddIntoCache<T>(T model, string cacheKey)
        {
            HttpRuntime.Cache.Insert(cacheKey, model);
        }
        public static T GetFromCache<T>(string cacheKey)
        {
            var model = HttpRuntime.Cache[cacheKey];
            return (T)Convert.ChangeType(model, typeof(T));
        }

        public static void ClearCache(string cacheKey)
        => HttpContext.Current.Cache.Remove(cacheKey);


        /// <summary>
        /// Sets the filter for userId property.
        /// </summary>
        /// <param name="filters">Filters to set for userId.</param>
        /// <param name="userId">Value to set for userId.</param>
        public static void SetUserIdFilters(FilterCollection filters, int userId)
        {
            if (HelperUtility.IsNotNull(filters))
            {
                //Checking for Id already Exists in Filters Or Not 
                if (filters.Exists(x => x.Item1 == ZnodeNoteEnum.UserId.ToString()))
                {
                    //If Id Already present in filters Remove It
                    filters.RemoveAll(x => x.Item1 == ZnodeNoteEnum.UserId.ToString());

                    //Add New Id Into filter.
                    filters.Add(new FilterTuple(ZnodeNoteEnum.UserId.ToString(), FilterOperators.Equals, userId.ToString()));
                }
                else
                    filters.Add(new FilterTuple(ZnodeNoteEnum.UserId.ToString(), FilterOperators.Equals, userId.ToString()));
            }
        }

        //Get User Details.
        public static UserViewModel GetUserDetails()
        {
            IWebstoreHelper helper = GetService<IWebstoreHelper>();
            UserViewModel currentUser = helper.GetUserViewModelFromSession();            
            return currentUser;
        }

        public static int? GetProfileId()
        {
            IWebstoreHelper helper = GetService<IWebstoreHelper>();
            UserViewModel currentUser = helper.GetUserViewModelFromSession();
            return currentUser?.Profiles?.Count > 0 ? currentUser?.ProfileId > 0 ? currentUser.ProfileId : currentUser.Profiles?.FirstOrDefault(x => x.IsDefault.GetValueOrDefault())?.ProfileId : HttpContext.Current.User.Identity.IsAuthenticated ? 0 : PortalAgent.CurrentPortal.ProfileId;
        }

        public static int? GetCurrentPortalId()=>
             PortalAgent.CurrentPortal.PortalId;
        

        //Round off the quantity of cart item/cartcount.
        public static string GetRoundOffQuantity(decimal quantity)
        {
            string productQuantity = quantity.ToString();
            if (!string.IsNullOrEmpty(productQuantity) && productQuantity != "0")
            {
                productQuantity = Convert.ToDecimal(productQuantity).ToInventoryRoundOff();
                if (productQuantity.Contains("."))
                {
                    decimal count = Convert.ToDecimal(productQuantity.Split('.')[1]);
                    if (!(count > 0))
                        productQuantity = productQuantity.Split('.')[0];
                }
            }
            return productQuantity;
        }

        //Round off the quantity of cart item/cartcount.
        public static string GetRoundOffQuantity(string quantity)
        {
            string productQuantity = quantity;
            if (!string.IsNullOrEmpty(productQuantity) && productQuantity != "0")
            {
                productQuantity = Convert.ToDecimal(productQuantity).ToInventoryRoundOff();
                if (productQuantity.Contains("."))
                {
                    decimal count = Convert.ToDecimal(productQuantity.Split('.')[1]);
                    if (!(count > 0))
                        productQuantity = productQuantity.Split('.')[0];
                }
            }
            return productQuantity;
        }

        //Get pixel id by pixel code.
        public static string GetPixelId(string key)
        {
            Dictionary<string, string> pixelsetting = PortalAgent.CurrentPortal.PixelSettings;
            if (pixelsetting.Count > 0)
                return pixelsetting.FirstOrDefault(x => string.Equals(x.Key, key, StringComparison.CurrentCultureIgnoreCase)).Value ?? string.Empty;

            return string.Empty;
        }

        /// <summary>
        /// Gets the folder name for current theme.
        /// </summary>
        /// <returns></returns>
        public static string GetThemeFolderPath()
        {
            return "~/Views/Themes/" + PortalAgent.CurrentPortal.Theme;
        }

        //Method is use to get theme path.
        public static string GetPortalThemePath()
           => $"/Views/Themes/{PortalAgent.CurrentPortal.Theme}";

        /// <summary>
        /// Gets the complete file path for theme file based on it's existence in child or parent theme.
        /// </summary>
        /// <param name="themeFolderRootPath"></param>
        /// <param name="relativeFilePath"></param>
        /// <returns></returns>
        public static string GetThemeFilePath(string themeFolderRootPath, string relativeFilePath)
        {
            string result = string.Empty;

            if (RelationalThemeHelper.ThemeFileExists(themeFolderRootPath, PortalAgent.CurrentPortal.Theme, relativeFilePath))
                result = themeFolderRootPath + PortalAgent.CurrentPortal.Theme + relativeFilePath;
            else if (!string.IsNullOrEmpty(PortalAgent.CurrentPortal.ParentTheme) && RelationalThemeHelper.ThemeFileExists(themeFolderRootPath, PortalAgent.CurrentPortal.ParentTheme, relativeFilePath))
                result = themeFolderRootPath + PortalAgent.CurrentPortal.ParentTheme + relativeFilePath;

            return result;
        }


        //Replace Home text with Home icon
        public static string ReplaceHomeIconInBreadcrumb(string breadcrumbHtml)
        => breadcrumbHtml?.Replace("<a href='/'>" + WebStore_Resources.LinkTextHome + "</a> /", "<a href='/' class='home-icon pr-5'></a>");

        public static bool GetCMSMode() => HttpContext.Current.Request.QueryString["cmsmode"] == "CMS_MODE" ? true : false;

    }
}