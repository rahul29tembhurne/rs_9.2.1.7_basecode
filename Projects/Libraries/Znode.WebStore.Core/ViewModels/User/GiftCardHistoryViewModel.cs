﻿using System;

namespace Znode.Engine.WebStore.ViewModels
{
    public class GiftCardHistoryViewModel : BaseViewModel
    {
        public int GiftCardHistoryId { get; set; }
        public string GiftCardNumber { get; set; }
        public int GiftCardId { get; set; }
        public int OrderId { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal TransactionAmount { get; set; }
        public string CurrencyCode { get; set; }
        public string OrderNumber { get; set; }
        public string CultureCode { get; set; }
    }
}