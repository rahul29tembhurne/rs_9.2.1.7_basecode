﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Libraries.ECommerce.Utilities;
namespace System.Web.Mvc.Html
{
    public static class HTMLExtensions
    {
        private const string ConfigButtonPartialViewPath = "~/Views/Shared/_ConfigButton.cshtml";
        private const string WidgetPlaceHolderPartialViewPath = "~/Views/Shared/_WidgetPlaceHolder.cshtml";

        private static ThemedViewEngine engine = new ThemedViewEngine();

        //Render Message by message key.
        public static MvcHtmlString RenderMessage(this HtmlHelper htmlHelper, string key)
        => MvcHtmlString.Create(HttpUtility.HtmlDecode(Helper.GetMessage(key, (string)HttpContext.Current.Request.RequestContext.RouteData.Values["controller"])));


        //Render Message by message key.
        public static MvcHtmlString RenderBlock(this HtmlHelper htmlHelper, string content)
        => MvcHtmlString.Create(HttpUtility.HtmlDecode(content));



        //Get Widgets control.
        public static MvcHtmlString WidgetPartial(this HtmlHelper htmlHelper, string widgetCode, string displayName, string widgetKey, string typeOfMapping, int mappingId = 0, Dictionary<string, object> properties = null)
        {
            ControllerContext controllerContext = htmlHelper.ViewContext.Controller.ControllerContext;
            ViewEngineResult viewResult = engine.FindPartialView(controllerContext, $"_Widget{widgetCode}", true);
            using (WidgetParameter prm = new WidgetParameter())
            {
                prm.WidgetCode = widgetCode;
                prm.WidgetKey = widgetKey;
                prm.TypeOfMapping = typeOfMapping;
                prm.CMSMappingId = mappingId;
                prm.DisplayName = displayName;
                prm.properties = properties;
                return GetWidget(htmlHelper, controllerContext, viewResult, prm);
            }
        }

        private static MvcHtmlString GetWidget(HtmlHelper htmlHelper, ControllerContext controllerContext, ViewEngineResult viewResult, WidgetParameter prm)
        {
            if (!Equals(prm, null) && prm.CMSMode
              && prm.TypeOfMapping.ToLower().Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString().ToLower()))
                return ConfigButton(htmlHelper, ConfigButtonPartialViewPath, prm).Concat(GetPartial(htmlHelper, viewResult, controllerContext, prm));
            else
                return GetPartial(htmlHelper, viewResult, controllerContext, prm);
        }

        //Get Widgets control in a non-blocking manner with Ajax. 
        public static MvcHtmlString WidgetPartialAjax(this HtmlHelper htmlHelper, string widgetCode, string displayName, string widgetKey, string typeOfMapping, int mappingId = 0, Dictionary<string, object> properties = null)
        {
            using (WidgetParameter prm = new WidgetParameter())
            {
                prm.WidgetCode = widgetCode;
                prm.WidgetKey = widgetKey;
                prm.TypeOfMapping = typeOfMapping;
                prm.CMSMappingId = mappingId;
                prm.DisplayName = displayName;
                prm.properties = properties;

                return PartialExtensions.Partial(htmlHelper, WidgetPlaceHolderPartialViewPath, prm);
            }
        }

        public static MvcHtmlString Concat(this MvcHtmlString first, MvcHtmlString htmlString) => MvcHtmlString.Create(first.ToString() + string.Concat(htmlString.ToString()));

        //Get Partial view of widgets.
        private static MvcHtmlString GetPartial(HtmlHelper htmlHelper, ViewEngineResult viewResult, ControllerContext controllerContext, object data = null)
        => BuildPartial(htmlHelper, controllerContext, viewResult, data);

        //Get Partial view of widgets.
        private static MvcHtmlString GetPartial(HtmlHelper htmlHelper, string partialViewName, object data = null)
        {
            ControllerContext controllerContext = htmlHelper.ViewContext.Controller.ControllerContext;
            ViewEngineResult viewResult = engine.FindPartialView(controllerContext, partialViewName, true);
            return BuildPartial(htmlHelper, controllerContext, viewResult, data);
        }

        //Find and build partial view for widgets render.
        private static MvcHtmlString BuildPartial(HtmlHelper htmlHelper, ControllerContext controllerContext, ViewEngineResult viewResult, object data = null)
        {
            ViewDataDictionary viewData = new ViewDataDictionary(data);
            TempDataDictionary tempData = htmlHelper.ViewContext.TempData;
            using (StringWriter stringWriter = new StringWriter())
            {
                ViewContext viewContext = new ViewContext(controllerContext, viewResult.View, viewData, tempData, stringWriter);

                viewResult.View.Render(viewContext, stringWriter);

                return MvcHtmlString.Create(stringWriter.GetStringBuilder().ToString());
            }
        }

        public static MvcHtmlString ConfigButton(HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
                => GetPartial(htmlHelper, partialViewName, widgetparameter);

        //Slider control widgets.
        public static MvcHtmlString SliderControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
            => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetSlider(widgetparameter));

        //Text control Widget.
        public static MvcHtmlString TextControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetContent(widgetparameter));

        public static MvcHtmlString SearchWidgetControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        {
            Dictionary<string, object> searchProperties = new Dictionary<string, object>();
            HttpRequest currentRequest = HttpContext.Current.Request;
            searchProperties.Add(WebStoreConstants.PageSize, currentRequest.QueryString[WebStoreConstants.PageSize]);
            searchProperties.Add(WebStoreConstants.PageNumber, currentRequest.QueryString[WebStoreConstants.PageNumber]);
            searchProperties.Add(WebStoreConstants.Sort, currentRequest.QueryString[WebStoreConstants.Sort]);
            widgetparameter.properties = searchProperties;

            return GetPartial(htmlHelper, partialViewName, WidgetHelper.GetSearchWidgetData(widgetparameter));
        }


        //Search control Widget.
        public static MvcHtmlString SearchControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, new AutoComplete());

        //Layout header control Widget.
        public static MvcHtmlString HeaderControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, htmlHelper.ViewContext.ViewData);

        //layout footer control Widget.
        public static MvcHtmlString FooterControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, htmlHelper.ViewContext.ViewData);

        //Top level cart count control Widget.
        public static MvcHtmlString CartControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, htmlHelper.ViewContext.ViewData);

        //Top level category menu control Widget.
        public static MvcHtmlString TopLevelNavigationControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, htmlHelper.ViewContext.ViewData);

        //offer banner control Widget.
        public static MvcHtmlString OfferBannerControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetSlider(widgetparameter));

        //Home page special control Widget.
        public static MvcHtmlString HomePageSpecialControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter, string type = null)
        => GetPartial(htmlHelper, partialViewName, htmlHelper.ViewContext.ViewData);

        //Footer Help section control Widget.
        public static MvcHtmlString HelpsControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetContentPages(widgetparameter));

        //Footer store info control Widget.
        public static MvcHtmlString StoreInfoControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, htmlHelper.ViewContext.ViewData);

        //footer customer support control Widget.
        public static MvcHtmlString CustomerSupportControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
       => GetPartial(htmlHelper, partialViewName, htmlHelper.ViewContext.ViewData);

        //Landing page Product grid control Widget.
        public static MvcHtmlString ProductGridControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetCategoryProducts(widgetparameter));

        //Recently viewed product control widget.
        public static MvcHtmlString RecentlyViewProductControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
       => GetPartial(htmlHelper, partialViewName, htmlHelper.ViewContext.ViewData);

        //Sub category list control Widget.
        public static MvcHtmlString SubCategoryListControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, htmlHelper.ViewContext.ViewData);

        //Facets list control Widget.
        public static MvcHtmlString FacetsListControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, htmlHelper.ViewContext.ViewData);

        //Advertisement banner control Widget.
        public static MvcHtmlString AdvertisementControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
       => GetPartial(htmlHelper, partialViewName, htmlHelper.ViewContext.ViewData);

        //Locale list control Widget.
        public static MvcHtmlString LocalesControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
       => GetPartial(htmlHelper, partialViewName, htmlHelper.ViewContext.ViewData);

        //Product List Widget Control.
        public static MvcHtmlString ProductControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
       => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetProducts(widgetparameter));

        //Link Widget Control.
        public static MvcHtmlString LinkControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetLinkWidget(widgetparameter));

        //Category List Widget Control.
        public static MvcHtmlString CategoryControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetCategories(widgetparameter));

        //Brand List Widget Control.
        public static MvcHtmlString BrandControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetBrands(widgetparameter));

        //Category grid control Widget.
        public static MvcHtmlString CategoryGridControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetSubCategories(widgetparameter));

        //Facets control for widget.
        public static MvcHtmlString FacetsControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetFacetList(widgetparameter));

        //Quick View control Widget.
        public static MvcHtmlString QuickViewControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetProductQuickView(widgetparameter));

        //Quick Order control Widget.
        public static MvcHtmlString QuickOrderControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, new AutoComplete() { Properties = widgetparameter.properties });

        //Quick Order pad control Widget.
        public static MvcHtmlString QuickOrderPadControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, new AutoComplete());

        [Obsolete("Google Tag Manager is now installed using a different approach. Obsolete since date: 15th March 2018")]
        //meta tag.
        public static MvcHtmlString TagManager(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetTagManager(widgetparameter));


        public static MvcHtmlString AddToCartLink(this HtmlHelper html, string text, string action, Dictionary<string, string> routeValues, string cssClassName, string clickElement = null, string disable = "")
        {
            if (HelperUtility.IsNotNull(routeValues))
            {
                string productId = string.Empty;
                routeValues.TryGetValue("ProductId", out productId);
                //Build a form tag.
                TagBuilder tbForm = new TagBuilder("form");
                tbForm.MergeAttribute("method", "POST");
                tbForm.MergeAttribute("action", action);
                tbForm.MergeAttribute("id", $"Form_{productId}");

                //Build hidden fields.
                List<string> inputs = new List<string>();
                if (HelperUtility.IsNotNull(routeValues))
                {
                    foreach (var key in routeValues.Keys)
                    {
                        string inputFormat = @"<input type='hidden' id='dynamic-" + key.ToLower() + "' name='{0}' value='{1}' />";

                        string input = string.Format(inputFormat, key, html.Encode(routeValues[key]));
                        inputs.Add(input);
                    }
                }

                //build a button.
                string submitBtn = $"<button type='submit' id ='button-addtocart_{productId}' onclick ='return  {clickElement}' {disable} class='{cssClassName}' value='{text}'>{text}</button>";
                inputs.Add(submitBtn);
                inputs.Add(html.AntiForgeryToken().ToString());

                string innerHtml = string.Join("\n", inputs.ToArray());
                tbForm.InnerHtml = innerHtml;

                // return self closing
                return new MvcHtmlString(tbForm.ToString());
            }
            return null;
        }

        //Add to cart ajax control.
        public static MvcHtmlString AddToCartAjaxRequest(this HtmlHelper html, string text, string action, Dictionary<string, string> routeValues, string cssClassName, string clickElement = null, string disable = "", string successElement = null)
        {
            if (HelperUtility.IsNotNull(routeValues))
            {
                string productId = string.Empty;
                routeValues.TryGetValue("ProductId", out productId);
                //Build a form tag.
                TagBuilder tbForm = new TagBuilder("form");
                tbForm.MergeAttribute("method", "POST");
                tbForm.MergeAttribute("action", action);
                tbForm.MergeAttribute("id", $"Form_{productId}");
                tbForm.MergeAttribute("data-ajax", "true");
                tbForm.MergeAttribute(" data-ajax-begin", "ZnodeBase.prototype.ShowLoader();");
                tbForm.MergeAttribute("data-ajax-success", successElement);
                tbForm.MergeAttribute("data-ajax-error", "ZnodeBase.prototype.HideLoader();");

                //Build hidden fields.
                List<string> inputs = new List<string>();
                if (HelperUtility.IsNotNull(routeValues))
                {
                    foreach (var key in routeValues.Keys)
                    {
                        string inputFormat = @"<input type='hidden' id='dynamic-" + key.ToLower() + "' name='{0}' value='{1}' />";

                        string input = string.Format(inputFormat, key, html.Encode(routeValues[key]));
                        inputs.Add(input);
                    }
                }

                //build a button.
                string submitBtn = $"<button data-test-selector='btnAddToCart' type='submit' id ='button-addtocart_{productId}' onclick ='return  {clickElement}' {disable} class='{cssClassName}' value='{text}'><i class='zf-cart'></i>{text}</button>";
                inputs.Add(submitBtn);
                inputs.Add(html.AntiForgeryToken().ToString());

                string innerHtml = string.Join("\n", inputs.ToArray());
                tbForm.InnerHtml = innerHtml;

                // return self closing
                return new MvcHtmlString(tbForm.ToString());
            }
            return null;
        }

        //Get cart item count.
        public static MvcHtmlString CartCount(this HtmlHelper html, MvcHtmlString innerHtml)
        {
            //Get cart item count.
            string cartItemCount = $"<a href='/cart'>{innerHtml}<i class='f-size zf-cart header-toolbar-cart'></i><span class='cartcount'>{Helper.GetRoundOffQuantity(WidgetHelper.GetCartCount())}</span></a>";
            return new MvcHtmlString(cartItemCount);
        }

        public static MvcHtmlString Video(this HtmlHelper htmlHelper, string url)
               => MvcHtmlString.Create(HttpUtility.HtmlDecode(MediaPaths.GetVideoTag(url)));

        public static MvcHtmlString TrackingPixelHtml(this HtmlHelper html, string key, object[] routeValues = null, string exteranalParameterstring = null)
        {
            if (HelperUtility.IsNotNull(routeValues))
            {
                string htmlstringMessageString = Helper.GetMessage(key, (string)HttpContext.Current.Request.RequestContext.RouteData.Values["controller"]);

                if (Equals(htmlstringMessageString, null)) return new MvcHtmlString(null);

                string htmlString = string.Format(htmlstringMessageString, routeValues);

                return MvcHtmlString.Create(HttpUtility.HtmlDecode(htmlString));
            }
            else if (!string.IsNullOrEmpty(key))
            {
                string htmlstringMessageString = Helper.GetMessage(key, (string)HttpContext.Current.Request.RequestContext.RouteData.Values["controller"]);
                return MvcHtmlString.Create(HttpUtility.HtmlDecode(htmlstringMessageString));
            }
            else
                return null;
        }

        //Form Widget Control.
        public static MvcHtmlString FormWidgetControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        => GetPartial(htmlHelper, partialViewName, WidgetHelper.GetFormConfiguration(widgetparameter));



        //Available total balance for ECertificate.
        public static MvcHtmlString ECertTotalBalanceControl(this HtmlHelper htmlHelper, string partialViewName, WidgetParameter widgetparameter)
        {
            var eCertTotalBalanceWidgetData = WidgetHelper.GetECertTotalBalance(widgetparameter);

            return (HttpContext.Current.User != null &&
                    HttpContext.Current.User.Identity.IsAuthenticated &&
                    HelperUtility.IsNotNull(eCertTotalBalanceWidgetData))
                   //Only authorized users can see ECert widget
                   ? GetPartial(htmlHelper, partialViewName, eCertTotalBalanceWidgetData)
                   : MvcHtmlString.Empty;
        }

        public static MvcHtmlString ZnodeCaptcha(this HtmlHelper htmlHelper, string partialViewName = "")
            => Convert.ToBoolean(PortalAgent.CurrentPortal.GlobalAttributes?.Attributes?.FirstOrDefault(x => x.AttributeCode == "IsCaptchaRequired")?.AttributeValue)
            ? GetPartial(htmlHelper, !string.IsNullOrEmpty(partialViewName) ? partialViewName : $"~/Views/Themes/{PortalAgent.CurrentPortal.Theme}/Views/Shared/_Captcha.cshtml", new object()) : null;
    }
}