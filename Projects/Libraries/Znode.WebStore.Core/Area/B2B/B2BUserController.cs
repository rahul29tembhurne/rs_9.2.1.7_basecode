﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.WebStore.Controllers
{
    public partial class UserController : BaseController
    {
        [AllowAnonymous]
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public virtual ActionResult B2BLogin(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                _userAgent.Logout();

            //Get user name from cookies.
            GetLoginRememberMeCookie();

            System.Web.HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);

            return PartialView("_Login", model);
        }

        //Login.
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult B2BLogin(LoginViewModel model, string returnUrl, bool isSinglePageCheckout = false)
        {
            if (ModelState.IsValid)
            {
                model.IsWebStoreUser = true;
                //Authenticate the User Credentials.
                LoginViewModel loginviewModel = _userAgent.Login(model);

                if (!loginviewModel.HasError)
                {
                    //Set the Authentication Cookie.           
                    _authenticationHelper.SetAuthCookie(model.Username, model.RememberMe);

                    //Remember me.
                    if (model.RememberMe)
                        SaveLoginRememberMeCookie(model.Username);

                    bool isMerged = _cartAgent.MergeCart();

                    if (!string.IsNullOrEmpty(returnUrl) && returnUrl.Contains("checkout"))
                        return RedirectToAction<CheckoutController>(x => x.Index(isSinglePageCheckout));

                    if (isMerged || string.IsNullOrEmpty(returnUrl))
                        return Json(new { status = true, error = "" }, JsonRequestBehavior.AllowGet);

                    //Encode the query string parameter for special characters.
                    if (!string.IsNullOrEmpty(returnUrl) && returnUrl.Contains("AddToWishList"))
                    {
                        returnUrl = $"{returnUrl.Remove(returnUrl.IndexOf("="))}={HttpUtility.UrlEncode(returnUrl.Substring(returnUrl.IndexOf("productSKU")).Split('=')[1])}";
                        return Redirect(returnUrl);
                    }

                    //Redirection to the Login Url.
                    _authenticationHelper.RedirectFromLoginPage(model.Username, true);
                    if (Request.IsAjaxRequest())
                    {
                        return Json(new { status = true, error = "" }, JsonRequestBehavior.AllowGet);
                    }
                    return new EmptyResult();
                }

                if (HelperUtility.IsNotNull(loginviewModel) && loginviewModel.IsResetPassword)
                {
                    TempData[WebStoreConstants.UserName] = model.Username;
                    return RedirectToAction<UserController>(x => x.B2BResetWebstorePassword());
                }

                if (Request.IsAjaxRequest() && HelperUtility.IsNotNull(loginviewModel) && loginviewModel.IsResetPassword)
                {
                    TempData[WebStoreConstants.UserName] = model.Username;
                    TempData[WebStoreConstants.CheckoutReturnUrl] = WebStoreConstants.CheckoutReturnUrl;
                    return Json(new { status = true, error = "", isresetpassword = true }, JsonRequestBehavior.AllowGet);
                }

                if (Request.IsAjaxRequest())
                {
                    return Json(new { status = false, error = loginviewModel.ErrorMessage, returnUrl = Convert.ToString(returnUrl) }, JsonRequestBehavior.AllowGet);
                }
                SetNotificationMessage(GetErrorNotificationMessage(loginviewModel.ErrorMessage));
                return View(loginviewModel);
            }
            return PartialView("_Login", model);
        }

        //Login Status.
        public virtual PartialViewResult B2BLoginStatus()
        {
            GetLoggedInUserFirstName();
            return PartialView("_LoginPartial");
        }

        #region Log Out
        //Logs off the user from the site.
        [AllowAnonymous]
        public virtual ActionResult B2BLogout()
        {
            if (User.Identity.IsAuthenticated)
            {
                ExternalLoginInfo loginInfo = SessionHelper.GetDataFromSession<ExternalLoginInfo>(WebStoreConstants.SocialLoginDetails);

                if (HelperUtility.IsNotNull(loginInfo))
                    return Redirect(_userAgent.Logout(loginInfo));
                _userAgent.Logout();
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ResetWebStorePassword
        [AllowAnonymous]
        [HttpGet]
        public virtual ActionResult B2BResetWebstorePassword()
        {
            if (HelperUtility.IsNull(TempData[WebStoreConstants.UserName]))
                return RedirectToAction<UserController>(x => x.Login(string.Empty));

            ViewBag.CheckoutLogin = TempData[WebStoreConstants.CheckoutReturnUrl];
            return PartialView(new ChangePasswordViewModel() { UserName = Convert.ToString(TempData[WebStoreConstants.UserName]) });
        }

        [AllowAnonymous]
        [HttpPost]
        public virtual ActionResult B2BResetWebstorePassword(ChangePasswordViewModel model, string returnUrl, bool isSinglePageCheckout = false)
        {
            if (ModelState.IsValid)
            {
                ChangePasswordViewModel resetPasswordModel = _userAgent.ChangePassword(model);
                if (!resetPasswordModel.HasError)
                {
                    LoginViewModel loginModel = new LoginViewModel() { Username = model.UserName, Password = model.NewPassword, };
                    LoginViewModel accountViewModel = _userAgent.Login(loginModel);

                    if (!accountViewModel.HasError)
                    {
                        Session.Add(WebStoreConstants.SuccessMessage, resetPasswordModel.SuccessMessage);
                        _authenticationHelper.SetAuthCookie(loginModel.Username, true);
                        SetNotificationMessage(GetSuccessNotificationMessage(resetPasswordModel.SuccessMessage));
                        if (!string.IsNullOrEmpty(returnUrl) && returnUrl.Contains(WebStoreConstants.CheckoutReturnUrl))
                            return RedirectToAction<CheckoutController>(x => x.Index(isSinglePageCheckout));
                        return RedirectToAction<UserController>(x => x.B2BDashboard());
                    }
                    else
                    {
                        SetNotificationMessage(GetErrorNotificationMessage(accountViewModel.ErrorMessage));
                        return PartialView(resetPasswordModel);
                    }
                }
                else
                {
                    SetNotificationMessage(GetErrorNotificationMessage(resetPasswordModel.ErrorMessage));
                    return PartialView(resetPasswordModel);
                }
            }
            return PartialView(model);
        }
        #endregion

        #region Forgot Password
        //Forgot Password.
        [AllowAnonymous]
        [HttpGet]
        public virtual ActionResult B2BForgotPassword()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction<HomeController>(x => x.Index());

            return PartialView("ForgotPassword");
        }

        //Forgot Password.
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult B2BForgotPassword(UserViewModel model)
        {
            ModelState.Remove("FirstName");
            ModelState.Remove("LastName");
            ModelState.Remove("Email");
            if (ModelState.IsValid)
            {
                model = _userAgent.ForgotPassword(model);

                if (!model.HasError)
                {
                    //tNotificationMessage(GenerateNotificationMessages(model.SuccessMessage, NotificationType.info));
                    return PartialView("_Login", new LoginViewModel() { SuccessMessage = model.SuccessMessage });
                }
                else
                    SetNotificationMessage(GetErrorNotificationMessage(model.ErrorMessage));
            }
            return PartialView("ForgotPassword", model);
        }
        #endregion

        [Authorize]
        public virtual ActionResult B2BDashboard()
        {
            if (string.IsNullOrEmpty(_userAgent.GetUserViewModelFromSession()?.RoleName))
                return RedirectToAction<HomeController>(x => x.Index());

            UserViewModel model = _userAgent.GetDashboard();

            if (HelperUtility.IsNull(model))
                return RedirectToAction<HomeController>(x => x.Index());

            if (model.AccountId > 0 && HelperUtility.IsNull(model.RoleName))
                return RedirectToAction<HomeController>(x => x.Index());

            //Sets the first name of logged in user in viewBag
            GetLoggedInUserFirstName();

            if (Request.IsAjaxRequest())
                return PartialView("_AccountMenu", model);

            return View("Dashboard", model);
        }

        //Gets first name of logged in user
        private void GetLoggedInUserFirstName()
        {
            if (User.Identity.IsAuthenticated)
            {
                string name = _userAgent.GetLoggedInUserFirstName();
                if (!string.IsNullOrEmpty(name))
                    ViewBag.UserFirstName = name;
                else
                    ViewBag.UserFirstName = "Guest";
            }
        }
    }
}
