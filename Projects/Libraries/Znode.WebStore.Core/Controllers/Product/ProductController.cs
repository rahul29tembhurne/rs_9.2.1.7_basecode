﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Models;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.WebStore.Controllers
{
    public class ProductController : BaseController
    {
        #region Private Readonly members
        private readonly IProductAgent _productAgent;
        private readonly IUserAgent _accountAgent;
        private readonly IAttributeAgent _attributeAgent;
        private readonly ICartAgent _cartAgent;
        private const string ComparableProductsView = "_ComparableProducts";
        private const string ProductComparePopup = "_ProductComparePopup";
        private const string SendMailPopUp = "_SendMailView";
        private readonly IWidgetDataAgent _widgetDataAgent;

        #endregion

        #region Public Constructor
        public ProductController(IProductAgent productAgent, IUserAgent userAgent, ICartAgent cartAgent, IWidgetDataAgent widgetDataAgent, IAttributeAgent attributeAgent)
        {
            _productAgent = productAgent;
            _accountAgent = userAgent;
            _cartAgent = cartAgent;
            _widgetDataAgent = widgetDataAgent;
            _attributeAgent = attributeAgent;
        }
        #endregion

        [HttpGet]
        public virtual ActionResult WriteReview(int Id, string name)
        => View("WriteReview", _productAgent.GetProductForReview(Id, name,null));

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult WriteReview(ProductReviewViewModel reviewModel)
        {
            if (ModelState.IsValid)
            {
                ProductReviewViewModel model = _productAgent.CreateReview(reviewModel);
                if (!model.HasError && model.CMSCustomerReviewId > 0)
                {
                    ModelState.Clear();
                    ProductReviewViewModel viewModel = _productAgent.GetProductForReview(reviewModel.PublishProductId, reviewModel.ProductName,reviewModel.Rating);
                    TempData[WebStoreConstants.Notifications] = GenerateNotificationMessages(WebStore_Resources.SuccessWriteReview, NotificationType.success);
                    return View("WriteReview", viewModel);
                }
                else
                {
                    TempData[WebStoreConstants.Notifications] = GenerateNotificationMessages(WebStore_Resources.ErrorWriteReview, NotificationType.error);
                    return View("WriteReview", _productAgent.GetProductForReview(reviewModel.PublishProductId, reviewModel.ProductName,null));
                }
            }
            TempData[WebStoreConstants.Notifications] = GenerateNotificationMessages(WebStore_Resources.ErrorWriteReview, NotificationType.error);
            return View("WriteReview", _productAgent.GetProductForReview(reviewModel.PublishProductId, reviewModel.ProductName,null));
        }



        [HttpGet]
        public virtual ActionResult Details(int id = 0, int? parentOmsSavedCartLineItemId = 0, string seo = "", bool isQuickView = false)
        {
            ViewBag.ProductId = id;
            ViewBag.Seo = seo;
            ViewBag.IsQuickView = isQuickView;

            SEOViewModel seoModel = _productAgent.GetSEODetails(id, seo);
            ViewBag.Title = string.IsNullOrEmpty(seoModel.SEOTitle) ? PortalAgent.CurrentPortal.WebsiteTitle : seoModel.SEOTitle;
            ViewBag.Keywords = seoModel?.SEOKeywords;
            ViewBag.Description = seoModel?.SEODescription;
            ViewBag.CanonicalURL = seoModel?.CanonicalURL;
            ViewBag.RobotTag = string.IsNullOrEmpty(seoModel.RobotTag) || seoModel?.RobotTag?.ToLower()== "none" ?string.Empty: seoModel?.RobotTag.Replace("_",",");

            return View("ProductDetails");
        }

		[HttpGet]
		[ChildActionOnly]
		[ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;publishState")]
		public virtual ActionResult BriefContent(int id = 0, string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
		{
			ViewBag.ProductId = id;
			ViewBag.Seo = seo;
			ViewBag.IsQuickView = isQuickView;
			ViewBag.IsLite = true;

			ProductViewModel product = _productAgent.GetProductBrief(id);
			if (IsNull(product))
				return Redirect("/404");

			product.ProductTemplateName = string.IsNullOrEmpty(product.ProductTemplateName) || Equals(product.ProductTemplateName, ZnodeConstant.ProductDefaultTemplate) ? "Details" : product.ProductTemplateName;

			string templateName = product.ProductTemplateName + "_Lite";

			return PartialView(templateName, product);
		}

		[ChildActionOnly]
        [ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;publishState")]
        public virtual ActionResult DetailsContent(int id = 0, string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
        {
			ViewBag.IsLite = false;
            ProductViewModel product = _productAgent.GetProduct(id);
            if (IsNull(product))
                return Redirect("/404");

            product.ProductTemplateName = string.IsNullOrEmpty(product.ProductTemplateName) || Equals(product.ProductTemplateName, ZnodeConstant.ProductDefaultTemplate) ? "Details" : product.ProductTemplateName;

            return PartialView(product.ProductTemplateName, product);
        }

		[HttpGet]
		[ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;expands;publishState")]
		public virtual ActionResult ExtendedContent(int id = 0, string expands = "", string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
		{
			string[] expandKeys = !string.IsNullOrEmpty(expands) ? expands.Split(',') : null;

			ShortProductViewModel product = _productAgent.GetExtendedProductDetails(id, expandKeys);
			if (IsNull(product))
				return new HttpNotFoundResult("The supplied id or one of the supplied expands is not valid.");
			
			return Json(product, "application/json", JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		[ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;expands;publishState")]
		public virtual ActionResult AlternateProductImages(int id = 0, string expands = "", string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
		{
			string[] expandKeys = !string.IsNullOrEmpty(expands) ? expands.Split(',') : null;
			ShortProductViewModel product = _productAgent.GetExtendedProductDetails(id, expandKeys);
			if (IsNull(product.ProductImage))
				return new HttpNotFoundResult("One of the supplied expands is not valid.");

			return PartialView("_AlternateImages", product);
		}

		[HttpGet]
		[ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;expands;publishState")]
		public virtual ActionResult AlternateProductImagesZoomEffect(int id = 0, string expands = "", string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
		{
			string[] expandKeys = !string.IsNullOrEmpty(expands) ? expands.Split(',') : null;
			ShortProductViewModel product = _productAgent.GetExtendedProductDetails(id, expandKeys);
			if (IsNull(product.ProductImage))
				return new HttpNotFoundResult("One of the supplied expands is not valid.");

			return PartialView("_AlternateImagesZoomEffect", product);
		}

		[HttpGet]
		[ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;expands;publishState")]
		public virtual ActionResult ProductInfo(int id = 0, string expands = "", string seo = "", bool isQuickView = false, string publishState = "PRODUCTION")
		{
			string[] expandKeys = !string.IsNullOrEmpty(expands) ? expands.Split(',') : null;
			ShortProductViewModel product = _productAgent.GetExtendedProductDetails(id, expandKeys);
			if (IsNull(product.ProductImage))
				return new HttpNotFoundResult("One of the supplied expands is not valid.");

			return PartialView("_ProductInfoLite", product);			
		}

		//Render Quick View Partial through widget.
		[HttpGet]
        [ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "id;publishState")]
        public virtual ActionResult GetProductQuickView(int id, bool isQuickView, string seo = "", string publishState = "PRODUCTION")
        {
            ProductViewModel product = new ProductViewModel();
            product.PublishProductId = id;
            product.IsQuickView = isQuickView;
            return View("_QuickViewProduct", product);
        }

        [HttpPost]
        public virtual ActionResult AddToCart(CartItemViewModel cartItem)
        {
            _cartAgent.CreateCart(cartItem);
            return RedirectToAction<CartController>(x => x.Index());
        }

        //Add product in the cart.
        [HttpPost]
        public virtual ActionResult AddToCartProduct(AddToCartViewModel cartItem, bool IsRedirectToCart = true)
        {
            AddToCartViewModel ShoppingCart = _cartAgent.AddToCartProduct(cartItem);

            if (IsRedirectToCart)
                return RedirectToAction<CartController>(x => x.Index());

            return Json(new { status = ShoppingCart.HasError, cartCount = ShoppingCart.CartCount, Product = ShoppingCart?.ShoppingCartItems?.FirstOrDefault(x => x.SKU == cartItem.SKU), ImagePath = $"{MediaPaths.MediaThumbNailPath}/{ShoppingCart?.ShoppingCartItems?.FirstOrDefault(x => x.SKU == cartItem.SKU)?.Product.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues}" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual ActionResult AddMultipleProductsToCart(List<CartItemViewModel> cartItems)
        {
            string errorMessage = _cartAgent.AddMultipleProductsToCart(cartItems);
            return Json(new
            {
                isSuccess = string.IsNullOrEmpty(errorMessage),
                message = string.IsNullOrEmpty(errorMessage) ? WebStore_Resources.MultipleCartSuccessMessage : errorMessage,
                cartCount = _cartAgent.GetCartCount(),
            }, JsonRequestBehavior.AllowGet);
        }

        //Method to get out of stock details for a product.
        public virtual JsonResult GetProductOutOfStockDetails(int productId)
        {
            //TO Do: Implement Logic for check out of stock
            ProductViewModel product = _productAgent.GetProduct(productId);
            if (!product.ShowAddToCart)
            {
                //Show Invetory Error message.
                string errorMessage = product.InventoryMessage;
                return Json(new { status = false, errorMessage = errorMessage }, JsonRequestBehavior.AllowGet);
            }
            if (IsNull(product))
                return Json(new { status = false }, JsonRequestBehavior.AllowGet);

            return Json(new { status = true }, JsonRequestBehavior.AllowGet);
        }

        //Get Product List By SKU.
        public virtual ActionResult GetProductListBySKU(string query)
        => Json(_productAgent.GetProductList(query), JsonRequestBehavior.AllowGet);

        //Get Selected product item properties.
        public virtual ActionResult GetAutoCompleteItemProperties(int productId)
        => Json(_productAgent.GetAutoCompleteProductProperties(productId), JsonRequestBehavior.AllowGet);

        //Get the view of Quick Order pad.
        public virtual ActionResult QuickOrderPad()
        {
            SessionHelper.SaveDataInSession<int>("AutoIndex", Convert.ToInt32(WebStoreConstants.DefaultQuickOrderPadRows));
            return View("_QuickOrderPadView");
        }

        //Add to wishlist method.
        [HttpGet]
        public virtual ActionResult AddToWishList(string productSKU)
        {
            if (!Request.IsAuthenticated)
            {
                //if (!Request.IsAjaxRequest())
                SetNotificationMessage(GetSuccessNotificationMessage(WebStore_Resources.ErrorLoginWishlist));
                return Json(new { status = false, isRedirectToLogin = true, link = $"/User/Login?returnurl={HttpUtility.UrlEncode($"/Product/AddToWishList?productSKU={productSKU}")}", message = WebStore_Resources.ErrorLoginWishlist }, JsonRequestBehavior.AllowGet);
            }
            bool flag = _accountAgent.CreateWishList(productSKU);
            if (Request.IsAjaxRequest())
                return Json(new { status = flag, message = flag ? WebStore_Resources.SuccessProductAddWishlist : WebStore_Resources.ErrorProductAddWishlist, style = flag ? "success-msg" : "error-msg", link = "/User/Wishlist", wishListId = GetWishListIdForProduct(productSKU) }, JsonRequestBehavior.AllowGet);

            SetNotificationMessage(flag ? GetSuccessNotificationMessage(WebStore_Resources.SuccessProductAddWishlist) : GetErrorNotificationMessage(WebStore_Resources.ErrorProductAddWishlist));

            return Redirect(_productAgent.GetProductUrl(productSKU, Url));
        }

        //Get product price.
        [HttpGet]
        public virtual ActionResult GetProductPrice(string productSKU = "", string parentProductSKU = "", string quantity = "", string addOnIds = "", int parentProductId = 0)
        {
            ProductViewModel viewModel = _productAgent.GetProductPriceAndInventory(productSKU, quantity, addOnIds, parentProductSKU, parentProductId);

            return Json(new
            {
                success = viewModel.ShowAddToCart,
                message = viewModel.InventoryMessage,
                Quantity = viewModel.Quantity,
                data = new
                {
                    style = viewModel.ShowAddToCart ? "success" : "error",
                    price = Helper.FormatPriceWithCurrency(viewModel.ProductPrice, viewModel.CultureCode),
                    sku = viewModel?.SKU,
                    productId = parentProductId > 0 ? parentProductId : viewModel.PublishProductId,
                    addOnMessage = viewModel?.AddOns?.Count > 0 ? viewModel.AddOns?.Select(x => x.InventoryMessage)?.FirstOrDefault() : null,
                    isOutOfStock = viewModel?.AddOns?.Count > 0 ? viewModel.AddOns?.Select(x => x.IsOutOfStock)?.FirstOrDefault() : null,
                }
            }, JsonRequestBehavior.AllowGet);
        }

        //Get associated bundle products.
        [HttpGet]
        public virtual ActionResult GetBundleProduct(int productId) =>
             View("_BundleProducts", _productAgent.GetBundleProduct(productId));

        //Get Product highlights.
        [ChildActionOnly]
        public virtual ActionResult GetProductHighlights(int productId, string highLightsCodes = "", string sku = "")
        {
            List<HighlightsViewModel> viewModel = _productAgent.GetProductHighlights(productId, highLightsCodes);

            //Assign product id to highlight list.
            viewModel.ForEach(x => x.PublishProductId = productId);
            //Assign SKU to highlight list.
            viewModel.ForEach(x => x.SKU = sku);
            return View("_ProductHighLights", viewModel);
        }

        //Get highlight data.
        public virtual ActionResult GetHighlightInfo(int highLightId, int productId, string sku="")
        {
            HighlightsViewModel highlight = _productAgent.GetHighlightInfo(highLightId, productId, sku);
            highlight = IsNotNull(highlight) ? highlight : new HighlightsViewModel();
            highlight.PublishProductId = productId;
            return View("HighlightInfo", highlight);
        }

        [HttpPost]
        //Get configurable data.
        public virtual ActionResult GetConfigurableProduct(ParameterProductModel model)
        {
            ProductViewModel product = _productAgent.GetConfigurableProduct(model);
            product.IsQuickView = model.IsQuickView;
            product.IsProductEdit = !Equals(ViewBag.IsProductEdit, null) ? ViewBag.IsProductEdit : false;
            product.ProductTemplateName = string.IsNullOrEmpty(product.ProductTemplateName) || Equals(product.ProductTemplateName, ZnodeConstant.ProductDefaultTemplate) ? "Details" : product.ProductTemplateName;
            if (model.IsQuickView)
                return ActionView("_QuickViewProductView", product);

            return ActionView(product.ProductTemplateName, product);
        }

        //Get Associated products to product.
        public virtual ActionResult GetGroupProductList(int productId, bool isCallForPricing)
        {
            List<GroupProductViewModel> associatedProductList = _productAgent.GetGroupProductList(productId);
            //Price message for group product.
            ViewBag.GroupProductMessage = _productAgent.GetGroupProductMessage(associatedProductList);
            ViewBag.IsMainProductCallForPricing = isCallForPricing;
            return ActionView("_GroupProductList", associatedProductList);
        }

        //Check Group Product Inventory.
        public virtual ActionResult CheckGroupProductInventory(int mainProductId, string productSKU = "", string quantity = "")
            => Json(_productAgent.CheckGroupProductInventory(mainProductId, productSKU, quantity), JsonRequestBehavior.AllowGet);

        #region Product Compare
        //Add product to comparison
        [HttpGet]
        public virtual ActionResult GlobalLevelCompareProduct(int productId = 0, int categoryId = 0)
        {
            string message = string.Empty;
            int errorCode;
            List<ProductViewModel> products = new List<ProductViewModel>();
            bool status = _productAgent.GlobalAddProductToCompareList(productId, categoryId, out message, out errorCode);
            if (status)
                products = _productAgent.GetCompareProductsDetails();

            return Json(new
            {
                success = status,
                count = products.Count,
                data = new
                {
                    html = IsNull(products) ? string.Empty : RenderRazorViewToString(ComparableProductsView, products),
                    popuphtml = RenderRazorViewToString(ProductComparePopup,
                    new ProductComparePopUpViewModel { Message = message, ErrorCode = errorCode, ProductId = productId, CategoryId = categoryId, HideViewComparisonButton = products.Count == 1 }),
                }
            }, JsonRequestBehavior.AllowGet);
        }

        //Check Product no of product available for comparison.
        [HttpGet]
        public virtual JsonResult ViewProductComparison()
        {
            bool status = _productAgent.GetCompareProducts();
            string message = string.Empty;
            if (status)
                message = WebStore_Resources.ProductCompareQauntityErrorMessage;

            return Json(new
            {
                success = status,
                message = message,
                data = new
                {
                    popuphtml = RenderRazorViewToString(ProductComparePopup, new ProductComparePopUpViewModel { Message = message }),
                }
            }, JsonRequestBehavior.AllowGet);
        }

        //Get product comparison details.
        public virtual ActionResult ViewComparison()
        {
            ProductCompareViewModel compareProducts = new ProductCompareViewModel();
            compareProducts.ProductList = _productAgent.GetCompareProductsDetails(true);
            if (compareProducts.ProductList.Count <= 0)
                return RedirectToAction("Index", "Home");

            return View("ViewComparison", compareProducts);
        }

        //Remove All the products from comparison
        [HttpGet]
        public virtual ActionResult DeleteComparableProducts()
        {
            _productAgent.DeleteComparableProducts();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public virtual ActionResult RemoveProductFormSession(int productId, string control)
        {
            bool status = false;
            string message = string.Empty;
            ProductCompareViewModel compareProducts = new ProductCompareViewModel();
            if (productId > 0)
                status = _productAgent.RemoveProductFormSession(productId);

            if (Equals(control, "Product"))
            {
                compareProducts.ProductList = _productAgent.GetCompareProductsDetails();
                if (IsNull(compareProducts.ProductList) || compareProducts.ProductList.Count <= 1)
                    message = WebStore_Resources.ProductCompareErrorMessage;
                return Json(new
                {
                    success = status,
                    message = message,
                    count = compareProducts.ProductList.Count,
                    data = new
                    {
                        html = IsNull(compareProducts.ProductList) ? string.Empty : RenderRazorViewToString("_ProductComparisonList", compareProducts.ProductList),
                    }
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                compareProducts.ProductList = _productAgent.GetCompareProductsDetails();
                return Json(new
                {
                    success = status,
                    count = compareProducts.ProductList.Count,
                    data = new
                    {
                        html = IsNull(compareProducts.ProductList) ? string.Empty : RenderRazorViewToString("_ComparableProducts", compareProducts.ProductList),
                    }
                }, JsonRequestBehavior.AllowGet);
            }

        }

        //Get Compare Product List
        [HttpGet]
        public virtual ActionResult GetCompareProductList()
        {
            List<ProductViewModel> products = _productAgent.GetCompareProductsDetails();
            return Json(new
            {
                count = products.Count,
                data = new
                {
                    html = IsNull(products) ? string.Empty : RenderRazorViewToString(ComparableProductsView, products),
                }
            }, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult SendComparedProductMail()
        {
            ViewBag.IsCompare = true;
            return PartialView(SendMailPopUp, new ProductCompareViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult SendComparedProductMail(ProductCompareViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                bool isMailSend = _productAgent.SendComparedProductMail(viewModel);
                return Json(new
                {
                    Type = isMailSend ? "success" : "error",
                    Message = isMailSend ? WebStore_Resources.SendMailMessage : WebStore_Resources.ErrorEmailSend
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                Type = false ? "success" : "error",
                Message = false ? WebStore_Resources.SendMailMessage : WebStore_Resources.ErrorEmailSend
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //Get List Of Recently View Product.
        [HttpGet]
        public virtual ActionResult GetRecentViewProducts(int productId = 0)
        {

            List<RecentViewModel> productList = _productAgent.GetRecentProductList(productId);
            ViewBag.ProductCount = 3;
            return ActionView("_RecentlyViewProduct", productList);
        }
       
        //Get link products.
        public virtual ActionResult GetLinkProducts(int productId)
        {
            List<LinkProductViewModel> model = _widgetDataAgent.GetLinkProductList(new WidgetParameter { CMSMappingId = productId });
            return PartialView("_LinkProductList", model);
        }

        //Generate new row for quick order pad.
        public virtual ActionResult QuickOrder()
        {
            int index = Convert.ToInt32(SessionHelper.GetDataFromSession<int>("AutoIndex")) + 1;
            SessionHelper.SaveDataInSession<int>("AutoIndex", index);
            return PartialView("_MultipleQuickOrders");
        }

        //Get attribute validations list.
        public virtual ActionResult GetPersonalisedAttributes(int productId = 0, Dictionary<string, string> PersonliseValues = null)
        => ActionView("_PersonalisedAttribute", _attributeAgent.GetAttributeValidationByCodes(productId, PersonliseValues));

        [HttpGet]
        public virtual ActionResult AllReviews(int id)
        {
            string sortingChoice = Request.QueryString["Sort"];
            int pageSize = 0;
            int pageNo = 0;
            Int32.TryParse(Request.QueryString["PageSize"], out pageSize);
            Int32.TryParse(Request.QueryString[WebStoreConstants.PageNumber], out pageNo);
            ProductAllReviewListViewModel reviewModelList = _productAgent.GetAllReviews(id, sortingChoice, pageSize, pageNo);
            return View("AllReviews", reviewModelList);
        }
        //Email To Friend
        public virtual ActionResult EmailToFriend() => View("_EmailToFriend", new EmailAFriendViewModel());

        //
        public virtual void SendMailToFriend(EmailAFriendViewModel emailAFriendViewModel)
        {
            emailAFriendViewModel.ProductUrl = Request.UrlReferrer.OriginalString;
            _productAgent.SendMailToFriend(emailAFriendViewModel);
        }

        //Gets the breadcrumb.
        public virtual ActionResult GetBreadCrumb(int categoryId, string productAssociatedCategoryIds, bool checkFromSession)
        {
            productAssociatedCategoryIds = string.IsNullOrEmpty(productAssociatedCategoryIds) ? null : productAssociatedCategoryIds;
            string breadCrumb = _productAgent.GetBreadCrumb(categoryId, productAssociatedCategoryIds?.Split(','), checkFromSession);
            string seeMore = _productAgent.GetSeeMoreString(breadCrumb);
            return Json(new
            {
                breadCrumb = breadCrumb ?? string.Empty,
                seeMore = seeMore ?? string.Empty,
            }, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult GetProductPrice(object products)
        {
            var _data = JsonConvert.DeserializeObject<List<ProductPriceViewModel>>(((string[])products)[0]);

            List<ProductPriceViewModel> result = _productAgent.GetProductPrice(_data);

            return Json(new { status = true, data = result });
        }

        public virtual JsonResult IsAsyncPrice() => Json(new { status = Helper.IsAsyncPrice });

        #region B2B Theme
        //returns the wishlistId of the recently added product to wishlist for logged in user 
        public virtual int GetWishListIdForProduct(string productSKU)
        {
            UserViewModel userModel = SessionHelper.GetDataFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

            if (IsNull(userModel)) return 0;

            return userModel?.WishList?.FirstOrDefault(x => x.SKU == productSKU)?.UserWishListId ?? 0;
        }      
        #endregion

    }
}
