﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.UI;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.WebStore.Controllers
{
    public class CategoryController : BaseController
    {
        #region Private Readonly members
        private readonly ICategoryAgent _categoryAgent;
        private readonly IWidgetDataAgent _widgetDataAgent;
        #endregion

        #region Public Constructor
        public CategoryController(ICategoryAgent categoryAgent, IWidgetDataAgent widgetDataAgent)
        {
            _categoryAgent = categoryAgent;
            _widgetDataAgent = widgetDataAgent;
        }
        #endregion
        //TO Do:Have To cache on Profile Basis.
        //Get Category list for header.
        // [DonutOutputCache(CacheProfile = "PortalCacheProfile")]
        [ChildActionOnly]
        [ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "profileId;portalId")]
        public virtual ActionResult TopLevelList(int? profileId,int portalId)
            => PartialView("_HeaderNav", _categoryAgent.GetCategories());

        public virtual JsonResult SiteMapList(int? pageSize, int? pageLength)
        {
            return Json(new { Result = _categoryAgent.GetSiteMapCategories(pageSize, pageLength) }, JsonRequestBehavior.AllowGet);
        }
        
        //Get Category list for header.
        public virtual ActionResult Index(string category, int categoryId = 0, bool? viewAll = false, bool fromSearch = false, string facetGroup="", string seodata="", string sort = "")
        {
            SEOUrlViewModel sEOUrlViewModel = JsonConvert.DeserializeObject<SEOUrlViewModel>(seodata);

            ViewBag.category = category;
            ViewBag.categoryId = categoryId;
            ViewBag.viewAll = viewAll;
            ViewBag.fromSearch = fromSearch;
            ViewBag.facetGroup = facetGroup;
            ViewBag.sortList = sort;

            TempData["Title"] = sEOUrlViewModel?.SEOTitle;
            TempData["Keywords"] = sEOUrlViewModel?.SEOKeywords;
            TempData["Description"] = sEOUrlViewModel?.SEODescription;
            TempData["CanonicalURL"] = sEOUrlViewModel?.CanonicalURL;
            TempData["RobotTag"] =  string.IsNullOrEmpty(sEOUrlViewModel?.RobotTag) || sEOUrlViewModel?.RobotTag.ToLower() == "none" ? string.Empty : sEOUrlViewModel?.RobotTag?.Replace("_", ",");

            return View("ProductList");
        }

        [ChildActionOnly]
        [ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "categoryId;publishState;viewAll;facetGroup;pageSize;pageNumber;sort")]
        public ActionResult CategoryContent(string category, int categoryId = 0, bool? viewAll = false, bool fromSearch = false, string publishState = "PRODUCTION", bool bindBreadcrumb = true,string facetGroup="", string sort = "")
        {
            if (viewAll.Value)
                SessionHelper.SaveDataInSession<bool>(WebStoreConstants.ViewAllMode, true);
            else
                SessionHelper.RemoveDataFromSession(WebStoreConstants.ViewAllMode);

            CategoryViewModel ViewModel = _categoryAgent.GetCategorySeoDetails(categoryId, bindBreadcrumb);

            if (bindBreadcrumb)
                _categoryAgent.GetBreadCrumb(ViewModel);

            if (!fromSearch)
                //Remove Facet from Session.
                _categoryAgent.RemoveFromSession(ZnodeConstant.FacetsSearchFields);

            if (HelperUtility.IsNotNull(ViewModel.SEODetails))
            {
                ViewBag.Title = ViewModel.SEODetails.SEOTitle;
                TempData["Title"] = ViewModel.SEODetails.SEOTitle;
                TempData["Keywords"] = ViewModel.SEODetails.SEOKeywords;
                TempData["Description"] = ViewModel.SEODetails.SEODescription;
                TempData["CanonicalURL"] = ViewModel.SEODetails.CanonicalURL;
                TempData["RobotTag"] =  string.IsNullOrEmpty(ViewModel.SEODetails.RobotTag) || ViewModel.SEODetails.RobotTag.ToLower() == "none" ? string.Empty : ViewModel.SEODetails.RobotTag?.Replace("_", ",");
            }
           
            Dictionary<string, object> searchProperties = GetSearchProperties();
            ViewModel.ProductListViewModel = _widgetDataAgent.GetCategoryProducts(new WidgetParameter { CMSMappingId = categoryId, LocaleId = PortalAgent.LocaleId, properties = searchProperties }
           );

            return PartialView("_ProductListContent", ViewModel);
        }

        public Dictionary<string, object> GetSearchProperties()
        {
            string pageSize = !string.IsNullOrEmpty(Convert.ToString(Request.QueryString[WebStoreConstants.PageSize])) ? 
                            Request.QueryString[WebStoreConstants.PageSize] : !string.IsNullOrEmpty(Convert.ToString(SessionHelper.GetDataFromSession<int?>(WebStoreConstants.PageSizeValue)))
                             ? Convert.ToString(SessionHelper.GetDataFromSession<int?>(WebStoreConstants.PageSizeValue)) : null;
            if(string.IsNullOrEmpty(pageSize))
                pageSize= ZnodeWebstoreSettings.DefaultPageSizeForPLP;

            Dictionary<string, object> searchProperties = new Dictionary<string, object>();

            searchProperties.Add(WebStoreConstants.PageSize, pageSize);
            searchProperties.Add(WebStoreConstants.PageNumber, Request.QueryString[WebStoreConstants.PageNumber]);
            searchProperties.Add(WebStoreConstants.Sort, Request.QueryString[WebStoreConstants.Sort]);
            return searchProperties;
        }

        public virtual ActionResult GetBreadCrumb(int categoryId)
        {
            string breadCrumb = _categoryAgent.GetBreadCrumb(categoryId);
            return Json(new
            {
                breadCrumb = breadCrumb ?? string.Empty
            }, JsonRequestBehavior.AllowGet);
        }
    }
}