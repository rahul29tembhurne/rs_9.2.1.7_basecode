﻿using System.Web.Mvc;

namespace Znode.Engine.WebStore.Controllers
{
    public class ErrorPageController : Controller
    {
        // GET: ErrorPage
        public virtual ActionResult Index()
        {
            HttpContext.Response.AddHeader("Location", "/Error");
            return new HttpStatusCodeResult(307);
        }
      
        public virtual ActionResult PageNotFound()
        {
            HttpContext.Response.AddHeader("Location", "/404");
            return new HttpStatusCodeResult(307);
        }

        [AllowAnonymous]
        public virtual ActionResult UnAuthorizedErrorRequest()=>
            View("UnAuthorizedRequest");
    }
}