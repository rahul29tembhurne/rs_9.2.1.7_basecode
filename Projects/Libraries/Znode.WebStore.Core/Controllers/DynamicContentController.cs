﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Controllers;

namespace Znode.WebStore.Core.Controllers
{
    public class DynamicContentController : BaseController
    {
        #region Public Constructor
        public DynamicContentController()
        {
        }
        #endregion

        //Get publised blog news data.
        [HttpGet]
        public virtual ActionResult Widget(string widgetCode, string displayName, string widgetKey, string typeOfMapping, int mappingId = 0, string properties = null)
        {
            using (WidgetParameter prm = new WidgetParameter())
            {
                prm.WidgetCode = widgetCode;
                prm.WidgetKey = widgetKey;
                prm.TypeOfMapping = typeOfMapping;
                prm.CMSMappingId = mappingId;
                prm.DisplayName = displayName;
                prm.properties = string.IsNullOrEmpty(properties) ? null : JsonConvert.DeserializeObject<Dictionary<string, object>>(properties);

                return PartialView("~/Views/Shared/_WidgetAjax.cshtml", prm);
            }
        }
    }
}
