﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Api.Controllers
{
    public class GiftCardController : BaseController
    {
        #region Private Variables

        private readonly IGiftCardCache _cache;
        private readonly IGiftCardService _service;

        #endregion

        #region Constructor
        public GiftCardController(IGiftCardService service)
        {
            _service = service;
            _cache = new GiftCardCache(_service);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get list of GiftCard.
        /// </summary>
        /// <returns>Returns list of GiftCard.</returns>
        [ResponseType(typeof(GiftCardListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetGiftCardList(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<GiftCardListResponse>(data) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new GiftCardListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }

            return response;
        }

        /// <summary>
        /// Get random Gift Card  number.
        /// </summary>
        /// <returns>Returns CardNumber.</returns>
        [ResponseType(typeof(StringResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetRandomGiftCardNumber()
        {
            HttpResponseMessage response;

            try
            {
                string cardNumber = _service.GetRandomCardNumber();
                response = IsNotNull(cardNumber) ? CreateOKResponse(new StringResponse { Response = cardNumber }) : CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new StringResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Create new GiftCard.
        /// </summary>
        /// <param name="giftCardModel">GiftCardModel model.</param>
        /// <returns>Returns created model.</returns>
        [ResponseType(typeof(GiftCardResponse))]
        [HttpPost, ValidateModel]
        public virtual HttpResponseMessage Create([FromBody] GiftCardModel giftCardModel)
        {
            HttpResponseMessage response;

            try
            {
                GiftCardModel giftCard = _service.CreateGiftCard(giftCardModel);

                if (IsNotNull(giftCard))
                {
                    response = CreateCreatedResponse(new GiftCardResponse { GiftCard = giftCard });
                    response.Headers.Add("Location", GetUriLocation(Convert.ToString(giftCard.GiftCardId)));
                }
                else
                    response = CreateInternalServerErrorResponse();
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new GiftCardResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new GiftCardResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Get GiftCard by GiftCard id.
        /// </summary>
        /// <param name="giftCardId">GiftCard id to get GiftCard details.</param>
        /// <returns>Returns GiftCard model.</returns>
        [ResponseType(typeof(GiftCardResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetGiftCard(int giftCardId)
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetGiftCard(giftCardId, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<GiftCardResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new GiftCardResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new GiftCardResponse { HasError = true, ErrorMessage = ex.Message});
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Update GiftCard details.
        /// </summary>
        /// <param name="giftCardModel">model to update.</param>
        /// <returns>Returns updated model.</returns>
        [ResponseType(typeof(GiftCardResponse))]
        [HttpPut]
        public virtual HttpResponseMessage Update([FromBody] GiftCardModel giftCardModel)
        {
            HttpResponseMessage response;
            try
            {
                //Update GiftCard.
                response = _service.UpdateGiftCard(giftCardModel) ? CreateCreatedResponse(new GiftCardResponse { GiftCard = giftCardModel, ErrorCode = 0 }) : CreateInternalServerErrorResponse();
                response.Headers.Add("Location", GetUriLocation(Convert.ToString(giftCardModel.GiftCardId)));
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new GiftCardResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new GiftCardResponse { HasError = true, ErrorMessage = ex.Message});
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return response;
        }


        /// <summary>
        /// Delete GiftCard.
        /// </summary>
        /// <param name="giftCardId">GiftCard Id.</param>
        /// <returns>Returns true if deleted sucessfully else return false.</returns>
        [ResponseType(typeof(TrueFalseResponse))]
        [HttpPost, ValidateModel]
        public virtual HttpResponseMessage Delete(ParameterModel giftCardId)
        {
            HttpResponseMessage response;

            try
            {
                response = CreateOKResponse(new TrueFalseResponse { IsSuccess = _service.DeleteGiftCard(giftCardId) });
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new GiftCardResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new GiftCardResponse { HasError = true, ErrorMessage = ex.Message});
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return response;
        }

        /// <summary>
        /// Get list of GiftCard history for a user.
        /// </summary>
        /// <returns>Returns list of GiftCardHistory.</returns>
        [ResponseType(typeof(GiftCardHistoryListResponse))]
        [HttpGet]
        public virtual HttpResponseMessage GetGiftCardHistoryList()
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetGiftCardHistoryList(RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(data) ? CreateOKResponse<GiftCardHistoryListResponse>(data) : CreateNoContentResponse();
            }
            catch (ZnodeException ex)
            {
                response = CreateInternalServerErrorResponse(new GiftCardHistoryListResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = ex.ErrorCode });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse(new GiftCardHistoryListResponse { HasError = true, ErrorMessage = ex.Message });
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }

            return response;
        }
        #endregion
    }
}
