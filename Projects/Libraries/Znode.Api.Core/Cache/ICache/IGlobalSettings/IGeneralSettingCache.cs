﻿namespace Znode.Engine.Api.Cache
{
    public interface IGeneralSettingCache
    {
        /// <summary>
        /// Get a list of all currencies.
        /// </summary>
        /// <param name="routeUri">URI to route</param>
        /// <param name="routeTemplate">Template of route</param>
        /// <returns>Response in string format</returns>
        string List(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets cache management data
        /// </summary>
        /// <param name="routeUri">URI to route</param>
        /// <param name="routeTemplate">Template of route</param>
        /// <returns>Response in string format</returns>
        string GetCacheManagementData(string routeUri, string routeTemplate);
    }
}