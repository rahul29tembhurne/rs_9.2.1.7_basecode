﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Cache
{
    public interface IUserCache
    {
        /// <summary>
        /// Get User account data.
        /// </summary>
        /// <param name="userId">user Id to get account details.</param>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route Template</param>
        /// <param name="portalId">portalId.</param>
        /// <returns>Returns account.</returns>
        string GetUser(int userId, string routeUri, string routeTemplate, int portalId = 0);

        /// <summary>
        /// This method will get the account details by user name
        /// </summary>
        /// <param name="username">string User Name</param>
        /// <param name="portalId">Portal id.</param>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the account details</returns>
        string GetUserByUsername(string username, int portalId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get user account list.
        /// </summary>
        /// <param name="loggedUserAccountId">loggedUserAccountId</param>
        /// <param name="routeUri">route uri.</param>
        /// <param name="routeTemplate">route template.</param>
        /// <returns>Returns user account list.</returns>
        string GetUserList(int loggedUserAccountId, string routeUri, string routeTemplate);


        /// <summary>
        /// Gets the assigned portals to user.
        /// </summary>
        /// <param name="aspNetUserId">User id.</param>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route template</param>
        /// <returns>Returns assigned portals to user.</returns>
        string GetPortalIds(string aspNetUserId, string routeUri, string routeTemplate);

        /// <summary>
        /// This method will useful for the login purpose.
        /// </summary>
        /// <param name="portalId">int Portal Id</param>
        /// <param name="model">UserModel model</param>
        /// <param name="errorCode">out error code</param>
        /// <returns></returns>
        UserModel Login(int portalId, UserModel model, out int? errorCode);
    }
}
