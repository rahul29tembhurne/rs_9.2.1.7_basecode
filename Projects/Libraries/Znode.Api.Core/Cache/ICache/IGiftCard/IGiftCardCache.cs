﻿namespace Znode.Engine.Api.Cache
{
    public interface IGiftCardCache
    {
        /// <summary>
        /// Get GiftCard list.
        /// </summary>
        /// <param name="routeUri">route uri.</param>
        /// <param name="routeTemplate">route template.</param>
        /// <returns>Returns GiftCard list.</returns>
        string GetGiftCardList(string routeUri, string routeTemplate);

        /// <summary>
        /// Get GiftCard on the basis of giftCardId.
        /// </summary>
        /// <param name="giftCardId">GiftCard id.</param>
        /// <param name="routeUri">route uri.</param>
        /// <param name="routeTemplate">route template.</param>
        /// <returns>Returns GiftCard.</returns>
        string GetGiftCard(int giftCardId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get list of Gift Card history for a user.
        /// </summary>
        /// <param name="routeUri">Route Uri.</param>
        /// <param name="routeTemplate">Route Template.</param>
        /// <returns>Returns gift card hisotry list for a user.</returns>
        string GetGiftCardHistoryList(string routeUri, string routeTemplate);
    }
}