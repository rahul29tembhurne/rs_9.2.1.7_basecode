﻿using Znode.Engine.Promotions;
using Znode.Engine.Shipping;
using Znode.Engine.Taxes;

namespace Znode.Engine.Api
{
    public static class ApiHelper 
    {
        // Caches all available promotion types in the application cache.
        public static void CacheAvailablePromotionTypes() => ZnodePromotionManager.CacheAvailablePromotionTypes();

        // Caches all available shipping types in the application cache.
        public static void CacheAvailableShippingTypes() => ZnodeShippingManager.CacheAvailableShippingTypes();

        // Caches all available tax types in the application cache.
        public static void CacheAvailableTaxTypes() => ZnodeTaxManager.CacheAvailableTaxTypes();

        //Caches all active promotions in the application cache.
        public static void CacheActivePromotions() => ZnodePromotionManager.CacheActivePromotions();
    }
}
