﻿using MongoDB.Driver;
using System;
using System.Configuration;
using System.Web;
using ZNode.Libraries.MongoDB.Data.Constants;
namespace Znode.Libraries.MongoDB.Data
{
    public class MongoConnectionHandler<T> : IDisposable
    {
        public MongoCollection<T> MongoCollection { get; private set; }

        private bool isDisposed;

        public MongoConnectionHandler()
        {
            //Get mongo Connection string.
            string connectionString = ConfigurationManager.ConnectionStrings[MongoSettings.DBKey].ConnectionString;

            MongoConnectionString(connectionString);
        }
        public MongoConnectionHandler(bool IsLog)
        {
            IsLog = true;
            //Get mongo Connection string.
            string connectionString = ConfigurationManager.ConnectionStrings[MongoSettings.DBLogKey]?.ConnectionString;

            //If existing connection string is not present then use the ZnodeMongoDB connection string
            if (string.IsNullOrEmpty(connectionString))
            {
                connectionString = ConfigurationManager.ConnectionStrings[MongoSettings.DBKey]?.ConnectionString;
                IsLog = false;
            }

            MongoConnectionString(connectionString, IsLog);
        }

        ~MongoConnectionHandler()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
            => isDisposed = true;

        private void MongoConnectionString(string connectionString, bool IsLogDB = false)
        {
            if (!string.IsNullOrEmpty(connectionString))
            {
                MongoDatabase db = null;
                if (HttpContext.Current != null)
                {
                    string objectContextKey = IsLogDB ? "mongologdb_" : "mongo_" + HttpContext.Current.GetHashCode().ToString("x");
                    if (!HttpContext.Current.Items.Contains(objectContextKey))
                    {
                        db = GetMongoContext(connectionString);
                        HttpContext.Current.Items.Add(objectContextKey, db);
                    }
                    else
                        db = HttpContext.Current.Items[objectContextKey] as MongoDatabase;

                }
                else
                    db = GetMongoContext(connectionString);

                // Get a reference to the collection object from the Mongo database object
                MongoCollection = db.GetCollection<T>(typeof(T).Name.ToLower());

            }
        }

        //Create the Context object, return the context.
        private static MongoDatabase GetMongoContext(string connectionString)
        {
            MongoClient mongoClient = MongoDBInstance.GetMongoClient(connectionString);
            MongoServer mongoServer = mongoClient.GetServer();

            // Get a reference to the "znode" database object 
            // from the Mongo server object
            string databaseName = MongoUrl.Create(connectionString).DatabaseName;
            return mongoServer.GetDatabase(databaseName);
        }
    }

    public static class MongoDBInstance
    {
        //volatile: ensure that assignment to the instance variable
        //is completed before the instance variable can be accessed
        private static volatile MongoClient mongoClient = null;
        private static readonly object syncLock = new Object();

        public static MongoClient GetMongoClient(string connectionString)
        {
            if (Equals(mongoClient, null))
            {
                lock (syncLock)
                {
                    if (Equals(mongoClient, null))
                        mongoClient = new MongoClient(connectionString);
                }
            }
            return mongoClient;
        }
    }
}
