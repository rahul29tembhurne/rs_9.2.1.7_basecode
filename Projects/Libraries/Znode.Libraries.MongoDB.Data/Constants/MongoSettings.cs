﻿namespace ZNode.Libraries.MongoDB.Data.Constants
{
    public static class MongoSettings
    {
        public const string DBKey = "ZnodeMongoDB";
        public const string DBLogKey = "ZnodeMongoDBForLog";
    }
}
