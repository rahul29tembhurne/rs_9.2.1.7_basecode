﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace Znode.Libraries.MongoDB.Data
{
    public interface IMongoRepository<T>  
    {

        /// <summary>
        /// Used to Query the Entity
        /// </summary>
        MongoConnectionHandler<T> Table { get; }

        /// <summary>
        /// Create/Insert document in mongo collection 
        /// If Mongo DB and collection is exists it will add document in existing db and collection.
        /// If DB and Collection is not exists then it will create first and add document in to it.
        /// </summary>
        /// <param name="entity">Mongo DB Entity (Document)</param>
        void Create(T entity);

        /// <summary>
        /// Create/Insert multiple documents in mongo collection 
        /// If Mongo DB and collection is exists it will add document in existing db and collection.
        /// If DB and Collection is not exists then it will create first and add document in to it.
        /// </summary>
        /// <param name="entities">List of entities</param>
        void Create(IEnumerable<T> entities);

        /// <summary>
        /// Delete document from collection by mongo collection _id 
        /// </summary>
        /// <param name="id">mongo collection _id</param>
        /// <returns>returns status after delete</returns>
        bool Delete(string id);

        /// <summary>
        /// Delete documents matching the Query
        /// </summary>
        /// <param name="query">Query to delete documents</param>
        /// <returns>returns status after delete</returns>
        bool DeleteByQuery(IMongoQuery query, bool ignoreEmbeddedVersionIdCheck = false);

        /// <summary>
        /// Get record by mongo objectId
        /// </summary>
        /// <param name="id">mongo objectId</param>
        /// <returns>Return Entity /Document</returns>
        T GetById(string id);

        /// <summary>
        /// Get entity by matching the mongo query.
        /// </summary>
        /// <param name="query">Mongo Query.</param>
        /// <returns>Returns entity.</returns>
        T GetEntity(IMongoQuery query, bool ignoreEmbeddedVersionIdCheck = false);

        /// <summary>
        ///  Get entity list by matching the mongo query.
        /// </summary>
        /// <param name="query">Mongo Query.</param>
        /// <returns>Returns entity list.</returns>
        List<T> GetEntityList(IMongoQuery query, bool ignoreEmbeddedVersionIdCheck = false);

        /// <summary>
        ///  Get entity list by matching the mongo query.
        /// </summary>
        /// <param name="query">Mongo Query.</param>
        /// <param name="sort">Mongo Sort.</param>
        /// <returns>Returns entity list.</returns>
        List<T> GetEntityList(IMongoQuery query, IMongoSortBy sort, bool ignoreEmbeddedVersionIdCheck = false);

        /// <summary>
        /// Update entity if entity already exist in mongo db
        /// </summary>
        /// <param name="query">Query to specify entity to update</param>
        /// <param name="entity">Entity to update</param>
        void UpdateEntity(IMongoQuery query, T entity, bool ignoreEmbeddedVersionIdCheck = false);

        /// <summary>
        /// Returns sorted paged list of mongo entities as per filter passed
        /// </summary>
        /// <param name="query">mongo Query</param>
        /// <param name="orderBy">Mongo Orderby Clause</param>
        /// <param name="pageIndex">pageIndex (number of records to skip)</param>
        /// <param name="pageSize">page size</param>
        /// <param name="totalCount">out total records</param>
        /// <returns>List of entities</returns>
        List<T> GetPagedList(IMongoQuery query, IMongoSortBy orderBy, int pageIndex, int pageSize, out int totalCount, bool ignoreEmbeddedVersionIdCheck = false);
    }
}
