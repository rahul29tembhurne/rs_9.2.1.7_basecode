﻿using System;

namespace Znode.Libraries.MongoDB.Data.DataModel
{
    public class _LogWidgetBrandEntity : WidgetBrandEntity, IDisposable
    {
        ~_LogWidgetBrandEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
