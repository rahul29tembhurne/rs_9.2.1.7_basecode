﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class MessageEntity : MongoEntity, IDisposable
    {
        public int LocaleId { get; set; }
        public int? PortalId { get; set; }
        public string MessageKey { get; set; }
        public string Message { get; set; }
        public string Area { get; set; }

        [BsonIgnore]
        public int CMSMessageId { get; set; }

        ~MessageEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
