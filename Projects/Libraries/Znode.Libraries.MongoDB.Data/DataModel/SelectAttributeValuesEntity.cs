﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Libraries.MongoDB.Data
{
    public class SelectAttributeValuesEntity : MongoEntity
    {
        public int? displayorder { get; set; }
        public string value { get; set; }
    }
}
