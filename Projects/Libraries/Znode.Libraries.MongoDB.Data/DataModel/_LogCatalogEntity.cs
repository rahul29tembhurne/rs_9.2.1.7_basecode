﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogCatalogEntity : CatalogEntity, IDisposable
    {
        ~_LogCatalogEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
