﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogMessageEntity : MessageEntity, IDisposable
    {
        ~_LogMessageEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
