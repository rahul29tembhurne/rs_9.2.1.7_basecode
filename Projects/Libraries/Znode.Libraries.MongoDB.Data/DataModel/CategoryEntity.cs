﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Znode.Libraries.MongoDB.Data
{
    [BsonIgnoreExtraElements]
    public class CategoryEntity : MongoEntity,IDisposable
    {
        public int ZnodeCategoryId { get; set; }
        public int ZnodeCatalogId { get; set; }
        public int[] ZnodeParentCategoryIds { get; set; }
        public int[] ProductIds { get; set; }
        public int LocaleId { get; set; }
        public string Name { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsActive { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ActivationDate { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? ExpirationDate { get; set; }
        public int[] ProfileIds { get; set; }
        public List<AttributeEntity> Attributes { get; set; }
        public string CatalogName { get; set; }
        [BsonIgnore]
        public string TempZnodeParentCategoryIds { get; set; }
        [BsonIgnore]
        public string TempProductIds { get; set; }
        [BsonIgnore]
        public string TempProfileIds { get; set; }
        public string CategoryCode { get; set; }
        public int CategoryIndex { get; set; }

        public CategoryEntity()
        {
            Attributes = new List<AttributeEntity>();
        }

        ~CategoryEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
