﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogTextWidgetEntity : TextWidgetEntity, IDisposable
    {
        ~_LogTextWidgetEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
