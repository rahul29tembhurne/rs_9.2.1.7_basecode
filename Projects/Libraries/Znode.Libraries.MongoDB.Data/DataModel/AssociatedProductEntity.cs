﻿namespace Znode.Libraries.MongoDB.Data
{
    public class AssociatedProductEntity : MongoEntity
    {
        public int ZnodeProductId { get; set; }
        public int ZnodeCatalogId { get; set; }
        public int AssociatedZnodeProductId { get; set; }
        public int AssociatedProductDisplayOrder { get; set; }
    }
}
