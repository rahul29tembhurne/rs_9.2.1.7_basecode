﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogWidgetCategoryEntity : WidgetCategoryEntity, IDisposable
    {
        ~_LogWidgetCategoryEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
