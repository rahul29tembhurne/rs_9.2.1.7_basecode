﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class _LogCategoryEntity : CategoryEntity, IDisposable
    {
        ~_LogCategoryEntity()
        {
            if (!isDisposed)
                Dispose();
        }
    }
}
