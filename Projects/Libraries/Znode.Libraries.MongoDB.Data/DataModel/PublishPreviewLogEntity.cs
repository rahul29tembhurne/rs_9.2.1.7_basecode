﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class PublishPreviewLogEntity : MongoEntity, IDisposable
    {
        public string SourcePublishState { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string LogMessage { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? LogCreatedDate { get; set; }
        public int? PreviousVersionId { get; set; }
        public int LocaleId { get; set; }
        public string LocaleDisplayValue { get; set; }
        ~PublishPreviewLogEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {
            isDisposed = true;
        }
    }
}
