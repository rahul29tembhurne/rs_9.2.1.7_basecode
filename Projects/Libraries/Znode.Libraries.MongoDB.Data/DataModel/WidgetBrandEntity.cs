﻿using System;

namespace Znode.Libraries.MongoDB.Data.DataModel
{
    public class WidgetBrandEntity : MongoEntity,IDisposable
    {
        public int WidgetBrandId { get; set; }
        public int BrandId { get; set; }
        public int MappingId { get; set; }
        public int PortalId { get; set; }
        public string WidgetsKey { get; set; }
        public string TypeOFMapping { get; set; }
        public int? DisplayOrder { get; set; }

        ~WidgetBrandEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
