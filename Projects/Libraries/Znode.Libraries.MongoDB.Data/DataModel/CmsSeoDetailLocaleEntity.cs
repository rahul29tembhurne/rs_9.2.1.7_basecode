﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class CmsSeoDetailLocaleEntity : MongoEntity, IDisposable
    {

        public int CMSSEODetailLocaleId { get; set; }
        public int CMSSEODetailId { get; set; }
        public Nullable<int> LocaleId { get; set; }
        public string SEOTitle { get; set; }
        public string SEODescription { get; set; }
        public string SEOKeywords { get; set; }

        public int PortalId { get; set; }

        ~CmsSeoDetailLocaleEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {
            isDisposed = true;
        }
    }
}
