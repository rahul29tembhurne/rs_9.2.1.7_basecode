﻿using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Znode.Libraries.MongoDB.Data.DataModel
{
    [BsonIgnoreExtraElements]
    public class GlobalAttributeEntity : MongoEntity
    {
        public string AttributeCode { get; set; }
        public string AttributeName { get; set; }
        public string AttributeValues { get; set; }
        public string AttributeTypeName { get; set; }     
        public bool IsHtmlTags { get; set; }      
        public int DisplayOrder { get; set; }
        public List<SelectValuesEntity> SelectValues { get; set; }
    }
}
