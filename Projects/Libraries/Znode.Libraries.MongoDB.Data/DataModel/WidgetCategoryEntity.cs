﻿using System;

namespace Znode.Libraries.MongoDB.Data
{
    public class WidgetCategoryEntity : MongoEntity,IDisposable
    {
        public int WidgetCategoryId { get; set; }
        public int ZnodeCategoryId { get; set; }
        public int MappingId { get; set; }
        public int PortalId { get; set; }
        public string WidgetsKey { get; set; }
        public string TypeOFMapping { get; set; }
        public int? DisplayOrder { get; set; }
        public string CategoryCode { get; set; }
        ~WidgetCategoryEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
