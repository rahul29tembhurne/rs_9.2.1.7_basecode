﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Znode.Libraries.MongoDB.Data
{
    [BsonIgnoreExtraElements]
    public class CatalogEntity : MongoEntity, IDisposable
    {
        public int ZnodeCatalogId { get; set; }
        public int[] ProfileIds { get; set; }
        public string CatalogName { get; set; }
        public string RevisionType { get; set; }
        public int LocaleId { get; set; }
        public _LogCatalogEntity LogCatalog { get; set; }

        [BsonIgnore]
        public string TempProfileIds { get; set; }

        ~CatalogEntity()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }
    }
}
