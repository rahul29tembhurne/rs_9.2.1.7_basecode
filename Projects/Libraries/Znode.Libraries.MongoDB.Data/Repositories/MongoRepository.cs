﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;

namespace Znode.Libraries.MongoDB.Data
{
    public class MongoRepository<T> : IMongoRepository<T>, IDisposable where T : IMongoEntity, IDisposable, new()
    {
        private readonly MongoConnectionHandler<T> MongoConnectionHandler;
        private readonly MongoEntity LogEntity;
        private readonly int? versionId;
        private bool isDisposed;

        public MongoRepository()
        {
            MongoConnectionHandler = new MongoConnectionHandler<T>();
        }

        public MongoRepository(int? versionId)
        {
            this.versionId = versionId;
            MongoConnectionHandler = new MongoConnectionHandler<T>();
        }

        public MongoRepository(bool IsLog)
        {
            MongoConnectionHandler = new MongoConnectionHandler<T>(IsLog);
        }

        public MongoRepository(MongoEntity entity)
        {
            MongoConnectionHandler = new MongoConnectionHandler<T>();
            LogEntity = entity;
        }

        ~MongoRepository()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }

        public virtual MongoConnectionHandler<T> Table
        {
            get
            {
                return MongoConnectionHandler;
            }
        }

        public virtual void Create(T entity)
        {
            try
            {
                // Save the entity with safe mode (WriteConcern.Acknowledged)
                var result = this.MongoConnectionHandler.MongoCollection.Save(
                    entity,
                    new MongoInsertOptions
                    {
                        WriteConcern = WriteConcern.Acknowledged
                    });

                if (result.HasLastErrorMessage)
                    throw new Exception(result.LastErrorMessage);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty);
                throw new ZnodeException(ErrorCodes.MongoAuthentication, "Mongo Connection Failed. Please Check your mongo crendentials or your server may be down.");
            }
        }

        public virtual void Create(IEnumerable<T> entities)
        {
            try
            {
                // Save the entities 
                this.MongoConnectionHandler.MongoCollection.InsertBatch(entities);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty);
                throw new ZnodeException(ErrorCodes.MongoAuthentication, "Mongo Connection Failed. Please Check your mongo crendentials or your server may be down.");
            }

        }
        public virtual bool Delete(string id)
        {
            try
            {
                var result = this.MongoConnectionHandler.MongoCollection.Remove(
                  Query<T>.EQ(e => e.Id,
                  new ObjectId(id)),
                  RemoveFlags.None,
                  WriteConcern.Acknowledged);

                if (result.HasLastErrorMessage)
                    throw new Exception(result.LastErrorMessage);

                return result.DocumentsAffected > 0;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty);
                throw new ZnodeException(ErrorCodes.MongoAuthentication, "Mongo Connection Failed. Please Check your mongo crendentials or your server may be down.");
            }
        }

        public virtual bool DeleteByQuery(IMongoQuery query, bool ignoreEmbeddedVersionIdCheck = false)
        {
            if (!ignoreEmbeddedVersionIdCheck)
                query = WrapWithVersionIdCheck(query);
            var result = this.MongoConnectionHandler.MongoCollection.Remove(query, WriteConcern.Acknowledged);

            if (result.HasLastErrorMessage)
                throw new Exception(result.LastErrorMessage);

            return result.DocumentsAffected > 0;
        }

        public virtual T GetById(string mongoId)
        {
            try
            {
                return this.MongoConnectionHandler.MongoCollection.FindOne(Query<T>.EQ(e => e.Id, new ObjectId(mongoId)));
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty);
                throw new ZnodeException(ErrorCodes.MongoAuthentication, "Mongo Connection Failed. Please Check your mongo crendentials or your server may be down.");
            }
        }

        public virtual T GetEntity(IMongoQuery query, bool ignoreEmbeddedVersionIdCheck = false)
        {
            try
            {
                if (!ignoreEmbeddedVersionIdCheck)
                    query = WrapWithVersionIdCheck(query);
                return this.MongoConnectionHandler.MongoCollection.FindOne(query);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty);
                throw new ZnodeException(ErrorCodes.MongoAuthentication, "Mongo Connection Failed. Please Check your mongo crendentials or your server may be down.");
            }
        }

        public virtual List<T> GetEntityList(IMongoQuery query, bool ignoreEmbeddedVersionIdCheck = false)
        {
            try
            {
                if (!ignoreEmbeddedVersionIdCheck)
                    query = WrapWithVersionIdCheck(query);
                return this.MongoConnectionHandler.MongoCollection.Find(query)?.ToList();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty);
                throw new ZnodeException(ErrorCodes.MongoAuthentication, "Mongo Connection Failed.Please Check your mongo crendential or server may down");   
            }
        }

        public virtual void UpdateEntity(IMongoQuery query, T entity, bool ignoreEmbeddedVersionIdCheck = false)
        {
            try
            {
                //Get entity from Mongo DB
                if (!ignoreEmbeddedVersionIdCheck)
                    query = WrapWithVersionIdCheck(query);
                var mongoEntity = this.MongoConnectionHandler.MongoCollection.FindOne(query);
                if (!Equals(mongoEntity, null))
                {
                    entity.Id = mongoEntity.Id;

                    //Update entity if entity exixt in mongo db
                    // Update the entity with safe mode (WriteConcern.Acknowledged)
                    var result = this.MongoConnectionHandler.MongoCollection.Update(query, Update.Replace(entity), WriteConcern.Acknowledged);

                    if (result.HasLastErrorMessage)
                        throw new Exception(result.LastErrorMessage);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty);
                throw new ZnodeException(ErrorCodes.MongoAuthentication, "Mongo Connection Failed. Please Check your mongo crendentials or your server may be down.");
            }
        }

        public virtual List<T> GetEntityList(IMongoQuery query, IMongoSortBy orderBy, bool ignoreEmbeddedVersionIdCheck = false)
        {
            try
            {
                if (!ignoreEmbeddedVersionIdCheck)
                    query = WrapWithVersionIdCheck(query);
                if (!Equals(orderBy, null))
                    return this.MongoConnectionHandler.MongoCollection.Find(query).SetSortOrder(orderBy).ToList();
                else
                    return this.MongoConnectionHandler.MongoCollection.Find(query).ToList();
            }

            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty);
                throw new ZnodeException(ErrorCodes.MongoAuthentication, "Mongo Connection Failed. Please Check your mongo crendentials or your server may be down.");
            }
        }

        public virtual List<T> GetPagedList(IMongoQuery query, IMongoSortBy orderBy, int pageIndex, int pageSize, out int totalCount, bool ignoreEmbeddedVersionIdCheck = false)
        {
            try
            {
                if (!ignoreEmbeddedVersionIdCheck)
                    query = WrapWithVersionIdCheck(query);
                long longTotalCount = this.MongoConnectionHandler.MongoCollection.Count(query);
                Int32.TryParse(Convert.ToString(longTotalCount), out totalCount);

                if (!Equals(orderBy, null))
                    return this.MongoConnectionHandler.MongoCollection.Find(query).SetSortOrder(orderBy).SetSkip((pageIndex - 1) * pageSize).SetLimit(pageSize).ToList();
                else
                    return this.MongoConnectionHandler.MongoCollection.Find(query).SetSkip((pageIndex - 1) * pageSize).SetLimit(pageSize).ToList();
            }

            catch (Exception ex)
            {
                totalCount = 0;
                ZnodeLogging.LogMessage(ex, string.Empty);
                throw new ZnodeException(ErrorCodes.MongoAuthentication, "Mongo Connection Failed. Please Check your mongo crendentials or your server may be down.");
            }
        }

        private IMongoQuery WrapWithVersionIdCheck(IMongoQuery query)
        {
            return this.versionId.HasValue
                ? Query.And(query, Query<MongoEntity>.EQ(x => x.VersionId, this.versionId.Value))
                : query;
        }
    }
}
