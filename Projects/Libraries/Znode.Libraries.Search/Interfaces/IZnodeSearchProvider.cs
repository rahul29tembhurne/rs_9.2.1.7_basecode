﻿using Znode.Engine.Api.Models;

namespace Znode.Libraries.Search
{
    public interface IZnodeSearchProvider
    {
        IZnodeSearchResponse SuggestTermsFor(IZnodeSearchRequest request);

        IZnodeSearchResponse FullTextSearch(IZnodeSearchRequest request);

        /// <summary>
        /// Method to check if elastic search is working
        /// </summary>
        /// <returns></returns>
        string CheckElasticSearch();
    }
}
