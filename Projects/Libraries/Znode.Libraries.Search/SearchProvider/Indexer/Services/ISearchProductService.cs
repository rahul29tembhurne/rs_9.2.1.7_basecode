﻿using System.Collections.Generic;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Libraries.Search
{
    public interface ISearchProductService
    {
        /// <summary>
        /// Get all products for creating index.
        /// </summary>
        /// <param name="catalogId">Catalog Id</param>
        /// <param name="versionId">catalog version Id</param>
        /// <param name="start">Start index of the product list.</param>
        /// <param name="pageLength">Page length of the product list.</param>
        /// <param name="indexstartTime">current create index start time</param>
        /// <param name="totalCount">Total count of the product.</param>
        /// <returns>All the products according to catalog Id.</returns>
        List<SearchProduct> GetAllProducts(int catalogId, int versionId, IEnumerable<int> publishCategoryIds, int start, int pageLength,long indexStartTime, out decimal totalPages);

        /// <summary>
        /// Gets a product by product ID.
        /// </summary>
        /// <param name="productId">Product Id of the product.</param>
        /// <returns>Returns the elastic product.</returns>
        SearchProduct GetProductByProductId(int productId);

        /// <summary>
        /// Get CatalogVersion id
        /// </summary>
        /// <param name="catalogId">catalog id.</param>
        /// <returns>catalog version id.</returns>
        int? GetCatalogVersionId(int catalogId);

        /// <summary>
        /// Get the synonyms data.
        /// </summary>
        /// <param name="catalogId">Catalog id.</param>
        /// <returns>Returns synonyms data.</returns>
        List<ZnodeSearchSynonym> GetSynonymsData(int catalogId);

        /// <summary>
        /// Get keywords data.
        /// </summary>
        /// <param name="catalogId">Catalog id.</param>
        /// <returns>Returns keywords data.</returns>
        List<ZnodeSearchKeywordsRedirect> GetKeywordsData(int catalogId);

        /// <summary>
        /// Converts the mongo products to elastic products.
        /// </summary>
        /// <param name="mongoProducts">Mongo Products.</param>
        /// <returns>Returns elastic products.</returns>
        List<SearchProduct> GetElasticProducts(List<ProductEntity> mongoProducts, long indexStartTime);

        /// <summary>
        /// Get Catalog Version Id.
        /// </summary>
        /// <param name="publishCatalogId">publish catalog id.</param>
        /// <param name="revisionType"></param>
        /// <param name="localeId"></param>
        /// <returns></returns>
        int GetVersionId(int publishCatalogId, string revisionType, int localeId = 0);

        /// <summary>
        /// Get Catalog Latest Version Id.
        /// </summary>
        /// <param name="publishCatalogId">publish catalog id.</param>
        /// <param name="revisionType"></param>
        /// <param name="localeId"></param>
        /// <returns></returns>
        int GetLatestVersionId(int publishCatalogId, string revisionType, int localeId = 0);


    }
}