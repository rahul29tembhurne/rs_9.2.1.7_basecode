﻿using System.Linq;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Libraries.Search
{
    public static class MongoDBToElasticMapper
    {
        public static SearchCategory ElasticCategory(CategoryEntity category)
        {

            if (ECommerce.Utilities.HelperUtility.IsNull(category) || category.Attributes?.Where(x => x.AttributeCode == "IsActive")?.Select(x => x.AttributeValues)?.FirstOrDefault() == "false") return null;

            return new SearchCategory
            {
                categoryid = category.ZnodeCategoryId,
                categoryname = category.Name,
                profileids = category.ProfileIds
            };
        }

        public static SearchProduct ElasticProduct(ProductEntity product)
        {
            return new SearchProduct
            {
                mongoid = product.Id.ToString(),
                znodeproductid = product.ZnodeProductId,
                name = product.Name,
                rawname = product.Name.ToLower(),
                sku = product.SKU,
                rawsku = product.SKU.ToLower(),
                catalogid = product.ZnodeCatalogId,
                categoryid = product.ZnodeCategoryIds,
                categoryname = product.CategoryName,
                attributes = product.Attributes.Select(x => ElasticAttributes(x)).ToList(),
                localeid = product.LocaleId,
                profileids = product.ProfileIds,
                isactive = product.IsActive,
                productindex = product.ProductIndex,
                indexid = product.IndexId,
                version = product.VersionId
            };
        }

        public static SearchAttributes ElasticAttributes(AttributeEntity attribute)
        {
            return new SearchAttributes
            {
                attributecode = attribute.AttributeCode,
                attributename = attribute.AttributeName,
                attributetypename = attribute.AttributeTypeName,
                iscomparable = attribute.IsComparable,
                ishtmltags = attribute.IsHtmlTags,
                isuseinsearch = attribute.IsUseInSearch,
                ispromorulecondition = attribute.IsPromoRuleCondition,
                isfacets = attribute.IsFacets,
                ispersonalizable = attribute.IsPersonalizable,
                displayorder = attribute.DisplayOrder,
                selectvalues = attribute.SelectValues.Select(x => ElasticSelectValues(x)).ToList()
            };
        }

        //Mapping select values for attributes.
        public static ElasticSelectValues ElasticSelectValues(SelectValuesEntity selectValues)
        {
            return new ElasticSelectValues
            {
                code = selectValues.Code,
                displayorder = selectValues.DisplayOrder.GetValueOrDefault(),
                value = selectValues.Value
            };
        }

    }
}
