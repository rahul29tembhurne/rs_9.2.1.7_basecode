﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Libraries.ECommerce.ShoppingCart
{
    public interface IZnodeCheckout
    {
        string AdditionalInstructions { get; set; }
        bool IsSuccess { get; set; }
        string PaymentResponseText { get; set; }
        string PoDocument { get; set; }
        int PortalID { get; set; }
        string PurchaseOrderNumber { get; set; }
        int ShippingID { get; set; }
        ZnodePortalCart ShoppingCart { get; set; }
        UserAddressModel UserAccount { get; set; }

        void AddAddOnsItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeShoppingCartItem shoppingCartItem);
        void AddBundleItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeShoppingCartItem shoppingCartItem);
        void AddConfigurableItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeShoppingCartItem shoppingCartItem);
        void AddGroupItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeShoppingCartItem shoppingCartItem);
        void AddLineItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeProductBaseEntity product, ZnodeShoppingCartItem shoppingCartItem, ZnodeCartItemRelationshipTypeEnum cartItemProductType);
        void AddSimpleItemInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeShoppingCartItem shoppingCartItem);
        void AddSimpleProductLineItemsInOrderLineItem(OrderLineItemModel orderLineItem, ZnodeCartItemRelationshipTypeEnum cartItemProductType);
        void AddSkuInventoryTracking(ZnodeProductBaseEntity product, Dictionary<string, string> skuInventoryTracking);
        bool CancelExistingOrder(ZnodeOrderFulfillment order, int orderId);
        int CreateOrderShipment(AddressModel shippingAddress, int? shippingId, string emailId);
        string GetCategoryNameByCategoryId(int categoryId);
        void GetCategoryNameByCategoryIds(List<int> categoryIds);
        void GetDistinctCategoryIdsforCartItem(ZnodeMultipleAddressCart addressCart);
        string GetInventoryTrackingBySKU(string sku, Dictionary<string, string> skusInventory);
        decimal GetLineItemDiscountAmount(decimal discountAmount, decimal quantity);
        decimal GetLineItemQuantity(ZnodeCartItemRelationshipTypeEnum productType, decimal groupProductQuantity, decimal cartQuantity);
        ZnodeOrderFulfillment GetOrderFullfillment(UserAddressModel userAccount, ZnodePortalCart shoppingCart, int portalId);
        decimal GetOrderLineItemPrice(ZnodeCartItemRelationshipTypeEnum cartItemProductType, ZnodeProductBaseEntity product, ZnodeShoppingCartItem shoppingCartItem);
        decimal GetParentProductPrice(ZnodeShoppingCartItem shoppingCartItem);
        string GetPaymentType(string paymentType, bool isPreAuthorize);
        void GetProductCategoryIds(int[] productCategoryIds, List<int> categoryIds);
        decimal GetProductPrice(ZnodeProductBaseEntity product);
        bool ManageOrderInventory(ZnodeOrderFulfillment order, ZnodePortalCart shoppingCart);
        bool SaveReferralCommission(ZnodeOrderFulfillment order);
        /// <summary>
        /// To Save referral commission and giftcard history.
        /// </summary>
        /// <param name="order">ZnodeOrderFulfillment model</param>
        /// <param name="userId">Current user Id</param>
        void SaveReferralCommissionAndGiftCardHistory(ZnodeOrderFulfillment order, int? userId);
        bool SaveReturnItems(int orderDetailId, ReturnOrderLineItemListModel model);
        List<OrderAttributeModel> SetLineItemAttributes(List<OrderAttributeModel> allAttributes, int[] productCategoryIds);
        void SetOrderAdditionalDetails(ZnodeOrderFulfillment order, SubmitOrderModel model);
        void SetOrderDetails(ZnodeOrderFulfillment order, ZnodePortalCart shoppingCart, UserAddressModel userAccount);
        void SetOrderDetailsToShoppingCart(ZnodeOrderFulfillment order);
        void SetOrderDiscount(OrderModel model, decimal discountAmount, string discountCode, int discountType);
        void SetOrderLineItems(ZnodeOrderFulfillment order, ZnodeMultipleAddressCart addressCart);
        void SetOrderModel(ZnodeOrderFulfillment order);
        void SetOrderShipmentDetails(ZnodeOrderFulfillment order);
        void SetOrderStateTrackingNumber(ZnodeOrderFulfillment order, SubmitOrderModel model);
        bool SetPaymentDetails(ZnodeOrderFulfillment order);
        Dictionary<string, string> SetSKUInventorySetting(ZnodePortalCart shoppingCart);
        ZnodeOrderFulfillment SubmitOrder(SubmitOrderModel model, ShoppingCartModel shoppingCartModel);
        void VerifySubmitOrderProcess(int orderDetailId);
    }
}