﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using Znode.Engine.Shipping;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Libraries.ECommerce.ShoppingCart
{
    [Serializable()]
    public class ZnodePortalCart : ZnodeShoppingCart
    {
        #region Constructor
        public ZnodePortalCart() : base() { }
        #endregion

        List<ZnodeMultipleAddressCart> _addressCarts = new List<ZnodeMultipleAddressCart>();

        #region Properties
        public int PortalID { get; set; }

        // Get Address based cart items.
        public List<ZnodeMultipleAddressCart> AddressCarts
        {
            get
            {
                var noAddressCarts = !_addressCarts.Any();
                var countNotEqual = _addressCarts.Count() != ShoppingCartItems.Cast<ZnodeShoppingCartItem>()
                                           .SelectMany(x => x.OrderShipments.Select(y => y.AddressID))
                                           .Distinct()
                                           .Count();

                var quantityNotEqual = _addressCarts.Sum(x => x.ShoppingCartItems.Cast<ZnodeShoppingCartItem>().Sum(s => s.Quantity)) != ShoppingCartItems.Cast<ZnodeShoppingCartItem>().Sum(s => s.Quantity);

                if (noAddressCarts || countNotEqual || quantityNotEqual)
                {
                    var allOrderShipments = ShoppingCartItems.Cast<ZnodeShoppingCartItem>().SelectMany(x => x.OrderShipments).ToList();

                    var userAddresses = this.UserAddress;

                    _addressCarts = allOrderShipments.GroupBy(x => new { x.AddressID, x.ShippingID },
                                                              (orderShipmentKey, orderShipmentKeyGroup) => new ZnodeMultipleAddressCart()
                                                              {
                                                                  AddressID = orderShipmentKey.AddressID,
                                                                  Shipping = GetShippingMethod(orderShipmentKey.ShippingID, orderShipmentKeyGroup.FirstOrDefault()),
                                                                  ShoppingCartItems = GetAddressCartItems(orderShipmentKey.AddressID, userAddresses)
                                                              }
                                                                ).ToList();
                }
                return _addressCarts;
            }
        }

        public override decimal ShippingCost
        {
            get
            {
                var addressCarts = AddressCarts.Where(x => x.Shipping != null).ToList();
                decimal totalShippingCost = addressCarts.Sum(x => x.ShippingCost);
                return !Equals(CustomShippingCost, null) ? CustomShippingCost.GetValueOrDefault() : totalShippingCost;
            }
        }

        //Gets the totalTaxCost 
        public override decimal TaxCost
        {
            get
            {
                decimal totalTaxCost = AddressCarts.Sum(x => x.OrderLevelTaxes);
                return !Equals(CustomTaxCost, null) ? CustomTaxCost.GetValueOrDefault() : totalTaxCost;
            }
        }

        // Gets total cost of items in the shopping cart before shipping and taxes
        public override decimal SubTotal
        {
            get
            {
                var subTotal = _addressCarts.SelectMany(x => x.ShoppingCartItems.Cast<ZnodeShoppingCartItem>()).Sum(item => item.ExtendedPrice);
                return subTotal;
            }
        }

        // Gets the total cost after shipping, taxes and promotions
        public override decimal Total
        {
            get
            {
                decimal tempCalc = 0;
                tempCalc = (SubTotal - Discount) + ShippingCost + ShippingDifference + (!Equals(CustomTaxCost, null) ? 0 : TaxCost) + OrderLevelTaxes - GiftCardAmount - CSRDiscount;
                return tempCalc;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Calculates final pricing, shipping and taxes in the cart.
        /// </summary>
        public override void Calculate() => Calculate(null);

        /// <summary>
        /// Calculates final pricing, shipping and taxes in the cart.
        /// </summary>
        public override void Calculate(int? profileId, bool isCalCulateTaxAndShipping = true)
        {
            // Clear previous messages
            this._ErrorMessage = new StringBuilder();

            // Promotions
            ZnodeCartPromotionManager cartPromoManager = new ZnodeCartPromotionManager(this, profileId);
            cartPromoManager.Calculate();

            var addressShippingPayments = _addressCarts.Select(x => new { x.AddressID, x.AddressCartID, x.Shipping, x.Payment }).ToList();

            int? portalId = _addressCarts?.Select(s => s.PortalId)?.FirstOrDefault();

            _addressCarts = new List<ZnodeMultipleAddressCart>();

            addressShippingPayments.ForEach(x =>
            {
                var item = AddressCarts.FirstOrDefault(y => y.AddressID == x.AddressID);
                if (!Equals(item, null))
                {
                    item.AddressCartID = x.AddressCartID;
                    item.Shipping = x.Shipping;
                    item.Payment = x.Payment;
                    item.PortalId = portalId;
                    item.Coupons = this.Coupons;
                    item.UserId = this.UserId;
                    item.CurrencyCode = this.CurrencyCode;
                    item.CultureCode = this.CultureCode;
                    item.OrderId = this.OrderId;
                    item.ProfileId = profileId;
                    item.PublishStateId = this.PublishStateId;
                    item.IsAllowWithOtherPromotionsAndCoupons = this.IsAllowWithOtherPromotionsAndCoupons;
                    item.Calculate();
                }
            });

            GiftCardAmount = 0;

            if (!Equals(GiftCardNumber, string.Empty))
                AddGiftCard(GiftCardNumber, this.OrderId);

            //to apply csr discount amount
            if (CSRDiscountAmount > 0)
                AddCSRDiscount(CSRDiscountAmount);
        }

        #endregion

        #region Private Methods

        private static ZnodeShippings GetShippingMethod(int shippingId, ZnodeOrderShipment orderShipment)
        {

            if (shippingId > 0 && !Equals(orderShipment, null))
            {
                return new ZnodeShippings()
                {
                    ShippingID = shippingId,
                    ShippingName = orderShipment.ShippingName
                };
            }
            return new ZnodeShippings();

        }

        private ZnodeGenericCollection<ZnodeShoppingCartItem> GetAddressCartItems(int addressId, UserAddressModel userAddress)
        {
            List<AddressModel> addresses = userAddress?.Addresses ?? null;

            if (IsNotNull(addresses))
            {
                AddressModel address = addresses.FirstOrDefault(x => x.AddressId == addressId);
                var items = ShoppingCartItems.Cast<ZnodeShoppingCartItem>().Where(y => y.OrderShipments.Any(z => z.AddressID == addressId)).Select(c => c.Clone()).ToList();
                ZnodeGenericCollection<ZnodeShoppingCartItem> returnItems = new ZnodeGenericCollection<ZnodeShoppingCartItem>();
                items.ForEach(y =>
                {
                    y.Product.AddressToShip = address;
                    y.Quantity = y.OrderShipments.Where(z => z.AddressID == addressId).Sum(s => s.Quantity);
                    returnItems.Add(y);
                });
                return returnItems;
            }

            return new ZnodeGenericCollection<ZnodeShoppingCartItem>();
        }
        #endregion
    }
}

