﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Libraries.ECommerce.ShoppingCart
{
    public class OrderInventoryManageHelper : IOrderInventoryManageHelper
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _orderLineItemRepository;
        private readonly IZnodeRepository<ZnodeOmsSavedCartLineItem> _savedCartLineItemRepository;
        public static Mutex MuTexLock = new Mutex();
        #endregion Private Variables

        #region Constructor

        public OrderInventoryManageHelper()
        {
            _orderLineItemRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
            _savedCartLineItemRepository = new ZnodeRepository<ZnodeOmsSavedCartLineItem>();
        }
        #endregion Constructor

        /// <summary>
        /// Set backordering for the shopping cart items (webstore)
        /// </summary>
        /// <param name="ShoppingCartItems"></param>
        public virtual void SetBackOrderingForShoppingCart(ZnodeGenericCollection<ZnodeShoppingCartItem> ShoppingCartItems)
        {
            IEnumerable<ZnodeShoppingCartItem> shoppingCartItems = ShoppingCartItems?.Cast<ZnodeShoppingCartItem>();

            Parallel.ForEach(shoppingCartItems, item =>
            {
                if(IsDisablePurchasing(item.Product.InventoryTracking))
                  item.Product.InventoryTracking = ZnodeConstant.AllowBackOrdering;

                SetBackOrderingForBundleProduct(item);

                SetBackOrderingForAddOnProduct(item);

                SetBackOrderingForConfigurableProduct(item);

                SetBackOrderingForGroupProduct(item);
            });
        }

        //to check dont track inventory set to true
        protected virtual bool IsDisablePurchasing(string inventoryTracking)
        {
            return (inventoryTracking.ToLower() == ZnodeConstant.DisablePurchasing.ToString().ToLower());
        }


        /// <summary>
        /// Set Back Ordering For Group Product
        /// </summary>
        /// <param name="item"></param>
        protected  void SetBackOrderingForGroupProduct(ZnodeShoppingCartItem item)
        {
            foreach (ZnodeProductBaseEntity group in item.Product.ZNodeGroupProductCollection)
            {
                if (IsDisablePurchasing(item.Product.InventoryTracking))
                    group.InventoryTracking = ZnodeConstant.AllowBackOrdering;
            }
        }

        /// <summary>
        /// Set Back Ordering For Configurable Product
        /// </summary>
        /// <param name="item"></param>
        protected  void SetBackOrderingForConfigurableProduct(ZnodeShoppingCartItem item)
        {
            foreach (ZnodeProductBaseEntity config in item.Product.ZNodeConfigurableProductCollection)
            {
                if (IsDisablePurchasing(item.Product.InventoryTracking))
                    config.InventoryTracking = ZnodeConstant.AllowBackOrdering;
            }
        }

        /// <summary>
        /// Set Back Ordering For AddOn Product
        /// </summary>
        /// <param name="item"></param>
        protected  void SetBackOrderingForAddOnProduct(ZnodeShoppingCartItem item)
        {
            foreach (ZnodeProductBaseEntity addon in item.Product.ZNodeAddonsProductCollection)
            {
                if (IsDisablePurchasing(item.Product.InventoryTracking))
                    addon.InventoryTracking = ZnodeConstant.AllowBackOrdering;
            }
        }

        /// <summary>
        /// Set Back Ordering For Bundle Product
        /// </summary>
        /// <param name="item"></param>
        protected  void SetBackOrderingForBundleProduct(ZnodeShoppingCartItem item)
        {
            if (item.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.Bundles &&  IsDisablePurchasing(item.Product.InventoryTracking))
            {
                item.Product.InventoryTracking = ZnodeConstant.AllowBackOrdering;
            }
        }

        /// <summary>
        /// Set backordering for the shopping cart items (admin)
        /// </summary>
        /// <param name="shoppingCartItems"></param>
        public virtual void SetBackOrderingForShoppingCart(List<ShoppingCartItemModel> shoppingCartItems)
        {
            Parallel.ForEach(shoppingCartItems, item =>
            {
                item.TrackInventory = false;
            });
        }

        
    }
}
