﻿using System;
using System.Collections.Generic;
using System.Web;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Libraries.ECommerce.Utilities;
using System.Linq;
using Znode.Admin.Core.Helpers;
using System.Diagnostics;
using Znode.Libraries.Framework.Business;
using System.Diagnostics;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Admin.Helpers
{
    public class SessionProxyHelper
    {
        //Summary
        //Redirect User to Index page in case the un authorized access.
        //Return the List of action Permission list in List<string> format.
        public static List<RolePermissionViewModel> GetUserPermission()
        {
            List<RolePermissionViewModel> lstUserPermission = null;
            try
            {
                if (Equals(HttpContext.Current.Session[AdminConstants.UserPermissionListSessionKey], null))
                {
                    //Get the User Roles Permission List
                    IEnumerable<RolePermissionViewModel> permissionList = GetService<IDependencyHelper>().GetPermissions();
                    if (!Equals(permissionList, null) && permissionList.Count() > 0)
                    {
                        lstUserPermission = permissionList.ToList();
                    }
                    HttpContext.Current.Session[AdminConstants.UserPermissionListSessionKey] = lstUserPermission;
                }
                else
                {
                    //Set value from Session Variable
                    lstUserPermission = HttpContext.Current.Session[AdminConstants.UserPermissionListSessionKey] as List<RolePermissionViewModel>;
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            return lstUserPermission;
        }

        //Summary
        // To Check whether Current login user is Admin or Not.
        public static bool IsAdminUser()
        {
            bool isAdminUser = false;
            if (Equals(HttpContext.Current.Session[AdminConstants.IsAdminUserSessionKey], null))
            {
                isAdminUser = GetService<IDependencyHelper>().IsUserInRole();
                HttpContext.Current.Session[AdminConstants.IsAdminUserSessionKey] = isAdminUser;
            }
            else
            {
                //Set value from Session Variable
                isAdminUser = Convert.ToBoolean(HttpContext.Current.Session[AdminConstants.IsAdminUserSessionKey]);
            }
            return isAdminUser;
        }


        //Summary
        //To Get the Menu list.
        //Return the List of Menu in MenuListModel format
        public MenuListViewModel GetMenuListByUserName()
        {
            MenuListViewModel menuList = new MenuListViewModel();
            try
            {
                if (Equals(HttpContext.Current.Session[AdminConstants.UserMenuListSessionKey], null))
                {
                    //Gets the Menu List
                    IMenuAgent _menuAgent = new MenuAgent(new MenuClient(), new RoleClient(), new AccessPermissionClient());

                    FilterCollection filters = new FilterCollection();
                    filters.Add(new FilterTuple(ZnodeMenuEnum.IsActive.ToString(), FilterOperators.Equals, "true"));
                    if (!IsAdminUser())
                    {
                        filters.Add(new FilterTuple(FilterKeys.Username.ToString(), FilterOperators.Equals, HttpContext.Current.User.Identity.Name));
                    }
                    menuList = _menuAgent.GetMenuList(filters, null, null, null);
                    HttpContext.Current.Session[AdminConstants.UserMenuListSessionKey] = menuList;
                }
                else
                {
                    //Set value from Session Variable
                    menuList = HttpContext.Current.Session[AdminConstants.UserMenuListSessionKey] as MenuListViewModel;
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            return menuList;
        }

        //Get the Login User Details based on the user name. To bind user id in Api request.
        public static UserViewModel GetUserDetails()
        {
            UserViewModel model = null;
            try
            {
                if (Equals(HttpContext.Current.Session[AdminConstants.UserAccountSessionKey], null))
                {
                    if (HttpContext.Current.User != null)
                    {
                        //Get the User Details. 
                        //Don't Use the Agent here, it will cause the infinite looping. As this method gets called from the BaseAgent.
                        UserClient client = new UserClient();
                        var userModel = client.GetAccountByUser(HttpContext.Current.User.Identity.Name);
                        if (!Equals(userModel, null))
                        {
                            model = userModel.ToViewModel<UserViewModel>();
                            HttpContext.Current.Session[AdminConstants.UserAccountSessionKey] = model;
                        }
                        client = null;

                    }

                }
                else
                {
                    //Set value from Session Variable
                    model = HttpContext.Current.Session[AdminConstants.UserAccountSessionKey] as UserViewModel;
                }
            }
            catch { }
            return model;
        }

        //Get the Collapse Menu Status from Session.
        public bool GetCollapseMenuStatus()
        {
            bool menuStatus = true;
            if (!Equals(HttpContext.Current.Session[AdminConstants.CollapseMenuStatus], null))
            {
                menuStatus = Convert.ToBoolean(HttpContext.Current.Session[AdminConstants.CollapseMenuStatus]);
            }
            return menuStatus;
        }
    }
}