﻿using System.Web.Mvc;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModels;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Admin.Controllers
{
    public class GiftCardController : BaseController
    {
        #region Private Variables

        private readonly IGiftCardAgent _giftCardAgent;
        private readonly IOrderAgent _orderAgent;
        private readonly IStoreAgent _storeAgent;

        private readonly string storeListAsidePanelPopup = "_AsideStorelistPanelPopup";
        private readonly string GiftCardList = "~/Views/GiftCard/List.cshtml";

        #endregion

        #region Public Constructor

        public GiftCardController(IGiftCardAgent giftCardAgent, IOrderAgent orderAgent, IStoreAgent storeAgent)
        {
            _giftCardAgent = giftCardAgent;
            _orderAgent = orderAgent;
            _storeAgent = storeAgent;
        }

        #endregion

        #region Public Methods
        // Get GiftCard list.
        public virtual ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, bool isExcludeExpired = true)
        {
            //Get and Set Filters from Cookies if exists.
            FilterHelpers.GetSetFiltersFromCookies(GridListType.ZnodeGiftCard.ToString(), model);
            //Assign default view filter and sorting if exists for the first request.
            FilterHelpers.GetDefaultView(GridListType.ZnodeGiftCard.ToString(), model);
            //Get GiftCard list.
            GiftCardListViewModel giftCardList = _giftCardAgent.GetGiftCardList(model.Expands, model.Filters, model.SortCollection, model.Page, model.RecordPerPage, isExcludeExpired);

            //Get the grid model.
            giftCardList.GridModel = FilterHelpers.GetDynamicGridModel(model, giftCardList?.GiftCardList, GridListType.ZnodeGiftCard.ToString(), string.Empty, null, true, true, giftCardList?.GridModel?.FilterColumn?.ToolMenuList);
            giftCardList.GridModel.TotalRecordCount = giftCardList.TotalResults;

            //Returns the GiftCard list.
            return ActionView(giftCardList);
        }

        //Get:Create GiftCard.
        public virtual ActionResult Create()
        {
            GiftCardViewModel giftCardViewModel = new GiftCardViewModel();

            giftCardViewModel.CardNumber = _giftCardAgent.GetRandomGiftCardNumber();
            giftCardViewModel.ExpirationDate = null;
            return View(AdminConstants.CreateEdit, giftCardViewModel);
        }

        //Post:Create GiftCard.
        [HttpPost]
        public virtual ActionResult Create(GiftCardViewModel giftCardViewModel)
        {
            string message = string.Empty;
            if (ModelState.IsValid)
            {
                if (IsNotNull(giftCardViewModel.RmaRequestModel))
                {
                    if (IsNotNull(giftCardViewModel) && (IsNotNull(giftCardViewModel.UserId)))
                    {
                        int accountId = giftCardViewModel.UserId.GetValueOrDefault();

                        //Craete gift card
                        GiftCardViewModel giftCard = _giftCardAgent.Create(giftCardViewModel);

                        if (IsNotNull(giftCard) && giftCard.GiftCardId > 0)
                        {
                            giftCardViewModel.GiftCardId = giftCard.GiftCardId;

                            //Update RMA 
                            if (_giftCardAgent.UpdateRMA(giftCardViewModel, out message))
                            {
                                SetNotificationMessage(string.IsNullOrEmpty(message) ? GetSuccessNotificationMessage(Admin_Resources.RecordCreationSuccessMessage) : GetErrorNotificationMessage(giftCardViewModel.ErrorMessage));
                                return RedirectToAction<GiftCardController>(x => x.Edit(giftCard.GiftCardId));
                            }
                        }
                        SetNotificationMessage((IsNotNull(giftCard) && giftCard.GiftCardId > 0) ? GetSuccessNotificationMessage(giftCardViewModel.SuccessMessage) : GetErrorNotificationMessage(giftCardViewModel.ErrorMessage));
                        return RedirectToAction(GiftCardList, "GiftCard");
                    }
                }
                else
                {
                    giftCardViewModel = _giftCardAgent.Create(giftCardViewModel);

                    if (!giftCardViewModel.HasError)
                    {
                        SetNotificationMessage(GetSuccessNotificationMessage(Admin_Resources.RecordCreationSuccessMessage));
                        return RedirectToAction<GiftCardController>(x => x.Edit(giftCardViewModel.GiftCardId));
                    }
                }
            }
            SetNotificationMessage(GetErrorNotificationMessage(giftCardViewModel.ErrorMessage));
            return View(AdminConstants.CreateEdit, giftCardViewModel);
        }

        //Get:Edit GiftCard.
        [HttpGet]
        public virtual ActionResult Edit(int giftCardId)
        {
            ActionResult action = GotoBackURL();
            if (action != null)
                return action;

            return ActionView(AdminConstants.CreateEdit, _giftCardAgent.GetGiftCard(giftCardId));
        }

        //Post:Edit GiftCard.
        [HttpPost]
        public virtual ActionResult Edit(GiftCardViewModel giftCardViewModel)
        {
            if (ModelState.IsValid)
            {
                SetNotificationMessage(_giftCardAgent.Update(giftCardViewModel).HasError
                ? GetErrorNotificationMessage(Admin_Resources.UpdateErrorMessage)
                : GetSuccessNotificationMessage(Admin_Resources.UpdateMessage));
                return RedirectToAction<GiftCardController>(x => x.Edit(giftCardViewModel.GiftCardId));
            }
            SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.UpdateErrorMessage));
            return View(AdminConstants.CreateEdit, giftCardViewModel);
        }

        //Delete GiftCard.      
        public virtual JsonResult Delete(string giftCardId)
        {
            bool status = false;
            if (!string.IsNullOrEmpty(giftCardId))
                status = _giftCardAgent.DeleteGiftCard(giftCardId);

            return Json(new { status = status, message = status ? Admin_Resources.DeleteMessage : Admin_Resources.AssociatedGiftCardDeleteErrorMessage }, JsonRequestBehavior.AllowGet);
        }

        //Check entered Customer id already exists or not.
        [HttpPost]
        public virtual JsonResult IsUserIdExist(int UserId, int portalId = 0)
          => Json(_giftCardAgent.CheckIsUserIdExist(UserId, portalId), JsonRequestBehavior.AllowGet);

        //Get Customer list based on portal.
        public virtual ActionResult GetCustomerList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, int portalId)
        {
            //Assign default view filter and sorting if exists for the first request.
            FilterHelpers.GetDefaultView(GridListType.ZnodeGiftCardCustomer.ToString(), model);
            //Get the list of Customers.    
            CustomerListViewModel customerList = _orderAgent.GetCustomerList(portalId, 0, false, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            //Get the grid model.
            customerList.GridModel = FilterHelpers.GetDynamicGridModel(model, customerList.List, GridListType.ZnodeGiftCardCustomer.ToString(), string.Empty, null, true, true, null);

            //Set portalId and IsAccountCustomer.
            customerList.PortalId = portalId;

            //Set the total record count.
            customerList.GridModel.TotalRecordCount = customerList.TotalResults;

            return ActionView(AdminConstants.CustomerListView, customerList);
        }

        //Get active currency.
        public virtual JsonResult GetActiveCurrencyToStore(int portalId)
        {
            CurrencyViewModel currencyViewModel = new CurrencyViewModel();
            currencyViewModel = _giftCardAgent.GetActiveCurrency(portalId);
            return Json(new { currencyViewModel = currencyViewModel }, JsonRequestBehavior.AllowGet);
        }

        //Get currency details by code.
        public virtual JsonResult GetCurrencyDetailsByCode(string currencyCode)
        {
            CurrencyViewModel currencyViewModel = new CurrencyViewModel();
            currencyViewModel = _giftCardAgent.GetCurrencyDetailsByCode(currencyCode);
            return Json(new { currencyViewModel = currencyViewModel }, JsonRequestBehavior.AllowGet);
        }

        //Get Portal List
        public virtual ActionResult GetPortalList([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            //Assign default view filter and sorting if exists for the first request.
            FilterHelpers.GetDefaultView(GridListType.ZnodeUserPortalList.ToString(), model);

            StoreListViewModel storeList = _storeAgent.GetStoreList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            //Get the grid model.
            storeList.GridModel = FilterHelpers.GetDynamicGridModel(model, storeList.StoreList, GridListType.ZnodeUserPortalList.ToString(), string.Empty, null, true);

            //Set the total record count
            storeList.GridModel.TotalRecordCount = storeList.TotalResults;

            return ActionView(storeListAsidePanelPopup, storeList);
        }
        #endregion
    }
}