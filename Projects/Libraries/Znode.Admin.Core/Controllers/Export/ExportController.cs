﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Controllers;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Api.Client.Sorts;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;

namespace Znode.Admin.Core.Controllers
{
    public class ExportController : BaseController
    {
        #region Private Variables
        private readonly IExportAgent _exportAgent;
        #endregion

        #region Constructor
        public ExportController(IExportAgent exportAgent)
        {
            _exportAgent = exportAgent;
        }
        #endregion

        #region Public Method
        public virtual JsonResult Export(string exportFileTypeId, string Type, int localId)
        {
            FilterCollection filters = SessionHelper.GetDataFromSession<FilterCollection>(DynamicGridConstants.FilterCollectionsSessionKey);
            SortCollection sort = SessionHelper.GetDataFromSession<SortCollection>(DynamicGridConstants.SortCollectionSessionKey);
            string exportContent = string.Empty;
            List<dynamic> exportList = _exportAgent.GetExportList(Type, filters, sort, null, null, SessionHelper.GetDataFromSession<int>(DynamicGridConstants.FolderId),
                                                                 localId);
            if (HelperUtility.IsNotNull(exportList) && exportList.Count > 0)
                exportContent = GetExportData(exportFileTypeId, exportList);

            JsonResult jsonResult = Json(new { content = exportContent, fileName = Equals(exportFileTypeId, "1") ? $"{Type}.xls" : $"{Type}.csv", status = Equals(string.IsNullOrEmpty(exportContent), false), message = Admin_Resources.NoRecordFoundText }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }
        #endregion

        #region Private Method
        /// <summary>
        /// Get Export Data By list
        /// </summary>
        /// <param name="exportFileTypeId">ExportTypeId - 1-Excel ,2-CSV</param>
        /// <param name="exportList">ExportList</param>
        /// <returns>ExportContent</returns>
        protected string GetExportData(string exportFileTypeId, List<dynamic> exportList)
            => new DownloadHelper().ExportDownload(exportFileTypeId, _exportAgent.CreateDataSource(exportList), null, null, null, false);

    }
    #endregion
}
