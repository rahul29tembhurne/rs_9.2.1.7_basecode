﻿using System.Web.Mvc;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.ViewModels;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.Controllers
{
    public class ImportController : BaseController
    {
        #region Private Variables
        private readonly IImportAgent _importAgent;
        private const string logStatusView = "_ImportLogStatus";
        private const string logDetailsView = "_ImportLogDetails";
        #endregion

        #region Public Constructor
        public ImportController(IImportAgent importAgent)
        {
            _importAgent = importAgent;
        }
        #endregion

        #region Public Methods

        //This method will fetch the list of import types and bind it on view.
        [HttpGet]
        public virtual ActionResult Index() => View(AdminConstants.Create, _importAgent.BindViewModel());

        //This method will will upload the file and process the uploaded data for import.
        [HttpPost]
        public virtual ActionResult Index(ImportViewModel importModel)
        {
            string message = importModel.IsPartialPage ? Admin_Resources.LinkViewImportLogs : "";
            bool status= _importAgent.ImportData(importModel);
            if (status)
                SetNotificationMessage(GetSuccessNotificationMessage(Admin_Resources.ImportProcessInitiated + message));
            else
                SetNotificationMessage(GetErrorNotificationMessage(Admin_Resources.ImportProcessFailed + message));

            //Assign the values back to model
            importModel = _importAgent.BindViewModel();
            return Json(new { status = status }, JsonRequestBehavior.AllowGet);
        }

        //Get all pricing list and code for pricing.
        [AllowAnonymous]
        public virtual ActionResult GetPricingList()
            => Json(new { pricingList = _importAgent.GetPricingList() }, JsonRequestBehavior.AllowGet);

        //Get all country list.
        [AllowAnonymous]
        public virtual ActionResult GetCountryList()
           => Json(new { countryList = _importAgent.GetCountryList() }, JsonRequestBehavior.AllowGet);

        //Get all Portal list
        [AllowAnonymous]
        public virtual ActionResult GetportalList() => Json(new { portalList = _importAgent.GetPortalList()},JsonRequestBehavior.AllowGet);

        //Action to get import template list for dropdown.
        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult BindTemplateList(int importHeadId, int familyId)
          => Json(new { templatenamelist = _importAgent.GetImportTemplateList(importHeadId, familyId) }, JsonRequestBehavior.AllowGet);

        //Action to get associated template list for selected template.
        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult GetAssociatedTemplateList(int templateId, int importHeadId, int familyId)
          => Json(new { templateMappingList = _importAgent.GetImportTemplateMappingList(templateId, importHeadId, familyId) }, JsonRequestBehavior.AllowGet);

        //Get all families for Product import
        [AllowAnonymous]
        public virtual ActionResult GetAllFamilies(bool isCategory)
            => Json(new { productFamilies = _importAgent.GetAllFamilies(isCategory) }, JsonRequestBehavior.AllowGet);

        //Action to get saved CSV data.
        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult GetCsvData(ImportViewModel model)
        {
            string fileName = string.Empty;
            string csvHeaders = _importAgent.GetCsvHeaders(model.FilePath, out fileName);
            return Json(new { Csvlist = csvHeaders, UpdateFileName = fileName }, JsonRequestBehavior.AllowGet);
        }

        // Download the template
        [HttpPost]
        public virtual ActionResult DownloadTemplate(int downloadImportHeadId, string downloadImportName, int downloadImportFamilyId)
            => View(AdminConstants.Create, _importAgent.DownloadTemplate(downloadImportHeadId, downloadImportName, downloadImportFamilyId, Response));

        //Show import logs list
        public virtual ActionResult list([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            // Get and Set Filters from Cookies if exists.
            FilterHelpers.GetSetFiltersFromCookies(GridListType.ZnodeImportProcessLog.ToString(), model);
            //Assign default view filter and sorting if exists for the first request.
            FilterHelpers.GetDefaultView(GridListType.ZnodeImportProcessLog.ToString(), model);
            ImportProcessLogsListViewModel importProcessLogs = _importAgent.GetImportLogs(model.Expands, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            //Get the grid model.
            importProcessLogs.GridModel = FilterHelpers.GetDynamicGridModel(model, importProcessLogs?.ProcessLogs, GridListType.ZnodeImportProcessLog.ToString(), string.Empty, null, true, true, importProcessLogs?.GridModel?.FilterColumn?.ToolMenuList);

            //Set the total record count
            importProcessLogs.GridModel.TotalRecordCount = importProcessLogs.TotalResults;
            return ActionView(importProcessLogs);
        }

        //Show logs status
        public virtual ActionResult ShowLogStatus(int importProcessLogId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            //Assign default view filter and sorting if exists for the first request.
            FilterHelpers.GetDefaultView(GridListType.ZnodeImportProcessLogStatus.ToString(), model);
            ImportProcessLogsListViewModel importProcessLogs = _importAgent.GetImportLogStatus(importProcessLogId, model.Expands, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            //Get the grid model.
            importProcessLogs.GridModel = FilterHelpers.GetDynamicGridModel(model, importProcessLogs?.ProcessLogs, GridListType.ZnodeImportProcessLogStatus.ToString(), string.Empty, null, true, true, importProcessLogs?.GridModel?.FilterColumn?.ToolMenuList);

            return ActionView(logStatusView, importProcessLogs);
        }

        //Show logs details
        public virtual ActionResult ShowLogDetails(int importProcessLogId, [ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            //Assign default view filter and sorting if exists for the first request.
            FilterHelpers.GetDefaultView(GridListType.ZnodeImportLogDetails.ToString(), model);
            ImportLogsListViewModel importLogs = _importAgent.GetImportLogDetails(importProcessLogId, model.Expands, model.Filters, model.SortCollection, model.Page, model.RecordPerPage);

            //Get the grid model.
            importLogs.GridModel = FilterHelpers.GetDynamicGridModel(model, importLogs?.LogsList, GridListType.ZnodeImportLogDetails.ToString(), string.Empty, null, true, true, importLogs?.GridModel?.FilterColumn?.ToolMenuList);

            //Set the total record count
            importLogs.GridModel.TotalRecordCount = importLogs.TotalResults;
            return ActionView(logDetailsView, importLogs);
        }

        //Delete logs 
        public virtual JsonResult DeleteLogs(string importProcessLogId)
        {
            string message = string.Empty;
            bool isDeleted = _importAgent.DeleteLog(importProcessLogId);
            message = isDeleted ? Admin_Resources.DeleteMessage : Admin_Resources.DeleteErrorMessage;
            return Json(new { status = isDeleted, message = message }, JsonRequestBehavior.AllowGet);
        }

        //Update Mappings
        [HttpPost]
        public virtual ActionResult UpdateMappings(ImportViewModel importModel)
        {
            if (importModel.TemplateId.Equals(0))
                return Json(new { status = false, message = Admin_Resources.ImportNoTemplateSelected }, JsonRequestBehavior.AllowGet);

            string message = string.Empty;
            bool isUpdated = _importAgent.UpdateMappings(importModel);
            message = isUpdated ? Admin_Resources.ImportMappingUpdateMessage : Admin_Resources.ImportMappingUpdateErrorMessage;
            return Json(new { status = isUpdated, message = message }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}