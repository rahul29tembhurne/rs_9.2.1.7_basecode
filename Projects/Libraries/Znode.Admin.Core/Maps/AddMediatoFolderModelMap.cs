﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Admin.Maps
{
    public static class AddMediatoFolderModelMap
    {
        public static AddMediatoFolderModel ToModel(int folderId, string mediaIds)
        {
            AddMediatoFolderModel model = new AddMediatoFolderModel();
            model.FolderId = folderId;
            model.MediaIds = mediaIds;
            return model;
        }
    }
}