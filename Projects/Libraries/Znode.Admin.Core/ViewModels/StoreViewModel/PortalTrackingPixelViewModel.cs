﻿using System.ComponentModel.DataAnnotations;
using Znode.Libraries.Resources;

namespace Znode.Engine.Admin.ViewModels
{
    public class PortalTrackingPixelViewModel : BaseViewModel
    {
        public int PortalId { get; set; }
        public int PortalPixelTrackingId { get; set; }
        public string StoreName { get; set; }

        [MaxLength(50, ErrorMessageResourceName = ZnodeAdmin_Resources.PixelLengthError, ErrorMessageResourceType = typeof(Admin_Resources))]
        [Display(Name = ZnodeAdmin_Resources.LabelPixel1, ResourceType = typeof(Admin_Resources))]
        public string PixelId1 { get; set; }

        [MaxLength(50, ErrorMessageResourceName = ZnodeAdmin_Resources.PixelLengthError, ErrorMessageResourceType = typeof(Admin_Resources))]
        [Display(Name = ZnodeAdmin_Resources.LabelPixel2, ResourceType = typeof(Admin_Resources))]
        public string PixelId2 { get; set; }

        [MaxLength(50, ErrorMessageResourceName = ZnodeAdmin_Resources.PixelLengthError, ErrorMessageResourceType = typeof(Admin_Resources))]
        [Display(Name = ZnodeAdmin_Resources.LabelPixel3, ResourceType = typeof(Admin_Resources))]
        public string PixelId3 { get; set; }

        [MaxLength(50, ErrorMessageResourceName = ZnodeAdmin_Resources.PixelLengthError, ErrorMessageResourceType = typeof(Admin_Resources))]
        [Display(Name = ZnodeAdmin_Resources.LabelPixel4, ResourceType = typeof(Admin_Resources))]
        public string PixelId4 { get; set; }

        [MaxLength(50, ErrorMessageResourceName = ZnodeAdmin_Resources.PixelLengthError, ErrorMessageResourceType = typeof(Admin_Resources))]
        [Display(Name = ZnodeAdmin_Resources.LabelPixel5, ResourceType = typeof(Admin_Resources))]
        public string PixelId5 { get; set; }

        public string HelpTextPixel1 { get; set; }
        public string HelpTextPixel2 { get; set; }
        public string HelpTextPixel3 { get; set; }
        public string HelpTextPixel4 { get; set; }
        public string HelpTextPixel5 { get; set; }

        public string CodePixel1 { get; set; }
        public string CodePixel2 { get; set; }
        public string CodePixel3 { get; set; }
        public string CodePixel4 { get; set; }
        public string CodePixel5 { get; set; }

        public string DisplayName1 { get; set; }
        public string DisplayName2 { get; set; }
        public string DisplayName3 { get; set; }
        public string DisplayName4 { get; set; }
        public string DisplayName5 { get; set; }
    }
}
