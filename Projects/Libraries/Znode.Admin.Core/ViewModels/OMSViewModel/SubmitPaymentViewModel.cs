﻿namespace Znode.Engine.Admin.ViewModels
{
    public class SubmitPaymentViewModel : BaseViewModel
    {
        public int OmsOrderId { get; set; }
        public int PaymentSettingId { get; set; }
        public int PaymentApplicationSettingId { get; set; }
        public string CustomerProfileId { get; set; }
        public string CustomerPaymentId { get; set; }
        public string CustomerGuid { get; set; }
        public string PaymentToken { get; set; }
        public int UserId { get; set; }
        public int ShippingOptionId { get; set; }
        public int BillingAddressId { get; set; }
        public int ShippingAddressId { get; set; }
        public int PortalId { get; set; }
        public int PortalCatalogId { get; set; }
        public bool EnableAddressValidation { get; set; }
        public bool RequireValidatedAddress { get; set; }
        public string AdditionalInfo { get; set; }
        public string PayPalReturnUrl { get; set; }
        public string PayPalCancelUrl { get; set; }
        public string Token { get; set; }
        public string CreditCardNumber { get; set; }
        public string AccountNumber { get; set; }
        public string ShippingMethod { get; set; }
        public string CardType { get; set; }
        public int? CreditCardExpMonth { get; set; }
        public int? CreditCardExpYear { get; set; }
        public string CardExpiration { get; set; }
        public string CardDetails { get; set; }
        public string CustomerShippingAddressId { get; set; }
        public string CardSecurityCode { get; set; }
        public string PaymentCode { get; set; }
    }
}