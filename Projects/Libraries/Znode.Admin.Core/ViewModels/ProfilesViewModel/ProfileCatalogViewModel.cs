﻿namespace Znode.Engine.Admin.ViewModels
{
    public class ProfileCatalogViewModel : BaseViewModel
    {
        public int ProfileCatalogId { get; set; }
        public int ProfileId { get; set; }
        public int PimCatalogId { get; set; }
        public string ProfileName { get; set; }
        public string CatalogName { get; set; }
        public string PimCatalogIds { get; set; }
    }
}