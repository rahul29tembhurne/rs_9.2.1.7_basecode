﻿using System.Collections.Generic;
using Znode.Engine.Admin.Models;

namespace Znode.Engine.Admin.ViewModels
{
    public class GlobalAttributeGroupListViewModel : BaseViewModel
    {
        public List<GlobalAttributeGroupViewModel> AttributeGroupList { get; set; }

        public GridModel GridModel { get; set; }
    }
}