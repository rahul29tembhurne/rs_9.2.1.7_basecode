﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Znode.Engine.Admin.Models;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Agents
{
    public class TypeaheadAgent : BaseAgent, ITypeaheadAgent
    {
        #region Private Variables
        private readonly ITypeaheadClient _typeaheadClient;
        private readonly IUserClient _userClient;

        #endregion

        #region Constructor
        public TypeaheadAgent(ITypeaheadClient typeaheadClient, IUserClient userClient)
        {
            _typeaheadClient = GetClient<ITypeaheadClient>(typeaheadClient);
            _userClient = GetClient<IUserClient>(userClient);
        }
        #endregion

        #region Public Methods.
        //Get the suggestions of typeahead.
        public virtual List<AutoComplete> GetAutocompleteList(string searchTerm, string searchtype, string fieldname)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", string.Empty, TraceLevel.Info);

            List<AutoComplete> typeaheadList = new List<AutoComplete>();
            //To bind the Typeahead RequestModel

            TypeaheadResponselistModel searchModel = _typeaheadClient.GetSearchlist(GetTypeaheadRequestModel(searchTerm, searchtype, fieldname));
            ZnodeLogging.LogMessage("Typeaheadlist count :", string.Empty, TraceLevel.Verbose, new { typeaheadlistCount= searchModel?.Typeaheadlist?.Count});

            if (Equals(searchtype, ZnodeConstant.StoreList))
            {
                string aspNetUserId = _userClient.GetAccountByUser(HttpContext.Current.User.Identity.Name)?.AspNetUserId;
            ZnodeLogging.LogMessage("Typeaheadlist count :", string.Empty, TraceLevel.Verbose, new { typeaheadlistCount= searchModel?.Typeaheadlist?.Count});

                string[] portalIds = _userClient.GetPortalIds(aspNetUserId)?.PortalIds;
                List<int> portalIdList = new List<int>();

                for (int index = 0; index < portalIds.Length; index++)
                    portalIdList.Add(Convert.ToInt32(portalIds[index]));

                searchModel.Typeaheadlist = (from item in searchModel.Typeaheadlist
                                             join portalId in portalIdList on item.Id equals portalId
                                             orderby item.Name ascending
                                             select item).ToList();
            }
            //Map autocomplete list
            searchModel?.Typeaheadlist?.ForEach(x =>
            {
                typeaheadList.Add(new AutoComplete
                {
                    text = x.Name,
                    value = Convert.ToString(x.Id),
                    Name = x.Name,
                    Id = x.Id,
                    DisplayText = x.Name
                });
            });

            ZnodeLogging.LogMessage("Agent method execution done.", string.Empty, TraceLevel.Info);

            return typeaheadList;
        }
        #endregion

        #region Private Methods.
        //To check item already exists
        private bool AlreadyExist(List<AutoComplete> autoCompleteList, AutoComplete autoCompleteModel)
            => autoCompleteList.Any(x => x.text == autoCompleteModel.text);

        //To bind the Typeahead RequestModel
        private TypeaheadRequestModel GetTypeaheadRequestModel(string searchTerm, string searchtype, string fieldname)
        {
            TypeaheadRequestModel requestmodel = new TypeaheadRequestModel();

            switch (searchtype)
            {
                case "StoreList":
                    requestmodel.Type = ZnodeTypeAheadEnum.StoreList;
                    break;
                case "CatalogList":
                    requestmodel.Type = ZnodeTypeAheadEnum.CatalogList;
                    break;
                case "ProductList":
                    requestmodel.Type = ZnodeTypeAheadEnum.ProductList;
                    break;
            }
            requestmodel.FieldName = fieldname;
            requestmodel.Searchterm = searchTerm;
            return requestmodel;
        }
        #endregion
    }
}
