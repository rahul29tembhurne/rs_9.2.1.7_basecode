﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.Maps;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Admin.Agents
{
    public class CartAgent : BaseAgent, ICartAgent
    {
        #region Private member

        private readonly IShoppingCartClient _shoppingCartsClient;
        private readonly IPublishProductClient _publishProductClient;
        private readonly IOrderStateClient _orderStateClient;
        private readonly IPortalClient _portalClient;
        private readonly IUserClient _userClient;

        #endregion Private member

        #region Constructor

        public CartAgent(IShoppingCartClient shoppingCartClient, IPublishProductClient publishProductClient, IOrderStateClient orderStateClient, IPortalClient portalClient, IUserClient userClient)
        {
            _shoppingCartsClient = GetClient<IShoppingCartClient>(shoppingCartClient);
            _publishProductClient = GetClient<IPublishProductClient>(publishProductClient);
            _orderStateClient = GetClient<IOrderStateClient>(orderStateClient);
            _portalClient = portalClient;
            _userClient = GetClient<IUserClient>(userClient);
        }

        #endregion Constructor

        #region Public methods

        //Create/Update new shopping cart.
        public virtual CartViewModel CreateCart(CartItemViewModel cartItem)
        {

            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNotNull(cartItem))
            {
                ZnodeLogging.LogMessage("Input parameter CartItemViewModel having:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info,new { UserId = cartItem.UserId });
                SetCreatedByUser(cartItem.UserId);
                ShoppingCartModel cartModel = new ShoppingCartModel();
                //If shopping cart is null then return shoppingCartModel with PortalId, LocaleId, PublishedCatalogId, UserId.
                if (cartItem.OmsOrderId > 0)
                    cartModel = GetFromSession<OrderModel>(AdminConstants.OMSOrderSessionKey + cartItem.OmsOrderId)?.ShoppingCartModel;
                else
                {
                    if (GetFromSession<ShoppingCartModel>(AdminConstants.CartModelSessionKey) != null)
                    {
                        cartModel.Coupons = GetFromSession<ShoppingCartModel>(AdminConstants.CartModelSessionKey).Coupons?.Count > 0 ? GetFromSession<ShoppingCartModel>(AdminConstants.CartModelSessionKey).Coupons : new List<CouponModel>();
                        cartModel.CSRDiscountAmount = GetFromSession<ShoppingCartModel>(AdminConstants.CartModelSessionKey).CSRDiscountAmount != 0 ? GetFromSession<ShoppingCartModel>(AdminConstants.CartModelSessionKey).CSRDiscountAmount : 0;
                        cartModel.GiftCardNumber = GetFromSession<ShoppingCartModel>(AdminConstants.CartModelSessionKey).GiftCardNumber;
                    }
                }

                cartModel.PortalId = cartItem.PortalId;
                cartModel.LocaleId = cartItem.LocaleId;
                cartModel.PublishedCatalogId = cartItem.CatalogId;
                cartModel.UserId = cartItem.UserId;
                cartModel.OmsOrderId = cartItem.OmsOrderId;

                //Get attribute values and code.
                if (!string.IsNullOrEmpty(cartItem.PersonalisedCodes))
                    PersonalisedItems(cartItem);
               
                //Create new cart.
                if (!string.IsNullOrEmpty(cartItem.GroupProductSKUs) && !string.IsNullOrEmpty(cartItem.GroupProductsQuantity) || cartItem?.GroupProducts?.Count > 0)
                    GetGroupShoppiongCartModel(cartItem, cartModel);
                else
                {
                    cartModel.ShoppingCartItems.Add(cartItem?.ToModel<ShoppingCartItemModel>());
                    if (cartItem.OmsOrderId > 0)
                        AddProductHistory(cartItem);
                }

                if (cartModel.Shipping?.ShippingId < 1)
                    cartModel.ShippingId = cartItem.ShippingId;

                AddressModel shippingAddress = cartModel.ShippingAddress ?? GetFromSession<ShoppingCartModel>(AdminConstants.CartModelSessionKey)?.ShippingAddress;

                string GiftCardNumber = cartModel.GiftCardNumber;

                decimal CSRDiscountAmount = cartModel.CSRDiscountAmount;
                ZnodeLogging.LogMessage("CSRDiscountAmount and GiftCardNumber:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info, new { CSRDiscountAmount = CSRDiscountAmount, GiftCardNumber= GiftCardNumber });

                if (cartModel.OmsOrderId > 0)
                    cartModel.IsMerged = true;

                if (IsNull(cartModel.ShippingAddress))
                    cartModel.ShippingAddress = shippingAddress;

                ShoppingCartModel newCartModel = _shoppingCartsClient.CreateCart(cartModel);
                                            
                PortalModel portal = _portalClient.GetPortal(newCartModel.PortalId, new ExpandCollection { ZnodePortalEnum.ZnodeOmsOrderState.ToString().ToLower() });
                if (IsNotNull(newCartModel))
                {
                    if (cartItem.OmsOrderId > 0)
                        GetUpdatedShoppingCart(cartModel, newCartModel, portal);


                    newCartModel.ShippingAddress = shippingAddress;
                    newCartModel.Payment = new PaymentModel() { ShippingAddress = shippingAddress, PaymentSetting = new PaymentSettingModel() };

                    //Set Gift Card Number and CSR Discount Amount Data For Calculation
                    SetCartDataForCalculation(newCartModel, GiftCardNumber, CSRDiscountAmount);

                    newCartModel = _shoppingCartsClient.Calculate(newCartModel);
                }

                // if persistent cart disabled, we need not call below method, need to check with portal record.
                SaveCartInCookie(newCartModel, portal);
                SaveCartInSession(cartItem.OmsOrderId, newCartModel);
                ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

                return cartModel?.ToViewModel<CartViewModel>();
            }
            return new CartViewModel() { HasError = true, ErrorMessage = string.Empty };
        }
        // Add multiple simplr product to cart.
        public virtual CartViewModel AddProductToCart(bool cartItems, int orderId)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ShoppingCartModel cartModel = GetShoppingCartFromSession(orderId);
            SaveLineItemHistorySession(cartModel?.ShoppingCartItems);
            return IsNotNull(cartModel) ? GetCartOrderStatusList(orderId, cartModel) : new CartViewModel();
        }

        // Get Cart method to check Session or Cookie to get the existing shopping cart.
        public virtual CartViewModel GetCart(int omsOrderId = 0)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ShoppingCartModel shoppingCartModel = new ShoppingCartModel();
            if (omsOrderId > 0)
                shoppingCartModel = GetFromSession<OrderModel>(AdminConstants.OMSOrderSessionKey + omsOrderId)?.ShoppingCartModel;
            else
                shoppingCartModel = GetFromSession<ShoppingCartModel>(AdminConstants.CartModelSessionKey) ??
                          GetCartFromCookie();

            if (IsNull(shoppingCartModel))
            {
                return new CartViewModel()
                {
                    HasError = true,
                    ErrorMessage = "Products are out of stock"
                };
            }
            if (shoppingCartModel.ShoppingCartItems.Count == 0)
                return new CartViewModel();

            return shoppingCartModel.ToViewModel<CartViewModel>();
        }

        /// <summary>
        /// Updates the Quantity updated in the shopping cart page.
        /// </summary>
        /// <param name="createOrderViewModel">CreateOrderViewModel</param>
        /// <param name="guid">GUID of the cart item</param>
        /// <param name="quantity">Quantity Selected</param>
        /// <returns>Bool value if the items are updated or not.</returns>
        public virtual CartViewModel UpdateCartItem(string guid, decimal quantity, int productId, int shippingId)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            // Get cart .
            ShoppingCartModel cart = GetFromSession<ShoppingCartModel>(AdminConstants.CartModelSessionKey);
            if (IsNull(cart) || cart.ShoppingCartItems?.Count < 1)
                return new CartViewModel();

            // Check if item exists.
            ShoppingCartItemModel cartItem = cart.ShoppingCartItems.FirstOrDefault(x => x.ExternalId == guid);
            if (IsNull(cartItem))
                return new CartViewModel();

            SetCreatedByUser(cart.UserId);
            string sku = string.Empty;

            sku = productId > 0 ? cartItem.GroupProducts?.Where(x => x.ProductId == productId)?.FirstOrDefault()?.Sku
                                   : !string.IsNullOrEmpty(cartItem.ConfigurableProductSKUs) ? cartItem.ConfigurableProductSKUs : cartItem.SKU;
            ZnodeLogging.LogMessage("sku to get productDetails:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { sku = sku });
            //Get inventory of sku and Check Inventory
            PublishProductsViewModel productDetails = CheckQuantity(sku, cartItem.SKU, quantity, cartItem.AddOnProductSKUs, cart.PortalId, productId);

            if (!productDetails.ShowAddToCart)
                cart.ShoppingCartItems.FirstOrDefault(c => c.ExternalId == guid).InsufficientQuantity = true;

            //Update quantity and update the cart.
            if (productId > 0)
                cart.ShoppingCartItems?.Where(w => w.ExternalId == guid)?.Select(x => x.GroupProducts?.Where(y => y.ProductId == productId)?.Select(z => { z.Quantity = quantity; return z; })?.FirstOrDefault()).ToList();
            else
                cart.ShoppingCartItems.Where(w => w.ExternalId == guid).Select(c => { c.Quantity = quantity; return c; }).ToList();

            if (cart.Shipping?.ShippingId < 1)
                cart.ShippingId = shippingId;

            cart.IsMerged = true;
            ShoppingCartModel shoppingCartModel = _shoppingCartsClient.CreateCart(cart);
            if (IsNotNull(shoppingCartModel))
            {
                shoppingCartModel.ShippingAddress = cart.ShippingAddress;
                shoppingCartModel.Payment = new PaymentModel() { ShippingAddress = shoppingCartModel.ShippingAddress, PaymentSetting = new PaymentSettingModel() };

                //Set Gift Card Number and CSR Discount Amount Data For Calculation
                SetCartDataForCalculation(shoppingCartModel, cart.GiftCardNumber, cart.CSRDiscountAmount);

                shoppingCartModel = _shoppingCartsClient.Calculate(shoppingCartModel);
            }
            SaveInSession(AdminConstants.CartModelSessionKey, shoppingCartModel);
            SaveLineItemHistorySession(shoppingCartModel.ShoppingCartItems);
            return shoppingCartModel?.ToViewModel<CartViewModel>();
        }

        //Remove all the cart items.
        public virtual void RemoveAllCartItems(int orderId = 0)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ShoppingCartModel cart = GetShoppingCartFromSession(orderId);

            if (IsNotNull(cart))
            {
                cart.ShoppingCartItems = new List<ShoppingCartItemModel>();
                cart.GiftCardNumber = string.Empty;
                cart.Coupons = new List<CouponModel>();
                cart.CSRDiscountAmount = 0;
                SaveCartInSession(orderId, cart);
            }
            RemoveInSession(AdminConstants.LineItemHistorySession);
        }

        //Get cart info from cookie.
        public virtual ShoppingCartModel GetCartFromCookie()
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            //Get cookie from current request.
            string cookieValue = GetFromCookie(AdminConstants.CartCookieKey);

            if (!string.IsNullOrEmpty(cookieValue))
            {
                try
                {
                    ShoppingCartModel shoppingCartModel = _shoppingCartsClient.GetShoppingCart(new CartParameterModel
                    {
                        CookieMappingId = cookieValue,
                        LocaleId = Convert.ToInt32(DefaultSettingHelper.DefaultLocale),
                        PortalId = StoreAgent.CurrentStore.PortalId,
                        UserId = null
                    });
                    if (IsNotNull(shoppingCartModel))
                        return shoppingCartModel;
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                    return null;
                }
            }
            return null;
        }

        //To apply & validate Coupon.
        public virtual CartViewModel ApplyCoupon(string couponCode, int orderId = 0)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ShoppingCartModel cart = GetShoppingCartFromSession(orderId);

            if (IsNull(cart))
                return (CartViewModel)GetViewModelWithErrorMessage(new CartViewModel(), string.Empty);

            List<CouponModel> promocode = GetCoupons(cart);
            ZnodeLogging.LogMessage("promocode list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, promocode?.Count());
            List<CouponViewModel> invalidCoupons = new List<CouponViewModel>();

            if (!string.IsNullOrEmpty(couponCode))
                GetNewCouponToApply(cart, couponCode);
            else
                invalidCoupons.Add(new CouponViewModel { Code = couponCode, PromotionMessage = "Coupon Code cannot be empty.", CouponApplied = false, CouponValid = false });

            //Set login user and selected user in cache.
            SetCreatedByUser(cart.UserId);

            //to remove invalid coupon
            RemoveInvalidCoupon(cart);

            //Calculate coupon for the cart.
            ShoppingCartModel shoppingCartModel = _shoppingCartsClient.Calculate(cart);

            SaveCartInSession(orderId, shoppingCartModel);

            //Get order status list in model & map to CartViewModel
            CartViewModel cartViewModel = GetCartOrderStatusList(orderId, shoppingCartModel);

            //return false if invalidCoupons contains invalid coupons.
            if (!IsCouponApplied(cartViewModel, invalidCoupons))
            {
                //Calculate all valid and invlid coupon.
                cartViewModel = CalculateAllCoupons(orderId, cartViewModel, shoppingCartModel, cart, promocode);

                //Add invalid coupons with error coupon message to the cart and display on view.
                foreach (CouponViewModel invalidCoupon in invalidCoupons)
                    cartViewModel.Coupons.Add(invalidCoupon);

                SaveCartInSession(orderId, shoppingCartModel);
            }
            if (orderId > 0)
                cartViewModel.OrderState = GetOrderModelFromSession(orderId)?.OrderState;
            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return cartViewModel;
        }

        //Removes the applied coupon from the cart.
        public virtual CartViewModel RemoveCoupon(string couponCode, int orderId = 0)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ShoppingCartModel cart = GetShoppingCartFromSession(orderId);

            if (IsNull(cart))
                return (CartViewModel)GetViewModelWithErrorMessage(new CartViewModel(), string.Empty);

            OrderModel orderModel = new OrderModel();
            if (orderId > 0)
                orderModel = GetOrderModelFromSession(orderId);

            if (IsNotNull(cart.Coupons))
            {
                StringBuilder sb = new StringBuilder();
                //Remove coupon from cart.
                foreach (CouponModel coupon in cart.Coupons)
                {
                    if (Equals(coupon.Code, couponCode))
                    {
                        if (orderId > 0)
                            sb.AppendFormat("{0},", coupon.Code);

                        cart.Coupons.Remove(coupon);
                        break;
                    }
                }

                string coupons = sb?.ToString();
                ZnodeLogging.LogMessage("coupons:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info,new { coupons = coupons });
                if (!string.IsNullOrEmpty(coupons))
                {
                    SaveOrderModelInSession(orderId, orderModel);
                }
            }

            cart = _shoppingCartsClient.Calculate(cart);

            SaveCartInSession(orderId, cart);

            //Get order status list in model & map to CartViewModel
            CartViewModel cartViewModel = GetCartOrderStatusList(orderId, cart);
            cartViewModel.OrderState = orderModel?.OrderState;
            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return cartViewModel;
        }

        //Apply GiftCard.
        public virtual CartViewModel ApplyGiftCard(string giftCard, int orderId = 0)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ShoppingCartModel cart = GetShoppingCartFromSession(orderId);

            cart.GiftCardNumber = giftCard;

            //Set login user and selected user in cache.
            SetCreatedByUser(cart.UserId);

            ShoppingCartModel shoppingCartModel = _shoppingCartsClient.Calculate(cart);

            SaveCartInSession(orderId, shoppingCartModel);

            //Get order status list in model & map to CartViewModel
            CartViewModel cartViewModel = GetCartOrderStatusList(orderId, shoppingCartModel);

            OrderModel orderModel = new OrderModel();
            if (orderId > 0)
                orderModel = GetOrderModelFromSession(orderId );

            if (cartViewModel.GiftCardApplied)
            {
                cartViewModel.SuccessMessage = Admin_Resources.ValidGiftCard;
                if (orderId > 0)
                {
                    RemoveKeyFromDictionary(orderModel, ZnodeConstant.OrderGiftCard);
                    OrderHistory(orderModel, ZnodeConstant.OrderGiftCard, string.Empty, giftCard);
                    SaveOrderModelInSession(orderId, orderModel);
                }
            }
            else
            {
                if (orderId > 0)
                    SaveOrderModelInSession(orderId, orderModel);

                cartViewModel = (CartViewModel)GetViewModelWithErrorMessage(cartViewModel, Admin_Resources.ErrorGiftCard);
            }
            cartViewModel.OrderState = orderModel?.OrderState;
            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return cartViewModel;
        }

        //Apply Csr Discount.
        public virtual CartViewModel ApplyCsrDiscount(decimal csrDiscount, string csrDesc)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(AdminConstants.CartModelSessionKey);

            if (shoppingCart?.ShoppingCartItems?.Count < 1)
                return new CartViewModel();

            shoppingCart.CSRDiscountAmount = csrDiscount;
            shoppingCart.CSRDiscountDescription = csrDesc;

            //Set login user and selected user in cache.
            SetCreatedByUser(shoppingCart.UserId);

            ShoppingCartModel shoppingCartModel = _shoppingCartsClient.Calculate(shoppingCart);

            SaveInSession(AdminConstants.CartModelSessionKey, shoppingCartModel);

            CartViewModel cartViewModel = shoppingCartModel.ToViewModel<CartViewModel>();

            if (cartViewModel.CSRDiscountApplied)
                cartViewModel.SuccessMessage = cartViewModel.CSRDiscountMessage;
            else
                return (CartViewModel)GetViewModelWithErrorMessage(cartViewModel, Admin_Resources.ErrorGiftCard);

            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return cartViewModel;
        }

        // Removes the items from the shopping cart.
        public virtual CartViewModel RemoveShoppingCartItem(string guid, int orderId = 0)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            // Get cart from Session.
            ShoppingCartModel cart = GetShoppingCartFromSession(orderId);

            if (orderId > 0 && cart.ShoppingCartItems.Count.Equals(1))
            {
                CartViewModel cartViewModel = GetCartOrderStatusList(orderId, cart);
                cartViewModel.HasError = true;
                cartViewModel.ErrorMessage = Admin_Resources.UnableToDeleteSingleItem;
                return cartViewModel;
            }

            if (string.IsNullOrEmpty(guid))
                return GetCartOrderStatusList(orderId, cart);

            SetCreatedByUser(cart.UserId);

            if (cart?.ShoppingCartItems?.Count < 1)
                return new CartViewModel();

            if (cart.ShoppingCartItems.Count.Equals(1))
            {
                //Remove all cart items if shopping cart having only one item.
                _shoppingCartsClient.RemoveAllCartItem(new CartParameterModel { UserId = cart.UserId.GetValueOrDefault(), CookieMappingId = cart.CookieMappingId, OmsOrderId = orderId });

                //Remove all cart from session.
                RemoveAllCartItems();
                return new CartViewModel()
                {
                    UserId = Convert.ToInt32(cart.UserId),
                    LocaleId = cart.LocaleId
                };
            }

            // Check if item exists.
            ShoppingCartItemModel item = cart.ShoppingCartItems.FirstOrDefault(x => x.ExternalId == guid);
            if (IsNull(item))
                return new CartViewModel();

            //to remove child item if deleting the parent product having autoaddon associated with it
            if (!string.IsNullOrEmpty(item.AutoAddonSKUs))
            {
                cart.RemoveAutoAddonSKU = item.AutoAddonSKUs;
                cart.IsParentAutoAddonRemoved = string.Equals(item.SKU.Trim(), item.AutoAddonSKUs.Trim(), StringComparison.OrdinalIgnoreCase) ? false : true;
            }

            // Remove item and update the cart in Session and API.
            cart.ShoppingCartItems.Remove(item);
            //Calculate cart and save in session.
            ShoppingCartModel shoppingCartModel = CartCalculateAndSaveInSession(cart, orderId, true, item);
            if (orderId > 0)
                SaveLineItemHistorySession(shoppingCartModel?.ShoppingCartItems);
            return GetCartOrderStatusList(orderId, shoppingCartModel);
        }
        //Get the group products
        public virtual ShoppingCartItemModel BindGroupProducts(string simpleProduct, string simpleProductQty, CartItemViewModel cartItem, bool isNewExtIdRequired, string productName = "")
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ShoppingCartItemModel shoppingCartItemModel = new ShoppingCartItemModel
            {
                ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId,
                SKU = cartItem.SKU,
                AddOnProductSKUs = cartItem.AddOnProductSKUs,
                AutoAddonSKUs = cartItem.AutoAddonSKUs,
                GroupProducts = new List<AssociatedProductModel> { new AssociatedProductModel { Sku = simpleProduct, Quantity = decimal.Parse(simpleProductQty) } }
            };

            AddProductHistory(cartItem, productName, decimal.Parse(simpleProductQty), simpleProduct);
            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return shoppingCartItemModel;
        }

        // Remove cart item from cart.
        public virtual CartViewModel RemoveItemFromCart(string productIds, int omsOrderId)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ShoppingCartModel cart = GetShoppingCartFromSession(omsOrderId);
            CartViewModel cartViewModel = new CartViewModel();
            if (IsNotNull(cart))
            {
                if (!string.IsNullOrEmpty(productIds))
                {
                    IDictionary<string, Tuple<string, string>> lineItemHistory = GetFromSession<IDictionary<string, Tuple<string, string>>>(AdminConstants.LineItemHistorySession);

                    foreach (string productId in productIds?.Split(',') ?? new string[0])
                    {
                        List<ShoppingCartItemModel> orderlineitems = cart.ShoppingCartItems?.Where(w => w.ParentProductId == Convert.ToInt32(productId)).ToList();
                        List<OrderLineItemDataModel> lineItemList = _shoppingCartsClient.GetOmsLineItemDetails(omsOrderId)?.OrderLineItemDetails;
                        if (orderlineitems.Any())
                        {
                            OrderModel orderModel = GetFromSession<OrderModel>(AdminConstants.OMSOrderSessionKey + omsOrderId);

                            foreach (ShoppingCartItemModel lineitem in orderlineitems)
                            {
                                Tuple<string, string> currentItemHistory = lineItemHistory?.Where(x => x.Value.Item1.ToString() == OrderAgent.GetProductSKU(lineitem)).Select(s => s.Value).FirstOrDefault();
                                if (IsNull(currentItemHistory))
                                {
                                    if (cart.ShoppingCartItems.Count.Equals(1))
                                        RemoveAllCart();
                                    if (lineitem.OmsOrderLineItemsId == 0)
                                        cart.ShoppingCartItems.Remove(lineitem);
                                    else
                                    {
                                        if (lineItemList?.Count > 0)
                                            UpdateLineItemQuantity(cart, lineitem, lineItemList);
                                    }
                                }
                                else
                                {
                                    SetProductQuantity(lineitem, currentItemHistory);
                                    cart.ShoppingCartItems[cart.ShoppingCartItems.FindIndex(x => x.ExternalId == lineitem.ExternalId)] = lineitem;
                                }

                                if (omsOrderId > 0)
                                    RemoveHistoryForCartLineItem(true, lineitem, orderModel);
                            }

                            if (omsOrderId > 0)
                                SaveInSession(AdminConstants.OMSOrderSessionKey + omsOrderId, orderModel);
                            else
                                SaveInSession(AdminConstants.CartModelSessionKey, cart);
                        }
                    }
                }
                cartViewModel = (omsOrderId > 0) ? GetCartOrderStatusList(omsOrderId, CartCalculateAndSaveInSession(cart, omsOrderId)) : CartCalculateAndSaveInSession(cart, omsOrderId)?.ToViewModel<CartViewModel>();
            }
            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return cartViewModel;
        }

        //Update Line Item Quantity to original Quantity
        public virtual void UpdateLineItemQuantity(ShoppingCartModel cart, ShoppingCartItemModel lineitem, List<OrderLineItemDataModel> lineItemList)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNotNull(lineitem.GroupProducts) && lineitem.GroupProducts.Count > 0)
            {
                UpdateGroupProductQuantity(lineitem, cart, lineItemList);
            }
            else if (!string.IsNullOrEmpty(lineitem.ConfigurableProductSKUs))
            {
                UpdateConfigurableProduct(lineitem, cart, lineItemList);
            }
            else
            {
                decimal? lineItemOriginalQuantity = lineItemList.FirstOrDefault(m => m.Sku.Equals(lineitem.SKU)).Quantity;

                cart.ShoppingCartItems.ForEach(m => { if (m.SKU == lineitem.SKU) m.Quantity = lineItemOriginalQuantity ?? 0; });
            }
        }

        //Update Configurable Line Item quantity
        public virtual void UpdateConfigurableProduct(ShoppingCartItemModel lineitem, ShoppingCartModel cart, List<OrderLineItemDataModel> lineItemList)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            decimal lineItemOriginalQuantity = GetConfigurableLineItemQuantity(lineitem, lineItemList);
            ZnodeLogging.LogMessage("parameter lineItemOriginalQuantity:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info,new { lineItemOriginalQuantity = lineItemOriginalQuantity });
            cart.ShoppingCartItems.ForEach(m =>
            {
                if (!string.IsNullOrEmpty(m.ConfigurableProductSKUs) && m.ConfigurableProductSKUs.Equals(lineitem.ConfigurableProductSKUs))
                    m.Quantity = lineItemOriginalQuantity;
            });
        }

        //Get Configurable Line Item quantity
        public virtual decimal GetConfigurableLineItemQuantity(ShoppingCartItemModel lineitem, List<OrderLineItemDataModel> lineItemList)
            => lineItemList.FirstOrDefault(m => m.Sku.Equals(lineitem.ConfigurableProductSKUs))?.Quantity ?? 0;

        //Update Group Product Quantity
        public virtual void UpdateGroupProductQuantity(ShoppingCartItemModel lineitem, ShoppingCartModel cart, List<OrderLineItemDataModel> lineItemList)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            decimal lineItemOriginalQuantity = 0;
            lineitem.GroupProducts.ForEach(m =>
            {
                lineItemOriginalQuantity = GetLineItemOriginalQuantity(lineItemList, m.Sku) ?? 0;
                UpdateCartGroupProductQuantity(cart, lineItemOriginalQuantity, m);
            });
        }

        public virtual decimal? GetLineItemOriginalQuantity(List<OrderLineItemDataModel> lineItemList, string sku)
        => lineItemList.FirstOrDefault(m => m.Sku.Equals(sku))?.Quantity;

        //Update Cart Group Product Quantity
        public virtual void UpdateCartGroupProductQuantity(ShoppingCartModel cart, decimal lineItemOriginalQuantity, AssociatedProductModel groupProduct)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            cart.ShoppingCartItems.ForEach(m =>
            {
                if (m.GroupProducts.Count > 0)
                {
                    m.GroupProducts.ForEach(n =>
                    {
                        if (n.ProductId == groupProduct.ProductId)
                            n.Quantity = lineItemOriginalQuantity;
                    });
                }
            });
        }

        //Remove cart from session.
        public virtual void RemoveCartSession()
            => RemoveInSession(AdminConstants.CartModelSessionKey);

        //Remove All Cart.
        public virtual CartViewModel RemoveAllCart()
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            // Get cart from Session.
            ShoppingCartModel cart = GetShoppingCartFromSession(0);

            if (IsNotNull(cart))
            {
                //Remove all cart items if shopping cart having only one item.
                _shoppingCartsClient.RemoveAllCartItem(new CartParameterModel { UserId = cart.UserId.GetValueOrDefault(), CookieMappingId = cart.CookieMappingId });

                //Remove all cart from session.
                RemoveAllCartItems();
                UserModel userModel = _userClient.GetUserAccountData(Convert.ToInt32(cart.UserId));
                return new CartViewModel()
                {
                    UserId = Convert.ToInt32(cart.UserId),
                    LocaleId = cart.LocaleId,
                    PublishedCatalogId = cart.PublishedCatalogId,
                    PortalId = cart.PortalId,
                    CustomerName= userModel.FullName
                };
            }
            RemoveInSession(AdminConstants.LineItemHistorySession);
            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return new CartViewModel();
        }

        //Get the updated shopping cart model in case of manage Order.
        public ShoppingCartModel GetUpdatedShoppingCart(ShoppingCartModel sessionCart, ShoppingCartModel newCart, PortalModel portal = null)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNotNull(sessionCart) && IsNotNull(newCart))
            {
                foreach (ShoppingCartItemModel newCartItem in newCart.ShoppingCartItems)
                {
                    foreach (ShoppingCartItemModel sessionCartItem in sessionCart.ShoppingCartItems)
                    {
                        //Check if main product sku is same.
                        if (string.Equals(newCartItem.SKU, sessionCartItem.SKU, StringComparison.OrdinalIgnoreCase))
                        {
                            //Check if group product sku is same.
                            if (IsNotNull(newCartItem.GroupProducts) && newCartItem.GroupProducts.Count > 0)
                            {
                                CheckGroupProductSKUs(sessionCartItem, newCartItem, portal);
                            }
                            //Check if configurable product sku is same.
                            else if (!string.IsNullOrEmpty(newCartItem.ConfigurableProductSKUs) || !string.IsNullOrEmpty(sessionCartItem.ConfigurableProductSKUs) && Equals(newCartItem.ConfigurableProductSKUs, sessionCartItem.ConfigurableProductSKUs))
                            {
                                CheckAddonProductSKUs(sessionCartItem, newCartItem, portal);
                                break;
                            }
                            //Check if bundle product sku is same.
                            else if (!string.IsNullOrEmpty(newCartItem.BundleProductSKUs) || !string.IsNullOrEmpty(sessionCartItem.BundleProductSKUs) && Equals(newCartItem.BundleProductSKUs, sessionCartItem.BundleProductSKUs))
                            {
                                CheckAddonProductSKUs(sessionCartItem, newCartItem, portal);
                                break;
                            }
                            else
                            {
                                //Check if add on product sku is same.
                                CheckAddonProductSKUs(sessionCartItem, newCartItem, portal);
                                break;
                            }
                        }
                        if (Equals(newCartItem.SKU, newCartItem.AutoAddonSKUs))
                            SetAdditionalAutoAddOnLineItemDetails(newCartItem, portal);
                    }
                }
            }
            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return newCart;
        }

        //Get the list of associated skus and their quantity for group products.
        public virtual void GetGroupShoppiongCartModel(CartItemViewModel cartItem, ShoppingCartModel shoppingCartModel)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            //Get sku's and quantity of associated group products.
            string[] groupProducts = string.IsNullOrEmpty(cartItem.GroupProductSKUs) ? cartItem.GroupProducts?.Select(x => x.Sku)?.ToArray() : cartItem.GroupProductSKUs?.Split(',');
            string[] groupProductsQuantity = string.IsNullOrEmpty(cartItem.GroupProductsQuantity) ? cartItem.GroupProducts?.Select(x => Convert.ToString(x.Quantity))?.ToArray() : cartItem.GroupProductsQuantity?.Split('_');

            for (int index = 0; index < groupProducts?.Length; index++)
            {
                bool isNewExtIdRequired = false;
                if (!Equals(index, 0))
                    isNewExtIdRequired = true;

                ShoppingCartItemModel cartItemModel = BindGroupProducts(groupProducts[index], groupProductsQuantity[index], cartItem, isNewExtIdRequired);
                if (IsNotNull(cartItemModel))
                    shoppingCartModel.ShoppingCartItems.Add(cartItemModel);
            }
        }

        //Set created by and modified by user.
        public void SetCreatedByUser(int? userId)
        {
            if (userId > 0)
            {
                _shoppingCartsClient.LoginAs = _shoppingCartsClient.UserId;
                _shoppingCartsClient.UserId = userId.GetValueOrDefault();
            }
        }

        // Add product cart line history.
        public void AddProductHistory(CartItemViewModel cartItem, string productName = "", decimal quantity = 0m, string sku = "")
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (cartItem.OmsOrderId > 0)
            {
                ZnodeLogging.LogMessage("Input parameters cartItem having:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsOrderId = cartItem.OmsOrderId });
                OrderModel orderModel = GetFromSession<OrderModel>(AdminConstants.OMSOrderSessionKey + cartItem.OmsOrderId);

                string addedExternalId = string.IsNullOrEmpty(productName) ? cartItem.ExternalId : cartItem.OmsOrderId + productName;
                string addedProductName = string.IsNullOrEmpty(productName) ? cartItem.ProductName : productName;
                decimal addedQuantity = string.IsNullOrEmpty(productName) ? cartItem.Quantity : quantity;
                string addedSKU = string.IsNullOrEmpty(productName) ? cartItem.SKU : sku;
                int lineItemsId = string.IsNullOrEmpty(productName) ? cartItem.OmsOrderLineItemsId : 0;

                OrderLineItemHistoryModel orderLineItemHistoryModel = new OrderLineItemHistoryModel() { OrderLineAdd = addedProductName, Quantity = addedQuantity.ToInventoryRoundOff(), SKU = addedSKU, OmsOrderLineItemsId = lineItemsId, TaxCost = cartItem.TaxCost, SubTotal = cartItem.Total.GetValueOrDefault() };

                RemoveKeyFromDictionary(orderModel, addedExternalId, true);
                OrderLineHistory(orderModel, addedExternalId, orderLineItemHistoryModel);

                SaveInSession(AdminConstants.OMSOrderSessionKey + cartItem.OmsOrderId, orderModel);
            }
        }

        //Get attribute values and code.
        public virtual void PersonalisedItems(CartItemViewModel cartItem)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            string[] Codes = cartItem.PersonalisedCodes?.Split(',');
            string[] Values = cartItem.PersonalisedValues?.Split('`');

            if (Convert.ToBoolean(cartItem.PersonalisedValues?.Contains('`')))
                Values = cartItem.PersonalisedValues?.Split('`');
            else
                Values = cartItem.PersonalisedValues?.Split(',');
            //}
            Dictionary<string, object> SelectedAttributes = new Dictionary<string, object>();
            if (IsNotNull(Values))
            {
                //Add code and value pair
                for (int index = 0; index < Codes.Length; index++)
                    SelectedAttributes.Add(Codes[index], Values[index]);
            }
            cartItem.PersonaliseValuesList = SelectedAttributes;
        }

        #endregion Public methods

        #region Private Methods

        private void SaveLineItemHistorySession(List<ShoppingCartItemModel> ShoppingCartItems)
        {
            ZnodeLogging.LogMessage("ShoppingCartItems list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, ShoppingCartItems?.Count());
            if (ShoppingCartItems.Any())
            {
                IDictionary<string, Tuple<string, string>> lineItemHistory = new Dictionary<string, Tuple<string, string>>();
                foreach (ShoppingCartItemModel orderlineitem in ShoppingCartItems)
                    lineItemHistory.Add(OrderAgent.GetProductSKU(orderlineitem), OrderAgent.GetItemHistory(orderlineitem));
                SaveInSession(AdminConstants.LineItemHistorySession, lineItemHistory);
            }
        }

        private void SetProductQuantity(ShoppingCartItemModel orderlineitem, Tuple<string, string> currentItemHistory)
        {
            //Here, Item1 = SKU, Item2 = Quantity
            if (orderlineitem.GroupProducts.Any())
                orderlineitem.GroupProducts.FirstOrDefault(x => currentItemHistory.Item1.Contains(x.Sku)).Quantity = Convert.ToDecimal(currentItemHistory.Item2);
            else
                orderlineitem.Quantity = Convert.ToDecimal(currentItemHistory.Item2);
        }

        //Check available quantity.
        private PublishProductsViewModel CheckQuantity(string sku, string parentProductSku, decimal quantity, string addOnSkus, int portalId, int parentProductId = 0)
        {
            //Get available quantity of cart item.
            PublishProductsViewModel product = new OrderAgent(GetClient<ShippingClient>(), GetClient<ShippingTypeClient>(), GetClient<StateClient>(), GetClient<CityClient>(), GetClient<ProductsClient>(), GetClient<BrandClient>(),
                GetClient<UserClient>(), GetClient<PortalClient>(), GetClient<AccountClient>(), GetClient<RoleClient>(), GetClient<DomainClient>(), GetClient<OrderClient>(), GetClient<EcommerceCatalogClient>(), GetClient<CustomerClient>(),
                GetClient<PublishProductClient>(), GetClient<MediaConfigurationClient>(), GetClient<PaymentClient>(), GetClient<ShoppingCartClient>(), GetClient<AccountQuoteClient>(), GetClient<OrderStateClient>(), GetClient<PIMAttributeClient>(), GetClient<CountryClient>(), GetClient<AddressClient>()).GetProductPriceAndInventory(sku, parentProductSku, quantity.ToString(), addOnSkus, portalId, parentProductId);

            //check if available quantity against selected quantity.
            //Commenting this method as GetProductPriceAndInventory method already performing the same operation
            //CheckCartQuantity(product, quantity);

            return product;
        }

        //Check quantity for cart item.
        private void CheckCartQuantity(PublishProductsViewModel viewModel, decimal? quantity)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (IsNotNull(viewModel))
            {
                bool AllowBackOrder = false;
                bool TrackInventory = false;
                decimal selectedQuantity = quantity.GetValueOrDefault();

                List<AttributesSelectValuesViewModel> inventorySetting = viewModel.Attributes?.SelectAttributeList(ZnodeConstant.OutOfStockOptions);
                ZnodeLogging.LogMessage("inventorySetting list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, inventorySetting?.Count());

                if (inventorySetting?.Count > 0)
                {
                    OrderAgent.TrackInventoryData(ref AllowBackOrder, ref TrackInventory, inventorySetting.FirstOrDefault().Code);

                    if (viewModel.Quantity < selectedQuantity && !AllowBackOrder && TrackInventory)
                    {
                        viewModel.InventoryMessage = !string.IsNullOrEmpty(viewModel.OutOfStockMessage) ? viewModel.OutOfStockMessage : WebStore_Resources.TextOutofStock;
                        viewModel.ShowAddToCart = false;
                        return;
                    }
                    else if (viewModel.Quantity < selectedQuantity && AllowBackOrder && TrackInventory)
                    {
                        viewModel.InventoryMessage = !string.IsNullOrEmpty(viewModel.BackOrderMessage) ? viewModel.BackOrderMessage : WebStore_Resources.TextBackOrderMessage;
                        viewModel.ShowAddToCart = true;
                        return;
                    }

                    if (!Between(selectedQuantity, Convert.ToDecimal(viewModel.Attributes?.Value(ZnodeConstant.MinimumQuantity)), Convert.ToDecimal(viewModel.Attributes?.Value(ZnodeConstant.MaximumQuantity)), true))
                    {
                        viewModel.InventoryMessage = string.Format(WebStore_Resources.WarningSelectedQuantity, viewModel.Attributes?.Value(ZnodeConstant.MinimumQuantity), viewModel.Attributes?.Value(ZnodeConstant.MaximumQuantity));
                        viewModel.ShowAddToCart = false;
                        return;
                    }
                    viewModel.InventoryMessage = !string.IsNullOrEmpty(viewModel.InStockMessage) ? viewModel.InStockMessage : WebStore_Resources.TextInstock;
                    viewModel.ShowAddToCart = true;
                }
            }
        }

        //Bind dropdown list for all shoppingcart items.
        public virtual CartViewModel GetCartOrderStatusList(int orderId, ShoppingCartModel cartModel)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (orderId > 0)
            {
                if (cartModel?.ShoppingCartItems?.Count > 0)
                {
                    CartViewModel cartViewModel = cartModel?.ToViewModel<CartViewModel>();
                    List<SelectListItem> orderStatusList = BindOrderStatus(new FilterTuple(ZnodeOmsOrderStateEnum.IsOrderLineItemState.ToString().ToLower(), FilterOperators.Equals, "true"));
                    cartViewModel?.ShoppingCartItems?.ForEach(x => x.ShippingStatusList = orderStatusList);
                    return cartViewModel;
                }
            }
            return cartModel?.ToViewModel<CartViewModel>();
        }

        //Bind Order Status dropdown.
        public virtual List<SelectListItem> BindOrderStatus(FilterTuple filter = null)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodeOmsOrderStateEnum.IsAccountStatus.ToString().ToLower(), FilterOperators.Equals, "false"));
            ZnodeLogging.LogMessage("Input parameters filters:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { filters = filters });
            if (IsNotNull(filters)) { filters.Add(filter); }
            return StoreViewModelMap.ToOrderStateList(_orderStateClient.GetOrderStates(null, filters, null, null, null)?.OrderStates);
        }

        private ShoppingCartModel GetShoppingCartFromSession(int orderId)
         => orderId > 0 ? GetFromSession<OrderModel>(AdminConstants.OMSOrderSessionKey + orderId)?.ShoppingCartModel
                        : GetFromSession<ShoppingCartModel>(AdminConstants.CartModelSessionKey)
                        ?? GetCartFromCookie();

        //Check if group product sku is same.
        private void CheckGroupProductSKUs(ShoppingCartItemModel sessionCartItem, ShoppingCartItemModel newCartItem, PortalModel portal)
        {
            string groupProdSKU = newCartItem.GroupProducts.FirstOrDefault().Sku;
            string mainGroupProdSKU = sessionCartItem.GroupProducts.FirstOrDefault().Sku;
            ZnodeLogging.LogMessage("groupProdSKU & mainGroupProdSKU:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { groupProdSKU = groupProdSKU, mainGroupProdSKU = mainGroupProdSKU });
            if (!string.IsNullOrEmpty(groupProdSKU) && !string.IsNullOrEmpty(mainGroupProdSKU) && Equals(groupProdSKU.ToLower(), mainGroupProdSKU.ToLower()))
                CheckAddonProductSKUs(sessionCartItem, newCartItem, portal);
        }

        //Check if add on product sku is same.
        private void CheckAddonProductSKUs(ShoppingCartItemModel sessionCartItem, ShoppingCartItemModel newCartItem, PortalModel portal)
        {
            if (!string.IsNullOrEmpty(newCartItem.AddOnProductSKUs) && !string.IsNullOrEmpty(sessionCartItem.AddOnProductSKUs))
            {
                string[] sessionAddOns = sessionCartItem.AddOnProductSKUs.Split(',');
                string[] newAddOns = newCartItem.AddOnProductSKUs.Split(',');
                for (int index = 0; index < sessionAddOns.Length; index++)
                {
                    if (Equals(sessionAddOns[index], newAddOns[index]))
                        SetAdditionalLineItemDetails(sessionCartItem, newCartItem, portal);
                    else
                        SetAdditionalLineItemDetails(sessionCartItem, newCartItem, portal);
                }
            }
            else
                SetAdditionalLineItemDetails(sessionCartItem, newCartItem, portal);
        }

        //Set custom data for newly create cart items.
        private void SetAdditionalLineItemDetails(ShoppingCartItemModel sessionCartItem, ShoppingCartItemModel newCartItem, PortalModel portal)
        {
            if (sessionCartItem.OmsOrderLineItemsId > 0)
            {
                newCartItem.CustomUnitPrice = sessionCartItem.CustomUnitPrice;
                newCartItem.TrackingNumber = sessionCartItem.TrackingNumber;
                newCartItem.OmsOrderStatusId = sessionCartItem.OmsOrderStatusId;
                newCartItem.IsEditStatus = true;
                newCartItem.IsSendEmail = sessionCartItem.IsSendEmail;
                newCartItem.OrderLineItemStatus = !string.IsNullOrEmpty(sessionCartItem.OrderLineItemStatus) ? sessionCartItem.OrderLineItemStatus : portal?.OrderStatus?.ToUpper();
                newCartItem.OmsOrderLineItemsId = sessionCartItem.OmsOrderLineItemsId;
                newCartItem.ShipSeperately = sessionCartItem.ShipSeperately;
                newCartItem.PartialRefundAmount = sessionCartItem.PartialRefundAmount;
            }
            else
            {
                newCartItem.IsEditStatus = true;
                newCartItem.OrderLineItemStatus = portal?.OrderStatus?.ToUpper() ?? Admin_Resources.TextPendingApproval.ToUpper();
            }
        }

        //Set OrderState, LineItemState etc for newly added cartItem.
        //This method is used only for Manage Order.
        private void SetAdditionalAutoAddOnLineItemDetails(ShoppingCartItemModel newCartItem, PortalModel portal)
        {
            newCartItem.IsEditStatus = true;
            if (IsNotNull(portal))
            {
                newCartItem.OmsOrderStatusId = portal.OrderStatusId;
                newCartItem.OrderLineItemStatus = portal.OrderStatus?.ToUpper();
            }
        }

        //Get the shopping cart items.
        private List<CartItemViewModel> GetShoppingCartItems(CartItemViewModel cartItemViewModel)
        {
            List<CartItemViewModel> cartItems = new List<CartItemViewModel>();
            if (IsNotNull(cartItemViewModel))
            {
                //Check if sku and product is of shopping cart items are null.
                if (!string.IsNullOrEmpty(cartItemViewModel.SKU) && !string.IsNullOrEmpty(cartItemViewModel.PublishProductId))
                {
                    //Check if multiple sku's selected.
                    if (cartItemViewModel.SKU.IndexOf(',') > 0)
                    {
                        //Split multiple skus and add to cartitems.
                        string[] skus = cartItemViewModel.SKU.Split(',');
                        string[] productIds = cartItemViewModel.PublishProductId.Split(',');

                        for (int index = 0; index < skus.Length; index++)
                            cartItems.Add(new CartItemViewModel { SKU = skus[index], PublishProductId = productIds[index], Quantity = cartItemViewModel.Quantity });
                    }
                    else
                        cartItems.Add(new CartItemViewModel { SKU = cartItemViewModel.SKU, PublishProductId = cartItemViewModel.PublishProductId, Quantity = cartItemViewModel.Quantity });
                }
            }
            return cartItems;
        }

        //Get total quantity on hand by sku.
        private decimal GetQuantityOnHandBySku(ShoppingCartItemModel cartItem, int portalId, int productId)
        {
            //Get the sku of product of which quantity needs to update.
            string sku = string.Empty;

            //Get selected sku.
            sku = productId > 0 ? cartItem.GroupProducts?.Where(x => x.ProductId == productId)?.FirstOrDefault()?.Sku
                                      : !string.IsNullOrEmpty(cartItem.ConfigurableProductSKUs) ? cartItem.ConfigurableProductSKUs : cartItem.SKU;

            ProductInventoryPriceListModel productInventory = _publishProductClient.GetProductPriceAndInventory(new ParameterInventoryPriceModel { Parameter = sku, PortalId = portalId });

            decimal quantityOnHand = (productInventory?.ProductList?.Where(w => w.SKU == sku)?.FirstOrDefault().Quantity).GetValueOrDefault();
            return quantityOnHand;
        }

        //Calculate all valid and invlid coupon.
        private CartViewModel CalculateAllCoupons(int orderId, CartViewModel cartViewModel, ShoppingCartModel shoppingCartModel, ShoppingCartModel cart, List<CouponModel> promocode)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            //If cart having invalid coupons then remove all coupons from current cart.
            cart.Coupons.Clear();

            //Add promoCodes(already applied coupon) to cart.
            foreach (CouponModel couponModel in promocode)
                cart.Coupons.Add(couponModel);

            //Set login user and selected user in cache.
            SetCreatedByUser(cart.UserId);

            shoppingCartModel = _shoppingCartsClient.Calculate(cart);

            //Get order status list in model & map to CartViewModel
            cartViewModel = GetCartOrderStatusList(orderId, shoppingCartModel);
            SaveCartInSession(orderId, shoppingCartModel);
            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return cartViewModel;
        }

        public void SaveCartInSession(int orderId, ShoppingCartModel cartModel, bool isFromDeleteCarItem = false, ShoppingCartItemModel shoppingCartItem = null)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (orderId > 0)
            {
                OrderModel orderModel = GetFromSession<OrderModel>(AdminConstants.OMSOrderSessionKey + orderId);

                RemoveHistoryForCartLineItem(isFromDeleteCarItem, shoppingCartItem, orderModel);
                cartModel.ShoppingCartItems?.Where(x => x.OmsOrderLineItemsId < 1)?.Select(y => { y.IsActive = true; return y; })?.ToList();
                orderModel.ShoppingCartModel = cartModel;
                SaveInSession(AdminConstants.OMSOrderSessionKey + orderId, orderModel);
            }
            else
                SaveInSession(AdminConstants.CartModelSessionKey, cartModel);
        }

        // Remove cart line product history.
        private void RemoveHistoryForCartLineItem(bool isFromDeleteCarItem, ShoppingCartItemModel shoppingCartItem, OrderModel orderModel)
        {
            if (isFromDeleteCarItem && IsNotNull(shoppingCartItem))
            {
                OrderLineItemHistoryModel orderLineItemHistoryModel = new OrderLineItemHistoryModel() { OrderLineDelete = shoppingCartItem.ProductName, Quantity = shoppingCartItem.Quantity.ToInventoryRoundOff(), SKU = shoppingCartItem.SKU, OmsOrderLineItemsId = shoppingCartItem.OmsOrderLineItemsId };
                RemoveKeyFromDictionary(orderModel, shoppingCartItem.ExternalId, true);
                OrderLineHistory(orderModel, shoppingCartItem.ExternalId, orderLineItemHistoryModel);
            }
        }

        //Get current coupon list.
        private List<CouponModel> GetCoupons(ShoppingCartModel cartModel)
        {
            //Applied coupons will add to promoCodes list.
            List<CouponModel> promoCodes = new List<CouponModel>();

            if (IsNotNull(cartModel.Coupons))
            {
                //Add to promoCodes when shopping cart having coupons already.
                foreach (CouponModel coupon in cartModel.Coupons)
                {
                    if (coupon.CouponApplied)
                        promoCodes.Add(coupon);
                }
            }
            ZnodeLogging.LogMessage("promoCodes list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, promoCodes?.Count());
            return promoCodes;
        }

        //Get new coupon code and added to cart.
        private void GetNewCouponToApply(ShoppingCartModel cartModel, string couponCode)
        {
            bool isCouponNotAvailableInCart = Equals(cartModel.Coupons.Where(p => Equals(p.Code, couponCode)).ToList().Count, 0);
            if (isCouponNotAvailableInCart)
            {
                CouponModel newCoupon = new CouponModel { Code = couponCode };
                cartModel.Coupons.Add(newCoupon);
            }
        }

        //return false if invalidCoupons contains invalid coupons.
        private bool IsCouponApplied(CartViewModel cartViewModel, List<CouponViewModel> invalidCoupons)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            bool cartCouponsApplied = (invalidCoupons.Count > 0 ? false : true);

            OrderModel orderModel = new OrderModel();
            if (cartViewModel.OmsOrderId > 0)
            {
                orderModel = GetOrderModelFromSession(cartViewModel.OmsOrderId);
                RemoveKeyFromDictionary(orderModel, ZnodeConstant.OrderCoupon);
            }
            StringBuilder sb = new StringBuilder();
            foreach (CouponViewModel couponViewModel in cartViewModel.Coupons)
            {
                if (couponViewModel.CouponApplied)
                {
                    cartCouponsApplied = true;
                    if (cartViewModel.OmsOrderId > 0 && !couponViewModel.IsExistInOrder)
                        sb.AppendFormat("{0},", couponViewModel.Code);
                }
                else
                {
                    cartCouponsApplied = false;
                    invalidCoupons.Add(couponViewModel);
                }
            }
            string coupons = sb.ToString();
            ZnodeLogging.LogMessage("coupons:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info, new { coupons= coupons });
            if (!string.IsNullOrEmpty(coupons) && cartViewModel.OmsOrderId > 0)
            {
                OrderHistory(orderModel, ZnodeConstant.OrderCoupon, string.Empty, coupons.TrimEnd(','));
                SaveOrderModelInSession(cartViewModel.OmsOrderId, orderModel);
            }
            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            return cartCouponsApplied;
        }

        //Add group product to cart items group product list.
        private void AddGroupProductList(ShoppingCartItemModel cartItemModel, string groupProducts, string groupProductsQuantity)
        {
            cartItemModel.GroupProducts.Add(new AssociatedProductModel
            {
                Quantity = Convert.ToDecimal(groupProductsQuantity),
                Sku = groupProducts
            });
        }

        //to remove invalid coupon form cart
        private void RemoveInvalidCoupon(ShoppingCartModel cartModel)
        {
            if (cartModel.Coupons?.Count > 0)
                cartModel.Coupons.RemoveAll(x => !x.CouponApplied && !string.IsNullOrEmpty(x.PromotionMessage));
        }

        //Set Gift Card Number and CSR Discount Amount Data For Calculation
        public static void SetCartDataForCalculation(ShoppingCartModel cartModel, string GiftCardNumber, decimal CSRDiscountAmount)
        {
            cartModel.GiftCardNumber = GiftCardNumber;
            cartModel.CSRDiscountAmount = CSRDiscountAmount;
        }

        // if persistent cart disabled, we need not call below method, need to check with portal record.
        public void SaveCartInCookie(ShoppingCartModel cartModel, PortalModel portal = null)
        {
            if ((IsNull(cartModel.UserDetails) || cartModel.UserDetails.UserId == 0) && (IsNotNull(portal) && IsNotNull(portal.AvailablePortalFeatures)) ? StoreViewModelMap.ToViewModel(portal).PersistentCartEnabled : StoreAgent.CurrentStore.PersistentCartEnabled)
                SaveInCookie(AdminConstants.CartCookieKey, cartModel.CookieMappingId.ToString());
        }

        //Cart Calculate And Save In Session.
        public virtual ShoppingCartModel CartCalculateAndSaveInSession(ShoppingCartModel cart, int orderId = 0, bool isFromDeleteCarItem = false, ShoppingCartItemModel shoppingCartItem = null)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            cart.IsMerged = true;
            //Create new cart.
            ShoppingCartModel shoppingCartModel = _shoppingCartsClient.CreateCart(cart);

            if (orderId > 0)
                shoppingCartModel = GetUpdatedShoppingCart(cart, shoppingCartModel);

            shoppingCartModel.Payment = new PaymentModel() { ShippingAddress = cart.ShippingAddress, PaymentSetting = new PaymentSettingModel() };

            //Set Gift Card Number and CSR Discount Amount Data For Calculation
            SetCartDataForCalculation(shoppingCartModel, cart.GiftCardNumber, cart.CSRDiscountAmount);

            //Calculate shopping cart.
            shoppingCartModel = _shoppingCartsClient.Calculate(shoppingCartModel);

            shoppingCartModel.UserDetails = cart.UserDetails;
            //Add address in shopping cart
            shoppingCartModel.ShippingAddress = cart.ShippingAddress;
            //Save shopping cart in session.
            SaveCartInSession(orderId, shoppingCartModel, isFromDeleteCarItem, shoppingCartItem);
            ZnodeLogging.LogMessage("Agent method executed.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return shoppingCartModel;
        }

        // Remove key from order history.
        private void RemoveKeyFromDictionary(OrderModel orderModel, string key, bool isFromLineItem = false)
        {
            if (IsNotNull(orderModel.OrderHistory) && !isFromLineItem)
            {
                if (orderModel.OrderHistory.ContainsKey(key))
                    orderModel.OrderHistory.Remove(key);
            }
            else
            {
                if (orderModel.OrderLineItemHistory.ContainsKey(key))
                    orderModel.OrderLineItemHistory.Remove(key);
            }
        }

        // Add key and value in order line dictionary.
        private void OrderLineHistory(OrderModel orderModel, string key, OrderLineItemHistoryModel lineHistory) => orderModel.OrderLineItemHistory?.Add(key, lineHistory);

        private OrderModel GetOrderModelFromSession(int orderId) => GetFromSession<OrderModel>(AdminConstants.OMSOrderSessionKey + orderId);

        private void SaveOrderModelInSession(int orderId, OrderModel orderModel) => SaveInSession(AdminConstants.OMSOrderSessionKey + orderId, orderModel);

        // Add key and value in dictionary.
        private void OrderHistory(OrderModel orderModel, string settingType, string oldValue, string newValue = "") => orderModel.OrderHistory?.Add(settingType, newValue);

        #endregion Private Methods
    }
}