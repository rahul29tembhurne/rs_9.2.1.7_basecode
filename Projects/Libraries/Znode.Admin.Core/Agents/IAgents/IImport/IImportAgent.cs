﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Engine.Admin.ViewModels;

namespace Znode.Engine.Admin.Agents
{
    //Interface for Import Agent
    public interface IImportAgent
    {
        /// <summary>
        /// This method will fetch the list of all import types.
        /// </summary>
        /// <returns></returns>
        ImportViewModel BindViewModel();

        /// <summary>
        /// This method process the data from the file for import.
        /// </summary>
        /// <param name="importModel"></param>
        /// <returns>bool</returns>
        bool ImportData(ImportViewModel importModel);

        /// <summary>
        /// This method will get the saved Template list
        /// </summary>
        /// <param name="importHeadId"></param>
        /// <returns></returns>
        List<SelectListItem> GetImportTemplateList(int importHeadId, int familyId);

        /// <summary>
        /// This method will get the mapped template list
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        List<List<SelectListItem>> GetImportTemplateMappingList(int templateId, int importHeadId, int familyId);

        /// <summary>
        /// This method is to get the CSV file headers
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        string GetCsvHeaders(HttpPostedFileBase fileName, out string changedFileName);

        /// <summary>
        /// Download the template
        /// </summary>
        /// <param name="importHeadId">Import Head Id</param>
        /// <param name="response">HttpResponseBase</param>
        ImportViewModel DownloadTemplate(int importHeadId, string importName, int downloadImportFamilyId, HttpResponseBase response);

        /// <summary>
        /// Get the import logs
        /// </summary>
        /// <param name="expands">ExpandCollection</param>
        /// <param name="filters">FilterCollection</param>
        /// <param name="sorts">SortCollection</param>
        /// <param name="pageIndex">PageIndex</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>ImportProcessLogsListViewModel</returns>
        ImportProcessLogsListViewModel GetImportLogs(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int pageIndex, int pageSize);

        /// <summary>
        /// Get the Import Logs status
        /// </summary>
        /// <param name="importProcessLogId">ImportProcessLogId</param>
        /// <param name="expands">ExpandCollection</param>
        /// <param name="filters">FilterCollection</param>
        /// <param name="sorts">SortCollection</param>
        /// <param name="pageIndex">PageIndex</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>ImportProcessLogsListViewModel</returns>
        ImportProcessLogsListViewModel GetImportLogStatus(int importProcessLogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int pageIndex, int pageSize);

        /// <summary>
        /// Get import logs details
        /// </summary>
        /// <param name="importProcessLogId">ImportProcessLogId</param>
        ///  <param name="expands">ExpandCollection</param>
        /// <param name="filters">FilterCollection</param>
        /// <param name="sorts">SortCollection</param>
        /// <param name="pageIndex">PageIndex</param>
        /// <param name="pageSize">PageSize</param>
        /// <returns>ImportLogsListViewModel</returns>
        ImportLogsListViewModel GetImportLogDetails(int importProcessLogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int pageIndex, int pageSize);

        /// <summary>
        /// Delete the Import logs
        /// </summary>
        /// <param name="importPorcessLogIds">ImportPorcessLogIds</param>
        /// <returns>bool</returns>
        bool DeleteLog(string importPorcessLogIds);

        /// <summary>
        /// Get all families for product import
        /// </summary>
        /// <param name="isCategory">isCategory</param>
        /// <returns>List<SelectListItem></returns>
        List<SelectListItem> GetAllFamilies(bool isCategory);

        /// <summary>
        /// Update Mappings
        /// </summary>
        /// <param name="model">ImportViewModel</param>
        /// <returns>bool</returns>
        bool UpdateMappings(ImportViewModel model);

        /// <summary>
        /// Get the import type list
        /// </summary>
        /// <param name="isDynamicReport">isDynamicReport</param>
        /// <returns>List<SelectListItem></returns>
        List<SelectListItem> GetImportTypeList(bool isDynamicReport = false);

        /// <summary>
        /// Get pricing list.
        /// </summary>
        /// <returns>SelectListItem list</returns>
        List<SelectListItem> GetPricingList();

        /// <summary>
        /// Get country list.
        /// </summary>
        /// <returns>SelectListItem country list.</returns>
        List<SelectListItem> GetCountryList();

        /// <summary>
        /// Get Portal list.
        /// </summary>
        /// <returns>SelectListItem Portal list.</returns>
        List<SelectListItem> GetPortalList();

        /// <summary>
        /// Check if the import process is going on or not
        /// </summary>
        /// <returns>bool</returns>
        bool CheckImportProcess();
    }
}
