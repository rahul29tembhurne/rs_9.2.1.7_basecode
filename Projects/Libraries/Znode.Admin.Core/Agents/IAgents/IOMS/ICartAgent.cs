﻿using System.Collections.Generic;
using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Admin.Agents
{
    public interface ICartAgent
    {
        /// <summary>
        /// create new cart.
        /// </summary>
        /// <param name="cartItem">CartItemViewModel</param>
        /// <returns>CartViewModel</returns>
        CartViewModel CreateCart(CartItemViewModel cartItem);

        /// <summary>
        /// Get Cart method to check Session or Cookie to get the existing shopping cart.
        /// </summary>
        /// <param name="omsOrderId">OMS order Id</param>
        /// <returns>Cart View Model</returns>
        CartViewModel GetCart(int omsOrderId = 0);

        /// <summary>
        /// Update selected shopping cart item.
        /// </summary>
        /// <param name="createOrderViewModel">CreateOrderViewModel</param>
        /// <param name="guid">Guid for every cart item.</param>
        /// <param name="quantity">Selected quantity.</param>
        /// <param name="productId">Id of product.</param>
        /// <param name="shippingId">Id of Shipping.</param>
        /// <returns>CartViewModel</returns>
        CartViewModel UpdateCartItem(string guid, decimal quantity, int productId, int shippingId);

        /// <summary>
        /// Remove all cart line Items.
        /// <param name="orderId">Order Id.</param>
        /// </summary>
        void RemoveAllCartItems(int orderId = 0);

        /// <summary>
        /// Get Cart method to check Session or Cookie to get the existing shopping cart.
        /// </summary>
        /// <returns>ShoppingCartModel</returns>
        ShoppingCartModel GetCartFromCookie();

        /// <summary>
        /// Adds the Coupon Code to the Cart.
        /// </summary>
        /// <param name="couponCode">couponCode of the Cart</param>
        /// <param name="orderId">Order Id.</param>
        /// <returns>CartViewModel</returns>
        CartViewModel ApplyCoupon(string couponCode, int orderId = 0);

        /// <summary>
        /// Removes the applied coupon from the cart.
        /// </summary>
        /// <param name="couponCode">Coupon applied to the cart.</param>
        /// <param name="orderId">Order Id.</param>
        /// <returns>Returns cart view model.</returns>
        CartViewModel RemoveCoupon(string couponCode, int orderId = 0);

        /// <summary>
        /// Apply GiftCard.
        /// </summary>
        /// <param name="giftCard">Giftcard number</param>
        /// <param name="orderId">Order Id.</param>
        /// ">Giftcard number</param>
        /// <returns>Returns the updated CartViewModel</returns> 
        CartViewModel ApplyGiftCard(string giftCard, int orderId = 0);

        /// <summary>
        /// Removes the items from the shopping cart.
        /// </summary>
        /// <param name="guid">GUID of the cart item</param>
        /// <param name="orderId">Order Id.</param>
        /// <returns>return CartViewModel if the items are removed or not.</returns>
        CartViewModel RemoveShoppingCartItem(string guid, int orderId = 0);

        /// <summary>
        /// Apply csr discount.
        /// </summary>
        /// <param name="csrDiscount">csr Discount</param>
        /// <param name="csrDesc">csrDesc</param>
        /// <returns>CartViewModel</returns>
        CartViewModel ApplyCsrDiscount(decimal csrDiscount, string csrDesc);

        /// <summary>
        /// Add multiple product to cart
        /// </summary>
        /// <param name="cartItems">bool for multiple cart item.</param>
        /// <param name="orderId">Order Id.</param>
        /// <returns>CartViewModel</returns>
        CartViewModel AddProductToCart(bool cartItems, int orderId);

        /// <summary>
        /// Remove cart items from cart.
        /// </summary>
        /// <param name="productIds">productIds</param>
        /// <returns>CartViewModel</returns>
        CartViewModel RemoveItemFromCart(string productIds, int omsOrderId);

        /// <summary>
        /// Remove cart from session .
        /// </summary>
        void RemoveCartSession();

        /// <summary>
        /// Remove all cart of user from session.
        /// </summary>
        /// <returns>CartViewModel</returns>
        CartViewModel RemoveAllCart();
    }
}