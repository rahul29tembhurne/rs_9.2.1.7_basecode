﻿using System.Collections.Generic;
using Znode.Engine.Api.Client.Sorts;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Admin.Agents
{
    public interface IExportAgent
    {
        /// <summary>
        /// Get List of Area type to be exported .
        /// </summary>
        /// <param name="type">Area type</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sorting Collection</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">PageSize to get list</param>
        /// <param name="folderId">Folder Id for media</param>
        /// <param name="localId">selected Local of entity</param>
        /// <returns>Return List of data of Area Type</returns>
        List<dynamic> GetExportList(string type, FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = null, int? pageSize = null, int folderId = 0, int localId = 0);

        /// <summary>
        /// Method to create a data set that contains column name exactly as in grid.
        /// </summary>
        /// <param name="dataModel">dataModel</param>
        /// <returns>Returns required data set.</returns>
        List<dynamic> CreateDataSource(List<dynamic> dataModel);
    }
}
