﻿using System.Collections.Generic;
using Znode.Engine.Admin.Models;

namespace Znode.Engine.Admin.Agents
{
    public interface ITypeaheadAgent
    {
        /// <summary>
        ///Get the suggestions of typeahead.
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <param name="searchtype"></param>
        /// <param name="fieldname"></param>
        /// <returns>Returns suggestions list.</returns>
        List<AutoComplete> GetAutocompleteList(string searchTerm, string searchtype, string fieldname);
    }
}
