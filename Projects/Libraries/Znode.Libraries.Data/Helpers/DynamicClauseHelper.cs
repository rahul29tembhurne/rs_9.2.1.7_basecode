﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Znode.Libraries.Data.Helpers
{
    public static class DynamicClauseHelper
    {
        static int placeLocator;
        static DateTime filterValueinDateTime;

        #region Public Methods

        #region Clauses for Entity Framework

        //To generate dynamic where clause for Entity Framework with filterValue.
        public static EntityWhereClauseModel GenerateDynamicWhereClauseWithFilter(FilterDataCollection filters)
        {
            placeLocator = 0;
            string whereClause = string.Empty;
            object[] filterClause = new object[] { };
            List<object> filterList = new List<object> { };

            if (filters?.Count > 0)
            {
                foreach (var tuple in filters)
                {
                    var filterKey = tuple.Item1;
                    var filterOperator = tuple.Item2;
                    var filterValue = tuple.Item3;

                    if (filterValue.IndexOf("''") >= 0)
                        filterValue = filterValue.Replace("''", "'");

                    //If filterkey contains | then seprate the filter key by | and generate or query.
                    if (filterKey.Contains("|"))
                    {
                        string whereClauseWithORClause = string.Empty;

                        foreach (string item in filterKey.Split('|'))
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                whereClauseWithORClause = (string.IsNullOrEmpty(whereClauseWithORClause))
                                    ? SetQueryParameter(item, filterOperator, filterValue, filterList)
                                    : $"{whereClauseWithORClause} {StoredProcedureKeys.OR} {SetQueryParameter(item, filterOperator, filterValue, filterList)}";
                                filterClause = filterList?.ToArray();
                            }
                        }

                        whereClause = string.IsNullOrEmpty(whereClause) ? $" ({whereClauseWithORClause})" : $" {whereClause} {StoredProcedureKeys.AND} ({whereClauseWithORClause})";
                    }
                    else
                    {
                        whereClause = (string.IsNullOrEmpty(whereClause))
                            ? SetQueryParameter(filterKey, filterOperator, filterValue, filterList)
                            : $"{whereClause} {StoredProcedureKeys.AND} {SetQueryParameter(filterKey, filterOperator, filterValue, filterList)}";
                        filterClause = filterList?.ToArray();
                    }
                }
            }
            return new EntityWhereClauseModel() { WhereClause = whereClause, FilterValues = filterClause };
        }

        //To generate dynamic order by clause for Entity Framework.-
        public static string GenerateDynamicOrderByClause(NameValueCollection sorts)
        {
            string orderBy = string.Empty;
            if (sorts?.Count > 0 && sorts.HasKeys())
            {
                foreach (string key in sorts.AllKeys)
                {
                    if (!string.IsNullOrWhiteSpace(key))
                    {
                        orderBy = orderBy+ (!string.IsNullOrWhiteSpace(orderBy)? ",":"") + $"{key} {sorts.Get(key)}";
                    }
                }
            }
            return orderBy;
        }

        #endregion

        #region Clauses for SP
        //To generate query based on Input filter parameters
        public static string GenerateDynamicWhereClause(FilterDataCollection filters)
        {
            string whereClause = string.Empty;
            if (filters?.Count > 0)
            {
                foreach (var tuple in filters)
                {
                    var filterKey = tuple.Item1;
                    var filterOperator = tuple.Item2;
                    var filterValue = tuple.Item3;
                    whereClause = (string.IsNullOrEmpty(whereClause))
                        ? SetQueryParameter(filterKey, filterOperator, filterValue, null)
                        : $"{whereClause} {StoredProcedureKeys.AND} {SetQueryParameter(filterKey, filterOperator, filterValue, null)}";
                }
            }
            return whereClause;
        }

        /// <summary>
        /// To Generate dynamic where clause for stored procedure.
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <returns>Return the where clause string.</returns>
        public static string GenerateDynamicWhereClauseForSP(FilterDataCollection filters)
        {
            string whereClause = string.Empty;

            if (filters?.Count > 0)
            {
                foreach (var tuple in filters)
                {
                    var filterKey = tuple.Item1;
                    var filterOperator = tuple.Item2;
                    var filterValue = tuple.Item3;
                    if (!Equals(filterKey, "_") && !Equals(filterKey.ToLower(), "usertype"))
                        whereClause = (string.IsNullOrEmpty(whereClause))
                            ? SetQueryParameterForSP(filterKey, filterOperator, filterValue)
                            : $"{whereClause} {StoredProcedureKeys.AND} {SetQueryParameterForSP(filterKey, filterOperator, filterValue)}";
                }
            }
            return whereClause;
        }

        /// <summary>
        /// To Generate dynamic where clause for stored procedure.
        /// </summary>
        /// <param name="filter">FilterDataTuple</param>
        /// <returns>Return the where clause string.</returns>
        public static string GenerateWhereClauseForSP(FilterDataTuple filter)
        {
            string whereClause = string.Empty;
            var filterKey = filter.Item1;
            var filterOperator = filter.Item2;
            var filterValue = filter.Item3;

            if (filter.FilterName.Contains("|"))
            {
                string quammaSepratedAttributeCode = filterKey.Replace("|", "','");
                whereClause = $"attributecode in ('{quammaSepratedAttributeCode}') and attributevalue {SetQueryParameterForSP(string.Empty, filterOperator, filterValue)}";

            }
            else if (!Equals(filterKey, "_") && !Equals(filterKey.ToLower(), "usertype"))
                whereClause = $"attributecode = '{filterKey }' and attributevalue {SetQueryParameterForSP(string.Empty, filterOperator, filterValue)}";

            return whereClause;
        }


        public static string GenerateWhereClauseForSP(FilterDataCollection filters)
        {
            if (filters.Count > 0)
            {
                List<string> _where = new List<string>();

                string whereClause = "<ArrayOfWhereClauseModel>";
                foreach (var filter in filters)
                {
                    whereClause += "<WhereClauseModel>";
                    var filterKey = filter.Item1;
                    var filterOperator = filter.Item2;
                    var filterValue = filter.Item3;

                    ReplaceSpecialCharacters(ref filterValue);
                    if (filter.FilterName.Contains("|"))
                    {
                        string quammaSepratedAttributeCode = filterKey.Replace("|", "','");
                        whereClause += $"<attributecode> in ('{quammaSepratedAttributeCode}') </attributecode>";
                        whereClause += $"<attributevalue> {SetQueryParameterForSP(string.Empty, filterOperator, filterValue)} </attributevalue>";

                    }
                    else if (!Equals(filterKey, "_") && !Equals(filterKey.ToLower(), "usertype"))
                    {
                        whereClause += $"<attributecode> = '{filterKey }' </attributecode>";
                        whereClause += $"<attributevalue> {SetQueryParameterForSP(string.Empty, filterOperator, filterValue)}</attributevalue>";

                    }

                    whereClause += "</WhereClauseModel>";
                }
                whereClause += "</ArrayOfWhereClauseModel>";

                return whereClause;
            }
            return string.Empty;
        }

        //Replace the special character with their escaped equivalents.
        private static void ReplaceSpecialCharacters(ref string filterValue)
        {
            if (filterValue.Contains("&"))
            {
                filterValue = filterValue.Replace("&", "&amp;");
            }
            else if (filterValue.Contains("<"))
            {
                filterValue = filterValue.Replace("<", "&lt;");
            }
            else if (filterValue.Contains(">"))
            {
                filterValue = filterValue.Replace(">", "&gt;");
            }
            else if (filterValue.Contains("\""))
            {
                filterValue = filterValue.Replace("\"", "&quot;");
            }
            else if (filterValue.Contains("\'"))
            {
                filterValue = filterValue.Replace("\'", "&apos;");
            }
        }

        /// <summary>
        /// To generate the where clause for custom reports
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public static string GenerateReportsWhereClauseForSP(FilterDataTuple filter)
        {
            string whereClause = string.Empty;
            var filterKey = filter.Item1;
            var filterOperator = filter.Item2;
            var filterValue = filter.Item3;
            whereClause = SetQueryParameterForSP(filterKey, filterOperator, filterValue);
            return whereClause;
        }

        #endregion

        #endregion

        #region Helper Methods

        //Returns query based on Input filter parameters
        private static string SetQueryParameter(string filterKey, string filterOperator, string filterValue, List<object> filterList)
        {
            string query = string.Empty;
            if (Equals(filterOperator, ProcedureFilterOperators.Equals)) return GenerateQuery(filterKey, " = ", filterValue, filterList);
            if (Equals(filterOperator, ProcedureFilterOperators.Like)) return GenerateContainsQuery(filterKey, "Contains", filterValue, filterList);
            if (Equals(filterOperator, ProcedureFilterOperators.GreaterThan)) return GenerateQuery(filterKey, " > ", filterValue, filterList);
            if (Equals(filterOperator, ProcedureFilterOperators.GreaterThanOrEqual)) return GenerateQuery(filterKey, " >= ", filterValue, filterList);
            if (Equals(filterOperator, ProcedureFilterOperators.LessThan)) return GenerateQuery(filterKey, " < ", filterValue, filterList);
            if (Equals(filterOperator, ProcedureFilterOperators.LessThanOrEqual)) return GenerateQuery(filterKey, " <= ", filterValue, filterList);
            if (Equals(filterOperator, ProcedureFilterOperators.Contains)) return GenerateContainsQuery(filterKey, "Contains", filterValue, filterList);
            if (Equals(filterOperator, ProcedureFilterOperators.StartsWith)) return GenerateContainsQuery(filterKey, "StartsWith", filterValue, filterList);
            if (Equals(filterOperator, ProcedureFilterOperators.EndsWith)) return GenerateContainsQuery(filterKey, "EndsWith", filterValue, filterList);
            if (Equals(filterOperator, ProcedureFilterOperators.Is)) return GenerateContainsQuery(filterKey, "Equals", filterValue, filterList);
            if (Equals(filterOperator, ProcedureFilterOperators.In)) return GenerateInQuery(filterKey, "in", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.NotIn)) return GenerateNotInQuery(filterKey, "not in", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.NotEquals)) return GenerateQuery(filterKey, " != ", filterValue, filterList);
            if (Equals(filterOperator, ProcedureFilterOperators.Or)) return GenerateOrQuery(filterKey, "or", filterValue, filterList);  //only for string in
            if (Equals(filterOperator, ProcedureFilterOperators.NotContains)) return GenerateNotContainsQuery(filterKey, "Contains", filterValue, filterList);
            if (Equals(filterOperator, ProcedureFilterOperators.Between)) return GenerateDateBetweenQuery(filterKey, " Between ", filterValue, filterList);
            return query;
        }

        //Returns query based on Input filter parameters for SP.
        private static string SetQueryParameterForSP(string filterKey, string filterOperator, string filterValue)
        {
            string query = string.Empty;
            if (Equals(filterOperator, ProcedureFilterOperators.Equals)) return AppendQuery(filterKey, " = ", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.Like)) return AppendLikeQuery(filterKey, " like ", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.GreaterThan)) return AppendQuery(filterKey, " > ", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.GreaterThanOrEqual)) return AppendQuery(filterKey, " >= ", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.LessThan)) return AppendQuery(filterKey, " < ", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.LessThanOrEqual)) return AppendQuery(filterKey, " <= ", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.Contains)) return AppendLikeQuery(filterKey, " like ", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.StartsWith)) return GenerateStartwithQuery(filterKey, " like ", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.EndsWith)) return GenerateEndwithQuery(filterKey, " like ", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.Is)) return GenerateIsquery(filterKey, " = ", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.NotIn)) return GenerateNotInQuery(filterKey, "not in", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.In)) return GenerateSPInQuery(filterKey, "in", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.NotEquals)) return GenerateNotaEquals(filterKey, "!=", filterValue);
            if (Equals(filterOperator, ProcedureFilterOperators.Between)) return AppendQuery(filterKey, " between ", filterValue);

            return query;
        }

        private static string GenerateIsquery(string filterKey, string filterOperator, string filterValue)
            => string.Format("{0}{1}{2}{3}'{4}'", StoredProcedureKeys.TildOperator, filterKey, StoredProcedureKeys.TildOperator, filterOperator, filterValue);

        // Method Return the genrated query.
        private static string AppendQuery(string filterKey, string filterOperator, string filterValue)
        {
            //This Special Case required to Get the Franchisable Product in which Product belongs to Portals & all product having franchisable flat true will comes.
            if (filterKey.Contains("$")) return AppendSpecialOrClauseToQuery(filterKey, filterOperator, filterValue);
            //To generate OR condition with null value for the filter key passed.
            if (filterKey.Contains("!")) return AppendNullOrClauseToQuery(filterKey, filterOperator, filterValue, null);
            return $"{StoredProcedureKeys.TildOperator}{filterKey}{ StoredProcedureKeys.TildOperator}{filterOperator}{filterValue}";
        }
        //Method return the generated query for not equal.
        private static string GenerateNotaEquals(string filterKey, string filterOperator, string filterValue)
        {
            //This Special Case required to Get the Franchisable Product in which Product belongs to Portals & all product having franchisable flat true will comes.
            if (filterKey.Contains("$")) return AppendSpecialOrClauseToQuery(filterKey, filterOperator, filterValue);
            //To generate OR condition with null value for the filter key passed.
            if (filterKey.Contains("!")) return AppendNullOrClauseToQuery(filterKey, filterOperator, filterValue, null);
            return string.Format("{0}{1}{2}{3}{4}'{5}'", StoredProcedureKeys.TildOperator, filterKey, " ", filterOperator, " ", filterValue);
        }
        //Method return the generated query for special case. forex "like"
        private static string AppendLikeQuery(string filterKey, string filterOperator, string filterValue)
        {
            if (filterKey.Contains("|")) return AppendOrClauseToQuery(filterKey, filterOperator, filterValue);
            return GenerateLikeQuery(filterKey, filterOperator, filterValue);
        }

        //Method return the generated query for special case. forex "like"
        private static string GenerateLikeQuery(string filterKey, string filterOperator, string filterValue)
            => $"{StoredProcedureKeys.TildOperator}{filterKey}{StoredProcedureKeys.TildOperator}{filterOperator}'%{filterValue}%'";

        //Method return the generated query for special case. forex "startswith"
        private static string GenerateStartwithQuery(string filterKey, string filterOperator, string filterValue)
            => $"{StoredProcedureKeys.TildOperator}{filterKey}{StoredProcedureKeys.TildOperator}{filterOperator}'{filterValue}%'";

        //Method return the generated query for special case. forex "endswith"
        private static string GenerateEndwithQuery(string filterKey, string filterOperator, string filterValue)
            => $"{StoredProcedureKeys.TildOperator}{filterKey}{StoredProcedureKeys.TildOperator}{filterOperator}'%{filterValue}'";

        //Returns query generated using filterKey, filterOperator, filterValue
        private static string GenerateQuery(string filterKey, string filterOperator, string filterValue, List<object> filterList)
        {
            //To generate OR condition with null value for the filter key passed.
            if (filterKey.Contains("!")) return AppendNullOrClauseToQuery(filterKey, filterOperator, filterValue, filterList);

            if (filterValue.Contains("'"))
                filterValue = filterValue.Replace("'", string.Empty).ToString();

            //Check if filter value is date time then generate date time query else generate query using filterKey, filterOperator, filterValue.
            if (IsDateTime(filterValue, out filterValueinDateTime))
            {
                string query = $"{filterKey}{filterOperator}(@{placeLocator})";
                if (!Equals(filterList, null))
                {
                    filterList.Add(filterValueinDateTime);
                    placeLocator++;
                }
                return query;
            }
            else
                return $"{filterKey}{filterOperator}{filterValue}";
        }

        //Returns query generated for between using filterKey, filterOperator, filterValue for date filters.
        private static string GenerateDateBetweenQuery(string filterKey, string filterOperator, string filterValue, List<object> filterList)
        {
            //This Special Case when date filter with BETWEEN used
            if (filterValue.Contains("'"))
                filterValue = filterValue.Replace("'", string.Empty).ToString();

            string query = string.Empty;
            int index = 0;
            if (filterValue.Contains("and"))
            {
                string[] dateFilterArray = filterValue.Split(new string[] { "and" }, StringSplitOptions.None);
                foreach (var dateItem in dateFilterArray)
                {
                    if (IsDateTime(dateItem, out filterValueinDateTime))
                    {
                        query = index == 0 ? $"{filterKey} >= (@{placeLocator})" : $"{query} and {filterKey} <= (@{ placeLocator})";
                        if (!Equals(filterList, null))
                        {
                            filterList.Add(filterValueinDateTime);
                            placeLocator++;
                            index++;
                        }
                    }
                }
            }
            return query;
        }

        //Returns query generated for 'like/contains/starts with/ends with' filter operator
        private static string GenerateContainsQuery(string filterKey, string filterOperator, string filterValue, List<object> filterList)
        {
            string query = $"{filterKey}.{filterOperator}(@{placeLocator})";
            if (!Equals(filterList, null))
            {
                filterList.Add(filterValue);
                placeLocator++;
            }
            return query;
        }

        //Returns query generated for 'not contains' filter operator.
        private static string GenerateNotContainsQuery(string filterKey, string filterOperator, string filterValue, List<object> filterList)
        {
            string query = $"!{filterKey}.{filterOperator}(@{placeLocator})";
            if (!Equals(filterList, null))
            {
                filterList.Add(filterValue);
                placeLocator++;
            }
            return query;
        }

        //Returns query generated using filterKey, filterOperator, filterValue for special case. forex "isNull"
        private static string GenerateIsQuery(string filterKey, string filterOperator, string filterValue)
            => $"{filterKey} == {filterValue}";

        //Returns query generated using filterKey, filterOperator, filterValue for special case. forex "in"
        private static string GenerateInQuery(string filterKey, string filterOperator, string filterValue)
        {
            string[] filterArray = filterValue.Split(',');
            string filterClause = string.Empty;
            for (int index = 0; index < filterArray.Length; index++)
            {
                string filter = $" or {filterKey} = {filterArray[index]}";
                filterClause += filter;
            }
            filterClause = filterClause.Remove(0, 4);

            if (!string.IsNullOrEmpty(filterClause))
                filterClause = string.Format("({0})", filterClause);

            return filterClause;
        }


        private static string GenerateSPInQuery(string filterKey, string filterOperator, string filterValue)
        {
            string[] filterArray = filterValue.Split(',');
            string filterValues = string.Empty;

            string filterClause = $"{filterKey} in";

            for (int index = 0; index < filterArray.Length; index++)
                filterValues += string.IsNullOrEmpty(filterValues) ? $"'{filterArray[index]}'" : $",'{filterArray[index]}'";

            if (!string.IsNullOrEmpty(filterValues))
                filterClause = string.Format("({0}({1}))", filterClause, filterValues);
            else
                filterClause = string.Empty;

            return filterClause;
        }

        //Returns query generated using filterKey, filterOperator, filterValue for special case. forex "not in"
        private static string GenerateNotInQuery(string filterKey, string filterOperator, string filterValue)
        {
            string[] filterArray = filterValue.Split(',');
            string filterClause = string.Empty;
            for (int index = 0; index < filterArray.Length; index++)
            {
                string filter = $" and {filterKey} != {filterArray[index]}";
                filterClause += filter;
            }
            filterClause = filterClause.Remove(0, 4);

            return filterClause;
        }

        //Returns query generated using filterKey, filterOperator, filterValue for in condition in string. forex "or"
        private static string GenerateOrQuery(string filterKey, string filterOperator, string filterValue, List<object> filterList)
        {
            string[] filterArray = filterValue.Split(',');
            string filterClause = string.Empty;
            for (int index = 0; index < filterArray.Length; index++)
            {
                string filter = $" or {filterKey} = @{index}";
                filterClause += filter;
            }
            filterClause = filterClause.Remove(0, 4);

            foreach (var filter in filterValue.Split(','))
                filterList.Add(filter);

            return filterClause;
        }

        //Appends 'Or' condition to query
        private static string AppendOrClauseToQuery(string filterKey, string filterOperator, string filterValue)
        {
            string strQuery = string.Empty;
            if (filterKey.Contains("|"))
            {
                string innerQuery = string.Empty;
                string[] strSplit = filterKey.Split('|');
                foreach (string item in filterKey.Split('|'))
                {
                    if (!string.IsNullOrEmpty(item))
                        innerQuery = (string.IsNullOrEmpty(innerQuery)) ? GenerateLikeQuery(item, filterOperator, filterValue) : innerQuery + " OR " + GenerateLikeQuery(item, filterOperator, filterValue);
                }
                strQuery = (string.IsNullOrEmpty(innerQuery)) ? string.Empty : string.Format("({0})", innerQuery);
            }
            return strQuery;
        }

        //Appends 'Or' condition with null value for the filter key passed with "$" separator.
        private static string AppendSpecialOrClauseToQuery(string filterKey, string filterOperator, string filterValue)
        {
            string[] strSplit = filterKey.Split('$');
            return $"({StoredProcedureKeys.TildOperator}{strSplit[0]}{ StoredProcedureKeys.TildOperator}={filterValue} OR ({StoredProcedureKeys.TildOperator}{strSplit[0]}{StoredProcedureKeys.TildOperator} is null and { StoredProcedureKeys.TildOperator}{strSplit[1]}{StoredProcedureKeys.TildOperator} = 1 ))";
        }

        //Appends 'Or' condition with null value for the filter key passed with "|" separator.
        private static string AppendNullOrClauseToQuery(string filterKey, string filterOperator, string filterValue, List<object> filterList)
        {
            string[] strSplit = filterKey.Split('!');
            if (IsDateTime(filterValue, out filterValueinDateTime))
            {
                string query = $"{strSplit[0]}{filterOperator}(@{placeLocator})";
                if (!Equals(filterList, null))
                {
                    filterList.Add(filterValueinDateTime);
                    placeLocator++;
                }
                return $"({query} || {strSplit[0]} = null)";
            }
            else
                return $"({StoredProcedureKeys.TildOperator}{strSplit[0]}{StoredProcedureKeys.TildOperator}{filterOperator}{filterValue} OR {StoredProcedureKeys.TildOperator}{ strSplit[0]}{StoredProcedureKeys.TildOperator} Is NULL)";
        }

        // Check Whether String value is a DateTime or not.
        private static bool IsDateTime(string filterValue, out DateTime filterValueInDateTime) => DateTime.TryParse(filterValue, out filterValueInDateTime);

        #endregion
    }
}
