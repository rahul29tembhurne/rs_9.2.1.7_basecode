﻿using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Libraries.Admin
{
    public class SearchHelper
    {
        private readonly IZnodeRepository<ZnodeSearchIndexServerStatu> _searchIndexServerStatusRepository;

        public SearchHelper()
        {
            _searchIndexServerStatusRepository = new ZnodeRepository<ZnodeSearchIndexServerStatu>();
        }

        //Creates search index server status.
        public virtual SearchIndexServerStatusModel CreateSearchIndexServerStatus(SearchIndexServerStatusModel searchIndexStatusModel)
        {
            if (HelperUtility.IsNotNull(searchIndexStatusModel))
                searchIndexStatusModel = _searchIndexServerStatusRepository.Insert(searchIndexStatusModel.ToEntity<ZnodeSearchIndexServerStatu>()).ToModel<SearchIndexServerStatusModel>();

            return searchIndexStatusModel;
        }

        //Updates search index server status.
        public virtual bool UpdateSearchIndexServerStatus(SearchIndexServerStatusModel searchIndexStatusModel)
        {
            if (searchIndexStatusModel?.SearchIndexServerStatusId > 0)
                return _searchIndexServerStatusRepository.Update(searchIndexStatusModel.ToEntity<ZnodeSearchIndexServerStatu>());

            return false;
        }
    }
}
