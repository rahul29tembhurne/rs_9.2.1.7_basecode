﻿using System;
using System.Linq;

namespace Znode.Libraries.Observer
{
    public sealed class Initializer<TModel> : IDisposable
    {
        private bool isDisposed;
        public Initializer(TModel model)
        {
            EventAggregator eve = new EventAggregator();

            var _assembly = AppDomain.CurrentDomain.GetAssemblies().
                            SingleOrDefault(assembly => assembly.GetName().Name == "Znode.Engine.Connector");

            Type objectType = (from type in _assembly.GetTypes()
                               where type.IsClass && type.Name == "GlobalConnector"
                               select type).Single();


            Activator.CreateInstance(objectType, eve);

            eve.Submit(model);
        }


        ~Initializer()
        {
            if (!isDisposed)
                Dispose();
        }

        public void Dispose()
        {

            isDisposed = true;
        }

    }
}
