﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Libraries.Search;

namespace Znode.Libraries.ElasticSearch
{

    // Implementation to get type ahead feature.   
    public class ElasticSuggestions : ElasticSearchBase
    {
        public IZnodeSearchResponse SuggestTermsFromExistingIndex(IZnodeSearchRequest request)
        {
            request.PageSize = 100;
            request.StartIndex = 0;
            List<QueryContainer> catalogIdLocaleIdContainerList = GetDefaultFilters(request);

            //Checks if the index is present or not.
            if (string.IsNullOrEmpty(request.IndexName))
                throw new Exception("Index name cannot be blank.");
            if (!ElasticSearchClient.IndexExists(request.IndexName).Exists)
                throw new Exception("Search index does not exist.");
            ISearchResponse<dynamic> suggestions = GetAutoSuggestions(request, catalogIdLocaleIdContainerList);
            // Get Suggestion by suggested term if no document is returned
            if (suggestions.Documents.Count == 0 && suggestions.Suggest?.Values?.FirstOrDefault()?.FirstOrDefault().Options.Count > 0)
                suggestions = GetSuggestionBySuggestedTerm(request, catalogIdLocaleIdContainerList, suggestions);

            return ElasticProductMapper.MapSuggestionResponse(suggestions, request.HighlightFieldName, request);
        }

        private ISearchResponse<dynamic> GetSuggestionBySuggestedTerm(IZnodeSearchRequest request, List<QueryContainer> catalogIdLocaleIdContainerList, ISearchResponse<dynamic> suggestions)
        {
            //Nivi Code
            string suggestedTerm = suggestions.Suggest?.Values?.FirstOrDefault()?.FirstOrDefault().Options.OrderByDescending(m => m.Score)?.FirstOrDefault().Text;
            suggestions = ElasticSearchClient
            .Search<dynamic>(s => s.Index(request.IndexName).Type(ElasticLibraryConstants.ElasticProductIndexType)
            .Query(q => (q.QueryString(c => c
            .Fields("productname.autocomplete")
            .Query(suggestedTerm)) &
             q.Bool(b => b.Must(catalogIdLocaleIdContainerList.ToArray()))) ||
             (q.Match(c => c.Field("sku.autocomplete")
             .Query(suggestedTerm)) & q.Bool(b => b.Must(catalogIdLocaleIdContainerList.ToArray())))
             ));
            return suggestions;
        }

        private ISearchResponse<dynamic> GetAutoSuggestions(IZnodeSearchRequest request, List<QueryContainer> catalogIdLocaleIdContainerList)
        {
            //Nivi Code
            return ElasticSearchClient
                .Search<dynamic>(s => s.Index(request.IndexName).Type(ElasticLibraryConstants.ElasticProductIndexType)
                .Query(q => (q.QueryString(c => c.Fields("productname.autocomplete")
                .Query(request.SearchText).Boost(900))
                & q.Bool(b => b.Must(catalogIdLocaleIdContainerList.ToArray()))) ||
                (q.Match(c => c.Field("sku.autocomplete")
                .Query(request.SearchText)) & q.Bool(b => b.Must(catalogIdLocaleIdContainerList.ToArray()))))
                .Suggest(ss => ss.Phrase("my-suggestions", p => p.Field("didyoumean").Text(request.SearchText))));
        }

        //Gets default filters for catalog, locale ID, etc.
        private List<QueryContainer> GetDefaultFilters(IZnodeSearchRequest request)
        {
            List<QueryContainer> catalogIdLocaleIdContainerList = new List<QueryContainer>();
            // Loop to add catalogId, locaelId and categoryId 
            foreach (var andItem in request.CatalogIdLocalIdDictionary ?? new Dictionary<string, List<string>>())
            {
                foreach (var item in andItem.Value)
                    // Added item to list to create "AND" query.
                    catalogIdLocaleIdContainerList.Add(GetTermQuery(andItem.Key.ToString(), item));
            }

            return catalogIdLocaleIdContainerList;
        }

        //Gets the term query.
        private QueryContainer GetTermQuery(string andItem, string itemValue, double boost = 0.0, bool isSortEnabled = false)
        {
            boost = isSortEnabled ? boost : 0.0;
            return new QueryContainerDescriptor<SearchProduct>().Term(s => s.Field(andItem).Value(itemValue.ToLower()).Boost(boost));
        }
    }
}