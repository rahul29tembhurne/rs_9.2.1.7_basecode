﻿using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.Admin;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Search;

namespace Znode.Libraries.ElasticSearch
{
    public class ElasticSearchIndexer
    {
        #region Public Properties

        public ISearchCategoryService CategoryService { get; set; }

        public int PageLength { get; set; }

        public ISearchProductService ProductService { get; set; }

        #endregion Public Properties

        #region Public constructor

        public ElasticSearchIndexer(ISearchProductService productService, ISearchCategoryService categoreyService, int pageLength = 1000)
        {
            CategoryService = categoreyService;
            PageLength = pageLength;
            ProductService = productService;
        }

        #endregion Public constructor

        #region Public Methods

        //Returns the product descriptor.
        public CreateIndexDescriptor AddProductDocuments(ElasticClient elasticClient, IndexName indexName, int publishCatalogId)
        {
            CreateIndexDescriptor productDescriptor = GetCreateIndexDescriptor(indexName, publishCatalogId);
            return productDescriptor;
        }

        //Creates index for the products.
        public void CreateIndex(ElasticClient elasticClient, IndexName indexName, SearchParameterModel searchParameterModel)
        {

            SearchHelper searchHelper = new SearchHelper();
            if (!IsIndexExists(elasticClient, indexName))
            {
                CreateIndexDescriptor createIndexDescriptor = AddProductDocuments(elasticClient, indexName, searchParameterModel.CatalogId);
                var indexResponse = elasticClient.CreateIndex(indexName, i => createIndexDescriptor);

                if (!indexResponse.IsValid)
                {
                    searchHelper.UpdateSearchIndexServerStatus(new SearchIndexServerStatusModel() { SearchIndexServerStatusId = searchParameterModel.SearchIndexServerStatusId, SearchIndexMonitorId = searchParameterModel.SearchIndexMonitorId, ServerName = Environment.MachineName, Status = ZnodeConstant.SearchIndexFailedStatus });
                    throw new Exception($"Create Index failed. {indexResponse.OriginalException.Message}");
                }
            }

            foreach (LocaleModel locale in searchParameterModel.ActiveLocales)
            {
                int start = 0;
                decimal totalPages;
                bool continueIndex = true;

                int latestVersionId = ProductService.GetLatestVersionId(searchParameterModel.CatalogId, searchParameterModel.revisionType, locale.LocaleId);
                searchParameterModel.VersionId = latestVersionId;
                int chunkSize = Convert.ToInt32(ZnodeApiSettings.SearchIndexChunkSize);

                IEnumerable<int> publishcategoryIds = CategoryService.GetPublishCategoryList(searchParameterModel.CatalogId, searchParameterModel.VersionId)?.Select(x => x.ZnodeCategoryId)?.Distinct();

                do
                {
                    List<SearchProduct> pagedProducts = ProductService.GetAllProducts(searchParameterModel.CatalogId, searchParameterModel.VersionId, publishcategoryIds, start, chunkSize, searchParameterModel.IndexStartTime, out totalPages);
                    pagedProducts.ForEach(x => x.revisionType = searchParameterModel.revisionType);
                    List<dynamic> productsData = GetElasticProducts(pagedProducts);

                    if (start < totalPages)
                    {
                        if (productsData?.Count > 0)
                        {
                            IBulkResponse indexResponse = elasticClient.IndexMany<dynamic>(productsData, indexName.Name, ElasticLibraryConstants.ElasticProductIndexType);
                            if (!indexResponse.IsValid)
                            {
                                searchHelper.UpdateSearchIndexServerStatus(new SearchIndexServerStatusModel() { SearchIndexServerStatusId = searchParameterModel.SearchIndexServerStatusId, SearchIndexMonitorId = searchParameterModel.SearchIndexMonitorId, ServerName = Environment.MachineName, Status = ZnodeConstant.SearchIndexFailedStatus });
                                throw new Exception($"Create Index failed. {indexResponse.ItemsWithErrors?.FirstOrDefault()?.Error?.CausedBy?.Reason}");
                            }
                            //Save InProgress status for the server name.
                            searchHelper.UpdateSearchIndexServerStatus(new SearchIndexServerStatusModel() { SearchIndexServerStatusId = searchParameterModel.SearchIndexServerStatusId, SearchIndexMonitorId = searchParameterModel.SearchIndexMonitorId, ServerName = Environment.MachineName, Status = ZnodeConstant.SearchIndexInProgressStatus });
                        }
                        start++;
                    }
                    else
                        continueIndex = false;
                } while (continueIndex);

                elasticClient.Refresh(indexName);

            }
            //Save complete status for the server name.
            searchHelper.UpdateSearchIndexServerStatus(new SearchIndexServerStatusModel() { SearchIndexServerStatusId = searchParameterModel.SearchIndexServerStatusId, SearchIndexMonitorId = searchParameterModel.SearchIndexMonitorId, ServerName = Environment.MachineName, Status = ZnodeConstant.SearchIndexCompleteStatus });
        }

        public void CreateDocuments(List<ProductEntity> pagedProducts, ElasticClient elasticClient, string indexName)
        {
            long indexStartTime = DateTime.Now.Ticks;
            List<dynamic> productsData = GetElasticProducts(ProductService.GetElasticProducts(pagedProducts, indexStartTime));
            if (productsData?.Count > 0)
            {
                List<List<dynamic>> productDataInChunks = HelperUtility.SplitCollectionIntoChunks<dynamic>(productsData, Convert.ToInt32(ZnodeApiSettings.SearchIndexChunkSize));

                Parallel.For(0, productDataInChunks.Count(), index =>
                {
                    IBulkResponse indexResponse = elasticClient.IndexMany<dynamic>(productDataInChunks[index], indexName, ElasticLibraryConstants.ElasticProductIndexType);

                    if (!indexResponse.IsValid)
                        throw new Exception($"Create Index failed. {indexResponse.ItemsWithErrors?.First()?.Error?.CausedBy?.Reason}");
                });
            }
        }

        //Create index for category products.
        public bool CreateIndexForCategoryProducts(CategoryProductSearchParameterModel categoryProductSearchParameterModel, ElasticClient elasticClient)
        {
            long indexStartTime = DateTime.Now.Ticks;

            //Here we are creating fresh index with new indexStartTime with newly records , till that time duplicate index will be present.
            CreateDocumentsForCatalogCategoryProducts(categoryProductSearchParameterModel.ProductEntities, elasticClient, categoryProductSearchParameterModel.IndexName, indexStartTime);

            //Delete index on the basis of indexStartTime. here we keep indexes which is having new indexStartTime for perticular categoryids and version id.
            return DeleteCatalogCategoryProducts(elasticClient,categoryProductSearchParameterModel.IndexName,categoryProductSearchParameterModel.CatalogId,categoryProductSearchParameterModel.PublishCategoryIds,categoryProductSearchParameterModel.RevisionType, Convert.ToString(categoryProductSearchParameterModel.VersionId), indexStartTime);
        }


        private List<dynamic> GetElasticProducts(List<SearchProduct> pagedProducts)
        {
            DataTable products = ToDataTable(pagedProducts.OrderBy(x => x.name).ToList());

            List<dynamic> productsData = new List<dynamic>();

            foreach (var item in products.AsEnumerable())
            {
                // Expando objects are IDictionary<string, object>
                IDictionary<string, object> dn = new ExpandoObject();

                foreach (var column in products.Columns.Cast<DataColumn>())
                    dn[column.ColumnName] = item[column];

                productsData.Add(dn);
            }

            return productsData;
        }

        //Delete elastic search index.
        public bool DeleteIndex(ElasticClient elasticClient, string indexName)
        {
            IDeleteIndexResponse response = elasticClient.DeleteIndex(indexName);
            if (response.IsValid)
                return true;
            else
            {
                ZnodeLogging.LogMessage(response.ServerError.Error.ToString(), ZnodeLogging.Components.Search.ToString());
                if (response.ServerError.Status == 404)
                    throw new ZnodeException(ErrorCodes.NotFound, "Index does not exist.");
                return false;
            }
        }

        public bool WriteSynonymsFile(ElasticClient elasticClient, int publishCatalogId, string indexName, bool isSynonymsFile)
        {
            bool result = true;
            INodesInfoResponse nodeInfo = elasticClient.NodesInfo();
            ElasticsearchDynamicValue pathDynamicValues = nodeInfo.Nodes[(nodeInfo.Nodes.FirstOrDefault().Key)].Settings["path"];
            string configPath = pathDynamicValues["home"].Value.ToString() + Path.DirectorySeparatorChar + "config";

            if (isSynonymsFile)
                result = WriteSynonymsTextFile(elasticClient, publishCatalogId, indexName, configPath);
            return result;
        }

        //Delete unaffected products from search index
        public bool DeleteProductData(ElasticClient elasticClient, IndexName indexName, long indexStartTime)
        {
            //Delete request for delete documents which does not match to current create index time.
            var request = new DeleteByQueryRequest<dynamic>(indexName, ElasticLibraryConstants.ElasticProductIndexType)
            {
                Query = new QueryContainer(new BoolQuery
                {
                    MustNot = new QueryContainer[] { new TermQuery { Field = "timestamp", Value = indexStartTime.ToString() } },
                })
            };

            return elasticClient.DeleteByQuery(request)?.IsValid ?? false;
        }

        //Delete unaffected products from search index
        public bool DeleteProductDataByRevisionType(ElasticClient elasticClient, IndexName indexName, string revisionType, long indexStartTime)
        {
            //Delete request for delete documents which does not match to current create index time.
            var request = new DeleteByQueryRequest<dynamic>(indexName, ElasticLibraryConstants.ElasticProductIndexType)
            {
                Query = new QueryContainer(new BoolQuery
                {
                    Must = new QueryContainer[] { new MatchQuery { Field = "revisiontype", Query = revisionType } },
                    MustNot = new QueryContainer[] { new TermQuery { Field = "timestamp", Value = indexStartTime.ToString() } },
                }
                )
            };

            var test = elasticClient.DeleteByQuery(request);
            var tt = test?.IsValid ?? false;

            return tt;
        }

        public bool RenameIndexData(ElasticClient elasticClient, int catalogIndexId, string oldIndexName, string newIndexName)
        {
            if (IsIndexExists(elasticClient, oldIndexName))
            {
                {
                    var reindexResponse = elasticClient.ReindexOnServer(r => r
          .Source(s => s
              .Index(oldIndexName)
           )
          .Destination(d => d
              .Index(oldIndexName)
          )
          .WaitForCompletion(true)
      );

                    var deleteIndex = elasticClient.DeleteIndex(oldIndexName);
                    return true;
                }
            }
            else
                return false;
        }

        //Delete unaffected products from search index
        public bool DeleteProduct(ElasticClient elasticClient, IndexName indexName, string znodeProductIds, string revisionType)
        {
            //Delete request for delete documents which does not match to current create index time.
            var request = new DeleteByQueryRequest<dynamic>(indexName, ElasticLibraryConstants.ElasticProductIndexType)
            {
                Query = new QueryContainer(new BoolQuery
                {
                    Must = new QueryContainer[] { new TermQuery { Field = ZnodeConstant.ZnodeProductId, Value = znodeProductIds } },
                })
            };

            return elasticClient.DeleteByQuery(request)?.IsValid ?? false;
        }

        //Delete unaffected products from search index
        public bool DeleteProducts(ElasticClient elasticClient, IndexName indexName, IEnumerable<object> productIds, string revisionType, string versionId)
        {
            ///Delete request for delete documents which does not match to current create index time.
            var request = new DeleteByQueryRequest<dynamic>(indexName, ElasticLibraryConstants.ElasticProductIndexType)
            {
                Query = new QueryContainer(new BoolQuery
                {
                    Must = new QueryContainer[] { new TermsQuery { Field = ZnodeConstant.ZnodeProductId, Terms = productIds } ,
                    new MatchQuery { Field = "revisiontype", Query = revisionType } ,
                    new MatchQuery { Field = "versionid", Query = versionId } },
                })
            };
            var s = elasticClient.DeleteByQuery(request);
            return elasticClient.DeleteByQuery(request)?.IsValid ?? false;
        }

        //Delete unaffected category name from search index
        public bool DeleteCategoryForGivenIndex(ElasticClient elasticClient, IndexName indexName, int categoryId)
        {
            //Delete request for delete documents which does not match to current create index time.
            var request = new DeleteByQueryRequest<dynamic>(indexName)
            {
                Query = new QueryContainer(new BoolQuery
                {
                    Must = new QueryContainer[] { new TermQuery { Field = ZnodeConstant.PublishCategoryId.ToLower(), Value = categoryId.ToString() } },
                })
            };
            return elasticClient.DeleteByQuery(request)?.IsValid ?? false;
        }

        //Delete unaffected catalog category products from search index
        public bool DeleteCatalogCategoryProducts(ElasticClient elasticClient, IndexName indexName, int publishCatalogId, List<int> publishCategoryIds, string revisionType, string versionId, long indexStartTime = 0)
        {
            IEnumerable<object> categoryIds = publishCategoryIds.Cast<object>();

            //Delete request for delete documents which does not match to current create index time.
            var request = new DeleteByQueryRequest<dynamic>(indexName)
            {
                Query = new QueryContainer(new BoolQuery
                {
                    Must = new QueryContainer[] { new MatchQuery { Field = "versionid", Query = versionId }, new MatchQuery { Field = "revisiontype", Query = revisionType }, new TermsQuery { Field = ZnodeConstant.PublishCategoryId, Terms = categoryIds } },
                    MustNot = new QueryContainer[] { new TermQuery { Field = "timestamp", Value = indexStartTime.ToString() } },
                })
            };
            IDeleteByQueryResponse deleteByQueryResponse = elasticClient.DeleteByQuery(request);
            return deleteByQueryResponse?.IsValid ?? false;
        }

        //Create documents for catalog and category products.
        public void CreateDocumentsForCatalogCategoryProducts(List<ProductEntity> pagedProducts, ElasticClient elasticClient, string indexName, long indexStartTime)
        {
            List<dynamic> productsData = GetElasticProducts(ProductService.GetElasticProducts(pagedProducts, indexStartTime));
            if (productsData?.Count <= 0)
                return;

            List<List<dynamic>> productDataInChunks = HelperUtility.SplitCollectionIntoChunks<dynamic>(productsData, !string.IsNullOrEmpty(ZnodeApiSettings.IndexChunkSizeOnCategoryPublish) ? Convert.ToInt32(ZnodeApiSettings.IndexChunkSizeOnCategoryPublish) : ZnodeConstant.IndexChunkSize);

            for (int index = 0; index < productDataInChunks.Count(); index++)
            {
                IBulkResponse indexResponse = elasticClient.IndexMany<dynamic>(productDataInChunks[index], indexName, ElasticLibraryConstants.ElasticProductIndexType);

                if (!indexResponse.IsValid)
                    throw new Exception($"Create Index failed. {indexResponse.ItemsWithErrors?.First()?.Error?.CausedBy?.Reason}");
            }
        }

        //Checks if the index exists.
        public bool IsIndexExists(ElasticClient elasticClient, IndexName indexName)
        {
            IndexExistsRequest request = new IndexExistsRequest(indexName);
            IExistsResponse result = elasticClient.IndexExists(request);
            return result.Exists;
        }

        #endregion Public Methods

        #region Private Methods

        //Creates create index descriptor.
        private CreateIndexDescriptor GetCreateIndexDescriptor(IndexName indexName, int publishCatalogId)
        {
            IndexSettings settings = CreateIndexSettings(publishCatalogId);

            //We have set the field limit to 5000.
            settings.Add("index.mapping.total_fields.limit", 5000);

            return ConfigureIndex(indexName, settings);
        }

        public CreateIndexDescriptor ConfigureIndex(IndexName indexName, IndexSettings settings)
        {
            return new CreateIndexDescriptor(indexName)
            .InitializeUsing(new IndexState() { Settings = settings })
            .Mappings(mappings => mappings.Map<dynamic>("_default_", s => s.DateDetection(false).DynamicTemplates(dts => dts
            .DynamicTemplate("strings", dt => dt.Match("*")
            .PathUnmatch("attributes.*")
            .MatchMappingType("string").Mapping(t => t
            .Text(d => d
            .Fields(fi => fi.Text(te => te.Name("lowercase")
            .Analyzer("lowercase_analyzer")).Keyword(ss => ss.Name("keyword")).Text(at => at.Name("autocomplete").Analyzer("autocomplete").SearchAnalyzer("autocomplete_search_analyser").Fielddata(true))))))
            .DynamicTemplate("nested", nest => nest.PathMatch("attributes.*").MatchMappingType("string").Mapping(x => x.Text(te => te.Index(false)))))
            .Properties(propes => propes.Text(productname => productname.Name("productname")
            .Fields(f => f.Keyword(ss => ss.Name("keyword")).Text(te => te.Name("lowercase").Analyzer("lowercase_analyzer")).Text(at => at.Name("autocomplete").Analyzer("autocomplete").SearchAnalyzer("autocomplete_search_analyser").Fielddata(true)))
            .Analyzer("standard_analyzer")
            .SearchAnalyzer("my_standard_synonym_analyzer")).Text(sku => sku.Name("sku").Fields(f => f.Keyword(ss => ss.Name("keyword"))
            .Text(te => te.Name("lowercase")
            .Analyzer("lowercase_analyzer")).Text(at => at.Name("autocomplete").Analyzer("autocomplete").SearchAnalyzer("autocomplete_search_analyser").Fielddata(true)))
            .Analyzer("standard_analyzer")
            .SearchAnalyzer("my_standard_synonym_analyzer")).Text(did => did.Name("didyoumean").Analyzer("didYouMean")))));
        }

        //Creates index setting.
        private IndexSettings CreateIndexSettings(int publishCatalogId)
        {
            //Get default token filters.
            List<string> tokenFilters = ZnodeApiSettings.DefaultTokenFilters.Split(',').ToList();

            //create analysis object
            IndexSettings indexsettings = new IndexSettings
            {
                Analysis = new Analysis
                {
                    TokenFilters = new TokenFilters(),
                    Analyzers = new Analyzers(),
                    Tokenizers = new Tokenizers()
                }
            };

            SetFilterForAnalyzers(tokenFilters, indexsettings, publishCatalogId);

            SetTokenizers(indexsettings);
            SetAnalyzers(tokenFilters, indexsettings); return indexsettings;
        }

        public virtual void SetAnalyzers(List<string> tokenFilters, IndexSettings indexsettings)
        {
            indexsettings.Analysis.Analyzers.Add("standard_analyzer", SetCustomAnalyzer("whitespace", tokenFilters));
            indexsettings.Analysis.Analyzers.Add("my_standard_synonym_analyzer", SetCustomAnalyzer("whitespace", tokenFilters));
            indexsettings.Analysis.Analyzers.Add("didYouMean", SetCustomAnalyzer("standard", tokenFilters, new List<string> { "html_strip" }));
            indexsettings.Analysis.Analyzers.Add("lowercase_analyzer", SetCustomAnalyzer("keyword", new List<string> { "lowercase" }));
            indexsettings.Analysis.Analyzers.Add("autocomplete", SetCustomAnalyzer("ngram", new List<string> { "lowercase" }));
            indexsettings.Analysis.Analyzers.Add("autocomplete_search_analyser", SetCustomAnalyzer("keyword", new List<string> { "lowercase" }));
        }

        public virtual void SetTokenizers(IndexSettings indexsettings)
        {
            indexsettings.Analysis.Tokenizers.Add("standard", new StandardTokenizer() { MaxTokenLength = 12 });
            indexsettings.Analysis.Tokenizers.Add("ngram", new NGramTokenizer()
            {
                MinGram = 2,
                MaxGram = 15,
                TokenChars = new List<TokenChar> { TokenChar.Letter, TokenChar.Digit, TokenChar.Punctuation, TokenChar.Symbol, TokenChar.Whitespace }
            });
        }

        public void SetFilterForAnalyzers(List<string> tokenFilters, IndexSettings indexsettings, int publishCatalogId)
        {
            SetSynonymTokenFilter(tokenFilters, indexsettings, publishCatalogId);

            SetAutoCompleteFilter(indexsettings);
            //To DO:
            //indexsettings.Analysis.TokenFilters.Add("stopwords", new StopTokenFilter
            //{
            //    IgnoreCase = true,
            //    StopWords = new StopWords(new List<string> { "a", "and", "are", "an", "as", "at", "be", "but", "by", "for", "if", "in", "in to", "is", "it", "no", "not", "of", "such", "that", "the", "their", "then", "there", "these", "they", "this", "to", "was", "will", "with", "which", "whether", "over", "nor" })
            //});
            //tokenFilters.Add("stopwords");

            SetStemmingFilter(tokenFilters, indexsettings);
            SetDefaultFilter(tokenFilters, indexsettings);
        }

        public void SetAutoCompleteFilter(IndexSettings indexsettings)
        {
            indexsettings.Analysis.TokenFilters.Add("autocomplete_filter", new EdgeNGramTokenFilter
            {
                MinGram = 1,
                MaxGram = 10
            });
        }

        public void SetDefaultFilter(List<string> tokenFilters, IndexSettings indexsettings)
        {
            indexsettings.Analysis.TokenFilters.Add("shingleTokenFilter", new ShingleTokenFilter
            {
                MinShingleSize = 2,
                MaxShingleSize = 10
            });
            tokenFilters.Add("shingleTokenFilter");
        }

        public void SetStemmingFilter(List<string> tokenFilters, IndexSettings indexsettings)
        {
            if (ConfigurationManager.AppSettings["EnableStemmingFilter"]?.Equals("1") ?? false)
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["StemExclusionWords"]))
                {
                    indexsettings.Analysis.TokenFilters.Add("preventStemmingFilter", new KeywordMarkerTokenFilter
                    {
                        Keywords = ConfigurationManager.AppSettings["StemExclusionWords"].Split(',')
                    });
                    tokenFilters.Add("preventStemmingFilter");
                }
                indexsettings.Analysis.TokenFilters.Add("stemmerTokenFilter", new StemmerTokenFilter
                {
                    Language = Language.English.ToString()
                });
                tokenFilters.Add("stemmerTokenFilter");
            }
        }

        private CustomAnalyzer SetCustomAnalyzer(string tokenizer, List<string> tokenFilters, List<string> charFilters = null)
        {
            CustomAnalyzer customAnalyzer = new CustomAnalyzer();
            customAnalyzer.Tokenizer = tokenizer;
            customAnalyzer.Filter = tokenFilters;

            if (charFilters?.Count > 0)
                customAnalyzer.CharFilter = charFilters;
            return customAnalyzer;
        }

        //Gets synonym token filter.
        private void SetSynonymTokenFilter(List<string> tokenFilters, IndexSettings indexsettings, int publishCatalogId = 0, bool isPublishSynonyms = false)
        {
            List<ZnodeSearchSynonym> synonymList = ProductService.GetSynonymsData(publishCatalogId);

            List<string> formattedSynonymList = new List<string>();

            if (isPublishSynonyms && synonymList.Count <= 0)
                throw new ZnodeException(ErrorCodes.ProcessingFailed, "no record found for processing synonyms.");

            if (synonymList.Count > 0)
            {
                foreach (ZnodeSearchSynonym item in synonymList)
                {
                    if (item.IsBidirectional.GetValueOrDefault())
                    {
                        formattedSynonymList.Add($"{item.OriginalTerm},{item.ReplacedBy}");
                    }
                    else
                        formattedSynonymList.Add($"{item.OriginalTerm} => {item.ReplacedBy}");
                }
            }

            if (formattedSynonymList?.Count > 0 && Equals(ZnodeApiSettings.UseSynonym, "true") && formattedSynonymList?.Count > 0)
            {
                //Create synonym token filter.
                SynonymTokenFilter synonymFilter = new SynonymTokenFilter();
                synonymFilter.IgnoreCase = true;
                synonymFilter.Synonyms = formattedSynonymList;
                //Add synonym token filter in indexsetting.
                indexsettings.Analysis.TokenFilters = new TokenFilters();
                indexsettings.Analysis.TokenFilters.Add("synonym", synonymFilter);
                tokenFilters.Add("synonym");
            }
        }

        //Write Synonyms file
        private bool WriteSynonymsTextFile(ElasticClient elasticClient, int publishCatalogId, string indexName, string configPath)
        {
            try
            {
                if (IsIndexExists(elasticClient, indexName))
                {

                    IndexSettings indexsettings = new IndexSettings
                    {
                        Analysis = new Analysis
                        {
                            TokenFilters = new TokenFilters(),
                            Analyzers = new Analyzers(),
                        }
                    };

                    //Get default token filters.
                    List<string> tokenFilters = ZnodeApiSettings.DefaultTokenFilters.Split(',').ToList();

                    SetSynonymTokenFilter(tokenFilters, indexsettings, publishCatalogId, true);
                    SetStemmingFilter(tokenFilters, indexsettings);
                    SetDefaultFilter(tokenFilters, indexsettings);
                    elasticClient.CloseIndex(indexName);

                    indexsettings.Analysis.Analyzers.Add("my_standard_synonym_analyzer", SetCustomAnalyzer("whitespace", tokenFilters));

                    IUpdateIndexSettingsResponse updateSettings = elasticClient.UpdateIndexSettings(new UpdateIndexSettingsRequest(indexName) { IndexSettings = indexsettings });

                    elasticClient.OpenIndex(indexName);

                    return true;
                }
                else
                    throw new ZnodeException(ErrorCodes.NotFound, "Search index does not exist.");
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error, ex);
                throw;
            }
        }
        private DataTable ToDataTable(List<SearchProduct> products)
        {
            DataTable tmp = new DataTable();

            //Tables created with columns
            foreach (SearchProduct product in products)
            {
                foreach (SearchAttributes attribute in product.searchableattributes)
                {
                    DataColumnCollection columns = tmp.Columns;
                    if (!columns.Contains(attribute.attributecode))
                    {
                        if (attribute.isfacets)
                        {
                           tmp.Columns.Add(attribute.attributecode.ToLower(), typeof(string[]));
                            continue;
                        }
                        else if (attribute.attributetypename == "Number")
                        {
                            tmp.Columns.Add(attribute.attributecode.ToLower(), typeof(double));
                            continue;
                        }
                        else
                            tmp.Columns.Add(attribute.attributecode.ToLower());
                    }
                }
            }
            tmp.Columns.Add("znodecatalogid", typeof(int));
            tmp.Columns.Add("localeid", typeof(int));
            tmp.Columns.Add("categoryid", typeof(int));
            tmp.Columns.Add("categoryname");
            tmp.Columns.Add("znodeproductid", typeof(int));
            tmp.Columns.Add(ZnodeConstant.productPrice, typeof(decimal));
            tmp.Columns.Add("isactive", typeof(bool));
            tmp.Columns.Add("hidefromsearch", typeof(bool));
            tmp.Columns.Add("productindex", typeof(int));
            tmp.Columns.Add("profileids", typeof(int[]));
            tmp.Columns.Add("displayorder", typeof(int));
            tmp.Columns.Add("timestamp", typeof(long));
            tmp.Columns.Add("autocompleteproductname");
            tmp.Columns.Add("autocompletesku");
            tmp.Columns.Add("didyoumean");
            tmp.Columns.Add("name");
            tmp.Columns.Add("versionid", typeof(int));
            tmp.Columns.Add("rating", typeof(decimal));
            tmp.Columns.Add("totalreviewcount", typeof(int));
            tmp.Columns.Add("attributes", typeof(object));
            tmp.Columns.Add("revisiontype", typeof(string));
            tmp.Columns.Add("brands", typeof(string[]));
            tmp.Columns.Add("salesprice");
            tmp.Columns.Add("retailprice");
            tmp.Columns.Add("culturecode");
            tmp.Columns.Add("currencycode");
            tmp.Columns.Add("currencysuffix");
            tmp.Columns.Add("seodescription");
            tmp.Columns.Add("seokeywords");
            tmp.Columns.Add("seotitle");
            tmp.Columns.Add("seourl");
            tmp.Columns.Add("imagesmallpath");
            //Add data of each product
            foreach (SearchProduct product in products)
            {
                DataRow dr = tmp.NewRow();
                foreach (SearchAttributes attribute in product.searchableattributes)
                {
                    foreach (DataColumn column in tmp.Columns)
                    {
                        if (
                            (attribute.attributecode.Equals(ZnodeConstant.OutOfStockOptions, StringComparison.InvariantCultureIgnoreCase) && column.ColumnName.Equals(ZnodeConstant.OutOfStockOptions, StringComparison.InvariantCultureIgnoreCase))
                            || 
                            (attribute.attributecode.Equals(ZnodeConstant.ProductType, StringComparison.InvariantCultureIgnoreCase) && column.ColumnName.Equals(ZnodeConstant.ProductType, StringComparison.InvariantCultureIgnoreCase)))
                        {
                            if (attribute.selectvalues.Count > 0)
                                dr[column.ColumnName] = attribute.selectvalues.FirstOrDefault()?.code;
                            continue;
                        }
                        if (attribute.attributecode.Equals(ZnodeConstant.Brand, StringComparison.InvariantCultureIgnoreCase) 
                            && column.ColumnName.Equals(ZnodeConstant.Brand, StringComparison.InvariantCultureIgnoreCase)) 
                        {
                            dr[column.ColumnName] = attribute.selectvalues.Select(y=>y.code).ToArray();
                            continue;
                        }// Added if condition to add brandcode in place of brandname in mongoentity > to fetch product using brandcode 

                        if (attribute.attributecode.Equals(column.ColumnName, StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (attribute.selectvalues.Count > 0)
                            {
                                if (column.DataType == typeof(string[]))
                                    dr[column.ColumnName] = attribute.selectvalues.Select(x => x.value).ToArray();
                                else
                                    dr[column.ColumnName] = attribute.selectvalues.FirstOrDefault().value;
                            }
                            else if (column.DataType == typeof(double))
                                dr[column.ColumnName] = string.IsNullOrEmpty(attribute.attributevalues) ? 0 : Convert.ToDouble(attribute.attributevalues);
                            else
                                dr[column.ColumnName] = attribute.attributevalues;
                            continue;
                        }
                    }
                }

                if (product.brands.Count > 0) dr["brands"] = product.brands.Select(x => x.brandcode).ToArray();
                dr["znodecatalogid"] = product.catalogid;
                dr["localeid"] = product.localeid;
                dr["categoryid"] = product.categoryid;
                dr["categoryname"] = product.categoryname;
                dr["znodeproductid"] = product.znodeproductid;
                dr["isactive"] = product.isactive;
                dr["hidefromsearch"] = product.attributes.FirstOrDefault(m => m.attributecode.ToLower().Equals("hidefromsearch"))?.attributevalues ?? "false";
                dr["productindex"] = product.productindex;
                dr["rating"] = product.rating;
                dr["totalreviewcount"] = product.totalreviewcount;
                dr["profileids"] = product.profileids;
                dr["displayorder"] = product.displayorder;
                dr["name"] = product.name;
                dr["timestamp"] = product.timestamp;
                dr["autocompleteproductname"] = product.name;
                dr["autocompletesku"] = product.sku;
                dr["didyoumean"] = product.name;
                dr["versionid"] = product.version;
                dr["attributes"] = product.attributes;
                dr["revisiontype"] = product.revisionType;
                dr[ZnodeConstant.productPrice] = product.productprice;
                dr["salesprice"] = product.salesprice;
                dr["retailprice"] = product.retailprice;
                dr["culturecode"] = product.culturecode;
                dr["currencycode"] = product.currencycode;
                dr["currencysuffix"] = product.currencysuffix;
                dr["seodescription"] = product.seodescription;
                dr["seokeywords"] = product.seokeywords;
                dr["seotitle"] = product.seotitle;
                dr["seourl"] = product.seourl;
                dr["imagesmallpath"] = product.imagesmallpath;
                tmp.Rows.Add(dr);
            }

            return tmp;
        }
        #endregion Private Methods
    }
}