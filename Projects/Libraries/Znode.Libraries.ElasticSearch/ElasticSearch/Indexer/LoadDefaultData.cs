﻿using Nest;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Search;

namespace Znode.Libraries.ElasticSearch
{
    public class LoadDefaultData
    {
        private readonly ElasticClient elasticClient;
        private readonly ElasticSearchIndexer indexer;

        public LoadDefaultData()
        {
            string uri = ZnodeApiSettings.ElasticSearchRootUri;
            Uri node = new Uri(string.IsNullOrEmpty(uri) ? "http://localhost:9200" : uri);
            ConnectionSettings settings = new ConnectionSettings(node).DisableDirectStreaming().InferMappingFor<SearchProduct>(m => m.IdProperty(p => p.indexid)).RequestTimeout(TimeSpan.FromMinutes(20));
            elasticClient = new ElasticClient(settings);

            //Elasticsearch implementation.
            indexer = new ElasticSearchIndexer(new SearchProductService(), new SearchCategoryService());
        }

        #region Public Methods
        //Populate the default data.
        public void IndexingDefaultData(string indexName, SearchParameterModel searchParameterModel)
        {
            try
            {
                //Call to Create index in elastic search.
                indexer.CreateIndex(elasticClient, indexName, searchParameterModel);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                throw;
            }
        }

        //Create document in given index.
        public void CreateDocuments(string indexName, List<ProductEntity> elasticPrducts)
            => indexer.CreateDocuments(elasticPrducts, elasticClient, indexName);

        //Create document in given index.
        public bool CreateIndexForCategoryProducts(CategoryProductSearchParameterModel categoryProductSearchParameterModel)
            => indexer.CreateIndexForCategoryProducts(categoryProductSearchParameterModel, elasticClient);

        //Populate the default data.
        public bool DeleteIndexData(string indexName, long indexStartTime)
        {
            try
            {
                //Call to delete index data in elastic search.
                return indexer.DeleteProductData(elasticClient, indexName, indexStartTime);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                throw;
            }
        }

        //Populate the default data.
        public bool DeleteProductDataByRevisionType(string indexName, string revisionType, long indexStartTime)
        {
            return indexer.DeleteProductDataByRevisionType(elasticClient, indexName, revisionType, indexStartTime);
        }

        //Delete product/document from given index. 
        public bool DeleteProduct(string indexName, string znodeProductIds, string revisionType)
         => indexer.DeleteProduct(elasticClient, indexName, znodeProductIds, revisionType);

        //Delete product/document from given index. 
        public bool DeleteProduct(string indexName, IEnumerable<object> productIds, string revisionType, string versionId)
            => indexer.DeleteProducts(elasticClient, indexName, productIds, revisionType, versionId);

        //Delete category/document from given index. 
        public bool DeleteCategoryForGivenIndex(string indexName, int categoryId)
            => indexer.DeleteCategoryForGivenIndex(elasticClient, indexName, categoryId);

        //Delete catalog category products/documents from given index. 
        public bool DeleteCatalogCategoryProducts(string indexName, int publishCatalogId, List<int> publishCategoryIds, string revisionType, string versionId)
            => indexer.DeleteCatalogCategoryProducts(elasticClient, indexName, publishCatalogId, publishCategoryIds, revisionType, versionId);

        //Checks if the index exists on the local system.
        public bool IsIndexExists(string indexName)
                => indexer.IsIndexExists(elasticClient, indexName);

        public bool WriteSynonymsFile(int publishCatalogId, string indexName, bool isSynonymsFile) =>
            indexer.WriteSynonymsFile(elasticClient, publishCatalogId, indexName, isSynonymsFile);

        public bool DeleteIndex(string indexName)
        {
            try
            {
                //Call to delete index data in elastic search.
                return indexer.DeleteIndex(elasticClient, indexName);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                throw;
            }
        }

        public bool RenameIndexData(int catalogIndexId, string oldIndexName, string newIndexName)
        {
            return indexer.RenameIndexData(elasticClient, catalogIndexId, oldIndexName, newIndexName);
        }
        #endregion
    }
}
