﻿using Nest;
using System.Collections.Generic;
using System.Linq;
using Znode.Libraries.Search;

namespace Znode.Libraries.Search
{
    public class MatchQueryBuilder : BaseQuery, ISearchQuery
    {
        readonly FunctionScoreQuery functionScoreQuery = new FunctionScoreQuery();
        readonly BoolQuery finalBoolQuery = new BoolQuery();

        public SearchRequest<dynamic> GenerateQuery(IZnodeSearchRequest request)
        {
            MatchQuery matchQuery = new MatchQuery();

            if (request.SearchableAttibute?.Count == 0)
                matchQuery.Field = "productname";
            else
                matchQuery.Field = request.SearchableAttibute?.FirstOrDefault().AttributeCode.ToLower();

            matchQuery.Query = request.SearchText;
            if (!string.IsNullOrEmpty(GetFeatureValue(request, "MinimumShouldMatch")))
                matchQuery.MinimumShouldMatch = GetFeatureValue(request, "MinimumShouldMatch");
            matchQuery = GetFuzzinessQuery(matchQuery, request);

            matchQuery.Operator = GetOperator(request);
            functionScoreQuery.ScoreMode = FunctionScoreMode.Sum;
            functionScoreQuery.BoostMode = FunctionBoostMode.Sum;
            functionScoreQuery.Functions = AddFunctionToSearchQuery(request);

            finalBoolQuery.Must = new List<QueryContainer> { matchQuery };

            //set conditions for boost or bury the products.
            BoolQuery boostAndBuryCondition = GetBoostOrBuryItem(request);

            finalBoolQuery.Should = new List<QueryContainer> { boostAndBuryCondition };

            finalBoolQuery.Filter = request.FilterValues;
            functionScoreQuery.Query = finalBoolQuery;

            AggregationBase aggregationBase = GetProductCountAggregation();

            if (request.GetCategoriesHeirarchy)
                aggregationBase = GetCategoryAggregation(aggregationBase);

            if (request.GetFacets && request.FacetableAttribute?.Count > 0)
                aggregationBase = GetFacetAggregation(aggregationBase, request.FacetableAttribute);

            List<ISort> SortSettings = AddSortToSearchQuery(request);

            SearchRequest<dynamic> searchRequest = new SearchRequest<dynamic>(request.IndexName, ElasticLibraryConstants.ElasticProductIndexType);
            searchRequest.Query = functionScoreQuery;
            searchRequest.From = request.StartIndex;
            searchRequest.Size = request.PageSize;

            if (SortSettings.Count > 0)
                searchRequest.Sort = SortSettings;

            searchRequest.Aggregations = aggregationBase;
            searchRequest.Suggest = GetSuggestionQuery(request);

            return searchRequest;

        }
    }
}
