﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Search;

namespace Znode.Libraries.Search
{
    public class MatchPhrasePrefixQueryBuilder : BaseQuery, ISearchQuery
    {
        readonly FunctionScoreQuery functionScoreQuery = new FunctionScoreQuery();
        readonly BoolQuery finalBoolQuery = new BoolQuery();

        public SearchRequest<dynamic> GenerateQuery(IZnodeSearchRequest request)
        {
            MatchPhrasePrefixQuery matchPharasePrefixQuery = new MatchPhrasePrefixQuery();

            if (request.SearchableAttibute?.Count == 0)
                matchPharasePrefixQuery.Field = "productname";
            else
                matchPharasePrefixQuery.Field = request.SearchableAttibute?.FirstOrDefault().AttributeCode.ToLower();

            matchPharasePrefixQuery.Query = request.SearchText;
            matchPharasePrefixQuery.Operator = GetOperator(request);

            //Fuzziness
            string isAutocorrect = request.FeatureList.Find(x => x.FeatureCode == "AutoCorrect")?.SearchFeatureValue;
            if (!string.IsNullOrEmpty(isAutocorrect) && Convert.ToBoolean(isAutocorrect))
            {
                matchPharasePrefixQuery.Fuzziness = Fuzziness.Auto;
            }

            functionScoreQuery.ScoreMode = FunctionScoreMode.Sum;
            functionScoreQuery.BoostMode = FunctionBoostMode.Sum;
            functionScoreQuery.Functions = AddFunctionToSearchQuery(request);

            finalBoolQuery.Must = new List<QueryContainer> { matchPharasePrefixQuery };

            //set conditions for boost or bury the products.
            BoolQuery boostAndBuryCondition = GetBoostOrBuryItem(request);

            finalBoolQuery.Should = new List<QueryContainer> { boostAndBuryCondition };

            finalBoolQuery.Filter = request.FilterValues;

            functionScoreQuery.Query = finalBoolQuery;
            AggregationBase aggregationBase = GetProductCountAggregation();

            if (request.GetCategoriesHeirarchy)
                aggregationBase = GetCategoryAggregation(aggregationBase);

            if (request.GetFacets && request.FacetableAttribute?.Count > 0)
                aggregationBase = GetFacetAggregation(aggregationBase, request.FacetableAttribute);

            List<ISort> SortSettings = AddSortToSearchQuery(request);

            SearchRequest<dynamic> searchRequest = new SearchRequest<dynamic>(request.IndexName, ElasticLibraryConstants.ElasticProductIndexType);
            searchRequest.Query = functionScoreQuery;
            searchRequest.From = request.StartIndex;
            searchRequest.Size = request.PageSize;
            if (SortSettings.Count > 0)
                searchRequest.Sort = SortSettings;
            searchRequest.Aggregations = aggregationBase;
            searchRequest.Suggest = GetSuggestionQuery(request);
            return searchRequest;

        }
    }
}
