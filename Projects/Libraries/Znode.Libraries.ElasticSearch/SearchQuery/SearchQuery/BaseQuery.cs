﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Search;

namespace Znode.Libraries.Search
{
    public class BaseQuery
    {

        protected CardinalityAggregation GetProductCountAggregation() =>
               new CardinalityAggregation("Count_Aggregation", $"znodeproductid.keyword");

        protected AggregationBase GetCategoryAggregation(AggregationBase aggregationBase) => aggregationBase &= new TermsAggregation("category_aggregation")
        {
            Field = $"categoryname.keyword",
            Aggregations = new TermsAggregation("categoryid")
            {
                Field = $"categoryid"
            }
        };

        protected AggregationBase GetFacetAggregation(AggregationBase aggregationBase, List<ElasticSearchAttributes> facetableAttribute)
        {
            foreach (var aggregationterm in facetableAttribute)
            {
                TermsAggregation termsAggregation = new TermsAggregation(aggregationterm.AttributeCode.ToLower())
                {
                    Field = $"{aggregationterm.AttributeCode.ToLower()}.keyword",
                    //This is for the no of values in facets.We are setting it 1000.
                    Size = 1000
                };
                aggregationBase = HelperUtility.IsNull(aggregationBase) ? termsAggregation : aggregationBase &= termsAggregation;
            }
            return aggregationBase;
        }

        protected MatchQuery GetFuzzinessQuery(MatchQuery query, IZnodeSearchRequest request)
        {
            string isAutocorrect = request.FeatureList.Find(x => x.FeatureCode == "AutoCorrect")?.SearchFeatureValue;
            if (!string.IsNullOrEmpty(isAutocorrect) && Convert.ToBoolean(isAutocorrect))
            {
                query.Fuzziness = Fuzziness.Auto;
            }
            return query;
        }

        protected Operator GetOperator(IZnodeSearchRequest request)
                  => string.Equals(request.Operator, "and", StringComparison.InvariantCultureIgnoreCase) ? Operator.And : Operator.Or;


        protected SuggestContainer GetSuggestionQuery(IZnodeSearchRequest request)
        {
            SuggestContainer suggestContainer = new SuggestContainer();
            SuggestBucket bucket = new SuggestBucket();
            bucket.Text = request.SearchText;
            bucket.Phrase = new PhraseSuggester
            {
                Field = "didyoumean",
            };
            suggestContainer.Add("suggestion", bucket);
            return suggestContainer;
        }

        protected string GetFeatureValue(IZnodeSearchRequest request, string featureCode) =>
          request.FeatureList.Find(x => x.FeatureCode == featureCode)?.SearchFeatureValue;

        protected List<IScoreFunction> AddFunctionToSearchQuery(IZnodeSearchRequest request)
        {
            List<IScoreFunction> scoreFunctions = new List<IScoreFunction>();
            scoreFunctions.Add(new WeightFunction { Weight = 0.0 });
            return scoreFunctions;
        }

        protected List<ISort> AddSortToSearchQuery(IZnodeSearchRequest request)
        {
            List<ISort> sorts = new List<ISort>();

            if (request.SortCriteria?.Count > 0)
            {
                foreach (SortCriteria sort in request.SortCriteria)
                {

                    SortDescriptor<dynamic> sortDescriptor = new SortDescriptor<dynamic>();
                    SortCriteria.SortNameEnum sortItem = sort.SortName;
                    SortOrder sortOrder = sort.SortDirection == 0 ? SortOrder.Ascending : SortOrder.Descending;
                    SortField sorting = new SortField();
                    switch (sortItem)
                    {
                        case SortCriteria.SortNameEnum.Price:
                            sorting.Field = $"{ZnodeConstant.productPrice}";
                            sorting.Order = sortOrder;
                            break;

                        case SortCriteria.SortNameEnum.ProductName:
                            sorting.Field = $"productname.keyword";
                            sorting.Order = sortOrder;
                            break;

                        case SortCriteria.SortNameEnum.HighestRated:
                            sorting.Field = $"{ZnodeConstant.ratings}";
                            sorting.Order = SortOrder.Descending;
                            break;

                        case SortCriteria.SortNameEnum.MostReviewed:
                            sorting.Field = $"{ZnodeConstant.totalreviewcount}";
                            sorting.Order = SortOrder.Descending;
                            break;

                        case SortCriteria.SortNameEnum.OutOfStock:
                            sortDescriptor.Field(ZnodeConstant.totalreviewcount, SortOrder.Descending);
                            break;

                        case SortCriteria.SortNameEnum.InStock:
                            sortDescriptor.Field(ZnodeConstant.totalreviewcount, SortOrder.Descending);
                            break;

                        case SortCriteria.SortNameEnum.DisplayOrder:
                            sorting.Field = $"displayorder";
                            sorting.Order = sortOrder;
                            break;

                        case SortCriteria.SortNameEnum.ProductBoost:
                            sorting.Field = $"productboost.keyword";
                            sorting.Order = SortOrder.Descending;
                            break;
                    }
                    sorts.Add(sorting);
                }

            }
            else
            {
                if (request.SortCriteria?.Count == 0 && request.FieldValueFactors?.Count > 0)
                {
                    foreach (var item in request.FieldValueFactors)
                    {
                        SortOrder sortOrder = item.Value == 1 ? SortOrder.Ascending : SortOrder.Descending;
                        SortField sorting = new SortField();
                        sorting.Field = item.Key;
                        sorting.Order = sortOrder;

                        sorts.Add(sorting);
                    }

                }
            }

            return sorts;
        }

        //Get boost and Bury Condition for search result.
        protected BoolQuery GetBoostOrBuryItem(IZnodeSearchRequest request)
        {
            BoolQuery finalBoolQuery = new BoolQuery();
            List<QueryContainer> queryContainers = new List<QueryContainer>();

            if (request.BoostAndBuryItemLists?.Count > 0)
            {
                var boostAndBuryConditions = request.BoostAndBuryItemLists.GroupBy(x => x.SearchCatalogRuleId);

                foreach (var conditions in boostAndBuryConditions)
                {
                    QueryContainer query = new QueryContainer();
                    QueryContainer childquery = new QueryContainer();

                    foreach (var item in conditions)
                    {
                        switch (item.SearchItemCondition.ToLower())
                        {
                            case "is":
                                MatchQuery isQuery = new MatchQuery();
                                isQuery.Field = item.SearchItemKeyword.ToLower() + ".lowercase";
                                isQuery.Query = item.SearchItemValue.ToLower();
                                isQuery.Boost = item.SearchItemBoostValue;
                                childquery = isQuery;
                                break;
                            case "contains":
                                WildcardQuery wildcardContainsQuery = new WildcardQuery();
                                wildcardContainsQuery.Field = item.SearchItemKeyword.ToLower() + ".lowercase";
                                wildcardContainsQuery.Value = "*" + item.SearchItemValue.ToLower() + "*";
                                wildcardContainsQuery.Boost = item.SearchItemBoostValue;
                                childquery = wildcardContainsQuery;
                                break;
                            case "startwith":
                                WildcardQuery wildcardStartWithQuery = new WildcardQuery();
                                wildcardStartWithQuery.Field = item.SearchItemKeyword.ToLower() + ".lowercase";
                                wildcardStartWithQuery.Value = item.SearchItemValue.ToLower() + "*";
                                wildcardStartWithQuery.Boost = item.SearchItemBoostValue;
                                childquery = wildcardStartWithQuery;
                                break;
                            case "endwith":
                                WildcardQuery wildcardEndsWithQuery = new WildcardQuery();
                                wildcardEndsWithQuery.Field = item.SearchItemKeyword.ToLower() + ".lowercase";
                                wildcardEndsWithQuery.Value = "*" + item.SearchItemValue.ToLower();
                                wildcardEndsWithQuery.Boost = item.SearchItemBoostValue;
                                childquery = wildcardEndsWithQuery;
                                break;
                            //greater than Query for numeric field.
                            case "gt":
                                NumericRangeQuery greaterThanQuery = new NumericRangeQuery();
                                greaterThanQuery.Field = item.SearchItemKeyword.ToLower();
                                greaterThanQuery.GreaterThan = Convert.ToDouble(item.SearchItemValue);
                                greaterThanQuery.Boost = item.SearchItemBoostValue;
                                childquery = greaterThanQuery;
                                break;
                            //greater than or Equal Query for numeric field.
                            case "gte":
                                NumericRangeQuery greaterThanOrEqualToQuery = new NumericRangeQuery();
                                greaterThanOrEqualToQuery.Field = item.SearchItemKeyword.ToLower();
                                greaterThanOrEqualToQuery.GreaterThanOrEqualTo = Convert.ToDouble(item.SearchItemValue);
                                greaterThanOrEqualToQuery.Boost = item.SearchItemBoostValue;
                                childquery = greaterThanOrEqualToQuery;
                                break;
                            //less than Query for numeric field.
                            case "lt":
                                NumericRangeQuery lessThanQuery = new NumericRangeQuery();
                                lessThanQuery.Field = item.SearchItemKeyword.ToLower();
                                lessThanQuery.LessThan = Convert.ToDouble(item.SearchItemValue);
                                lessThanQuery.Boost = item.SearchItemBoostValue;
                                childquery = lessThanQuery;
                                break;
                            //less than or Equal Query for numeric field.
                            case "lte":
                                NumericRangeQuery lessThanOrEqualTo = new NumericRangeQuery();
                                lessThanOrEqualTo.Field = item.SearchItemKeyword.ToLower();
                                lessThanOrEqualTo.LessThanOrEqualTo = Convert.ToDouble(item.SearchItemValue);
                                lessThanOrEqualTo.Boost = item.SearchItemBoostValue;
                                childquery = lessThanOrEqualTo;
                                break;
                            //Equal Query for numeric field.
                            case "et":
                                TermQuery equalQuery = new TermQuery();
                                equalQuery.Field = item.SearchItemKeyword.ToLower();
                                equalQuery.Value = Convert.ToDouble(item.SearchItemValue);
                                equalQuery.Boost = item.SearchItemBoostValue;
                                childquery = equalQuery;
                                break;
                        }

                        //If IsItemForAll is true then all the condition will be in and condition.
                        if (conditions.FirstOrDefault().IsItemForAll)
                            query &= childquery;
                        else
                            query |= childquery;

                    }

                    queryContainers.Add(query);
                }
                finalBoolQuery.Should = queryContainers;
            }
            return finalBoolQuery;
        }

    }
}
