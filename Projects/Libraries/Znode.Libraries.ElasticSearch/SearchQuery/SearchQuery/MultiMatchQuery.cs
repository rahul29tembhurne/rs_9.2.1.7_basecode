﻿using Nest;
using System;
using System.Collections.Generic;

namespace Znode.Libraries.Search
{
    public class MultiMatchQueryBuilder : BaseQuery, ISearchQuery
    {
        readonly FunctionScoreQuery functionScoreQuery = new FunctionScoreQuery();
        readonly BoolQuery finalBoolQuery = new BoolQuery();

        public SearchRequest<dynamic> GenerateQuery(IZnodeSearchRequest request)
        {
            MultiMatchQuery multiMatchQuery = new MultiMatchQuery();
            List<Field> mutipleFileds = new List<Field>();

            foreach (var item in request.SearchableAttibute)
                mutipleFileds.Add(new Field(item.AttributeCode.ToLower(), item.BoostValue));

            multiMatchQuery.Query = request.SearchText;
            multiMatchQuery.Fields = mutipleFileds.ToArray();

            if (!string.IsNullOrEmpty(GetFeatureValue(request, "MinimumShouldMatch")))
                multiMatchQuery.MinimumShouldMatch = GetFeatureValue(request, "MinimumShouldMatch");

            multiMatchQuery.Operator = GetOperator(request);

            switch (request.SubQueryType?.ToLower())
            {
                case "best":
                    multiMatchQuery.Type = TextQueryType.BestFields;
                    break;

                case "most":
                    multiMatchQuery.Type = TextQueryType.MostFields;
                    break;

                case "cross":
                    multiMatchQuery.Type = TextQueryType.CrossFields;
                    break;

                case "phrase":
                    multiMatchQuery.Type = TextQueryType.Phrase;
                    break;
            }
            //Fuzziness
            string isAutocorrect = request.FeatureList.Find(x => x.FeatureCode == "AutoCorrect")?.SearchFeatureValue;

            if (!string.IsNullOrEmpty(isAutocorrect) && Convert.ToBoolean(isAutocorrect))
            {
                multiMatchQuery.Fuzziness = Fuzziness.Auto;
            }

            functionScoreQuery.ScoreMode = FunctionScoreMode.Sum;
            functionScoreQuery.BoostMode = FunctionBoostMode.Sum;

            functionScoreQuery.Functions = AddFunctionToSearchQuery(request);

            finalBoolQuery.Must = new List<QueryContainer> { multiMatchQuery };

            //set conditions for boost or bury the products.
            BoolQuery boostAndBuryCondition = GetBoostOrBuryItem(request);

            finalBoolQuery.Should = new List<QueryContainer> { boostAndBuryCondition };

            finalBoolQuery.Filter = request.FilterValues;

            functionScoreQuery.Query = finalBoolQuery;

            AggregationBase aggregationBase = GetProductCountAggregation();

            if (request.GetCategoriesHeirarchy)
                aggregationBase = GetCategoryAggregation(aggregationBase);

            if (request.GetFacets && request.FacetableAttribute?.Count > 0)
                aggregationBase = GetFacetAggregation(aggregationBase, request.FacetableAttribute);

            List<ISort> SortSettings = AddSortToSearchQuery(request);

            SearchRequest<dynamic> searchRequest = new SearchRequest<dynamic>(request.IndexName, ElasticLibraryConstants.ElasticProductIndexType);
            searchRequest.Query = functionScoreQuery;
            searchRequest.From = request.StartIndex;
            searchRequest.Size = request.PageSize;

            if (SortSettings.Count > 0)
                searchRequest.Sort = SortSettings;

            searchRequest.Aggregations = aggregationBase;
            searchRequest.Suggest = GetSuggestionQuery(request);

            return searchRequest;
        }
    }
}
