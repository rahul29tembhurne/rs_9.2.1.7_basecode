﻿

var xhr = new XMLHttpRequest();
var calledfrom = '';
var storeId = '';
var productId = '';
/*Called from PDP,CART and STORELocator Use My LOCation Button CLick*/
function GetLocation(c) {
    try
    {
        var ipaddress = $("#hdnIPAddress").val();
        console.log("IPaddress: " + ipaddress);
        calledfrom=c;     
        xhr.onreadystatechange = processRequest;
        //xhr.open('GET', "//ipinfo.io/json", true);
        xhr.open('GET', ipaddress, true);
        xhr.send();
       
    }
    catch(err)
    {
        alert(err.message);
    }
}

/*Called from GetLocation()*/
function processRequest(e) {   
    if (xhr.readyState == 4 && xhr.status == 200) {
        $("#responseerror").text("");
        var response = JSON.parse(xhr.responseText);
        //alert("Response" + response);
        var sku = $("#sku").val();
        $("#storedetails").html('');
        /*StoreLocator Case*/
        if (typeof sku === 'undefined') {           
            sku = '0';
            $("#storedetails").append("<br/>");
        }
        else/*PDP Case*/
        {
            $("#storedetails").append("<div class='col-lg-12 col-md-12 nopadding margin-top'><h1>" + "Select a Store (" + response.region + ")</h1><br/></div>");
        }
        $("#storedetails").show();          
        //var sku = $("#sku").val();                         
        var array = response.loc.split(",");
        var _userCord = new google.maps.LatLng(array[0], array[1]); 
        var response = JSON.parse(xhr.responseText);
        alert("Response" + response);
        //alert("calledfrom-" + calledfrom);
        if (calledfrom == 'pdp'){
            GetStores(response.region, sku, _userCord);
        }
        else if(calledfrom == 'cart')
        {
            GetStoresForCart(response.region, sku, _userCord);
        }
        else if (calledfrom == 'storelocator') {
            GetStoresForStoreLocator(response.region, sku, _userCord);
        }
        
    }
    else
    {
        
       // alert("else");
        var sku = $("#sku").val();
        var _userCord = new google.maps.LatLng("21.1500", "79.1000");
        //var _userCord = new google.maps.LatLng(array[0], array[1]);
        //alert(_userCord);
        GetStores("Maharashtra", sku, _userCord);
        //alert("calledfrom-" + calledfrom);
        if (calledfrom == 'pdp') {
            GetStores("Maharashtra", sku, _userCord);
        }
        else if (calledfrom == 'cart') {
            GetStoresForCart("Maharashtra", sku, _userCord);
        }
        else if (calledfrom == 'storelocator') {
            GetStoresForStoreLocator("Maharashtra", sku, _userCord);
        }
        $("#responseerror").text("It seems currently this service is down, but no problem You can always search for nearest stores by entering your address.")
        return false;
        
    }
    
}

/*******************PDP/STORE LOCATOR METHODS START*****************************/
/*Called from processRequest(e) for both PDP and Store Locator*/
function GetStores(state, sku, _userCord) {
   
    try {
        $.ajax({
            type: "GET",
            url: $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#storedetails").html('');
                $.each(data, function (i, item) {

                    if (i == "StoreLocations") {
                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var _storeCord = new google.maps.LatLng(address['Latitude'], address['Longitude']);

                            var distanceinmeters = google.maps.geometry.spherical.computeDistanceBetween(_userCord, _storeCord);
                            var distanceinmiles = distanceinmeters / 1609.344;
                            address['distance'] = distanceinmiles;
                            json[j].distance = distanceinmiles;

                        });

                        json.sort(function (a, b) {
                            return Math.floor(a.distance) - Math.floor(b.distance)
                        });
                        var cnt = 0;
                        $.each(json, function (j, val) {
                            cnt = cnt + 1;
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var addr = address['StoreAddress'];
                            try {
                                var addressdetail = addr.replace(/^,/, '');
                                addressdetail = addressdetail + "Phone:" + address['PhoneNumber'];
                                //alert(addressdetail);
                            }
                            catch (err) {
                                console.log(err.message);
                            }
                            $("#storedetails").append("<div class='col-lg-12 col-md-12 nopadding' id=" + address['StoreName'] + ">");
                            if (sku == '0') {
                                /*STORE LOCATOR CASE*/
                                $("#storedetails").append("<div class='col-lg-8 col-xs-8 col-md-8 nopadding'>" + "<div class='form-group'><h4>" + address['StoreName'] + "</h4></div>" + "<div class='form-group'> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div class='form-group'>" + addr.replace(/^,/, '') + "</div>" + "<div class='form-group'><button id=" + address['WarehouseId'] + " onclick=' return SaveinCookie(this,\"" + address['StoreName'] + "\",\"" + addressdetail + "\");' class='btn btn-primary' data-dismiss='modal' aria-hidden='true' value='Make this My Store'>Make this My Store</button></div>" + "</div>");
                            }
                            else {
                                /*PDP CASE*/
                                $("#storedetails").append("<div class='col-lg-8 col-xs-8 col-md-8 nopadding'>" + "<div class='form-group'><h4>" + address['StoreName'] + "</h4></div>" + "<div class='form-group'> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div class='form-group'>" + addr.replace(/^,/, '') + "</div>" + "<div class='form-group'><button id=" + address['WarehouseId'] + " onclick='Click(this,\"" + address['StoreName'] + "\",\"" + addressdetail + "\");' class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Make this My Store</button></div>" + "</div>");
                            }

                            $("#storedetails").append("<div class='col-lg-4 col-xs-4 col-md-4 nopadding text-center'>" + "<br/><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> miles away" + "</div>");
                            $("#storedetails").append("</div><br/><hr>");
                            $("#storedetails").show();
                        });
                        if (cnt == 0) {
                            $("#storedetails").append("Sorry No Stores Found for Your State");
                            $("#storedetails").show();
                        }
                    }
                });
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
               // console.log(data.responseText);
            }

        });
    }
    catch (err) {
        console.log(err.message);
    }
}

/*Called from PDP on Make This My Store Click.
Custom values are taken from $("#SelectedWarehouseId"),$("#SelectedWarehouseName"),$("#SelectedWarehouseAddress") and assigned in SetCartItemModelValuesmethod of Product.ts
to be passed on to cart*/
function Click(obj, storename, storeaddress) {
    try {
      
        var selectedstoreid = $(obj).attr('id');      
        $("#SelectedWarehouseId").val(selectedstoreid);
        $("#SelectedWarehouseName").val(storename);
        $("#SelectedWarehouseAddress").val(storeaddress);

        $("#defaultstorename").html('');
        $("#defaultstorename").append("<label class='storename'>" + storename + "</label>" + "&nbsp;<span class='select-store'><a style='text-decoration: underline;' data-toggle='modal' data-target='#searchstore' href='#'>Change Store</a></span>");
        $('.select-store').show();

        $("#Pickup").prop('checked', true);
        //@ajax.Action("DisplayStoreinHeader","CustomUser");
        //$.ajax({ url: "/CustomUser/DisplayStoreinHeader", type: "get" }).done(function (t) { $(".MyStore:first").html(t); });
        $(".StoreLocatorLink").html("<a title='My Store' href='../Home/storeLocator'>" + storename + "</a>");
    }
    catch (err) {
        console.log(err.message);
    }


}

/*Called from PDP on clicking Pick up in store Radio Button*/
function AssignStoreDetails(selectedstoreid, storename, storeaddress) {   
    $("#SelectedWarehouseId").val(selectedstoreid);
    $("#SelectedWarehouseName").val(storename);
    $("#SelectedWarehouseAddress").val(storeaddress);
}

/*Called from PDP on clicking Ship Radio Button*/
function RemoveStoreDetails() {  
    $("#SelectedWarehouseId").val('');
    $("#SelectedWarehouseAddress").val('');
}

function Validate(control, event) {    
    alert("Validate pickup");    
    var isSizeSelected = true;
    var isWidthSelected = true;

    var selectedSize = $('#Size :selected').text();
    var selectedWidth = $('#ShoeWidth :selected').text();   
    if (selectedSize != "Select") {
        $("#SizeError").hide();

    }
    else {
        $("#SizeError").show();
        $("#Size").css("border", "2px red solid", "color", "red");
        isSizeSelected = false;
        return false;
    }
    if (selectedWidth != "Select") {
        $("#WidthError").hide();
    }
    else {
        $("#WidthError").show();
        $("#ShoeWidth").css("border", "2px red solid", "color", "red");
        isWidthSelected = false;
        return false;
    }

    if (isSizeSelected == true && isWidthSelected == true) {
        if (Product.prototype.BindAddOnProductSKU(control, event)) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }

}

function ValidateSwatch(control, event) {
    console.log("ValidateSwatch called");
    var Codes = [];
    var isSizeSelected = true;
    var isWidthSelected = true;
    var IsProductWithWidth = $("#HaveWidth").val();

    $(" input.ConfigurableAttribute:checked").each(function () {       
        Codes.push($(this).attr('code'));
    });
    console.log("1");
    if (Codes.indexOf("Size") == -1) {
        $("#SizeError").show();
        $("#Size").css("border", "2px red solid", "color", "red");
        isSizeSelected = false;
        console.log("isSizeSelected = false;");
        return false;
    }
    else {
        $("#SizeError").hide();
    }
  
    if (IsProductWithWidth=="True" && Codes.indexOf("ShoeWidth") == -1) {
       
        $("#WidthError").show();
        $("#ShoeWidth").css("border", "2px red solid", "color", "red");
        isWidthSelected = false;
        console.log("isWidthSelected = false;");
        return false;
    }
    else {
        $("#WidthError").hide();
    }

    if (isSizeSelected == true && isWidthSelected == true) {
        if (Product.prototype.BindAddOnProductSKU(control, event)) {
            return true;
        }
        else {
            console.log("Product.prototype.BindAddOnProductSKU(control, event) = false;");
            return false;
        }
    }
    else {
        return false;
    }

}
function RedirectToCart() {
    var webstoreURL = $("#WebstoreURL").val();
    //alert(webstoreURL);
    window.location.href = webstoreURL + "/cart";
}
/*******************PDP METHODS END*****************************/




/*******************CART METHODS START*****************************/
function GetStoresForCart(state, sku, _userCord) {   
    try {    
        $.ajax({
            type: "GET",
            url: $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $.each(data, function (i, item) {

                    if (i == "StoreLocations") {
                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var _storeCord = new google.maps.LatLng(address['Latitude'], address['Longitude']);

                            var distanceinmeters = google.maps.geometry.spherical.computeDistanceBetween(_userCord, _storeCord);
                            var distanceinmiles = distanceinmeters / 1609.344;
                            address['distance'] = distanceinmiles;
                            json[j].distance = distanceinmiles;

                        });

                        json.sort(function (a, b) {
                            return Math.floor(a.distance) - Math.floor(b.distance)
                        });
                        var cnt = 0;
                        $("#storedetails").html('');
                        $.each(json, function (j, val) {
                            cnt = cnt + 1;
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var addr = address['StoreAddress'];
                            try {
                                var addressdetail = addr.replace(/^,/, '');
                                addressdetail = addressdetail + "Phone:" + address['PhoneNumber'];                               
                            }
                            catch (err) {
                                console.log(err.message);
                            }
                            $("#storedetails").append("<div class='col-lg-12 col-md-12 nopadding' id=" + address['StoreName'] + ">");
                            $("#storedetails").append("<div class='col-lg-8 col-xs-8 col-md-8 nopadding'>" + "<div class='form-group'><h4>" + address['StoreName'] + "</h4></div>" + "<div class='form-group'><h4 style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div><div class='form-group'>" + addr.replace(/^,/, '') + "</div>" + "<div class='form-group'><p class='storeLocationCoordinate recent-order-reorder'><a href='#' style='text-decoration: underline;'>Map and Directions</a></p></div>" + "<div class='form-group'><button id=" + address['WarehouseId'] + " onclick='ClickCart(this,\"" + address['StoreName'] + "\",\"" + addressdetail + "\",\"" + sku + "\");' class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>Make this My Store</button></div>" + "</div>");
                            

                            $("#storedetails").append("<div class='col-lg-4 col-xs-4 col-md-4 text-center'>" + "<br/><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> miles away" + "</div>");
                            $("#storedetails").append("</div><br/><hr>");
                            $("#storedetails").show();
                        });
                        if (cnt == 0) {
                            $("#storedetails").append("Sorry No Stores Found for Your State");
                            $("#storedetails").show();
                        }
                    }
                });
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
               // console.log(data.responseText);
            }

        });
    }
    catch (err) {
        console.log(err.message);
    }
}

function ClickCart(obj, storename, storeaddress,sku) {
    try {
       
        
        var selectedstoreid = $(obj).attr('id');
        var clickedradiobutton ="#Pickup-"+ $("#hdnProductID").val();        
        $(clickedradiobutton).prop('checked', true);
       
        $("#SelectedWarehouseId").val(selectedstoreid);
        $("#SelectedWarehouseName").val(storename);
        $("#SelectedWarehouseAddress").val(storeaddress);        
       
        $(".defstore").each(function () {           
            $(this).html('');
            $(this).append("<label class='storename'>" + storename + "</label>" + "&nbsp;<span class='select-store'><a style='text-decoration: underline;' data-toggle='modal' data-target='#searchstore' href='#'>Change Store</a></span>");
        });
        
        var productId = $("#ProductId").val();         
        //alert("productId=" + productId);
        $.ajax({
            type: "POST",
            url: "/rscart/UpdateDeliveryPreferenceToPickUp/",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ storeId: selectedstoreid, storeName: storename, storeAddress: storeaddress, productId: productId }),
            processData: false,
            success: function (response) {
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
               // console.log(data.responseText);
                $("#" + selectedstoreid).removeClass('btn btn-primary-1');
                $("#" + selectedstoreid).addClass('btn btn-primary');
            }
        });
        $(".StoreLocatorLink").html("<a title='My Store' href='../Home/storeLocator'>" + storename + "</a>");
        $("#hdnSelectedStoreID").val(storeId);
        $("." + storeId + ":first").prop('checked', true);        
       
           
    }
    catch (err) {
        console.log(err.message);
    }


}

/*Called from Cart on clicking Pick up in store Radio Button*/
function AssignStoreDetailsForCart(selectedstoreid, storename, storeaddress, productId) {
  
    $.ajax({
        type: "POST",
        url: "/rscart/UpdateDeliveryPreferenceToPickUp/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ storeId: selectedstoreid, storeName: storename, storeAddress: storeaddress, productId: productId }),
        processData: false,
        success: function (response) {
        },
        failure: function (data) {
            console.log("Failure-" + data.responseText);
        },
        error: function (data) {
            //console.log(data.responseText);
            $("#" + selectedstoreid).removeClass('btn btn-primary-1');
            $("#" + selectedstoreid).addClass('btn btn-primary');
        }
    });

}

/*Called from Cart on clicking Ship Radio Button*/
function RemoveStoreDetailsForCartOnShipClick(productId) {
    $.ajax({
        type: "POST",
        url: "/rscart/UpdateDeliveryPreferenceToShip/",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ productId: productId }),
        processData: false,
        success: function (response) {
        },
        failure: function (data) {
            console.log("Failure-" + data.responseText);
        },
        error: function (data) {
            //console.log(data.responseText);
            $("#" + selectedstoreid).removeClass('btn btn-primary-1');
            $("#" + selectedstoreid).addClass('btn btn-primary');
        }
    });

}

/*******************CART METHODS END******************************/
   






function FitGuide(fitguideurl) {
    window.open(fitguideurl, "_blank", "toolbar=yes,width=1000,height=700");
}

///Called On Store Locator Make This My Store Click
function SaveinCookie(obj, storename, storeaddress) {
    try {       
        var selectedstoreid = $(obj).attr('id');

        $(".btn-primary-1").each(function () {
            $(this).removeClass('btn btn-primary-1');
            $(this).addClass('btn btn-primary');
            $(this).text('Make This My Store');
        });

        $("#" + selectedstoreid).removeClass('btn btn-primary');
        $("#" + selectedstoreid).addClass('btn btn-primary-1');
        $("#" + selectedstoreid).text('My Store');
       
        $.ajax({
            type: "POST",
            url: "/rshome/saveincookie/",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ storeId: selectedstoreid, storeName: storename, storeAddress: storeaddress }),
            processData: false,
            success: function (response) {
                $(".StoreLocatorLink").html("<a title='My Store' href='../Home/storeLocator'>" + storename + "</a>");
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
                //console.log(data.responseText);
                $("#" + selectedstoreid).removeClass('btn btn-primary');
                $("#" + selectedstoreid).addClass('btn btn-primary');
            }
        });

        return false;

    }
    catch (err) {
        console.log(err.message);
        $("#" + selectedstoreid).removeClass('btn btn-primary');
        $("#" + selectedstoreid).addClass('btn btn-primary');
    }  
    return false;
}
function GetPickUpRadio(obj)
{   
    storeId = $(obj).attr("data-pickupTarget");
    productId = $(obj).attr("data-product");
   
}
/****************************STORE LOCATOR METHODS START*****************************************************/
function GetLatLng() {
    try
    {
        // alert("inside GetLatLng");
        $("#responseerror").text("");
        var addr = $("#txtzipcitystate").val();       
        if (addr == '')
        {
            $("#error").text("*This is a required field.")
            return false;
        }
        else
        {
            $("#error").text("");
        }
        var lat = '';
        var lng = '';
        var address = addr;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                lat = results[0].geometry.location.lat();
                lng = results[0].geometry.location.lng();
               
              //  alert('Latitude: ' + lat + ' Logitude: ' + lng);
                var _userCord = new google.maps.LatLng(lat, lng);
                GetStoresForStoreLocator('ALL', '0', _userCord);
               // initMap();
            }     
            else {
                console.log("Geocode was not successful for the following reason: " + status);
            }
        });
       
    }
    catch(err)
    {
        console.log(err.message);
    }
}

function GetStoresForStoreLocator(state, sku, _userCord) {

    try {
        var imagePath =$("#ImagePathURL").val();
        var webstoreUrl = $("#WebStoreUrl").val();
        
        $.ajax({
            type: "GET",
            url: $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $("#storedetails").html('');
                $.each(data, function (i, item) {

                    if (i == "StoreLocations") {
                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var _storeCord = new google.maps.LatLng(address['Latitude'], address['Longitude']);

                            var distanceinmeters = google.maps.geometry.spherical.computeDistanceBetween(_userCord, _storeCord);
                            var distanceinmiles = distanceinmeters / 1609.344;
                            address['distance'] = distanceinmiles;
                            json[j].distance = distanceinmiles;

                        });

                        json.sort(function (a, b) {
                            return Math.floor(a.distance) - Math.floor(b.distance)
                        });
                        var cnt = 0;
                        $.each(json, function (j, val) {
                            
                            cnt = cnt + 1;
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var addr = address['StoreAddress'];
                            //var storedetailshref = "window.location.href=" + webstoreUrl + address['seourl'];
                            if (address['seourl'] == null) {
                                storedetailshref = "#";
                            }
                            else {
                                storedetailshref = webstoreUrl + '/' + address['seourl'];
                            }
                            //alert(storedetailshref);
                            try {
                                var addressdetail = addr.replace(/^,/, '');
                                addressdetail = addressdetail + "Phone:" + address['PhoneNumber'];
                                //alert(addressdetail);
                            }
                            catch (err) {
                                console.log(err.message);
                            }
                            //$("#storedetails").append("<div class='col-lg-12 col-md-12 nopadding storeLocationCoordinate' id=" + address['StoreName'] + " data-distance='0'  data-lng=" + address['Longitude'] + "  data-lat=" + address['Latitude'] + "  data-title=" + address['StoreName'] + "  data-address=" + addr.replace(/^,/, '') + ">");
                            //$("#storedetails").append("<div class='col-lg-8 col-xs-12 col-md-8 nopadding'>" + "<div class='form-group'><h4>" + address['StoreName'] + "</h4></div>" + "<div class='form-group'><h4 style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div><div class='form-group'>" + addr.replace(/^,/, '') + "</div>" + "<div class='form-group'><p class='storeLocationCoordinate recent-order-reorder'><a href='#' style='text-decoration: underline;'>Map and Directions</a></p></div>" + "<div class='form-group'><button id=" + address['WarehouseId'] + " onclick=' return SaveinCookie(this,\"" + address['StoreName'] + "\",\"" + addressdetail + "\");' class='btn btn-bg-secondary' data-dismiss='modal' aria-hidden='true' value='Make this My Store'>Make this My Store</button></div>" + "</div>");
                            //$("#storedetails").append("<div class='col-lg-4 col-xs-12 col-md-4 text-center'>" + "<br/><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> miles away" + "</div>");
                            //$("#storedetails").append("</div><br/><hr>");

                            var HtmlTags = $("#storedetails").html();
                            HtmlTags += "<div class='col-lg-12 col-md-12 nopadding storeLocationCoordinate' id=" + address['StoreName'] + " data-distance='0'  data-lng=" + address['Longitude'] + "  data-lat=" + address['Latitude'] + "  data-address=" + addr.replace(/^,/, '') + "  data-title=" + address['StoreName'] + ">"
                            HtmlTags += "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12 nopadding-mobile'><img alt=" + address['StoreName'] + " title=" + address['StoreName'] + " src='" + imagePath + address['ImageName'] + "'/></div>"
                            HtmlTags += "<div class='col-lg-8 col-md-8 col-sm-8 col-xs-12 nopadding'>" + "<div class='col-lg-5 col-md-5 col-sm-5 col-xs-12 nopadding-mobile'>" + "<div class='form-group'><h4 style='margin: 0px;'>" + address['StoreName'] + "</h4></div>"
                            HtmlTags += "<div class='form-group'><h4 style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div> <div class='form-group'><div class='store-hours'>" + $("#message").html() + "</div></div></div>"
                            HtmlTags += "<div class='col-lg-4 col-md-4 col-sm-4 nopadding col-xs-12'><div class='form-group'>" + addr.replace(/^,/, '') + "</div>"
                            HtmlTags += "<div class='form-group'><p class='storeLocationCoordinate recent-order-reorder'><a href=" + address['MapLocationURL'] + " target='_blank' style='text-decoration: underline;' title='Map and Directions'>Map and Directions</a></p></div></div>"
                            HtmlTags += "<div class='col-lg-3 col-md-3 col-sm-3 col-xs-12 text-center margin-bottom'><h1><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> miles away" + "</h1></div>"
                            HtmlTags += "<div class='col-lg-12 col-xs-12 col-md-12'><div class='form-inlineclass'><div>"
                            //HtmlTags += "<button class='btn red' onclick=" + storedetailshref + " value='Store Details'>Store Details</button></div>"
                            HtmlTags += "<a style='color: #fff !important;font-weight: normal;' class='btn red' href=" + storedetailshref + " target='_blank' title='Store Details'>Store Details</a></div>"
                            HtmlTags += "<div><button id=" + address['WarehouseId'] + " onclick=' return SaveinCookie(this,\"" + address['StoreName'] + "\",\"" + addressdetail + "\");' class='btn btn-primary' data-dismiss='modal' aria-hidden='true' value='Make this My Store' title='Make this My Store' style='margin-left: 15px;'>Make this My Store</button></div></div></div>" + "</div>";
                            HtmlTags += "</div></div><br/><hr>";

                            $("#storedetails").html(HtmlTags);
                            $("#storedetails").show();
                        });
                        if (cnt == 0) {
                            $("#storedetails").append("Sorry No Stores Found for Your State");
                            $("#storedetails").show();
                        }
                    }
                });
            },
            failure: function (data) {
                console.log("Failure-" + data.responseText);
            },
            error: function (data) {
               // console.log(data.responseText);
            }

        });
    }
    catch (err) {
        console.log(err.message);
    }
}

function GetMapURL(storename,ctrl) {

    var state = "ALL";
    var sku = "0";
    var locationMapUrl = ""; //alert(ctrl.attr("onclick")); alert(ctrl.attr("href"));
    $.ajax({
        type: "GET",
        url: $("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
           // $("#storedetails").html('');
            $.each(data, function (i, item) {

                if (i == "StoreLocations") {
                    var json = JSON.parse(JSON.stringify(data[i]));
                    $.each(json, function (j, val) {
                        var address1 = JSON.stringify(json[j]);
                        var address = JSON.parse(address1); 
                        if (address['StoreName'] == storename) {
                            locationMapUrl = address['MapLocationURL'];
                            //return window.open(locationMapUrl, '_blank');
                             window.location.href = locationMapUrl;
                            //locationMapUrl=locationMapUrl.replace("'", "");
                            //window.open(locationMapUrl, '_blank');
                           // alert("In");
                           // $(ctrl).closest("lnkMapDirection").attr("href", locationMapUrl);
                           // $(this).closest("lnkMapDirection").click();
                        }
                        //alert(address['StoreName']);
                    });
                }
            });
        },
        failure: function (data) {
            console.log("Failure-" + data.responseText);
        },
        error: function (data) {
           // console.log(data.responseText);
        }

    });


}
/****************************STORE LOCATOR METHODS END*************************************************/

/****************************_ProductAttributes.cshtml METHODS START*************************************************/


function RedirectToCart()
{
    var webstoreURL= $("#WebstoreURL").val();
    alert(webstoreURL);
    window.location.href = webstoreURL+ "/cart";
}
function AddQty()
{
    
    var Add = parseFloat($("#Quantity").val());
    if(Add == 1)
    {
        $("#Quantity").val(Add + 1);
        $("#Subtract").addClass('disabled').removeAttr("href");
    }

    if(Add > 1)
    {
        $("#Quantity").val(Add + 1);
        $("#Subtract").removeClass("disabled").attr("href", "#");
    }
}
function SubQty()
{
    
    var Add = parseFloat($("#Quantity").val());
    if(Add == 1)
    {
        $("#Subtract").addClass('disabled').removeAttr("href");
    }
    else
    {
        $("#Quantity").val(Add - 1);
    }
}
/****************************_ProductAttributes.cshtml METHODS END*************************************************/