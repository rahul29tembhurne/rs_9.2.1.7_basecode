﻿
//----------------------------------//
// Add smooth scrolling to Reviews links - ProductContent.cshtml Page
function scroll_to_div(div_id)
{
    $('html,body').animate(
    {
        scrollTop: $("#"+div_id).offset().top
    },
    'slow');
}

//**************************************************************//

//Modale Zoom Image - ProductImage.cshtml Page
function ChangeImage(img) {
    //$("#product-image").attr('src', img);
    //$("#product-lens-image").attr('href', zoomimg);
    //$(".zoomImg:first").attr('src', zoomimg);
    $("#product-image").attr('src', img);
    $("#product-lens-image").attr('href', img);
    $(".zoomImg:first").attr('src', img);

}
$(document).ready(function () {
    $('.ex1').zoom();
});


$('.item').on('click', 'a', function (e) {
    var $this = $(this);
    e.preventDefault();
    // Use EasyZoom's `swap` method
    //api1.swap($this.data('standard'), $this.attr('href'));
});

//-------------------------------------//
/* activate the carousel  Zoom Image Modal Call - ProductImage.cshtml Page*/
$("#modal-carousel").carousel({ interval: false });

/* change modal title when slide changes */
$("#modal-carousel").on("slid.bs.carousel", function () {
    $(".modal-title")
    .html($(this)
    .find(".active img")
    .attr("title"));
});

/* when clicking a thumbnail */
$(".row .abc").click(function () {
    var content = $(".carousel-inner");
    var title = $(".modal-title");

    content.empty();
    title.empty();

    var id = this.id;
    var repo = $("#img-repo .item");
    var repoCopy = repo.filter("#" + id).clone();
    var active = repoCopy.first();

    active.addClass("active");
    title.html(active.find("img").attr("title"));
    content.append(repoCopy);

    // show the modal
    $("#modal-gallery").modal("show");
});

//Nivi Code
//Video Code Js//

$(function () {
    $(".video").click(function ()
    {
        var t = $(this).data("target"), a = $(this).attr("data-video"), i = a + "?modestbranding=1&rel=0&controls=0&showinfo=0&html5=1&autoplay=0"; $(t + " iframe").attr("src", i),
            $(t + " button.close").click(function ()
        { $(t + " iframe").attr("src", a) })
    })
});
//

//--------------------------------------------///
/*Plus Minus click on add qty */
function wcqib_refresh_quantity_increments() {
    jQuery("div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)").each(function (a, b) {
        var c = jQuery(b);
        c.addClass("buttons_added"), c.children().first().before('<input type="button" value="-" class="minus" />'), c.children().last().after('<input type="button" value="+" class="plus" />')
    })
}
String.prototype.getDecimals || (String.prototype.getDecimals = function () {
    var a = this,
        b = ("" + a).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
    return b ? Math.max(0, (b[1] ? b[1].length : 0) - (b[2] ? +b[2] : 0)) : 0
}), jQuery(document).ready(function () {
    wcqib_refresh_quantity_increments()
}), jQuery(document).on("updated_wc_div", function () {
    wcqib_refresh_quantity_increments()
}), jQuery(document).on("click", ".plus, .minus", function () {
    var a = jQuery(this).closest(".quantity").find(".qty"),
        b = parseFloat(a.val()),
        c = parseFloat(a.attr("max")),
        d = parseFloat(a.attr("min")),
        e = a.attr("step");
    b && "" !== b && "NaN" !== b || (b = 0), "" !== c && "NaN" !== c || (c = ""), "" !== d && "NaN" !== d || (d = 0), "any" !== e && "" !== e && void 0 !== e && "NaN" !== parseFloat(e) || (e = 1), jQuery(this).is(".plus") ? c && b >= c ? a.val(c) : a.val((b + parseFloat(e)).toFixed(e.getDecimals())) : d && b <= d ? a.val(d) : b > 0 && a.val((b - parseFloat(e)).toFixed(e.getDecimals())), a.trigger("change")
});