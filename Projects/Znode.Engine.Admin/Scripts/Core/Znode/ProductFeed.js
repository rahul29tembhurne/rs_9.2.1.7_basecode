var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var ProductFeed = /** @class */ (function (_super) {
    __extends(ProductFeed, _super);
    function ProductFeed() {
        var _this = _super.call(this) || this;
        ProductFeed.prototype._multiFastItemdata = new Array();
        return _this;
    }
    ProductFeed.prototype.Init = function () {
        ProductFeed.prototype.DisplayXMLSiteMapType();
        ProductFeed.prototype.SetFeedIsSelectAllPortalOnInit();
    };
    ProductFeed.prototype.DisplayCustomDate = function (control) {
        if ($.trim($(control).val()) === "Use date / time of this update") {
            $('#CustomDate-content').show();
        }
        else {
            $('#CustomDate-content').hide();
        }
        if ($.trim($(control).val()) === "Use the database update date") {
            var dt = new Date(Date.now());
            var date = dt.toLocaleDateString() + " " + dt.toLocaleTimeString();
            $('#DBDate').val(date);
        }
    };
    ProductFeed.prototype.DisplayXMLSiteMapType = function () {
        if ($('#ddlXMLSiteMap').val() == 'XmlSiteMap') {
            $('#rdbXMLSiteMapType').show();
            $('#GoogleFeedFields').hide();
        }
        else if ($('#ddlXMLSiteMap').val() == 'Google' || $('#ddlXMLSiteMap').val() == 'Bing') {
            $('#rdbXMLSiteMapType').hide();
            $('#GoogleFeedFields').show();
        }
    };
    ProductFeed.prototype.ShowHideStoreList = function (ctrl) {
        if (ctrl != '') {
            if (ctrl.checked) {
                $(".chkStoresList").hide();
                $("#StoreName-error").text('').removeClass("field-validation-error").hide();
                $("#txtPortalName").parent("div").removeClass('input-validation-error');
            }
            else {
                ZnodeBase.prototype.SetInitialMultifastselectInput(null, null, $("#txtPortalName"));
                $(".chkStoresList").show();
            }
        }
    };
    ProductFeed.prototype.DeleteProductFeed = function (control) {
        var productFeedId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (productFeedId.length > 0) {
            Endpoint.prototype.DeleteProductFeed(productFeedId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    };
    ProductFeed.prototype.CheckPortalCheckBoxByIds = function (ids, lastModification) {
        var arr = ids.split(',');
        for (var i = 0; i < arr.length; i++) {
            $('input:checkbox[name="PortalId"][value=' + arr[i] + ']').prop('checked', true);
            if (arr[i] == '0') {
                $(".chkStoresList").find("input[type=checkbox]").each(function () {
                    $(this).prop('checked', false);
                });
                $(".chkStoresList").hide();
            }
        }
        if (lastModification === "Use date / time of this update") {
            $('#CustomDate-content').show();
        }
        else {
            $('#CustomDate-content').hide();
        }
    };
    ProductFeed.prototype.SetPortals = function () {
        ProductFeed.prototype._multiFastItemdata = new Array();
        if ($('input:checkbox[name="PortalId"]').prop('checked') == false) {
            $("#Stores").val();
            $(".chkStoresList").find(".fstChoiceItem").each(function () {
                if ($(this).data('value') != undefined)
                    ProductFeed.prototype._multiFastItemdata.push($(this).data('value'));
            });
            if (ProductFeed.prototype._multiFastItemdata.length == 0) {
                $("#StoreName-error").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorSelectPortal")).addClass("field-validation-error").show();
                $("#txtPortalName").parent("div").addClass('input-validation-error');
                return false;
            }
            else {
                $("#StoreName-error").text('').removeClass("field-validation-error").hide();
                $("#txtPortalName").parent("div").removeClass('input-validation-error');
            }
        }
        else {
            ProductFeed.prototype._multiFastItemdata.push("0");
            $("#StoreName-error").text('').removeClass("field-validation-error").hide();
            $("#txtPortalName").parent("div").removeClass('input-validation-error');
        }
        $("#txtPortalName").val(ProductFeed.prototype._multiFastItemdata);
        $("#Stores").val(ProductFeed.prototype._multiFastItemdata);
        return true;
    };
    ProductFeed.prototype.SetFeedIsSelectAllPortalOnInit = function () {
        if ($('input:checkbox[name="PortalId"]').prop('checked') == true) {
            $(".chkStoresList").hide();
        }
        else {
            if (($('#Stores').val() != undefined) && ($('#Stores').val() != "")) {
                var portalsArray = $('#Stores').val().split(',');
                Endpoint.prototype.GetPortalList(Constant.storelist, function (response) {
                    ZnodeBase.prototype.SetInitialMultifastselectInput(portalsArray, response, $("#txtPortalName"));
                });
            }
            else {
                ZnodeBase.prototype.SetInitialMultifastselectInput(null, null, $("#txtPortalName"));
            }
            $(".chkStoresList").show();
        }
    };
    ProductFeed.prototype.GenerateProductFeed = function (url) {
        Endpoint.prototype.GenerateProductFeed(url, function (response) {
            if (response.success) {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "success", isFadeOut, fadeOutTime);
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "error", isFadeOut, fadeOutTime);
            }
        });
    };
    return ProductFeed;
}(ZnodeBase));
$(document).off("click", "#ZnodeProductFeed .z-download");
$(document).on("click", "#ZnodeProductFeed .z-download", function (e) {
    e.preventDefault();
    ProductFeed.prototype.GenerateProductFeed($(this).attr('href'));
});
//# sourceMappingURL=ProductFeed.js.map