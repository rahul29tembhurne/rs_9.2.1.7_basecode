﻿class Quote extends ZnodeBase {
    constructor() {
        super();
    }

    Init(): any {
        Order.prototype.Init();
    }

    HideConvertToOrderColumn(): void {
        $('#grid tbody tr').each(function () {
            $(this).find("td").each(function () {
                if ($(this).hasClass('grid-action')) {
                    if ($(this).next().children().hasClass("z-active")) {
                        $(this).children().children("ul").children().find(".z-orders").parent().hide();
                    }
                }
            });
            $(this).find("td.IsConvertedToOrder").each(function () {
                if ($(this).children("i").hasClass("z-active")) {
                    $(this).next().children().children("ul").children().find(".z-orders").parent().hide();
                }
            });
        });
        $('#grid th').each(function () {
            if ($.trim($(this).text()) == "Is Converted To Order")
                $(this).hide();
        });
        $('#grid').find(".IsConvertedToOrder").hide();
    }
}