var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var User = /** @class */ (function (_super) {
    __extends(User, _super);
    function User() {
        var _this = _super.call(this) || this;
        User.prototype._multiFastItemdata = new Array();
        return _this;
    }
    User.prototype.Init = function () {
        $("#ddlUserType").off("change");
        $("#ddlUserType").on("change", function () {
            $("#hdnRoleName").val($("#ddlUserType option:selected").text());
            Account.prototype.ShowHidePermissionDiv();
        });
        $("#ddlUserType").change();
        Account.prototype.ShowHidePermissionDiv();
        Account.prototype.ValidateAccountsCustomer();
        User.prototype.GetUserPermissionList();
        $("#rolelist").on("change", function () {
            $("#hdnRoleName").val($("#rolelist option:selected").text());
        });
        $("#rolelist").change();
        User.prototype.SetRoleName();
        User.prototype.SetIsSelectAllPortalOnInit();
        User.prototype.SubmitOnEnterKey();
    };
    User.prototype.SetRoleName = function () {
        $("#ddlUserType").on("change", function () {
            $("#hdnRoleName").val($("#ddlUserType option:selected").text());
        });
    };
    User.prototype.DeleteUsers = function (control) {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            Endpoint.prototype.DeleteUsers(accountIds, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    };
    User.prototype.DeleteCustomer = function (control) {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            Endpoint.prototype.DeleteCustomer(accountIds, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    };
    User.prototype.EnableUserAccount = function () {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.EnableDisableUserAccount(accountIds, true, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/User/UsersList";
            });
            ZnodeBase.prototype.HideLoader();
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    };
    User.prototype.DisableUserAccount = function () {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.EnableDisableUserAccount(accountIds, false, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/User/UsersList";
            });
            ZnodeBase.prototype.HideLoader();
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    };
    User.prototype.EnableCustomerAccount = function () {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.EnableDisableCustomerAccount(accountIds, true, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/Customer/CustomersList";
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    };
    User.prototype.DisableCustomerAccount = function () {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        ;
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.EnableDisableCustomerAccount(accountIds, false, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/Customer/CustomersList";
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    };
    User.prototype.UserResetPassword = function () {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.UserResetPassword(accountIds, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/User/UsersList";
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    };
    User.prototype.CustomerResetPassword = function () {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.CustomerResetPassword(accountIds, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/Customer/CustomersList";
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    };
    User.prototype.ResetPasswordCustomer = function () {
        var accountId = $("#AccountId").val();
        window.location.href = window.location.protocol + "//" + window.location.host + "/customer/singleresetpassword?accountId=" + accountId;
    };
    User.prototype.ResetPasswordUsers = function () {
        var userId = $("#divAddCustomerAsidePanel #UserId").val();
        if (userId == undefined)
            userId = $("#UserId").val();
        ZnodeBase.prototype.ShowLoader();
        Endpoint.prototype.SingleResetPassword(userId, function (res) {
            ZnodeBase.prototype.HideLoader();
            var errorType = 'error';
            if (res.status) {
                errorType = 'success';
            }
            ZnodeBase.prototype.CancelUpload("divAddCustomerAsidePanel");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, errorType, isFadeOut, fadeOutTime);
        });
    };
    User.prototype.SetIsSelectAllPortal = function () {
        User.prototype._multiFastItemdata = new Array();
        if ($("#AllStoresCheck").val() != undefined) {
            if ($("#AllStoresCheck").prop('checked')) {
                $("#IsSelectAllPortal").val("true");
                $("#txtPortalIds").val(User.prototype._multiFastItemdata.push("0"));
                $("#PortalIdString").val("");
            }
            else {
                $("#IsSelectAllPortal").val("false");
                $("#areaList").find(".fstChoiceItem").each(function () {
                    if ($(this).data('value') != undefined)
                        User.prototype._multiFastItemdata.push($(this).data('value'));
                });
                $("#txtPortalIds").val(User.prototype._multiFastItemdata);
            }
        }
        return true;
    };
    User.prototype.SetIsSelectAllPortalOnInit = function () {
        $("#AllStoresCheck").on("change", function () {
            if ($(this).prop('checked')) {
                $("#IsSelectAllPortal").val("true");
                $("#divPortalIds").hide();
            }
            else {
                $("#IsSelectAllPortal").val("false");
                $("#divPortalIds").show();
            }
        });
        if ($("#IsSelectAllPortal").val() == "True") {
            $("#AllStoresCheck").attr('checked', 'checked');
            $(".storediv").hide();
        }
        else if (!$('#txtPortalIds').prop('disabled')) {
            if (($('#PortalIdString').val() != undefined) && ($('#PortalIdString').val() != "")) {
                var portalsArray = $('#PortalIdString').val().split(',');
                Endpoint.prototype.GetPortalList(Constant.storelist, function (response) {
                    ZnodeBase.prototype.SetInitialMultifastselectInput(portalsArray, response, $("#txtPortalIds"));
                });
            }
            else {
                ZnodeBase.prototype.SetInitialMultifastselectInput(null, null, $("#txtPortalIds"));
            }
            $(".storediv").show();
        }
    };
    User.prototype.ShowHideStoreListinput = function (ctrl) {
        if (ctrl != '') {
            if (ctrl.checked) {
                $(".fstElement").removeClass('input-validation-error');
                $("#errorRequiredStore").text('').text("").removeClass("field-validation-error").hide();
                $(".storediv").hide();
            }
            else {
                ZnodeBase.prototype.SetInitialMultifastselectInput(null, null, $("#txtPortalIds"));
                $(".storediv").show();
            }
        }
    };
    User.prototype.ShowHidePortals = function () {
        if ($("#IsSelectAllPortal").val() == "True") {
            $("#divPortalIds").hide();
        }
        else {
            $("#divPortalIds").show();
        }
        $("#chkIsSelectAllPortal").on("change", function () {
            if ($(this).prop('checked')) {
                $("#IsSelectAllPortal").val("true");
                $("#divPortalIds").hide();
            }
            else {
                $("#IsSelectAllPortal").val("false");
                $("#divPortalIds").show();
            }
        });
    };
    User.prototype.ClickSelectAllPortal = function () {
        $(".chkPortal").click(function () {
            if ($(this).prop('checked')) {
                if ($('.chkPortal:checked').length == ($('.chkPortal').length / 2)) {
                    $("#chkIsSelectAllPortal").prop('checked', 'checked');
                    $("#IsSelectAllPortal").val("true");
                    $("#divPortalIds").hide();
                }
            }
        });
    };
    User.prototype.SubmitOnEnterKey = function () {
        $("#btnPassword").keypress(function (e) {
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                $('button[type=submit]').click();
                return true;
            }
        });
        return true;
    };
    User.prototype.OnAccountSelection = function () {
        var selectedAccount = $("#AccountId").val();
        if (selectedAccount == 0 && selectedAccount == "") {
            $('#ddlDepartment').children('option:not(:first)').remove();
            $('#ddlAccountType').children('option:not(:first)').remove();
            $('#divDepartmentId').hide();
            $('#divUserTypeId').hide();
            $('#divRole').hide();
            $('#ddlPortals').show();
            $('#customer_general_information').show();
            return false;
        }
        Endpoint.prototype.GetAccountDepartmentList(selectedAccount, function (response) {
            $('#ddlDepartment').children('option:not(:first)').remove();
            for (var i = 0; i < response.length; i++) {
                var opt = new Option(response[i].Text, response[i].Value);
                $('#ddlDepartment').append(opt);
            }
            $('#divDepartmentId').show();
        });
        Endpoint.prototype.GetRoleList(function (response) {
            $('#ddlUserType').children('option').remove();
            for (var i = 0; i < response.length; i++) {
                if (response[i].Value == $("#hdnRoleName").val())
                    var opt = new Option(response[i].Text, response[i].Value, false, true);
                else
                    var opt = new Option(response[i].Text, response[i].Value);
                $('#ddlUserType').append(opt);
                $('#divUserTypeId').show();
            }
        });
        $('#divRole').show();
        $('#ddlPortals').show();
        $('#customer_general_information').show();
        $('#errorSelectAccountId').html("");
    };
    User.prototype.OnUserTypeSelection = function () {
        var selectedRole = $("#ddlUserType option:selected").text();
        if (selectedRole == null || selectedRole == "") {
            $('#ddlRole').children('option:not(:first)').remove();
            $('#divRole').show();
            return false;
        }
        var selectedAccount = $("#AccountId").val();
        if (selectedAccount == '') {
            selectedAccount = 0;
        }
        $('#divRole').show();
        Endpoint.prototype.GetPermissionList(selectedAccount, $("#AccountPermissionAccessId").val(), function (response) {
            $('#permission_options').html("");
            $('#permission_options').html(response);
            $("#ddlPermission").attr("onchange", "User.prototype.OnPermissionSelection();");
        });
        $("#ddlPermission").change();
    };
    User.prototype.OnPermissionSelection = function () {
        var permission = $("#ddlPermission option:selected").attr('data-permissioncode');
        var $sel = $("#divRole");
        var value = $sel.val();
        var text = $("option:selected", $sel).text();
        $('#PermissionCode').val(permission);
        $('#PermissionsName').val(text);
    };
    User.prototype.GetSelectedAccount = function () {
        $("#grid").find("tr").click(function () {
            var accountName = $(this).find("td[class='accountnamecolumn']").text();
            var accountId = $(this).find("td")[0].innerHTML;
            $("#PortalId").val($(this).find("td[class='portalId']").text());
            $("#ddlUserType option:selected").prop("selected", false);
            if (accountId != undefined && accountName != undefined) {
                $('#AccountName').val(accountName);
                $('#selectedAccountName').val(accountName);
                $('#AccountId').val(accountId);
                $('#accountListId').hide(700);
                GiftCard.prototype.GetActiveCurrencyToStore("");
                $("#ZnodeUserAccountList").html("");
                ZnodeBase.prototype.RemovePopupOverlay();
                User.prototype.OnAccountSelection();
                return;
            }
            else {
                ZnodeBase.prototype.RemovePopupOverlay();
                return;
            }
        });
    };
    //This method is used to get account list on aside panel.
    User.prototype.GetAccountList = function (selectedPortal) {
        $("#divRole").hide();
        $("#maxBudgetDiv").hide();
        if (selectedPortal == 0)
            selectedPortal = $("#PortalId").val();
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Customer/GetAccountList?portalId=' + selectedPortal, 'accountListId');
    };
    //Clear selected account name.
    User.prototype.ClearAccountName = function () {
        $('#AccountName').val(undefined);
        $('#AccountId').val(undefined);
        $('#ddlDepartment').children('option').remove();
        $('#ddlAccountType').children('option').remove();
        $('#ZnodeUserAccountList').html("");
        $('#ddlUserType').children('option').remove();
        $('#divDepartmentId').hide();
        $('#divUserTypeId').hide();
        $('#divRole').hide();
        $('#ddlPortals').show();
        $('#hdnRoleName').val("");
        return false;
    };
    User.prototype.CheckIsAllPortalSelected = function () {
        if ($("#IsSelectAllPortal").val() == "True") {
            $("#areaList ul li input:checkbox").click();
        }
    };
    User.prototype.ConvertToOrder = function () {
        window.location.href = $("#hdnOrderURL").val();
    };
    User.prototype.GetUserPermissionList = function () {
        var selectedAccount = $("#AccountId").val();
        if (selectedAccount == '' || selectedAccount == null) {
            selectedAccount = 0;
        }
        Endpoint.prototype.GetPermissionList(selectedAccount, $("#AccountPermissionAccessId").val(), function (response) {
            $('#permission_options').html("");
            $('#permission_options').html(response);
            $("#ddlPermission").attr("onchange", "User.prototype.OnPermissionSelection();");
        });
    };
    User.prototype.ValidateUser = function (backURL) {
        var userName = $("#UserName").val();
        var isSubmit = true;
        var isValidate = User.prototype.SaveAdminUser();
        if (isValidate) {
            Endpoint.prototype.IsUserNameAnExistingShopper(userName, function (response) {
                if (typeof (backURL) != "undefined")
                    $.cookie("_backURL", backURL, { path: '/' });
                if (response) {
                    if ($("#UserId").val() == 0) {
                        $(".drop-panel-overlay").show();
                        $("#ConfirmPopup").show();
                        isSubmit = false;
                    }
                }
                if (isSubmit)
                    $("#frmcreateeditstoreadmin").submit();
            });
        }
    };
    User.prototype.ConvertShopperToAdmin = function () {
        $("#frmcreateeditstoreadmin").attr('action', 'ConvertShopperToAdmin');
        $("#frmcreateeditstoreadmin").submit();
    };
    User.prototype.SaveAdminUser = function () {
        var isValidate = true;
        $("#txtPortalName").val('');
        $('#PortalIds').val("0");
        $('#PortalId').val("0");
        $('#hdnPortalId').val("0");
        if ($("#UserName").val() == "") {
            $("#UserName").addClass('input-validation-error');
            $("#errorRequiredUserName").text(ZnodeBase.prototype.getResourceByKeyName("ErrorUsernameRequired")).addClass("text-danger").show();
            isValidate = false;
        }
        if ($("#AllStoresCheck").val() != undefined) {
            if ($("#AllStoresCheck").prop('checked')) {
                $(".fstElement").css({ "background-color": "#e7e7e7" });
                $(".fstElement").removeClass('input-validation-error');
                $("#errorRequiredStore").text('').text("").removeClass("field-validation-error").hide();
                $("#txtPortalName").removeClass('input-validation-error');
            }
            else {
                if ($("#txtPortalIds").val() == "") {
                    $(".fstElement").addClass('input-validation-error');
                    $("#errorRequiredStore").text(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneStore")).show();
                    isValidate = false;
                }
                else {
                    $(".fstElement").removeClass('input-validation-error');
                    $("#errorRequiredStore").text('').removeClass("field-validation-error").hide();
                }
            }
        }
        return isValidate;
    };
    return User;
}(ZnodeBase));
//# sourceMappingURL=User.js.map