﻿declare module Znode.Core {
    interface BaseModel {
        uri: string;
        properties: { [key: string]: string };
    }
    interface MultiSelectDDlModel extends BaseModel {
        Value: any;
        IsAjax: boolean;
        IsMultiple: boolean;
        Controller: string;
        Action: string;
        ItemIds: string[];
        SuccessCallBack: any;
        flag: boolean;
    }

    interface ServerConfigurationDDlModel extends BaseModel {
        ServerId: number;
        PartialViewName: string;
        SuccessCallBack: any;
        flag: boolean;
    }

    type ProductDetailModel = {
        ProductId: number;
        SKU: string;
        MainProductSKU: string;
        Quantity: string;
        DecimalValue: number;
        DecimalPoint: number;
        InventoryRoundOff: number;
        QuantityError: string;
        MainProductId: number;
    }

    type OrderLineItemModel = {
        OrderId: number;
        Guid: string;
        Quantity: string;
        CustomQuantity: string;
        UnitPrice: number;        
        TrackingNumber: string;
        OrderLineItemStatusId: number;
        ReasonForReturnId: number;
        OrderLineItemStatus: string;
        ReasonForReturn: string;
        ProductId: number;
        IsShippingReturn: boolean;
        PartialRefundAmount: string;
    }

    type CartModel = {
        PublishProductId: number;
        SKU: string;
        ProductName: string;
        Quantity: number;
        ProductType: string;
    }

    type PortalPaymentApproverViewModel = {
        ApprovalUserIds: string[];
        PaymentSettingIds: string[];
        PortalPaymentGroupId: number;
    }

    type ApproverDetailsViewModel = {
        EnableApprovalManagement: boolean;
        ApprovalUserIds: string[];
        PortalApprovalTypeId: number;
        PortalApprovalLevelId: number;
        OrderLimit: number;
        PortalId: number;
        PortalPaymentGroupId: number;
        PortalApprovalId: number;
        PaymentTypeIds: string[];
        PortalPaymentUserApproverList: Array<PortalPaymentApproverViewModel>
    }

    type ProductParameterModel = {
        PublishProductId: number;
        LocaleId: number;
        PortalId: number;
        UserId: number;
        OMSOrderId: number;
    }

    type AddOnDetails = {
        ParentSKU: string;
        SKU: string;
        Quantity: string;
        ParentProductId: number;
    }
}

declare module System.Collections.Generic {
    interface KeyValuePair<TKey, TValue> {
        key: TKey;
        value: TValue;
    }
}
declare module System {
    interface Guid {
    }
    interface Tuple<T1, T2> {
        item1: T1;
        item2: T2;
    }
}

module Constant {
    export const GET = "GET";
    export const json = "json";
    export const Function = "function";
    export const string = "string";
    export const object = "object";
    export const innerLoderHtml = "<div class='loader-inner' style='margin:0 auto;text-align:center;padding:20px;'><img src='../Content/Images/throbber.gif' alt='Loading' class='dashboard-loader' /></div>"
    export const configurableProduct = "ConfigurableProduct";
    export const simpleProduct = "SimpleProduct";
    export const groupedProduct = "GroupedProduct";
    export const bundleProduct = "BundleProduct";
    export const addOns = "AddOns";
    export const ZnodeCustomerShipping = "ZnodeCustomerShipping";
    export const GuestUser = "Guest User";
    export const gocoderGoogleAPI = $("#gocoderGoogleAPI").val();//To be fetched from config file
    export const gocoderGoogleAPIKey = $("#gocoderGoogleAPIKey").val();//To be fetched from config file 
    export const inventory = "Inventory";
    export const category = "Category";
    export const seo = "SEO";
    export const defaultAdmin = "admin@znode.com";
    export const CATALOG = "Catalog";
    export const image = "Image";
    export const AmericanExpressCardCode = "AMEX";
    export const shippingSettings = "ShippingSettings";
    export const productSetting = "ProductSetting";
    export const productDetails = "ProductDetails";
    export const storelist = "Storelist";
}

module ErrorMsg {
    export const CallbackFunction = "Callback is not defined. No request made.";
    export const APIEndpoint = "API Endpoint not available: ";
    export const InvalidFunction = "invalid function name : ";
    export const ErrorMessageForCategoryCode = "Alphanumeric values are allowed,Must contain at least one alphabet in CategoryCode."
}
