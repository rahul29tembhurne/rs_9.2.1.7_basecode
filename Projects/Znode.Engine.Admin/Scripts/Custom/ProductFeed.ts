﻿class ProductFeed extends ZnodeBase {
    _Model: any;
    _endPoint: Endpoint;

    constructor() {
        super();
    }

    Init(): any {
        ProductFeed.prototype.DisplayXMLSiteMapType();
    }

    DisplayCustomDate(control): any  {
        if ($.trim($(control).val()) === "Use date / time of this update") {
            $('#CustomDate-content').show();
        }
        else {
            $('#CustomDate-content').hide();
        }

        if ($.trim($(control).val()) === "Use the database update date") {
            var dt = new Date(Date.now());
            var date = dt.toLocaleDateString() + " " + dt.toLocaleTimeString();
            $('#DBDate').val(date);
        }
    }

    DisplayXMLSiteMapType(): any {
        if ($('#ddlXMLSiteMap').val() == 'XmlSiteMap') {
            $('#rdbXMLSiteMapType').show();
            $('#GoogleFeedFields').hide();
        } else if ($('#ddlXMLSiteMap').val() == 'Google' || $('#ddlXMLSiteMap').val() == 'Bing') {
            $('#rdbXMLSiteMapType').hide();
            $('#GoogleFeedFields').show();
        } 
    }

    ShowHideStoreList(ctrl): any {
        if (ctrl != '') {
            if (ctrl.checked) {
                $(".chkStoresList").find("input[type=checkbox]").each(function () {
                    $(this).prop('checked', false);
                });
                $(".chkStoresList").hide();
            } else {
                $(".chkStoresList").show();
            }
        }
    }

    public DeleteProductFeed(control): void {
        var productFeedId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (productFeedId.length > 0) {
            Endpoint.prototype.DeleteProductFeed(productFeedId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    public CheckPortalCheckBoxByIds(ids: string, lastModification: string): void {
        var arr = ids.split(',');
        for (var i = 0; i < arr.length; i++) {
            $('input:checkbox[name="PortalId"][value=' + arr[i] + ']').prop('checked', true);
            if (arr[i] == '0') {
                $(".chkStoresList").find("input[type=checkbox]").each(function () {
                    $(this).prop('checked', false);
                });
                $(".chkStoresList").hide();
            }
        }

        if (lastModification === "Use date / time of this update") {
            $('#CustomDate-content').show();
        }
        else {
            $('#CustomDate-content').hide();
        }
    }

    public GenerateProductFeed(url): void {
        Endpoint.prototype.GenerateProductFeed(url, function (response) {
            if (response.success) {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "success", isFadeOut, fadeOutTime);
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "error", isFadeOut, fadeOutTime);
            }
        });
    }
}

$(document).off("click", "#ZnodeProductFeed .z-download");
$(document).on("click", "#ZnodeProductFeed .z-download", function (e) {
    e.preventDefault();
    ProductFeed.prototype.GenerateProductFeed($(this).attr('href'));
});