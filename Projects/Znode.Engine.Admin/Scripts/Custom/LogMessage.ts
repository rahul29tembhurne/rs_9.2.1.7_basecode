﻿class LogMessage extends ZnodeBase {
    _Model: any;
    _endPoint: Endpoint;
    _callBackCount = 0;
    isMoveFolder;
    localeId: number;
    constructor() {
        super();

    }

    ConfigureLogs(): any {
        $("#frmConfigureLogs").attr('action', 'ConfigureLogs');
        $("#frmConfigureLogs").submit();
    }

    PurgeLogs(): any {
        var logCategoryIds = $("#LogCategoryIdToBeDeleted").val();
        ZnodeBase.prototype.ShowLoader();
        $.ajax({
            url: "/LogMessage/PurgeLogs?logCategoryIds=" + logCategoryIds,
            type: 'POST',
            success: function (response) {
                ZnodeBase.prototype.HideLoader();
                if (response.status)
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "success", isFadeOut, fadeOutTime);
                else
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "error", isFadeOut, fadeOutTime);
            }
        });
    }

    DeleteLogsPopup(): any {
        $("#LogsDeletePopup").modal('show')
    }

    PurgeLogsPopup(zPublishAnchor) {
        if (zPublishAnchor != null) {
            zPublishAnchor.attr("href", "#");
            $("#LogCategoryIdToBeDeleted").val($(zPublishAnchor).attr('id'));
        }
        $("#LogsDeletePopup").modal('show');
    }
}