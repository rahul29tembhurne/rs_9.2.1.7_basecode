﻿class GiftCard extends ZnodeBase {
    _endPoint: Endpoint;
    constructor() {
        super();
        this._endPoint = new Endpoint();
    }

    Init() {
        GiftCard.prototype.ValidateGiftCard();
    }

    ValidateGiftCard(): any {
        $("#UserId").on("blur", function () {
            ZnodeBase.prototype.ShowLoader();
            GiftCard.prototype.ValidateGiftCardUserId();
            GiftCard.prototype.ValidatePortal();
            ZnodeBase.prototype.HideLoader();
        });
    }

    ValidatePortal(): boolean {
        if ($("#PortalId").val() == "" || $("#PortalId").val() == 0 || $("#txtPortalName").val() == "") {
            $("#errorRequiredStore").text('').text(ZnodeBase.prototype.getResourceByKeyName("SelectPortal")).addClass("field-validation-error").show();
            $("#txtPortalName").addClass('input-validation-error');
            return false;
        }
        else {
            $("#errorRequiredStore").text('').text(ZnodeBase.prototype.getResourceByKeyName("SelectPortal")).removeClass("field-validation-error");
            $("#txtPortalName").removeClass('input-validation-error');
            return true;
        }
    }

    ValidateGiftCardUserId(): any {
        if (!GiftCard.prototype.ValidatePortal())
            return false;

        var isValid = true;
        if ($("#UserId").val() != '') {
            Endpoint.prototype.IsUserIdForGiftCardExist($("#UserId").val(), $("#PortalId").val(), function (response) {
                if (!response) {
                    $("#UserId").addClass("input-validation-error");
                    $("#valUserId").addClass("error-msg");
                    $("#valUserId").text(ZnodeBase.prototype.getResourceByKeyName("AlreadyExistGiftCardUserId"));
                    $("#valUserId").show();
                    isValid = false;
                }
            });
        }
        return isValid;
    }

    DeleteGiftCard(control): any {
        var giftCardId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (giftCardId.length > 0) {
            Endpoint.prototype.DeleteGiftCard(giftCardId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    ShowTextBox(): any {
        if ($('#EnableToCustomerAccount').prop("checked") == true) {
            $('#ShowUserId').show();
            $('#sendMail').show();
        } else {
            $("#valUserId").text('').removeClass("field-validation-valid");
            $("#UserId").val('');
            $('#ShowUserId').hide();
            $('#sendMail').hide();
        }
    }

    ValidateUserId(): any {
        if (!GiftCard.prototype.ValidatePortal())
            return false;

        if ($("#EnableToCustomerAccount").prop('checked') == true) {
            var userId = $("#UserId").val();
            if (userId.length < 1) {
                $("#valUserId").text('').removeClass("field-validation-error");
                $("#valUserId").text(ZnodeBase.prototype.getResourceByKeyName("RequiredUserId")).addClass("field-validation-error");
                $("#valUserId").show();
                return false;
            }
            else if (isNaN(userId) || userId <= 0) {
                $("#valUserId").text('').removeClass("field-validation-error");
                $("#valUserId").text(ZnodeBase.prototype.getResourceByKeyName("EnterNumericError")).addClass("field-validation-error");
                $("#valUserId").show();
                return false;
            }
            else if (!GiftCard.prototype.ValidateGiftCardUserId()) {
                return false;
            }
            else if ($("#UserId").hasClass("valid")) {
                $("#valUserId").hide();
                return true;
            }
            else {
                $("#valUserId").show();
                return false;
            }
        }
        else
            return true;
    }

    GetListBasedOnSelection(): any {
        var dropDownSelection = $("#ExpiredGiftCard").val();
        var isExcludeExpired = true;

        if (dropDownSelection == "All")
            isExcludeExpired = false;

        Endpoint.prototype.GiftCardList(isExcludeExpired, function (response) {
            $("#ZnodeGiftCard").html(response);
        });
    }

    //Get customer list based on portal id.
    GetCustomerList(): any {
        var portalId: number = $("#PortalId").val();
        if (portalId > 0) {
            ZnodeBase.prototype.BrowseAsidePoupPanel('/GiftCard/GetCustomerList?PortalId=' + portalId, 'customerDetails');
        }
        else {
            $("#errorRequiredStore").text('').text(ZnodeBase.prototype.getResourceByKeyName("SelectPortal")).addClass("field-validation-error").show();
            $("#txtPortalName").addClass('input-validation-error');
            return false;
        }
    }

    //Set customer id for which gift card is created.
    SetCustomerId(): void {
        $("#grid").find("tr").on("click", function () {
            var userId: string = $(this).find("td")[0].innerHTML;
            $('#UserId').val(userId.match(/>(.*?)</)[1]);
            $('#customerDetails').hide(700);
            Products.prototype.HideErrorMessage($("#UserId"), $("#valUserId"));
            ZnodeBase.prototype.RemovePopupOverlay();
        });
    }

    GetActiveCurrencyToStore(currencyCode): void {
        if (currencyCode != "") {
            Endpoint.prototype.GetCurrencyDetailsByCode(currencyCode, function (response) {
                $("span[for='CurrencySymbol']").html(response.currencyViewModel.Symbol);
                $("label[for='CurrencyName']").html(response.currencyViewModel.CurrencyName);
            });
        }
        else {
            var portalId = $("#PortalId").val();
            if (portalId == undefined || portalId == "") {
                portalId = "0";
            }
            Endpoint.prototype.GetActiveCurrencyToStore(parseInt(portalId), function (response) {
                $("span[for='CurrencySymbol']").html(response.currencyViewModel.Symbol);
                $("label[for='CurrencyName']").html(response.currencyViewModel.CurrencyName);
                $("#CurrencyCode").val(response.currencyViewModel.CurrencyCode);
            });
        }
    }

    //This method is used to get portal list on aside panel.
    GetPortalList(): any {
        ZnodeBase.prototype.BrowseAsidePoupPanel('/GiftCard/GetPortalList', 'divStoreListAsidePanel');
    }

    //This method is used to select portal from list and show it on textbox.
    GetPortalDetail(): void {
        $("#grid").find("tr").on("click", function () {
            let portalName: string = $(this).find("td[class='storecolumn']").text();
            let portalId: string = $(this).find("td")[0].innerHTML;
            $('#txtPortalName').val(portalName);
            $('#PortalId').val(portalId);
            $('#UserId').val('');
            $("#valUserId").text('').text("").removeClass("field-validation-error").hide();
            $("#UserId").removeClass('input-validation-error');

            GiftCard.prototype.GetActiveCurrencyToStore("");

            $("#errorRequiredStore").text('').text("").removeClass("field-validation-error").hide();
            $("#txtPortalName").removeClass('input-validation-error');
            $('#divStoreListAsidePanel').hide(700);
            ZnodeBase.prototype.RemovePopupOverlay();
        });
    }
}
