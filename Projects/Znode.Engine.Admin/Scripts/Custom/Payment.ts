﻿class Payment extends ZnodeBase {

    constructor() {
        super();
    }
    Init(): void {
        Payment.prototype.OnLoad();
    }

    OnLoad(): void {
        $(document).off("change", "#ddlPaymentTypes").on("change", "#ddlPaymentTypes", function () {
            Payment.prototype.GetPaymentTypesForm($('#ddlPaymentTypes').val(), null);
        });

        $(document).off("change", "#ddlPaymentGetways").on("change", "#ddlPaymentGetways", function () {
            Payment.prototype.GetPaymentGetwayForm($('#ddlPaymentGetways').val(), null);
        });

        $(document).off("change", "#ddlTestMode").on("change", "#ddlTestMode", function () {
            Payment.prototype.GetPaymentSettingCredentials();
        });

        $(document).off("change", "#IsPoDocUploadEnable").on("change", "#IsPoDocUploadEnable", function () {
            Payment.prototype.ToggleIsPODocRequired();
        });

        if ($("#PaymentTypeId").val() > 0 && $("#PaymentSettingId").val()) {
            $("#IsPoDocUploadEnable").trigger("change");
            $("#IsPoDocRequire").trigger("change");
        }

        $(document).off("change", "#IsPoDocRequire").on("change", "#IsPoDocRequire", function () {
            var IsPODocRequired = $("#IsPoDocRequire");
            IsPODocRequired.is(":checked") ? IsPODocRequired.val("true") : IsPODocRequired.val("false");
        });

        Payment.prototype.PaymentNameValidation();
    }

    GetPaymentTypesForm(paymentTypeId: string, paymentSettingModel: Object): void {
        ZnodeBase.prototype.ShowLoader();
        Endpoint.prototype.GetPaymentTypeForm(paymentTypeId, paymentSettingModel, function (res) {
            $("#paymenttypeform-container").show();
            $("#paymenttypeform-container").html(res);
            Payment.prototype.OnLoad();
            ZnodeBase.prototype.HideLoader();
        });
    }

    ValidatePayment(): boolean {
        var IsValid: boolean = true;
        IsValid = Payment.prototype.IsCardTypeSelected();
        if (IsValid == true) {
            IsValid = Payment.prototype.ValidatePaymentName();
        }
        return IsValid;
    }

    IsCardTypeSelected(): boolean {
        var paymentType = $('#ddlPaymentTypes option:Selected').text().toLowerCase();
        if (paymentType != "cod" && paymentType != "purchase order" && paymentType != "paypal express" && paymentType != "amazonpay") {
            if ($("#EnableVisa").is(":checked") == false
                && $("#EnableMasterCard").is(":checked") == false
                && $("#EnableAmericanExpress").is(":checked") == false
                && $("#EnableDiscover").is(":checked") == false) {
                $("#AcceptedCardsValidation").show();
                return false;
            }
        }
        return true;

    }

    IsMatchRegularExpressionString(str: string, regax: string): boolean {
        if (str.match(regax)) {
            return true;
        }
        else {
            return false;
        }
    }

    GetPaymentGetwayForm(gatewayId: number, paymentSettingModel: Object): void {
        if (gatewayId > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.GetPaymentGetwayForm(gatewayId, paymentSettingModel, function (res) {
                $("#PaymentGetwayForm-container").html(res);
                $("#PaymentGetwayForm-container").show();
                ZnodeBase.prototype.HideLoader();
            });

        }
    }

    GetPaymentSettingCredentials(): void {
        var paymentSettingId = $("#PaymentApplicationSettingId").val();
        if (paymentSettingId > 0) {
            ZnodeBase.prototype.ShowLoader();
            var testMode = $("#ddlTestMode").val();
            var paymentGatewayId = $("#PaymentGatewayId").val();

            Endpoint.prototype.GetPaymentSettingCredentials(paymentSettingId, testMode, paymentGatewayId, function (response) {
                $("#PaymentGetwayForm-container").html(response);
                $("#PaymentGetwayForm-container").show();
                Payment.prototype.OnLoad();
                ZnodeBase.prototype.HideLoader();
            });
        }
    }

    DeleteMultiplePaymentSettings(control): any {
        var paymentSettingIds = MediaManagerTools.prototype.unique();
        if (paymentSettingIds.length > 0) {
            Endpoint.prototype.DeleteMultiplePaymentSettings(paymentSettingIds.join(","), function (response) {
                DynamicGrid.prototype.RefreshGridOndelete(control, response);
            });
        }
    }
    ToggleIsPODocRequired(): void {
        var divIsPODocRequired = $("#divIsPODocRequired");
        var EnablePODocUpload = $("#IsPoDocUploadEnable").is(":checked");
        if (EnablePODocUpload) {
            divIsPODocRequired.show();
            $("#IsPoDocUploadEnable").val("true");
            var IsPODocRequired = $("#IsPoDocRequire");
            if (IsPODocRequired != null && typeof IsPODocRequired != 'undefined') {
                IsPODocRequired.is(":checked") ? IsPODocRequired.val("true") : IsPODocRequired.val("false");
            }
        }
        else {
            divIsPODocRequired.hide();
            $("#IsPoDocRequire").prop("checked", false);
            $("#IsPoDocRequire").val("false");
            $("#IsPoDocUploadEnable").val("false");
        }
    }

    PaymentNameValidation(): any {
        $("#PaymentName :input").blur(function () {
            Payment.prototype.ValidatePaymentName();
        });
    }

    ValidatePaymentName(): boolean {       
        var isValid: boolean = true;
        if ($("#PaymentName").val() == '') {
            this.SetPaymentValidation("PaymentName", "errorSpanPaymentName", "PaymentNameRequiredError");
        }
        else if ($("#PaymentDisplayName").val() == '') {
            this.SetPaymentValidation("PaymentDisplayName", "errorSpanPaymentDisplayName", "PaymentNameRequiredError");
        }
        else {
            Endpoint.prototype.IsPaymentNameExist($("#PaymentName").val(), $("#PaymentSettingId").val(), function (response) {
                if (!response) {
                    Payment.prototype.SetPaymentValidation("PaymentName", "errorSpanPaymentName", "AlreadyExistPaymentName");
                    isValid = false;
                }
            });
        }
        return isValid;
    }

    private SetPaymentValidation(controlId, validationControlId, errorKey): void {
        $("#" + controlId).addClass("input-validation-error");
        $("#" + validationControlId).addClass("error-msg");
        $("#" + validationControlId).text(ZnodeBase.prototype.getResourceByKeyName(errorKey));
        $("#" + validationControlId).show();
    }
}