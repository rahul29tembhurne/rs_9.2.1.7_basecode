﻿declare function reInitializationMce(): any;
declare function submitCard(model: any, callBack: any): any;
declare var savedUserCCDetails;
declare var enabledPaymentProviders;
declare var guid: string;
let selectedProductIds: string;
declare function autocompletewrapper(control: any, controlValue: any): any;
var CVVNumber;
class Order extends ZnodeBase {
    isCustomerValid: boolean;
    _Model: any;
    _endPoint: Endpoint;
    constructor() {
        super();
    }

    Init(): any {
        Order.prototype.ShippingSameAsBillingHandler();
        Order.prototype.SetProductDetails();
        Order.prototype.SetCustomerErrorMessage();
        Order.prototype.SetDataOnUpdate();
        Order.prototype.SetPurchaseOrderNumber();
        Order.prototype.SetDataOnConvertingQuoteToOrder();
        Order.prototype.ChangeOrderStatus();

    }

    SetAttributeForPortalId(): any {
        var portalId = $("#selectedPortalId").val();
        if (portalId > 0)
            $("#txtPortalName").attr("data-portalid", portalId);
    }

    GetCatalogListByPortalId(portalId: number, isAllowGlobalLevelUserCreation: boolean = false): any {
        var portalId: number = portalId;
        $("#txtCustomerName").val('');
        $("#hdnUserId").val(0);
        $(".tab-details").hide(true);
        Endpoint.prototype.CreateNewOrderByPortalIdChange(portalId, function (response) {
            var cartData = $.parseHTML(response);
            $("#AddressDiv").html($(cartData).find('#AddressDiv').html());
            Order.prototype.ShippingSameAsBillingHandler();
            $("#ShoppingCartDiv").html($(cartData).find('#ShoppingCartDiv').html());
            $("#shippingMethodDiv").html($(cartData).find('#shippingMethodDiv').html());
        });
    }

    PrintOnManange(): any {
        Endpoint.prototype.PrintOnManage(Order.prototype.GetOrderId(), function (response) {
            var originalContents = document.body.innerHTML;

            if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
                setTimeout(function () { document.body.innerHTML = response }, 1);
                setTimeout(function () { window.print(); }, 10);
                setTimeout(function () { document.body.innerHTML = originalContents }, 20);
                setTimeout(function () { Order.prototype.HideLoader(); }, 30);
            }

            else {
                document.body.innerHTML = response;
                window.print();
                document.body.innerHTML = originalContents;
            }
        });
    }

    PrintHandler(e): any {
        var printContents = $("#printContentDiv").html();
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }

    GetCustomerList(): any {
        $("#divStoreListAsidePanel").html('');
        Order.prototype.SetAttributeForPortalId();
        var portalId: number = parseInt($("#txtPortalName").attr("data-portalid"));
        var accountId: number = 0;

        var requestedUrl = window.location.href.toLocaleLowerCase();
        if (requestedUrl.indexOf("createquoteforcustomer") > 0) {
            accountId = $("#hdnAccountId").val();
        }
        else if (requestedUrl.indexOf("createaccountquote") > 0) {
            accountId = $("#hdnAccountId").val();
        }
        else {
            accountId = 0;
        }
        if (portalId > 0) {
            $("#selectedPortalId").val(portalId);
            ZnodeBase.prototype.BrowseAsidePoupPanel('/Order/GetCustomerList?PortalId=' + portalId + '&isAccountCustomer=' + Order.prototype.IsQuote() + '&accountId=' + accountId, 'customerDetails');
        }
        else {
            $("#errorRequiredStore").text('').text(ZnodeBase.prototype.getResourceByKeyName("SelectPortal")).addClass("field-validation-error").show();
            $("#txtPortalName").addClass('input-validation-error');
            return false;
        }
    }

    ClosePanel(): any {
        $("#panel-pop-up").html('');
        $("#panel-pop-up").hide();
    }

    ResetCustomerDetails(): any {
        $("#customerDetails").html('');
        $("#panel-pop-up").html('');
        $("#panel-pop-up").hide();
    }

    SetCustomerDetailsById(): any {
        $("#grid tbody tr").on("click", function (e) {
            e.preventDefault();
            let userId: string = $(this).find("td a").attr("href").split('?')[1].split('&')[1].split('=')[1];
            var cartParameter = {
                "UserId": userId,
                "PortalId": Order.prototype.GetPortalId(),
                "PublishedCatalogId": $("#PortalCatalogId").val(),
                "IsQuote": Order.prototype.IsQuote(),
            };
            $("#hdnUserId").val(userId);
            $(".tab-details").hide(true);
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.BindCustomerDetails(cartParameter, function (response) {
                var cartData = $.parseHTML(response);
                $("#txtCustomerName").val($(cartData).find('#UserAddressDataViewModel_FullName').val());
                $('#CustomerNameError').text('');
                $("#hdnAccountId").val($(cartData).find('#hdnAccountId').val());
                Order.prototype.SetUserDetailsbyHtml(cartData);
                ZnodeBase.prototype.CancelUpload('customerDetails');
                Order.prototype.HideLoader();
            });

            Order.prototype.ClickSelectedTab("z-address");
            $(".chkShippingBilling").remove();
        });
    }

    GetCartParameters(): any {
        var cartParameter = {
            "UserId": $("#hdnUserId").val(),
            "LocaleId": $("#LocaleId").val(),
            "PortalId": Order.prototype.GetPortalId(),
            "PublishedCatalogId": $("#PortalCatalogId").val(),
            "IsQuote": Order.prototype.IsQuote(),
        };
        return cartParameter;
    }

    SetCustomerErrorMessage(): any {
        $("#btnSaveNextToShoppingCart").on("click", function (e) {
            var href = $(this).attr("href");
            $('#CustomerNameError').text('');
            if (!($('#hdnUserId').val() > 0)) {
                $('#CustomerNameError').text(ZnodeBase.prototype.getResourceByKeyName("ErrorCustomerSelect"));
                e.preventDefault();//this will prevent the link trying to navigate to another page
            }
        });
    }

    GetNewCustomerView(): any {
        var portalId = Order.prototype.GetPortalId();
        Endpoint.prototype.GetNewCustomerView(portalId, function (response) {
            $("#panel-pop-up").html(response);
            $("#panel-pop-up").show(700);
            $("#panel-pop-up").append("<a class=\"panel-pop-up-close\" onclick='Order.prototype.ClosePanel()'><i class='z-close-circle'></i></a>");
        });
    }

    ShippingSameAsBillingHandler(): any {
        if ($("#shippingSameAsBillingAddress").is(':checked')) {
            $("#BillingAddressContainer").hide();
        }
        else {
            $("#BillingAddressContainer").show();
        }
    }

    IsLogin(): any {
        if ($("#CreateLogin").is(':checked')) {
            $("#divnewLogin").show();
        }
        else {
            $("#divnewLogin").hide();
        }
    }

    ChangeTrackingNumberSuccessCallback(response): any {
        $("#labelTrackingNumber").html(response.TrackingUrl);
        $("#btnChangeTrackingNumber").show();
        $("#btnChangeTrackingNumber").attr("href", "/order/ManangeTrackingNumber?trackingNumber=" + response.TrackingNumber + "&omsOrderId=" + response.OmsOrderId);

    }
    ValidateTrackingNumber(): boolean {
        if ($("#OrderTextValue").val() == "") {
            $("#OrderTextValue").addClass("input-validation-error");
            $("#spnTrackingNumber").show();
            return false;
        }
        else {
            $("#OrderTextValue").removeClass("input-validation-error");
            $("#spnTrackingNumber").hide();
            return true;
        }
    }

    ValidateShippingAccountNumber(): boolean {
        if ($("#OrderTextValue").val() == "") {
            $("#OrderTextValue").addClass("input-validation-error");
            $("#spnShippingAccountNumber").show();
            return false;
        }
        else {
            $("#OrderTextValue").removeClass("input-validation-error");
            $("#spnShippingAccountNumber").hide();
            return true;
        }
    }

    ChangeShippingAccountNumberSuccessCallback(response): any {
        $("#labelShippingAccountNumber").html(response.ShippingAccountNumber);
        $("#btnChangeShippingAccountNumber").show();
        $("#btnChangeShippingAccountNumber").attr("href", "/order/ManangeShippingAccountNumber?shippingAccountNumber=" + response.ShippingAccountNumber + "&omsOrderId=" + response.OmsOrderId);

    }
    ValidateShippingMethod(): boolean {
        if ($("#OrderTextValue").val() == "") {
            $("#OrderTextValue").addClass("input-validation-error");
            $("#spnShippingMethod").show();
            return false;
        }
        else {
            $("#OrderTextValue").removeClass("input-validation-error");
            $("#spnShippingMethod").hide();
            return true;
        }
    }

    ChangeShippingMethodSuccessCallback(response): any {
        $("#labelShippingMethod").html(response.ShippingMethod);
        $("#btnChangeShippingMethod").show();
        $("#btnChangeShippingMethod").attr("href", "/order/ManangeShippingMethod?shippingMethod=" + response.ShippingMethod + "&omsOrderId=" + response.OmsOrderId);

    }
    AddCustomerSuccessCallback(response): any {
        $(".tab-details").hide(true);
        var html = $.parseHTML(response);
        Order.prototype.HideLoader();
        if ($($(html).find('.duplicateusererrormessage')[0]).val() != null && typeof $($(html).find('.duplicateusererrormessage')[0]).val() != 'undefined' && $($(html).find('.duplicateusererrormessage')[0]).val() != "") {

            $($($(html).find('.showduplicateusererrormessage')[0])[0]).text($($(html).find('.duplicateusererrormessage')[0]).val());
            $("#ShowDuplicateUserErrorMessage").text($($(html).find('.duplicateusererrormessage')[0]).val());
            return false;
        }
        if (response.indexOf("field-validation-error") < 0) {
            if ($(html).find("#hdnHasError").val() == "False") {
                ZnodeBase.prototype.CancelUpload('customerDetails');
                $("#txtCustomerName").val($($(html).find('#UserName')[0]).val() + ' | ' + $($(html).find('#FirstName')[0]).val() + ' ' + $($(html).find('#LastName')[0]).val());
                $('#CustomerNameError').text('');
                var userId = $($(html).find('#hdnCreatedUserId')[0]).val();
                $("#hdnUserId").val(userId);

                Endpoint.prototype.BindCustomerDetails(Order.prototype.GetCartParameters(), function (response) {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("CustomerCreatedSuccessfully"), "success", isFadeOut, fadeOutTime);
                    $('#CustomerNameError').text('');
                    var cartData = $.parseHTML(response);

                    Order.prototype.SetUserDetailsbyHtml(cartData);

                    Order.prototype.ClickSelectedTab("z-address");
                });
            }
            else {
                $("#error-create-customer").html("");
                $("#error-create-customer").html($(html).find("#hdnErrorMessage").val());
            }
        }
        else {
            $("#divtaxProductListPopup").html(response);
        }
        Order.prototype.RemoveFormDataValidation();
    }

    ValidateCustomer(): boolean {
        var flag = true;
        if (Order.prototype.isCustomerValid != undefined && !Order.prototype.isCustomerValid) {
            Order.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("InvalidCustomer"), $("#txtCustomerName"), $("#CustomerNameError"));
            $("#btnCreateOrder").attr("disabled", "disabled");
            return flag = false;
        }
        else {
            Order.prototype.HideErrorMessage($("#txtCustomerName"), $("#CustomerNameError"));
            $("#btnCreateOrder").prop("disabled", false);
        }

        return flag;
    }

    ShowErrorMessage(errorMessage: string = "", controlToValidateSelector: any, validatorSelector: any) {
        controlToValidateSelector.removeClass("input-validation-valid").addClass("input-validation-error");
        validatorSelector.removeClass("field-validation-valid").addClass("field-validation-error").html("<span>" + errorMessage + "</span>");
    }

    HideErrorMessage(controlToValidateSelector: any, validatorSelector: any) {
        controlToValidateSelector.removeClass("input-validation-error").addClass("input-validation-valid");
        validatorSelector.removeClass("field-validation-error").addClass(" field-validation-valid").html("");
    }

    SetUserDetailsbyHtml(cartData): any {
        //Set address details of user.
        $("#AddressDiv").html($(cartData).find('#AddressDiv').html());
        Order.prototype.ShippingSameAsBillingHandler();
        //Set shopping cart details of user.
        $("#ShoppingCartDiv").html($(cartData).find('#ShoppingCartDiv').html());
        if ($("#hdnUserId").val() > 0) {
            $("#ShoppingCartDiv").find('#productListDiv').show();
        }

        //Set shipping methods of user.
        $("#shippingMethodDiv").html($(cartData).find('#shippingMethodDiv').html());

        //Set payment option for user.
        $("#paymentMethodsDiv").html($(cartData).find('#paymentMethodsDiv').html());

        //Display discount view if shopping cart having items other wise hide the discount view.
        Order.prototype.ShowHideDiscountView();

        OrderSidePanel.prototype.SetSidePanelData("z-customers");
    }

    updateQueryStringParameter(uri, key, value): any {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            return uri + separator + key + "=" + value;
        }
    }

    SetUserAddress(addressTypeName, response): any {
        $('#UserAddressDataViewModel_' + addressTypeName + '_FirstName').val(response.FirstName);
        $('#UserAddressDataViewModel_' + addressTypeName + '_LastName').val(response.LastName);
        $('#UserAddressDataViewModel_' + addressTypeName + '_CompanyName').val(response.CompanyName);
        $('#UserAddressDataViewModel_' + addressTypeName + '_Address1').val(response.Address1);
        $('#UserAddressDataViewModel_' + addressTypeName + '_Address2').val(response.Address2);
        $('#UserAddressDataViewModel_' + addressTypeName + '_Address3').val(response.Address3);
        $('#UserAddressDataViewModel_' + addressTypeName + '_CityName').val(response.CityName);
        $('#UserAddressDataViewModel_' + addressTypeName + '_StateName').val(response.StateName);
        $('#UserAddressDataViewModel_' + addressTypeName + '_PostalCode').val(response.PostalCode);
        $('#UserAddressDataViewModel_' + addressTypeName + '_PhoneNumber').val(response.PhoneNumber);
        if (response.AddressId == 0) {
            $('#UserAddressDataViewModel_' + addressTypeName + '_AddressId').val(response.AddressId);
            $('#UserAddressDataViewModel_' + addressTypeName + '_CountryName').prop('selectedIndex', 0);
        }
        else {
            $('#UserAddressDataViewModel_' + addressTypeName + '_AddressId').val(response.AddressId);
        }
    }

    SetProductDetails(): any {
        var myEnum = {
            SimpleProduct: 0,
            ConfigurableProduct: 1,
            GroupedProduct: 2,
            BundleProduct: 3
        };
        var omsOrderId = Order.prototype.GetOrderId();
        var ZnodeOrderProductList = $("table[data-swhgcontainer='ZnodeOrderProductList']");
        ZnodeOrderProductList.find('.publishproductid').hide();
        ZnodeOrderProductList.find('.productType').hide();
        var gridTD = $('#ZnodeOrderProductList #grid tbody tr:eq(0)').find('td');
        Order.prototype.HideTableTH(gridTD, 'productType');
        Order.prototype.HideTableTH(gridTD, 'publishproductid');

        $("#ZnodeOrderProductList #grid").each(function (e) {
            if ($('#ZnodeOrderProductList #grid tbody tr:eq(0)').find('td').hasClass('addToCart')) {
                var indexOfRow = $('#ZnodeOrderProductList #grid tbody tr:eq(0)').find('.addToCart').index() + 1;
                $('th:nth-child(' + indexOfRow + ')').hide();
                $('td:nth-child(' + indexOfRow + ')').hide();
            }
        });
        var gridCheckbox = $('#grid tbody tr input:checkbox[name=PublishProductId]');
        if (gridCheckbox.length == 0)
            gridCheckbox = $('#grid tbody tr').find('input:checkbox[name=PublishProductId]');

        gridCheckbox.each(function (e) {
            $(this).attr("data-container", "body").attr("data-toggle", "popover").attr("data-content", " ").addClass('addtocartpopover');
        });
        gridCheckbox.on("click",
            function (event) {
                DynamicGrid.prototype.rowCheckChange();
                event.stopPropagation();
                var publishProductId = this.id.split('_')[1];

                var catalogId = $("#PortalCatalogId").val();
                var localeId = $("#LocaleId").val();
                var userId = $('#hdnUserId').val() == undefined ? $("#labelCustomerId").text().trim() : $("#hdnUserId").val();

                if ($(this).parents('tr:eq(0)').find("i").hasClass("z-close-circle")) {
                    $("#outOfStock-btn").html('<button type="button" id="outOfStockPopup" data-toggle="modal" data-target="#OutOfStockPopup"></button>');
                    $("#outOfStockPopup").click();
                    return false;
                }
                else {
                    var portalId = Order.prototype.GetPortalId();
                    $(".popover-content").empty();

                    if ($(".popover").length > 0) {
                        $(".popover").remove();
                    }

                    if ($(this).prop('checked')) {
                        Endpoint.prototype.GetPublishProduct(publishProductId, localeId, portalId, userId, catalogId, function (response) {
                            $(".popover-content").append(response);
                        });

                        //validationCheck.push(publishProductId);
                    }
                    else if (!$(this).prop('checked')) {
                        Order.prototype.RemoveItemFromCart(event, publishProductId, true);

                    }
                }
            });
    }

    GenerateInvoice(event): any {

        if (event == 'undefined' || event == "" || event == null) {
            return false;
        } else {

            var eventText = event.text;
            if (eventText != "" && eventText != null && eventText != 'undefined' && eventText.toLowerCase().trim() != "generate invoice") {
                return false;
            }
            var arrIds = DynamicGrid.prototype.GetMultipleSelectedIds();
            if (arrIds != undefined && arrIds.length > 0) {
                $("#orderIds").val(arrIds);
                var newForm = $('<form ></form>', { action: '/Order/DownloadPDF', method: 'POST' }).append($('<input></input>', {
                    'name': 'orderIds',
                    'id': 'orderIds',
                    'value': arrIds,
                    'type': 'hidden'
                }));

                $("body").append(newForm);
                newForm.submit();
                setTimeout(function () { ZnodeBase.prototype.HideLoader() }, 1000);
                return true;
            }
            else {
                $("#SuccessMessage").html("");
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper("At least one order should be selected.", "error", isFadeOut, fadeOutTime);
                return false;
            }
        }
    }

    GetProductsList(): any {
        ZnodeBase.prototype.ShowLoader();
        $("#customerDetails").html("");
        $("#divCustomerAddressPopup").html("");
        DynamicGrid.prototype.ClearCheckboxArray();
        var portalCatalogId = $("#PortalCatalogId").val();
        var portalId = Order.prototype.GetPortalId();
        var userId = $('#hdnUserId').val() == undefined ? $("#labelCustomerId").text().trim() : $("#hdnUserId").val();
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Order/ProductList?portalCatalogId=' + portalCatalogId + '&portalId=' + portalId + '&userId=' + userId + '', 'getProductsList');

    }

    UpdateCartQauntity(control): any {
        var productid = $(control).attr("data-cart-productid");
        var externalId = $(control).attr("data-cart-externalid");
        var shippingid = $("input[name='ShippingId']:checked").val();
        var minQuantity = $(control).attr("data-cart-minquantity");
        var maxQuantity = $(control).attr("data-cart-maxquantity");
        var sku = $(control).attr("data-cart-sku");
        var inventoryRoundOff = $(control).attr("data-inventoryRoundOff");
        $("#quantity_error_msg_" + externalId).text('');
        var selectedQty = $(control).val();

        if (selectedQty.split(".")[1] != null && parseInt(selectedQty.split(".")[1].length) > parseInt(inventoryRoundOff)) {
            $("#quantity_error_msg_" + externalId).text('Please enter quantity having ' + inventoryRoundOff + ' numbers after decimal point.');
            $("#btnCreateOrder").attr("disabled", "disabled");
            return false;
        }
        var matches = selectedQty.match(/^-?[\d.]+(?:e-?\d+)?$/);
        if (matches == null) {
            $("#quantity_error_msg_" + externalId).text('Please enter numeric value');
            $("#btnCreateOrder").attr("disabled", "disabled");
            return false;
        }

        if (parseFloat(selectedQty) < parseFloat(minQuantity) || parseFloat(selectedQty) > parseFloat(maxQuantity)) {
            $("#quantity_error_msg_" + externalId).text('Selected quantity should be in between ' + minQuantity + ' to ' + maxQuantity + '.');
            $("#btnCreateOrder").attr("disabled", "disabled");
            return false;
        }

        $("#btnCreateOrder").prop("disabled", false);
        var guid = $(control).attr("data-cart-externalid");
        ZnodeBase.prototype.ShowLoader();
        Endpoint.prototype.UpdateCartQUantity(guid, selectedQty, productid, shippingid, Order.prototype.IsQuote(), function (response) {
            $("#divShoppingCart").html("");
            $("#divShoppingCart").html(response.html);
            $("#publishProductDv").html('');
            $("#publishProductDv").hide();
            if (typeof $("#hdnShoppingCartCount").val() == 'undefined' || $("#hdnShoppingCartCount").val() == '0') {
                $("#couponContainer").html("");
                $("#csr-discount-status").html("");
                $("#txtcsrDiscount").val("");
                $("#div-coupons-promotions").hide();
            }
            else {
                Order.prototype.BindCouponHtml(response.coupons);
            }
            Order.prototype.SetGiftCardMessage();
            Order.prototype.ClearShippingEstimates();
            Order.prototype.HideLoader();
            $('*[data-autocomplete-url]').each(function () { autocompletewrapper($(this), $(this).data("onselect-function")); });
        });
    }

    DeleteCartItem(control): void {
        ZnodeBase.prototype.ShowLoader();
        var guid: string = $(control).attr('data_cart_externalid');
        var orderId: number = Order.prototype.GetOrderId();
        Endpoint.prototype.DeleteCartItem(guid, orderId, Order.prototype.IsQuote(), function (response) {
            if (orderId > 0) {
                if (response.hasError == true) {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.errorMessage, "error", isFadeOut, fadeOutTime);
                    return false;
                }
                $("#orderLineItems").html(response.html);
                Order.prototype.OnTaxExemptPageLoadCheck();
            }
            else {
                $("#divShoppingCart").html("");
                $("#divShoppingCart").html(response.html);
            }
            $("#publishProductDv").html('');
            $("#publishProductDv").hide();

            Order.prototype.SetGiftCardMessage();

            if (typeof $("#hdnShoppingCartCount").val() == 'undefined' || $("#hdnShoppingCartCount").val() == '0') {
                $("#couponContainer").html("");
                $("#csr-discount-status").html("");
                $("#txtcsrDiscount").val("");
                $("#div-coupons-promotions").hide();
            }
            else {
                Order.prototype.BindCouponHtml(response.coupons);
            }
            Order.prototype.ClearShippingEstimates();
            Order.prototype.HideLoader();
            $('*[data-autocomplete-url]').each(function () { autocompletewrapper($(this), $(this).data("onselect-function")); });
        });
    }

    ResendOrderLineItemConfirmMail(cartLineItemId: number): void {
        Endpoint.prototype.ResendOrderConfirmationForCartItem(Order.prototype.GetOrderId(), cartLineItemId, function (response) {
            window.location.reload(true);
        });
    }

    AddressDivShowHide(tab): any {
        var activeTab = $(tab).attr('id');
        $("#btnContinue").show();
        $("#btnCreateOrder").hide();
        $("#OrderAsidePannel>li.tab-selected").removeClass("tab-selected");
        $(tab).closest('li').addClass('tab-selected').removeClass("disabled");
        if (activeTab == 'z-customers') {
            $("#CustomerDiv").show();
            $("#AddressDiv").hide();
            $("#ShoppingCartDiv").hide();
            $("#shippingMethodDiv").hide();
            $("#paymentMethodsDiv").hide();
            $("#publishProductDv").html('');
            $("#publishProductDv").hide();
            $("#ReviewDiv").hide();
            if ($("#txtCustomerName").val() != "" || $("#txtCustomerName").val() != undefined) {
                $("#CustomerNameError").text('').removeClass("field-validation-error");
                $("#txtCustomerName").removeClass("input-validation-error");
            }
            $("#paypal-button").hide();
        }
        else if (activeTab == 'z-address') {
            $("#CustomerDiv").hide();
            $("#AddressDiv").show();
            $("#ShoppingCartDiv").hide();
            $("#shippingMethodDiv").hide();
            $(".chkShippingBilling").remove();
            $("#paymentMethodsDiv").hide();
            $("#publishProductDv").html('');
            $("#publishProductDv").hide();
            $("#ReviewDiv").hide();
            $("#paypal-button").hide();
        }
        else if (activeTab == 'z-shopping-cart') {
            $("#CustomerDiv").hide();
            $("#AddressDiv").hide();
            $("#ShoppingCartDiv").show();
            Order.prototype.ShowHideDiscountView();
            $("#shippingMethodDiv").hide();
            $("#paymentMethodsDiv").hide();
            $("#publishProductDv").html('');
            $("#publishProductDv").hide();
            $("#ReviewDiv").hide();
            $("#paypal-button").hide();
        }
        else if (activeTab == 'z-shipping-methods') {
            Order.prototype.AddCustomShippingAmount('', '', true);
            Order.prototype.GetShippingEstimates();
            $("#CustomerDiv").hide();
            $("#AddressDiv").hide();
            $("#ShoppingCartDiv").hide();
            $("#shippingMethodDiv").show();
            $("#paymentMethodsDiv").hide();
            $("#publishProductDv").html('');
            $("#publishProductDv").hide();
            $("#ReviewDiv").hide();
            $("#paypal-button").hide();
            if ($("#hndShippingclassName").val() == Constant.ZnodeCustomerShipping) {
                $("#customerShippingDiv").show();
            }
        }
        else if (activeTab == 'z-payment') {
            $("#CustomerDiv").hide();
            $("#AddressDiv").hide();
            $("#ShoppingCartDiv").hide();
            $("#shippingMethodDiv").hide();
            $("#paymentMethodsDiv").show();
            $("#publishProductDv").html('');
            $("#publishProductDv").hide();
            $("#ReviewDiv").hide();

            if (($("#hdnTotalOrderAmount").val() == undefined || $("#hdnTotalOrderAmount").val() > 0.00) && ($("#hdnOverDueAmount").val() >= 0.00)) {
                $("#ddlPaymentTypes").prop("disabled", false);
            }
            else {
                $(".HidePaymentTypeDiv").hide();
                $("#div-COD").show();
                $("#ddlPaymentTypes").val($("#ddlPaymentTypes option:first").val());
                $("#ddlPaymentTypes").prop("disabled", true);
            }
            Order.prototype.ShowAndSetPayment("#ddlPaymentTypes", true);
        }
        else if (activeTab == 'z-review') {
            if (Order.prototype.ReviewOrder()) {
                $("#CustomerDiv").hide();
                $("#AddressDiv").hide();
                $("#ShoppingCartDiv").hide();
                $("#shippingMethodDiv").hide();
                $("#paymentMethodsDiv").hide();
                $("#publishProductDv").html('');
                $("#publishProductDv").hide();
                $("#ReviewDiv").show();
                $("#btnContinue").hide();
                Order.prototype.DisableCheckouButtonOnReviewPage();

                if ($("#ddlPaymentTypes option:selected").length == 0 || $("#ddlPaymentTypes option:selected").attr("id") && $("#ddlPaymentTypes option:selected").attr("id").toLowerCase() != 'paypal_express') {
                    $("#btnCreateOrder").show();
                }
                else {
                    Order.prototype.DisableCheckouButtonOnReviewPage();
                    if (parseFloat($("#hdnTotalOrderAmount").val()) == 0 && $("#ddlPaymentTypes option:selected").length == 1) {
                        $("#btnCreateOrder").show();
                    }
                    else {
                        $("#btnCreateOrder").hide();
                        $("#paypal-button").show();
                    }
                }
            }
        }
        OrderSidePanel.prototype.SetSidePanelData($("#OrderAsidePannel>li.tab-selected").attr("id"));
        if ($('#shippingMethodDiv input[name="ShippingId"]').length > 0) {
            $("#div-shipping-method-change").show();
        }
        else { $("#div-shipping-method-change").hide(); }
    }

    GetShippingEstimates(): void {
        ZnodeBase.prototype.ShowLoader();
        var shippingId = $('#shippingMethodDiv input[name="ShippingId"]:checked').val();
        var customerShippingAccountNumber: string = $("#ShippingListViewModel_AccountNumber").val();
        var customerShippingMethod: string = $("#ShippingListViewModel_ShippingMethod").val();
        $("#ShippingDetails").empty();
        Endpoint.prototype.GetShippingOptions(function (response) {
            ZnodeBase.prototype.ShowLoader();
            if (response != null && response.result != "") {
                $("#ShippingDetails").empty();
                $("#div-shipping-method-change").show();
                $("#ShippingDetails").append(response.result);
                $("#ShippingListViewModel_AccountNumber").val(customerShippingAccountNumber);
                $("#ShippingListViewModel_ShippingMethod").val(customerShippingMethod);
                Order.prototype.DisableShippingForFreeShippingAndDownloadableProduct();
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("NoShippingOptionList"), "error", isFadeOut, fadeOutTime);
            }
            shippingId = shippingId != "" && typeof shippingId != "undefined" ? shippingId : $("#hdnQuoteShipppingId").val();
            var shippingClassName: string = "";

            if (shippingId != "" && typeof shippingId != "undefined") {
                $('#shippingMethodDiv input[name="ShippingId"][value=' + shippingId + ']').prop('checked', true);
                shippingClassName = $('#shippingMethodDiv input[name="ShippingId"]:checked').attr("data-ShippingClassName");
                let shippingCode: string = $('#shippingMethodDiv input[name="ShippingId"]:checked').attr("id");

                Order.prototype.ShowEditCustomShipping(shippingCode);

            }
            if ($("#cartFreeShipping").val() != "True")
                Order.prototype.ShippingChangeHandler(shippingClassName);
            ZnodeBase.prototype.HideLoader();
        });
    }

    GetShippingEstimatesForManage(): void {
        ZnodeBase.prototype.ShowLoader();
        var shippingId = $('#shippingMethodDiv input[name="ShippingId"]:checked').val();
        Endpoint.prototype.GetShippingOptionsForManage(Order.prototype.GetOrderId(), function (response) {
            ZnodeBase.prototype.ShowLoader();
            if (response != null && response.ShippingOptionList != null) {
                var loopCount = response.ShippingOptionList.length;
                for (var iCount = 0; iCount < loopCount; iCount++) {
                    var shippingRateId = "#ShippingId_" + response.ShippingOptionList[iCount].ShippingId;
                    var shippingRate = " - " + response.ShippingOptionList[iCount].FormattedShippingRate;
                    $(shippingRateId).html(shippingRate);
                }
                $('#shippingMethodDiv input[name="ShippingId"][value=' + shippingId + ']').prop('checked', true);
                ZnodeBase.prototype.HideLoader();
            }
        });
    }

    ClearShippingEstimates(): void {
        $('.ShippingOptionsWithRates').html('');
    }

    BindUserAddress(e): any {
        if (!($('#hdnUserId').val() > 0)) {
            $('#CustomerNameError').text(ZnodeBase.prototype.getResourceByKeyName("ErrorCustomerSelect"));
            e.preventDefault();
        }
        $("#CustomerDiv").toggle();
        $("#AddressDiv").toggle();
        Endpoint.prototype.BindCustomerDetails(Order.prototype.GetCartParameters(), function (response) {
            var cartData = $.parseHTML(response);
            Order.prototype.SetUserDetailsbyHtml(cartData);
        });
        $(".chkShippingBilling").remove();
    }

    SubmitOrder(): any {
        let isValid: boolean = true;
        isValid = Order.prototype.ValidateDetails("false");
        if (isValid) {
            if ($("#hdnTotalOrderAmount").val() == undefined || $("#hdnTotalOrderAmount").val() > 0.00 && ($("#hdnOverDueAmount").val() >= 0.00)) {
                let selectedPaymentText: string = $("#ddlPaymentTypes option:selected").attr("id").toLowerCase();
                let paymentSettingId: string = $("#ddlPaymentTypes").val();
                $("#hdnPaymentTypeId").val(paymentSettingId);
                switch (selectedPaymentText) {
                    case "cod":
                        Order.prototype.SubmitCheckOut();
                        break;
                    case "purchase_order":
                        Order.prototype.SubmitCheckOut();
                        break;
                    case "credit_card":
                        Order.prototype.GetPaymentDetails(paymentSettingId);
                        Order.prototype.SubmitPayment();
                        break;
                }
            }
            else {
                Order.prototype.SubmitCheckOut();
            }
        }
    }

    SubmitCheckOut(): void {
        $('#status-message').remove();
        var controllerName: string;
        if ($('.z-cancel').closest('a').attr('id') == "btnCancelQuoteOrderId") {
            controllerName = "Quote";
        }
        else
            controllerName = "Order";
        $("#PortalId").val(Order.prototype.GetPortalId());
        ZnodeBase.prototype.ShowLoader();
        $.ajax({
            url: "/Order/SubmitOrder",
            data: $("#checkoutform").serialize(),
            type: 'POST',
            success: function (data) {
                Order.prototype.HideLoader();
                if (!data.HasError) {
                    var form = $('<form action="CheckoutReceipt" method="post">' +
                        '<input type="hidden" name="orderId" value="' + data.OrderId + '" />' +
                        '<input type="text" name= "ReceiptHtml" value= "' + data.ReceiptHtml + '" />' +
                        '<input type="hidden" name= "IsEmailSend" value= ' + data.IsEmailSend + ' />' +
                        '</form>');
                    var action = controllerName == "Quote" ? "/Quote/CheckoutReceipt" : "/Order/CheckoutReceipt";
                    form.attr("action", action);
                    $('body').append(form);
                    $(form).submit();
                }
                else {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(data.ErrorMessage, "error", isFadeOut, fadeOutTime);
                }
            }
        });
    }

    ValidateDetails(isQuote): boolean {
        var isValid = true;
        if ($("#txtCustomerName").val() == '') {
            isValid = false;
            $("#txtCustomerName").css({
                "border": "1px solid red"
            });
            Order.prototype.ClickSelectedTab("z-customers");
        }
        else if ($("[name='ShippingId']").val() == undefined || $("[name='ShippingId']").val() == "") {
            if (isQuote != "true") {
                Order.prototype.ClickSelectedTab("z-shipping-methods");
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorShippingNotAvailable"), "error", isFadeOut, fadeOutTime);
                isValid = false;
            }
        }
        else if ($("#ddlPaymentTypes option:selected").attr("id") != null && $("#ddlPaymentTypes option:selected").attr("id") != undefined) {
            if ($("#ddlPaymentTypes option:selected").attr("id").toLowerCase() == 'select payment type') {
                if (isQuote != "true") {
                    if ($("#hdnTotalOrderAmount").val() == undefined || $("#hdnTotalOrderAmount").val() > 0.00 && ($("#hdnOverDueAmount").val() >= 0.00)) {
                        isValid = false;
                        $("#ddlPaymentTypes").css({
                            "border": "1px solid red"
                        });
                        Order.prototype.ClickSelectedTab("z-payment");
                    }
                }

            }
            else if ($("#ddlPaymentTypes option:selected").attr("id").toLowerCase() == 'credit_card' && $("#OrderAsidePannel>li.tab-selected").attr("id") == 'z-review') {
                let isCCValid: boolean = true;
                if ($('#creditCardTab').css('display') == 'block' && $('ul#creditCardTab ').find('li.active').find('a').attr('href') == "#savedCreditCard-panel") {
                    var savedCartOptionValue = $("input[name='CCListdetails']:checked").val();
                    if (savedCartOptionValue == null || savedCartOptionValue == "") {
                        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorSavedCreditCardOption"), "error", isFadeOut, fadeOutTime);
                        isValid = false;
                        isCCValid = false;
                        Order.prototype.ClickSelectedTab("z-payment");
                    }
                }
                else {
                    $('input[data-payment="number"],input[data-payment="exp-month"],input[data-payment="exp-year"],input[data-payment="cvc"],input[data-payment="cardholderName"]').each(function () {
                        if ($.trim($(this).val()) == '') {
                            isValid = false;
                            isCCValid = false;
                            $(this).css({
                                "border": "1px solid red"
                            });
                        } else {
                            $(this).css({
                                "border": "1px solid #c3c3c3"
                            });
                        }
                    });

                    if (!Order.prototype.Mod10($('input[data-payment="number"]').val())) {
                        isValid = false;
                        isCCValid = false;
                        $('#errornumber').show();
                        $('input[data-payment="number"]').css({
                            "border": "1px solid red"
                        });
                    }
                    else {
                        $('#errornumber').hide();
                    }

                    if ($('input[data-payment="exp-month"]').val() > 12 || $('input[data-payment="exp-month"]').val() < 1) {
                        isValid = false;
                        isCCValid = false;
                        $('#errormonth').show();
                        $('input[data-payment="exp-month"]').css({
                            "border": "1px solid red"
                        });
                    }
                    else {
                        $('#errormonth').hide();
                    }

                    var currentYear = (new Date).getFullYear();
                    var currentMonth = (new Date).getMonth() + 1;

                    if ($('input[data-payment="exp-year"]').val() < currentYear) {
                        isValid = false;
                        isCCValid = false;
                        $('#erroryear').show();
                        $('input[data-payment="exp-year"]').css({
                            "border": "1px solid red"
                        });
                    }
                    else {
                        $('#erroryear').hide();
                    }

                    if ($('input[data-payment="exp-year"]').val() == currentYear && $('input[data-payment="exp-month"]').val() < currentMonth) {
                        isValid = false;
                        isCCValid = false;
                        $('#errormonth').show();
                        $('input[data-payment="exp-month"]').css({
                            "border": "1px solid red"
                        });
                        $('#erroryear').show();
                        $('input[data-payment="exp-year"]').css({
                            "border": "1px solid red"
                        });
                    }

                    if ($('input[data-payment="cvc"]').val() == '') {
                        $('#errorcvc').show();
                    }
                    else {
                        $('#errorcvc').hide();
                    }

                    if ($('input[data-payment="cardholderName"]').val() == '') {
                        $('#errorcardholderName').show();
                    }
                    else {
                        $('#errorcardholderName').hide();
                    }

                    if (isCCValid == true) {
                        var cardNumber = $("#div-CreditCard [data-payment='number']").val();
                        var cardType = Order.prototype.DetectCardType(cardNumber);
                        if ($("#addNewCreditCard-panel").attr("class").indexOf("active") != -1) {
                            if (cardType.toLowerCase() != $("input[name='PaymentProviders']:checked").val().toLowerCase()) {
                                $("#ajaxBusy").dialog('close');
                                var message = "The selected card type is of " + $("input[name='PaymentProviders']:checked").val().toLowerCase() + ".  Please check the credit card number and the card type.";
                                if (message != undefined) {
                                    Order.prototype.ShowErrorPaymentDialog(message);
                                    isValid = false;
                                }
                                isValid = false;
                            }
                        }
                    }
                }
                if (isValid == false) {
                    Order.prototype.ClickSelectedTab("z-payment");
                }
            }
            else if ($("#ddlPaymentTypes option:selected").attr("id").toLowerCase() == 'purchase_order') {
                if ($("#PurchaseOrderNumber").val() == "") {
                    $("#PurchaseOrderNumber").css({
                        "border": "1px solid red"
                    });
                    isValid = false;
                    Order.prototype.ClickSelectedTab("z-payment");
                }
            }
            else if (typeof $("#hdnShoppingCartCount").val() == 'undefined' || $("#hdnShoppingCartCount").val() == '0') {
                isValid = false;
                Order.prototype.ClickSelectedTab("z-shopping-cart");
            }
            else if ($("#hdnIsAnyProductOutOfStock").val().toLowerCase() == 'true' || $("#hdnIsAnyProductOutOfStock").val() == true) {
                isValid = false;
                Order.prototype.ClickSelectedTab("z-shopping-cart");
            }

        }
        else {
            if ($("#ShippingAddress_AddressId").val() == '0') {
                isValid = false;
            };
            if (!isValid) {
                Order.prototype.ClickSelectedTab("z-address");
            }
        }

        if (!Order.prototype.ShowAllowedTerritoriesError()) {
            isValid = false;
        }
        return isValid;
    }

    ClearCreditCartDetails(): any {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var LastTab = $(e.relatedTarget).attr('href'); // get last tab
            if (LastTab == "#savedCreditCard-panel") {
                $('input:radio[name=CCListdetails]').prop('checked', false);
            }
            if (LastTab == "#addNewCreditCard-panel") {
                $("input:radio[name=PaymentProviders]:first").prop('checked', true);
                $("#div-CreditCard [data-payment='number']").val('');
                $("#div-CreditCard [data-payment='cvc']").val('');
                $("#div-CreditCard [data-payment='exp-month']").val('');
                $("#div-CreditCard [data-payment='exp-year']").val('');
                $("#div-CreditCard [data-payment='cardholderName']").val('');
            }
        });
    }

    DisableShippingForFreeShippingAndDownloadableProduct(): any {
        if ($("#cartFreeShipping").val() == "True" && $("#hdnIsFreeShipping").val() == "True") {
            $('input[name="ShippingId"]').prop('checked', false);
            $('input[name="ShippingId"]').attr('disabled', true);
            $('input[name="ShippingId"]').next('span').addClass('disable-radio');
            var span = $('input[name="ShippingId"]').next().next();
            span.addClass('disable-radio');
            $("#FreeShipping").attr('disabled', false);
            $("#FreeShipping").prop("checked", true);
            Order.prototype.ShippingChangeHandler('ZnodeCustomShipping', 'FreeShipping');
            $("#message-freeshipping").show();
        }
        else {
            $('input[name="ShippingOptions"]').next('label').removeClass('disable-radio')
        }
    }

    SubmitQuote() {
        var isValid = true;
        isValid = Order.prototype.ValidateDetails("true");
        if (isValid) {
            $('#status-message').remove();
            $("#PortalId").val(Order.prototype.GetPortalId());
            ZnodeBase.prototype.ShowLoader();
            $.ajax({
                url: "/Order/SubmitQuote",
                data: $("#checkoutform").serialize(),
                type: 'POST',
                success: function (data) {
                    Order.prototype.HideLoader();
                    if (!data.HasError) {
                        var requestedUrl = window.location.href.toLocaleLowerCase();
                        if (requestedUrl.indexOf("createquoteforcustomer") > 0) {
                            window.location.href = "/Account/CustomersList?accountId=" + data.AccountId;
                        }
                        else if (requestedUrl.indexOf("createaccountquote") > 0) {
                            window.location.href = "/Account/AccountQuoteList?accountId=" + data.AccountId;
                        }
                        else {
                            window.location.href = "/Quote/AccountQuoteList";
                        }

                        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("QuoteSubmittedSuccessfully"), "success", isFadeOut, fadeOutTime);
                    }
                    else {
                        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(data.ErrorMessage, "error", isFadeOut, fadeOutTime);
                    }
                }
            });
        }
    }

    ClickSelectedTab(tabToClick: string) {
        $("#OrderAsidePannel>li.tab-selected").removeClass("tab-selected");
        $("#OrderAsidePannel").find('#' + tabToClick).addClass('tab-selected');
        $("#OrderAsidePannel").find('#' + tabToClick).find('a').click();
    }

    ChangeShipping(): any {
        ZnodeBase.prototype.CancelUpload("ReviewOrderPopup");
        Order.prototype.ClickSelectedTab("z-shipping-methods");
    }

    ChangeAddress(): any {
        ZnodeBase.prototype.CancelUpload("ReviewOrderPopup");
        Order.prototype.ClickSelectedTab("z-address");
    }

    BindAddOnProductSKU(event): any {
        if ($("#product-details-quantity input[name='Quantity']").attr('data-change') == "true") {
            $("#button-addtocart").attr("disabled", "disabled");
            event.preventDefault();
            return false;
        }
        var personalisedForm = $("#frmPersonalised");
        if (personalisedForm.length > 0 && !personalisedForm.valid())
            return false;
        var addOnValues = [];
        var bundleProducts = [];
        var groupProducts = "";
        var groupProductsQuantity = "";
        var personalisedCodes = [];
        var personalisedValues = [];
        var quantity: string = "";
        var flag: boolean = true;
        var groupProductName = "";
        var productType: string = $("#dynamic-producttype").val();
        selectedProductIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        //for checkbox   
        $(".chk-product-addons").each(function () {
            var optional = $(this).data("isoptional");
            var id = $(this).attr("id");
            var addOnId = $(this).data("addongroupid");
            if (optional == "False") {
                flag = true;
            }
            else {
                var isError = true;
                $('#' + id).find(':checkbox').each(function () {
                    if ($(this).is(':checked')) {
                        isError = false;
                    }
                });
                if (!isError) {
                    $("#paraComment-" + addOnId).css("display", "none");
                    flag = true;
                }
                else {
                    $("#paraComment-" + addOnId).removeAttr("style");
                    flag = false;
                }
            }
        });

        //Get selected add on product skus.
        addOnValues = Order.prototype.GetSelectedAddons();
        //Get bundle product skus.
        bundleProducts = Order.prototype.GetSelectedBundelProducts();
        //Get group product skus and their quantity.
        $("#groupProductList").find("input[type=text].quantity").each(function () {
            if ($(this).attr("name") != "Quantity") {
                var quantity = $(this).val();
                if (quantity != null && quantity != "") {
                    groupProducts = groupProducts + $(this).attr("name") + ",";
                    groupProductsQuantity + $(this).val() + "_";
                    groupProductsQuantity = groupProductsQuantity + $(this).val() + "_";
                    groupProductName = groupProductName + $(this).attr("data-productname") + ",";
                }
            }
        });
        groupProductsQuantity = groupProductsQuantity.substr(0, groupProductsQuantity.length - 1);
        groupProducts = groupProducts.substr(0, groupProducts.length - 1);
        if (productType == "GroupedProduct") {
            if (groupProductsQuantity == null || groupProductsQuantity == "") {
                $("#group-product-error").html(ZnodeBase.prototype.getResourceByKeyName("RequiredProductQuantity"));
                return false;
            }
            else if (!Order.prototype.OnAssociatedProductQuantityChange()) {
                $("#group-product-error").html(ZnodeBase.prototype.getResourceByKeyName("ErrorProductQuantity"));
                return false;
            }
            else {
                if (!Order.prototype.CheckGroupProductQuantity(Order.prototype.BindProductParameterModel(), groupProducts, groupProductsQuantity))
                    return false;
            }
        }
        else {
            quantity = $("#product-details-quantity input[name='Quantity']").val();
        }

        $("input[IsPersonalizable = True]").each(function () {
            var $label = $("label[for='" + this.id + "']")
            personalisedValues.push($(this).val());
            personalisedCodes.push($label.text());
        });
        Order.prototype.SetCartItemModelValues(addOnValues, bundleProducts, groupProducts, groupProductsQuantity, quantity, personalisedCodes, personalisedValues, groupProductName);

        //if flag=true then only show loader.
        if (flag)
            ZnodeBase.prototype.ShowLoader();

        return flag;
    }

    BindProductParameterModel(): Znode.Core.ProductParameterModel {
        var _productParameter: Znode.Core.ProductParameterModel = {
            PortalId: Order.prototype.GetPortalId(),
            LocaleId: parseInt($("#LocaleId").val()),
            PublishProductId: parseInt($("#dynamic-parentproductId").val()),
            UserId: parseInt($("#hdnUserId").val()),
            OMSOrderId: $("#OmsOrderId").val() == "" || $("#OmsOrderId").val() == 'undefined' ? $("#OrderId").val() : $("#OmsOrderId").val()
        };
        return _productParameter;

    }

    CheckGroupProductQuantity(productParameters: Znode.Core.ProductParameterModel, groupProductSkus: string, groupProductQuantities: string): boolean {
        var isSuccess: boolean = true;
        Endpoint.prototype.CheckGroupProductInventory(productParameters, groupProductSkus, groupProductQuantities, function (response) {
            if (response.ErrorMessage != "" && response.ErrorMessage != null && response.ErrorMessage != "undefined") {
                $("#group-product-error").html(response.ErrorMessage);
                isSuccess = false;
            }
        });
        return isSuccess;
    }

    OnQuantityChange(control): any {
        $("#product-details-quantity input[name='Quantity']").attr('data-change', 'true');
        $("#quantity-error-msg").text('');
        $("#dynamic-inventory").text('');
        var _productDetail = Order.prototype.BindProductModel(control, false);
        if (this.CheckIsNumeric(_productDetail.Quantity, _productDetail.QuantityError)) {
            if (this.CheckDecimalValue(_productDetail.DecimalPoint, _productDetail.DecimalValue, _productDetail.InventoryRoundOff, _productDetail.QuantityError)) {
                if (this.CheckQuantityGreaterThanZero(parseInt(_productDetail.Quantity), _productDetail.QuantityError)) {
                    $("#button-addtocart").prop("disabled", false);
                    Order.prototype.UpdateProductVariations(false, _productDetail.SKU, _productDetail.MainProductSKU, _productDetail.Quantity, _productDetail.MainProductId, function (response) {
                        var salesPrice = response.data.price;
                        Order.prototype.UpdateProductValues(response, _productDetail.Quantity);
                        Order.prototype.RefreshPrice(salesPrice);
                        Order.prototype.InventoryStatus(response);
                    });
                }
            }
        }
    }

    OnAddonSelect(): any {
        $("#dynamic-product-addons").on("change", ".AddOn", function (ev) {
            $('p[id*="paraComment"]').hide();
            var _productSKU = Order.prototype.GetGroupProductSKUQuantity($(this).attr("data-parentsku"));
            if (_productSKU != null) {
                Order.prototype.UpdateProductVariations(false, _productSKU.SKU, _productSKU.ParentSKU, _productSKU.Quantity, _productSKU.ParentProductId, function (response) {
                    var salesPrice = response.data.price;
                    Order.prototype.UpdateProductValues(response, _productSKU.Quantity);
                    Order.prototype.RefreshPrice(salesPrice);
                    Order.prototype.InventoryStatus(response);
                });
            }
        });
    }

    GetGroupProductSKUQuantity(parentSKU: string): Znode.Core.AddOnDetails {
        var _productSKU: Znode.Core.AddOnDetails;
        $("input[type=text].quantity").each(function () {
            if ($(this).attr("name") != "Quantity") {
                var groupProductQuantity = $(this).val();
                if (groupProductQuantity != null && groupProductQuantity != "") {
                    _productSKU = {
                        Quantity: groupProductQuantity,
                        SKU: $(this).attr("data-sku"),
                        ParentSKU: parentSKU,
                        ParentProductId: 0
                    };
                    return false;
                }
            }
        });
        if (_productSKU == null) {
            _productSKU = {
                Quantity: $("#Quantity").val(),
                SKU: $("#Quantity").attr("data-sku"),
                ParentSKU: $("#Quantity").attr("data-parentsku"),
                ParentProductId: parseInt($("#Quantity").attr("data-parentProductId")),
            };
        }
        return _productSKU;
    }

    UpdateProductVariations(htmlContainer: boolean, sku: string, parentSku: string, quantity: string, parentProductId: number, callbackMethod): any {
        var selectedAddOnIds = Order.prototype.getAddOnIds("");
        var portalId = Order.prototype.GetPortalId();
        let omsOrderId: number = $("#OmsOrderId").val() == "" || $("#OmsOrderId").val() == 'undefined' ? $("#OrderId").val() : $("#OmsOrderId").val();
        if ((parseInt($("#OrderId").val()) > 0 && $.isNumeric($("#OrderId").val())) || (parseInt($("#OmsOrderId").val()) > 0 && $.isNumeric($("#OmsOrderId").val()))) {
            portalId = $("#PortalId").val();
        }
        Endpoint.prototype.GetProductPrice(portalId, sku, parentSku, quantity, selectedAddOnIds, parentProductId, omsOrderId, function (res) {
            if (callbackMethod) {
                callbackMethod(res);
            }
        });
    }

    UpdateProductValues(response, quantity): any {
        var selectedAddOnIds = Order.prototype.getAddOnIds("");
        // Set form values for submit
        $("#dynamic-productid").val(response.data.productId);
        $("#dynamic-sku").val(response.data.sku);
        $("#Quantity").val(quantity);
        $("#dynamic-addons").val(selectedAddOnIds);
        $("input[name='AddOnValueIds']").val(selectedAddOnIds);
        $("#dynamic-productName").val(response.data.ProductName);
        if (response.data.addOnMessage != undefined) {
            $("#dynamic-addOninventory").show();
            $("#dynamic-addOninventory").html(response.data.addOnMessage);
        }
        else {
            $("#dynamic-addOninventory").hide();
            $("#dynamic-addOninventory").html("");
        }
    }

    RefreshPrice(amount): any {
        $("#product_Detail_Price_Div").show();
        $("#layout-product .dynamic-product-price").html(amount);
        $("#oms_pdp_price").text(amount);
    }

    InventoryStatus(response): any {
        if (response.success) {
            // In stock
            $("#dynamic-inventory").hide();
            $("#button-addtocart").prop("disabled", false);
            $("#product-details-quantity input[name='Quantity']").attr("data-change", "false");
        } else {
            // Out of stock
            $("#button-addtocart").attr("disabled", "disabled");
            $("#dynamic-inventory").show().html(response.message);
        }
    }

    getAddOnIds(parentSelector): any {
        var selectedAddOnIds = [];
        var addOnsCustomText = [];
        if (typeof parentSelector == "undefined") { parentSelector = ""; }
        $(parentSelector + " select.AddOn").each(function () {
            selectedAddOnIds.push($(this).val());
        });

        $(parentSelector + " input.AddOn:checked").each(function () {
            selectedAddOnIds.push($(this).val());
        });

        $(parentSelector + ".AddOnValues").each(function () {
            var optional = $(this).data("isoptional");
            var displayType = $(this).data("addondisplaytype");
            if (displayType == "TextBox" && optional == "False") {
                selectedAddOnIds.push($(this).data("addonvalueid"));
                addOnsCustomText.push($(this).val());
            }
            else if (displayType == "TextBox" && optional == "True" && $(this).val() != "") {
                selectedAddOnIds.push($(this).data("addonvalueid"));
                addOnsCustomText.push($(this).val());
            }
        });
        return (selectedAddOnIds.join());
    }

    HideLoader(): any {
        ZnodeBase.prototype.HideLoader();
        this.ToggleFreeShipping();
    }

    AddToCartSuccessCallBack(panel): any {
        Order.prototype.HideLoader();

        $(".popover").hide();
        if (Order.prototype.IsQuote()) {
            $("#div-coupons-promotions").hide();
        }
        else {
            if (typeof $("#hdnShoppingCartCount").val() == 'undefined' || $("#hdnShoppingCartCount").val() == '0') {
                $("#div-coupons-promotions").hide();
            }
            else {
                $("#div-coupons-promotions").show();
            }
        }
    }

    //TO Do Relace paymentType With Payment Type Code
    ShowPaymentHtml(control): any {
        var selectionId = "#" + control.id + " option:selected";
        Order.prototype.ShowAndSetPayment(selectionId, false);
        if ($("#OmsOrderId").val() > 0)
            $("#divAdditionalInfo").hide();
    }

    ShowAndSetPayment(selectionId, isFirstTime) {
        var paymentType = $("#ddlPaymentTypes option:selected").attr("id");
        $("#paypal-button").hide();
        $("#cart-ponumber-status").hide();

        if (paymentType == undefined || paymentType == null || paymentType == "")
            return;

        if (paymentType !== "")
            $('span#valPaymentTypes').text("");

        if (paymentType.toLowerCase() === "credit_card") {
            $("#PurchaseOrderNumber").val("");
            Order.prototype.creditCardPayment(selectionId, isFirstTime);
        }
        else if (paymentType.toLowerCase() == "purchase_order") {
            $(".HidePaymentTypeDiv").hide();
            $("#paypal-button").hide();
            $("#div-PurchaseOrder").show();
            $("#PurchaseOrderNumber").css({
                "border": "1px solid #c3c3c3"
            });
        }
        else if (paymentType.toLowerCase() == "paypal_express") {
            var isCreditCardEnabled = $("#hdnIsCreditCardEnabled").val();
            if (isCreditCardEnabled == "0" && !isFirstTime) {
                Order.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorOrderOverDueAmount"));
                return false;
            }
            $("#PurchaseOrderNumber").val("");
            $(".HidePaymentTypeDiv").hide();
            $("#paypal-button").show();
        }
        else if (paymentType.toLowerCase() == "cod") {
            $("#PurchaseOrderNumber").val("");
            $(".HidePaymentTypeDiv").hide();
            $("#div-COD").show();
            $("#paypal-button").hide();
        }
        else {
            $("#PurchaseOrderNumber").val("");
            $(".HidePaymentTypeDiv").hide();
            $("#paypal-button").hide();
        }
        Order.prototype.ClearCreditCartDetails();
    }
    OnAttributeSelect(control): any {
        var productId = $("#dynamic-parentproductId").val();
        var localeId = $("#LocaleId").val();
        var portalId = Order.prototype.GetPortalId();
        var userId = $("#hdnUserId").val();
        var sku = $("#dynamic-configurableProductSKUs").val();
        var ParentProductSKU = $("#dynamic-sku").val();
        var Codes = [];
        var Values = [];

        var selectedCode = $(control).attr('code');
        var selectedValue = $(control).val();

        $("select.ConfigurableAttribute").each(function () {
            Values.push($(this).val());
            Codes.push($(this).attr('id'));
        });

        $(" input.ConfigurableAttribute:checked").each(function () {
            Values.push($(this).val());
            Codes.push($(this).attr('code'));
        });

        var parameters = {
            "SelectedCode": selectedCode,
            "SelectedValue": selectedValue,
            "SKU": sku,
            "Codes": Codes.join(),
            "Values": Values.join(),
            "ParentProductId": productId,
            "ParentProductSKU": ParentProductSKU,
            "IsQuickView": $("#isQuickView").val(),
            "UserId": $("#hdnUserId").val(),
            "PortalId": Order.prototype.GetPortalId(),
            "LocaleId": $("#LocaleId").val()
        };


        Endpoint.prototype.GetProduct(parameters, function (res) {
            $(".popover-content").html(res);
        });
    }

    ReviewOrder(): any {
        var isValid = Order.prototype.ValidateDetails("false");

        if (isValid) {
            ZnodeBase.prototype.ShowLoader();
            $.ajax({
                url: "/Order/GetReviewOrder",
                data: $("#checkoutform").serialize(),
                type: 'POST',
                success: function (data) {
                    Order.prototype.HideLoader();
                    $("#ReviewDiv").html(data);
                    if ($("#ddlPaymentTypes option:selected").val() != "") {
                        $("#paymentMethodName").html($("#ddlPaymentTypes option:selected").text());
                    }
                    let shippingId: string = $("input[name='ShippingId']:checked").attr("id");
                    let shippingName: string = $("#" + shippingId).attr("data-shipping-name");
                    shippingName = typeof shippingName == "undefined" || shippingName == "" ? $("input[name='ShippingId']:checked").attr("data-shipping-name") : shippingName;
                    $("#selectedShippingName").html(shippingName);
                    GridPager.prototype.Init();
                    if (Order.prototype.IsQuote()) {
                        $("#divPaymentReview").hide();
                        $("#label-review-order").html(ZnodeBase.prototype.getResourceByKeyName("LabelReviewQuote"));
                        $("#label-review-order-sub-msg").html(ZnodeBase.prototype.getResourceByKeyName("ReviewQuoteSubMessage"));
                    }
                }
            });
        }
        return isValid;
    }

    SetCreditCardValidations(): any {
        $('input[data-payment="exp-month"]').on("keypress", function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

        $('input[data-payment="exp-month"]').on("focusout", function (e) {
            var monthVal = $('input[data-payment="exp-month"]').val();
            if (monthVal.length == 1 && (monthVal >= 1 || monthVal <= 9)) {
                monthVal = 0 + monthVal;
                $('input[data-payment="exp-month"]').val(monthVal);
            }
        });
        $('input[data-payment="exp-year"]').on("keypress", function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        $('input[data-payment="cvc"]').on("keypress", function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    }

    CanclePayment(): any {
        Order.prototype.HidePaymentProcessDialog();
        $("#ajaxBusy").dialog('close');
        $("#div-CreditCard").hide();
        $("#div-CreditCard [data-payment='number']").val('');
        $("#div-CreditCard [data-payment='cvc']").val('');
        $("#div-CreditCard [data-payment='exp-month']").val('');
        $("#div-CreditCard [data-payment='exp-year']").val('');
        $("#div-CreditCard [data-payment='cardholderName']").val('');
        $("#ddlPaymentTypes").val('');
        $("#ddlPaymentTypes option").each(function () {
            if ($(this).html() == "Select Payment") {
                $(this).attr("selected", "selected");
                return;
            }
        });
    }

    GetPaymentDetails(paymentSettingId): any {
        Endpoint.prototype.GetPaymentDetails(paymentSettingId, function (response) {
            if (!response.HasError) {
                $("#hdnGatwayName").val(response.GatewayName);
                $("#paymentProfileId").val(response.PaymentProfileId);
                $("#hdnPaymentApplicationSettingId").val(response.PaymentApplicationSettingId);
                $("#hdnEncryptedTotalAmount").val(response.Total);
                $("#hdnIsCreditCardEnabled").val(response.IsCreditCardEnabled);
            }
        });
    }

    GetEncryptedAmount(total): any {
        Endpoint.prototype.GetEncryptedAmountByAmount(total, function (response) {
            if (response != undefined || response.data != undefined) {
                $("#hdnEncryptedTotalAmount").val(response.data);
            }
        });
    }

    ClearPaymentAndDisplayMessage(message): any {
        Order.prototype.CanclePayment();
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(message, "error", isFadeOut, fadeOutTime);
    }

    creditCardPayment(selectionId, isFirstTime): any {
        $(".HidePaymentTypeDiv").hide();
        $("#div-CreditCard input:password").val("");
        //$("#div-CreditCard input:text").val("");
        $("#SaveCreditCard").prop("checked", false);
        $("#PaymentSettingId").val($(selectionId).val());
        $("#hdnGatwayName").val('');
        $("#hdnEncryptedTotalAmount").val('');

        Order.prototype.GetPaymentDetails($(selectionId).val());

        if ($("#hdnGatwayName").val() != undefined && $("#hdnGatwayName").val().length > 0) {
            var gatewayName = $("#hdnGatwayName").val();
            var paymentAppGatewayName = $("#hdnGatwayName").val().toLowerCase();
            var isCreditCardEnabled = $("#hdnIsCreditCardEnabled").val();
            if (isCreditCardEnabled == "0" && !isFirstTime) {
                Order.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorOrderOverDueAmount"));
                return false;
            }

            if (gatewayName.toLowerCase() == 'authorize.net') {
                paymentAppGatewayName = 'authorizenet';
            }

            if (gatewayName.toLowerCase() == 'chase paymentech') {
                paymentAppGatewayName = 'paymentech';
            }
            if (gatewayName.toLowerCase() == "twocheckout") {
                $('#Save-credit-card').hide();
            }
            if (gatewayName.toLowerCase() == "payflow") {
                $('#Save-credit-card').hide();
            }

            var profileId = null;
            if ($("#paymentProfileId").val().length > 0) {
                profileId = $("#paymentProfileId").val();
            }

            var paymentCreditCardModel = {
                gateway: paymentAppGatewayName,
                profileId: profileId,
                paymentSettingId: $('#hdnPaymentApplicationSettingId').val(),
                twoCoUrl: Config.PaymentTwoCoUrl + $('#hdnPaymentApplicationSettingId').val(),
                customerGUID: $("#hdnCustomerGUID").val()
            };
            $.ajax({
                type: "POST",
                url: Config.PaymentScriptUrl,
                data: paymentCreditCardModel,
                success: function (response) {
                    $("#div-CreditCard").show();
                    Order.prototype.AppendResponseToHTML(response);
                    Order.prototype.SetCreditCardValidations();
                    if ($("#hdnAnonymousUser").val() == "true" || gatewayName.toLowerCase() == "twocheckout" || gatewayName.toLowerCase() == "payflow") {
                        $("#Save-credit-card").hide();
                    }
                    else {
                        $("#Save-credit-card").show();
                    }

                    if (enabledPaymentProviders != '') {
                        var payProvidersHtml = '';
                        var toSplittedPayProviders = enabledPaymentProviders.split(",");
                        for (var iPayProviders = 0; iPayProviders < toSplittedPayProviders.length; iPayProviders++) {
                            payProvidersHtml += "<div class='col-xs-12 col-sm-3 nopadding save-cart'><input id=radioPaymentProviders" + iPayProviders + " type=radio name=PaymentProviders value=" + toSplittedPayProviders[iPayProviders] + " /><label class='lbl padding-8' for=radioPaymentProviders" + iPayProviders + "><img src=../../Content/images/" + toSplittedPayProviders[iPayProviders] + ".png class='img-responsive' style='float:right;' /></label></div>";
                        }

                        $('#paymentProviders').html("<ul>" + payProvidersHtml + "</ul>");
                        $("#" + $('input[name="PaymentProviders"]')[0].id).prop("checked", true);
                    }
                    if (savedUserCCDetails != '') {
                        $('#radioCCList').show();
                        $('#radioCCList').html('');
                        var iCCOrder = 0;
                        var creditCardHtml = "";
                        $.each(JSON.parse(savedUserCCDetails), function () {
                            creditCardHtml += "<div class='col-sm-12 nopadding'><label><input onclick=Order.prototype.OnSavedCreditCardClick(" + this['CreditCardLastFourDigit'].split(" ")[3] + "); id=radioSavedCreditCard" + iCCOrder + " type=radio name=CCListdetails value=" + this['PaymentGUID'] + " /><span class='lbl padding-8' for=radioSavedCreditCard" + iCCOrder + ">" + this['CreditCardLastFourDigit'] + "</span></label></div>";
                            iCCOrder++;
                        });
                        $('#radioCCList').append("<div class='col-sm-12 nopadding'>" + creditCardHtml + "</div>");
                        //$('input[name="CCListdetails"]')[0].checked = true;
                        $('#divAddNewCCDetails').show();
                        var savedCCRadio = $("#radioSavedCreditCard0");
                        if (savedCCRadio.length > 0) {
                            savedCCRadio.prop('checked', 'true');
                            savedCCRadio.parent().append(Order.prototype.GetCVVHtml())
                            savedCCRadio.click();
                        }
                        Order.prototype.BindEvent();
                    } else {
                        $('#divAddNewCCDetails').hide();
                        $('#credit-card-div').show();
                        jQuery('#creditCardTab').hide();
                        jQuery('#savedCreditCard-panel').removeClass('active');
                        jQuery('#addNewCreditCard-panel').addClass('in active');
                    }

                    $("#div-CreditCard").show();
                    $("#divOrderSavePage").hide();
                    $("#div-CreditCard [data-payment='number']").focus();

                    return false;
                },
                error: function () {
                    Order.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorContactPaymentApp"));
                    return false;
                }
            });

        } else {
            Order.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorPaymentGateway"));
            return false;
        }
        return false;
    }

    BindEvent(): void {
        $("#radioCCList input[type='radio']").on("change", Order.prototype.AppendCVVHtml);

        //restrict inputs
        $(document).on("keypress", 'input[data-payment="cvv"]', function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        //restrict cut-copy-paste
        $(document).on("cut copy paste", 'input[data-payment="cvv"]', function (e) {
            e.preventDefault();
        });
    }

    //Append CVV Code HTML
    AppendCVVHtml(event): any {
        var currentElement = event.currentTarget;
        $('.error-cvv').hide();
        $('[name=SaveCard-CVV]').hide()
        $(currentElement).parent().find('[name=SaveCard-CVV]').length > 0 ? $(currentElement).parent().find('[name=SaveCard-CVV]').show() : $(currentElement).parent().append(Order.prototype.GetCVVHtml());
    }
    //Get CVV Code HTML
    GetCVVHtml(): string {
        return "<div class='col-sm-12 nopadding margin-top margin-bottom'><input class='form-control' name='SaveCard-CVV' data-payment='cvv' type='password' placeholder='Enter CVV' maxlength='3'/></div>";
    }

    private AppendResponseToHTML(response): void {
        //if script element is already present then add html to response
        if ($("#div_payment_option").find("script").length > 0) {
            $("#div_payment_option").find("script").html(response);
            return;
        }
        var script: any = document.createElement("script");
        script.innerHTML = response;
        $("#div_payment_option").append(script);
    }

    OnSavedCreditCardClick(cardNo): any {
        $("#hdnCreditCardNumber").val(cardNo);
    }
    IsOrderTotalGreaterThanZero(total): any {
        if (total > 0.00) {
            return true;
        } else {
            Order.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorZeorOrderTotal"));
        }
    }

    Mod10(ccNum): boolean {
        var valid = "0123456789";  // Valid digits in a credit card number
        var len = ccNum.length;  // The length of the submitted cc number
        var iCCN = parseInt(ccNum);  // integer of ccNum
        var sCCN = ccNum.toString();  // string of ccNum

        sCCN = sCCN.replace(/^\s+|\s+$/g, '');  // strip spaces

        var iTotal = 0;  // integer total set at zero
        var bNum = true;  // by default assume it is a number
        var bResult = false;  // by default assume it is NOT a valid cc
        var temp;  // temp variable for parsing string
        var calc;  // used for calculation of each digit

        // Determine if the ccNum is in fact all numbers
        for (var j = 0; j < len; j++) {
            temp = "" + sCCN.substring(j, j + 1);
            if (valid.indexOf(temp) == -1) {
                bNum = false;
            }
        }

        // if it is NOT a number, you can either alert to the fact, or just pass a failure
        if (!bNum) {
            bResult = false;
        }

        // Determine if it is the proper length
        if ((len == 0) && (bResult)) {  // nothing, field is blank AND passed above # check
            bResult = false;
        } else {  // ccNum is a number and the proper length - let's see if it is a valid card number
            if (len >= 15) {  // 15 or 16 for Amex or V/MC
                for (var i = len; i > 0; i--) {  // LOOP throught the digits of the card
                    calc = Math.floor(iCCN) % 10;  // right most digit
                    calc = Math.floor(parseInt(calc));  // assure it is an integer
                    iTotal += calc;  // running total of the card number as we loop - Do Nothing to first digit
                    i--;  // decrement the count - move to the next digit in the card
                    iCCN = iCCN / 10;                               // subtracts right most digit from ccNum
                    calc = Math.floor(iCCN) % 10;    // NEXT right most digit
                    calc = calc * 2;                                       // multiply the digit by two
                    // Instead of some screwy method of converting 16 to a string and then parsing 1 and 6 and then adding them to make 7,
                    // I use a simple switch statement to change the value of calc2 to 7 if 16 is the multiple.
                    switch (calc) {
                        case 10: calc = 1; break;       //5*2=10 & 1+0 = 1
                        case 12: calc = 3; break;       //6*2=12 & 1+2 = 3
                        case 14: calc = 5; break;       //7*2=14 & 1+4 = 5
                        case 16: calc = 7; break;       //8*2=16 & 1+6 = 7
                        case 18: calc = 9; break;       //9*2=18 & 1+8 = 9
                        default: calc = calc;           //4*2= 8 &   8 = 8  -same for all lower numbers
                    }
                    iCCN = iCCN / 10;  // subtracts right most digit from ccNum
                    iTotal += calc;  // running total of the card number as we loop
                }  // END OF LOOP
                if ((iTotal % 10) == 0) {  // check to see if the sum Mod 10 is zero
                    bResult = true;  // This IS (or could be) a valid credit card number.
                } else {
                    bResult = false;  // This could NOT be a valid credit card number
                }
            }
        }
        return bResult; // Return the results
    }

    DetectCardType(number): any {
        var re = {
            electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
            maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
            dankort: /^(5019)\d+$/,
            interpayment: /^(636)\d+$/,
            unionpay: /^(62|88)\d+$/,
            visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
            mastercard: /^5[1-5]\d{14}$|^2(?:2(?:2[1-9]|[3-9]\d)|[3-6]\d\d|7(?:[01]\d|20))\d{12}$/,
            amex: /^3[47][0-9]{13}$/,
            diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
            discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
            jcb: /^(?:2131|1800|35\d{3})\d{11}$/
        };
        if (re.electron.test(number)) {
            return 'ELECTRON';
        } else if (re.maestro.test(number)) {
            return 'MAESTRO';
        } else if (re.dankort.test(number)) {
            return 'DANKORT';
        } else if (re.interpayment.test(number)) {
            return 'INTERPAYMENT';
        } else if (re.unionpay.test(number)) {
            return 'UNIONPAY';
        } else if (re.visa.test(number)) {
            return 'VISA';
        } else if (re.mastercard.test(number)) {
            return 'MASTERCARD';
        } else if (re.amex.test(number)) {
            return 'AMEX';
        } else if (re.diners.test(number)) {
            return 'DINERS';
        } else if (re.discover.test(number)) {
            return 'DISCOVER';
        } else if (re.jcb.test(number)) {
            return 'JCB';
        } else {
            return undefined;
        }
    }

    SubmitPayment(): any {
        var Total = $("#hdnTotalAmt").val();
        if (Order.prototype.IsOrderTotalGreaterThanZero(Total)) {

            var payment = Order.prototype.GetPaymentModel();
            var cardType = Order.prototype.DetectCardType($("#div-CreditCard [data-payment='number']").val());

            var paymentDetails = {
                "CardType": cardType,
                "CardExpirationMonth": $("#div-CreditCard [data-payment='exp-month']").val(),
                "CardExpirationYear": $("#div-CreditCard [data-payment='exp-year']").val()
            }

            $("#div-CreditCard").hide();
            submitCard(payment, function (response) {
                if (response.GatewayResponse == undefined) {
                    if (response.indexOf("Unauthorized") > 0) {
                        Order.prototype.ClearPaymentAndDisplayMessage('We were unable to process your credit card payment. <br /><br />Reason:<br />' + response + '<br /><br />If the problem persists, contact us to complete your order.');
                    }
                } else {
                    var isSuccess = response.GatewayResponse.IsSuccess;
                    if (isSuccess) {
                        var submitPaymentViewModel = {
                            PaymentSettingId: $('#PaymentSettingId').val(),
                            PaymentApplicationSettingId: $('#hdnPaymentApplicationSettingId').val(),
                            CustomerProfileId: response.GatewayResponse.CustomerProfileId,
                            CustomerPaymentId: response.GatewayResponse.CustomerPaymentProfileId,
                            CustomerShippingAddressId: response.GatewayResponse.CustomerShippingAddressId,
                            CustomerGuid: response.GatewayResponse.CustomerGUID,
                            PaymentToken: $("input[name='CCdetails']:checked").val(),
                            UserId: $("#hdnUserId").val(),
                            ShippingOptionId: $("[name='ShippingId']").val(),
                            BillingAddressId: $("#UserAddressDataViewModel_BillingAddress_AddressId").val(),
                            ShippingAddressId: $("#UserAddressDataViewModel_ShippingAddress_AddressId").val(),
                            CardSecurityCode: $("#div-CreditCard [data-payment='cvc']").val(),
                            PortalId: Order.prototype.GetPortalId(),
                            PortalCatalogId: $("#PortalCatalogId").val(),
                            AdditionalInfo: $("#additionalInstructions").val(),
                            EnableAddressValidation: $("input[name='EnableAddressValidation']").val(),
                            RequireValidatedAddress: $("input[name='RequireValidatedAddress']").val(),
                            CreditCardNumber: $("#hdnCreditCardNumber").val(),
                            AccountNumber: $("#ShippingListViewModel_AccountNumber").val(),
                            ShippingMethod: $("#ShippingListViewModel_ShippingMethod").val(),
                            CardType: paymentDetails.CardType,
                            CardExpiration: paymentDetails.CardExpirationMonth + "-" + paymentDetails.CardExpirationYear,
                        };
                        submitPaymentViewModel["CardSecurityCode"] = submitPaymentViewModel["PaymentToken"] ? $("[name='SaveCard-CVV']:visible").val() : $("#div-CreditCard [data-payment='cvc']").val();
                        $.ajax({
                            type: "POST",
                            url: "/order/submitpayment",
                            data: submitPaymentViewModel,
                            success: function (response) {
                                if (response.Data.OrderId !== undefined && response.Data.OrderId > 0) {
                                    $("#ajaxBusy").dialog('close');
                                    var form = $('<form action="CheckoutReceipt" method="post">' +
                                        '<input type="hidden" name="orderId" value="' + response.Data.OrderId + '" />' +
                                        '<input type="text" name= "ReceiptHtml" value= "' + response.Data.ReceiptHtml + '" />' +
                                        '<input type="hidden" name= "IsEmailSend" value= ' + response.Data.IsEmailSend + ' />' +
                                        '</form>');
                                    $('body').append(form);
                                    $(form).submit();
                                }
                                else {
                                    $("#ajaxBusy").dialog('close');
                                    var errorMessage = response.Data.ErrorMessage == undefined || response.Data.ErrorMessage == "" ? ZnodeBase.prototype.getResourceByKeyName("ErrorOrderPlace") : response.Data.ErrorMessage;
                                    Order.prototype.ClearPaymentAndDisplayMessage(errorMessage);
                                    return false;
                                }
                            },
                            error: function () {
                                Order.prototype.ClearPaymentAndDisplayMessage("Failed to process order");
                            }
                        });
                    }
                    else {
                        var errorMessage = response.GatewayResponse.ResponseText;
                        if (errorMessage == undefined) {
                            errorMessage = response.GatewayResponse.GatewayResponseData;
                        }

                        if (errorMessage != undefined && errorMessage.toLowerCase().indexOf("missing card data") >= 0) {
                            Order.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorCardDataMissing"));
                        } else if (errorMessage != undefined && errorMessage.indexOf("Message=") >= 0) {
                            Order.prototype.ClearPaymentAndDisplayMessage(errorMessage.substr(errorMessage.indexOf("=") + 1));
                            $("#div-CreditCard").show();
                        } else if (errorMessage.indexOf('customer') > 0) {
                            Order.prototype.ClearPaymentAndDisplayMessage(errorMessage);
                        } else {
                            Order.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorOrderPlace"));
                        }
                    }
                }
            });
        }
    }

    PaypalExpressCheckout(): any {
        var isValid = true;
        isValid = Order.prototype.ValidateDetails("false");
        if (isValid) {
            return Order.prototype.PayPalPaymentProcess();
        }
    }

    PayPalPaymentProcess(): any {
        var Total = $("#hdnTotalAmt").val();
        if (Order.prototype.IsOrderTotalGreaterThanZero(Total)) {
            var paymentOptionId = $('#ddlPaymentTypes').val();
            var urlhost = document.location.origin;//window.location.host;
            var cancelUrl = urlhost + "/order/createorder";
            var returnUrl = urlhost + "/order/submitpaypalorder?paymentOptionId=" + paymentOptionId;

            var shippingId = $("input[name='ShippingId']:checked").val();
            var additionalInformation = $("#additionalInstructions").val();
            if (shippingId == undefined) {
                returnUrl = returnUrl + "&shippingId=0&additionalNotes=" + additionalInformation;
            }
            else {
                returnUrl = returnUrl + "&shippingId=" + shippingId + "&additionalNotes=" + additionalInformation;
            }

            var billingaddressId = $("#UserAddressDataViewModel_BillingAddress_AddressId").val();
            var shipingaddressId = $("#UserAddressDataViewModel_ShippingAddress_AddressId").val();
            var paymentmodel = {
                PaymentSettingId: paymentOptionId,
                UserId: $("#hdnUserId").val(),
                BillingAddressId: billingaddressId,
                ShippingAddressId: shipingaddressId,
                PortalId: Order.prototype.GetPortalId(),
                PortalCatalogId: $("#PortalCatalogId").val(),
                PaymentApplicationSettingId: $("#hdnPaymentApplicationSettingId").val(),
                PayPalReturnUrl: returnUrl,
                PayPalCancelUrl: cancelUrl
            };
            var paypalDetails = [];
            Endpoint.prototype.ProcessPayPalPayment(paymentmodel, function (response) {
                var message = response.message;
                if (message.toLowerCase().indexOf("false") >= 0) {
                    Order.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorProcessPayment"));
                    $("#paypal-button").hide();
                    return false;
                }
                else {

                    if (message != undefined && message.indexOf('Message=') >= 0) {
                        var errorMessage = message.substr(message.indexOf('=') + 1);
                        Order.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorOrderOverDueAmount"));
                        $("#ddlPaymentTypes").val('');
                    } else if (message.indexOf("http") != -1) {
                        paypalDetails.push(response.message);
                        paypalDetails.push(response.token);
                    }
                    else {
                        Order.prototype.ClearPaymentAndDisplayMessage(message);
                        $("#ddlPaymentTypes").val(paymentOptionId);
                    }
                }
            });
            return paypalDetails;
        }
    }
    ShippingSelectHandler(ShippingId, shippingTypeClass): any {
        $("#hndShippingclassName").val(shippingTypeClass);
        $("#AccountNumber").val("");
        $("#AccountNumber").val("");
        if (shippingTypeClass == Constant.ZnodeCustomerShipping) {
            $("#customerShippingDiv").show();
            Order.prototype.HideLoader();
        }
        else {
            $("#customerShippingDiv").hide();
        }
        ZnodeBase.prototype.ShowLoader();
        Endpoint.prototype.ShippingSelectHandler(Number($("#hdnUserId").val()), ShippingId, Order.prototype.GetOrderId(), function (response) {
            $("#asidePannelmessageBoxContainerId").hide();
            if (response.shippingErrorMessage != "" && response.shippingErrorMessage != null && response.shippingErrorMessage != "undefined") {
                $("#asidePannelmessageBoxContainerId div").html(response.shippingErrorMessage);
                $("#asidePannelmessageBoxContainerId").show();
                return false;
            }
            //Set shopping cart details of user.
            $("#orderLineItems").html(response.CartView);
            Order.prototype.OnTaxExemptPageLoadCheck();
            $("#lblShippingType").html(response.ShippingType);

            if ($("#hdnUserId").val() > 0) {
                $("#ShoppingCartDiv").find('#productListDiv').show();
            }
            $("#isGiftcardValid").val("true");
            $("#cart-giftcard-status").hide().html("");
            var success: boolean = $("#hdnGiftCardApplied").val();
            var message: string = $("#hdnGiftCardMessage").val();

            if (message != '') {
                if (success) {
                    var giftCardMsg: string = "<p class='green- color'>" + "<a href='#' class='z-close' onclick='Order.prototype.RemoveGiftCard();'></a>" + " - " + message + "</p>";
                    $("#cart-giftcard-status").show().html(giftCardMsg);
                } else {
                    var giftCardMsg: string = "<p class='field-validation-error'>" + "<a href='#' class='z-close' onclick='Order.prototype.RemoveGiftCard();'></a>" + " - " + message + "</p>";
                    $("#cart-giftcard-status").show().html(giftCardMsg);
                    $("#isGiftcardValid").val("false");
                }
            }
            ZnodeBase.prototype.CancelUpload('ShippingPanel');
            Order.prototype.HideLoader();
            Order.prototype.ShippingErrorMessage();
        });
    }


    ShippingChangeHandler(ShippingclassName: string, shippingCode: string = ""): any {
        ZnodeBase.prototype.ShowLoader();
        $("#hndShippingclassName").val(ShippingclassName);
        if (ShippingclassName == Constant.ZnodeCustomerShipping) {
            $("#customerShippingDiv").show();
            Order.prototype.HideLoader();
        }
        else {
            $("#ShippingListViewModel_AccountNumber").val("");
            $("#ShippingListViewModel_ShippingMethod").val("");
            $("#customerShippingDiv").hide();
        }

        $.ajax({
            url: "/Order/CalculateShippingCharges",
            data: $("#checkoutform").serialize(),
            type: 'POST',
            success: function (data) {
                $("#divShoppingCart").html("");
                //Set shopping cart details of user.
                $("#divShoppingCart").html(data);
                if ($("#hdnUserId").val() > 0) {
                    $("#ShoppingCartDiv").find('#productListDiv').show();
                }
                $("#isGiftcardValid").val("true");
                $("#cart-giftcard-status").hide().html("");
                var success: boolean = $("#hdnGiftCardApplied").val();
                var message: string = $("#hdnGiftCardMessage").val();

                if (message != '') {
                    if (success) {
                        var giftCardMsg: string = "<p class='green- color'>" + "<a href='#' class='z-close' onclick='Order.prototype.RemoveGiftCard();'></a>" + " - " + message + "</p>";
                        $("#cart-giftcard-status").show().html(giftCardMsg);
                    } else {
                        var giftCardMsg: string = "<p class='field-validation-error'>" + "<a href='#' class='z-close' onclick='Order.prototype.RemoveGiftCard();'></a>" + " - " + message + "</p>";
                        $("#cart-giftcard-status").show().html(giftCardMsg);
                        $("#isGiftcardValid").val("false");
                    }
                }
                Order.prototype.HideLoader();
                Order.prototype.ShippingErrorMessage();
                Order.prototype.ShowAllowedTerritoriesError();
            },
            error: function (textStatus, errorThrown) {
                Order.prototype.HideLoader();
            }
        });

        if (shippingCode != "") {
            $(".dev-custom-shipping-edit").hide();
            $(".dev-custom-shipping").hide();

            Order.prototype.ShowEditCustomShipping(shippingCode);
        }

    }

    public ShowEditCustomShipping(shippingCode: string): void {
        if (location.pathname.indexOf("Quote") <= -1)
            $("#div-Edit-" + shippingCode + "").show();
    }

    GetSelectedAddons(): any {
        var addOnValues = [];
        $(".AddOn").each(function () {
            var values = "";
            if ($(this).is(":checked")) {
                values = $(this).val();
            }
            else {
                values = $(this).children(":selected").attr("data-addonsku");
            }
            if (values != null && values != "") {
                addOnValues.push(values);
            }
        });
        return addOnValues;
    }

    GetSelectedBundelProducts(): any {
        var bundleProducts = [];
        $(".bundle").each(function () {
            var values = $(this).attr("data-bundlesku");
            bundleProducts.push(values);
        });
        return bundleProducts;
    }

    SetCartItemModelValues(addOnValues, bundleProducts, groupProducts, groupProductsQuantity, quantity, personalisedcodes, personalisedvalues, groupProductName = ""): any {
        $("#dynamic-addonProductSKU").val(addOnValues);
        $("#dynamic-portalId").val(Order.prototype.GetPortalId());
        $("#dynamic-catalogId").val($("#PortalCatalogId").val());
        $("#dynamic-orderId").val(Order.prototype.GetOrderId());
        $("#dynamic-userId").val($("#hdnUserId").val());
        $("#dynamic-localeId").val($("#LocaleId").val());
        $("#dynamic-bundleProductSKU").val(bundleProducts);
        if (quantity != null || quantity != "") {
            $("#dynamic-quantity").val(quantity);
        }
        $("#dynamic-groupProductSKUs").val(groupProducts);
        $("#dynamic-groupProductsQuantity").val(groupProductsQuantity);
        $("#dynamic-personalisedcodes").val(personalisedcodes);
        $("#dynamic-personalisedvalues").val(personalisedvalues);
        $("#dynamic-shippingId").val($("input[name='ShippingId']:checked").val());
        $("#dynamic-groupProductNames").val(groupProductName);
    }

    OnAssociatedProductQuantityChange(): boolean {
        $("#dynamic-product-variations .quantity").each(function () {
            var _productDetail = Order.prototype.BindProductModel(this, true);
            if (_productDetail.Quantity != null && _productDetail.Quantity != "") {
                if (Order.prototype.CheckIsNumeric(_productDetail.Quantity, _productDetail.QuantityError)) {
                    if (Order.prototype.CheckDecimalValue(_productDetail.DecimalPoint, _productDetail.DecimalValue, _productDetail.InventoryRoundOff, _productDetail.QuantityError)) {
                        if (Order.prototype.CheckQuantityGreaterThanZero(parseInt(_productDetail.Quantity), _productDetail.QuantityError)) {
                            $("#button-addtocart").prop("disabled", false);
                            $(_productDetail.QuantityError).text("");
                            $(_productDetail.QuantityError).removeClass("error-msg");
                            return true;
                        }
                    }
                }
            }
            else {
                $("#button-addtocart").prop("disabled", false);
                $(_productDetail.QuantityError).text("");
                $(_productDetail.QuantityError).removeClass("error-msg");
                return true;
            }
            return false;
        });
        return true;
    }

    CheckDecimalValue(decimalPoint: number, decimalValue: number, inventoryRoundOff: number, quantityError: string, isPrice: boolean = false): boolean {
        if (decimalValue != 0 && decimalPoint > inventoryRoundOff) {
            if (isPrice)
                $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("EnterPriceHaving") + inventoryRoundOff + ZnodeBase.prototype.getResourceByKeyName("XNumbersAfterDecimalPoint"));
            else
                $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("EnterQuantityHaving") + inventoryRoundOff + ZnodeBase.prototype.getResourceByKeyName("XNumbersAfterDecimalPoint"));
            $(quantityError).css("class", "error-msg");
            $("#button-addtocart").attr("disabled", "disabled");
            return false;
        }
        return true;
    }

    CheckIsNumeric(selectedQty: string, quantityError: string): boolean {
        var matches = selectedQty.match(/^-?[\d.]+(?:e-?\d+)?$/);
        if (matches == null) {
            $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("RequiredNumericValue"));
            $(quantityError).css("class", "error-msg");
            $("#button-addtocart").attr("disabled", "disabled");
            return false;
        }
        return true;
    }

    CheckQuantityGreaterThanZero(selectedQty: number, quantityError: string): boolean {
        if (selectedQty == 0) {
            $("#button-addtocart").attr("disabled", "disabled");
            $(quantityError).css("class", "error-msg");
            $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("ErrorProductQuantity"));
            return false;
        }
        return true;
    }

    BindProductModel(control, isGroup: boolean): Znode.Core.ProductDetailModel {
        var _productDetail: Znode.Core.ProductDetailModel = {
            InventoryRoundOff: parseInt($(control).attr("data-inventoryroundoff")),
            ProductId: parseInt($(control).attr('data-productId')),
            Quantity: $(control).val(),
            SKU: $(control).attr("data-sku"),
            MainProductSKU: $(control).attr("data-parentsku"),
            DecimalPoint: $(control).val().split(".")[1] != null ? $(control).val().split(".")[1].length : 0,
            DecimalValue: $(control).val().split(".")[1] != null ? $(control).val().split(".")[1] : 0,
            QuantityError: isGroup ? "#quantity-error-msg_" + $(control).attr('data-productId') : "#quantity-error-msg",
            MainProductId: parseInt($(control).attr("data-parentProductId")),
        };
        return _productDetail;
    }

    ApplyPromoCode(isCouponApplied): void {
        if ($("#promocode").val() == "" || $("#promocode").val() == undefined) {
            var htmlString: string = "<div>";
            htmlString = htmlString + "<p class='field-validation-error'>" + ZnodeBase.prototype.getResourceByKeyName("ErrorCouponCode") + "</p>";
            htmlString = htmlString + "</div>";
            $("#couponContainer").show();
            $("#couponContainer").html("");
            $("#couponContainer").html(htmlString);
        }
        else {
            var orderId: number = Order.prototype.GetOrderId();
            var url: string = "";
            if (orderId > 0)
                url = "/Order/ManageApplyCoupon?orderId=" + orderId + "";
            else
                url = "/Order/ApplyCoupon";

            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.useCouponCode(url, $("#promocode").val(), function (response) {
                if (orderId > 0) {
                    $("#orderLineItems").html(response.html);
                    Order.prototype.OnTaxExemptPageLoadCheck();
                    Order.prototype.BindCouponHtml(response.coupons);
                }
                else {
                    $("#divShoppingCart").html("");
                    $("#divShoppingCart").html(response.html);
                    Order.prototype.BindCouponHtml(response.coupons);
                }
            });
        }
    }

    BindCouponHtml(couponsCodes): any {
        if (couponsCodes != null) {
            var coupons = couponsCodes;
            var htmlString: string = "<div>";
            for (var couponIndex = 0; couponIndex < coupons.length; couponIndex++) {
                var style: string = coupons[couponIndex].CouponApplied ? "green-color" : "field-validation-error";
                var message: string = coupons[couponIndex].PromotionMessage;
                var couponCode: string = coupons[couponIndex].Code;
                htmlString = htmlString + "<p class='" + style + "'>";
                if (!coupons[couponIndex].IsExistInOrder) {
                    htmlString += "<a href='#' class='z-close dirtyignore' onclick='Order.prototype.RemoveCoupon(&#39;" + couponCode + "&#39;)'></a>";
                }
                htmlString += "<b>" + couponCode + "</b> " + " - " + message + "</p>";
                if (coupons[couponIndex].CouponApplied) {
                    $("#promocode").val(coupons[couponIndex].Coupon);
                }
            }
            htmlString = htmlString + "</div>";
            $("#couponContainer").show();
            $("#couponContainer").html("");
            $("#couponContainer").html(htmlString);
        }
        Order.prototype.HideLoader();
        $('*[data-autocomplete-url]').each(function () { autocompletewrapper($(this), $(this).data("onselect-function")); });
    }

    public ApplyTaxExempt(): void {
        $("#containerTaxExempt").show();
        $("#btnTaxExempt").hide();
    }

    public ConfirmTaxExemptOrder(): void {
        Endpoint.prototype.UpdateForTaxExempt(Order.prototype.GetOrderId(), "0.0", "TaxView", function (response) {
            $("#divShoppingCart").html("");
            $("#divShoppingCart").html(response);
            $("#chkTaxExempt").attr("disabled", true);
            $("#spnTaxExempt").hide();
            $("#messageTaxExcempt").html("This Order Tax Exempted");
        });
    }

    public OnTaxExemptPageLoadCheck(): void {
        if ($("#hdnTaxCostEdited").val() == "True") {
            $("#chkTaxExempt").attr("disabled", true);
            $("#chkTaxExempt").attr("checked", true);
            $("#spnTaxExempt").hide();
            $("#btnTaxExempt").hide();
            $("#containerTaxExempt").show();
            $("#messageTaxExcempt").html("This Order Tax Exempted");
        }
        $("#divTaxExemptContainer").show();
    }

    public OnTaxExemptChecked(): void {
        $("#PopUpTaxExemptSubmitOrder").modal("show");
    }

    RemoveCoupon(coupon): void {
        ZnodeBase.prototype.ShowLoader();
        var orderId: number = Order.prototype.GetOrderId();
        var url: string = "";
        if (orderId > 0)
            url = "/Order/ManageRemoveCoupon?orderId=" + orderId + "";
        else
            url = "/Order/RemoveCoupon";
        Endpoint.prototype.removeCouponCode(url, coupon, function (response) {
            if (Order.prototype.GetOrderId() > 0) {
                $("#orderLineItems").html(response.html);
                Order.prototype.OnTaxExemptPageLoadCheck();
                Order.prototype.BindCouponHtml(response.coupons);
            }
            else {
                $("#divShoppingCart").html("");
                $("#divShoppingCart").html(response.html);
                Order.prototype.BindCouponHtml(response.coupons);
            }
            $("#promocode").val("");
        });
    }

    RemoveOrderGridIcone(): any {
        Order.prototype.HideGridColumn('IsInRMA');
        $('#grid tbody tr').each(function () {
            if (($(this).find('td').find('.z-void-payment').attr('href').toLowerCase().indexOf('cc_authorized') >= 0) || ($(this).find('td').find('.z-void-payment').attr('href').toLowerCase().indexOf('cc_captured') >= 0)) {
                if ($(this).find('.paymentType').text().toLowerCase() == 'paypal express') {
                    $(this).find('.z-void-payment').parents('li').remove();
                }
            }
            else {
                $(this).find('.z-void-payment').parents('li').remove();
            }
        });
        $("#listcontainerId").show();
    }

    HideGridColumn(cssClass): any {
        var indexOfRow: number = $('#grid tbody tr:eq(0)').find('.' + cssClass + '').index() + 1;
        $('th:nth-child(' + indexOfRow + ')').hide();
        $('#grid tbody tr').find('.' + cssClass + '').hide();
    }

    ApplyGiftCard(): void {
        if ($("#txtgiftcard").val() == "" || $("#txtgiftcard").val() == undefined) {
            var htmlString: string = "<div>";
            htmlString = htmlString + "<p class='field-validation-error'>" + ZnodeBase.prototype.getResourceByKeyName("ErrorGiftCardNo") + "</p>";
            htmlString = htmlString + "</div>";
            $("#cart-giftcard-status").show();
            $("#cart-giftcard-status").html("");
            $("#cart-giftcard-status").html(htmlString);
        }
        else {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.applyGiftCard(Order.prototype.GetGiftCardURL(), $("#txtgiftcard").val(), function (response) {
                $("#divShoppingCart").html("");
                $("#divShoppingCart").html(response);
                $("#isGiftcardValid").val("true");
                $("#cart-giftcard-status").hide().html("");
                let success: string = $("#hdnGiftCardApplied").val();
                let message: string = $("#hdnGiftCardMessage").val();
                if (success.toLowerCase() == "true") {
                    var giftCardMsg: string = "<p class='green- color'>" + "<a href='#' class='z-close dirtyignore' onclick='Order.prototype.RemoveGiftCard();'></a>" + " - " + message + "</p>";
                    $("#cart-giftcard-status").show().html(giftCardMsg);
                } else {
                    var giftCardMsg: string = "<p class='field-validation-error'>" + "<a href='#' class='z-close dirtyignore' onclick='Order.prototype.RemoveGiftCard();'></a>" + " - " + message + "</p>";
                    $("#cart-giftcard-status").show().html(giftCardMsg);
                    $("#isGiftcardValid").val("false");
                }
                Order.prototype.HideLoader();
                $('*[data-autocomplete-url]').each(function () { autocompletewrapper($(this), $(this).data("onselect-function")); });
            });
        }
    }

    RemoveGiftCard(): any {
        ZnodeBase.prototype.ShowLoader();
        Endpoint.prototype.applyGiftCard(Order.prototype.GetGiftCardURL(), "", function (response) {
            $("#divShoppingCart").html("");
            $("#divShoppingCart").html(response);
            $("#isGiftcardValid").val("true");
            $("#cart-giftcard-status").hide().html("");
            if ($("#txtgiftcard").val().trim().length > 0) {
                var success = $("#hdnGiftCardApplied").val();
                var message = $("#hdnGiftCardMessage").val();
                $("#txtgiftcard").val("");
            }
            Order.prototype.HideLoader();
            $('*[data-autocomplete-url]').each(function () { autocompletewrapper($(this), $(this).data("onselect-function")); });
        });
    }

    GetOrderId(): number {
        var omsOrderId = $("#hdnManageOmsOrderId").val();
        if (omsOrderId != null && omsOrderId != "") {
            let orderId: number = parseInt(omsOrderId);
            if (orderId > 0) {
                return orderId;
            }
        }
        return 0;
    }

    GetGiftCardURL(): string {
        var orderId: number = Order.prototype.GetOrderId();
        var url: string = "";
        if (orderId > 0)
            url = "/Order/ManageApplyGiftCard?orderId=" + orderId + "";
        else
            url = "/Order/ApplyGiftCard";
        return url;
    }
    CustomerAddressViewHandler(control): any {
        Order.prototype.ShowHideAddressCheckBoxDiv(control);
        if (control == 'shipping') {
            $("#BillingAddressContainer").hide();
            $('#IsShippingAddressChange').val('true');
        }
        else if (control == 'billing') {
            $("#BillingAddressContainer").show();
            $("#ShippingAddressContainer").hide();
        }
        else {
            $("#addressDetails").find('.chkShippingBilling').show();
        }
        $("#addressDetails").find('#shippingSameAsBillingAddressDiv').remove();
    }

    ShowHideAddressCheckBoxDiv(addressType): any {
        if (addressType == 'shipping') {
            $("#DefaultBillingAddressDiv").hide();
            $("#DefaultShippingAddressDiv").show();
        }

        if (addressType == 'billing') {
            $("#DefaultShippingAddressDiv").hide();
            $("#DefaultBillingAddressDiv").show();
        }
    }
    GetCustomerAddressForManange(control, selectedAddressId, shippingBillingId): any {
        $("#customerDetails").html("");
        $("#hdnIsShipping").val(control);
        var ShippingAddressId: number = control == 'shipping' ? selectedAddressId : shippingBillingId;
        var BillingAddressId: number = control == 'billing' ? selectedAddressId : shippingBillingId;
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Order/GetUserAddressForManageById?selectedAddressId=' + selectedAddressId + '&ShippingAddressId=' + ShippingAddressId + '&BillingAddressId=' + BillingAddressId + '&userId=' + $("#hdnUserId").val() + '&portalId=' + Order.prototype.GetPortalId() + '' + '&control=' + control + '', 'addressDetails');
        $("#addressDetails").find('.chkShippingBilling').remove();
    }

    GetCustomerAddressForChange(control, selectedAddressId, shippingBillingId): any {
        $("#customerDetails").html("");
        $("#hdnIsShipping").val(control);
        var ShippingAddressId: number = control == 'shipping' ? selectedAddressId : shippingBillingId;
        var BillingAddressId: number = control == 'billing' ? selectedAddressId : shippingBillingId;
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Order/GetUserAddressById?selectedAddressId=' + selectedAddressId + '&ShippingAddressId=' + ShippingAddressId + '&BillingAddressId=' + BillingAddressId + '&userId=' + $("#hdnUserId").val() + '&portalId=' + Order.prototype.GetPortalId() + '', 'addressDetails');
        $("#addressDetails").find('.chkShippingBilling').remove();
    }

    CreateNewAddress(): any {
        $("#customerDetails").html("");
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Order/CreateNewAddress?userId=' + $('#hdnUserId').val() + '&portalId=' + Order.prototype.GetPortalId() + '', 'addressDetails');
    }
    ChangeAddressSuccessForManage(response): any {
        if (response.shippingErrorMessage != "" && response.shippingErrorMessage != null && response.shippingErrorMessage != "undefined") {

            $("#asidePannelmessageBoxContainerId div").html(response.shippingErrorMessage);
            $("#asidePannelmessageBoxContainerId").show();
            return false;
        }
        if (response.addressView.indexOf("field-validation-error") < 0) {
            Order.prototype.HideLoader();
            ZnodeBase.prototype.CancelUpload('addressDetails');
            $("#customerInformation").html(response.addressView);
            $("#divTotal").html(response.orderTotal);
            $("#divShoppingCart").html("");
            $("#divShoppingCart").html(response.totalView);
            Order.prototype.RemoveFormDataValidation();
            Order.prototype.ToggleFreeShipping();
        }
        else {
            $("#divCustomerAddressPopup").html(response.addressView);
            $(".chkShippingBilling").show();
        }
    }
    ChangeAddressSuccessCallback(response): any {
        if (response.ErrorMassage != "" && response.ErrorMassage != null && response.ErrorMassage != "undefined") {
            $("#asidePannelmessageBoxContainerId div").html(response.ErrorMassage);
            $("#asidePannelmessageBoxContainerId").show();
            return false;
        }
        if (response.addressView.indexOf("field-validation-error") < 0) {
            ZnodeBase.prototype.CancelUpload('addressDetails');
            $("#divShoppingCart").html("");
            $("#divShoppingCart").html(response.totalView);
            $("#customerAddresses").html(response.addressView);
            $("#shippingMethodDiv").html(response.shippingOptionView);
            Order.prototype.ShowAllowedTerritoriesError();
        }
        else {
            $("#divCustomerAddressPopup").html(response.addressView);
            $(".chkShippingBilling").show();
        }
    }


    RemoveFormDataValidation(): any {
        $('form').removeData('validator');
        $('form').removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse('form');
        $('#IsDefaultShipping').rules('remove');
        $('#IsDefaultBilling').rules('remove');
    }

    GetUserAddressBySelectedAddress(control): any {
        var addressId: number = $(control).val();
        var isShipping = $("#hdnIsShipping").val();
        var isB2BCustomer: boolean = $("#hdnAccountId").val() > 0 ? true : false;

        Endpoint.prototype.GetUserAddressBySelectedAddress(addressId, isB2BCustomer, $("#hdnUserId").val(), Order.prototype.GetPortalId(), $("#hdnAccountId").val(), function (response) {

            $("#addressDiv").html(response.html);
            Order.prototype.ShowHideAddressCheckBoxDiv(isShipping);
            if (response.address.AddressId == 0) {
                $(".chkShippingBilling").show();
                $('#UserId').val($("#hdnUserId").val());
                $('#AddressId').val(response.address.AddressId);
                $('#CountryName').prop('selectedIndex', 0);
                if (isShipping == 'shipping') {
                    $('#SelectedShippingId').val(response.address.AddressId);
                    $('#IsShippingAddressChange').val('true');
                }
                else
                    $('#SelectedBillingId').val(response.address.AddressId);
            }
            else {
                $('#AddressId').val(response.address.AddressId);
                if (isShipping == 'shipping') {
                    $('#SelectedShippingId').val(response.address.AddressId);
                    $('#IsShippingAddressChange').val('true');
                }
                else
                    $('#SelectedBillingId').val(response.address.AddressId);
            }
            Order.prototype.HideLoader();
        });
    }

    ValidateRefundOrder(): boolean {
        var status = true;
        for (var i = 0; i < $("#RefundOrderLineitems tr").not("thead tr").length; i++) {
            if (Number($("input[name='RefundOrderLineitems[" + i + "].RefundAmount']").val()) > Number($("input[name='RefundOrderLineitems[" + i + "].RefundableAmountLeft']").val())) {
                $("#valRefundOrderLineitems_" + i).text('').removeClass("field-validation-error");
                $("#valRefundOrderLineitems_" + i).text(ZnodeBase.prototype.getResourceByKeyName("RefundAmountError")).addClass("field-validation-error");
                $("#valRefundOrderLineitems_" + i).show();
                status = false;
            }
        }
        if (Number($("#ShippingRefundDetails_RefundAmount").val()) > Number($("#ShippingRefundDetails_RefundableAmountLeft").val())) {
            $("#valRefundOrderShipping").text('').removeClass("field-validation-error");
            $("#valRefundOrderShipping").text(ZnodeBase.prototype.getResourceByKeyName("RefundAmountError")).addClass("field-validation-error");
            $("#valRefundOrderShipping").show();
            status = false;
        }
        if (Number($("#TotalRefundDetails_RefundAmount").val()) > Number($("#TotalRefundDetails_RefundableAmountLeft").val())) {
            $("#valRefundOrderTotal").text('').removeClass("field-validation-error");
            $("#valRefundOrderTotal").text(ZnodeBase.prototype.getResourceByKeyName("RefundAmountError")).addClass("field-validation-error");
            $("#valRefundOrderTotal").show();
            status = false;
        }
        return status;
    }

    IsQuote(): boolean {
        var isQuote = $("#IsQuote").val();
        if (isQuote != null)
            return isQuote.toLowerCase() == "true";
        return false;
    }

    ApplyCSRDiscount(): any {
        if ($("#txtcsrDiscount").val() == "" || $("#txtcsrDiscount").val() == undefined) {
            var htmlString: string = "<div>";
            htmlString = htmlString + "<p class='field-validation-error'>" + ZnodeBase.prototype.getResourceByKeyName("ErrorCSRDiscount") + "</p>";
            htmlString = htmlString + "</div>";
            $("#csr-discount-status").show();
            $("#csr-discount-status").html("");
            $("#csr-discount-status").html(htmlString);
        }
        else if ($("#txtcsrDiscount").val() != "" && $("#txtcsrDiscount").val() != undefined && $("#txtcsrDiscount").val() < 0) {
            var htmlString: string = "<div>";
            htmlString = htmlString + "<p class='field-validation-error'>" + ZnodeBase.prototype.getResourceByKeyName("ErrorCSRDisountNegative") + "</p>";
            htmlString = htmlString + "</div>";
            $("#csr-discount-status").show();
            $("#csr-discount-status").html("");
            $("#csr-discount-status").html(htmlString);
        }
        else {
            if ($("#txtcsrDiscount").val() == "") {
                $("#txtcsrDiscount").val(0);
            }
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.ApplyCSRDiscount($("#txtcsrDiscount").val(), $("#txtcsrDesc").val(), function (response) {
                $("#divShoppingCart").html("");
                $("#divShoppingCart").html(response);
                if ($("#txtcsrDiscount").val().trim().length > 0) {
                    var success = $("#hdnCSRDiscountApplied").val();
                    var message = $("#hdnCsrSuccessMessage").val();
                    if (success.toLowerCase() == "true") {
                        var discountMsg = "<p class='green- color'>" + "<a href='#' class='z-close' onclick='Order.prototype.RemoveCSRDiscount();'></a>" + "  " + message + "</p>";
                        $("#csr-discount-status").show().html(discountMsg);
                    } else {
                        var discountMsg = "<p class='field-validation-error'>" + "<a href='#' class='z-close' onclick='Order.prototype.RemoveCSRDiscount();'></a>" + "  " + message + "</p>";
                        $("#csr-discount-status").show().html(discountMsg);
                    }
                }
                else {
                    $("#csr-discount-status").html("");
                }
                Order.prototype.HideLoader();
            });
            if ($("#txtcsrDiscount").val() == "0") {
                $("#txtcsrDiscount").val("");
            }
        }
    }

    RemoveCSRDiscount(): any {
        ZnodeBase.prototype.ShowLoader();
        Endpoint.prototype.ApplyCSRDiscount(0, "", function (response) {
            $("#divShoppingCart").html("");
            $("#divShoppingCart").html(response);
            var success = $("#hdnCSRDiscountApplied").val();
            var message = $("#hdnCsrSuccessMessage").val();
            $("#csr-discount-status").show().html(message);
            $("#txtcsrDiscount").val("");
            Order.prototype.HideLoader();
        });
    }


    AddNewUser(): any {
        var portalId: number = parseInt($("#txtPortalName").attr("data-portalid"));
        if (portalId > 0) {
            $("#ZnodeUserPortalList").html("");
            $("#ZnodeOrderCustomer").html("");
            ZnodeBase.prototype.BrowseAsidePoupPanel('/Order/AddNewCustomer?portalid=' + portalId + '', 'customerDetails');
        }
        else {
            $("#errorRequiredStore").text('').text(ZnodeBase.prototype.getResourceByKeyName("SelectPortal")).addClass("field-validation-error").show();
            $("#txtPortalName").addClass('input-validation-error');
            return false;
        }
    }

    MakeAccountIdMandetory(): void {
        if (Order.prototype.IsQuote()) {
            $("#lblAccountId").addClass("required");
        }
    }

    SetPortalId(): any {
        var portalId = Order.prototype.GetPortalId();
        $("#PortalId").val(portalId);
        $("#selectedPortalId").val(portalId);
        if (Order.prototype.IsQuote()) {
            if ($("#AccountId").val() == "") {
                $("#errorSelectAccountId").html("Please select account.");
                return false;
            }
        }
        $("#frmCreateCustomer").submit();
        return true;
    }

    OrderStatusTrackingNumHideShow(ShippingTypeName): any {
        var shippingTypeName = ShippingTypeName;
        if (shippingTypeName.toLowerCase().indexOf("fedex") >= 0) {
            $("#txtTrackingNumber").text(ZnodeBase.prototype.getResourceByKeyName("LabelFedExTrackingNo"));
        }
        else if (shippingTypeName.toLowerCase().indexOf("ups") >= 0) {
            $("#txtTrackingNumber").text(ZnodeBase.prototype.getResourceByKeyName("LabelUPSTrackingNo"));
        }
        else if (shippingTypeName.toLowerCase().indexOf("usps") >= 0) {
            $("#txtTrackingNumber").text(ZnodeBase.prototype.getResourceByKeyName("LabelUSPSTrackingNo"));
        }
        if (shippingTypeName.toLowerCase().indexOf("fedex") >= 0 || shippingTypeName.toLowerCase().indexOf("ups") >= 0 || shippingTypeName.toLowerCase().indexOf("usps") >= 0) {
            $("#btnChangeTrackingNumber").css("display", "inline");
        }

    }
    RemovePagingControlsFromGrid(): any {
        $("div.pagination-top").remove();
        $("div.pagination-bottom").remove();
        $("div.grid-control").remove();
    }

    AddNotesSuccessCallBack(): any {
        $("#AdditionalNotes").val('');
    }
    ManageNotesSuccessCallBack(): any {
        $("#AdditionalNotes").val('');
    }
    SubmitManageNotes(): any {
        $("#ManageOrderNotes").submit();
    }

    AddNotesValidation(): any {
        if ($("#AdditionalNotes").val() == '') {
            return false;
        }
        return true;
    }

    GetAccountId() {
        var href = window.location.href.toLowerCase();
        if (href.indexOf("?accountid=") != -1) {
            return Order.prototype.GetParameterByName("accountid", href);
        }
        else {
            return "0";
        }
    }

    GetParameterByName(name, url) {
        if (!url) {
            url = window.location.href.toLowerCase();
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    AddProductToCart(e): any {
        let selectedIds: string = DynamicGrid.prototype.GetMultipleSelectedIds();
        selectedProductIds = "";
        let count: number = selectedIds == "" ? 0 : selectedIds.split(",").length;

        if (count > 0) {
            let orderId: number = Order.prototype.GetOrderId();
            Endpoint.prototype.AddProductToCart(orderId, function (response) {
                Order.prototype.HideAsidePopUpPanel();
                if (orderId > 0) {
                    $("#orderLineItems").html(response.html);
                    Order.prototype.FreezManageOrder(false);
                    Order.prototype.OnTaxExemptPageLoadCheck();
                }
                else {
                    $("#divShoppingCart").empty();
                    $("#divShoppingCart").append(response.html);
                }
                Order.prototype.SetGiftCardMessage();
                Order.prototype.ClearShippingEstimates();
                if (Order.prototype.IsQuote()) {
                    $("#div-coupons-promotions").hide();
                }
                else {
                    if (typeof $("#hdnShoppingCartCount").val() == 'undefined' || $("#hdnShoppingCartCount").val() == '0') {
                        $("#div-coupons-promotions").hide();
                    }
                    else {
                        $("#div-coupons-promotions").show();
                        Order.prototype.BindCouponHtml(response.coupons);
                    }
                }
                Order.prototype.ToggleFreeShipping();
            });
        } else {
            $("#productMessageBoxContainerId").show();
        }
    }

    ClosePopUp(e): void {
        let productIds: string = Order.prototype.GetOrderId() > 0 ? selectedProductIds : DynamicGrid.prototype.GetMultipleSelectedIds();

        if (productIds != null && typeof productIds != "undefined" && productIds != "") {
            Order.prototype.RemoveItemFromCart(e, productIds, true);
        }
        Order.prototype.HideAsidePopUpPanel();
        selectedProductIds = "";
    }

    HideAsidePopUpPanel(): any {
        var popup = $("#aside-popup-panel");
        $('.aside-popup-panel').hide(700);
        popup.fadeOut(700, function () {
            $("body").css('overflow', 'auto');
            $(this).remove();
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        });
    }

    HideTableTH(gridTD, cssClass): any {
        if (gridTD.hasClass('addToCart')) {
            var indexOfRow = $('#grid tbody tr:eq(0)').find('.' + cssClass + '').index() + 1;
            $('th:nth-child(' + indexOfRow + ')').hide();
        }
    }

    SetDataOnUpdate() {
        if ($("#hdnActionName").val() == "converttoorder" || $("#hdnActionName").val() == "reorder" || $("#hdnActionName").val() == "editorder") {
            Order.prototype.ShowAsidePanelSelected();
        }
    }

    SetDataOnConvertingQuoteToOrder() {
        if ($("#hdnActionName").val() == "converttoorder") {
            Order.prototype.ShowAsidePanelSelected();
        }
    }

    ShowAsidePanelSelected() {
        for (var i = 0; i < ($("#OrderAsidePannel li").length - 2); i++)
            OrderSidePanel.prototype.ContinueOrder();
        $("#z-customers a").click();
        Order.prototype.ShowAndSetPayment("#ddlPaymentTypes option:selected", true);
    }

    RemoveItemFromCart(event, ids, isRemoveAllCartItems): void {
        if (event.stopPropagation) event.stopPropagation();
        let orderId: number = Order.prototype.GetOrderId();
        $.ajax({
            url: "/Order/RemoveItemFromCart",
            data: { "productIds": ids, "omsOrderId": orderId, "isRemoveAllCartItems": isRemoveAllCartItems },
            type: 'GET',
            success: function (data) {
                $(".popover").popover('hide');
                if (isRemoveAllCartItems) {
                    if (orderId > 0) {
                        $("#orderLineItems").html(data.html);
                        Order.prototype.OnTaxExemptPageLoadCheck();
                    }
                    else {
                        $("#divShoppingCart").html("");
                        $("#divShoppingCart").html(data.html);
                    }
                    $("#publishProductDv").html('');
                    $("#publishProductDv").hide();

                    Order.prototype.SetGiftCardMessage();

                    if (typeof $("#hdnShoppingCartCount").val() == 'undefined' || $("#hdnShoppingCartCount").val() == '0') {
                        $("#couponContainer").html("");
                        $("#csr-discount-status").html("");
                        $("#txtcsrDiscount").val("");
                        $("#div-coupons-promotions").hide();
                    }
                    else {
                        Order.prototype.BindCouponHtml(data.coupons);
                    }
                    Order.prototype.ClearShippingEstimates();
                    Order.prototype.HideLoader();
                    $('*[data-autocomplete-url]').each(function () { autocompletewrapper($(this), $(this).data("onselect-function")); });
                }
            }
        });
    }

    //Display discount view if shopping cart having items other wise hide the discount view. 
    ShowHideDiscountView(): void {
        if (typeof $("#hdnShoppingCartCount").val() == 'undefined' || $("#hdnShoppingCartCount").val() == '0') {
            $("#div-coupons-promotions").hide();
        }
        else {
            $("#div-coupons-promotions").show();
        }
    }

    CaptureVoidPayment(url): void {
        Endpoint.prototype.CaptureVoidPayment(url, function (response) {
            if (response.success) {
                $("#refreshGrid").click();
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "success", isFadeOut, fadeOutTime);
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "error", isFadeOut, fadeOutTime);
            }
        });
    }

    DisableCheckouButtonOnReviewPage(): void {
        var priceValue = parseFloat($("#hdnTotalOrderAmount").val());

        if (priceValue >= 0) {
            $("#btnCreateOrder").removeAttr('style');
        } else {
            $("#btnCreateOrder").css("display", "none");
        }
        var validQuantity: boolean = true;
        if ($("#hdnIsAnyProductOutOfStock").val().toLowerCase() == 'true' || $("#hdnIsAnyProductOutOfStock").val() == true) {
            $("#btnCreateOrder").attr("disabled", "disabled");
        }
        else {
            $("#btnCreateOrder").prop("disabled", false);
        }
        $('tr#cart-row-div').each(function () {
            if ($('#quantity_error_msg_' + $(this).find('#CartQuantity').attr('data-cart-externalid') + '').text().trim() != '') {
                validQuantity = false;
            }
        });
        if (validQuantity) {
            $("#btnCreateOrder").prop("disabled", false);
        }
        else {
            $("#btnCreateOrder").attr("disabled", "disabled");
        }
    }

    public ToggleFreeShipping(): void {
        if ($("#cartFreeShipping").val() != undefined) {
            let freeshipping: string = $("#cartFreeShipping").val();
            if (freeshipping.toLowerCase() == "true") {
                $("#message-freeshipping").show();
            }
            else {
                $("#message-freeshipping").hide();
            }
        }
    }

    public SetPurchaseOrderNumber(): void {
        let poNumber: string = $("#PONumber").val();
        if (poNumber != "") {
            $("#PurchaseOrderNumber").val(poNumber);
        }
    }

    public ChangeOrderStatus(): void {
        $("#btnColumnTax").on("click", function () {
            $("#divColumnTax").toggle();
            $("#dynamic-Column-Tax").toggle();
        });
        $("#btnColumnShipping").on("click", function () {
            $("#divColumnShipping").toggle();
            $("#dynamic-shipping-cost").toggle();
        });
        $("#btnCSRDiscountAmount").on("click", function () {
            $("#divCSRDiscountAmount").toggle();
            $("#dynamic-csr-discount-amount").toggle();
        });
        $("#PopUpTaxExemptSubmitOrder #btn-cancel-popup").on("click", function () { $("#chkTaxExempt").attr("checked", false) });
    }

    public DisableSubmitButtonsOnPaypal(): void {
        Endpoint.prototype.GetOrderStateValueById($("#hdnOmsOrderStateId").val(), function (response) {
            if (!response.isEdit || $("#orderLineItems").find(".table>tbody").length <= 0) {
                Order.prototype.FreezManageOrder(true);
            }
            else {
                Order.prototype.FreezManageOrder(false);
            }
        });
    }

    public changeShippingMethod(): void {
        var orderId = $("#OmsOrderId").val();
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Order/GetShippingPanel?omsOrderId=' + orderId + '', 'ShippingPanel');
    }

    public onddlSelectListChange(obj): void {
        if (obj.id == "ddlOrderStatus") {
            Endpoint.prototype.GetOrderStateValueById($(obj).val(), function (response) {
                Order.prototype.DisableSubmitButtonsOnPaypal();
                $("#btnChangeOrderStatus").hide();
                $("#SelectedItemId").val($(obj).val());
                $("#SelectedItemValue").val($(obj).find("option:selected").text());
            });
        }
        else {
            $("#SelectedItemId").val($(obj).val());
            $("#SelectedItemValue").val($(obj).find("option:selected").text());
        }
    }
    public UpdateOrderStatus(data): void {
        if (data != undefined && data != null) {
            $("#ddlListItem").val(data);
            if (data.pageName == "PaymentStatus") {
                $("#labelPaymentStatus").html(data.SelectedItemValue);
                $("#btnChangePaymentStatus").show();
                $("#btnChangePaymentStatus").attr("href", "/order/ManangeOrderStatus?omsOrderId=" + Order.prototype.GetOrderId() + "&orderStatus=" + data.SelectedItemValue + "&pageName=PaymentStatus");
            }
            else {
                $("#Order_State").html(data.SelectedItemValue);
                $('#hdnOrderStatus').val(data.SelectedItemValue);
                $("#btnChangeOrderStatus").show();
                $("#btnChangeOrderStatus").attr("href", "/order/ManangeOrderStatus?omsOrderId=" + Order.prototype.GetOrderId() + "&orderStatus=" + data.SelectedItemValue + "&pageName=OrderStatus");
                $("#hdnOmsOrderStateId").val(data.SelectedItemId);
                Order.prototype.DisableSubmitButtonsOnPaypal();
            }
            $("#divorderStatus").toggle();
        }
    }
    public ChangeOrderStatusBegin(pageName: string): boolean {
        if ($("#pageName").val() == "PaymentStatus") {
            $("#spnOrderStatus").hide();
            if ($("#ddlPaymentStatus").val() == "" || $("#ddlPaymentStatus").val() == undefined) {
                $("#spnPaymentStatus").show();
                return false;
            }
            else {
                return true;
            }
        }
        else {
            $("#spnPaymentStatus").hide();
            if ($("#ddlOrderStatus").val() == "" || $("#ddlOrderStatus").val() == undefined) {
                $("#spnOrderStatus").show();
                return false;
            }
            else {
                return true;
            }
        }
    }
    public FreezManageOrder(flag): void {
        if (flag) {
            $("#orderInformation a").hide();
            $("#customerInformation a").hide();
            $("#divTotal a").hide();
            $("#layout-cart").find("a").hide();
            $("#btnTaxExempt").hide();
            $("#btnApplyCoupon").hide();
            $("#giftcard-apply").hide();
            $("#addProducts").hide();

        } else {
            $("#orderInformation a").show();
            $("#customerInformation a").show();
            $("#divTotal a").show();
            $("#layout-cart").find("a").show();
            $("#btnApplyCoupon").show();
            $("#giftcard-apply").show();
            $("#addProducts").show();
            if ($("#hdnTaxCostEdited").val() != "True") {
                $("#btnTaxExempt").show();
            }
            if ($("#Order_State").html().length > 1500) {
                $("#btnChangeOrderStatus").hide();
            }
            else {
                $("#btnChangeOrderStatus").show();
            }
        }
        $.each($("#orderLineItems").find(".table>tbody"), function (e, v) {
            var id = "#shippingstatus_" + $(v).find("tr>td:last li:eq(3) a").attr("data_cart_externalid");
            if ($(id).text().trim().toLowerCase() == "shipped" || $("#returnLineItems").find(".table>tbody").length > 0) {
                $("#btnShippingType").hide();
            }
        });
    }
    public UpdateOrderText(data): void {
        if (data.pagetype == "ShippingView") {
            $("#shipping-cost").text(data.amount);
        }
        else if (data.pagetype == "CSRDiscountAmountView") {
            $("#csr-discount-amount").text(data.amount);
        }
        else if (data.pagetype == "TaxView") {
            $("#tax-cost").text(data.amount);
        }
    }

    public UpdateTrackingNumber(): void {
        Endpoint.prototype.UpdateTrackingNumber($("#OmsOrderId").val(), $("#TrackingNumber").val(), function (response) {
            if (response.success) {
                $("#labelTrackingNumber").html(response.trackingNumber);
                if (!response.isStatusButtonShow) {
                    $("#btnChangeTrackingNumber").hide();
                    $("#divTrackingNumber").hide();
                }
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PaymentStatusUpdatedSuccessfully"), "success", isFadeOut, fadeOutTime);
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PaymentStatusUpdateFailed"), "error", isFadeOut, fadeOutTime);
            }
        });
    }

    public UpdateColumnShipping(): void {
        Endpoint.prototype.UpdateColumnShipping($("#OmsOrderId").val(), $("#ColumnShipping").val(), function (response) {
            if (response.success) {
                $("#labelTrackingNumber").html(response.trackingNumber);
                if (!response.isStatusButtonShow) {
                    $("#divColumnShipping").hide();
                }
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PaymentStatusUpdatedSuccessfully"), "success", isFadeOut, fadeOutTime);
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PaymentStatusUpdateFailed"), "error", isFadeOut, fadeOutTime);
            }
        });
    }

    public UpdateCSRDiscountAmount(): void {
        Endpoint.prototype.UpdateCSRDiscountAmount($("#OmsOrderId").val(), $("#CSRDiscountAmount").val(), function (response) {
            if (response.success) {
                $("#labelTrackingNumber").html(response.trackingNumber);
                if (!response.isStatusButtonShow) {
                    $("#divCSRDiscountAmount").hide();
                }
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PaymentStatusUpdatedSuccessfully"), "success", isFadeOut, fadeOutTime);
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PaymentStatusUpdateFailed"), "error", isFadeOut, fadeOutTime);
            }
        });
    }

    public UpdateTaxCost(): void {
        Endpoint.prototype.UpdateTaxCost($("#OmsOrderId").val(), $("#TaxCost").val(), function (response) {
            if (response.success) {
                $("#labelTaxCost").html(response.trackingNumber);
                if (!response.isStatusButtonShow) {
                    $("#divColumnTax").hide();
                }
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PaymentStatusUpdatedSuccessfully"), "success", isFadeOut, fadeOutTime);
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PaymentStatusUpdateFailed"), "error", isFadeOut, fadeOutTime);
            }
        });
    }

    public UpdateShippingType(): void {
        Endpoint.prototype.UpdateShippingType($("#OmsOrderId").val(), $("#ShippingType").val(), function (response) {
            if (response.success) {
                $("#labelShippingType").html(response.trackingNumber);
                if (!response.isStatusButtonShow) {
                    $("#divShippingTypeName").hide();
                }
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PaymentStatusUpdatedSuccessfully"), "success", isFadeOut, fadeOutTime);
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PaymentStatusUpdateFailed"), "error", isFadeOut, fadeOutTime);
            }
        });
    }

    public UpdateOrderPaymentStatus(): void {
        Endpoint.prototype.UpdateOrderPaymentStatus($("#OmsOrderId").val(), $("#OrderPaymentStatus").val(), function (response) {
            if (response.success) {
                $("#labelOrderPaymentStatus").html(response.trackingNumber);
                if (!response.isStatusButtonShow) {
                    $("#divPaymentStatus").hide();
                }
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PaymentStatusUpdatedSuccessfully"), "success", isFadeOut, fadeOutTime);
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PaymentStatusUpdateFailed"), "error", isFadeOut, fadeOutTime);
            }
        });
    }

    public ShippingErrorMessage(): boolean {
        if ($("#hdnShippiingErrorMessage").length > 0) {
            let shippingErrorMessage: string = $("#hdnShippiingErrorMessage").val();
            if ($("#hdnHasError").val().toLowerCase() == "true" && shippingErrorMessage != "" && shippingErrorMessage != null && shippingErrorMessage != 'undefined') {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(shippingErrorMessage, 'error', isFadeOut, 10000);
                return false;
            }
            return true;
        }

    }

    public ShowErrorPaymentDialog(message): void {
        $("#ErrorPaymentModal").modal({
            "keyboard": true,
            "show": true
        }).find('p').html(message);
    }

    public ShowPaymentProcessDialog(): void {
        $("#PaymentModal").modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true
        });
    }

    public HidePaymentProcessDialog(): void {
        $(".modal-backdrop").remove();
        $("#PaymentModal").modal('hide');
    }

    public SetGiftCardMessage(): void {
        $("#isGiftcardValid").val("true");
        $("#cart-giftcard-status").hide().html("");
        if ($("#txtgiftcard").val() != undefined && $("#txtgiftcard").val().trim().length > 0) {
            var success = $("#hdnGiftCardApplied").val();
            var message = $("#hdnGiftCardMessage").val();
            if (success) {
                var giftCardMsg: string = "<p class='green- color'>" + "<a href='#' class='z-close' onclick='Order.prototype.RemoveGiftCard();'></a>" + " - " + message + "</p>";
                $("#cart-giftcard-status").show().html(giftCardMsg);
            } else {
                $("#txtgiftcard").val("");
            }
        }
    }

    public EditCartItem(guid: string): void {
        $("#unit-price-" + guid).show();
        $("#quantity-" + guid).hide();
        $("#cartQuantity-" + guid).show();
        $("#shippingstatus_" + guid).hide();
        $("#shipping-status-" + guid).show();

        $("#quantity_error_msg_" + guid).html("");
        $("#quantity_error_msg_" + guid).hide();

        $("#shipSeperately_" + guid).show();
        $("#edit_" + guid).hide();
        $("#update_" + guid).show();
        var cartItemStatus = $("#quantity-" + guid).attr("data-isactive");

        if (cartItemStatus = "False") {
            $("#cartQuantity-" + guid).show();
            $("#cartQuantity-" + guid).val($("#quantity-" + guid).text());
        }

        var orderLineItemStatus = $("#shippingstatus_" + guid).text();
        var statusId = $("#shipping-status-" + guid + " > option").filter(function () {
            return $(this).text() === orderLineItemStatus;
        }).first().attr("value");
        if (statusId == null || status == undefined) {
            statusId = "";
            $("#partialRefund_" + guid).hide();
        }
        $("#shipping-status-" + guid).val(statusId);
        this.SetLineItemLabelAndInputs(orderLineItemStatus.toLowerCase(), guid);

    }

    public ShowHideCustomQuantity(control: any, guid: string): void {
        if ($(control).prop("checked")) {
            $("#custom-quantity-" + guid).show();
        }
        else {
            $("#custom-quantity-" + guid).html("");
            $("#custom-quantity-" + guid).hide();
        }
    }

    public CancleEditCartItem(guid: string): void {
        $("#unitprice-" + guid).show();
        $("#unit-price-" + guid).hide();
        $("#quantity-" + guid).show();
        $("#cartQuantity-" + guid).hide();
        $("#shipSeperately_" + guid).hide();
        $("#trackingnumber_" + guid).show();
        $("#tracking-number-" + guid).hide();
        $("#shippingcost_" + guid).show();
        $("#shippingstatus_" + guid).show();
        $("#shipping-status-" + guid).hide();
        $("#unit_price_error_msg_" + guid).html("");
        $("#quantity_error_msg_" + guid).html("");
        $("#partialRefund_error_msg_" + guid).html("");
        $("#ship-seperately-" + guid).text("");
        $("#custom-quantity-" + guid).html("");
        $("#custom-quantity-" + guid).hide();
        $("#edit_" + guid).show();
        $("#update_" + guid).hide();
        $("#reason_" + guid).hide();
        if ($("[id^=lblpartialRefund_]").text().trim() == '')
            $("#partialRefund").hide();
        $("#partialRefund_" + guid).hide();
        $("#returnShipping").hide();
        $("#returnShipping_" + guid).hide();
        $("#partialRefund_" + guid).val(" ");
    }

    public UpdateCartItem(guid: string): void {
        var _orderLineItemDetail = Order.prototype.BindCartItemModel(guid);
        var quantityError: string = "#quantity_error_msg_" + guid;
        if (this.CheckForValidRmaConfigured($("#shipping-status-" + guid + " :selected").text())) {
            if (this.CheckQuantityValidations(_orderLineItemDetail.Quantity, guid, quantityError)) {
                if (this.CheckMinMaxQuantity(_orderLineItemDetail.Quantity, guid, quantityError)) {
                    if (this.CheckUnitPriceValidations(_orderLineItemDetail.UnitPrice.toString(), guid)) {
                        if (this.CheckPartialAmountValidations(_orderLineItemDetail.PartialRefundAmount, $("#extendedPrice_" + guid).text(), guid)) {
                            if (_orderLineItemDetail.CustomQuantity != null && _orderLineItemDetail.CustomQuantity != "") {
                                if (this.CheckCustomQuantityValidations(_orderLineItemDetail.Quantity, _orderLineItemDetail.CustomQuantity, guid, quantityError)) {
                                    Endpoint.prototype.UpdateCartItem(_orderLineItemDetail, function (response) {
                                        Order.prototype.CancleEditCartItem(guid);
                                        Order.prototype.DisplayUpdatedLineItemData(response, guid);
                                        Order.prototype.BindCouponHtml(response.coupons);
                                        if (response.hasError) {
                                            $(quantityError).html(response.errorMessage);
                                            $(quantityError).show();
                                        }
                                        else {
                                            $(quantityError).html("");
                                            $(quantityError).hide();
                                        }
                                        $(".ReturnList_IsShippingReturn").prop("disabled", "disabled");
                                    });
                                }
                            }
                            else {
                                Endpoint.prototype.UpdateCartItem(_orderLineItemDetail, function (response) {
                                    Order.prototype.CancleEditCartItem(guid);
                                    Order.prototype.DisplayUpdatedLineItemData(response, guid);
                                    Order.prototype.BindCouponHtml(response.coupons);
                                    $(".ReturnList_IsShippingReturn").prop("disabled", "disabled");
                                });
                            }
                        }
                    }
                }
            }
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorForRma"), "error", isFadeOut, fadeOutTime);
        }
    }

    private CheckMinMaxQuantity(quantity: string, guid: string, quantityError: string): boolean {
        var control = $("#cartQuantity-" + guid);
        var minQuantity = $(control).attr("data-cart-minquantity");
        var maxQuantity = $(control).attr("data-cart-maxquantity");
        if (parseFloat(quantity) < parseFloat(minQuantity) || parseFloat(quantity) > parseFloat(maxQuantity)) {
            $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("SelectedQuantityBetween") + minQuantity + ZnodeBase.prototype.getResourceByKeyName("To") + maxQuantity + '.').show();
            return false;
        }
        return true;
    }

    DisplayUpdatedLineItemData(response: any, guid: string) {
        if (response.cartView != null && response.cartView != "") {
            $("#divShoppingCart").html("");
            $("#divShoppingCart").html(response.cartView);
            $("#addProducts").show();
            Order.prototype.HideLoader();
        }
        else {
            $("#unitprice-" + guid).text(response.unitPrice);
            $("#shippingcost_" + guid).text(response.shippingCost);
            $("#extendedPrice_" + guid).text(response.extendedPrice);
            $("#quantity-" + guid).text(response.quantity);
            if (response.trackingNumber != null && response.trackingNumber != "")
                $("#trackingnumber_" + guid).html(Order.prototype.MapTrackingNoToURL(response.trackingNumber));
            if (response.orderLineItemStatus != null && response.orderLineItemStatus != "") {
                $("#shippingstatus_" + guid).text(response.orderLineItemStatus);
                $("#shippingstatus_" + guid).attr("data-orderstate", response.orderLineItemStatusId);
            }

            if (response.partialRefund != null && response.partialRefund != undefined && response.partialRefund != "") {
                $("#lblpartialRefund_" + guid).show();
                if (response != null && response.partialRefund != null) {
                    var refundAmount = parseFloat(response.partialRefund);
                    if (refundAmount.toString() == "NaN") {
                        $("#lblpartialRefund_" + guid).text(response.partialRefund);
                    } else {
                        $("#lblpartialRefund_" + guid).text($("#hdnCurrencySymbol_" + guid).val() + parseFloat(response.partialRefund).toFixed(2));
                    }
                } else {
                    $("#lblpartialRefund_" + guid).text($("#hdnCurrencySymbol_" + guid).val() + "0.00");
                }
                $("#partialRefund").show();
            }
            if (!response.isEditStatus)
                $("#actionLinks_" + guid).remove();
            $("#divTotal").html(response.totalView);
        }

        if (response.returnLineItemView != null && response.returnLineItemView != "") {
            $("#returnLineItems").html(response.returnLineItemView);
            $("#returnLineItems").show();
            Order.prototype.RemovePagingControlsFromGrid();
        }
    }

    public FreezManageOrderForCartCount(): void {
        var cartCount: number = parseInt($("#hdnCartCount").val());
        if (cartCount < 1) {
            Order.prototype.FreezManageOrder(true);
            $("#order-discount").hide();
        }
        $("#addProducts").show();
    }

    public BindCartItemModel(guid: string): Znode.Core.OrderLineItemModel {
        var _orderLineItemDetail: Znode.Core.OrderLineItemModel = {
            OrderId: Order.prototype.GetOrderId(),
            Quantity: ($("#cartQuantity-" + guid).val() == null && $("#cartQuantity-" + guid).val() == "") ? $("#quantity-" + guid).text() : $("#cartQuantity-" + guid).val(),
            UnitPrice: $("#unit-price-" + guid).val(),
            ProductId: parseInt($("#cartQuantity-" + guid).attr("data-cart-productid")),
            TrackingNumber: $("#tracking-number-" + guid).val(),
            Guid: guid,
            CustomQuantity: $("#custom-quantity-" + guid).val(),
            OrderLineItemStatusId: $("#shipping-status-" + guid).val(),
            ReasonForReturnId: $("#ddlReasonList_" + guid).val(),
            OrderLineItemStatus: $("#shipping-status-" + guid + " :selected").text(),
            ReasonForReturn: $("#ddlReasonList_" + guid + " :selected").text(),
            IsShippingReturn: $("#IsShippingReturn_" + guid).prop("checked"),
            PartialRefundAmount: $("#partialRefund_" + guid).val()
        };
        return _orderLineItemDetail;
    }

    private CheckUnitPriceValidations(unitPrice: string, guid: string) {
        var quantityError: string = "#unit_price_error_msg_" + guid;
        var decimalPoint: number = unitPrice.split(".")[1] != null ? unitPrice.split(".")[1].length : 0;
        var decimalValue: number = unitPrice.split(".")[1] != null ? parseInt(unitPrice.split(".")[1]) : 0;
        var priceRoundOff: number = parseInt($("#unit-price-" + guid).attr("data-priceRoundOff"));
        if (unitPrice == "")
            return true;

        if (this.CheckIsNumeric(unitPrice, quantityError)) {
            if (this.CheckDecimalValue(decimalPoint, decimalValue, priceRoundOff, quantityError, true)) {
                if (parseFloat(unitPrice) > 999999) {
                    $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("ErrorPriceRange"));
                    $(quantityError).css("class", "error-msg");
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    private CheckQuantityValidations(quantity: string, guid: string, quantityError: string) {
        var decimalPoint: number = quantity.split(".")[1] != null ? quantity.split(".")[1].length : 0;
        var decimalValue: number = quantity.split(".")[1] != null ? parseInt(quantity.split(".")[1]) : 0;
        var priceRoundOff: number = parseInt($("#cartQuantity-" + guid).attr("data-inventoryRoundOff"));
        if (this.CheckIsNumeric(quantity, quantityError)) {
            if (this.CheckDecimalValue(decimalPoint, decimalValue, priceRoundOff, quantityError)) {
                if (Order.prototype.CheckQuantityGreaterThanZero(parseInt(quantity), quantityError)) {
                    return true;
                }
            }
        }
        return false;
    }

    private CheckCustomQuantityValidations(quantity: string, customQuantity: string, guid: string, quantityError: string) {
        if ($("#custom-quantity-" + guid).css("display") != "none") {
            if (this.CheckQuantityValidations(customQuantity, guid, quantityError)) {
                var custom: number = parseFloat(customQuantity);
                var actual: number = parseFloat(quantity);
                if (custom < 1 || custom > actual) {
                    $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("ErrorInvalidQuantity"));
                    $(quantityError).show();
                    return false;
                }
                return true;
            }
            return false;
        }
        return true;
    }

    private CheckPartialAmountValidations(partialRefundAmount: string, totalAmount: string, guid: string): boolean {
        var refundAmount: number = parseFloat(partialRefundAmount);
        var total: number = Number(totalAmount.replace(/[^0-9\.-]+/g, ""));
        if (refundAmount > 0) {
            var decimalPoint: number = partialRefundAmount.split(".")[1] != null ? partialRefundAmount.split(".")[1].length : 0;
            var decimalValue: number = partialRefundAmount.split(".")[1] != null ? parseFloat(partialRefundAmount.split(".")[1]) : 0;
            var priceRoundOff: number = parseFloat($("#partialRefund_" + guid).attr("data-priceRoundOff"));
            var quantityError: string = "#partialRefund_error_msg_" + guid;
            if (this.CheckIsNumeric(partialRefundAmount, quantityError)) {
                if (this.CheckDecimalValue(decimalPoint, decimalValue, priceRoundOff, quantityError, true)) {
                    if (refundAmount > total) {
                        $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("ErrorInvalidAmount"));
                        $(quantityError).show();
                        return false;
                    }
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    //This method is used to get portal list on aside panel
    GetPortalList(): any {
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Order/GetPortalList', 'divStoreListAsidePanel');
    }

    public SubmitEditOrder(): void {
        var paymentType = $("#PaymentType").val().toLowerCase();
        if ($("#OverDueAmount").val() < 0 && (paymentType == 'credit_card' || paymentType == 'amazon_pay') && $('#hdnPaymentStatus').val().toLowerCase() == "cc_authorized") {
            Order.prototype.ShowErrorPaymentDialog(ZnodeBase.prototype.getResourceByKeyName("ErrorCaptureOrder"));
            return;
        }

        let notes: string = "";
        if ($("#AdditionalNotes").val() != "") {
            notes = $("#AdditionalNotes").val();
        }
        var orderStatus = $('#hdnOrderStatus').val().toLowerCase();
        if (($("#OverDueAmount").val() > 0 && paymentType == 'credit_card' && orderStatus != "cancelled" && orderStatus != "returned")) {
            ZnodeBase.prototype.BrowseAsidePoupPanel('/Order/GetPaymentById?&userId=' + $('#hdnUserId').val() + '&portalId=' + Order.prototype.GetPortalId() + '&paymentType=' + paymentType + '', 'paymentStatusPanel');
        }
        else {
            Order.prototype.UpdateManageOrder(notes);
            Order.prototype.ShowLoader();
        }
    }

    public PlaceOrder(): void {
        if (Order.prototype.ValidatePayment()) {
            Order.prototype.setCVVNumber();
            let selectedPaymentText: string = $("#ddlPaymentTypes option:selected").attr("id").toLowerCase();
            let paymentSettingId: string = $("#ddlPaymentTypes").val();
            $("#paymentStatusPanel").hide();
            ZnodeBase.prototype.RemovePopupOverlay();
            switch (selectedPaymentText) {
                case "credit_card":
                    Order.prototype.SubmitEditOrderPayment();
                    break;
            }
        }
    }

    public setCVVNumber(): any {
        CVVNumber = $("[name='SaveCard-CVV']:visible").val();
    }

    public getCVVNumber(): void {
        return CVVNumber;
    }
    public ValidatePayment(): boolean {
        if ($("#OverDueAmount").val() > 0) {
            if ($("#ddlPaymentTypes option:selected").val() == '') {
                $('#' + $(this).attr('id')).addClass('input-validation-error');
                $('#' + $(this).attr('id')).attr('style', 'border: 1px solid rgb(195, 195, 195)');
                $('span#valPaymentTypes').removeClass('field-validation-valid');
                $('span#valPaymentTypes').addClass('field-validation-error');
                $('span#valPaymentTypes').text(ZnodeBase.prototype.getResourceByKeyName("ErrorSelectPaymentType"));
                return false;
            }
            if ($('span#valPaymentTypes').text().length > 0) {
                $('span#valPaymentTypes').text("");
            }

            if ($("#ddlPaymentTypes option:selected").attr("id").toLowerCase() == 'credit_card') {
                if (!Order.prototype.ValidateCreditCard())
                    return false;
            } else if ($("#ddlPaymentTypes option:selected").attr("id").toLowerCase() == 'purchase_order') {
                let poControl: any = $("#PurchaseOrderNumber");
                let purchaseOrderNumber: string = poControl.val();
                if (purchaseOrderNumber == "" || purchaseOrderNumber == null || purchaseOrderNumber == 'undefined') {
                    $("#cart-ponumber-status").show();
                    poControl.addClass('input-validation-error');
                    return false;
                }
                else {
                    $("#cart-ponumber-status").hide();
                }

            }
        }
        return true;
    }

    public ValidateCreditCard(): boolean {
        let isValid: boolean = true;
        if ($("#ddlPaymentTypes option:selected").attr("id").toLowerCase() == 'credit_card') {
            let isCCValid: boolean = true;
            if ($('#creditCardTab').css('display') == 'block' && $('ul#creditCardTab ').find('li.active').find('a').attr('href') == "#savedCreditCard-panel") {
                var savedCartOptionValue = $("input[name='CCListdetails']:checked").val();
                if (savedCartOptionValue == null || savedCartOptionValue == "") {
                    Order.prototype.ShowErrorPaymentDialog(ZnodeBase.prototype.getResourceByKeyName("ErrorSavedCreditCardOption"));
                    isValid = false;
                    isCCValid = false;
                }
                var cvvNumber: string = $("[name='SaveCard-CVV']:visible").val();
                if (!cvvNumber || cvvNumber.length != 3) {
                    isValid = false;
                    isCCValid = false;
                    $("[name='SaveCard-CVV']:visible").css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                    $("[name='SaveCard-CVV']:visible").parent().find("span.field-validation-error").length <= 0 ?
                        $("[name='SaveCard-CVV']:visible").parent().append("<span class='field-validation-error error-cvv'>" + ZnodeBase.prototype.getResourceByKeyName("CVVErrorMessage") + "</span>") :
                        $("[name='SaveCard-CVV']:visible").parent().find("span.field-validation-error").show();
                }
            }
            else {
                $('input[data-payment="number"],input[data-payment="exp-month"],input[data-payment="exp-year"],input[data-payment="cvc"],input[data-payment="cardholderName"]').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        isCCValid = false;
                        $(this).css({
                            "border": "1px solid red"
                        });
                    } else {
                        $(this).css({
                            "border": "1px solid #c3c3c3"
                        });
                    }
                });

                if (!Order.prototype.Mod10($('input[data-payment="number"]').val())) {
                    isValid = false;
                    isCCValid = false;
                    $('#errornumber').show();
                    $('input[data-payment="number"]').css({
                        "border": "1px solid red"
                    });
                }
                else {
                    $('#errornumber').hide();
                }

                if ($('input[data-payment="exp-month"]').val() > 12 || $('input[data-payment="exp-month"]').val() < 1) {
                    isValid = false;
                    isCCValid = false;
                    $('#errormonth').show();
                    $('input[data-payment="exp-month"]').css({
                        "border": "1px solid red"
                    });
                }
                else {
                    $('#errormonth').hide();
                }

                var currentYear = (new Date).getFullYear();
                var currentMonth = (new Date).getMonth() + 1;

                if ($('input[data-payment="exp-year"]').val() < currentYear) {
                    isValid = false;
                    isCCValid = false;
                    $('#erroryear').show();
                    $('input[data-payment="exp-year"]').css({
                        "border": "1px solid red"
                    });
                }
                else {
                    $('#erroryear').hide();
                }

                if ($('input[data-payment="exp-year"]').val() == currentYear && $('input[data-payment="exp-month"]').val() < currentMonth) {
                    isValid = false;
                    isCCValid = false;
                    $('#errormonth').show();
                    $('input[data-payment="exp-month"]').css({
                        "border": "1px solid red"
                    });
                    $('#erroryear').show();
                    $('input[data-payment="exp-year"]').css({
                        "border": "1px solid red"
                    });
                }

                if ($('input[data-payment="cvc"]').val() == '') {
                    $('#errorcvc').show();
                }
                else {
                    $('#errorcvc').hide();
                }

                if ($('input[data-payment="cardholderName"]').val() == '') {
                    $('#errorcardholderName').show();
                }
                else {
                    $('#errorcardholderName').hide();
                }

                if (isCCValid == true) {
                    var cardNumber = $("#div-CreditCard [data-payment='number']").val();
                    var cardType = Order.prototype.DetectCardType(cardNumber);
                    if ($("#addNewCreditCard-panel").attr("class").indexOf("active") != -1) {
                        if (cardType.toLowerCase() != $("input[name='PaymentProviders']:checked").val().toLowerCase()) {
                            $("#ajaxBusy").dialog('close');
                            var message = "The selected card type is of " + $("input[name='PaymentProviders']:checked").val().toLowerCase() + ".  Please check the credit card number and the card type.";
                            if (message != undefined) {
                                Order.prototype.ShowErrorPaymentDialog(message);
                                isValid = false;
                            }
                            isValid = false;
                        }
                    }
                }
            }
            return isValid;
        }
    }

    //This method is used to select portal from list and show it on textbox
    GetPortalDetail(): void {
        $("#ZnodeUserPortalList #grid").find("tr").on("click", function () {
            if (window.location.href.toLocaleLowerCase().indexOf("createquoteforcustomer") >= 0) {
                let portalName: string = $(this).find("td[class='storecolumn']").text();
                let portalId: string = $(this).find("td")[0].innerHTML;
                Order.prototype.SetPortalOnSelection(portalName, parseInt(portalId));
                $(".tab-details").hide(true);
                var userId = $('#hdnUserId').val() == undefined ? $("#labelCustomerId").text().trim() : $("#hdnUserId").val();
                Endpoint.prototype.CreateNewOrderByPortalIdChangeForUser(parseInt(portalId), parseInt(userId), function (response) {
                    var cartData = $.parseHTML(response);
                    Order.prototype.ShippingSameAsBillingHandler();
                    $("#ShoppingCartDiv").html($(cartData).find('#ShoppingCartDiv').html());
                    $("#shippingMethodDiv").html($(cartData).find('#shippingMethodDiv').html());
                });
                $("#divStoreListAsidePanel").html('');
                ZnodeBase.prototype.RemovePopupOverlay();
            }
            else {
                let portalName: string = $(this).find("td[class='storecolumn']").text();
                let portalId: string = $(this).find("td")[0].innerHTML;
                Order.prototype.SetPortalOnSelection(portalName, parseInt(portalId));
                Order.prototype.GetCatalogListByPortalId(parseInt(portalId), $("#isAllowGlobalLevelUserCreation").val());
                ZnodeBase.prototype.RemovePopupOverlay();
            }
        });
    }

    public SetPortalOnSelection(portalName: string, portalId: number): void {
        $('#txtPortalName').val(portalName);
        $('#PortalId').val(portalId);
        $('#selectedPortalId').val(portalId);
        $('#ddlPortal').val(portalId);
        $("#txtPortalName").attr("data-portalid", portalId);
        $("#errorRequiredStore").text('').text("").removeClass("field-validation-error").hide();
        $("#txtPortalName").removeClass('input-validation-error');
        $('#divStoreListAsidePanel').hide(700);
    }

    public UpdateManageOrder(notes): void {
        Endpoint.prototype.UpdateManageOrder(Order.prototype.GetOrderId(), notes, function (data) {
            if (data.hasError)
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(data.errorMessage, "error", isFadeOut, fadeOutTime);
            else {
                Order.prototype.CheckOrderStateAndPaymentType(data);
                window.location.reload(true);
            }
            //Condition for checking default order status to show print button for packaging slip.
            if (data.SelectedItemValue && data.SelectedItemValue.toString().toLowerCase() == "shipped") {
                $("#packingSlipLink").show();
            }
            else {
                $("#packingSlipLink").hide();
            }
        });
    }

    public CheckOrderStateAndPaymentType(data): void {
        var paymentType = $("#PaymentType").val().toLowerCase();
        var orderState = data.OrderState != null ? data.OrderState.toLowerCase() : "";
        var receiverEmail = data.UserName;
        if (paymentType == 'purchase_order' && orderState == 'shipped') {
            Endpoint.prototype.SendPoEmail(receiverEmail, Order.prototype.GetOrderId(), function (response) { });
        }
    }

    public CheckoutReceipt(action: string, orderId: number, receiptHtml: string, isEmailSend: boolean): boolean {
        var form = $('<form action="CheckoutReceipt" method="post">' +
            '<input type="hidden" name="orderId" value="' + orderId + '" />' +
            '<input type="text" name= "ReceiptHtml" value= "' + receiptHtml + '" />' +
            '<input type="hidden" name= "IsEmailSend" value= ' + isEmailSend + ' />' +
            '</form>');
        form.attr("action", action);
        $('body').append(form);
        $(form).submit();

        return true;
    }

    public OnConfirmSubmitOrder(): boolean {
        if (!Order.prototype.ShowAllowedTerritoriesError())
            return false;

        $('#PopUpConfirmSubmitOrder').modal('show');
    }
    public HideTrackingNumber(): void {
        $("#btnChangeTrackingNumber").hide();
    }
    public HideStatusForUpdate(): void {
        $("#btnChangePaymentStatus").hide();
    }
    public HideOrderStatusForUpdate(): void {
        $("#btnChangeOrderStatus").hide();
    }

    public SetOrderStatusForUpdate(orderStatus): void {
        $("#ddlOrderStatus option").filter(function () {
            return $(this).text() == orderStatus;
        }).attr('selected', true);
    }

    public OnCapturedSubmitOrder(): void {
        $('#PopUpCapturedSubmitOrder').modal('show');
    }
    public HideCSRDiscountAmountForUpdate(): void {
        $('#CSRDiscountAmount').hide();
    }
    public HideTaxCostForUpdate(): void {
        $('#TaxCostAmount').hide();
    }
    public HideShippingCostForUpdate(): void {
        $('#ShippingCostAmount').hide();
    }
    public HideShippingAccountNumber(): void {
        $("#btnChangeShippingAccountNumber").hide();
    }
    public HideShippingMethod(): void {
        $("#btnChangeShippingMethod").hide();
    }
    public OnOrderCancelEdit(id: string, value: string): void {
        if ($("#hdnShippingTrackingUrl").val() == "") {
            $("#" + id).html(value);
        } else {
            $("#" + id).html("<a target=_blank href=" + $("#hdnShippingTrackingUrl").val() + value + ">" + value + "</a>");
        }
        $("#btnChangeTrackingNumber").show();
    }
    public OnShippingAccountNumberCancelEdit(id: string, value: string): void {
        $("#" + id).html(value);
        $("#btnChangeShippingAccountNumber").show();
    }
    public OnShippingMethodCancelEdit(id: string, value: string): void {
        $("#" + id).html(value);
        $("#btnChangeShippingMethod").show();
    }
    public MapTrackingNoToURL(value: string): string {
        if ($("#hdnShippingTrackingUrl").val() == "") {
            return value;
        } else {
            return "<a target=_blank href=" + $("#hdnShippingTrackingUrl").val() + value + ">" + value + "</a>";
        }

    }
    public OnTotalTableCancelEdit(pageName: string, value: string): void {
        if (pageName == "CSRDiscountAmountView") {
            $("#dynamic-csr-discount-amount").html("$" + parseFloat(value).toFixed(3));
            $("#CSRDiscountAmount").show();
        } else if (pageName == "TaxView") {
            $("#dynamic-Column-Tax").html(value);
            $("#TaxCostAmount").show();
        }
        else {
            $("#dynamic-shipping-cost").html("$" + parseFloat(value).toFixed(3));
            $("#ShippingCostAmount").show();
        }
    }
    public ConfirmCapturedOrder(omsOrderId: string, token: string, status: string): void {
        window.location.href = "/Order/CapturePayment?omsOrderId=" + omsOrderId + "&paymentTransactionToken=" + token + "&PaymentStatus=" + status;
    }
    public OnOrderStatusCancelEdit(id: string, value: string, pageName: string): void {
        $("#" + id).html(value);
        if (pageName == "PaymentStatus") {
            $("#btnChangePaymentStatus").show();
        } else {
            $("#btnChangeOrderStatus").show();
        }
    }
    SubmitEditOrderPayment(): any {
        if (!Order.prototype.ShowAllowedTerritoriesError())
            return false;
        var Total = $("#hdnTotalAmt").val();
        if (Order.prototype.IsOrderTotalGreaterThanZero(Total)) {
            Order.prototype.GetEncryptedAmount(Total);
            var payment = Order.prototype.GetPaymentModel();
            $("#div-CreditCard").hide();
            submitCard(payment, function (response) {
                if (response.GatewayResponse == undefined) {
                    if (response.indexOf("Unauthorized") > 0) {
                        Order.prototype.ClearPaymentAndDisplayMessage('We were unable to process your credit card payment. <br /><br />Reason:<br />' + response + '<br /><br />If the problem persists, contact us to complete your order.');
                    }
                } else {
                    var isSuccess = response.GatewayResponse.IsSuccess;
                    if (isSuccess) {
                        var submitPaymentViewModel = {
                            OmsOrderId: Order.prototype.GetOrderId,
                            PaymentSettingId: $('#PaymentSettingId').val(),
                            CustomerShippingAddressId: response.GatewayResponse.CustomerShippingAddressId,
                            PaymentApplicationSettingId: $('#hdnPaymentApplicationSettingId').val(),
                            CustomerProfileId: response.GatewayResponse.CustomerProfileId,
                            CustomerPaymentId: response.GatewayResponse.CustomerPaymentProfileId,
                            CustomerGuid: response.GatewayResponse.CustomerGUID,
                            PaymentToken: $("input[name='CCdetails']:checked").val(),
                            AdditionalInfo: $("#AdditionalNotes").val(),
                            CreditCardNumber: $("#hdnCreditCardNumber").val(),
                        };
                        submitPaymentViewModel["CardSecurityCode"] = payment["CardSecurityCode"];

                        Endpoint.prototype.SubmitEditOrderpayment(submitPaymentViewModel, function (response) {
                            $("#ajaxBusy").dialog('close');
                            if (response === undefined || response.Data === undefined || response.Data.OrderId === undefined || response.Data.OrderId <= 0 || response.Data.HasError == true) {
                                var errorMessage = response.Data == undefined || response.Data.ErrorMessage == undefined || response.Data.ErrorMessage == "" ? ZnodeBase.prototype.getResourceByKeyName("ErrorOrderPlace") : response.Data.ErrorMessage;
                                Order.prototype.ClearPaymentAndDisplayMessage(errorMessage);
                                return false;
                            }
                            else {
                                location.reload(true);
                            }
                        });
                    }
                    else {
                        var errorMessage = response.GatewayResponse.ResponseText;
                        if (errorMessage == undefined) {
                            errorMessage = response.GatewayResponse.GatewayResponseData;
                        }

                        if (errorMessage != undefined && errorMessage.toLowerCase().indexOf("missing card data") >= 0) {
                            Order.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorCardDataMissing"));
                        } else if (errorMessage != undefined && errorMessage.indexOf("Message=") >= 0) {
                            Order.prototype.ClearPaymentAndDisplayMessage(errorMessage.substr(errorMessage.indexOf("=") + 1));
                        } else if (errorMessage.indexOf('customer') > 0) {
                            Order.prototype.ClearPaymentAndDisplayMessage(errorMessage);
                        } else {
                            Order.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorOrderPlace"));
                        }
                    }
                }
            });
        }
    }

    public GetPaymentModel(): Object {
        var Total = $("#hdnTotalAmt").val();
        if ($("#ajaxProcessPaymentError").html() == undefined) {
        } else {
            $("#ajaxProcessPaymentError").html(ZnodeBase.prototype.getResourceByKeyName("ProcessingPayment"));
        }

        var cardNumber = $("#div-CreditCard [data-payment='number']").val();
        if (cardNumber != "") {
            $("#hdnCreditCardNumber").val(cardNumber.slice(-4));
        } else {
            var card = $("input[name='CCListdetails']:checked").next().html();
            if (card != "") {
                $("#hdnCreditCardNumber").val(card.split(" ")[3]);
            }
        }

        var IsAnonymousUser = $("#hdnAnonymousUser").val();

        var guid = $('#GUID').val();

        var discount = $('#dynamic-discount-amount').text().substring(1);
        var ShippingCost = $('#ShippingCost').val();

        var SubTotal = $('#dynamic-subtotal-amount').text().substring(1);

        var cardType = Order.prototype.DetectCardType(cardNumber);

        Order.prototype.ShowPaymentProcessDialog();

        var paymentSettingId = $('#PaymentSettingId').val();
        var PaymentApplicationSettingId = $('#hdnPaymentApplicationSettingId').val();

        var CustomerPaymentProfileId = $('#CustomerPaymentProfileId').val();
        var CustomerProfileId = $('#CustomerProfileId').val();
        var CardDataToken = $('#CardDataToken').val();
        var billingEmail = $('#address_email').val();
        var stateCode = $("#hdnstateCode").val();
        if (billingEmail == undefined && billingEmail == null) {
            billingEmail = $('#hdnGuestUserName').val();
        }
        var gatewayName = $("#hdnGatwayName").val();
        var paymentAppGatewayName = $("#hdnGatwayName").val().toLowerCase();

        if (gatewayName.toLowerCase() == 'authorize.net') {
            paymentAppGatewayName = 'authorizenet';
        }
        else if (gatewayName.toLowerCase() == 'chase paymentech') {
            paymentAppGatewayName = 'paymentech';
        }
        else if (gatewayName.toLowerCase() == 'payflow') {
            if ($("#hdnEncryptedTotalAmount").val() != undefined && $("#hdnEncryptedTotalAmount").val() != null) {
                Total = $("#hdnEncryptedTotalAmount").val();
            }
        }
        var paymentModel = {
            "GUID": guid,
            "GatewayType": paymentAppGatewayName,
            "BillingCity": $('#UserAddressDataViewModel_BillingAddress_CityName').val(),
            "BillingCountryCode": $('#UserAddressDataViewModel_BillingAddress_CountryName').val(),
            "BillingFirstName": $('#UserAddressDataViewModel_BillingAddress_FirstName').val(),
            "BillingLastName": $('#UserAddressDataViewModel_BillingAddress_LastName').val(),
            "BillingPhoneNumber": $('#UserAddressDataViewModel_BillingAddress_PhoneNumber').val(),
            "BillingPostalCode": $('#UserAddressDataViewModel_BillingAddress_PostalCode').val(),
            "BillingStateCode": stateCode,
            "BillingStreetAddress1": $('#UserAddressDataViewModel_BillingAddress_Address1').val(),
            "BillingStreetAddress2": $('#UserAddressDataViewModel_BillingAddress_Address2').val(),
            "BillingEmailId": billingEmail,
            "ShippingCost": ShippingCost,
            "ShippingCity": $('#UserAddressDataViewModel_ShippingAddress_CityName').val(),
            "ShippingCountryCode": $('#UserAddressDataViewModel_ShippingAddress_CountryName').val(),
            "ShippingFirstName": $('#UserAddressDataViewModel_ShippingAddress_FirstName').val(),
            "ShippingLastName": $('#UserAddressDataViewModel_ShippingAddress_LastName').val(),
            "ShippingPhoneNumber": $('#UserAddressDataViewModel_ShippingAddress_PhoneNumber').val(),
            "ShippingPostalCode": $('#UserAddressDataViewModel_ShippingAddress_PostalCode').val(),
            "ShippingStateCode": $('#UserAddressDataViewModel_ShippingAddress_StateName').val(),
            "ShippingStreetAddress1": $('#UserAddressDataViewModel_ShippingAddress_Address1').val(),
            "ShippingStreetAddress2": $('#UserAddressDataViewModel_ShippingAddress_Address2').val(),
            "SubTotal": SubTotal,
            "Total": Total,
            "Discount": discount,
            "CardNumber": cardNumber,
            "CreditCardNumber": $("#hdnCreditCardNumber").val(),
            "CardExpirationMonth": $("#div-CreditCard [data-payment='exp-month']").val(),
            "CardExpirationYear": $("#div-CreditCard [data-payment='exp-year']").val(),
            "GatewayCurrencyCode": $('#hdnCurrencySuffix').val(),
            "CustomerPaymentProfileId": CustomerPaymentProfileId,
            "CustomerProfileId": CustomerProfileId,
            "CardDataToken": CardDataToken,
            "CardType": cardType,
            "PaymentSettingId": paymentSettingId,
            "PaymentApplicationSettingId": PaymentApplicationSettingId,
            "IsAnonymousUser": IsAnonymousUser,
            "IsSaveCreditCard": $("#SaveCreditCard").is(':checked'),
            "CardHolderName": $("#div-CreditCard [data-payment='cardholderName']").val(),
            "CustomerGUID": $("#hdnCustomerGUID").val(),
            "PaymentToken": ($("#addNewCreditCard-panel").attr("class").indexOf("active") != -1) ? "" : $("input[name='CCListdetails']:checked").val()
        };
        paymentModel["CardSecurityCode"] = paymentModel["PaymentToken"] ? Order.prototype.getCVVNumber() : $("#div-CreditCard [data-payment='cvc']").val();
        return paymentModel
    }

    public GetReasonsForReturn(guid: string): any {
        $("#quantity_error_msg_" + guid).text("");
        $("#quantity_error_msg_" + guid).hide();
        var shippingStatusListControl = $("#shipping-status-" + guid);
        if (shippingStatusListControl != null) {
            var orderLineItemStatus: string = $("#shipping-status-" + guid + " :selected").text().toLowerCase();
            this.SetLineItemLabelAndInputs(orderLineItemStatus, guid);
        }
        var partialRefundProductcount = 0;
        $("#layout-cart span[id ^= 'lblpartialRefund']").each(function (e) {
            if ($(this).html() != "" || $(this).parent("td").find("input[id ^= 'partialRefund']").is(":visible")) { partialRefundProductcount = partialRefundProductcount + 1; }
        });
        if (partialRefundProductcount > 0) { $("#partialRefund").show(); }
        else { $("#partialRefund").hide(); $("input[id ^= 'partialRefund']").hide(); }
    }

    private SetLineItemLabelAndInputs(orderLineItemStatus: string, guid: string): any {
        var shippingcost = Number($("#shippingcost_" + guid).text().replace(/[^0-9\.-]+/g, ""));
        if (orderLineItemStatus == "returned") {
            if (Order.prototype.ValidateLineItemForReturn(guid)) {
                if ($("#DownloadableProductKey_" + guid).val() == "True") {
                    $("#custom-quantity-" + guid).hide();
                }
                else {
                    $("#custom-quantity-" + guid).show();
                    $("#custom-quantity-" + guid).val('');
                }
                $("#quantity-" + guid).show();
                $("#cartQuantity-" + guid).hide();
                $("#unitprice-" + guid).show();
                $("#unit-price-" + guid).hide();

                if ($("[id^=lblpartialRefund_]").text().trim() == '')
                    $("#partialRefund").hide();
                else {
                    $("#partialRefund").show();
                    $("#lblpartialRefund_" + guid).show();
                }
                $("#partialRefund_" + guid).hide();

                if ($("#ShipSeperately_" + guid).val() == "True") {
                    $("#trackingnumber_" + guid).show();
                    $("#tracking-number-" + guid).hide();
                    if (shippingcost > 0) {
                        $("#returnShipping").show();
                        $("#returnShipping_" + guid).show();
                    }
                }

                $("#reason_" + guid).html("");
                Endpoint.prototype.GetReasonsForReturn(function (response) {
                    var data: any = response.data;
                    var selectControl = "<select id='ddlReasonList_" + guid + "' name= 'Reason' ></select>";
                    $("#reason_" + guid).html(selectControl);
                    $("#reason_" + guid).show();
                    $("#reasonForReturn").show();
                    $.each(data, function (i) {
                        var optionhtml = '<option value="' +
                            data[i].Value + '">' + data[i].Text + '</option>';
                        $("#ddlReasonList_" + guid).append(optionhtml);
                    });
                });
            }
        }
        else if (orderLineItemStatus == "partial refund") {
            $("#reason_" + guid).hide();
            $("#reasonForReturn").hide();
            $("#returnShipping").hide();
            $("#returnShipping_" + guid).hide();
            $("#quantity-" + guid).show();
            $("#cartQuantity-" + guid).hide();
            $("#custom-quantity-" + guid).hide();
            $("#unitprice-" + guid).show();
            $("#unit-price-" + guid).hide();
            if ($("#ShipSeperately_" + guid).val() == "True") {
                $("#tracking-number-" + guid).hide();
                $("#custom-quantity-" + guid).hide();
                $("#custom-quantity-" + guid).val('');
                $("#cartQuantity-" + guid).hide();
                $("#trackingnumber_" + guid).show();
            }

            $("#partialRefund").show();
            $("#partialRefund_" + guid).show();
            if ($("#lblpartialRefund_" + guid).text().trim() != '')
                $("#lblpartialRefund_" + guid).show();
        }
        else if (orderLineItemStatus == "shipped") {
            $("#reason_" + guid).hide();
            $("#reasonForReturn").hide();
            $("#returnShipping").hide();
            $("#returnShipping_" + guid).hide();
            $("#custom-quantity-" + guid).hide();
            $("#unitprice-" + guid).show();
            $("#unit-price-" + guid).hide();
            $("#unit-price-" + guid).val('');
            $("#quantity-" + guid).show();
            $("#cartQuantity-" + guid).hide();
            $("#tracking-number-" + guid).hide();
            if ($("[id^=lblpartialRefund_]").text().trim() == '')
                $("#partialRefund_" + guid).hide();
            else {
                $("#partialRefund").show();
                $("#lblpartialRefund_" + guid).show();
            }

            if ($("#ShipSeperately_" + guid).length > 0 && $("#ShipSeperately_" + guid).val() == "True") {
                $("#trackingnumber_" + guid).hide();
                $("#tracking-number-" + guid).show();
                $("#custom-quantity-" + guid).show();
                $("#custom-quantity-" + guid).val('');
                $("#tracking-number-" + guid).val($("#trackingnumber_" + guid).html());
            }

            $("#partialRefund_" + guid).hide();
        }
        else if (orderLineItemStatus == "submitted" || orderLineItemStatus == "pending approval"
            || orderLineItemStatus == "open") {
            $("#reason_" + guid).hide();
            $("#reasonForReturn").hide();
            $("#returnShipping").hide();
            $("#returnShipping_" + guid).hide();
            $("#custom-quantity-" + guid).hide();
            $("#unitprice-" + guid).show();
            $("#unit-price-" + guid).hide();
            $("#unit-price-" + guid).val('');
            if (orderLineItemStatus == "pending approval") {
                $("#quantity-" + guid).show();
                $("#cartQuantity-" + guid).hide();
            } else {
                $("#quantity-" + guid).hide();
                $("#cartQuantity-" + guid).show();
            }
            if (orderLineItemStatus == "submitted") {
                if ($("#DownloadableProductKey_" + guid).val() == "True") {
                    $("#quantity-" + guid).show();
                    $("#cartQuantity-" + guid).hide();
                }
                else {
                    $("#cartQuantity-" + guid).show();
                }
            }
            $("#tracking-number-" + guid).hide();
            $("#partialRefund_" + guid).hide();

            if ($("#ShipSeperately_" + guid).length > 0 && $("#ShipSeperately_" + guid).val() == "True") {
                if ($("#trackingnumber_" + guid).length > 0) $("#trackingnumber_" + guid).show();
            }
        }
        else {
            $("#reason_" + guid).hide();
            $("#reasonForReturn").hide();
            $("#returnShipping").hide();
            $("#returnShipping_" + guid).hide();
            $("#custom-quantity-" + guid).hide();
            $("#unitprice-" + guid).show();
            $("#unit-price-" + guid).hide();
            $("#unit-price-" + guid).val('');
            $("#quantity-" + guid).show();
            $("#cartQuantity-" + guid).hide();
            $("#tracking-number-" + guid).hide();
            if ($("#ShipSeperately_" + guid).length > 0 && $("#ShipSeperately_" + guid).val() == "True") {
                if ($("#trackingnumber_" + guid).length > 0)
                    $("#trackingnumber_" + guid).show();
            }
            $("#lblpartialRefund_" + guid).show();
            $("#partialRefund_" + guid).hide();
        }
    }

    private CheckForValidRmaConfigured(shippingStatus: string): boolean {
        if (shippingStatus.toLowerCase() == "returned") { return ($("#hdnIsValidForRma").val() == "True") } else { return true };
    }

    private ShowCustomQuantity(guid: string): void {
        $("#unitprice-" + guid).show();
        if ($("#unit-price-" + guid).val() != null && $("#unit-price-" + guid).val() != "") {
            var unitPriceLength = $("#unitprice-" + guid).text().split(',').length;
            var unitPrice: number = unitPriceLength > 1 ? Order.prototype.PriceAsFloat($("#unitprice-" + guid).text()) : Order.prototype.PriceAsFloat($("#unitprice-" + guid).text());
            $("#unit-price-" + guid).val(unitPrice);
        }
        $("#unit_price_error_msg_" + guid).html("");
        $("#unit-price-" + guid).hide();
        $("#quantity-" + guid).show();
        var cartQuantity: number = parseFloat($("#quantity-" + guid).text());
        $("#cartQuantity-" + guid).hide();
        $("#cartQuantity-" + guid).val(cartQuantity);
        var cartCount: number = parseInt($("#hdnCartCount").val());
        if (cartQuantity > 1) {
            $("#custom-quantity-" + guid).show();
            $("#custom-quantity-" + guid).val('');
        }
    }

    public CheckForReturnItem(guid: string, quantityError: string): boolean {
        if ($("#shipping-status-" + guid + " :selected").text().toLowerCase() == "returned") {
            var cartQuantity: number = parseFloat($("#quantity-" + guid).text());
            var cartCount: number = parseInt($("#hdnCartCount").val());
            if (cartCount < 2) {
                var customQuantity: number = parseFloat($("#custom-quantity-" + guid).val());
                if (cartQuantity > 1 && customQuantity < 1) {
                    $(quantityError).text("Please specify custom Quantity to return item.");
                    return false;
                }
            }
        }
        return true;
    }

    public SendReturnedOrderMail(): void {
        let orderId: number = $("#OmsOrderId").val();
        if (orderId > 0) {
            Endpoint.prototype.SendReturnedOrderEmail(orderId, function (response) {
                window.location.reload(true);
            });
        }
        else
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorOrderId"), "error", isFadeOut, fadeOutTime);
    }

    public PriceAsFloat(price: string): number {
        return parseFloat(price.replace(/[^0-9.,]/g, "").replace(/[^\d\.]/g, '.'));
    }

    public ValidateLineItemForReturn(guid: string): boolean {
        var paymentType = $("#PaymentType").val().toLowerCase();
        if ((paymentType == 'credit_card' || paymentType == 'amazon_pay') && $('#hdnPaymentStatus').val().toLowerCase() == "cc_authorized") {
            Order.prototype.ShowErrorPaymentDialog(ZnodeBase.prototype.getResourceByKeyName("ErrorCaptureOnReturnOrder"));
            $("#shipping-status-" + guid).val("");
            return false;
        }
        return true;
    }

    public RemoveAllCartSuccess(): void {
        Order.prototype.SetGiftCardMessage();
        $("#couponContainer").html("");
        $("#csr-discount-status").html("");
        $("#txtcsrDiscount").val("");
        $("#div-coupons-promotions").hide();
        Order.prototype.ClearShippingEstimates();
        Order.prototype.ClickSelectedTab("z-shopping-cart");
    }

    public ShowAllowedTerritoriesError(): boolean {
        var orderId: number = Order.prototype.GetOrderId();
        if ($("#dynamic-allowesterritories").length > 0) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("AllowedTerritories"), "error", isFadeOut, fadeOutTime);
            return false;
        }
        if ($("#hndShippingclassName").val() != undefined && $("#hndShippingclassName").val() == Constant.ZnodeCustomerShipping && orderId > 0) {
            if ($("#labelShippingAccountNumber").text().trim() == undefined || $("#labelShippingAccountNumber").text().trim() == "") {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorEnterAccountNumber"), 'error', isFadeOut, fadeOutTime);
                return false;
            }
            if ($("#labelShippingMethod").text().trim() == undefined || $("#labelShippingMethod").text().trim() == "") {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorEnterShippingMethod"), 'error', isFadeOut, fadeOutTime);
                return false;
            }
        }
        return true;
    }

    public UpdateOrderTextDetails(): boolean {
        var pageType: string = $("#pageName").val();
        var amount: number = $("#OrderTextValue").val();
        if (!isNaN(amount)) {
            switch (pageType) {
                case "ShippingView":
                    if (amount < 0) {
                        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorShippingtAmountNegative"), "error", isFadeOut, fadeOutTime);
                        return false;
                    }
                    return true;
                case "CSRDiscountAmountView":
                    if (amount < 0) {
                        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorCSRDisountNegative"), "error", isFadeOut, fadeOutTime);
                        return false;
                    }
                    return true;
                default:
                    return true;
            }
        }
        else
            return false;
    }


    public UpdateReturnShippingHistory(omsOrderLineItemsId: number, checkbox): void {
        Endpoint.prototype.UpdateReturnShippingHistory(omsOrderLineItemsId, $("#OmsOrderId").val(), $(checkbox).prop("checked"), function (response) {

        });
    }

    public GetPortalId(): number {
        return $("#txtPortalName").attr("data-portalid") != undefined ? parseInt($("#txtPortalName").attr("data-portalid")) : parseInt($("#PortalId").val());
    }

    PrintOnPackageSlip(OmsOrderLineItemsId: string): any {
        Endpoint.prototype.PrintOnPackageSlip(Order.prototype.GetOrderId(), OmsOrderLineItemsId, function (response) {

            var originalContents = document.body.innerHTML;

            document.body.innerHTML = response;

            window.print();

            document.body.innerHTML = originalContents;
        });
    }
    //CheckboxClearOnload(): any {
    //    if (CheckBoxCollection.length <= 0)
    //        $("#ZnodeOrder #grid").find("input:checkbox").attr("checked", false);
    //}

    public AddCustomShipping(code: string): any {

        $(".dev-custom-shipping").hide();
        $("#div_" + code + "").show();

    }

    public CancelCustomShipping(code: string): void {
        $("#div_" + code + "").hide();
        $("#CustomShipping_" + code + "").val("");
        Order.prototype.ShowEditCustomShipping(code);
    }

    public AddCustomShippingAmount(customShippingCost: any, estimateShippingCost: any, isRemove: boolean = false): void {

        if ((customShippingCost != "" && customShippingCost != null && typeof customShippingCost != "undefined") || isRemove) {
            Endpoint.prototype.AddCustomShippingAmount(customShippingCost, estimateShippingCost, function (response) {

            });
        }
    }
}
