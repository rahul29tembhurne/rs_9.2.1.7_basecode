﻿class User extends ZnodeBase {
    _endPoint: Endpoint;

    constructor() {
        super();
    }

    Init() {
        User.prototype.GetUserPermissionList();
        $("#rolelist").on("change", function () {
            $("#hdnRoleName").val($("#rolelist option:selected").text());
        });
        $("#rolelist").change();

        User.prototype.SetRoleName();
        User.prototype.SetIsSelectAllPortal();
        User.prototype.OnPermissionSelection();
        User.prototype.SubmitOnEnterKey();
    }

    SetRoleName() {
        $("#ddlUserType").on("change", function () {
            $("#hdnRoleName").val($("#ddlUserType option:selected").text());
        });
    }

    DeleteUsers(control): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            Endpoint.prototype.DeleteUsers(accountIds, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    DeleteCustomer(control): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            Endpoint.prototype.DeleteCustomer(accountIds, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    EnableUserAccount(): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.EnableDisableUserAccount(accountIds, true, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/User/UsersList";
            });
            ZnodeBase.prototype.HideLoader();
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    DisableUserAccount(): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.EnableDisableUserAccount(accountIds, false, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/User/UsersList";
            });
            ZnodeBase.prototype.HideLoader();
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    EnableCustomerAccount(): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.EnableDisableCustomerAccount(accountIds, true, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/Customer/CustomersList";
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    DisableCustomerAccount(): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();;
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.EnableDisableCustomerAccount(accountIds, false, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/Customer/CustomersList";
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    UserResetPassword(): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.UserResetPassword(accountIds, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/User/UsersList";
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    CustomerResetPassword(): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.CustomerResetPassword(accountIds, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/Customer/CustomersList";
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    ResetPasswordCustomer() {
        var accountId = $("#AccountId").val();
        window.location.href = window.location.protocol + "//" + window.location.host + "/customer/singleresetpassword?accountId=" + accountId;
    }

    ResetPasswordUsers() {
        var userId = $("#divAddCustomerAsidePanel #UserId").val();
        if (userId == undefined)
            userId = $("#UserId").val();
        ZnodeBase.prototype.ShowLoader();
        Endpoint.prototype.SingleResetPassword(userId, function (res) {
            ZnodeBase.prototype.HideLoader();
            var errorType = 'error';
            if (res.status) {
                errorType = 'success';
            }
            ZnodeBase.prototype.CancelUpload("divAddCustomerAsidePanel");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, errorType, isFadeOut, fadeOutTime);
        });
    }

    SetIsSelectAllPortal() {
        if ($("#areaList ul li input:checkbox[value='multiselect-all']").prop('checked')) {
            $("#IsSelectAllPortal").val("true");
        }
        else {
            $("#IsSelectAllPortal").val("false");
        }
        return true;
    }

    ShowHidePortals() {
        if ($("#IsSelectAllPortal").val() == "True") {
            $("#divPortalIds").hide();
        }
        else {
            $("#divPortalIds").show();
        }
        $("#chkIsSelectAllPortal").on("change", function () {
            if ($(this).prop('checked')) {
                $("#IsSelectAllPortal").val("true");
                $("#divPortalIds").hide();
            }
            else {
                $("#IsSelectAllPortal").val("false");
                $("#divPortalIds").show();
            }
        });
    }

    ClickSelectAllPortal() {
        $(".chkPortal").click(function () {
            if ($(this).prop('checked')) {
                if ($('.chkPortal:checked').length == ($('.chkPortal').length / 2)) {
                    $("#chkIsSelectAllPortal").prop('checked', 'checked');
                    $("#IsSelectAllPortal").val("true");
                    $("#divPortalIds").hide();
                }
            }
        });
    }

    SubmitOnEnterKey(): boolean {
        $("#btnPassword").keypress(function (e) {
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                $('button[type=submit]').click();
                return true;
            }
        });
        return true;
    }

    OnAccountSelection(): any {
        var selectedAccount = $("#AccountId").val();
        if (selectedAccount == 0 && selectedAccount == "") {
            $('#ddlDepartment').children('option:not(:first)').remove();
            $('#ddlAccountType').children('option:not(:first)').remove();
            $('#divDepartmentId').hide();
            $('#divUserTypeId').hide();
            $('#divRole').hide();
            $('#approvalNamesDiv').hide();
            $('#maxBudgetDiv').hide();
            $('#ddlPortals').show();
            $('#customer_general_information').show();
            return false;
        }
        Endpoint.prototype.GetAccountDepartmentList(selectedAccount, function (response) {
            $('#ddlDepartment').children('option:not(:first)').remove();
            for (var i = 0; i < response.length; i++) {
                var opt = new Option(response[i].Text, response[i].Value);
                $('#ddlDepartment').append(opt);
            }
            $('#divDepartmentId').show();
        });
        Endpoint.prototype.GetRoleList(function (response) {
            $("#hdnRoleName").val("");
            $('#ddlUserType').children('option').remove();
            for (var i = 0; i < response.length; i++) {
                if (response[i].Value == $("#hdnRoleName").val())
                    var opt = new Option(response[i].Text, response[i].Value, false, true);
                else
                    var opt = new Option(response[i].Text, response[i].Value);
                $('#ddlUserType').append(opt);
                $('#divUserTypeId').show();
            }
        });
        $('#divRole').hide();
        $('#approvalNamesDiv').hide();
        $('#maxBudgetDiv').hide();
        $('#ddlPortals').hide();
        $('#customer_general_information').hide();
        $('#errorSelectAccountId').html("");
        User.prototype.OnUserTypeSelection();
    }

    OnUserTypeSelection(): any {
        var selectedRole = $("#ddlUserType option:selected").text();
        if (selectedRole == null && selectedRole == "") {
            $('#ddlRole').children('option:not(:first)').remove();
            $('#divRole').hide();
            return false;
        }

        if (selectedRole == "User") {
            $('#divRole').show();
            Endpoint.prototype.GetPermissionList($("#AccountId").val(), $("#AccountPermissionAccessId").val(), function (response) {
                $('#permission_options').html("");
                $('#permission_options').html(response);
                $("#ddlPermission").attr("onchange", "User.prototype.OnPermissionSelection();");
            });
            $("#ddlPermission").change();
        }
        else {
            $('#divRole').hide();
            $('#approvalNamesDiv').hide();
            $('#maxBudgetDiv').hide();
            $("#BudgetAmount").val("");
        }
    }

    OnPermissionSelection(): any {     
        var permission = $("#ddlPermission option:selected").attr('data-permissioncode');
        var $sel = $("#divRole");
        var value = $sel.val();
        var text = $("option:selected", $sel).text();
        $('#PermissionCode').val(permission);
        $('#PermissionsName').val(text);
        if (permission != undefined && permission == 'ARA') {
            User.prototype.ShowApprovalList();
            $('#maxBudgetDiv').hide();
        }
        else if (permission != undefined && permission == 'SRA')
        {
            User.prototype.ShowApprovalList();
            $('#maxBudgetDiv').show();
        }
        else {
            $('#approvalNamesDiv').hide();
            $('#maxBudgetDiv').hide();
            $("#BudgetAmount").val("");
        }
    }

    ShowApprovalList(): any {
        var accountId = $("#AccountId").val();
        var userId = parseInt($("#UserId").val(), 10);
        Endpoint.prototype.GetApproverList(accountId, userId, function (response) {
            var approvalUserId = $("#ApprovalUserId").val();
            $("#ddlApproverList").html("");
            $('#ddlApproverList').find('option').remove().end();
            $('#ddlApproverList').children('option:not(:first)').remove();
            for (var i = 0; i < response.length; i++) {
                if (response[i].Value == approvalUserId)
                    var opt = new Option(response[i].Text, response[i].Value, false, true);
                else
                    var opt = new Option(response[i].Text, response[i].Value);

                $('#ddlApproverList').append(opt);
            }
            $('#approvalNamesDiv').show();
        });
    }

    GetSelectedAccount(): any {
        $("#grid").find("tr").click(function () {
            var accountName: string = $(this).find("td[class='accountnamecolumn']").text();
            var accountId: string = $(this).find("td")[0].innerHTML;
            $("#PortalId").val($(this).find("td[class='portalId']").text());
            $("#ddlUserType option:selected").prop("selected", false);
            if (accountId != undefined && accountName != undefined) {
                $('#AccountName').val(accountName);
                $('#selectedAccountName').val(accountName);
                $('#AccountId').val(accountId);
                $('#accountListId').hide(700);
                GiftCard.prototype.GetActiveCurrencyToStore("");
                $("#ZnodeUserAccountList").html("");
                ZnodeBase.prototype.RemovePopupOverlay();
                User.prototype.OnAccountSelection();
                return;
            }
            else {
                ZnodeBase.prototype.RemovePopupOverlay();
                return;
            }
        });
    }

    //This method is used to get account list on aside panel.
    GetAccountList(selectedPortal: number): any {
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Customer/GetAccountList?portalId=' + selectedPortal, 'accountListId');
    }

    //Clear selected account name.
    ClearAccountName(): boolean {
        $('#AccountName').val(undefined);
        $('#AccountId').val(undefined);
        $('#ddlDepartment').children('option').remove();
        $('#ddlUserType').children('option').remove();
        $('#ddlAccountType').children('option').remove();
        $('#divDepartmentId').hide();
        $('#divUserTypeId').hide();
        $('#divRole').hide();
        $('#approvalNamesDiv').hide();
        $('#maxBudgetDiv').hide();
        $('#ZnodeUserAccountList').html("");
        $('#ddlPortals').show();
        $('#hdnRoleName').val("");
        return false;
    }

    CheckIsAllPortalSelected(): any {
        if ($("#IsSelectAllPortal").val() == "True") {
            $("#areaList ul li input:checkbox").click();
        }
    }

    ConvertToOrder() {
        window.location.href = $("#hdnOrderURL").val();
    }

    GetUserPermissionList(): void {
        if ($("#hdnRoleName").val() == "User") {
            Endpoint.prototype.GetPermissionList($("#AccountId").val(), $("#AccountPermissionAccessId").val(), function (response) {
                $('#permission_options').html("");
                $('#permission_options').html(response);
                $("#ddlPermission").attr("onchange", "User.prototype.OnPermissionSelection();");
            });
        }
    }
}