﻿var boostValues: Array<string> = new Array();

class SearchConfiguration extends ZnodeBase {
    _endpoint: Endpoint;

    constructor() {
        super();
        this._endpoint = new Endpoint();
    }

    Init() {
        SearchConfiguration.prototype.GetSearchIndexMonitorList();
    }

    GetProductBoostValuesByCatalogId(): any {
        $(".groupPannel").removeClass("tab-selected");
        $("#lnkProductBoostList").parent("li").addClass("tab-selected");
        $("#createIndexSection").html("");

        var catalogId = $("#hdnPublishCatalogId").val();
        Endpoint.prototype.GetProductBoostSetting(catalogId, function (response) {
            $("#BoostSetting").html(response);
        });
    }

    GetCategoryProductBoostValuesByCatalogId(): any {
        $(".groupPannel").removeClass("tab-selected");
        $("#lnkCategoryBoostList").parent("li").addClass("tab-selected");
        $("#createIndexSection").html("");

        var catalogId = $("#hdnPublishCatalogId").val();
        Endpoint.prototype.GetProductCategoryBoostSetting(catalogId, function (response) {
            $("#BoostSetting").html(response);
        });
    }

    GetFieldBoostValuesByCatalogId(): any {
        $(".groupPannel").removeClass("tab-selected");
        $("#lnkFieldBoostList").parent("li").addClass("tab-selected");
        $("#createIndexSection").html("");

        var catalogId = $("#hdnPublishCatalogId").val();
        Endpoint.prototype.GetFieldBoostSetting(catalogId, function (response) {
            $("#BoostSetting").html(response);
        });
    }

    GetStoreSearchConfigurations() {
        $("#BoostSetting").html("");
        $(".groupPannel").removeClass("active");
        $("#lnkSearchProfileList").removeClass("active");
        $("#lnkKeywordsRedirectList").removeClass("active");
        $("#lnkSynonymsList").removeClass("active");
        $("#lnkCreateIndex").parent("li").addClass("active");

        Endpoint.prototype.GetSearchConfiguration($("#hdnPublishCatalogId").val(), $('#txtCatalogName').val(), function (response) {
            $("#createIndexSection").html(response);
            SearchConfiguration.prototype.GetSearchIndexMonitorList();
        });

    }

    GetSearchProfiles() {
        //$(".groupPannel").removeClass("tab-selected")
        //$("#lnkSearchProfileList").parent("li").addClass("tab-selected")
        $("#BoostSetting").html("");
        $("#createIndexSection").html("");

        Endpoint.prototype.GetSearchProfiles(function (response) {
            $("#ZnodeSearchProfileAttribute").html(response);
            $(".groupPannel").removeClass("tab-selected");
            $("#lnkSearchProfileList").parent("li").addClass("tab-selected");
        });
    }

    GetSearchFacets() {
        $(".groupPannel").removeClass("tab-selected");
        $("#lnkSearchFacetsList").parent("li").addClass("tab-selected");
        $("#BoostSetting").html("");

        Endpoint.prototype.GetAssociatedCatalogAttributes(function (response) {
            $("#createIndexSection").html(response);
        });

    }

    GetSearchIndexMonitorList(): any {
        if ($("#HasError").val() == "True")
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper($("#ErrorMessage").val(), 'error', isFadeOut, fadeOutTime);

        var catalogIndexId = $("#CatalogIndexId").val();
        if (catalogIndexId != undefined) {
            Endpoint.prototype.GetSearchIndexMonitorList(catalogIndexId, function (response) {
                $("#searchIndexMonitorList").html("");
                $("#searchIndexMonitorList").html(response);
            });
        }
    }

    GetSearchIndexServerStatusList(searchIndexMonitorId): any {
        Endpoint.prototype.GetSearchIndexMonitorList(searchIndexMonitorId, function (response) {
            $("#searchIndexMonitorList").html("");
            $("#searchIndexMonitorList").html(response);
        });
    }

    ViewServerStatus(zViewAnchor): any {
        zViewAnchor.attr("href", "#");
        var indexMonitorId = $($(zViewAnchor[0]).closest("tr").find("td")[0]).html();
        ZnodeBase.prototype.BrowseAsidePoupPanel('/SearchConfiguration/GetSearchIndexServerStatusList?searchIndexMonitorId=' + indexMonitorId, 'divSearchIndexServerStatusList');
    }

    GetCreateIndexSchedulerView(): any {
        $("#createSchedulerError").hide();
        if (parseInt($("#CatalogIndexId").val(), 10) > 0) {
            var footer = "<button type='button' class='popup-panel-close' onclick='ZnodeBase.prototype.CancelUpload('divCreateScheduler')'><i class='z-close'></i></button>";
            ZnodeBase.prototype.ShowLoader();
            var catalogId = $("#hdnPublishCatalogId").val();
            var url = "/TouchPointConfiguration/Create?ConnectorTouchPoints=createIndex_" + $("#IndexName").val() + "&indexName=" + $("#IndexName").val() + "&schedulerCallFor=Indexer&catalogId=" + catalogId + "&catalogIndexId=" + parseInt($("#CatalogIndexId").val(), 10);
            Endpoint.prototype.GetPartial(url, function (response) {
                var htmlContent = footer + response;
                $("#divCreateScheduler").html(htmlContent);
                $($("#divCreateScheduler").find("a.grey")).attr("href", "#");
                $($("#divCreateScheduler").find("a.grey")).attr("onclick", "ZnodeBase.prototype.CancelUpload('divCreateScheduler')");
                $("#divCreateScheduler").show(700);
                $("body").append("<div class='modal-backdrop fade in'></div>");
                ZnodeBase.prototype.HideLoader();
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper("Please create index before creating scheduler.", 'error', isFadeOut, fadeOutTime);
        }
    }

    CreateScheduler(): any {
        var isValid: boolean = SearchConfiguration.prototype.ValidateSchedulerData();
        var schedulerName: string = $("#SchedulerName").val();
        if (isValid) {

            var weekDays = [];
            $('[name=WeekDaysArray]:checked').each(function () {
                weekDays.push(this.value);
            });

            var erpTaskSchedulerViewModel = {
                "ERPTaskSchedulerId": $("#ERPTaskSchedulerId").val(),
                "ExpireDate": $("#ExpireDate").val(),
                "ExpireTime": $("#ExpireTime").val(),
                "IndexName": $("#IndexName").val(),
                "IsEnabled": $("[id=IsActive]:checked").val(),
                "IsRepeatTaskEvery": $("#IsRepeatTaskEvery").val(),
                "SchedulerCallFor": $("#SchedulerCallFor").val(),
                "PortalId": $("#PortalId").val(),
                "CatalogId": $("#CatalogId").val(),
                "CatalogIndexId": $("#CatalogIndexId").val(),
                "RecurEvery": $("#RecurEvery").val(),
                "RepeatTaskEvery": $("#RepeatTaskEvery option:selected").val(),
                "RepeatTaskForDuration": $("#RepeatTaskForDuration option:selected").val(),
                "SchedulerFrequency": $('[name=SchedulerFrequency]:checked').val(),
                "SchedulerName": $("#SchedulerName").val(),
                "StartDate": $("#StartDate").val(),
                "StartTime": $("#StartTime").val(),
                "TouchPointName": $("#TouchPointName").val(),
                "WeekDaysArray": weekDays,
                "MonthsArray": $("[name = MonthsArray]").val(),
                "DaysArray": $("[name=DaysArray]").val(),
                "OnDaysArray": $("[name=OnDaysArray]").val(),
                "IsMonthlyDays": $("[name=IsMonthlyDays]:checked").val()
            };

            if (parseInt($("#ERPTaskSchedulerId").val(), 10) > 0) {
                Endpoint.prototype.EditSearchScheduler(erpTaskSchedulerViewModel, function (response) {
                    if (response.status) {
                        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SearchSchedulerUpdatedSuccessfully"), response.status ? 'success' : 'error', isFadeOut, fadeOutTime);
                        ZnodeBase.prototype.CancelUpload('divCreateScheduler');
                    }
                    else {
                        $("#createSchedulerError").text(response.message);
                        $("#createSchedulerError").show();
                    }
                });
            }
            else {
                Endpoint.prototype.CreateSearchScheduler(erpTaskSchedulerViewModel, function (response) {
                    if (response.status) {
                        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SearchSchedulerCreatedSuccessfully"), response.status ? 'success' : 'error', isFadeOut, fadeOutTime);
                        ZnodeBase.prototype.CancelUpload('divCreateScheduler');
                        $("#schedulerNameText").val(schedulerName);
                        $("#schedulerName").removeClass("hidden");
                        $(".createScheduler").html("");
                        $(".createScheduler").html("<i class='z-add-circle'></i>" + ZnodeBase.prototype.getResourceByKeyName("UpdateScheduler"));
                    }
                    else {
                        $("#createSchedulerError").text(response.message);
                        $("#createSchedulerError").show();
                    }
                });
            }
        }
    }

    ValidateSchedulerData(): boolean {
        var isValid = true;

        //Validate SchedulerName
        isValid = SearchConfiguration.prototype.ValidateSchedulerName() && isValid;

        //Validate TouchPointName
        isValid = SearchConfiguration.prototype.ValidateTouchpointName() && isValid;

        //Validate StartDate
        if ($("#StartDate").val() == "" || $("#StartDate").val().length < 1 || $("#StartDate").val() == null) {
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorStartDateRequired"), $("#StartDate"), $("#valStartDate"));
            isValid = isValid && false;
        }
        else {
            Products.prototype.HideErrorMessage($("#StartDate"), $("#valStartDate"));
        }

        //Validate StartTime
        if ($("#StartTime").val() == "" || $("#StartTime").val().length < 1 || $("#StartTime").val() == null) {
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorStartTimeRequired"), $("#StartTime"), $("#valStartTime"));
            isValid = isValid && false;
        }
        else {
            Products.prototype.HideErrorMessage($("#StartTime"), $("#valStartTime"));
        }
        return isValid;
    }

    ValidateSchedulerName(): boolean {
        if ($("#SchedulerName").val() == "" || $("#SchedulerName").val().length < 1 || $("#SchedulerName").val() == null) {
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorSchedulerNameRequired"), $("#SchedulerName"), $("#valSchedulerName"));
            return false;
        }
        else {
            Products.prototype.HideErrorMessage($("#SchedulerName"), $("#valSchedulerName"));
        }

        if ($("#SchedulerName").val().length > 100) {
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorSchedulerNameLength"), $("#SchedulerName"), $("#valSchedulerName"));
            return false;
        } else {
            Products.prototype.HideErrorMessage($("#SchedulerName"), $("#valSchedulerName"));
        }

        return true;
    }

    ValidateTouchpointName(): boolean {
        if ($("#TouchPointName").val() == "" || $("#TouchPointName").val().length < 1 || $("#TouchPointName").val() == null) {
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("TouchPointNameRequired"), $("#TouchPointName"), $("#valTouchPointName"));
            return false;
        } else {
            Products.prototype.HideErrorMessage($("#TouchPointName"), $("#valTouchPointName"));
        }

        if ($("#TouchPointName").val().length > 100) {
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorTouchPointNameLength"), $("#TouchPointName"), $("#valTouchPointName"));
            return false;
        } else {
            Products.prototype.HideErrorMessage($("#TouchPointName"), $("#valTouchPointName"));
        }
        return true;
    }

    ValidateBoostField(object): boolean {
        var isValid = true;
        if (isNaN($(object).val())) {
            $(object).addClass("input-validation-error");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("InvalidBoostValue"), 'error', isFadeOut, fadeOutTime);
            isValid = false;
        }
        else if ($(object).val() > 999999) {
            $(object).addClass("input-validation-error");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorBoostValue"), 'error', isFadeOut, fadeOutTime);
            isValid = false;
        }
        else {
            $(object).remove("input-validation-error");
            $(object).removeClass("input-validation-error");
            isValid = true;
        }
        return isValid;
    }

    //This method is used to get portal list on aside panel.
    GetPortalList(): any {
        ZnodeBase.prototype.BrowseAsidePoupPanel('/SearchConfiguration/GetPortalList', 'divStoreListAsidePanel');
    }

    //This method is used to select portal from list and show it on textbox.
    GetCatalogDetails(): void {
        $("#ZnodeStoreCatalog").find("tr").click(function () {
            let catalogName: string = $(this).find("td[class='catalogcolumn']").text();
            let publishCatalogId: string = $(this).find("td")[0].innerHTML;
            $('#txtCatalogName').val(catalogName);
            $('#hdnPublishCatalogId').val(publishCatalogId);
            if ($("#SearchConfigurationType").val() == "CreateIndex")
                SearchConfiguration.prototype.GetStoreSearchConfigurations();
            else {
                let selectedTab = $('#SelectedTab').val();
                if (selectedTab != undefined && selectedTab != "") {
                    switch (selectedTab) {
                        case "GetGlobalProductCategoryBoost": SearchConfiguration.prototype.GetCategoryProductBoostValuesByCatalogId(); break;
                        case "GetFieldLevelBoost": SearchConfiguration.prototype.GetFieldBoostValuesByCatalogId(); break;
                        case "GetGlobalProductBoost": SearchConfiguration.prototype.GetProductBoostValuesByCatalogId(); break;
                        case "GetSearchSynonymsList": SearchConfiguration.prototype.GetSynonymsByCatalogId(); break;
                        case "GetCatalogKeywordsList": SearchConfiguration.prototype.GetKeywordsRedirectByCatalogId(); break;
                    }
                }
            }
            $("#errorRequiredCatalog").text("").removeClass("field-validation-error").hide();
            $("#txtCatalogName").removeClass('input-validation-error');
            $('#divCataloglistPopup').hide(700);
            ZnodeBase.prototype.RemovePopupOverlay();
        });
    }

    GetSynonymsByCatalogId(): any {
        $("#createIndexSection").html("");
        $(".groupPannel").removeClass("active");
        $("#lnkCreateIndex").removeClass("active");
        $("#lnkSearchProfileList").removeClass("active");
        $("#lnkKeywordsRedirectList").removeClass("active");
        $("#lnkSynonymsList").parent("li").addClass("active");

        var catalogId = $("#hdnPublishCatalogId").val();
        Endpoint.prototype.GetSynonymsList(catalogId, function (response) {
            $("#SearchSynonymsList").html(response);
        });
    }

    GetKeywordsRedirectByCatalogId(): any {
        $("#createIndexSection").html("");
        $(".groupPannel").removeClass("active");
        $("#lnkCreateIndex").removeClass("active");
        $("#lnkSearchProfileList").removeClass("active");
        $("#lnkSynonymsList").removeClass("active");
        $("#lnkKeywordsRedirectList").parent("li").addClass("active");

        var catalogId = $("#hdnPublishCatalogId").val();
        Endpoint.prototype.GetKeywordsRedirectList(catalogId, function (response) {
            $("#SearchKeywordsList").html(response);
        });
    }

    SetLinkParamter(control): any {
        var catalogId: number = $("#publishCatalogId").val();
        var catalogName: string = $("#catalogName").val();
        var href = $(control).attr("href");
        if (catalogId != undefined && catalogName != undefined) {
            var encodedCatalogName = encodeURIComponent(catalogName);
            $(control).attr("href", href + "?catalogId=" + catalogId + "&catalogName=" + encodedCatalogName);
        }

    }

    GetAddSynonymsView(zViewAnchor: any): any {
        zViewAnchor.attr("href", "#");
        var searchSynonymsId = $(zViewAnchor).attr("data-parameter").split('&')[0].split('=')[1];

        ZnodeBase.prototype.BrowseAsidePoupPanel('/SearchConfiguration/EditSearchSynonyms?searchSynonymsId=' + searchSynonymsId, 'divAddSynonymsPopup');
    }

    CreateSynonyms(): any {
        var originalTerms = $("#frmCreateEditSynonyms input[name=OriginalTerm]").val();
        var replaceByTerms = $("#frmCreateEditSynonyms input[name=ReplacedBy]").val();

        if (SearchConfiguration.prototype.ValidateSynonymsForm(originalTerms, replaceByTerms)) {
            var SearchSynonymsId: number = $("#frmCreateEditSynonyms input[name=SearchSynonymsId]").val();
            var synonymsViewModel = {
                "SearchSynonymsId": SearchSynonymsId,
                "PublishCatalogId": $("#PublishCatalogId").val(),
                "OriginalTerm": $("#frmCreateEditSynonyms input[name=OriginalTerm]").val(),
                "ReplacedBy": $("#frmCreateEditSynonyms input[name=ReplacedBy]").val(),
                "IsBidirectional": $("#frmCreateEditSynonyms [id=IsBidirectional]:checked").val(),
            };

            if (SearchSynonymsId > 0) {
                Endpoint.prototype.EditSynonyms(synonymsViewModel, function (response) {
                    if (response.status) {
                        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RecordUpdatededSuccessfully"), response.status ? 'success' : 'error', isFadeOut, fadeOutTime);
                        ZnodeBase.prototype.CancelUpload('divAddSynonymsPopup');
                        SearchConfiguration.prototype.GetSynonymsByCatalogId();
                    }
                    else {
                        $("#CreateSynonymsError").text(response.message);
                        $("#CreateSynonymsError").show();
                    }
                });
            }
            else {
                Endpoint.prototype.AddSynonyms(synonymsViewModel, function (response) {
                    if (response.status) {
                        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RecordCreatedSuccessfully"), response.status ? 'success' : 'error', isFadeOut, fadeOutTime);
                        ZnodeBase.prototype.CancelUpload('divAddSynonymsPopup');
                        SearchConfiguration.prototype.GetSynonymsByCatalogId();
                    }
                    else {
                        $("#CreateSynonymsError").text("Failed");
                        $("#CreateSynonymsError").show();
                    }
                });
            }
        }
    }

    ValidateSynonymsForm(originalTerms: string, replaceByTerms: string): boolean {
        if (originalTerms == "" && replaceByTerms == "") {
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorOriginalTerm"), $("#OriginalTerm"), $("#valOriginalTerm"));
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorReplacedTerm"), $("#ReplacedBy"), $("#valReplacedBy"));
            return false;
        }
        else if (originalTerms == "") {
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorOriginalTerm"), $("#OriginalTerm"), $("#valOriginalTerm"));
            return false;
        }
        else if (replaceByTerms == "") {
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorReplacedTerm"), $("#ReplacedBy"), $("#valReplacedBy"));
            return false;
        }

        return true;
    }

    DeleteSearchSynonyms(control): any {
        var searchSynonymsId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (searchSynonymsId.length > 0) {
            Endpoint.prototype.DeleteSearchSynonyms(searchSynonymsId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    DeleteKeywords(control): any {
        var searchKeywordsRedirectId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (searchKeywordsRedirectId.length > 0) {
            Endpoint.prototype.DeleteKeywords(searchKeywordsRedirectId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    CreateKeywordsRedirect(): any {
        var keywords = $("#frmCreateEditKeywords input[name=Keywords]").val();
        var url = $("#frmCreateEditKeywords input[name=URL]").val();

        if (SearchConfiguration.prototype.ValidateKeywordsForm(keywords, url)) {
            var SearchKeywordsRedirectId: number = $("#frmCreateEditKeywords input[name=SearchKeywordsRedirectId]").val();
            var keywordsViewModel = {
                "SearchKeywordsRedirectId": SearchKeywordsRedirectId,
                "PublishCatalogId": $("#PublishCatalogId").val(),
                "Keywords": $("#frmCreateEditKeywords input[name=Keywords]").val(),
                "URL": $("#frmCreateEditKeywords input[name=URL]").val(),
            };
            if (SearchKeywordsRedirectId > 0) {
                Endpoint.prototype.EditKeywords(keywordsViewModel, function (response) {
                    if (response.status) {
                        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RecordUpdatededSuccessfully"), response.status ? 'success' : 'error', isFadeOut, fadeOutTime);
                        ZnodeBase.prototype.CancelUpload('divAddKeywordsPopup');
                        SearchConfiguration.prototype.GetKeywordsRedirectByCatalogId();
                    }
                    else {
                        $("#CreateKeywordsError").text(response.message);
                        $("#CreateKeywordsError").show();
                    }
                });
            }
            else {
                Endpoint.prototype.AddKeywords(keywordsViewModel, function (response) {
                    if (response.status) {
                        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RecordCreatedSuccessfully"), response.status ? 'success' : 'error', isFadeOut, fadeOutTime);
                        ZnodeBase.prototype.CancelUpload('divAddKeywordsPopup');
                        SearchConfiguration.prototype.GetKeywordsRedirectByCatalogId();
                    }
                    else {
                        $("#CreateKeywordsError").text("Failed");
                        $("#CreateKeywordsError").show();
                    }
                });
            }
        }
    }

    ValidateKeywordsForm(keywords: string, url: string): boolean {
        if (keywords == "" && url == "") {
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorKeywordsTerm"), $("#Keywords"), $("#valKeywords"));
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorURLTerm"), $("#URL"), $("#valURL"));
            return false;
        }
        else if (keywords == "") {
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorKeywordsTerm"), $("#Keywords"), $("#valKeywords"));
            return false;
        }
        else if (url == "") {
            $("#valKeywords").html("");
            Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorURLTerm"), $("#URL"), $("#valURL"));
            return false;
        }

        if (url != "") {
            var URLs = url.split(",");
            if (URLs.length > 1) {
                Products.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorMultipleURL"), $("#URL"), $("#valURL"));
                return false;
            }
        }
        return true;
    }

    GetAddKeywordsView(zViewAnchor: any): any {
        zViewAnchor.attr("href", "#");
        var searchKeywordsRedirectId = $(zViewAnchor).attr("data-parameter").split('&')[0].split('=')[1];

        ZnodeBase.prototype.BrowseAsidePoupPanel('/SearchConfiguration/EditSearchKeywordsRedirect?searchKeywordsRedirectId=' + searchKeywordsRedirectId, 'divAddKeywordsPopup');
    }

    WriteSearchFile(isSynonymsFile: boolean): any {
        var catalogId = $("#hdnPublishCatalogId").val();
        Endpoint.prototype.WriteSynonymsFile(catalogId, isSynonymsFile, function (response) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, response.status ? 'success' : 'error', isFadeOut, fadeOutTime);
        });
    }

    DeleteIndex(): any {
        var catalogIndexId = $("#CatalogIndexId").val();
        Endpoint.prototype.DeleteIndex(catalogIndexId, function (response) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, response.status ? 'success' : 'error', isFadeOut, fadeOutTime);
        });
    }

    RevisionTypePopUp(): any {
        $("#PortalIndexPopup").modal('show');
    }

    CreateIndexSection(): any {
        let publishStateData: string = 'NONE';

        if ($('#radBtnPublishState').length > 0) {
            publishStateData = ZnodeBase.prototype.mergeNameValuePairsToString($('#radBtnPublishState').serializeArray());
            $("#RevisionType").val(publishStateData);
            $("#frmCreateIndexData").submit();
        }
    }
}