﻿class Store extends ZnodeBase {
    _endPoint: Endpoint;
    _Model: any;
    _notification: ZnodeNotification;
    
    constructor() {
        super();
        this._endPoint = new Endpoint();
        this._notification = new ZnodeNotification();
        Store.prototype.GetValueOnFormPost();
        Store.prototype.BindCurrencyInfo();
        Store.prototype.LoadCatalogTree();
        Store.prototype.DisplayTreeNodeInfo();
        Store.prototype.SearchJstreeNode();
        WebSite.prototype.Init();
    }

    Init() {
        Store.prototype.ShowHideValidationEmailNotifaction();
        Store.prototype.ShowAllRightLeftButton();
        Store.prototype.IsDefaultWarehouse();
        $(document).on("UpdateGrid", Store.prototype.PortalProfileUpdateResult);

        $(document).on("click", "#btnAllMoveLeft", function () {
            var selectedCitiesCount = $("#SelectedCities li").length;
            $("#SelectedCities li").each(function () {
                $("#UnAssignedListBox").append("<li data-id=" + $(this).attr("data-id") + " title='" + $(this).html() + "' data-value=" + $(this).attr("data-value") + ">" + $(this).html() + "</li>");
                $(this).remove();
                $("#btnAllMoveLeft").addClass("not-active");
                $("#btnAllMoveRight").removeClass("not-active");
                $("#btnMoveLeft").addClass("not-active");
            });
            var unAssignedListBoxCount = $("#UnAssignedListBox").length;
            if ((unAssignedListBoxCount + selectedCitiesCount) > 1) {
                $("#UnAssignedListBox li").each(function () {
                    if ($(this).hasClass('selected')) {
                        $("#btnAllUp").removeClass("not-active");
                        $("#btnMoveUp").removeClass("not-active");
                        $("#btnMoveDown").removeClass("not-active");
                        $("#btnAllDown").removeClass("not-active");
                    }
                });
            }
        });
        $(document).on("click", "#btnAllMoveRight", function () {
            $("#UnAssignedListBox li").each(function () {
                if ($(this).attr("data-mustshow") != "y") {
                    $("#SelectedCities").append("<li data-id=" + $(this).attr("data-id") + " title='" + $(this).html() + "' data-value=" + $(this).attr("data-value") + ">" + $(this).html() + "</li>");
                    $(this).remove();
                    $("#btnAllMoveRight").addClass("not-active");
                    $("#btnAllMoveLeft").removeClass("not-active");
                    $("#btnAllUp").addClass("not-active");
                    $("#btnMoveUp").addClass("not-active");
                    $("#btnMoveDown").addClass("not-active");
                    $("#btnAllDown").addClass("not-active");
                    $("#btnMoveRight").addClass("not-active");
                }
            });
        });
        $(document).on("click", "#btnMoveUp", function () {
            Store.prototype.listbox_move("UnAssignedListBox", "up");
        });
        $(document).on("click", "#btnMoveDown", function () {
            Store.prototype.listbox_move("UnAssignedListBox", "down");
        });
        $(document).on("click", "#btnAllUp", function () {
            Store.prototype.listbox_move("UnAssignedListBox", "First");
        });
        $(document).on("click", "#btnAllDown", function () {
            Store.prototype.listbox_move("UnAssignedListBox", "Last");
        });

        $(document).on("click", "#SelectedCities li", function (e) {
            if ($(this) != undefined) {
                $("#SelectedCities li").removeClass("selected");
                $(this).addClass("selected");
                $("#btnMoveLeft").removeClass("not-active");
            }
        });

        $(document).on("click", "#UnAssignedListBox li", function (e) {
            if ($(this) != undefined) {
                $("#UnAssignedListBox li").removeClass("selected");
                $(this).addClass("selected");
                if ($("#UnAssignedListBox li").length > 1) {
                    $("#btnAllUp").removeClass("not-active");
                    $("#btnMoveUp").removeClass("not-active");
                    $("#btnMoveDown").removeClass("not-active");
                    $("#btnAllDown").removeClass("not-active");
                }
                $("#btnMoveRight").removeClass("not-active");
            }
        });

        $(document).on("click", "#btnMoveLeft", function () {
            var elements = $("#SelectedCities li.selected");
            if (elements != undefined) {
                $("#UnAssignedListBox").append($("#SelectedCities li.selected"));
                elements.removeClass("selected");
                $("#btnMoveLeft").addClass("not-active");
                if ($("#SelectedCities li").length < 1)
                    $("#btnAllMoveLeft").addClass("not-active");
                if ($("#UnAssignedListBox li").length > 0) {
                    $("#btnAllMoveRight").removeClass("not-active");
                }
            }
        });

        $(document).on("click", "#btnMoveUp", function () {
            Store.prototype.listbox_move("RequestedSelectedCities", "up");
        });

        $(document).on("click", "#btnMoveRight", function () {
            var elements = $("#UnAssignedListBox li.selected");
            if (elements != undefined) {
                $("#SelectedCities").append($("#UnAssignedListBox li.selected"));
                elements.removeClass("selected");
                $("#btnMoveRight").addClass("not-active");
                if ($("#UnAssignedListBox li").length < 1) {
                    $("#btnAllMoveRight").addClass("not-active");
                    $("#btnAllMoveRight").addClass("not-active");
                    $("#btnAllUp").addClass("not-active");
                    $("#btnMoveUp").addClass("not-active");
                    $("#btnMoveDown").addClass("not-active");
                    $("#btnAllDown").addClass("not-active");
                }
                if ($("#SelectedCities li").length > 0)
                    $("#btnAllMoveLeft").removeClass("not-active");
            }
        });
    }

    ValidateStore(): any {
        $("#DomainName :input").blur(function () {
            ZnodeBase.prototype.ShowLoader();
            Store.prototype.ValidateStoreDomainName();
            ZnodeBase.prototype.HideLoader();
        });
    }

    ValidateStoreDomainName(): boolean {
        var isValid = true;
        if ($("#aside-popup-panel #DomainName").val() != '') {
            Endpoint.prototype.IsDomainNameExist($("#aside-popup-panel #DomainName").val(), $("#aside-popup-panel #DomainId").val(), function (response) {
                if (!response) {
                    $("#aside-popup-panel #DomainName").addClass("input-validation-error");
                    $("#aside-popup-panel #errorSpanDomainName").addClass("error-msg");
                    $("#aside-popup-panel #errorSpanDomainName").text(ZnodeBase.prototype.getResourceByKeyName("AlreadyExistStoreDomainName"));
                    $("#aside-popup-panel #errorSpanDomainName").show();
                    isValid = false;
                    ZnodeBase.prototype.HideLoader();
                }
            });
        }
        return isValid;
    }
    DeleteMultipleStore(control): any {
        var portalId = this.GetCheckedStoreCode("StoreCode");
        if (portalId.length > 0) {
            Endpoint.prototype.DeleteStoreByStoreCode(portalId, function (response) {
                DynamicGrid.prototype.RefreshGridOndelete(control, response);
            });
        }
    }

    GetCheckedStoreCode(columnName: string): any {
        var storeControl = $('#grid input:checked').parent().parent() ? $('#grid input:checked').parent().parent().find('label[data-columnname=' + columnName + ']') : "";
        var storeCodes: string = "";
        if (storeControl) {
            for (var index = 0; index < storeControl.length; index++) {
                var element = storeControl[index];
                storeCodes = storeCodes.concat(element['textContent'], ',');
            }
            return storeCodes.slice(0, -1)
        }
        return storeCodes;
    }

    ViewPortalCatalog(): any {
        var publishCatalogId = $('#hdnPublishCatalogId :selected').val();
        var url = window.location.protocol + "//" + window.location.host + "/Store/ViewPortalCatalog?PublishCatalogId=" + publishCatalogId + "&portalId=" + $("#PortalId").val();
        window.open(url, '_blank');
    }

    EnableDomain(): any {
        var domainIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (domainIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.EnableDisableDomain($("#PortalId").val(), domainIds, true, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/Store/UrlList?portalId=" + $("#PortalId").val();
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    DisableDomain(): any {
        var domainIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (domainIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.EnableDisableDomain($("#PortalId").val(), domainIds, false, function (res) {
                ZnodeBase.prototype.HideLoader();
                window.location.href = window.location.protocol + "//" + window.location.host + "/Store/UrlList?portalId=" + $("#PortalId").val();
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    ShowAllRightLeftButton(): any {
        var left = $("#SelectedCities li").length;
        var right = $("#UnAssignedListBox li").length;
        if (left < 1) {
            $("#btnAllMoveLeft").addClass("not-active");
            $("#btnMoveLeft").addClass("not-active");
        }
        if (right < 1) {
            $("#btnAllMoveRight").addClass("not-active");
            $("#btnMoveRight").addClass("not-active");
        }
    }

    CopyStore(): any {
        $("#grid tbody tr td").find(".z-copy").on("click", function (e) {
            e.preventDefault();
            var portalId = $(this).attr("data-parameter").split('&')[0].split('=')[1];
            Endpoint.prototype.CopyStore(portalId, function (res) {
                if (res != "") {
                    $("#divCopyStorePopup").modal("show");
                    $("#divCopyStorePopup").html(res);
                }
                else
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorMessage"), 'error', isFadeOut, fadeOutTime);
            });
        });
    }

    CopyStoreResult(response: any): any {
        if (response.status) {
            Endpoint.prototype.GetStoreList(function (res) {
                $("#divCopyStorePopup").html(res);
                $("#divCopyStorePopup").modal("hide");
            });
        }
        $("#divCopyStorePopup").modal("hide");
        WebSite.prototype.RemovePopupOverlay();
        window.location.href = window.location.protocol + "//" + window.location.host + "/Store/List";
    }

    DeleteMultipleUrl(control): any {
        var urlIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (urlIds.length > 0) {
            Endpoint.prototype.DeleteUrl(urlIds, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    DeleteMultiplePortalProfile(control): any {
        var portalProfileIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (portalProfileIds.length > 0) {
            Endpoint.prototype.DeletePortalProfile(portalProfileIds, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    BindCurrencyInfo(): any {
        if ($('#ddlCurrencyType').val() == 0) {
            Document.prototype.write("Please select Currency");
        }

        if ($("#ddlCurrencyType").val() == $("#OldCurrencyId").val()) {
            $("#txtCurrencySuffix").val($("#CurrencySuffix").val());
            $("#txtCurrencyPreviewVal").val($("#CurrencyPreview").val());
        } else {
            Endpoint.prototype.GetCurrencyInfo($("#ddlCurrencyType").val(), $("#OldCurrencyId").val(), function (response) {
                $("#txtCurrencySuffix").val(response.CurrencySuffix);
                $("#txtCurrencyPreviewVal").val(response.CurrencyPreview);
            });
        }
    }

    SetDefaultOrderState(RequiresManualApproval, pendingApproval) {

        if ($(RequiresManualApproval).prop("checked") == true) {
            $('#ddlOrderStatus').append('<option value="50" selected=selected>' + pendingApproval + '</option>');
            $('#ddlOrderStatus').attr("disabled", true);
        } else {
            $('#ddlOrderStatus').attr("disabled", false);
            $('#ddlOrderStatus').find("option[value='50']").remove();
        }
    }

    SetRequiresManualApproval(pendingApproval, pendingApprovalText) {
        if (pendingApproval == "50") {
            $("#IsMannualApproval").prop("checked", "checked");
            Store.prototype.SetDefaultOrderState($("#IsMannualApproval"), pendingApprovalText);
        }
    }

    GetValueOnFormPost(): any {
        $("#frmStore").on("submit", function () {
            $("#OrderStatusId").val($("#ddlOrderStatus").val());
        });
    }

    GetWarehouses(): void {
        var warehouseId = $("#warehouseList option:selected").val();
        var portalId = (parseInt($("#PortalId").val(), 10));
        Endpoint.prototype.GetWarehouses(portalId, warehouseId, function (res) {
            $("#warehouse-manage").html(res);
            ZnodeBase.prototype.activeAsidePannel();
        });
    }

    AssociateWarehouses(portalId: number, backURL: string) {
        if ($("#warehouseList").val() == undefined || $("#warehouseList").val() == null || $("#warehouseList").val() == "") {
            $("#Error-Warehouse").html(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneWarehouse"));
        }
        else {
            var alternateWarehouseIds = [];
            var warehouseId = (parseInt($("#WarehouseId").val(), 10));
            $("#UnAssignedListBox").find("li").each(function () {
                if ($("#UnAssignedListBox").find("li").length > 0) {
                    var id = $(this).attr("data-value");
                    alternateWarehouseIds.push(id);
                }
            });
            if (typeof (backURL) != "undefined")
                $.cookie("_backURL", backURL, { path: '/' });
            Endpoint.prototype.AssociateWarehouseToStore(portalId, warehouseId, alternateWarehouseIds.join(","), function (response) {
                window.location.href = window.location.protocol + "//" + window.location.host + "/Store/GetAssociatedWarehouseList?portalId=" + portalId;
            });
        }
    }

    EditPortalCatalog(): any {
        $("#grid tbody tr td").find(".z-edit").click(function (e) {
            e.preventDefault();
            var portalCatalogId = $(this).attr("data-parameter").split('&')[0].split('=')[1];
            Endpoint.prototype.EditPortalCatalog(portalCatalogId, function (res) {
                if (res != "") {
                    $("#divPortalCatalog").html(res);
                    $("#divPortalCatalog").modal("show");
                }
                else
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorMessage"), 'error', isFadeOut, fadeOutTime);
            });
        });
    }

    EditPortalCatalogResult(response: any) {
        if (response.status) {
            Endpoint.prototype.GetPortalAssociatedCatalog(response.portalId, function (res) {
                $("#ReplaceAsidePannelContentDiv").html(res);
            });
        }
        $("#divPortalCatalog").modal("hide");
        ZnodeBase.prototype.RemovePopupOverlay();
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, response.status ? 'success' : 'error', true, fadeOutTime);
    }

    listbox_move(listID, direction) {
        var selValue = $("#" + listID + " .selected").attr("data-value");
        var selText = $("#" + listID + " .selected").html();
        var selId = $("#" + listID + " .selected").attr("data-id");
        var mustShow = $("#" + listID + " .selected").attr("data-mustshow");
        var datatype = $("#" + listID + " .selected").attr("data-datatype");
        if (selValue != undefined && selId != undefined) {
            var dynamicHtml = "<li data-id='" + selId + "' title='" + selText + "' data-datatype='" + datatype + "' data-mustshow='" + mustShow + "' data-value='" + selValue + "'>" + selText + "</li>";
            if (direction == "First") {
                $("#" + listID).prepend(dynamicHtml);
            }
            else if (direction == "Last") {
                $("#" + listID).append(dynamicHtml);
            }
            else if (direction == "down") {
                var dynamicHtmlTest = $("#" + listID + " .selected").next("li");
                if (dynamicHtmlTest.length != 0) {
                    $("#" + listID + " .selected").next("li").after(dynamicHtml);
                } else { return false; }
            }
            else {
                var dynamicHtmlTest = $("#" + listID + " .selected").prev("li");
                if (dynamicHtmlTest.length != 0) {
                    $("#" + listID + " .selected").prev("li").before(dynamicHtml);
                } else { return false; }
            }
            $("#" + listID + " .selected").remove();
            $("#" + listID + " li[data-id='" + selId + "']").addClass("selected");
        }

    }
    LoadCatalogTree(): any {
        $("#Catalog_Tree").jstree({
            'core': {
                'multiple': false,
                'data': {
                    "url": function (node) {
                        var nodeId = "";
                        var url = "";
                        var portalCatalogId = $("#PublishCatalogId").val();
                        if (node.id == "#") {
                            url = '/Store/GetCatalogTree?portalCatalogId=' + portalCatalogId + "&publishCategoryId=-1";
                        }
                        else if (node.id == "0") {
                            url = '/Store/GetCatalogTree?portalCatalogId=' + portalCatalogId + "&publishCategoryId=0";
                        }
                        else {
                            url = '/Store/GetCatalogTree?portalCatalogId=' + portalCatalogId + "&publishCategoryId=" + node.id;
                        }
                        return url;
                    },
                    "success": function (new_data) {
                        return eval(new_data);
                    }
                }
            },
            //TODO Need to implement DB search
            //"search": {
            //    "case_insensitive": true,
            //    "ajax": {
            //        "url": '/Store/Demo',
            //        "success": function (searchresult) {
            //            var data = ["2", "5"];
            //            return data;
            //        }
            //    }
            //},
            "plugins": ['state', 'search', "themes", "json_data", "wholerow"]
        });
    }

    DisplayTreeNodeInfo(): any {
        $('#Catalog_Tree').on("select_node.jstree", function (e, data) {
            var url;
            if (data.node.id == 0) {
                url = "/Store/GetPublishCatalogDetails?publishCatalogId=" + $("#PublishCatalogId").val();
            }
            else if (data.node.id.indexOf("_product") > -1) {
                var id = data.node.id.split('_');
                url = "/Store/GetPublishProductDetails?publishProductId=" + id[0] + "&portalId=" + $("#PortalId").val();
            }
            else {
                url = "/Store/GetPublishCategoryDetails?publishCategoryId=" + data.node.id;
            }
            Endpoint.prototype.GetPublishInfo(url, function (response) {
                $("#DisplayPublishInfo").html(response);
            });

        });
    }

    SearchJstreeNode(): any {
        var timeout = 0;
        $('#TreeSearch').keyup(function () {
            if (timeout) { clearTimeout(timeout); }
            timeout = setTimeout(function () {
                var value = $('#TreeSearch').val();
                $('#Catalog_Tree').jstree(true).search(value);
            }, 250);
        });
    }

    IsDefaultWarehouse(): void {
        if ($("#IsDefaultWarehouse").is(":checked")) {
            $('.lblShippingOrigin').prop("disabled", true);
        }
        else {
            $('.lblShippingOrigin').prop('disabled', false);
        }
    };

    DeleteStoreLocator(control): any {
        var StoreLocationCode = this.GetCheckedStoreCode("StoreLocationCode");
        if (StoreLocationCode) {
            Endpoint.prototype.DeleteStoreLocatorByCode(StoreLocationCode, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    DefaultSubmit(SelectedIdArr: string[], Controller: string, Action: string, Callback: string) {
        var action = "SetDefault";
        var ids = [];
        ids = MediaManagerTools.prototype.unique();

        if (ids.length == 0)
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        else if (ids.length > 1)
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAnyOneToSetAsDefault"), 'error', isFadeOut, fadeOutTime);
        else {
            if (Action == "AssociateCountries")
                this.submitDefaultForCountry(ids, action, Controller, Action, Callback);
            else
                this.submit(ids, action, Controller, Action, Callback);
        }
    }

    submitDefaultForCountry(SelectedIdArr: string[], action: string, Controller: string, Action: string, Callback: any) {
        var countryCode = Store.prototype.GetMultipleValuesOfGridColumn('Country Code');
        var portalCountryId = parseInt(SelectedIdArr.toString());
        var isDefault = true;
        Endpoint.prototype.AssociateCountriesForStore($("#PortalId").val(), countryCode, isDefault, portalCountryId, function (res) {
            $("#associateCountryList").hide(700);
            window.location.href = window.location.protocol + "//" + window.location.host + "/Store/GetAssociatedCountryList?portalId=" + $('#PortalId').val();
        });
    }

    ActiveSubmit(SelectedIdArr: string[], Controller: string, Action: string, Callback: string) {
        var action = "SetActive";
        var ids = [];
        ids = MediaManagerTools.prototype.unique();
        if (ids.length == 0)
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        else {
            this.submit(ids, action, Controller, Action, Callback);
        }
    }

    DeActivateSubmit(SelectedIdArr: string[], Controller: string, Action: string, Callback: string) {
        var action = "SetDeActive";
        var ids = [];
        ids = MediaManagerTools.prototype.unique();
        if (ids.length == 0)
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        else {
            //ajax call.
            this.submit(ids, action, Controller, Action, Callback);
        }
    }

    submit(SelectedIdArr: string[], action: string, Controller: string, Action: string, Callback: any) {
        this._Model = { "LocaleId": SelectedIdArr.toString(), "Action": action, "PortalId": $("#hdnPortalID").val() };
        var url = "/" + Controller + "/" + Action;
        this.AssociateLocale(url, this._Model, Controller, Action, Callback);
    }

    AssociateLocale(url: string, model: any, controller: string, Action: any, callback: any): void {
        Endpoint.prototype.AssociateLocales(url, model, function (data) {
            if (data != "") {
                window.location.href = "/Store/LocaleList?portalId=" + $("#hdnPortalID").val();
            }
        });
    }

    BindCSSBasedOnThemeId(): any {
        $("span[data-valmsg-for='CMSThemeCSSId']").html("");
        var selectedTheme = $("#ddlTheme").val();
        if (selectedTheme == 0 && selectedTheme == "") {
            $('#ddlCSS').children('option:not(:first)').remove();
            return false;
        }
        Endpoint.prototype.GetCSSListForStore(selectedTheme, function (response) {
            $('#ddlCSS').children('option:not(:first)').remove();
            for (var i = 0; i < response.length; i++) {
                var opt = new Option(response[i].Text, response[i].Value);
                $('#ddlCSS').append(opt);
            }
        });
    }

    ValidateCSS(): any {
        var selectedTheme = $("#ddlTheme").val();
        if (selectedTheme == 0 && selectedTheme == "") {
            $("span[data-valmsg-for='CMSThemeCSSId']").html("Please select theme first.");
            $("span[data-valmsg-for='CMSThemeCSSId']").attr("class", "field-validation-error");
            return true;
        }
    }

    TestAvalaraConnection() {
        ZnodeBase.prototype.ShowLoader();
        $.ajax({
            url: "/Store/TestAvalaraConnection",
            data: $("#frmEditTax").serialize(),
            type: 'POST',
            success: function (data) {
                Store.prototype.HideLoader();
                if (!data.HasError) {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(data.Message, "success", isFadeOut, fadeOutTime);
                }
                else {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(data.Message, "error", isFadeOut, fadeOutTime);
                }
            }
        });
    }

    AssociateCountries() {
        var countryCode = DynamicGrid.prototype.GetMultipleSelectedIds();
        var portalId = $('#PortalId').val();
        if (countryCode.length > 0) {
            Endpoint.prototype.AssociateCountriesForStore(portalId, countryCode, false, 0, function (res) {
                $("#associateCountryList").hide(700);
                window.location.href = window.location.protocol + "//" + window.location.host + "/Store/GetAssociatedCountryList?portalId=" + portalId;
            });
        }
        else {
            $('#associatedCountryId').show();
        }
    }

    GetMultipleValuesOfGridColumn(columnName): any {
        var column = [];
        var value = "";
        var index = 0;
        $("#grid").find("tr.grid-header").find("th").each(function () {
            column.push($(this));
        });
        for (var i = 0; i < column.length; i++) {
            if (column[i].text().trim() == columnName) {
                index = i;
                break;
            }
        }
        $("#grid").find("tr").each(function () {
            if ($(this).find(".grid-row-checkbox").length > 0) {
                if ($(this).find(".grid-row-checkbox").is(":checked")) {
                    value = value + $(this).find("td")[index].innerHTML + ",";
                }
            }
        });
        return value.substr(0, value.length - 1);
    }

    GetUnassociatedCountryList(portalId: number): any {
        DynamicGrid.prototype.ClearCheckboxArray();
        ZnodeBase.prototype.ShowLoader();
        Endpoint.prototype.GetUnassociatedCountryList(portalId, function (res) {
            if (res != null && res != "") {
                $("#associateCountryList").html(res);
                $("#associateCountryList").show(700);
                $("body").css('overflow', 'hidden');
                ZnodeBase.prototype.HideLoader();
                $("body").append("<div class='modal-backdrop fade in'></div>");
            }
        });
    }

    UnAssociateCountries(control): any {
        var portalCountryId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (portalCountryId.length > 0) {
            Endpoint.prototype.RemoveAssociatedCountries(portalCountryId, $("#PortalId").val(), function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    EditAssociatedPriceListPrecedenceForStore(): any {
        $("#grid tbody tr td").find(".z-edit").click(function (e) {
            e.preventDefault();

            var listName = $(this).attr("data-parameter").split('&')[1].split('=')[1];
            var priceListId = parseInt($(this).attr("data-parameter").split('&')[0].split('=')[1]);

            Endpoint.prototype.EditAssociatedPriceListPrecedence(0, priceListId, $('#PortalId').val(), listName, function (res) {
                if (!res.status) {
                    $("#priceListPrecedence").modal("hide");
                    return false;
                }
                $("#priceListPrecedence").modal("show");
                $("#priceListPrecedence").html(res.data.html);
            });
        });
    }

    EditAssociatedPriceListPrecedenceForProfile(): any {
        $("#grid tbody tr td").find(".z-edit").click(function (e) {
            e.preventDefault();

            var listName = $(this).attr("data-parameter").split('&')[1].split('=')[1];
            var priceListProfileId = parseInt($(this).attr("data-parameter").split('&')[0].split('=')[1]);
            Endpoint.prototype.EditAssociatedPriceListPrecedence(priceListProfileId, $('#PriceListId').val(), $('#PortalId').val(), listName, function (res) {
                if (!res.status) {
                    $("#priceListPrecedence").modal("hide");
                    return false;
                }
                $("#priceListPrecedence").modal("show");
                $("#priceListPrecedence").html(res.data.html);
            });
        });
    }

    EditAssociatedPriceListPrecedenceResultForStore(data: any) {
        $("#priceListPrecedence").modal("hide");
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(data.Message, data.Status ? 'error' : 'success', isFadeOut, fadeOutTime);
        ZnodeBase.prototype.RemovePopupOverlay();
        Store.prototype.AssociatedStoresList();
    }

    EditAssociatedPriceListPrecedenceResultForProfile(data: any) {
        $("#priceListPrecedence").modal("hide");
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(data.Message, data.Status ? 'error' : 'success', isFadeOut, fadeOutTime);
        ZnodeBase.prototype.RemovePopupOverlay();
        Store.prototype.AssociatedProfilesList();
    }

    AssociatedStoresList() {
        Endpoint.prototype.GetAssociatedPriceListForStore($("#PortalId").val(), function (response) {
            $("#associateStore").html('');
            $("#associateStore").html(response);
            GridPager.prototype.UpdateHandler();
        });
    }

    AssociatedProfilesList() {
        Endpoint.prototype.GetAssociatedPriceListForProfile($("#PortalId").val(), $("#ProfileId").val(), function (response) {
            $("#associateProfile").html('');
            $("#associateProfile").html(response);
            GridPager.prototype.UpdateHandler();
        });
    }

    PreviewStore(zpreview): any {
        if (zpreview.attr("href") === "?DomainUrl=#") {
            zpreview.attr("href", "#");
        }
        else {
            zpreview.attr('target', '_blank');
        }
        var url = zpreview.attr("href");
        if (url.indexOf("?DomainUrl=") >= 0) {
            zpreview.attr("href", url.replace("?DomainUrl=", "//"));
        }
    }

    ValidatePrecedanceField(object): any {
        var isValid = true;
        var regex = new RegExp('^\\d{0,}?$');
        if (isNaN($(object).val())) {
            $(object).addClass("input-validation-error");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RequiredNumericValue"), 'error', isFadeOut, fadeOutTime);
            isValid = false;
        }
        else if (!regex.test($(object).val())) {
            $(object).addClass("input-validation-error");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("DisplayOrderRange"), 'error', isFadeOut, fadeOutTime);
            isValid = false;
        }
        else if ($(object).val() == '') {
            $(object).addClass("input-validation-error");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PrecedenceIsRequired"), 'error', isFadeOut, fadeOutTime);
            isValid = false;
        }
        else if ($(object).val() == 0) {
            $(object).addClass("input-validation-error");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("DisplayOrderRange"), 'error', isFadeOut, fadeOutTime);
            isValid = false;
        }
        else {
            $(object).remove("input-validation-error");
            $(object).removeClass("input-validation-error");
            isValid = true;
        }
        return isValid;
    }

    ValidateDomainNameField(object): any {
        var isValid = true;
        if ($(object).val() == '') {
            $(object).addClass("input-validation-error");
            if ($(object).val() == '')
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("DomainNameIsRequired"), 'error', isFadeOut, fadeOutTime);

            isValid = false;
        }
        else {
            $(object).remove("input-validation-error");
            $(object).removeClass("input-validation-error");
            isValid = true;
        }
        return isValid;
    }

    AssociateTaxClass(portalId: number): void {
        ZnodeBase.prototype.ShowLoader();
        var linkTaxClassIds: string = DynamicGrid.prototype.GetMultipleSelectedIds('ZnodePortalTaxClassAssociatedList');
        if (linkTaxClassIds.length > 0)
            Endpoint.prototype.AssociateTaxClassListToStore(linkTaxClassIds, portalId, function (res) {
                Endpoint.prototype.StoreAssociatedTaxClassList(portalId, function (response) {
                    $("#ZnodePortalTaxClassList").html('');
                    $("#ZnodePortalTaxClassList").html(response);
                    GridPager.prototype.UpdateHandler();
                });
                $("#DivGetUnAssociatedTaxListForStore").hide(700);
                ZnodeBase.prototype.RemovePopupOverlay();
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, res.status ? "success" : "error", isFadeOut, fadeOutTime);
                DynamicGrid.prototype.ClearCheckboxArray();
                ZnodeBase.prototype.HideLoader();
                ZnodeBase.prototype.RemoveAsidePopupPanel();
            });
        else {
            $('#associatedTaxClass').show();
            ZnodeBase.prototype.HideLoader();
        }
    }

    UnAssociateTaxClass(control: any): void {
        var taxClassIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (taxClassIds.length > 0) {
            Endpoint.prototype.UnAssociateTaxClass(taxClassIds, $("#PortalId").val(), function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    GetUnassociatedShipping(portalId) {
        DynamicGrid.prototype.ClearCheckboxArray();
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Store/GetUnAssociatedShippingList?portalId=' + portalId, 'divUnassociatedShippingListPopup');
    }

    AssociateShipping(portalId) {
        var shippingIds: string = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (shippingIds.length > 0) {
            Endpoint.prototype.AssociateShippingToStore(portalId, shippingIds, function (res) {
                if (res.status) {
                    Endpoint.prototype.GetAssociatedShippingListToStore(portalId, function (res) {
                        DynamicGrid.prototype.ClearCheckboxArray();
                        $("#ZnodeAssociatedShippingListToPortal").html(res);
                    });
                }
                $("#divUnassociatedShippingListPopup").hide(700);
                ZnodeBase.prototype.RemovePopupOverlay();
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, res.status ? 'success' : 'error', isFadeOut, fadeOutTime);
            });
            ZnodeBase.prototype.RemoveAsidePopupPanel();
        }
        else {
            $("#divAssociatedPortalShippingError").show();
        }
    }

    UnAssociateAssociatedShipping(control): any {
        var shippingId: string = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (shippingId.length > 0) {
            Endpoint.prototype.UnAssociateAssociatedShippingToStore(shippingId, $("#PortalId").val(), function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    SetPortalDefaultTax(): void {
        var taxClassIds = MediaManagerTools.prototype.unique();
        if (taxClassIds.length == 0)
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        else if (taxClassIds.length > 1)
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAnyOneToSetAsDefault"), 'error', isFadeOut, fadeOutTime);
        else {
            Endpoint.prototype.SetPortalDefaultTax(taxClassIds.toString(), $("#PortalId").val(), function (data) {
                window.location.href = "/Store/TaxList?portalId=" + $("#PortalId").val();
            });
        }
    }

    AssociatePaymentSetting(portalId: number): void {
        ZnodeBase.prototype.ShowLoader();
        var linkTaxClassIds: string = DynamicGrid.prototype.GetMultipleSelectedIds('ZnodePayment');
        if (linkTaxClassIds.length > 0)
            Endpoint.prototype.AssociatePaymentSetting(linkTaxClassIds, portalId, function (res) {
                Endpoint.prototype.GetAssociatedPaymentList(portalId, function (response) {
                    $("#AssociatedPaymentListToPortal").html('');
                    $("#AssociatedPaymentListToPortal").html(response);
                    GridPager.prototype.UpdateHandler();
                });
                $("#DivGetUnAssociatedPaymentSettingListForStore").hide(700);
                ZnodeBase.prototype.RemovePopupOverlay();
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, res.status ? "success" : "error", isFadeOut, fadeOutTime);
                DynamicGrid.prototype.ClearCheckboxArray();
                ZnodeBase.prototype.HideLoader();
                ZnodeBase.prototype.RemoveAsidePopupPanel();
            });
        else {
            $('#associatedTaxClass').show();
            ZnodeBase.prototype.HideLoader();
        }
    }

    RemoveAssociatedPaymentSetting(control: any): void {
        var taxClassIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (taxClassIds.length > 0) {
            Endpoint.prototype.RemoveAssociatedPaymentSetting(taxClassIds, $("#PortalId").val(), function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    GenerateImages(): boolean {
        Endpoint.prototype.GenerateImages($("#PortalId").val(), function (res) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, res.status ? "success" : "error", isFadeOut, fadeOutTime);
        });
        return false;
    }

    //This method is used to get publish catalog list on aside panel
    GetCatalogList(): any {
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Store/GetCatalogList', 'divCataloglistPopup');
    }

    //This method is used to get publish catalog list on aside panel
    GetPortalList(): any {
        ZnodeBase.prototype.BrowseAsidePoupPanel('/StoreLocator/GetPortalList', 'divStorePopupPanel');
    }

    //This method is used to select catalog from list and show it on textbox
    GetCatalogDetail(): void {
        $("#ZnodeStoreCatalog").find("tr").click(function () {
            let catalogName: string = $(this).find("td[class='catalogcolumn']").text();
            let publishProductId: string = $(this).find("td")[0].innerHTML;
            $('#txtCatalogName').val(catalogName);
            $('#hdnPublishCatalogId').val(publishProductId);
            $("#errorRequiredCatalog").text("").removeClass("field-validation-error").hide();
            $("#txtCatalogName").removeClass('input-validation-error');
            $('#divCataloglistPopup').hide(700);
            ZnodeBase.prototype.RemovePopupOverlay();
        });
    }

    //validation for store textbox
    ValidateForStore(): boolean {
        if ($("#txtPortalName").val() == "") {
            $("#errorRequiredStore").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorSelectPortal")).addClass("field-validation-error").show();
            $("#txtPortalName").addClass('input-validation-error');
            return false;
        }
        return true;
    }
    //This method is used to select portal from list and show it on textbox
    GetPortalDetail(): void {
        $("#grid").find("tr").click(function () {
            let portalName: string = $(this).find("td[class='storecolumn']").text();
            let portalId: string = $(this).find("td")[0].innerHTML;
            $('#txtPortalName').val(portalName);
            $('#hdnPortalId').val(portalId);
            $("#errorRequiredStore").text("").removeClass("field-validation-error").hide();
            $("#txtPortalName").removeClass('input-validation-error');
            $('#divStorePopupPanel').hide(700);
            ZnodeBase.prototype.RemovePopupOverlay();
        });
    }

    GetPortalPublishStatus(zViewAnchor): any {
        zViewAnchor.attr("href", "#");
        var portalId: string = $(zViewAnchor).attr("data-parameter").split('&')[0].split('=')[1];
        var storeName: string = $(zViewAnchor).attr("data-parameter").split('&')[1].split('=')[1];
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Store/GetPortalPublishStatus?portalId=' + portalId + '&storeName=' + storeName, 'divPortalPublishStatusList');
    }

    CloseUnassociateCountriesPopup() {
        ZnodeBase.prototype.CancelUpload('associateCountryList');
        _gridContainerName = "#ZnodePortalCountry";
    }

    PublishStorePopup(zPublishAnchor): any {
        zPublishAnchor.attr("href", "#");
        $("#HdnStoreId").val($(zPublishAnchor).attr("data-parameter").split('&')[0].split('=')[1]);
        $("#PublishStore").modal('show');
    }

    PublishStoreSetting(): any {
        let publishStateFormData: string = 'NONE';
        let publishContentFormData: string = '';
        
        if ($('#radBtnPublishState').length > 0)
            publishStateFormData = ZnodeBase.prototype.mergeNameValuePairsToString($('#radBtnPublishState').serializeArray());
        if ($('#chkBxPublishContentChoice').length > 0)
            publishContentFormData = ZnodeBase.prototype.mergeNameValuePairsToString($('#chkBxPublishContentChoice').serializeArray());
        
        Endpoint.prototype.PublishStoreSetting($("#HdnStoreId").val(), publishStateFormData, publishContentFormData, function (res) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, res.status ? 'success' : 'error', isFadeOut, fadeOutTime);
            ZnodeProgressNotifier.prototype.InitiateProgressBar(function () {
                DynamicGrid.prototype.RefreshGridNoNotification($("#ZnodeStore").find("#refreshGrid"));
            }); 
        });
    }

    ShowHideEmailNotifaction(): any {
        $('#enableDisable').toggle();
        if (!($("#EnableToStore").prop('checked'))) {
            $("#OrderAmount").rules("remove", 'required');
            $("#Email").rules("remove", 'required');
        }
        else {
            $("#OrderAmount").rules("add", "required");
            $("#Email").rules("add", 'required');
        }
    }

    ShowHideValidationEmailNotifaction(): any {
        if ($("#OrderAmount").val() != "" || $("#Email").val() != "") {
            $('#enableDisable').show();
            $("#EnableToStore").prop('checked', true);
        } else {
            $('#enableDisable').hide();
            $("#EnableToStore").prop('checked', false);
            $("#OrderAmount").rules("remove", 'required');
            $("#Email").rules("remove", 'required');
        }
    }

    ValidateEmailNotifaction(): any {
        if (!$("#EnableToStore").prop('checked')) {
            $('#OrderAmount').val('');
            $('#Email').val('');
        }
        return true;
    }

    PortalProfileUpdateResult(): any {
        $("[update-container-id='ZnodePortalProfile'] #refreshGrid").click();
    }
}


