﻿class Account extends ZnodeBase {
    _endPoint: Endpoint;
    isCatalogValid: boolean;
    catalogId: number;

    constructor() {
        super();
        this._endPoint = new Endpoint();
    }

    Init() {
        $("#ddlUserType").off("change");
        $("#ddlUserType").on("change", function () {
            $("#hdnRoleName").val($("#ddlUserType option:selected").text());
            Account.prototype.ShowHidePermissionDiv();
        });

        $("#ddlUserType").change();
        Account.prototype.ShowHidePermissionDiv();
        Account.prototype.ValidateAccountsCustomer();
        User.prototype.OnPermissionSelection();
        if ($("#ddlPermission").is(':visible')) {
            $("#ddlPermission").change();
        }
        Order.prototype.Init();
        GiftCard.prototype.GetActiveCurrencyToStore("");
        Account.prototype.EditAddress();
        Account.prototype.EditChildAccount();
        Account.prototype.EditAccountCustomerList();
        Account.prototype.BindStates();
        //Account.prototype.AutocompleteCatalog();
    }

    ValidateAccountsCustomer(): any {
        $("#frmCreateEditCustomerAccount").submit(function () {
            return Account.prototype.ValidationForUser();
        });
    }

    ValidateAccountPermissionName(): any {
        $("#AccountPermissionName").on("blur", function () {
            ZnodeBase.prototype.ShowLoader();
            Account.prototype.ValidateExistAccountPermissionName();
            ZnodeBase.prototype.HideLoader();
        });
    }

    ValidateExistAccountPermissionName(): boolean {
        var isValid = true;
        if ($("#AccountPermissionName").val() != '') {
            Endpoint.prototype.IsAccountPermissionExist($("#AccountPermissionName").val(), $("#AccountId").val(), $("#AccountPermissionId").val(), function (response) {
                if (!response) {
                    $("#AccountPermissionName").addClass("input-validation-error");
                    $("#errorSpanAccountPermissionName").addClass("error-msg");
                    $("#errorSpanAccountPermissionName").text(ZnodeBase.prototype.getResourceByKeyName("AlreadyExistAccountPermissionName"));
                    $("#errorSpanAccountPermissionName").show();
                    isValid = false;
                    ZnodeBase.prototype.HideLoader();
                }
            });
        }
        return isValid;
    }

    SubmitCustomerCreateEditForm() {
        return Account.prototype.ValidationForUser();
    }

    DeleteMultipleDepartments(control): any {
        var departmentId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (departmentId.length > 0) {
            Endpoint.prototype.DeleteMultipleDepartments(departmentId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    DeleteMultipleNotes(control): any {
        var noteId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (noteId.length > 0) {
            Endpoint.prototype.DeleteMultipleNotes(noteId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }
    DeleteMultipleAccount(control): any {
        var accountId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountId.length > 0) {
            Endpoint.prototype.DeleteMultipleAccount(accountId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    DeleteAddress(control): any {
        var accountId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountId.length > 0) {
            Endpoint.prototype.DeleteAddress(accountId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    ShowHidePermissionDiv(): any {
        if ($("#hdnRoleName").val() != "User") {
            $("#permissionsToHide").hide();
        }
        else {
            $("#permissionsToHide").show();
        }
    }
    DeleteMultipleAccountCustomer(control): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            Endpoint.prototype.DeleteAccountCustomers(accountIds, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    UnAssociatePriceList(control): any {
        var priceListId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (priceListId.length > 0) {
            Endpoint.prototype.UnAssociatePriceListFromAccount(priceListId, $('#AccountId').val(), function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    EditAssociatedPriceListPrecedence(): any {
        $("#grid tbody tr td").find(".z-edit").on("click",function (e) {
            e.preventDefault();
            var priceListId = parseInt($(this).attr("data-parameter").split('&')[0].split('=')[1]);
            var listName = $(this).attr("data-parameter").split('&')[1].split('=')[1];
            Endpoint.prototype.EditAssociatedPriceListPrecedenceForAccount(priceListId, $('#AccountId').val(), listName, function (res) {
                $("#priceListPrecedence").modal("show");
                $("#priceListPrecedence").html(res);
            });
        });
    }

    EditAssociatedPriceListPrecedenceResult(data: any) {
        $("#priceListPrecedence").modal("hide");
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(data.Message, "success", isFadeOut, fadeOutTime);
        ZnodeBase.prototype.RemovePopupOverlay();

        if ($('#AccountId').val() > 0)
            Account.prototype.AssociatedPriceList();
    }

    AssociatedPriceList() {
        Endpoint.prototype.GetAssociatedPriceListForAccount($("#AccountId").val(), function (response) {
            $("#AssociatedPriceListToAccount").html('');
            $("#AssociatedPriceListToAccount").html(response);
            GridPager.prototype.UpdateHandler();
        });
    }

    AssociatePriceListToAccount() {
        var priceListId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (priceListId.length > 0) {
            Endpoint.prototype.AssociatePriceListForAccount($("#AccountId").val(), priceListId, function (res) {
                $("#DivGetUnAssociatedPriceListForAccount").hide(700);
                window.location.href = window.location.protocol + "//" + window.location.host + "/Account/GetAssociatedPriceListForAccount?accountId=" + $("#AccountId").val();
            });
        }
        else {
            $('#associatedPriceListId').show();
        }
    }

    ValidateForStore(): boolean {
        if ($("#txtPortalName").is(':visible')) {
            if ($("#txtPortalName").val() == "") {
                $("#errorRequiredStore").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorSelectPortal")).addClass("field-validation-error").show();
                $("#txtPortalName").addClass('input-validation-error');
                return false;
            }
        }
        if ($("#RadioSpecific").is(':checked') == true) {
            if ($("#txtCatalogName").val() == "") {
                $("#errorRequiredCatalog").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorSelectCatalog")).addClass("field-validation-error").show();
                $("#txtCatalogName").addClass('input-validation-error');
                return false;
            }
        }

        return true;
    }

    ValidationForUser() {
        var flag: boolean = true;
        var _AllowGlobalLevelUserCreation = $("#AllowGlobalLevelUserCreation").val();
        if (_AllowGlobalLevelUserCreation == "False" && $("#AccountName").val() == "" && $("#hdnPortalId").val() == "") {
            $("#errorRequiredStore").text('').text(ZnodeBase.prototype.getResourceByKeyName("SelectPortal")).addClass("field-validation-error").show();
            $("#txtPortalName").addClass('input-validation-error');
            flag = false;
        }

        if ($("#hdnRoleName").val() == "User") {
            if ($("#BudgetAmount").is(':visible')) {
                if ($("#BudgetAmount").val() == null || $("#BudgetAmount").val() == "") {
                    $("#errorRequiredAccountPermissionAccessId").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorBudgetAmount")).addClass("field-validation-error").show();
                    $("#BudgetAmount").addClass('input-validation-error');
                    flag = false;
                }
            }
            if ($("#ddlApproverList").is(':visible')) {
                if ($("#ddlApproverList").val() == null || $("#ddlApproverList").val() == "") {
                    $("#errorRequiredApprovalUserId").html("<span>" + ZnodeBase.prototype.getResourceByKeyName("SelectApprovalUserId") + "</span>");
                    $("#ddlApproverList").addClass('input-validation-error');
                    flag = false;
                }
            }
        }
        if (!$("#BudgetAmount").is(':visible')) {
            $("#BudgetAmount").val("");
        }
        if ($("#Email").is(':visible') && $("#Email").val() == '') {
            $("#errorRequiredEmail").text('').text(ZnodeBase.prototype.getResourceByKeyName("EmailAddressIsRequired")).removeClass('field-validation-valid').addClass("field-validation-error").show();
            $("#Email").removeClass('valid').addClass('input-validation-error');
            flag = false;
        }
        return flag;
    }

    ParentAccountChange() {
        var parentAccountId = $("#CompanyAccount_ParentAccountId").val();
        if (parentAccountId > 0) {
            Endpoint.prototype.GetAccountsPortal(parentAccountId, function (res) {
                $("#hdnPortalId").val(res.portalId);
                $("#txtPortalName").val(res.storeName);
                $("#hdnStoreName").val(res.storeName);
            });
        }
    }

    EnableCustomerAccount(): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.CustomerEnableDisableAccount($("#AccountId").val(), accountIds, true, function (res) {
                ZnodeBase.prototype.HideLoader();
                DynamicGrid.prototype.RefreshGridOndelete($("#ZnodeAccountsCustomer").find("#refreshGrid"), res);
                if (res.status == true)
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("EnableMessage"), 'success', isFadeOut, fadeOutTime);
                else
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, 'error', isFadeOut, fadeOutTime);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    DisableCustomerAccount(): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.CustomerEnableDisableAccount($("#AccountId").val(), accountIds, false, function (res) {
                ZnodeBase.prototype.HideLoader();
                DynamicGrid.prototype.RefreshGridOndelete($("#ZnodeAccountsCustomer").find("#refreshGrid"), res);
                if (res.status == true)
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("DisableMessage"), 'success', isFadeOut, fadeOutTime);
                else
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, 'error', isFadeOut, fadeOutTime);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    EnableDisableAccount(): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.CustomerEnableDisableAccount($("#AccountId").val(), accountIds, false, function (res) {
                ZnodeBase.prototype.HideLoader();
                DynamicGrid.prototype.RefreshGridOndelete($("#ZnodeAccountsCustomer").find("#refreshGrid"), res);
                if (res.status == true)
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("DisableMessage"), 'success', isFadeOut, fadeOutTime);
                else
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, 'error', isFadeOut, fadeOutTime);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    CustomerResetPassword(): any {
        var accountIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountIds.length > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.CustomerAccountResetPassword($("#AccountId").val(), accountIds, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete($("#ZnodeAccountsCustomer").find("#refreshGrid"), res);
                ZnodeBase.prototype.HideLoader();
                if (res.status == true)
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SuccessResetPassword"), 'success', isFadeOut, fadeOutTime);
                else
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName(res.message), 'error', isFadeOut, fadeOutTime);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    UpdateQuoteStatus(control, statusId): any {
        var quoteIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (quoteIds.length > 0 && statusId > 0) {
            Endpoint.prototype.UpdateQuoteStatus(quoteIds, statusId, function (res) {
                DynamicGrid.prototype.RefreshGrid(control, res);
                DynamicGrid.prototype.ClearCheckboxArray();
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    SetUpdateQuoteUrl() {
        var updatePageType: string = $("#UpdatePageType").val();

        $("#grid tbody tr").each(function () {
            var newhref: string;
            var quoteIdLink: string;
            var orderStatus: string = $(this).find('.z-view').attr("data-parameter").split('&')[1].split('=')[1];

            if ((orderStatus.toUpperCase() == "ORDERED")) {
                var omsQuoteId: number = parseInt($(this).find('.z-view').attr("data-parameter").split('&')[0].split('=')[1]);
                var accountId = $('#UserAccountId').val();

                //New href for z-view.
                newhref = "/Order/Manage?OmsOrderId=" + omsQuoteId + "&accountId=" + accountId + "&updatePageType=" + updatePageType;

                //New href for quoteId link.
                quoteIdLink = "/Order/Manage?OmsOrderId=" + omsQuoteId + "&accountId=" + accountId + "&updatePageType=" + updatePageType;
            }
            else {
                //Append updatePageType to existing href of z-view.
                newhref = $(this).find('.z-view').attr('href') + "&updatePageType=" + updatePageType;

                //Append updatePageType to existing href of quoteId link.
                quoteIdLink = $(this).find('td').eq(1).find('a').attr('href') + "&updatePageType=" + updatePageType;
            }
            $(this).find('.z-view').attr('href', newhref);
            $(this).find('td').eq(1).find('a').attr('href', quoteIdLink);
        });
    }

    DeleteAssociatedProfiles(accountId, control): any {
        var accountProfileIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (accountProfileIds.length > 0) {
            Endpoint.prototype.DeleteAssociatedProfilesForAccounts(accountProfileIds, accountId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    AssociateProfileToAccount(accountId) {
        var profileId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (profileId.length > 0) {
            Endpoint.prototype.AssociateProfileForAccount(accountId, profileId, function (res) {
                $("#DivGetUnAssociatedProfileForAccount").hide(700);
                window.location.href = window.location.protocol + "//" + window.location.host + "/Account/GetAssociatedProfileForAccount?accountId=" + accountId;
            });
        }
        else {
            $('#associatedProfileId').show();
        }
    }

    ValidateOrderStatus(href) {
        if (href.toLowerCase().indexOf("ordered") == -1) {
            $("#hdnOrderURL").val(href);
            $("#btnConvertToOrder").click();
        }
        else
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("OrderAlreadyPlacedForQuote"), 'error', isFadeOut, fadeOutTime);
    }

    ValidateDepartmentNameField(object): any {
        var isValid = true;
        if ($(object).val() == '') {
            $(object).addClass("input-validation-error");
            if ($(object).val() == '')
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("DepartmentNameIsRequired"), 'error', isFadeOut, fadeOutTime);

            isValid = false;
        }
        else {
            $(object).remove("input-validation-error");
            $(object).removeClass("input-validation-error");
            isValid = true;
        }
        return isValid;
    }

    ValidatePrecedanceField(object): any {
        var regex = new RegExp('^\\d{0,}?$');
        var isValid = true;
        if (isNaN($(object).val())) {
            $(object).addClass("input-validation-error");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RequiredNumericValue"), 'error', isFadeOut, fadeOutTime);
            isValid = false;
        }
        else if (!regex.test($(object).val())) {
            $(object).addClass("input-validation-error");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("DisplayOrderRange"), 'error', isFadeOut, fadeOutTime);
            isValid = false;
        }
        else if ($(object).val() == '') {
            $(object).addClass("input-validation-error");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PrecedenceIsRequired"), 'error', isFadeOut, fadeOutTime);
            isValid = false;
        }
        else if ($(object).val() == 0) {
            $(object).addClass("input-validation-error");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("DisplayOrderRange"), 'error', isFadeOut, fadeOutTime);
            isValid = false;
        }
        else {
            $(object).remove("input-validation-error");
            $(object).removeClass("input-validation-error");
            isValid = true;
        }
        return isValid;
    }

    SetDefaultProfile(): any {
        var accountProfileId: string[] = MediaManagerTools.prototype.unique();
        var accountId: number = $('#HdnAccountId').val();
        var profileId: number = parseInt(Store.prototype.GetMultipleValuesOfGridColumn("Profile ID"));

        if (accountProfileId.length == 0)
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        else if (accountProfileId.length > 1)
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAnyOneToSetAsDefault"), 'error', isFadeOut, fadeOutTime);
        else {
            Endpoint.prototype.SetAccountDefaultProfile(accountId, parseInt(accountProfileId.toString()), profileId, function (res) {
                window.location.href = window.location.protocol + "//" + window.location.host + "/Account/GetAssociatedProfileForAccount?accountId=" + accountId;
            });
        }
    }

    RemoveOrderGridIcon(): any {
        Order.prototype.HideGridColumn('IsInRMA');
        $('#grid tbody tr').each(function () {
            if ($(this).find('td.paymentStatus').html().trim().toLocaleLowerCase() == 'cc_captured' || $(this).find('td.paymentStatus').html().trim().toLocaleLowerCase() == 'cc_voided' || $(this).find('td.IsInRMA').find('i').hasClass('z-active')) {
                $(this).find('.z-edit').parents('li').remove();
            }
        });
        $("#listcontainerId").show();
    }

    //This method is used to get portal list on aside panel
    GetPortalList(): any {
        if ($('#ZnodeUserAccountList').length == 1)
            $('#ZnodeUserAccountList').html("");
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Account/GetPortalList', 'divStoreListAsidePanel');
    }

    //This method is used to select portal from list and show it on textbox
    GetPortalDetail(): void {
        $("#grid").find("tr").on("click",function () {
            let portalName: string = $(this).find("td[class='storecolumn']").text();
            let portalId: string = $(this).find("td")[0].innerHTML;
            $('#txtPortalName').val(portalName);
            $('#hdnPortalId').val(portalId);
            $('#PortalId').val(portalId);
            $('#ddlPortal').val(portalId);
            $("#errorRequiredStore").text("").removeClass("field-validation-error").hide();
            $("#txtPortalName").removeClass('input-validation-error');
            Account.prototype.BindParentAccountBasedOnPortalId();
            $('#divStoreListAsidePanel').hide(700);
            ZnodeBase.prototype.RemovePopupOverlay();
        });
    }

    BindParentAccountBasedOnPortalId(): any {
        $("span[data-valmsg-for='PortalId']").html("");
        var portalId = $("#hdnPortalId").val();
        if (portalId == 0 && portalId == "") {
            $('#CompanyAccount_ParentAccountId').children('option').remove();
            return false;
        }
        Endpoint.prototype.GetParentAccountList(portalId, function (response) {
            $('#CompanyAccount_ParentAccountId').children('option').remove();
            for (var i = 0; i < response.length; i++) {
                var opt = new Option(response[i].Text, response[i].Value);
                $('#CompanyAccount_ParentAccountId').append(opt);
            }
        });
    }

    EditAddress(): void {
        $("#AccountAddressList #grid tbody tr td").find(".z-edit").on("click",function (e) {
            e.preventDefault();
            $("#divAddCustomerAddress").html("");
            var href = $(this).attr('href');
            ZnodeBase.prototype.BrowseAsidePoupPanel(href, 'divAddCustomerAddress');
        });
    }

    EditChildAccount(): void {
        $("#subAccountListId #grid tbody tr td").find(".z-edit").on("click",function (e) {
            e.preventDefault();
            $("#divAddCustomerAddress").html("");
            $("#divAddSubAccountPanel").html("");
            var href = $(this).attr('href');
            ZnodeBase.prototype.BrowseAsidePoupPanel(href, 'divAddSubAccountPanel');
        });
    }

    EditAccountCustomerList(): void {
        $("#ZnodeAccountsCustomer #grid tbody tr td").find(".z-edit").on("click",function (e) {
            e.preventDefault();
            $("#divAddCustomerAddress").html("");
            $("#divAddSubAccountPanel").html("");
            $("#divAddCustomerAsidePanel").html("");
            var href = $(this).attr('href');
            ZnodeBase.prototype.BrowseAsidePoupPanel(href, 'divAddCustomerAsidePanel');
        });
    }

    CreateChildAccount(): any {
        $("#divAddSubAccountPanel").html("");
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Account/CreateSubAccount?parentAccountId=' + $('#AccountId').val(), 'divAddSubAccountPanel');
    }

    ValidateUserNameExists(): boolean {
        var isSubmit: boolean = true;
        if ($("#divAddCustomerAsidePanel #UserName").val() != '') {
            Endpoint.prototype.IsUserNameExist($("#divAddCustomerAsidePanel #UserName").val(), $("#PortalId").val(), function (response) {
                if (!response) {
                    $("#UserName").addClass("input-validation-error");
                    $("#errorUserName").addClass("error-msg");
                    $("#errorUserName").text(ZnodeBase.prototype.getResourceByKeyName("AlreadyExistUserName"));
                    $("#errorUserName").show();
                    ZnodeBase.prototype.HideLoader();
                    return false;
                }
                else
                    $("#frmCreateEditCustomerAccount").submit();
            });
        }
        if (Account.prototype.ValidateBudgetAmount() && isSubmit) $("#frmCreateEditCustomerAccount").submit(); else return isSubmit;
    }

    ValidateBudgetAmount(): boolean {
        if ($("#BudgetAmount").is(':visible')) {
            if ($("#BudgetAmount").val() == null || $("#BudgetAmount").val() == "") {
                $("#errorRequiredAccountPermissionAccessId").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorBudgetAmount")).addClass("field-validation-error").show();
                $("#BudgetAmount").addClass('input-validation-error');
                return false;
            }
        }
        return true;
    }

    ValidateChildAccountExists(): boolean {
        var isValid = true;
        if ($("#divAddSubAccountPanel #CompanyAccount_Name").val() != '') {
            Endpoint.prototype.IsAccountNameExist($("#divAddSubAccountPanel #CompanyAccount_Name").val(), $("#divAddSubAccountPanel #CompanyAccount_AccountId").val(), $("#CompanyAccount_PortalId").val(), function (response) {
                if (!response) {
                    $("#CompanyAccount_Name").addClass("input-validation-error");
                    $("#accountNameErrorId").addClass("error-msg");
                    $("#accountNameErrorId").text(ZnodeBase.prototype.getResourceByKeyName("AlreadyExistAccountName"));
                    $("#accountNameErrorId").show();
                    isValid = false;
                    ZnodeBase.prototype.HideLoader();
                }
            });
        }
        if ($("#RadioSpecific").is(':checked') == true) {
            if ($("#txtCatalogName").val().trim() == "" || $("#hdnPublishCatalogId").val().trim() === '') {
                $("#errorRequiredCatalog").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorSelectCatalog")).addClass("field-validation-error").show();
                $("#txtCatalogName").addClass('input-validation-error');
                return false;
            }
        }
        return isValid;
    }

    ValidateAccountPermissionNameField(object): any {
        var isValid = true;
        if ($(object).val() == '') {
            $(object).addClass("input-validation-error");
            if ($(object).val() == '')
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("PermissionNameIsRequired"), 'error', isFadeOut, fadeOutTime);

            isValid = false;
        }
        else {
            $(object).remove("input-validation-error");
            $(object).removeClass("input-validation-error");
            isValid = true;
        }
        return isValid;
    }

    GetPublishCatalogList(control): any {
        $("#divStoreListAsidePanel").html("");
        ZnodeBase.prototype.BrowseAsidePoupPanel('/Account/GetCatalogList', 'divCataloglistPopup');
    }

    RadioChangeEvent(control): any {
        if ($(control).attr("value") == "IsDefault") {
            $("#txtCatalogName").val("");
            $("#hdnPublishCatalogId").val("");
            $("#catalogField").hide();
            $("#errorRequiredCatalog").text('').removeClass("field-validation-error").hide();
            $("#txtCatalogName").removeClass('input-validation-error');
        }
        else {
            $("#catalogField").show();
        }
    }

    ValidateCatalog(): boolean {
        var flag = true;
        if (Account.prototype.isCatalogValid != undefined && !Account.prototype.isCatalogValid) {
            Account.prototype.ShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("InvalidCatalogName"));
            return flag = false;
        }
        else {
            Account.prototype.HideErrorMessage();
        }
        return flag;
    }

    ShowErrorMessage(errorMessage: string = "") {
        $("#txtCatalogName").removeClass("input-validation-valid").addClass("input-validation-error");
        $("#errorRequiredCatalog").text('').text(errorMessage).addClass("field-validation-error").show();
        $("#hdnPublishCatalogId").val(0);
    }

    HideErrorMessage() {
        $("#txtCatalogName").removeClass("input-validation-error").addClass("input-validation-valid");
        $("#errorRequiredCatalog").removeClass("field-validation-error").addClass(" field-validation-valid").hide();

    }

    AutocompleteCatalog() {
        Account.prototype.isCatalogValid = false;
        $("#txtCatalogName").autocomplete({
            source: function (request, response) {
                try {
                    Endpoint.prototype.GetCatalogList(request.term, function (res) {
                        if (res.PortalCatalogs != null) {
                            if (res.PortalCatalogs.length > 0) {
                                var catalogNames = new Array();
                                res.PortalCatalogs.forEach(function (catalogName) {
                                    if (catalogName.CatalogName != undefined)
                                        catalogNames.push(catalogName.CatalogName);
                                });
                                if ($.inArray(request.term, catalogNames) == -1)
                                    Account.prototype.isCatalogValid = false;
                                else
                                    Account.prototype.isCatalogValid = true;
                                response($.map(res.PortalCatalogs, function (item) {
                                    return {
                                        label: item.CatalogName,
                                        catalogId: item.PublishCatalogId,
                                    };
                                }));
                            }
                            else {
                                Account.prototype.isCatalogValid = false;
                                $(".ui-autocomplete").hide();
                                ZnodeBase.prototype.HideLoader();
                            }
                        } else {
                            $("#hdnPublishCatalogId").val('');
                            Account.prototype.isCatalogValid = false;
                        }
                    });
                } catch (err) {
                }
            },
            search: function () {
                $("#hdnPublishCatalogId").val('');
            },
            select: function (event, ui) {
                $("#hdnPublishCatalogId").val(ui.item.catalogId);
                Account.prototype.isCatalogValid = true;
            },
            focus: function (event, ui) {
                $("#hdnPublishCatalogId").val(ui.item.catalogId);
            }

        }).focusout(function () {
            ZnodeBase.prototype.HideLoader();
            let isValid: boolean = Account.prototype.ValidateCatalog();
            if (!isValid || $("#txtCatalogName").val().trim() == '')
                $("#hdnPublishCatalogId").val('');

            return isValid;
        });
    }

    public BindStates(action: string = ""): void {

        let countryCode: string = $('select[name="CountryName"]').val();

        if (countryCode == "" || countryCode == null || typeof countryCode == 'undefined') {
            countryCode = $('select[name="CompanyAccount.Address.CountryName"]').val();
            $('#SelectStateName').attr("name", "CompanyAccount.Address.StateName");
        }
        if (countryCode == "" || countryCode == null || typeof countryCode == 'undefined') {
            countryCode = $('select[name="Address.CountryName"]').val();
            $('#SelectStateName').attr("name", "Address.StateName");
        }

        if (countryCode.toLowerCase() == 'us' || countryCode.toLowerCase() == 'ca') {
            Endpoint.prototype.GetStates(countryCode, function (response) {
                var stateName = $('#SelectStateName');
                stateName.empty();

                $("#StateName").val('');
                $("#StateName").attr("disabled", "disabled");
                $("#dev-statecode-textbox").hide();
                $("#dev-statecode-select").show();

                $.each(response.states, function (key, value) {
                    stateName.append('<option value="' + value.Value + '">' + value.Text + '</option>');
                });

                let code: string = $("#hdn_StateCode").val();

                $("#SelectStateName option").filter(function () {
                    return this.text.toLowerCase() == code.toLowerCase();
                }).attr('selected', true);
            });
        }
        else {
            $("#StateName").prop("disabled", false);
            $("#dev-statecode-textbox").show();
            $("#dev-statecode-select").hide();
        }
    }
}