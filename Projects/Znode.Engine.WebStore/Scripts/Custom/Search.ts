﻿class Search extends ZnodeBase {

    Init() {
        ZSearch.prototype.Init();
        var categoryId: number = parseInt(ZSearch.prototype.GetQueryStringParameterByName("CategoryId"), 10);

        if (categoryId > 0)
        {
            window.sessionStorage.removeItem("lastCategoryId");
            window.sessionStorage.setItem("lastCategoryId", $("#categoryId").val());
            localStorage.setItem("isFromCategoryPage", "true");
        }

        Category.prototype.changeProductViewDisplay();
        Category.prototype.GetCompareProductList()
        Category.prototype.setProductViewDisplay();
    }

    GetFilterResult(item: any, facet: any): any {
        if ($(facet).prop("checked"))
            window.location.href = window.location.protocol + "//" + window.location.host + "/" + item;
        else {
            item = item + "&IsRemove=true";
            window.location.href = window.location.protocol + "//" + window.location.host + "/" + item;
        }
    }

    RemoveFacet(item: any): any {
        window.location.href = window.location.protocol + "//" + window.location.host + "/" + item;        
    }

    ValidateSearch(): boolean {
        $("#btnSearchTerm").on("click", function () {
            var searchTerm = $("input[name=SearchTerm]").val();
            if (searchTerm != null && searchTerm != undefined && searchTerm.trim().length <= 0) {
                return false;
            }
            return true;
        });
        return true;
    }
}