﻿Checkout.prototype.ShowLoader = function (): void {
    $("#Single-loader-content-backdrop").show();
}

Checkout.prototype.HideLoader = function (): void {
    $("#Single-loader-content-backdrop").hide();
}

ZnodeBase.prototype.ShowLoader = function (): void {
    $("#Single-loader-content-backdrop").show();
}

ZnodeBase.prototype.HideLoader = function (): void {
    $("#Single-loader-content-backdrop").hide();
}

Checkout.prototype.AmazonShippingOptions = function (OrderReferenceId, paymentSettingId, total): void {
    $("#loaderId").html(" <div class='loader-inner' style='margin:0 auto;text-align:center;padding:20px;'><img src= '../Content/Images/loading.svg' alt= 'Loading' class='dashboard-loader' /></div>");
    Endpoint.prototype.AmazonShippingOptions(OrderReferenceId, paymentSettingId, total, function (response) {
        $(".shipping-method").html(response);
        $("#loaderId").html("");

    });
}

Checkout.prototype.ShippingOptions = function (): void {
    $("#loaderId").html(" <div class='loader-inner' style='margin:0 auto;text-align:center;padding:20px;'><img src= '../Content/Images/loading.svg' alt= 'Loading' class='dashboard-loader' /></div>");
    Endpoint.prototype.ShippingOptions(true,function (response) {
        $("#loaderId").html("");
        $(".shipping-method").html(response);
        Checkout.prototype.DisableShippingForFreeShippingAndDownloadableProduct();
    });
}

Product.prototype.GetProductBreadCrumb = function (categoryId, isQuickView): void {
    var categoryIds: Array<string> = $("#categoryIds").val().split(",");
    if (isFromCategoryPage == "true" && categoryId > 0) {
        if ($.inArray(categoryId.toString(), categoryIds) > -1) {
            Endpoint.prototype.GetBreadCrumb(categoryId, $("#categoryIds").val(), false, function (response) {
                if (!isQuickView) {
                    response.breadCrumb = response.breadCrumb.replace("<a href='/'>Home</a> /", "<a href='/' class='home-icon pr-5 title='Home''></a>");
                    $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname'>" + $(".product-name").html() + "</span>")
                }
                $("#seeMore").html(response.seeMore)
            })
        }
        else {
            if (!$("#categoryIds").val()) {
                if (!isQuickView) {
                    $("#breadCrumb").html("<a href='/' class='home-icon pr-5 title='Home''></a>" + $(".product-name").html())
                }
                $("#seeMore").html("")
            }
            else {
                Endpoint.prototype.GetBreadCrumb(parseInt(categoryIds[0], 10), $("#categoryIds").val(), false, function (response) {
                    if (!isQuickView) {
                        response.breadCrumb = response.breadCrumb.replace("<a href='/'>Home</a> /", "<a href='/' class='home-icon pr-5 title='Home''></a>");
                        $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname'>" + $(".product-name").html() + "</span>")
                    }
                    $("#seeMore").html(response.seeMore)
                })
            }
        }
    }
    //new tab
    else if (isFromCategoryPage == "true" && (isNaN(categoryId))) {
        Endpoint.prototype.GetBreadCrumb(0, $("#categoryIds").val(), true, function (response) {
            if (!isQuickView) {
                response.breadCrumb = response.breadCrumb.replace("<a href='/'>Home</a> /", "<a href='/' class='home-icon pr-5' title='Home'></a>");
                $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname'>" + $(".product-name").html() + "</span>")
            }
            $("#seeMore").html(response.seeMore)
        })
    }
    else if (isFromCategoryPage != "true") {
        if (!$("#categoryIds").val()) {
            if (!isQuickView) {
                $("#breadCrumb").html("<a href='/' class='home-icon pr-5' title='Home'></a>" + $(".product-name").html())
            }
            $("#seeMore").html("")
        }
        else {
            Endpoint.prototype.GetBreadCrumb(parseInt(categoryIds[0], 10), $("#categoryIds").val(), false, function (response) {
                if (!isQuickView) {
                    response.breadCrumb = response.breadCrumb.replace("<a href='/'>Home</a> /", "<a href='/' class='home-icon pr-5' title='Home'></a>");
                    $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname'>" + $(".product-name").html() + "</span>")
                }
                $("#seeMore").html(response.seeMore)
            })
        }
    }
}

