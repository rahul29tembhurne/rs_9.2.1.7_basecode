﻿class User extends ZnodeBase {
    constructor() {
        super();
    }
    Init() {
        User.prototype.RemoveIconWishlist();
        User.prototype.LoadQuote();
        User.prototype.RestrictEnterButton();
        User.prototype.BottomButtonOnClick();
        User.prototype.BindStates();
    }

    RestrictEnterButton(): void {
        $('#frmUpdateQuoteQuantity').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
    }

    RemoveIconWishlist(): any {
        $("#layout-account-wishlist .wishlist-item-remove a").on("click", function (ev) {
            ev.preventDefault();
            User.prototype.RemoveWishlistItem(this);
        });
    }

    RemoveWishlistItem(el): any {
        var clicked = $(el);
        var wishlistId = clicked.data("id");
        var wishListCount = parseInt($("#wishlistcount").text());

        Endpoint.prototype.RemoveProductFromWishList(wishlistId, function (res) {
            if (res.success) {
                clicked.closest(".wishlist-item").remove();
                $("#wishlistcount").html(res.data.total);
            }
            else {
            }
        });
    }

    UpdateQuoteStatus(control, statusId): any {
        var quoteIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (quoteIds.length > 0 && statusId > 0) {
            ZnodeBase.prototype.ShowLoader();
            Endpoint.prototype.UpdateQuoteStatus(quoteIds, statusId, function (res) {
                DynamicGrid.prototype.RefreshGrid(control, res);
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, res.status ? "success" : "error", isFadeOut, fadeOutTime);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    GenerateInvoice(): any {

        var arrIds = [];
        var collection = CheckBoxCollection;
        if (CheckBoxCollection.length > 0) {
            for (let entry of CheckBoxCollection) {
                arrIds.push(entry.replace("rowcheck_", ""));
            }
        }

        if (arrIds != undefined && arrIds.length > 0) {
            $("#orderIds").val(arrIds);
            setTimeout(function () { ZnodeBase.prototype.HideLoader() }, 1000);
            return true;
        }
        else {
            $("#SuccessMessage").html("");
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneOrder"), "error", false, 0);
            return false;
        }
    }

    LoadQuote(): any {
        $("#btnBottomReview").on('click', function () {
            $("#OrderStatus").val('IN REVIEW');
        });

        $("#btnBottomApprove").on('click', function () {
            $("#OrderStatus").val('APPROVED');
        });

        $("#btnBottomReject").on('click', function () {
            $("#OrderStatus").val('REJECTED');
        });

        $("#btnTopReview").on('click', function () {
            $("#OrderStatus").val('IN REVIEW');
        });

        $("#btnTopApprove").on('click', function () {
            $("#OrderStatus").val('APPROVED');
        });

        $("#btnTopReject").on('click', function () {
            $("#OrderStatus").val('REJECTED');
        });
    }

    UpdateQuoteLineItemQuantity(control): boolean {
        var sku: string = $(control).attr("data-cart-sku")
        var minQuantity: number = parseInt($(control).attr("min-Qty"));
        var maxQuantity: number = parseInt($(control).attr("max-Qty"));
        $("#quantity_error_msg_" + sku).text('');
        var inventoryRoundOff: number = parseInt($(control).attr("data-inventoryRoundOff"));
        var selectedQty: string = $(control).val();
        var decimalPoint: number = 0;

        var decimalValue: number = 0
        if (selectedQty.split(".")[1] != null) {
            decimalPoint = selectedQty.split(".")[1].length;
            decimalValue = parseInt(selectedQty.split(".")[1]);
        }
        if (this.CheckDecimalValue(decimalPoint, decimalValue, inventoryRoundOff, sku)) {
            if (this.CheckIsNumeric(selectedQty, sku)) {
                if (this.CheckMinMaxQuantity(parseInt(selectedQty), minQuantity, maxQuantity, sku)) {
                    $(control).closest("form").submit();
                }
            }
        }
        return false;
    }

    CheckDecimalValue(decimalPoint: number, decimalValue: number, inventoryRoundOff: number, sku: string): boolean {
        if (decimalValue != 0 && decimalPoint > inventoryRoundOff) {
            $("#quantity_error_msg_" + sku).text(ZnodeBase.prototype.getResourceByKeyName("EnterQuantityHaving") + inventoryRoundOff + ZnodeBase.prototype.getResourceByKeyName("XNumbersAfterDecimalPoint"));
            return false;
        }
        return true;
    }

    CheckIsNumeric(selectedQty: string, sku: string): boolean {
        var matches = selectedQty.match(/^-?[\d.]+(?:e-?\d+)?$/);
        if (matches == null) {
            $("#quantity_error_msg_" + sku).text(ZnodeBase.prototype.getResourceByKeyName("RequiredNumericValue"));
            return false;
        }
        return true;
    }
    CheckMinMaxQuantity(selectedQty: number, minQuantity: number, maxQuantity: number, sku: string): boolean {
        if (selectedQty < minQuantity || selectedQty > maxQuantity) {
            $("#quantity_error_msg_" + sku).text(ZnodeBase.prototype.getResourceByKeyName("SelectedQuantityBetween") + minQuantity + ZnodeBase.prototype.getResourceByKeyName("To") + maxQuantity + ZnodeBase.prototype.getResourceByKeyName("FullStop"));
            return false;
        }
        return true;
    }

    DeleteCurrentAddress(): any {
        var url = $("#deleteCurrentAddress").attr('data-url');
        $("#frmEditAddress").attr('action', url);
        $("#frmEditAddress").submit();
    }

    DeleteTemplate(control): any {
        var templateIds = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (templateIds.length > 0) {
            Endpoint.prototype.DeleteTemplate(templateIds, function (res) {
                DynamicGrid.prototype.RefreshGrid(control, res);
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, res.status ? "success" : "error", isFadeOut, fadeOutTime);
            });
        }
        else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectAtleastOneRecord"), 'error', isFadeOut, fadeOutTime);
        }
    }

    ProcessContinueOnClick(): any {
        if (parseInt($("#InventoryOutOfStockCount").val()) == parseInt($("#ShoppingCartItemsCount").val())) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("QuoteItemsOutOfStockErrorMsg"), "error", isFadeOut, 0);
            return false;
        }

        var cartItemcount = $("#CartItemCount").val();
        if (parseInt(cartItemcount) > 0) {
            var omsQuoteLineItemId = $("#omsQuoteLineItemId").val();
            $("#QuoteConfirmPopup").modal('show');
        }
        else {
            User.prototype.ProcessQuote();
        }
    }

    ProcessQuote(): any {
        $("#FormQuoteView").attr('action', "/User/ProcessQuote").submit();
    }

    DeleteQuoteLineItem(): any {
        var omsQuoteLineItemId = $("#OmsQuoteLineItemId").val();
        var omsQuoteId = $("#OmsQuoteId").val();
        var orderStatus = $("#OrderStatus").val();
        var roleName = $("#RoleName").val();
        var token = $('input[name="__RequestVerificationToken"]', $('#FormQuoteView')).val();

        Endpoint.prototype.DeleteQuoteLineItem(omsQuoteLineItemId, omsQuoteId, 1, orderStatus, roleName, token, function (res) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(res.message, res.status ? "success" : "error", isFadeOut, fadeOutTime);
            window.location.href = window.location.protocol + "//" + window.location.host + "/User/QuoteHistory";
        });
    }

    DeleteDraft(): any {
        $("#DraftConfirmPopup").modal('show');
    }

    ValidateCreateEditTemplate(): any {
        var templateName = $("#TemplateName").val();
        var isValid: boolean = true;
        if (!templateName) {
            $("#validTemplateName").html(ZnodeBase.prototype.getResourceByKeyName("RequiredTemplateName"))
            $("#validTemplateName").addClass("error-msg");
            $("#validTemplateName").show();
            isValid = false;
        }
        Endpoint.prototype.IsTemplateNameExist(templateName, $("#OmsTemplateId").val(), function (response) {
            if (!response) {
                $("#validTemplateName").html(ZnodeBase.prototype.getResourceByKeyName("TemplateNameAlreadyExist"))
                $("#validTemplateName").addClass("error-msg");
                $("#validTemplateName").show();
                isValid = false;
            }
        });
        if (isValid)
            $("#frmCreateEditTemplate").submit();
        else
            return false;
    }

    SetManageQuoteUrl(): any {
        $("#grid tbody tr td").find(".zf-view").each(function () {
            var orderStatus = $(this).attr("data-parameter").split('&')[1].split('=')[1];
            var newhref = $(this).attr("href");
            if (newhref.length > 0) {
                if (orderStatus.toLowerCase() == "ordered") {
                    var omsQuoteId = $(this).attr("data-parameter").split('&')[0].split('=')[1];
                    newhref = window.location.protocol + "//" + window.location.host + "/User/OrderReceipt?OmsOrderId=" + omsQuoteId;
                }
                else {
                    newhref = window.location.protocol + "//" + window.location.host + newhref;
                }
            }
            $(this).attr('href', newhref);
        });
    }

    SetQuoteIdLinkURL(): any {
        $("#grid tbody tr .linkQuoteId").each(function () {
            var orderStatus = $(this).children().attr("href").split('&')[1].split('=')[1];
            var newhref = $(this).children().attr("href");
            if (newhref.length > 0) {
                if (orderStatus.toLowerCase() == "ordered") {
                    var omsQuoteId = $(this).children().attr("href").split('&')[0].split('=')[1];
                    newhref = window.location.protocol + "//" + window.location.host + "/User/OrderReceipt?OmsOrderId=" + omsQuoteId;
                }
                else {
                    newhref = window.location.protocol + "//" + window.location.host + newhref;
                }
            }
            $(this).children().attr('href', newhref);
        });
    }

    HideAddressChangeLink(): any {
        $("#FormQuoteView").find('.address-change').hide();
        $("#FormQuoteView").find('.change-address').hide();
    }
    //Saved Credit Card Region
    ShowPaymentOptions(data, CustomerPaymentGUID, isAccount): any {
        Endpoint.prototype.GetPaymentDetails(data, true, function (response) {
            if (!response.HasError) {
                Endpoint.prototype.GetSaveCreditCardCount(response.PaymentApplicationSettingId, CustomerPaymentGUID, function (count) {
                    $("#creditCardCount").html($("#creditCardCount").html().replace("0", count.toString()));
                });
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorContactPaymentApp"), "error", false, 0);
            }
        });
    }

    HideGridColumnForPODocument(): void {
        //Hide PODocument path column

        $('#grid tbody tr').each(function () {
            var podoc = $(this).find('td').last();
            if (podoc.hasClass('z-podocument')) {

                //Get the PoDocument path if exist and create a hyper link to download PODocument.
                var filePath = podoc.text();

                if (filePath != "" && typeof filePath != "undefined") {
                    $(this).find('td').each(function () {
                        if ($(this).hasClass("z-paymenttype")) {
                            if ($(this).text().toLocaleLowerCase() == "purchase_order") {
                                $(this).text("");
                                $(this).append($('<div>').html("<a href='" + podoc.text() + "' target='_blank'>Purchase Order</a>"));
                            }
                        }
                    })
                }
            }
        })
    }

    BottomButtonOnClick(): void {
        $('#btnBottomReject').on('click', function (e) {
            $("#FormQuoteView").submit();
        });

        $('#btnBottomApprove').on('click', function (e) {
            $("#FormQuoteView").submit();
        });

        $('#btnBottomReview').on('click', function (e) {
            $("#FormQuoteView").submit();
        });
    }

    public PrintOrderDetails(e): void {
        var printContents = $("#userorderdetails").html();
        var originalContents = document.body.innerHTML;
        var orderNumber = $("#OrderNumber").val();
        var emailAddress = $("#EmailAddress").val();
        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
        $("#OrderNumber").val(orderNumber);
        $("#EmailAddress").val(emailAddress);
        $.validator.unobtrusive.parse($("#frmOrderDetails"));
    }

    public LoginMethod(): void {
        let actualurl: string = window.location.href;
        //If actual url does not contain return url then only append return url.
        if (actualurl.indexOf("returnUrl") == -1) {
            actualurl = decodeURIComponent(actualurl);

            let returnUrl: string = decodeURIComponent(actualurl.replace(document.location.origin, ''));
            if (returnUrl != "/User/Login")
                window.location.href = window.location.protocol + "//" + window.location.host + '/User/Login?returnUrl=' + returnUrl;
        }
        else
            window.location.href = window.location.protocol + "//" + window.location.host + '/User/Login';
    }

    public AppendLoaderOnSubmit(): void {
        if ($("#login_password").val() != "" && $("#login_username").val() != "") {
            if ($(".field-validation-error").eq(0).html() == "") {
                ZnodeBase.prototype.ShowLoader();
            }
        }
    }

    BindAddressModel(): Znode.Core.AddressModel {
        var stateName = $('#txtStateCode[disabled]').length > 0 ? $("#SelectStateName option:selected").val() : $("#txtStateCode").val();
        var _addressModel: Znode.Core.AddressModel = {
            Address1: $("input[name=Address1]").val(),
            Address2: $("input[name=Address2]").val(),
            AddressId: parseInt($("#AddressId").val()),
            CityName: $("input[name=CityName]").val(),
            FirstName: $("input[name=FirstName]").val(),
            LastName: $("input[name=LastName]").val(),
            PostalCode: $("input[name=PostalCode]").val(),
            StateName: stateName,
            CountryName: $('select[name="CountryName"]').val()

        };
        return _addressModel;
    }

    public SaveChanges(event, id): any {
        event.preventDefault();
        if (id != "" && typeof id != "undefined" && id != null) {
            $("input[name=Address1]").val($("#recommended-address1-" + id + "").text());
            $("input[name=Address2]").val($("#recommended-address2-" + id + "").text());
            $("input[name=CityName]").val($("#recommended-address-city-" + id + "").text());
            $("input[name=PostalCode]").val($("#recommended-address-postalcode-" + id + "").text());
            $("input[name=StateName]").val($("#recommended-address-state-" + id + "").text());
            $("#formChange").val("true");
        }
        $('#custom-modal').modal('hide');
        $("#btnSaveAddress").closest("form").submit();
        return true;
    }

    public RecommendedAddress(): boolean {
        if (!$("#frmEditAddress").valid())
            return false;

        ZnodeBase.prototype.ShowLoader();
        var addressModel = User.prototype.BindAddressModel();
        let isSuggestedAddress: boolean = false;
        Endpoint.prototype.GetRecommendedAddress(addressModel, function (response) {
            var htmlString = response.html;
            if (htmlString != "" && typeof htmlString != "undefined" && htmlString != null) {
                $('#custom-modal').find('#custom-content').empty();
                $('#custom-modal').modal('show').find('#custom-content').append(htmlString);

                $("#user-entered-address").empty();
                let enteredaddress: string = "<div class='address-street'><div id='enteredAddress1'>" + addressModel.Address1 + "</div>";
                if (addressModel.Address2 != "" && typeof addressModel.Address2 != "undefined" && addressModel.Address2 != null) {
                    enteredaddress += "<div id='enteredAddress2'>" + addressModel.Address2 + "</div> ";
                }
                enteredaddress += "<div class='address-citystate'><span id='enteredCity'>" + addressModel.CityName + "</span> <span id='enteredState'>" + addressModel.StateName + "</span> <span id='enteredPostalCode'>" + addressModel.PostalCode + "</span> <div id='enteredCountry'>" + addressModel.CountryName + "</div></div>";

                $("#user-entered-address").append(enteredaddress);
                User.prototype.MatchAddress();
                ZnodeBase.prototype.HideLoader();
                isSuggestedAddress = false;
                $(".address-popup").modal("hide");
            }
            else {
                isSuggestedAddress = true;
            }
        });
        return isSuggestedAddress;
    }

    public MatchAddress(): void {
        for (var i = 1; i < $(".address-details").length; i++) {
            User.prototype.ValidateRecommandedAddress("#enteredAddress1", "#recommended-address1-" + i);
            User.prototype.ValidateRecommandedAddress("#enteredAddress2", "#recommended-address2-" + i);
            User.prototype.ValidateRecommandedAddress("#enteredCity", "#recommended-address-city-" + i);
            User.prototype.ValidateRecommandedAddress("#enteredState", "#recommended-address-state-" + i);
            User.prototype.ValidateRecommandedAddress("#enteredCountry", "#recommended-address-country-" + i);
            User.prototype.ValidateRecommandedAddress("#enteredPostalCode", "#recommended-address-postalcode-" + i);
        }
    }

    public ValidateRecommandedAddress(selector, recommandedAddressSelector): void {
        if (!($(selector).text().trim().toLowerCase() == $(recommandedAddressSelector).text().trim().toLowerCase())) {
            $(recommandedAddressSelector).addClass("address-error");
        }
    }

    public HideShowAddressPopUP(): void {
        $("#AddressError").html("")
        $("#custom-modal").modal("hide");
        $(".address-popup").modal("show");
    }

    public BindStates(): void {
        let countryCode: string = ($('select[name="CountryName"]').val() != undefined) ? $('select[name="CountryName"]').val() : $('select[name="Address.CountryName"]').val();

        if (countryCode.toLowerCase() == 'us' || countryCode.toLowerCase() == 'ca') {
            Endpoint.prototype.GetStates(countryCode, function (response) {
                var stateName = $('#SelectStateName');
                stateName.empty();
                $("#txtStateCode").attr("disabled", "disabled");
                $("#dev-statecode-textbox").hide();
                $("#dev-statecode-select").show();
                $("#txtStateCode").val('');
                $.each(response.states, function (key, value) {
                    stateName.append('<option value="' + value.Value + '">' + value.Text + '</option>');
                });

                let code: string = $("#hdn_StateCode").val();

                $("#SelectStateName option").filter(function () {
                    return this.text.toLowerCase() == code.toLowerCase();
                }).attr('selected', true);
            });
        }
        else {
            $("#txtStateCode").prop("disabled", false);
            $("#dev-statecode-textbox").show();
            $("#dev-statecode-select").hide();
        }
    }
}


$("#btnSaveAddress").on("click", function () {
    var addressId = $("#addressid").val();
    if ($('#asdefault_shipping:checked').length > 0 && (addressId == "" || typeof addressId == 'undefined')) {
        return User.prototype.RecommendedAddress();
    }
});

$('#custom-modal').on('hidden.bs.modal', function () {
    if ($("#custom-modal .close, .popup").length > 1) {
        $('body').addClass('modal-open');
    }
});

$('.address-popup').on('hidden.bs.modal', function () {
    $('body').addClass('modal-open');
});