﻿declare var enabledPaymentProviders;
declare var savedUserCCDetails;
declare var coupons: any;
declare var dataLayer: any;
declare function submitCard(model: any, callBack: any): any;
class Checkout extends ZnodeBase {
    constructor() {
        super();
    }

    Init() {
        Checkout.prototype.SelectShippingOption();
        $("#applyCoupon").submit();
        $("#promocode").removeAttr("style");
        $("#applyGiftCard").submit();
        $("#giftCard").removeAttr("style");
        User.prototype.BindStates();
    }

    SelectShippingOption(): void {
        var shippingId = $("#ShippingId").val();
        if (shippingId != undefined && shippingId > 0) {
            $("input[name='ShippingOptions']").each(function () {
                if ($(this).val() == shippingId) {
                    $(this).prop('checked', 'checked');
                    Checkout.prototype.CalculateShipping("");
                }
            });
        }
    }

    ShippingOptions(): void {
        $("#loaderId").html(" <div class='loader-inner' style='margin:0 auto;text-align:center;padding:20px;'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
        Endpoint.prototype.ShippingOptions(function (response) {
            $("#loaderId").html("");
            $(".shipping-method").html(response);
            Checkout.prototype.DisableShippingForFreeShippingAndDownloadableProduct();
        });
    }

    SubmitOrder(): any {
        var paymentOptionValue = $("input[name='PaymentOptions']:checked").val();
        var shippingOptionValue = $("input[name='ShippingOptions']:checked").val();

        $("#errorAccountNumber").hide();
        $("#errorShippingMethod").hide();

        Checkout.prototype.HidePaymentLoader();
        if (($("#shipping-content .address-name").text().trim() == "")) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RequiredShippingAddress"), "error", false, 0);
        }
        else if (($("#billing-content .address-name").text().trim() == "")) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RequiredBillingAddress"), "error", false, 0);
        }
        else if ((shippingOptionValue == null || shippingOptionValue == "") && ($("#cartFreeShipping").val() != "True" || $("#hdnIsFreeShipping").val() != "True")) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectShippingOption"), "error", isFadeOut, fadeOutTime);
        }
        else if ($("#hndShippingclassName").val() != undefined && $("#hndShippingclassName").val() == Constant.ZnodeCustomerShipping && ($("#AccountNumber").val() == undefined || $("#AccountNumber").val() == "")) {
            $("#errorAccountNumber").show();
        }
        else if ($("#hndShippingclassName").val() != undefined && $("#hndShippingclassName").val() == Constant.ZnodeCustomerShipping && ($("#ShippingMethod").val() == undefined || $("#ShippingMethod").val() == "")) {
            $("#errorShippingMethod").show();
        }
        else if (paymentOptionValue == null || paymentOptionValue == "") {
            if ($("#hdnTotalOrderAmount").val().replace(',', '.') > 0.00) {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectPaymentOption"), "error", false, 0);
            }
            else {
                Checkout.prototype.SubmitCheckOutForm();
            }
        }
        else {
            if (!Checkout.prototype.ShippingErrorMessage()) {
                return false;
            }

            if ($("#dynamic-allowesterritories").length > 0) {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("AllowedTerritories"), "error", false, 0);
                return false;
            }
            var paymentOption: string = $("input[name='PaymentOptions']:checked").attr("id").toLowerCase();
            switch (paymentOption) {
                case "credit_card":
                    Checkout.prototype.SubmitPayment();
                    break;
                case "cod":
                    $("#btnCompleteCheckout").prop("disabled", false);
                    $("#btnCompleteCheckout").show();
                    $('#txtPurchaseOrderNumber').val('');
                    Checkout.prototype.SubmitCheckOutForm();
                    break;
                default:
                    $("#btnCompleteCheckout").prop("disabled", false);
                    $("#btnCompleteCheckout").show();
                    // global data
                    if (Checkout.prototype.CheckValidPODocument()) {
                        Checkout.prototype.SubmitCheckOutForm();
                    }
                    else
                        return false;
            }

        }
    }

    //Get all the selected values required to submit order.
    SetOrderFormData(data): any {
        data["ShippingOptionId"] = $("input[name='ShippingOptions']:checked").val();
        data["PaymentSettingId"] = $("input[name='PaymentOptions']:checked").val();
        data["ShippingAddressId"] = $("#shipping-content").find("#AddressId").val();
        data["BillingAddressId"] = $("#billing-content").find("#AddressId").val();
        data["AdditionalInstruction"] = $("#AdditionalInstruction").val();
        data["PurchaseOrderNumber"] = $("#txtPurchaseOrderNumber").val();
        data["PODocumentName"] = $("#po-document-path").val();
        data["AccountNumber"] = $("#AccountNumber").val();
        data["ShippingMethod"] = $("#ShippingMethod").val();
    }

    ShowLoaderForExistingCustomerLogin(): any {
        if ($("#login_username").val() != "" && $("#login_password").val() != "") {
            $("#loader-content-backdrop-login").show();
        }
    }

    DisableShippingForFreeShippingAndDownloadableProduct(): any {
        if ($("#cartFreeShipping").val() == "True" && $("#hdnIsFreeShipping").val() == "True") {
            $('input[name="ShippingOptions"]').prop('checked', false);
            $('input[name="ShippingOptions"]').next('label').addClass('disable-radio');
            $("#FreeShipping").attr("checked", "checked");
            var form = $("#form0");
            var shippingOptionId = $("#FreeShipping").val();
            var shippingAddressId = $("#shipping-content").find("#AddressId").val();
            var shippingCode = $("#FreeShipping").attr("data-shippingCode")
            $("#hndShippingclassName").val('ZnodeShippingCustom');
            if (shippingOptionId != null || shippingOptionId != undefined || shippingOptionId != "") {
                if (form.attr('action').match("shippingOptionId")) {
                    var url = form.attr('action').split('?')[0];
                    form.attr('action', "")
                    form.attr('action', url);
                }

                form.attr('action', form.attr('action') + "?shippingOptionId=" + shippingOptionId + "&shippingAddressId=" + shippingAddressId + "&shippingCode=" + shippingCode + "");
                form.submit();
            }
            $("#message-freeshipping").show();
        }
        else {
            $('input[name="ShippingOptions"]').next('label').removeClass('disable-radio')
        }
    }

    //Create form to submit order.
    CreateForm(data): any {
        var form = $('<form/></form>');
        form.attr("action", "/Checkout/SubmitOrder");
        form.attr("method", "POST");
        form.attr("style", "display:none;");
        form.attr("enctype", "multipart/form-data");
        Checkout.prototype.AddFormFields(form, data);
        $("body").append(form);
        return form;
    }

    AddFormFields(form, data): any {
        if (data != null) {
            $.each(data, function (name, value) {
                if (value != null) {
                    var input = $("<input></input>").attr("type", "hidden").attr("name", name).val(value);
                    form.append(input);
                }
            });
            if ($("#PODocument") != null && $("#PODocument").val() != "") {
                form.append($("#PODocument"));
            }
        }
    }

    CalculateShipping(ShippingclassName: string): any {
        var form = $("#form0")
        var shippingOptionId = $("input[name='ShippingOptions']:checked").val();
        var shippingAddressId = $("#shipping-content").find("#AddressId").val();
        var shippingCode = $("input[name='ShippingOptions']:checked").attr("data-shippingCode")
        $("#hndShippingclassName").val(ShippingclassName);
        $("#messageBoxContainerId").hide();
        if (ShippingclassName.toLowerCase() == (Constant.ZnodeCustomerShipping).toLowerCase()) {
            $("#customerShippingDiv").show();
        }
        else {
            $("#customerShippingDiv").hide();
        }
        if (shippingOptionId != null || shippingOptionId != "") {
            if (form.attr('action').match("shippingOptionId")) {
                var url = form.attr('action').split('?')[0];
                form.attr('action', "")
                form.attr('action', url);
            }

            form.attr('action', form.attr('action') + "?shippingOptionId=" + shippingOptionId + "&shippingAddressId=" + shippingAddressId + "&shippingCode=" + shippingCode + "");
            form.submit();
        }
    }

    SetUserCreationStatusMessage(response): any {
        var newUrl = window.location.href.replace('?mode=guest', '')
        if (response.hasError) {
            $("#ExistingUserError").html(response.message);
        }
        else {
            window.location.href = newUrl;
        }
    }

    CheckDiscountCodeValue(codeType): any {
        Checkout.prototype.ShowLoader();
        var discountCode = "";
        if (codeType == "coupon") {
            discountCode = $("#promocode").val();
            if (discountCode == null || discountCode == "") {
                $("#promocode").css({ "border": "1px solid red", "background": "rgb(255, 206, 206)" });
                $("#giftCard").removeAttr("style");
                Checkout.prototype.HideLoader();
                return false;
            }
        }
        else if (codeType == "giftcard") {
            discountCode = $("#giftCard").val();
            if (discountCode == null || discountCode == "") {
                $("#giftCard").css({ "border": "1px solid red", "background": "rgb(255, 206, 206)" });
                $("#promocode").removeAttr("style");
                Checkout.prototype.HideLoader();
                return false;
            }
        }
    }

    DisplayAppliedDiscountCode(data): any {
        $(".cart-total").html(data.html);
        if ($('#dynamic-order-total'))
            $('#dynamic-cart-order-total').html($('#dynamic-order-total')[0].innerText);
        var htmlString = "<div class='col-xs-12 nopadding'>";
        if (data.isGiftCard) {
            let msg: string = data.message;
            let isGiftCardApplied: boolean = data.isGiftCardApplied;
            Checkout.prototype.AppendGiftCardMessage(data.message, data.isGiftCardApplied);
        }
        else {
            $("#cartFreeShipping").val(data.freeshipping);
            coupons = data.coupons;
            for (var dataIndex = 0; dataIndex < coupons.length; dataIndex++) {
                var style = coupons[dataIndex].CouponApplied ? "success-msg padding-top" : "error-msg";
                var message = coupons[dataIndex].PromotionMessage;
                var couponCode = coupons[dataIndex].Code;
                Checkout.prototype.RemoveDiscountMessages();
                htmlString = htmlString + "<p class='" + style + "'>" + "<a class='zf-close' onclick='Checkout.prototype.RemoveAppliedCoupon(" + dataIndex + ")' style='cursor:pointer;color:#cc0000;padding-right:3px;' title='Remove Coupon Code'></a>" + "<b>" + couponCode + "</b>" + " - " + message + "</p>";
            }
            htmlString = htmlString + "</div>";
            $("#couponMessageContainer").html("");
            $("#couponMessageContainer").html(htmlString);
        }
        Checkout.prototype.DisablePaymentOnZeroOrderTotal();
        Checkout.prototype.ToggleFreeShipping();
        Checkout.prototype.HideLoader();
    }

    RemoveDiscountMessages(): void {
        if ($("#couponMessageContainer .success-msg") != null) {
            $("couponMessageContainer .success-msg").each(function () { $(this).remove() });
        }

        if ($("couponMessageContainer .error-msg") != null) {
            $("couponMessageContainer .error-msg").each(function () { $(this).remove() });
        }
    }

    DisablePaymentOnZeroOrderTotal(): any {
        if ($("#hdnTotalOrderAmount").val().replace(',', '.') > 0.00) {
            $('input[name="PaymentOptions"]').next('label').removeClass('disable-radio')
        }
        else {
            Checkout.prototype.ShowHidePaymentOption('cod');
            $('input[name="PaymentOptions"]').prop('checked', false);
            $('input[name="PaymentOptions"]').next('label').addClass('disable-radio')
        }
    }

    RemoveAppliedCoupon(couponIndex): any {
        var _code = coupons[couponIndex].Code;
        coupons = new Array();
        Checkout.prototype.RemoveCoupon(_code);
    }

    RemoveCoupon(code: string): void {
        Checkout.prototype.ShowLoader();
        Endpoint.prototype.RemoveCouponCode(code, function (response) {
            Checkout.prototype.DisplayAppliedDiscountCode(response);
            $("#promocode").val("");
            Checkout.prototype.DisablePaymentOnZeroOrderTotal();
        })
    }

    RemoveGiftCard(): any {
        Checkout.prototype.ShowLoader();
        Endpoint.prototype.RemoveGiftCard("", function (response) {
            Checkout.prototype.DisplayAppliedDiscountCode(response);
            $("#giftCard").val("");
            $("#giftCardMessageContainer").html("");
            Checkout.prototype.DisablePaymentOnZeroOrderTotal();
        })
    }

    SetAddressErrorNotificationMessage(data) {
        if (data.status) {
            if (data.error == "")
                location.reload(true);           
            else {
                User.prototype.HideShowAddressPopUP();
                $("#AddressError").html(data.error);
            }
        }
        else {
            location.reload(true);
        }
    }


    ShowPaymentOptions(data): any {
        var paymentId = data;
        if (paymentId != null && paymentId != "" && paymentId != "undefined") {
            $("#messageBoxContainerId").hide();
            var controlId = paymentId.id;
            switch (controlId.toLowerCase()) {
                case "cod":
                    $("#btnCompleteCheckout").show();
                    Checkout.prototype.ShowHidePaymentOption(controlId.toLowerCase());
                    break;
                case "purchase_order":
                    $("#btnCompleteCheckout").show();
                    Checkout.prototype.ShowHidePaymentOption(controlId.toLowerCase());
                    Checkout.prototype.GetPurchaseOrderHtml(controlId.toLowerCase());
                    break;
                case "credit_card":
                    $("#btnCompleteCheckout").show();
                    var Total = $("#Total").val();
                    if (!Checkout.prototype.IsOrderTotalGreaterThanZero(Total)) {
                        return false;
                    }

                    $("#PaymentSettingId").val(paymentId.value);
                    $("#hdnGatwayName").val('');
                    $("#hdnEncryptedTotalAmount").val('');

                    Checkout.prototype.ShowLoader();
                    Endpoint.prototype.GetPaymentDetails(paymentId.value, true, function (response) {
                        if (!response.HasError) {
                            Checkout.prototype.SetPaymentDetails(response);

                            Checkout.prototype.CreditCardPayment(controlId);

                        }
                        Checkout.prototype.HideLoader();
                    });

                    break;
                case "paypal_express":
                    $("#PaymentSettingId").val(paymentId.value);
                    $("#btnCompleteCheckout").hide();
                    Checkout.prototype.ShowHidePaymentOption(controlId.toLowerCase());
                    Checkout.prototype.ShowLoader();
                    Endpoint.prototype.GetPaymentDetails(paymentId.value, false, function (response) {
                        if (!response.HasError) {
                            Checkout.prototype.SetPaymentDetails(response);
                            var Total = $("#Total").val();
                            if (!Checkout.prototype.IsOrderTotalGreaterThanZero(Total)) {
                                return false;
                            }
                        }
                        Checkout.prototype.HideLoader();
                    });
                    break;
                case "amazon_pay":
                    $("#PaymentSettingId").val(paymentId.value);
                    $("#btnCompleteCheckout").hide();
                    Checkout.prototype.ShowHidePaymentOption(controlId.toLowerCase());
                    Checkout.prototype.ShowLoader();
                    Endpoint.prototype.GetPaymentDetails(paymentId.value, false, function (response) {
                        if (!response.HasError) {
                            Checkout.prototype.SetPaymentDetails(response);
                            var Total = $("#Total").val();
                            if (!Checkout.prototype.IsOrderTotalGreaterThanZero(Total)) {
                                $("#payWithAmazonDiv").hide();
                                return false;
                            }
                        }
                        Checkout.prototype.HideLoader();
                    });
                    break;
                default:
                    $("#btnCompleteCheckout").show();
                    Checkout.prototype.ShowHidePaymentOption(controlId.toLowerCase());
                    break;

            }

        }

    }

    ShowHidePaymentOption(option) {
        switch (option) {
            case "cod":
                $("#div-CreditCard").hide();
                $("#paypal-button").hide();
                $("#divpurchase-order").hide();
                $("#payWithAmazonDiv").hide();
                Checkout.prototype.RemoveCreditCardValidationOnPaymentMethodChange();
                break;
            case "purchase_order":
                $("#div-CreditCard").hide();
                $("#paypal-button").hide();
                $("#payWithAmazonDiv").hide();
                Checkout.prototype.RemoveCreditCardValidationOnPaymentMethodChange();
                break;
            case "credit_card":
                $("#div-CreditCard").show();
                $("#paypal-button").hide();
                $("#divpurchase-order").hide();
                $("#payWithAmazonDiv").hide();
                break;
            case "paypal_express":
                $("#div-CreditCard").hide();
                $("#divpurchase-order").hide();
                $("#paypal-button").show();
                $("#payWithAmazonDiv").hide();
                Checkout.prototype.RemoveCreditCardValidationOnPaymentMethodChange();
                break
            case "amazon_pay":
                $("#div-CreditCard").hide();
                $("#divpurchase-order").hide();
                $("#paypal-button").hide();
                $("#payWithAmazonDiv").show();
                Checkout.prototype.RemoveCreditCardValidationOnPaymentMethodChange();
                break
            default:

        }
    }

    SetPaymentDetails(response): any {
        if (!response.HasError) {
            $("#hdnGatwayName").val(response.GatewayName);
            $("#paymentProfileId").val(response.PaymentProfileId);
            $("#hdnPaymentApplicationSettingId").val(response.PaymentApplicationSettingId);
            $("#hdnEncryptedTotalAmount").val(response.Total);
        }
    }

    ClearPaymentAndDisplayMessage(message): any {
        Checkout.prototype.CanclePayment();
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(message, "error", isFadeOut, fadeOutTime);
    }

    CanclePayment(): any {
        Checkout.prototype.HidePaymentProcessDialog();
        $("#div-CreditCard").hide();
        $("#div-CreditCard [data-payment='number']").val('');
        $("#div-CreditCard [data-payment='cvc']").val('');
        $("#div-CreditCard [data-payment='exp-month']").val('');
        $("#div-CreditCard [data-payment='exp-year']").val('');
        $("#div-CreditCard [data-payment='cardholderName']").val('');
        $("#" + $("input[name='PaymentOptions']:checked").attr("id") + "").attr('checked', false);
    }

    SetCreditCardValidations(): any {
        $('input[data-payment="exp-month"]').on("keypress", function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

        $('input[data-payment="exp-month"]').on("focusout", function (e) {
            var monthVal = $('input[data-payment="exp-month"]').val();
            if (monthVal.length == 1 && (monthVal >= 1 || monthVal <= 9)) {
                monthVal = 0 + monthVal;
                $('input[data-payment="exp-month"]').val(monthVal);
            }
        });
        $('input[data-payment="exp-year"]').on("keypress", function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        $('input[data-payment="cvc"]').on("keypress", function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    }

    SubmitPayment(): any {
        var Total = $("#Total").val();
        Total = Total.replace(',', '.');
        if (Checkout.prototype.IsOrderTotalGreaterThanZero(Total)) {
            var isValid = true;
            if (!$("#radioCCList").is(':visible')) {
                $('input[data-payment="number"],input[data-payment="exp-month"],input[data-payment="exp-year"],input[data-payment="cvc"]').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    } else {
                        $(this).css({
                            "border": "1px solid black",
                            "background": ""
                        });
                    }
                });
                isValid = Checkout.prototype.ValidateCreditCardDetails();
            }
            else {
                isValid = Checkout.prototype.ValidateCVV();
            }
            if (isValid == false) {
                //$("#ajaxBusy").dialog('close');
                Checkout.prototype.HidePaymentProcessDialog();
                return false;
            }

            if (isValid) {
                Endpoint.prototype.GetshippingBillingAddress(function (response) {
                    if (!response.Billing.HasError) {
                        if ($("#ajaxProcessPaymentError").html() == undefined) {
                        } else {
                            $("#ajaxProcessPaymentError").html(ZnodeBase.prototype.getResourceByKeyName("ProcessingPayment"));
                        }
                        Checkout.prototype.ShowPaymentProcessDialog();

                        var BillingCity = response.Billing.CityName;
                        var BillingCountryCode = response.Billing.CountryName;
                        var BillingFirstName = response.Billing.FirstName;
                        var BillingLastName = response.Billing.LastName;
                        var BillingPhoneNumber = response.Billing.PhoneNumber;
                        var BillingPostalCode = response.Billing.PostalCode;
                        var BillingStateCode = response.Billing.StateName;
                        if (response.Billing.StateCode != undefined && response.Billing.StateCode != null && response.Billing.StateCode != "") {
                            BillingStateCode = response.Billing.StateCode;
                        }
                        var BillingStreetAddress1 = response.Billing.Address1;
                        var BillingStreetAddress2 = response.Billing.Address2;
                        var BillingEmailId = response.Billing.EmailAddress;

                        var ShippingCity = response.Shipping.CityName;
                        var ShippingCountryCode = response.Shipping.CountryName;
                        var ShippingFirstName = response.Shipping.FirstName;
                        var ShippingLastName = response.Shipping.LastName;
                        var ShippingPhoneNumber = response.Shipping.PhoneNumber;
                        var ShippingPostalCode = response.Shipping.PostalCode;
                        var ShippingStateCode = response.Shipping.StateName;
                        var ShippingStreetAddress1 = response.Shipping.Address1;
                        var ShippingStreetAddress2 = response.Shipping.Address2;

                        var cardNumber = $("#div-CreditCard [data-payment='number']").val();

                        var IsAnonymousUser = $("#hdnAnonymousUser").val() == 0 ? true : false;

                        var guid = $('#GUID').val();

                        var discount = $('#Discount').val();
                        var ShippingCost = $('#ShippingCost').val();

                        var SubTotal = $('#SubTotal').val();

                        var cardType = Checkout.prototype.DetectCardType(cardNumber);

                        if (cardNumber != "") {
                            $("#hdnCreditCardNumber").val(cardNumber.slice(-4));
                        }

                        if ($("#addNewCreditCard-panel").attr("class").indexOf("active") != -1) {
                            if (cardType.toLowerCase() != $("input[name='PaymentProviders']:checked").val().toLowerCase()) {
                                Checkout.prototype.HidePaymentProcessDialog();
                                var message = ZnodeBase.prototype.getResourceByKeyName("SelectedCardType") + $("input[name='PaymentProviders']:checked").val().toLowerCase() + ZnodeBase.prototype.getResourceByKeyName("SelectCardNumberAndCardType");

                                if (message != undefined) {
                                    Checkout.prototype.ShowErrorPaymentDialog(message);
                                }
                                return false;
                            }
                        }

                        var paymentSettingId = $('#PaymentSettingId').val();
                        var PaymentApplicationSettingId = $('#hdnPaymentApplicationSettingId').val();

                        var CustomerPaymentProfileId = $('#CustomerPaymentProfileId').val();
                        var CustomerProfileId = $('#CustomerProfileId').val();
                        var CardDataToken = $('#CardDataToken').val();

                        var gatewayName = $("#hdnGatwayName").val();
                        var paymentAppGatewayName = $("#hdnGatwayName").val().toLowerCase();

                        if (gatewayName.toLowerCase() == 'authorize.net') {
                            paymentAppGatewayName = 'authorizenet';
                        }
                        else if (gatewayName.toLowerCase() == 'chase paymentech') {
                            paymentAppGatewayName = 'paymentech';
                        }
                        else if (gatewayName.toLowerCase() == 'payflow') {
                            if ($("#hdnEncryptedTotalAmount").val() != undefined && $("#hdnEncryptedTotalAmount").val() != null) {
                                Total = $("#hdnEncryptedTotalAmount").val();
                            }
                        }
                        if (Total.indexOf(',') > -1) {
                            Total.replace(',', '');
                        }
                        var payment = {
                            "GUID": guid,
                            "GatewayType": paymentAppGatewayName,
                            "BillingCity": BillingCity,
                            "BillingCountryCode": BillingCountryCode,
                            "BillingFirstName": BillingFirstName,
                            "BillingLastName": BillingLastName,
                            "BillingPhoneNumber": BillingPhoneNumber,
                            "BillingPostalCode": BillingPostalCode,
                            "BillingStateCode": BillingStateCode,
                            "BillingStreetAddress1": BillingStreetAddress1,
                            "BillingStreetAddress2": BillingStreetAddress2,
                            "BillingEmailId": BillingEmailId,
                            "ShippingCost": ShippingCost,
                            "ShippingCity": ShippingCity,
                            "ShippingCountryCode": ShippingCountryCode,
                            "ShippingFirstName": ShippingFirstName,
                            "ShippingLastName": ShippingLastName,
                            "ShippingPhoneNumber": ShippingPhoneNumber,
                            "ShippingPostalCode": ShippingPostalCode,
                            "ShippingStateCode": ShippingStateCode,
                            "ShippingStreetAddress1": ShippingStreetAddress1,
                            "ShippingStreetAddress2": ShippingStreetAddress2,
                            "SubTotal": SubTotal,
                            "Total": Total,
                            "Discount": discount,
                            "PaymentToken": ($("#addNewCreditCard-panel").attr("class").indexOf("active") != -1) ? "" : $("input[name='CCListdetails']:checked").val(),
                            "CardNumber": cardNumber,
                            "CardExpirationMonth": $("#div-CreditCard [data-payment='exp-month']").val(),
                            "CardExpirationYear": $("#div-CreditCard [data-payment='exp-year']").val(),
                            "GatewayCurrencyCode": $('#hdnCurrencySuffix').val(),
                            "CustomerPaymentProfileId": CustomerPaymentProfileId,
                            "CustomerProfileId": CustomerProfileId,
                            "CardDataToken": CardDataToken,
                            "CardType": cardType,
                            "PaymentSettingId": paymentSettingId,
                            "PaymentApplicationSettingId": PaymentApplicationSettingId,
                            "IsAnonymousUser": IsAnonymousUser,
                            "IsSaveCreditCard": $("#SaveCreditCard").is(':checked'),
                            "CardHolderName": $("#div-CreditCard [data-payment='cardholderName']").val(),
                            "CustomerGUID": $("#hdnCustomerGUID").val()
                        };
                        payment["CardSecurityCode"] = payment["PaymentToken"] ? $("[name='SaveCard-CVV']:visible").val() : $("#div-CreditCard [data-payment='cvc']").val();

                        $("#div-CreditCard").hide();

                        submitCard(payment, function (response) {
                            if (response.GatewayResponse == undefined) {
                                if (response.indexOf("Unauthorized") > 0) {
                                    Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorProcessCreditCardPayment") + response + ZnodeBase.prototype.getResourceByKeyName("ContactUsToCompleteOrder"));
                                }
                            } else {
                                var isSuccess = response.GatewayResponse.IsSuccess;
                                if (isSuccess) {
                                    var submitPaymentViewModel = {
                                        PaymentSettingId: $('#PaymentSettingId').val(),
                                        PaymentApplicationSettingId: $('#hdnPaymentApplicationSettingId').val(),
                                        CustomerProfileId: response.GatewayResponse.CustomerProfileId,
                                        CustomerPaymentId: response.GatewayResponse.CustomerPaymentProfileId,
                                        CustomerShippingAddressId: response.GatewayResponse.CustomerShippingAddressId,
                                        CustomerGuid: response.GatewayResponse.CustomerGUID,
                                        PaymentToken: $("input[name='CCdetails']:checked").val(),
                                        ShippingAddressId: $("#shipping-content").find("#AddressId").val(),
                                        BillingAddressId: $("#billing-content").find("#AddressId").val(),
                                        ShippingOptionId: $("input[name='ShippingOptions']:checked").val(),
                                        AdditionalInstruction: $("#AdditionalInstruction").val(),
                                        CreditCardNumber: $("#hdnCreditCardNumber").val(),
                                        CardSecurityCode: payment["CardSecurityCode"],
                                        Total: $("#Total").val(),
                                        SubTotal: $('#SubTotal').val(),
                                        AccountNumber: $("#AccountNumber").val(),
                                        ShippingMethod: $("#ShippingMethod").val(),
                                    };
                                    $.ajax({
                                        type: "POST",
                                        url: "/checkout/submitorder",
                                        async: true,
                                        data: submitPaymentViewModel,
                                        success: function (response) {
                                            if (response.error != null && response.error != "" && response.error != 'undefined') {
                                                Checkout.prototype.ClearPaymentAndDisplayMessage(response.error);
                                                return false;
                                            } else if (response.receiptHTML != null && response.receiptHTML != "" && response.receiptHTML != 'undefined') {
                                                Checkout.prototype.CanclePayment();
                                                //This will focus to the top of screen.
                                                $(this).scrollTop(0);
                                                $('body, html').animate({ scrollTop: 0 }, 'fast');

                                                $("#divSinglePageCheckout").html('');
                                                $("#divSinglePageCheckout").html(response.receiptHTML);
                                                $(".cartcount").html('0');
                                                $("#messageBoxContainerId").hide();
                                                $(".cartAmount").html('');
                                                window.history.pushState("", "", "/Checkout/OrderCheckoutReciept?omsOrderId=" + response.omsOrderId);
                                            }
                                        },
                                        error: function () {
                                            Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorProcessOrder"));
                                            return false;
                                        }
                                    });
                                }
                                else {
                                    var errorMessage = response.GatewayResponse.ResponseText;
                                    if (errorMessage == undefined) {
                                        errorMessage = response.GatewayResponse.GatewayResponseData;
                                    }

                                    if (errorMessage != undefined && errorMessage.toLowerCase().indexOf("missing card data") >= 0) {
                                        Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorOrderPlacementCardDataMissing"));
                                    } else if (errorMessage != undefined && errorMessage.indexOf("Message=") >= 0) {
                                        Checkout.prototype.ClearPaymentAndDisplayMessage(errorMessage.substr(errorMessage.indexOf("=") + 1));
                                        $("#div-CreditCard").show();
                                    } else if (errorMessage != null && errorMessage != undefined && errorMessage.indexOf('customer') > 0) {
                                        Checkout.prototype.ClearPaymentAndDisplayMessage(errorMessage);
                                    } else {
                                        Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorOrderPlacement"));
                                    }
                                }
                            }
                        });
                    }
                });
            }

        }
    }
    //Validate CVV Code
    ValidateCVV(): boolean {
        var cvvNumber: string = $("[name='SaveCard-CVV']:visible").val();
        if (!cvvNumber || cvvNumber.length != 3) {
            $("[name='SaveCard-CVV']:visible").css({
                "border": "1px solid red",
                "background": "#FFCECE"
            });
            $("[name='SaveCard-CVV']:visible").parent().find("span").length <= 0 ?
                $("[name='SaveCard-CVV']:visible").parent().append("<span class='field-validation-error error-cvv' style='margin-left:2%;'>" + ZnodeBase.prototype.getResourceByKeyName("CVVErrorMessage") + "</span>") : "";
            $(window).scrollTop(0);
            $(document).scrollTop(0);
            return false;
        }
        $("[name='SaveCard-CVV']:visible").parent().find("span").hide();
        $("[name='SaveCard-CVV']:visible").removeAttr("style");
        return true;
    }
    IsOrderTotalGreaterThanZero(total): any {
        if (total != "" && total != null && total != 'undefined') {
            total = total.replace(',', '');
        }

        if (total > 0.00) {
            return true;
        } else {
            Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("SelectCODForZeroOrderTotal"));
        }
    }

    Mod10(ccNum): boolean {
        var valid = "0123456789";  // Valid digits in a credit card number
        var len = ccNum.length;  // The length of the submitted cc number
        var iCCN = parseInt(ccNum);  // integer of ccNum
        var sCCN = ccNum.toString();  // string of ccNum

        sCCN = sCCN.replace(/^\s+|\s+$/g, '');  // strip spaces

        var iTotal = 0;  // integer total set at zero
        var bNum = true;  // by default assume it is a number
        var bResult = false;  // by default assume it is NOT a valid cc
        var temp;  // temp variable for parsing string
        var calc;  // used for calculation of each digit

        // Determine if the ccNum is in fact all numbers
        for (var j = 0; j < len; j++) {
            temp = "" + sCCN.substring(j, j + 1);
            if (valid.indexOf(temp) == -1) {
                bNum = false;
            }
        }

        // if it is NOT a number, you can either alert to the fact, or just pass a failure
        if (!bNum) {
            bResult = false;
        }

        // Determine if it is the proper length
        if ((len == 0) && (bResult)) {  // nothing, field is blank AND passed above # check
            bResult = false;
        } else {  // ccNum is a number and the proper length - let's see if it is a valid card number
            if (len >= 15) {  // 15 or 16 for Amex or V/MC
                for (var i = len; i > 0; i--) {  // LOOP throught the digits of the card
                    calc = Math.floor(iCCN) % 10;  // right most digit
                    calc = Math.floor(parseInt(calc));  // assure it is an integer
                    iTotal += calc;  // running total of the card number as we loop - Do Nothing to first digit
                    i--;  // decrement the count - move to the next digit in the card
                    iCCN = iCCN / 10;                               // subtracts right most digit from ccNum
                    calc = Math.floor(iCCN) % 10;    // NEXT right most digit
                    calc = calc * 2;                                       // multiply the digit by two
                    // Instead of some screwy method of converting 16 to a string and then parsing 1 and 6 and then adding them to make 7,
                    // I use a simple switch statement to change the value of calc2 to 7 if 16 is the multiple.
                    switch (calc) {
                        case 10: calc = 1; break;       //5*2=10 & 1+0 = 1
                        case 12: calc = 3; break;       //6*2=12 & 1+2 = 3
                        case 14: calc = 5; break;       //7*2=14 & 1+4 = 5
                        case 16: calc = 7; break;       //8*2=16 & 1+6 = 7
                        case 18: calc = 9; break;       //9*2=18 & 1+8 = 9
                        default: calc = calc;           //4*2= 8 &   8 = 8  -same for all lower numbers
                    }
                    iCCN = iCCN / 10;  // subtracts right most digit from ccNum
                    iTotal += calc;  // running total of the card number as we loop
                }  // END OF LOOP
                if ((iTotal % 10) == 0) {  // check to see if the sum Mod 10 is zero
                    bResult = true;  // This IS (or could be) a valid credit card number.
                } else {
                    bResult = false;  // This could NOT be a valid credit card number
                }
            }
        }
        return bResult; // Return the results
    }

    DetectCardType(number): any {
        var re = {
            electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
            maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
            dankort: /^(5019)\d+$/,
            interpayment: /^(636)\d+$/,
            unionpay: /^(62|88)\d+$/,
            visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
            mastercard: /^5[1-5]\d{14}$|^2(?:2(?:2[1-9]|[3-9]\d)|[3-6]\d\d|7(?:[01]\d|20))\d{12}$/,
            amex: /^3[47][0-9]{13}$/,
            diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
            discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
            jcb: /^(?:2131|1800|35\d{3})\d{11}$/
        };
        if (re.electron.test(number)) {
            return 'ELECTRON';
        } else if (re.maestro.test(number)) {
            return 'MAESTRO';
        } else if (re.dankort.test(number)) {
            return 'DANKORT';
        } else if (re.interpayment.test(number)) {
            return 'INTERPAYMENT';
        } else if (re.unionpay.test(number)) {
            return 'UNIONPAY';
        } else if (re.visa.test(number)) {
            return 'VISA';
        } else if (re.mastercard.test(number)) {
            return 'MASTERCARD';
        } else if (re.amex.test(number)) {
            return 'AMEX';
        } else if (re.diners.test(number)) {
            return 'DINERS';
        } else if (re.discover.test(number)) {
            return 'DISCOVER';
        } else if (re.jcb.test(number)) {
            return 'JCB';
        } else {
            return undefined;
        }
    }


    PayPalPaymentProcess(): any {
        var Total = $("#Total").val();
        var url = [];
        if (Checkout.prototype.IsOrderTotalGreaterThanZero(Total)) {
            Endpoint.prototype.GetPaymentDetails($('#PaymentSettingId').val(), false, function (response) {
                Checkout.prototype.SetPaymentDetails(response);
                if (!response.HasError) {
                    url = Checkout.prototype.PayPalPayment();

                }
            });
        }
        if (url != null)
            return url;

        return false;
    }

    OnClickPayPalExpress(): any {
        var paymentOptionValue = $("input[name='PaymentOptions']:checked").val();
        var shippingOptionValue = $("input[name='ShippingOptions']:checked").val();
        var AccountNumber = $("input[name='AccountNumber']").val();
        var ShippingMethod = $("input[name='ShippingMethod']").val();


        if (($("#shipping-content .address-name").text() == " ")) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RequiredShippingAddress"), "error", false, 0);
        }
        else if (($("#billing-content .address-name").text() == " ")) {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("RequiredBillingAddress"), "error", false, 0);
        }
        else if (shippingOptionValue == null || shippingOptionValue == "") {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectShippingOption"), "error", false, 0);
        }
        else if (paymentOptionValue == null || paymentOptionValue == "") {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectPaymentOption"), "error", false, 0);
        }
        else if ($('#customerShippingDiv').is(':visible')) {
            if ((AccountNumber == null || ShippingMethod == null) || (AccountNumber == "" || ShippingMethod == "")) {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("CustomerShippingError"), "error", true, 5000);
            }
        }
        else {
            if (!Checkout.prototype.ShippingErrorMessage()) {
                return false;
            }
            if ($("#dynamic-allowesterritories").length > 0) {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("AllowedTerritories"), "error", false, 0);
                return false;
            }
            return Checkout.prototype.PayPalPaymentProcess();
        }
    }


    ValidateCreditCardDetails(): any {

        var isValid = true;

        if (!Checkout.prototype.Mod10($('input[data-payment="number"]').val())) {
            isValid = false;
            $('#errornumber').show();
            Checkout.prototype.PaymentError("number");
        }
        else {
            $('#errornumber').hide();
            Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="number"]');
        }

        var currentYear = (new Date).getFullYear();
        var currentMonth = (new Date).getMonth() + 1;

        if ($('input[data-payment="exp-year"]').val() == currentYear && $('input[data-payment="exp-month"]').val() < currentMonth) {
            isValid = false;
            $('#errormonth').show();
            Checkout.prototype.PaymentError("exp-month");
            $('#erroryear').show();
            Checkout.prototype.PaymentError("exp-year");
        }
        else {

            $('#errormonth').hide();
            $('#erroryear').hide();
            Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="exp-year"]');
            Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="exp-month"]');
        }

        if ($('input[data-payment="exp-month"]').val() > 12 || $('input[data-payment="exp-month"]').val() < 1) {
            isValid = false;
            $('#errormonth').show();
            Checkout.prototype.PaymentError("exp-month");
        }
        else {
            $('#errormonth').hide();
            Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="exp-month"]');
        }

        if ($('input[data-payment="exp-year"]').val() < currentYear) {
            isValid = false;
            $('#erroryear').show();
            Checkout.prototype.PaymentError("exp-year");

        } else {
            $('#erroryear').hide();
            Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="exp-year"]');
        }

        if ($('input[data-payment="cvc"]').val() == '') {
            $('#errorcvc').show();
        }
        else {
            $('#errorcvc').hide();
            Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="cvc"]');
        }

        if ($('input[data-payment="cvc"]').val().length < 3) {
            isValid = false;
            $('#errorcardnumber').show();
            Checkout.prototype.PaymentError("cvc");
        }
        else {
            $('#errorcardnumber').hide();
            Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="cvc"]');
        }

        if ($('input[data-payment="cardholderName"]').val().trim() == '' || $('input[data-payment="cardholderName"]').val().trim().length > 100) {
            isValid = false;
            $('#errorcardholderName').show();
            Checkout.prototype.PaymentError("cardholderName");
        }
        else {
            $('#errorcardholderName').hide();
            Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="cardholderName"]');
        }
        if (!isValid) {
            $(window).scrollTop(0);
            $(document).scrollTop(0);
        }
        return isValid;
    }

    RemoveCreditCardValidationCSS(control): any {
        $(control).css('border', '1px solid #c3c3c3');
        $(control).css('background', '');
    }

    PaymentError(control): any {
        $('input[data-payment=' + control + ']').css({
            "border": "1px solid red",
            "background": "#FFCECE"
        });
    }

    RemoveCreditCardValidationOnPaymentMethodChange(): any {
        $('#errornumber').hide();
        Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="number"]');
        $('#errormonth').hide();
        $('#erroryear').hide();
        Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="exp-year"]');
        Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="exp-month"]');
        $('#errorcardnumber').hide();
        Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="cvc"]');
        $('#errorcvc').hide();
    }

    SubmitForApproval(): any {
        var orderStatus = $('#OrderStatus').val();
        var quoteId = $('#QuoteId').val();
        var permissionCode = $('#PermissionCode').val();
        var shippingOptionValue = $("input[name='ShippingOptions']:checked").val();
        var additionalNotes: string = $("#AdditionalInstruction").val();

        if (shippingOptionValue == null || shippingOptionValue == "") {
            Checkout.prototype.DisplaySelectOptionMessage();
            return false;
        }
        Checkout.prototype.ShowLoader();
        if (orderStatus.toUpperCase() == "REJECTED") {
            return Checkout.prototype.CreateQuoteRedirectToReceipt("PENDING APPROVAL", "REJECTED", quoteId, shippingOptionValue, additionalNotes);
        }

        if (orderStatus.toUpperCase() == "DRAFT") {
            return Checkout.prototype.CreateQuoteRedirectToReceipt("PENDING APPROVAL", "DRAFT", quoteId, shippingOptionValue, additionalNotes);
        }

        if (permissionCode.toUpperCase() == "ARA" || permissionCode.toUpperCase() == "SRA") {
            return Checkout.prototype.CreateQuoteRedirectToReceipt("PENDING APPROVAL", null, 0, shippingOptionValue, additionalNotes);
        }
    }

    SubmitForDraft(): any {
        var orderStatus = $('#OrderStatus').val();
        var roleName = $('#RoleName').val();
        var quoteId = $('#QuoteId').val();
        var shippingOptionValue = $("input[name='ShippingOptions']:checked").val();
        var additionalNotes: string = $("#AdditionalInstruction").val();

        if (shippingOptionValue == null || shippingOptionValue == "") {
            return Checkout.prototype.DisplaySelectOptionMessage();
        }
        Checkout.prototype.ShowLoader();
        if (orderStatus.toUpperCase() == "REJECTED" || (orderStatus.toUpperCase() == "IN REVIEW" && roleName.toLowerCase() == "administrator") || (orderStatus.toUpperCase() == "PENDING APPROVAL" && roleName.toLowerCase() == "administrator")) {
            return Checkout.prototype.CreateQuoteRedirectToReceipt("DRAFT", orderStatus, quoteId, shippingOptionValue, additionalNotes);
        }
        else if (orderStatus.toUpperCase() == "APPROVED" || orderStatus.toUpperCase() == "DRAFT") {
            return Checkout.prototype.CreateQuoteRedirectToHistory("DRAFT", orderStatus, quoteId, shippingOptionValue, additionalNotes);
        }
        else {
            return Checkout.prototype.CreateQuoteRedirectToReceipt("DRAFT", null, quoteId, shippingOptionValue, additionalNotes);
        }
    }

    ShippingErrorMessage(): any {
        var shippingErrorMessage = $("#ShippingErrorMessage").val();
        var shippingHasError = $("#ValidShippingSetting").val();
        this.ShowGiftCardMessage();
        Checkout.prototype.HidePaymentLoader();
        if (shippingHasError != null && shippingHasError != "" && shippingHasError != 'undefined' && shippingHasError.toLowerCase() == "false" && shippingErrorMessage != null && shippingErrorMessage != "" && shippingErrorMessage != 'undefined') {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(shippingErrorMessage, "error", false, 0);
            return false;
        }
        else if (shippingErrorMessage != null && shippingErrorMessage != "" && shippingErrorMessage != 'undefined') {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(shippingErrorMessage, "error", false, 0);
            return true;
        }
        Checkout.prototype.DisablePaymentOnZeroOrderTotal();
        Checkout.prototype.ToggleFreeShipping();
        return true;
    }

    DisplaySelectOptionMessage(): any {
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectShippingOption"), "error", false, 0);
        return false;
    }

    CreateQuoteRedirectToReceipt(omsOrderState: any, oldOrderStatus: any, quoteId: any, shippingOptionValue: any, additionalNotes: string): any {
        Endpoint.prototype.CreateQuote(omsOrderState, oldOrderStatus, quoteId, shippingOptionValue, additionalNotes, function (response) {
            window.location.href = window.location.protocol + "//" + window.location.host + "/Checkout/QuoteReceipt?quoteId=" + response.omsQuoteId;
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, response.status ? "success" : "error", isFadeOut, fadeOutTime);
        })
    }

    CreateQuoteRedirectToHistory(omsOrderState: any, oldOrderStatus: any, quoteId: any, shippingOptionValue: any, additionalNotes: string): any {
        Endpoint.prototype.CreateQuote(omsOrderState, oldOrderStatus, quoteId, shippingOptionValue, additionalNotes, function (response) {
            window.location.href = window.location.protocol + "//" + window.location.host + "/User/QuoteHistory";
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, response.status ? "success" : "error", isFadeOut, fadeOutTime);
        })
    }

    GetPurchaseOrderHtml(paymentType: string): any {
        var paymentSettingId: number = $("input[name='PaymentOptions']:checked").val();
        Endpoint.prototype.GetPurchanseOrder(paymentType, paymentSettingId, function (response) {
            $("#payment-provider-content").html(response);
            $('form').removeData('validator');
            $('form').removeData('unobtrusiveValidation');
            $.validator.unobtrusive.parse('form');
        });
    }

    SubmitCheckOutForm(): void {
        var data = {};

        //Get all the selected values required to submit order.
        Checkout.prototype.SetOrderFormData(data);

        //Create form to submit order.
        var form = Checkout.prototype.CreateForm(data);

        // submit form
        form.submit();
        form.remove();
    }

    CheckValidPODocument(): boolean {
        var purchaseOrderNumber: string = $('#txtPurchaseOrderNumber').val()
        if (purchaseOrderNumber != null) {
            if (purchaseOrderNumber.length < 1) {
                $('#txtPurchaseOrderNumber').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
                $('#errorpurchaseorder').show();
                $('#errorpurchaseorder').text(ZnodeBase.prototype.getResourceByKeyName('ErrorRequiredPurchaseOrder'));
                $(window).scrollTop(0);
                $(document).scrollTop(0);
                return false;
            }
            else if (purchaseOrderNumber.length > 50) {
                $('#txtPurchaseOrderNumber').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
                $('#errorpurchaseorder').show();
                $('#errorpurchaseorder').text(ZnodeBase.prototype.getResourceByKeyName('ErrorPurchaseOrderLength'));
                $(window).scrollTop(0);
                $(document).scrollTop(0);
                return false;
            }
            else if ($("#IsPoDocRequire").val() == "True") {
                if ($("#PODocument").val() == null || $("#PODocument").val() == "") {
                    $("#errorFileTypeAndSize").html(ZnodeBase.prototype.getResourceByKeyName("ErrorFileRequired"));
                    $(window).scrollTop(0);
                    $(document).scrollTop(0);
                    return false;
                }
            }
            return true;
        }
        else
            return true;
    }

    public HidePONumberValidateMessage(): void {
        var purchaseOrderNumber: string = $('#txtPurchaseOrderNumber').val()
        if (purchaseOrderNumber != null) {
            if (purchaseOrderNumber.length > 0) {
                $('#errorpurchaseorder').text("");
                $('#txtPurchaseOrderNumber').removeAttr('style');
                $('#errorpurchaseorder').hide();
            }
        }
    }

    ShowGiftCardMessage(): void {
        if ($("#giftCard").val() != undefined && $("#giftCard").val().trim().length > 0
            && $("#cartGiftCardMessage").val() != undefined && $("#cartGiftCardMessage").val().trim().length > 0) {
            let msg: string = $("#cartGiftCardMessage").val();
            let isGiftCardApplied: boolean = $("#cartGiftCardApplied").val();
            this.AppendGiftCardMessage(msg, isGiftCardApplied);
        }
    }

    AppendGiftCardMessage(msg: string, giftCardApplied: any): void {
        let htmlString: any = "<div class='col-xs-12 nopadding'>";
        let style: string = giftCardApplied == true || giftCardApplied == "True" ? "success-msg padding-top" : "error-msg";
        htmlString = htmlString + "<p class='" + style + "'>" + "<a class='zf-close' onclick='Checkout.prototype.RemoveGiftCard();' style='cursor:pointer;color:#cc0000;padding-right:3px;'></a>" + msg + "</p>";
        htmlString = htmlString + "</div>";
        $("#giftCardMessageContainer").html("");
        if (msg != null) {
            $("#giftCardMessageContainer").html(htmlString);
        }
    }

    UploadPODocument(file, callback): any {
        CommonHelper.prototype.GetAjaxHeaders(function (response) {
            var data = new FormData();
            data.append("file", file[0]);
            $.ajax({
                type: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", response.Authorization);
                    xhr.setRequestHeader("Znode-UserId", response.ZnodeAccountId);
                    response.DomainName = response.DomainName.replace(/^https?:\/\//, '');
                    response.DomainName = response.DomainName.replace(/^http?:\/\//, '');
                    xhr.setRequestHeader("Znode-DomainName", response.DomainName);
                    // xhr.setRequestHeader("Token", response.Token);
                },
                url: response.ApiUrl + "/apiupload/uploadpodocument?filePath=~/Data/Media/PODocument",
                contentType: false,
                dataType: "json",
                processData: false,
                data: data,
                success: function (data) {
                    callback(data);
                },
                error: function (error) {
                    var jsonValue = JSON.parse(error.responseText);
                }
            });
        })
    }

    RemovePoDocument(file, callback): any {
        CommonHelper.prototype.GetAjaxHeaders(function (response) {
            var data = new FormData();
            $.ajax({
                type: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", response.Authorization);
                    xhr.setRequestHeader("Znode-UserId", response.ZnodeAccountId);
                    xhr.setRequestHeader("Znode-DomainName", response.DomainName);
                    xhr.setRequestHeader("Token", response.Token);
                },
                url: response.ApiUrl + "/apiupload/removepodocument?filePath=~/Data/Media/PODocument&file=" + file,
                contentType: false,
                dataType: "json",
                data: data,
                processData: false,
                success: function (data) {
                    callback(data);
                },
                error: function (error) {
                    var jsonValue = JSON.parse(error.responseText);
                }
            });
        })
    }

    public ShowPaymentLoader(): void {
        $("#Single-loader-content-backdrop").show();
    }

    public HidePaymentLoader(): void {
        $("#Single-loader-content-backdrop").hide();
    }

    public CreditCardPayment(controlId): boolean {
        if ($("#hdnGatwayName").val() != undefined && $("#hdnGatwayName").val().length > 0) {
            var gatewayName = $("#hdnGatwayName").val();
            var paymentAppGatewayName = $("#hdnGatwayName").val().toLowerCase();
            if (gatewayName.toLowerCase() == 'authorize.net') {
                paymentAppGatewayName = 'authorizenet';
            }

            if (gatewayName.toLowerCase() == 'chase paymentech') {
                paymentAppGatewayName = 'paymentech';
            }
            if (gatewayName.toLowerCase() == "twocheckout") {
                $('#Save-credit-card').hide();
            }
            if (gatewayName.toLowerCase() == "payflow") {
                $('#Save-credit-card').hide();
            }

            var profileId = null;
            if ($("#paymentProfileId").val().length > 0) {
                profileId = $("#paymentProfileId").val();
            }

            var paymentCreditCardModel = {
                gateway: paymentAppGatewayName,
                profileId: profileId,
                paymentSettingId: $('#hdnPaymentApplicationSettingId').val(),
                twoCoUrl: Config.PaymentTwoCoUrl + $('#hdnPaymentApplicationSettingId').val(),
                customerGUID: $("#hdnCustomerGUID").val()
            };
            $.ajax({
                type: "POST",
                url: Config.PaymentScriptUrl,
                data: paymentCreditCardModel,
                success: function (response) {
                    Checkout.prototype.AppendResponseToHTML(response);
                    Checkout.prototype.SetCreditCardValidations();
                    var IsAnonymousUser = $("#hdnAnonymousUser").val() == 0 ? false : true;
                    if ($("#hdnAnonymousUser").val() == "true" || gatewayName.toLowerCase() == "twocheckout" || gatewayName.toLowerCase() == "payflow" || !IsAnonymousUser) {
                        $("#Save-credit-card").hide();
                    }
                    else {
                        $("#Save-credit-card").show();
                    }

                    if (enabledPaymentProviders != '') {
                        var payProvidersHtml = '';
                        var toSplittedPayProviders = enabledPaymentProviders.split(",");
                        for (var iPayProviders = 0; iPayProviders < toSplittedPayProviders.length; iPayProviders++) {
                            payProvidersHtml += "<div class='col-xs-6 col-sm-3 nopadding save-cart styled-input'><input id=radioPaymentProviders" + iPayProviders + " type=radio name=PaymentProviders value=" + toSplittedPayProviders[iPayProviders] + " /><label class='lbl padding-8' for=radioPaymentProviders" + iPayProviders + "><img src=../../Content/images/" + toSplittedPayProviders[iPayProviders] + ".png class='img-responsive' style='float:right;' /></label></div>";
                        }

                        $('#paymentProviders').html("<ul>" + payProvidersHtml + "</ul>");
                        $("#" + $('input[name="PaymentProviders"]')[0].id).prop("checked", true);

                    }
                    if (savedUserCCDetails != '') {
                        $('#radioCCList').show();
                        $('#radioCCList').html('');
                        var iCCOrder = 0;
                        var creditCardHtml = ""
                        $.each(JSON.parse(savedUserCCDetails), function () {
                            creditCardHtml += "<div class='col-sm-12 nopadding styled-input'><input onclick=Checkout.prototype.OnSavedCreditCardClick(" + this['CreditCardLastFourDigit'].split(" ")[3] + "); id=radioSavedCreditCard" + iCCOrder + " type=radio name=CCListdetails value=" + this['PaymentGUID'] + " /><label for=radioSavedCreditCard" + iCCOrder + ">" + this['CreditCardLastFourDigit'] + "</label></div>";
                            iCCOrder++;
                        });
                        $('#radioCCList').append("<div class='col-sm-12 nopadding'>" + creditCardHtml + "</div>");
                        $('#divAddNewCCDetails').show();
                        var savedCCRadio = $("#radioSavedCreditCard0");
                        if (savedCCRadio.length > 0) {
                            savedCCRadio.prop('checked', 'true');
                            savedCCRadio.parent().append(Checkout.prototype.GetCVVHtml())
                            savedCCRadio.click();
                        }
                        Checkout.prototype.BindEvent();

                    }
                    else {
                        $('#divAddNewCCDetails').hide();
                        $('#credit-card-div').show();
                        jQuery('#creditCardTab').hide()
                        jQuery('.single-page-checkout .credit-card-container .section-heading').show();
                        jQuery('#savedCreditCard-panel').removeClass('active')
                        jQuery('#addNewCreditCard-panel').addClass('in active')
                    }

                    Checkout.prototype.ShowHidePaymentOption(controlId.toLowerCase());

                    $("#divOrderSavePage").hide();
                    return false;
                },
                error: function (error) {
                    Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorPaymentApplication"));
                    return false;
                }
            });

        } else {
            Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorPaymentAsNoGatewayAvailable"));
            return false;
        }
    }

    BindEvent(): void {
        $("#radioCCList input[type='radio']").on("change", Checkout.prototype.AppendCVVHtml);
        //restrict inputs
        $(document).on("keypress", 'input[data-payment="cvv"]', function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
        //restrict cut-copy-paste
        $(document).on("cut copy paste", 'input[data-payment="cvv"]', function (e) {
            e.preventDefault();
        });
    }
    //Append CVV Code HTML
    AppendCVVHtml(event): any {
        var currentElement = event.currentTarget;
        $('.error-cvv').hide();
        $('[name=SaveCard-CVV]').hide()
        $(currentElement).parent().find('[name=SaveCard-CVV]').length > 0 ? $(currentElement).parent().find('[name=SaveCard-CVV]').show() : $(currentElement).parent().append(Checkout.prototype.GetCVVHtml());
    }
    //Get CVV Code HTML
    GetCVVHtml(): string {
        return "<input class='form-control' name='SaveCard-CVV' data-payment='cvv' type='password' placeholder='Enter CVV' maxlength='3'  style='width:25%;margin-left:2%;'/>";
    }
    //Append javascript Response to Html.
    private AppendResponseToHTML(response): void {
        //if script element is already present then add html to response
        if ($("#payment-provider-content").find("script").length > 0) {
            $("#payment-provider-content").find("script").html(response);
            return;
        }
        var script: any = document.createElement("script");
        script.innerHTML = response;
        $("#payment-provider-content").append(script);
    }
    public OnSavedCreditCardClick(cardNo): any {
        $("#hdnCreditCardNumber").val(cardNo);
        Checkout.prototype.ClearNewlyAddedCreditCardDetailsOnToggle();
    }
    public PayPalPayment(): any {
        var urlhost = document.location.origin;
        var ShippingOptionId = $("input[name='ShippingOptions']:checked").val();
        var AdditionalInstruction = $("#AdditionalInstruction").val();
        var ShippingAddressId = $("#shipping-content").find("#AddressId").val();
        var BillingAddressId = $("#billing-content").find("#AddressId").val();

        var PaymentSettingId = $('#PaymentSettingId').val();
        var cancelUrl = urlhost + "/checkout/index";
        var returnUrl = urlhost + "/checkout/SubmitPaypalOrder?ShippingAddressId=" + ShippingAddressId + "&BillingAddressId=" + BillingAddressId + "&ShippingOptionId=" + ShippingOptionId + "&AdditionalInstruction=" + AdditionalInstruction + "&PaymentSettingId=" + PaymentSettingId + "";

        var submitPaymentViewModel = {
            PaymentSettingId: PaymentSettingId,
            PaymentApplicationSettingId: $("#hdnPaymentApplicationSettingId").val(),
            ShippingAddressId: ShippingAddressId,
            BillingAddressId: BillingAddressId,
            ShippingOptionId: ShippingOptionId,
            AdditionalInstruction: AdditionalInstruction,
            PayPalReturnUrl: returnUrl,
            PayPalCancelUrl: cancelUrl,
            PaymentType: "PayPalExpress",
            Total: $("#Total").val(),
            SubTotal: $('#SubTotal').val(),
            AccountNumber: $("#AccountNumber").val(),
            ShippingMethod: $("#ShippingMethod").val(),
        };

        var paypalDetails = [];
        $.ajax({
            type: "POST",
            url: "/checkout/submitorder",
            data: submitPaymentViewModel,
            async: false,
            success: function (response) {
                if (response.error != null && response.error != "" && response.error != 'undefined') {
                    Checkout.prototype.ClearPaymentAndDisplayMessage(response.error);
                    $("#paypal-button").hide();
                    return false;
                } else if (response.responseText != null && response.responseText != "" && response.responseText != 'undefined') {
                    $("#paypal-button").hide();

                    if (response.responseText != undefined && response.responseText.indexOf('Message=') >= 0) {
                        var errorMessage = response.responseText.substr(response.responseText.indexOf('=') + 1);
                        Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("SelectCOD"));

                    } else if (response.responseText.indexOf("http") != -1) {
                        paypalDetails.push(response.responseText);
                        paypalDetails.push(response.responseToken)
                    }
                    else {
                        Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorProcessPayment"));
                    }
                }
            },
            error: function () {
                Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorProcessOrder"));
                return false;
            }
        });
        return paypalDetails;
    }

    public ClearNewlyAddedCreditCardDetailsOnToggle(): any {
        $("#CredidCardNumber").val('');
        $("#CredidCardExpMonth").val('');
        $("#CredidCardExpYear").val('');
        $("#CredidCardCVCNumber").val('');
        $("#CardHolderName").val('');
    }

    private ToggleFreeShipping(): void {
        let freeshipping: string = $("#cartFreeShipping").val();
        if (freeshipping != null) {
            if (freeshipping.toLowerCase() == "true") {
                $("#message-freeshipping").show();
            }
            else {
                $("#message-freeshipping").hide();
            }
        }
    }

    public ShowPaymentProcessDialog(): void {
        $("#PaymentModal").modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true
        });
    }
    public ShowAmazonPaymentProcessDialog(): void {
        $("#AmazonPaymentModal").modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true
        });
    }

    public HideAmazonPaymentProcessDialog(): void {
        $(".modal-backdrop").remove();
        $("#AmazonPaymentModal").modal('hide');
        $("body").removeClass("modal-open");
    }

    public HidePaymentProcessDialog(): void {
        $(".modal-backdrop").remove();
        $("#PaymentModal").modal('hide');
        $("body").removeClass("modal-open");
    }

    public ShowErrorPaymentDialog(message): void {
        $("#ErrorPaymentModal").modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true
        }).find('p').html(message);
    }

    public HideChangeAddressLink(): void {
        var roleName: string = $("#RoleName").val();
        var addressCount: number = $("#AddressCount").val();
        if ((roleName.toLowerCase() == "manager" || roleName.toLowerCase() == "user") && addressCount == 1)
            $(".address-change").hide();
    }

    //Disable fields, when Manager/ User changing address in checkout page.
    public DisableFields(): void {
        var roleName: string = $("#RoleName").val();
        if (roleName.toLowerCase() == "manager" || roleName.toLowerCase() == "user") {
            $(".edit-address-form :input:not(:button):not(:checkbox)")
                .attr("readonly", true);

            $('.address_country').attr("disabled", true);
            $('.address_state').attr("disabled", true);
            $("#asdefault_billing").attr('disabled', 'disabled');
            $("#asdefault_shipping").attr('disabled', 'disabled');

            if ($('#AddressId').val() <= 0)
                $("#btnSaveAddress")
                    .attr("disabled", true);
        }
    }

    public ChangeCartReviewSequence(): void {
        if ($("#allPaymentOptionsDiv").length == 0) {
            $('.shopping-cart .title span').text('4');
        }
    }

    public ScrollTop(): void {
        $(window).scrollTop(0);
        $(document).scrollTop(0);
    }

    public ModifyQuertyString(): void {
        var query = window.location.search.substring(1);
    }

    public PutDataIntoDatalayer(Data: any): void {
        dataLayer.push(Data);
    }

    //AmazonPay Methods.

    //Call on change  on shipping and address.
    public CalculateAmazonShipping(ShippingclassName: string, amazonOrderReferenceId: string): any {

        var form = $("#form0")
        var shippingOptionId = $("input[name='ShippingOptions']:checked").val();
        var shippingAddressId = $("#shipping-content").find("#AddressId").val();
        var shippingCode = $("input[name='ShippingOptions']:checked").attr("data-shippingCode")
        $("#hndShippingclassName").val(ShippingclassName);
        var paymentSettingId = $("#hdnPaymentSettingId").val();
        var total = 0;
        $("#messageBoxContainerId").hide();
        if (ShippingclassName.toLowerCase() == (Constant.ZnodeCustomerShipping).toLowerCase()) {
            $("#customerShippingDiv").show();
        }
        else {
            $("#customerShippingDiv").hide();
        }
        if (shippingOptionId == null || shippingOptionId != undefined || shippingOptionId != "") {
            //shippingOptionId = 0;   //commented because shipping charges not applied on shopping cart.
            if (form.attr('action') != undefined && form.attr('action').match("shippingOptionId")) {
                var url = form.attr('action').split('?')[0];
                form.attr('action', "")
                form.attr('action', url);
            }
            form.attr('action', form.attr('action') + "?shippingOptionId=" + shippingOptionId + "&shippingAddressId=" + shippingAddressId + "&shippingCode=" + shippingCode + "&amazonOrderReferenceId=" + $("#hdnOrderReferenceId").val() + "&paymentSettingId=" + paymentSettingId + "&total=" + total + "");
            form.submit();
        }
    }

    //Load shipping option for amazon pay checkout page.
    public AmazonShippingOptions(OrderReferenceId: string, paymentSettingId: number, total: string): void {
        $("#loaderId").html(" <div class='loader-inner' style='margin:0 auto;text-align:center;padding:20px;'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
        Endpoint.prototype.AmazonShippingOptions(OrderReferenceId, paymentSettingId, total, function (response) {
            $(".shipping-method").html(response);
            $("#loaderId").html("");
        });
    }

    //Process amazon payment.
    public AmazonPayProcess(total, paymentSettingId, paymentApplicationSettingId): any {
        var url = [];
        var shippingOptionValue = $("input[name='ShippingOptions']:checked").val();
        if (shippingOptionValue == null || shippingOptionValue == "") {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SelectShippingOption"), "error", isFadeOut, fadeOutTime);
            return false;
        }
        if (Checkout.prototype.IsOrderTotalGreaterThanZero(total)) {
            Endpoint.prototype.GetPaymentDetails(paymentSettingId, false, function (response) {
                Checkout.prototype.SetPaymentDetails(response);
                if (!response.HasError) {
                    $("#ajaxProcessPaymentError").html(ZnodeBase.prototype.getResourceByKeyName("ProcessingPayment"));
                    Checkout.prototype.ShowAmazonPaymentProcessDialog();
                    url = Checkout.prototype.AmazonPayPayment(paymentSettingId, paymentApplicationSettingId);
                }
            });
        }
        if (url != null)
            return url;
        Checkout.prototype.HidePaymentProcessDialog();

        return false;
    }

    //Call :Amazon Pay
    public AmazonPayPayment(paymentSettingId, paymentApplicationSettingId): any {

        var urlhost = document.location.origin;
        var ShippingOptionId = $("input[name='ShippingOptions']:checked").val();
        var AdditionalInstruction = $("#AdditionalInstruction").val() == undefined ? "" : $("#AdditionalInstruction").val();
        var ShippingAddressId = $("#shipping-content").find("#AddressId").val();
        var BillingAddressId = $("#billing-content").find("#AddressId").val();
        var PaymentType: "AmazonPay";
        var PaymentSettingId = paymentSettingId;
        var amazonOrderReferenceId = $("#hdnOrderReferenceId").val();
        var cancelUrl = urlhost + "/checkout/index";
        var returnUrl = urlhost + "/checkout/SubmitAmazonOrder?amazonOrderReferenceId=" + amazonOrderReferenceId + "&PaymentType=AmazonPay&ShippingOptionId=" + ShippingOptionId + "&PaymentSettingId=" + PaymentSettingId + "&PaymentApplicationSettingId=" + paymentApplicationSettingId + "&AdditionalInstruction=" + AdditionalInstruction + "";

        var submitPaymentViewModel = {
            PaymentSettingId: PaymentSettingId,
            PaymentApplicationSettingId: paymentApplicationSettingId,
            ShippingAddressId: ShippingAddressId,
            BillingAddressId: BillingAddressId,
            ShippingOptionId: ShippingOptionId,
            AdditionalInstruction: AdditionalInstruction,
            AmazonPayReturnUrl: returnUrl,
            AmazonPayCancelUrl: cancelUrl,
            AmazonOrderReferenceId: amazonOrderReferenceId,
            PaymentType: "AmazonPay",
            Total: $("#Total").val(),
            SubTotal: $('#SubTotal').val(),
            AccountNumber: $("#AccountNumber").val(),
            ShippingMethod: $("#ShippingMethod").val(),
        };
        var amazonPayDetails = [];
        $.ajax({
            type: "POST",
            url: "/checkout/submitorder",
            data: submitPaymentViewModel,
            async: false,
            success: function (response) {
                if (response.error != null && response.error != "" && response.error != 'undefined') {
                    Checkout.prototype.ClearPaymentAndDisplayMessage(response.error);
                    $("#paypal-button").hide();
                    return false;
                }
                else if (response.responseText != null && response.responseText != "" && response.responseText != 'undefined') {
                    $("#paypal-button").hide();

                    if (response.responseText != undefined && response.responseText.indexOf('Message=') >= 0) {
                        var errorMessage = response.responseText.substr(response.responseText.indexOf('=') + 1);
                        Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("SelectCOD"));

                    } else if (response.responseText == "True") {
                        window.location.href = returnUrl + "&captureId=" + response.responseToken;
                    }
                    else {
                        Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorProcessPayment"));
                    }
                }
            },
            error: function () {
                Checkout.prototype.ClearPaymentAndDisplayMessage(ZnodeBase.prototype.getResourceByKeyName("ErrorProcessOrder"));
                Checkout.prototype.HideAmazonPaymentProcessDialog();
                return false;
            }
        });
        return amazonPayDetails;
    }

    //End Amazon Pay Methods.   
}

$("#CredidCardNumber").on("blur", function (event) {
    let CredidCardNumber: string = $('input[data-payment="number"]').val();
    if (!Checkout.prototype.Mod10(CredidCardNumber) && CredidCardNumber != "") {
        $('#errornumber').show();
        Checkout.prototype.PaymentError("number");
        Checkout.prototype.ScrollTop();
        return false;
    }
    else {
        $('#errornumber').hide();
        Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="number"]');
    }
});

$("#CredidCardExpMonth").on("blur", function (event) {

    var currentMonth = (new Date).getMonth() + 1;
    if (($('input[data-payment="exp-month"]').val() > 12 || $('input[data-payment="exp-month"]').val() < 1) && $('input[data-payment="exp-month"]').val() != "") {
        $('#errormonth').show();
        Checkout.prototype.PaymentError("exp-month");
        Checkout.prototype.ScrollTop();
        return false;
    }
    else {
        $('#errormonth').hide();
        Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="exp-month"]');
    }
});

$("#CredidCardExpYear").on("blur", function (event) {
    var currentYear = (new Date).getFullYear();
    var currentMonth = (new Date).getMonth() + 1;

    if ($('input[data-payment="exp-year"]').val() == currentYear && $('input[data-payment="exp-month"]').val() < currentMonth) {
        $('#errormonth').show();
        Checkout.prototype.PaymentError("exp-month");
        $('#erroryear').show();
        Checkout.prototype.PaymentError("exp-year");
        Checkout.prototype.ScrollTop();
        return false;
    }
    else {
        $('#errormonth').hide();
        $('#erroryear').hide();
        Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="exp-year"]');
        Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="exp-month"]');
    }
    $("#CredidCardExpMonth").trigger('blur');
    if ($('input[data-payment="exp-year"]').val() < currentYear && $('input[data-payment="exp-year"]').val() != "") {
        $('#erroryear').show();
        Checkout.prototype.PaymentError("exp-year");
        Checkout.prototype.ScrollTop();
        return false;
    } else {
        $('#erroryear').hide();
        Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="exp-year"]');
    }
});

$("#CredidCardCVCNumber").on("blur", function (event) {
    if ($('input[data-payment="cvc"]').val().length < 3 && $('input[data-payment="cvc"]').val() != "") {
        $('#errorcvc').show();
        Checkout.prototype.PaymentError("cvc");
        Checkout.prototype.ScrollTop();
        return false;
    }
    else {
        $('#errorcvc').hide();
        Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="cvc"]');
    }
});

$("#CardHolderName").on("blur", function (event) {
    if ($('input[data-payment="cardholderName"]').val().trim() == "") {
        $('#errorcardholderName').show();
        Checkout.prototype.PaymentError("cardholderName");
        Checkout.prototype.ScrollTop();
        return false;
    }
    else {
        $('#errorcardholderName').hide();
        Checkout.prototype.RemoveCreditCardValidationCSS('input[data-payment="cardholderName"]');
    }
});