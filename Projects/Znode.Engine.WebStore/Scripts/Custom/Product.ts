﻿var isFromCategoryPage: string;
var isAddToCartGroupProduct: boolean = true;

class Product extends ZnodeBase {
    constructor() {
        super();
    }
    Init() {
        isFromCategoryPage = localStorage.getItem("isFromCategoryPage");
        Product.prototype.GetProductBreadCrumb(parseInt(window.sessionStorage.getItem("lastCategoryId"), 10), false);
        Product.prototype.ActiveReadReviews();
    }

    GetProductDetails(control): any {
        $('#quick-view-content').html("<span style='position:absolute;top:0;bottom:0;left:0;right:0;text-align:center;transform:translate(0px, 45%);font-weight:600;'>Loading...</span>");
        var productId = control.dataset.value;
        var isQuickView = control.dataset.isquickview;
        Endpoint.prototype.GetProductDetails(productId, isQuickView, function (res) {
            if (res != null && res != "") {
                $("#quick-view-content").html(res);
                isFromCategoryPage = localStorage.getItem("isFromCategoryPage");
                Product.prototype.GetProductBreadCrumb(parseInt(window.sessionStorage.getItem("lastCategoryId"), 10), true);
            }
        });
    }

    GetProductOutOfStockDetails(control, e): any {
        e.preventDefault();
        var productId = $(control).parent().find("#dynamic-productid").val();
        Endpoint.prototype.GetProductOutOfStockDetails(productId, function (response) {
            if (response.status) {
                Product.prototype.ShowHideWishlistErrorMsg(control, false);
                $(control).closest("form").submit();
            }
            else {
                Product.prototype.ShowHideWishlistErrorMsg(control, true);
                return false;
            }
        });
    }
    //if true then show wishlist error message otherwise hide error message
    ShowHideWishlistErrorMsg(control, isShow): any {
        var errorMessageField = $(control).parent().parent().find("#wishlist-error-msg");
        errorMessageField.text("");
        isShow ? errorMessageField.text(ZnodeBase.prototype.getResourceByKeyName("WishlistProductunavailable")) : "";
        isShow ? errorMessageField.addClass("error-msg") : errorMessageField.removeClass("error-msg");
        isShow ? $(control).prop("disabled", true) : $(control).prop("disabled", false);
    }

    AddToWishList(control): any {
        var sku = $(control).attr("data-sku");
        Endpoint.prototype.AddToWishList(sku, function (res) {
            if (res.status) {
                $("#accountWishList").attr('href', res.link);
                $("#accountWishList").attr('class', res.style);
                $("#accountWishList").html(res.message);
                $("#accountWishList_" + control.dataset.id).attr('href', res.link);
                $("#accountWishList_" + control.dataset.id).attr('class', res.style);
                $("#accountWishList_" + control.dataset.id).text(res.message);
            } else {
                if (res.isRedirectToLogin) {
                    // document.location = res.link;
                    document.location.href = res.link;
                }
            }
        });
    }

    OnQuantityChange(): boolean {
        var flag = true;
        var cartCount = 0;
        $("#quantity-error-msg").text('');
        var productId: number = parseInt($("#scrollReview form").children("#dynamic-productid").val());
        var _productDetail = Product.prototype.BindProductModelData();
        //Getting cart count data for the product to perform validation while adding more product quantity in cart.
        Endpoint.prototype.GetCartCountByProductId(productId, function (response) {
            cartCount = parseInt(response);
        });

        cartCount = cartCount + parseInt(_productDetail.Quantity);
        if (this.CheckIsNumeric(_productDetail.Quantity, productId, _productDetail.QuantityError)) {
            if (this.CheckDecimalValue(_productDetail.DecimalPoint, _productDetail.DecimalValue, _productDetail.InventoryRoundOff, productId, _productDetail.QuantityError)) {
                if (this.CheckQuantityGreaterThanZero(_productDetail.MaxQuantity, _productDetail.MinQuantity, cartCount, productId, _productDetail.QuantityError)) {
                    flag = false;
                    $("#button-addtocart_" + productId).prop("disabled", false);

                    Product.prototype.UpdateProductVariations(false, _productDetail.SKU, _productDetail.MainProductSKU, _productDetail.Quantity, _productDetail.MainProductId, function (response) {
                        var salesPrice = response.data.price;
                        flag = Product.prototype.UpdateProductValues(response, _productDetail.Quantity);
                        if (flag == true) {
                            flag = Product.prototype.InventoryStatus(response);
                        }
                    });
                }
                else {
                    flag = false;
                }
            }
            else {
                flag = false;
            }
        }
        else {
            flag = false;
        }
        return flag;
    }

    OnAssociatedProductQuantityChange(): boolean {
        $("#QuickViewQuantiyErrorMessage").text('');
        $("#dynamic-product-variations .quantity").each(function () {
            var productId: number = parseInt($("#scrollReview form").children("#dynamic-productid").val());
            var _productDetail = Product.prototype.BindProductModel(this, true);
            var _showAddToCart = $("#ShowAddToCart").val();
            if (_productDetail.Quantity != null && _productDetail.Quantity != "") {
                if (Product.prototype.CheckIsNumeric(_productDetail.Quantity, productId, _productDetail.QuantityError)) {
                    if (Product.prototype.CheckDecimalValue(_productDetail.DecimalPoint, _productDetail.DecimalValue, _productDetail.InventoryRoundOff, productId, _productDetail.QuantityError)) {
                        if (Product.prototype.CheckQuantityGreaterThanZero(_productDetail.MaxQuantity, _productDetail.MinQuantity, parseInt(_productDetail.Quantity), productId, _productDetail.QuantityError)) {
                            if (_showAddToCart != "False") {
                                $("#button-addtocart_" + productId).prop("disabled", false);
                            }
                            $(_productDetail.QuantityError).text("");
                            $(_productDetail.QuantityError).removeClass("error-msg");
                            isAddToCartGroupProduct = true;
                            return true;
                        }
                        else {
                            isAddToCartGroupProduct = false;
                        }
                    }
                    else {
                        isAddToCartGroupProduct = false;
                    }
                }
                else {
                    isAddToCartGroupProduct = false;
                }
            }
            else {
                if (_showAddToCart != "False") {
                    $("#button-addtocart_" + productId).prop("disabled", false);
                }
                $(_productDetail.QuantityError).text("");
                $(_productDetail.QuantityError).removeClass("error-msg");
                isAddToCartGroupProduct = true;
                return true;
            }
            isAddToCartGroupProduct = false;
            return false;
        });
        return true;
    }

    BindProductModel(control, isGroup: boolean): Znode.Core.ProductDetailModel {
        var _productDetail: Znode.Core.ProductDetailModel = {
            MainProductId: parseInt($(control).attr("data-parentProductId")),
            InventoryRoundOff: parseInt($(control).attr("data-inventoryroundoff")),
            ProductId: parseInt($(control).attr('data-productId')),
            Quantity: $(control).val(),
            MaxQuantity: parseInt($(control).attr("data-maxquantity")),
            MinQuantity: parseInt($("#Quantity").attr("data-minquantity")),
            SKU: $(control).attr("data-sku"),
            MainProductSKU: $(control).attr("data-parentsku"),
            DecimalPoint: $(control).val().split(".")[1] != null ? $(control).val().split(".")[1].length : 0,
            DecimalValue: $(control).val().split(".")[1] != null ? $(control).val().split(".")[1] : 0,
            QuantityError: isGroup ? "#quantity-error-msg_" + $(control).attr('data-productId') : "#quantity-error-msg"
        };
        return _productDetail;
    }

    BindProductModelData(): Znode.Core.ProductDetailModel {
        var isGroup = false;
        var _productDetail: Znode.Core.ProductDetailModel = {
            MainProductId: parseInt($("#Quantity").attr("data-parentProductId")),
            InventoryRoundOff: parseInt($("#Quantity").attr("data-inventoryroundoff")),
            ProductId: parseInt($("#Quantity").attr('data-productId')),
            Quantity: $("#Quantity").val(),
            MaxQuantity: parseInt($("#Quantity").attr("data-maxquantity")),
            MinQuantity: parseInt($("#Quantity").attr("data-minquantity")),
            SKU: $("#Quantity").attr("data-sku"),
            MainProductSKU: $("#Quantity").attr("data-parentsku"),
            DecimalPoint: $("#Quantity").val().split(".")[1] != null ? $("#Quantity").val().split(".")[1].length : 0,
            DecimalValue: $("#Quantity").val().split(".")[1] != null ? $("#Quantity").val().split(".")[1] : 0,
            QuantityError: isGroup ? "#quantity-error-msg_" + $("#Quantity").attr('data-productId') : "#quantity-error-msg"
        };
        return _productDetail;
    }

    CheckDecimalValue(decimalPoint: number, decimalValue: number, inventoryRoundOff: number, productId: number, quantityError: string): boolean {
        if (decimalValue != 0 && decimalPoint > inventoryRoundOff) {
            $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("EnterQuantityHaving") + inventoryRoundOff + ZnodeBase.prototype.getResourceByKeyName("XNumbersAfterDecimalPoint"));
            $(quantityError).addClass("error-msg");
            return false;
        }
        return true;
    }

    CheckIsNumeric(selectedQty: string, productId: number, quantityError: string): boolean {
        var matches = selectedQty.match(/^-?[\d.]+(?:e-?\d+)?$/);
        if (matches == null) {
            $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("RequiredNumericValue"));
            $(quantityError).addClass("error-msg");
            return false;
        }
        return true;
    }

    CheckQuantityGreaterThanZero(maxQuantity: number, minQuantity: number, selectedQty: number, productId: number, quantityError: string): boolean {
        if (selectedQty == 0) {
            $("#dynamic-inventory").text("");
            $(quantityError).addClass("error-msg");
            $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("ErrorProductQuantity"));
            return false;
        }
        if (maxQuantity < selectedQty || minQuantity > selectedQty) {
            $("#dynamic-inventory").text("");
            $(quantityError).addClass("error-msg");
            $(quantityError).text(ZnodeBase.prototype.getResourceByKeyName("SelectedQuantityBetween") + minQuantity + " to" + maxQuantity);
            return false;
        }
        return true;
    }

    InventoryStatus(response): boolean {
        // In stock
        var invetoryMessge: string;
        if (response.Quantity && response.Quantity > 0) {
            invetoryMessge = response.message + '<span class="product-count padding-left-5">(' + response.Quantity + ')</span>';
        } else {
            invetoryMessge = response.message;
        }

        if (response.success) {
            $("#dynamic-inventory").show().html(invetoryMessge);
            $("#button-addtocart_" + response.data.productId).prop("disabled", false);
            $("#product-details-quantity input[name='Quantity']").attr('data-change', 'false');
            return true;
        } else {
            $("#dynamic-inventory").show().html(invetoryMessge);
            return false;
        }
    }

    RefreshPrice(amount): void {
        $("#product_Detail_Price_Div").show();
        $("#layout-product .dynamic-product-price").html(amount);
    }

    OnAddonSelect(control: any): void {
        var _productSKU: Znode.Core.AddOnDetails;
        _productSKU = Product.prototype.GetGroupProductSKUQuantity(control);
        if (_productSKU != null && _productSKU.SKU != null && _productSKU.Quantity != null) {
            Product.prototype.UpdateProductVariations(false, _productSKU.SKU, _productSKU.ParentSKU, _productSKU.Quantity, _productSKU.ParentProductId, function (response) {
                var salesPrice = response.data.price;
                Product.prototype.UpdateProductValues(response, _productSKU.Quantity);
                Product.prototype.RefreshPrice(salesPrice);
                Product.prototype.InventoryStatus(response);
                Product.prototype.RemoveAddonRequired(control);
            });
        }
        else {
            if ($("#quick-view-popup-ipad").is(":visible")) {
                $("#QuickViewQuantiyErrorMessage").html(ZnodeBase.prototype.getResourceByKeyName("EnterQuantityError"));
            } else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("EnterQuantityError"), "error", true, fadeOutTime);
            }
            $("#dynamic-inventory").show().text('');
            $(".AddOn").val('0');
        }
    }

    RemoveAddonRequired(control): void {
        var addOnId = $(control).data("addongroupid");
        var errormsgdivid = $(control).data("errormsgdivid");
        $("#" + errormsgdivid + addOnId).css("display", "none");
    }

    GetGroupProductSKUQuantity(control): Znode.Core.AddOnDetails {
        var _productSKU: Znode.Core.AddOnDetails
        $("input[type=text].quantity").each(function () {
            if ($(this).attr("name") != "Quantity") {
                var groupProductQuantity = $(this).val();
                if (groupProductQuantity != null && groupProductQuantity != "") {
                    _productSKU = {
                        Quantity: groupProductQuantity,
                        SKU: $(this).attr("data-sku"),
                        ParentSKU: $(control).attr("data-sku"),
                        ParentProductId: parseInt($("#dynamic-parentproductid").val()),
                    }
                    return false;
                }
            }
        });
        if (_productSKU == null) {
            _productSKU = {
                Quantity: $("#Quantity").val(),
                SKU: $("#Quantity").attr("data-sku"),
                ParentSKU: $("#Quantity").attr("data-parentsku"),
                ParentProductId: parseInt($("#Quantity").attr("data-parentProductId")),
            }
        }
        return _productSKU;
    }

    getAddOnIds(parentSelector): any {
        var selectedAddOnIds = [];
        if (typeof parentSelector == "undefined") { parentSelector = ""; }
        $(parentSelector + " select.AddOn").each(function () {
            if ($(this).val() != "0") {
                selectedAddOnIds.push($(this).val());
            }
        });

        $(parentSelector + " input.AddOn:checked").each(function () {
            if ($(this).val() != "0") {
                selectedAddOnIds.push($(this).val());
            }
        });
        return (selectedAddOnIds.join());
    }

    UpdateProductVariations(htmlContainer: boolean, sku: string, parentProductSKU: string, quantity: string, parentProductId: number, callbackMethod): any {
        var selectedAddOnIds = Product.prototype.getAddOnIds("");
        Endpoint.prototype.GetProductPrice(sku, parentProductSKU, quantity, selectedAddOnIds, parentProductId, function (res) {

            if (callbackMethod) {
                callbackMethod(res);
            }
        });
    }

    UpdateProductValues(response, quantity): any {
        var selectedAddOnIds = Product.prototype.getAddOnIds("");
        // Set form values for submit
        $("#dynamic-sku").val(response.data.sku);
        $("#Quantity").val(quantity);
        $("#dynamic-addons").val(selectedAddOnIds);
        $("input[name='AddOnValueIds']").val(selectedAddOnIds);
        $("#dynamic-productName").val(response.data.ProductName);
        if (response.data.addOnMessage != undefined) {
            $("#dynamic-addOninventory").show();
            $("#dynamic-addOninventory").html(response.data.addOnMessage);
            return false;
        }
        else {
            $("#dynamic-addOninventory").hide();
            $("#dynamic-addOninventory").html("");
            return true;
        }
    }

    OnAttributeSelect(control): any {
        var productId = $("#scrollReview form").children("#dynamic-parentproductid").val();
        var Codes = [];
        var Values = [];
        var sku = $("#dynamic-configurableproductskus").val();
        var ParentProductSKU = $("#dynamic-sku").val();
        var selectedCode = $(control).attr('code');
        var selectedValue = $(control).val();

        $("select.ConfigurableAttribute").each(function () {
            Values.push($(this).val());
            Codes.push($(this).attr('id'));
        });

        $(" input.ConfigurableAttribute:checked").each(function () {
            Values.push($(this).val());
            Codes.push($(this).attr('code'));
        });
        var catgoryIds: string = $("#categoryIds").val();
        var parameters = {
            "SelectedCode": selectedCode,
            "SelectedValue": selectedValue,
            "SKU": sku,
            "Codes": Codes.join(),
            "Values": Values.join(),
            "ParentProductId": productId,
            "ParentProductSKU": ParentProductSKU,
            "IsQuickView": $("#isQuickView").val()
        }

        Endpoint.prototype.GetProduct(parameters, function (res) {
            $("#layout-product").replaceWith(res);
            $("#categoryIds").val(catgoryIds);
            isFromCategoryPage = localStorage.getItem("isFromCategoryPage");
            Product.prototype.GetProductBreadCrumb(parseInt(window.sessionStorage.getItem("lastCategoryId"), 10), $("#isQuickView").val());
            $("#breadcrumb-productname").html($(".product-name").html())
        });
    }


    //TO DO:
    GetSuggestionResult(item: any): any {
        $.each(item.properties, function (brand) {
            window.location.href = "/Search/Index?SearchTerm=" + encodeURIComponent(item.displaytext) + "&CategoryId=" + item.properties[brand];
        });
    }

    ShowErrorAddonError(isError: boolean, id: string, addOnId: string): boolean {
        if (!isError) {
            $("#" + id + addOnId).css("display", "none");
            return true;
        }
        else {
            $("#" + id + addOnId).removeAttr("style");
            return false;
        }
    }

    //Bind add on values of Product.
    BindAddOnProductSKU(control, event): any {
        var productType: string = $(control).closest('form').children("#dynamic-producttype").val();
        if (productType == "GroupedProduct") {
            if (isAddToCartGroupProduct == false) {
                return false;
            }
        }
        else if (Product.prototype.OnQuantityChange() == false) {
            return false;
        }

        var personalisedForm = $("#frmPersonalised");
        if (personalisedForm.length > 0 && !personalisedForm.valid())
            return false;
        var addOnValues = [];
        var bundleProducts = [];
        var groupDontTrackInventory = "";
        var groupProducts = "";
        var groupProductsQuantity = "";
        var personalisedCodes = [];
        var personalisedValues = [];
        var quantity: string = "";
        var flag: boolean = true;
        var finalFlag: boolean = true;
        //for checkbox   
        $(".chk-product-addons").each(function () {
            var optional = $(this).data("isoptional");
            var displayType = $(this).data("displaytype");
            var id = $(this).attr("id");
            var addOnId = $(this).data("addongroupid");
            if (optional == "False") {
                flag = true;
            }
            else {
                var isError = true;
                if (displayType != "") {
                    var addOnDivId = "";
                    displayType = displayType.toLowerCase();
                    if (displayType == "checkbox") {
                        if ($('#' + id + ' input[type=checkbox]:checked').length > 0) {
                            isError = false;
                        }
                        addOnDivId = "paraCommentCheckBox-";
                    }
                    if (displayType == "radiobutton") {
                        if ($('#' + id + ' input[type=radio]:checked').length > 0) {
                            isError = false;
                        }
                        addOnDivId = "paraCommentRadioButton-";
                    }
                    if (displayType == "dropdown") {
                        var isSelected = $('#' + id).find('option:selected').val() == "0" || $('#' + id).find('option:selected').val() == undefined ? false : true;
                        if (isSelected) {
                            isError = false;
                        }
                        addOnDivId = "paraCommentDropDown-";
                    }

                    if (!isError) {
                        $("#" + addOnDivId + addOnId).css("display", "none");
                        flag = true;
                    }
                    else {
                        $("#" + addOnDivId + addOnId).removeAttr("style");
                        flag = false;
                    }

                    if (flag == false) {
                        finalFlag = false;
                    }
                }
            }
        });

        //Get selected add on product skus.
        addOnValues = Product.prototype.GetSelectedAddons();

        //Get bundle product skus.
        bundleProducts = Product.prototype.GetSelectedBundelProducts();

        //Get group product skus and their quantity.
        $("input[type=text].quantity").each(function () {
            if ($(this).attr("name") != "Quantity") {
                var quantity = $(this).val();
                if (quantity != null && quantity != "") {
                    groupProducts = groupProducts + $(this).attr("data-sku") + ",";
                    groupProductsQuantity + $(this).val() + "_";
                    groupProductsQuantity = groupProductsQuantity + $(this).val() + "_";
                }
                if (parseInt($(this).attr("data-maxquantity")) < parseInt(quantity))
                    return Product.prototype.CheckQuickViewAndShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("RequiredProductQuantity"));

                if (quantity != null && quantity != "" && $(this).attr("data-inventory") == "False") {
                    groupDontTrackInventory = groupDontTrackInventory + $(this).attr("data-sku") + ",";
                }
            }
        })
        groupProductsQuantity = groupProductsQuantity.substr(0, groupProductsQuantity.length - 1);
        groupProducts = groupProducts.substr(0, groupProducts.length - 1);

        if (productType == "GroupedProduct") {
            if (groupProductsQuantity == null || groupProductsQuantity == "") {
                return Product.prototype.CheckQuickViewAndShowErrorMessage(ZnodeBase.prototype.getResourceByKeyName("RequiredProductQuantity"));
            }
            else if (!Product.prototype.OnAssociatedProductQuantityChange()) {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorProductQuantity"), "error", true, 10000);
                return false;
            }
            else {
                var mainProductId: number = parseInt($("#dynamic-parentproductid").val());
                if (!Product.prototype.CheckGroupProductQuantity(mainProductId, groupProducts, groupProductsQuantity, groupDontTrackInventory))
                    return false;
            }
        }
        else {
            quantity = $("#Quantity").val();
        }
        $("input[IsPersonalizable = True]").each(function () {
            var $label = $("label[for='" + this.id + "']");
            personalisedValues.push($(this).val());
            personalisedCodes.push($label.text());
        });
        //Map Cart item model values for add to cart.
        Product.prototype.SetCartItemModelValues(addOnValues, bundleProducts, groupProducts, groupProductsQuantity, quantity, personalisedCodes.join(), personalisedValues.join(), control);
        return finalFlag;
    }

    CheckGroupProductQuantity(productId: number, groupProductSkus: string, groupProductQuantities: string, groupDontTrackInventory: string): boolean {
        var isSuccess: boolean = true;
        if (groupDontTrackInventory.trim() != "") {
            Endpoint.prototype.CheckGroupProductInventory(productId, groupProductSkus, groupProductQuantities, function (response) {
                if (!response.ShowAddToCart) {
                    Product.prototype.CheckQuickViewAndShowErrorMessage(response.InventoryMessage);
                }
                isSuccess = response.ShowAddToCart;
            });
        }
        return isSuccess;
    }

    CheckQuickViewAndShowErrorMessage(errorMessage): boolean {
        if ($("#quick-view-popup-ipad").is(":visible")) {
            $("#QuickViewQuantiyErrorMessage").html(errorMessage);
        } else {
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(errorMessage, "error", true, fadeOutTime);
        }
        return false;
    }

    GetSelectedAddons(): any {
        var addOnValues = [];
        $(".AddOn").each(function () {
            var values = "";
            if ($(this).is(":checked")) {
                values = $(this).val();
            }
            else {
                values = $(this).children(":selected").attr("data-addonsku");
            }
            if (values != null && values != "") {
                addOnValues.push(values);
            }
        });
        return addOnValues;
    }

    GetSelectedBundelProducts(): any {
        var bundleProducts = [];
        $(".bundle").each(function () {
            var values = $(this).attr("data-bundlesku");
            bundleProducts.push(values);
        })
        return bundleProducts;
    }

    SetCartItemModelValues(addOnValues, bundleProducts, groupProducts, groupProductsQuantity, quantity, personalisedcodes, personalisedvalues, control): any {
        $(control).closest('form').children("#dynamic-addonproductskus").val(addOnValues);
        $(control).closest('form').children("#dynamic-bundleproductskus").val(bundleProducts);

        if (quantity != null || quantity != "") {
            $(control).closest('form').children("#dynamic-quantity").val(quantity);
        }
        $(control).closest('form').children("#dynamic-personalisedcodes").val(personalisedcodes);
        $(control).closest('form').children("#dynamic-personalisedvalues").val(personalisedvalues);
        $(control).closest('form').children("#dynamic-groupproductskus").val(groupProducts);
        $(control).closest('form').children("#dynamic-groupproductsquantity").val(groupProductsQuantity);
        $(control).closest('form').children("#dynamic-groupproductsquantity").val(groupProductsQuantity);
    }

    ActiveReadReviews(): any {
        var url = document.URL.toString();
        var name = '';
        if (!(url.indexOf('#') === -1)) {
            var urlParts = url.split("#");
            name = urlParts[1];
        }
        if (name == "scrollReview") {
            $("#tab-reviews").click();
        }
    }


    SendMailPopUp(): any {
        Endpoint.prototype.SendMail(function (res) {
            $("#btnSendMailPopup").click();
            $("#popUp_sendMail").html(res);
            $("#divSendMail").html(res);
        });
    }

    SendMailResult(data: any) {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(data.Message, data.Type, isFadeOut, fadeOutTime);
        $("#divSendMail").hide();
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        $("#divSendMail").find("button[class=close]").click();
    }

    OnClickSendMail(): void {
        $("#divSendMail").hide();
        ZnodeBase.prototype.ShowLoader();
    }

    EmailToFriendSuccess(): any {
        jQuery('#modelEmailToFriend').trigger('click');
        $("#YourMailId").val("");
        $("#FriendMailId").val("");
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SuccessMailSending"), "success", true, 10000);
        ZnodeBase.prototype.HideLoader();
    }
    EmailToFriendBegin(): any {
        $("#ProductName").val($(".product-name").html());
        ZnodeBase.prototype.ShowLoader();
        jQuery('#modelEmailToFriend').trigger('click');
    }
    EmailToFriendFailure() {
        jQuery('#modelEmailToFriend').trigger('click');
        $("#YourMailId").val("");
        $("#FriendMailId").val("");
        ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("ErrorMailSending"), "error", true, 10000);
        ZnodeBase.prototype.HideLoader();
    }

    GetProductBreadCrumb(categoryId: number, isQuickView: boolean): void {
        var categoryIds: Array<string> = $("#categoryIds").val().split(",");
        if (isFromCategoryPage == "true" && categoryId > 0) {
            if ($.inArray(categoryId.toString(), categoryIds) > -1) {
                Endpoint.prototype.GetBreadCrumb(categoryId, $("#categoryIds").val(), false, function (response) {
                    if (!isQuickView)
                        $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname'>" + $(".product-name").html() + "</span>")
                    $("#seeMore").html(response.seeMore)
                })
            }
            else {
                if (!$("#categoryIds").val()) {
                    if (!isQuickView)
                        $("#breadCrumb").html("<a href='/'>" + ZnodeBase.prototype.getResourceByKeyName("TextHome") + "</a>" + " / " + $(".product-name").html())
                    $("#seeMore").html("")
                }
                else {
                    Endpoint.prototype.GetBreadCrumb(parseInt(categoryIds[0], 10), $("#categoryIds").val(), false, function (response) {
                        if (!isQuickView)
                            $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname'>" + $(".product-name").html() + "</span>")
                        $("#seeMore").html(response.seeMore)
                    })
                }
            }
        }
        //new tab
        else if (isFromCategoryPage == "true" && (isNaN(categoryId))) {
            Endpoint.prototype.GetBreadCrumb(0, $("#categoryIds").val(), true, function (response) {
                if (!isQuickView)
                    $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname'>" + $(".product-name").html() + "</span>")
                $("#seeMore").html(response.seeMore)
            })
        }
        else if (isFromCategoryPage != "true") {
            if (!$("#categoryIds").val()) {
                if (!isQuickView)
                    $("#breadCrumb").html("<a href='/'>" + ZnodeBase.prototype.getResourceByKeyName("TextHome") + "</a>" + " / " + $(".product-name").html())
                $("#seeMore").html("")
            }
            else {
                Endpoint.prototype.GetBreadCrumb(parseInt(categoryIds[0], 10), $("#categoryIds").val(), false, function (response) {
                    if (!isQuickView)
                        $("#breadCrumb").html(response.breadCrumb + " / " + "<span id='breadcrumb-productname'>" + $(".product-name").html() + "</span>")
                    $("#seeMore").html(response.seeMore)
                })
            }
        }
    }

    public GetPriceAsync(): void {
        var products = new Array();
        $(".product-details .price-span").each(function () {
            var _control: any = $(this);
            var _product: ProductModel = { sku: _control.data("sku"), type: _control.data("type") };
            products.push(_product);
        })
        if (products.length > 0)
            Product.prototype.CallPriceApi(products);

    }

    private CallPriceApi(products: any): void {
        Endpoint.prototype.CallPriceApi(JSON.stringify(products), function (responce) {
            Product.prototype.AssignPricetoProduct(responce.data);
        });
    }

    private AssignPricetoProduct(data): void {
        $.each(data, function (index, value) {
            if (value.DisplaySalesPrice != null && value.DisplaySalesPrice != "") {
                $(".product-details .price-span[data-sku='" + value.sku + "']").html(value.DisplaySalesPrice);
                if (value.DisplayRetailPrice != null && value.DisplayRetailPrice != "") {
                    $(".product-details .price-span[data-sku='" + value.sku + "']").append("<span class='cut-price'>" + value.DisplayRetailPrice + "</span>");
                }
            }
            else if (value.DisplayRetailPrice != null && value.DisplayRetailPrice != "") {
                $(".product-details .price-span[data-sku='" + value.sku + "']").html(value.DisplayRetailPrice);
            }

        });
    }
}

type ProductModel = {
    sku: string;
    type: string;
};

// Click for write review form stars
$(document).on("click", "#layout-writereview .setrating label", function () {
    $("#layout-writereview .setrating label").removeClass("full").addClass("empty"); // Reset all to empty            

    var stars = $(this).data("stars");
    $("#Rating").val(stars);
    for (var a = 1; a <= stars; a += 1) {
        $(".star" + a).removeClass("empty").addClass("full");
    }
});

$(document).on("keypress", "#product-details-quantity input[name='Quantity']", function () {
    $(this).attr("data-change", "true");
});