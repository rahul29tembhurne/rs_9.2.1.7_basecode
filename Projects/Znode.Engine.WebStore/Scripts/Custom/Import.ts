﻿class Import extends ZnodeBase {
    constructor() {
        super();
    }

    ValidateImportFile(): boolean {        
        if ($("#ImportData").val() == "") {
            $("#importErrorFileTypeAndSize").html(ZnodeBase.prototype.getResourceByKeyName("FileNotPresentError"));
            return false;
        }
    }

    //Delete logs by log ids.
    DeleteImportLogs(control): any {
        var importProcessLogId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (importProcessLogId.length > 0) {
            Endpoint.prototype.DeleteImportLogs(importProcessLogId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }
}
