﻿using Microsoft.AspNet.Identity.Owin;

namespace Znode.Engine.Api.Models
{
    public class SocialLoginModel
    {
        public ExternalLoginInfo LoginInfo { get; set; }
        public bool IsPersistent { get; set; }
        public int PortalId { get; set; }
        public string UserName { get; set; }
    }
}
