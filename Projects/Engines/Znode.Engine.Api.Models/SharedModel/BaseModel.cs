﻿using System;
using System.Xml.Serialization;

namespace Znode.Engine.Api.Models
{
    public abstract class BaseModel
    {
        [XmlIgnore]
        public int CreatedBy { get; set; }
        [XmlIgnore]
        public  DateTime CreatedDate { get; set; }
        [XmlIgnore]
        public int ModifiedBy { get; set; }
        [XmlIgnore]
        public  DateTime ModifiedDate { get; set; }
        public string ActionMode { get; set; }
        public object Custom1 { get; set; }
        public object Custom2 { get; set; }
        public object Custom3 { get; set; }
        public object Custom4 { get; set; }
        public object Custom5 { get; set; }
    }
}
