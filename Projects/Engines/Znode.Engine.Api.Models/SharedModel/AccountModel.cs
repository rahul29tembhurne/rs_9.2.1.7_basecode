﻿namespace Znode.Engine.Api.Models
{
    public class AccountModel : BaseModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public int AccountId { get; set; }
        public string ExternalId { get; set; }
        public int? ParentAccountId { get; set; }
        public string ParentAccountName { get; set; }
        public string AccountAddress { get; set; }
        public AddressModel Address { get; set; }
        public int? PortalId { get; set; }
        public string StoreName { get; set; }
        public string ShippingPostalCode { get; set; }
        public string BillingPostalCode { get; set; }
        public int? PublishCatalogId { get; set; }
        public string CatalogName { get; set; }
    }
}
