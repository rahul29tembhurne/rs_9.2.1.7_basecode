﻿namespace Znode.Engine.Api.Models.Responses
{
    public class CMSTextWidgetConfigurationResponse : BaseResponse
    {
        public CMSTextWidgetConfigurationModel CMSTextWidgetConfigurtion { get; set; }
    }
}
