﻿namespace Znode.Engine.Api.Models.Responses
{
    public class WebStoreWidgetResponse : BaseResponse
    {
        //Slider Widget Response.
        public CMSWidgetConfigurationModel Slider { get; set; }

        public CMSTextWidgetConfigurationModel CMSTextWidget { get; set; }
        public int CMSWidgetsId { get; set; }
    }
}
