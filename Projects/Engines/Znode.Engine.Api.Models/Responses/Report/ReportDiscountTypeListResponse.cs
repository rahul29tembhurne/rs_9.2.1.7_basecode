﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
    public class ReportDiscountTypeListResponse : BaseListResponse
    {
        public List<ReportDiscountTypeModel> DiscountTypeList { get; set; }
    }
}
