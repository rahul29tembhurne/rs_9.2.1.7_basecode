﻿namespace Znode.Engine.Api.Models
{
    public class OrderDiscountModel : BaseModel
    {
        public int OmsOrderDiscountId { get; set; }
        public int? OmsOrderDetailsId { get; set; }
        public int? OmsOrderLineItemId { get; set; }
        public int? OmsDiscountTypeId { get; set; }
        public string DiscountCode { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? OriginalDiscount { get; set; }
        public string Description { get; set; }
    }
}
