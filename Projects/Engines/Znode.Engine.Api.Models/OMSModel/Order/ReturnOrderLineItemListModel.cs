﻿using System.Collections.Generic;

namespace Znode.Engine.Api.Models
{
    public class ReturnOrderLineItemListModel :  BaseListModel
    {
        public List<ReturnOrderLineItemModel> ReturnItemList { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TaxCost { get; set; }
        public decimal ShippingCost { get; set; }
        public decimal Total { get; set; }
    }
}
