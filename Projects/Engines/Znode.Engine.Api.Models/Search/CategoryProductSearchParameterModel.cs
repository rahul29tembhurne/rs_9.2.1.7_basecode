﻿using System.Collections.Generic;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Engine.Api.Models
{
    public class CategoryProductSearchParameterModel
    {
        public int CatalogId { get; set; }
        public int SearchIndexMonitorId { get; set; }
        public int SearchIndexServerStatusId { get; set; }
        public int VersionId { get; set; }
        public string IndexName { get; set; }
        public string RevisionType { get; set; }
        public long IndexStartTime { get; set; }
        public List<int> PublishCategoryIds { get; set; }
        public List<LocaleModel> ActiveLocales { get; set; }
        public List<ProductEntity> ProductEntities { get; set; }
    }
}
