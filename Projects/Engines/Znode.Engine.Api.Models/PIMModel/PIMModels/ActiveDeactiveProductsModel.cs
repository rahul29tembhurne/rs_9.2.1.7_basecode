﻿namespace Znode.Engine.Api.Models
{
    public class ActiveDeactiveProductsModel
    {
        public string ProductIds { get; set; }
        public bool IsActive { get; set; }
        public int LocaleId { get; set; }
    }
}
