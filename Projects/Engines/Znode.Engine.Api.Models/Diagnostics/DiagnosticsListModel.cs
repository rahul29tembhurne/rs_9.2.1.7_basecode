﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class DiagnosticsListModel : BaseListModel
    {
        public DiagnosticsListModel()
        {
            DiagnosticsList = new List<DiagnosticsModel>();
        }
        public List<DiagnosticsModel> DiagnosticsList { get; set; }
    }
}
