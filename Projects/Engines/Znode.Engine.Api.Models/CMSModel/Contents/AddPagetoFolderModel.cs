﻿namespace Znode.Engine.Api.Models
{
    public class AddPagetoFolderModel : BaseModel
    {
        public int FolderId { get; set; }
        public string PageIds { get; set; }
    }
}
