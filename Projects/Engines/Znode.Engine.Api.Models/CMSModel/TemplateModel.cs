﻿using System.ComponentModel.DataAnnotations;

namespace Znode.Engine.Api.Models
{
    public class TemplateModel : BaseModel
    {
        public int CMSTemplateId { get; set; }  
        [Required]
        public string Name { get; set; }
        public string FileName { get; set; }
    }
}
