﻿using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Taxes.Helper;
using Znode.Engine.Taxes.Interfaces;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Taxes
{
    // This is the base class for all tax types.
    public class ZnodeTaxesType : IZnodeTaxesType
    {
        #region Member variables
        private string _className;
        private List<ZnodeTaxRuleControl> _controls;
        #endregion


        #region public Properties
        public string ClassName
        {
            get
            {
                if (String.IsNullOrEmpty(_className))
                {
                    _className = GetType().Name;
                }

                return _className;
            }

            set { _className = value; }
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public int Precedence { get; set; }
        public ZnodeShoppingCart ShoppingCart { get; set; }
        public ZnodeTaxBag TaxBag { get; set; }

        public List<ZnodeTaxRuleControl> Controls
        {
            get { return _controls ?? (_controls = new List<ZnodeTaxRuleControl>()); }
        }

        #endregion

        // Binds the shopping cart and tax data to the tax rule.
        public virtual void Bind(ZnodeShoppingCart shoppingCart, ZnodeTaxBag taxBag)
        {
            ShoppingCart = shoppingCart;
            TaxBag = taxBag;
        }

        // Calculates the tax and updates the shopping cart.
        public virtual void Calculate() { }

        //CCH full order return request ,compensates the transaction for returns or losses.
        public virtual void PartialReturnOrderLineItem() { }

        //CCH full order return request ,compensates the transaction for returns or losses.
        public virtual void CCHFullReturnRequest(ShoppingCartModel shoppingCartModel) { }

        // returnsTrue if everything is good for submitting the order; otherwise, false.
        public virtual bool PreSubmitOrderProcess() => true;

        // Process anything that must be done after the order is submitted.  
        public virtual void PostSubmitOrderProcess()
        {
            // Most taxes don't need any further processing after the order is submitted
        }

        // Process anything that must be done after the order is submitted.  
        public virtual void CancelOrderRequest(ShoppingCartModel shoppingCartModel)
        {
            // Most taxes don't need any further processing after the order is submitted
        }

        // This Method Indicates whether or not the tax rule is valid and should be applied to the shopping cart.
        public virtual bool IsValid()
        {
            bool isValid = false;

            AddressModel destinationAddress = ShoppingCart.Payment.ShippingAddress;

            //Check if tax rule applies to all countries
            if (Equals(TaxBag.DestinationCountryCode, null) || string.Equals(destinationAddress.CountryName, TaxBag.DestinationCountryCode, StringComparison.OrdinalIgnoreCase))
            {
                // Check if tax rule applies to all states
                if (Equals(TaxBag?.DestinationStateCode, null))
                    isValid = true;
                else if (string.Equals(TaxBag.DestinationStateCode, destinationAddress.StateCode, StringComparison.OrdinalIgnoreCase))
                {
                    // Check if tax rule applies to specific state
                    if (Equals(TaxBag?.CountyFIPS, null))
                        isValid = true;
                    else
                    {
                        ZnodeTaxHelper taxHelper = new ZnodeTaxHelper();
                        TaxRuleModel taxRuleModel = new TaxRuleModel() { CountyFIPS = TaxBag.CountyFIPS };
                        List<CityModel> cityList = taxHelper.GetTaxRuleByCountryFIPSCityStateCodePostalCode(destinationAddress, taxRuleModel);
                        if (!Equals(cityList?.Count, 0))
                            isValid = true;
                    }
                }
            }

            return isValid;
        }

        // call this method to cancel the tax request for 3rd party integrations using the transaction number
        public virtual bool CancelTaxRequest(string taxTransactionNumber, int? portalId) => false;

        // call this method in order to fulfill the tax requirement 
        public virtual bool TaxFulfillment(string taxTransactionNumber) => false;

        //Returns order line item.
        public virtual void ReturnOrderLineItem(ShoppingCartModel orderModel) { }

        //Get the currency.
        public string GetCulture(int currentStore)
        {
            IZnodeRepository<ZnodeCurrency> znodeCurrency = new ZnodeRepository<ZnodeCurrency>();
            IZnodeRepository<ZnodeCulture> znodeCulture = new ZnodeRepository<ZnodeCulture>();
            IZnodeRepository<ZnodePortalUnit> znodePortalUnit = new ZnodeRepository<ZnodePortalUnit>();
            //all client side transactions
            string culture = (from _znodePortalUnit in znodePortalUnit.Table
                               join _znodeCurrency in znodeCurrency.Table on _znodePortalUnit.CurrencyId equals _znodeCurrency.CurrencyId
                               join _znodeCulture in znodeCulture.Table on _znodePortalUnit.CultureId equals _znodeCulture.CultureId
                               where _znodePortalUnit.PortalId == currentStore
                               select
                                   _znodeCulture.CultureCode //TODO-U323
                        )?.FirstOrDefault();
            return culture;
        }

        public string GetCurrencyCode(int currentStore)
        {
            IZnodeRepository<ZnodeCurrency> znodeCurrency = new ZnodeRepository<ZnodeCurrency>();
            IZnodeRepository<ZnodePortalUnit> znodePortalUnit = new ZnodeRepository<ZnodePortalUnit>();

            string currencyCode = (from _znodePortalUnit in znodePortalUnit.Table
                               join _znodeCurrency in znodeCurrency.Table on _znodePortalUnit.CurrencyId equals _znodeCurrency.CurrencyId
                               where _znodePortalUnit.PortalId == currentStore
                               select _znodeCurrency.CurrencyCode 
                        )?.FirstOrDefault();
            return currencyCode;
        }
    }
}
