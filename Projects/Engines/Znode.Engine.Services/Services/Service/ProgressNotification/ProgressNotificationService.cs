﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Engine.Services
{
    public partial class ProgressNotificationService : BaseService, IProgressNotificationService
    {
        #region Private variables
        private readonly IMongoRepository<__ProgressNotifierEntity> _ProgressNotifierMongoRepository;
        #endregion

        #region Constructor
        public ProgressNotificationService()
        {
            _ProgressNotifierMongoRepository = new MongoRepository<__ProgressNotifierEntity>();
        }
        #endregion

        #region Public Methods

        //Get all progress notification.
        public virtual async Task<List<ProgressNotificationModel>> GetProgressNotifications()
        {
            List<ProgressNotificationModel> result = new List<ProgressNotificationModel>();

            Task t = new Task(() =>
            {
                result = _ProgressNotifierMongoRepository.GetEntityList(Query.Empty, true).ToModel<ProgressNotificationModel>().ToList();

                //Delete notifications once all the processes have been completed or failed. Deleting after returning data to make sure it is forwarded to the UI at least once.
                if (result.Count(x => !x.IsCompleted && !x.IsFailed) == 0)
                {
                    PreviewHelper.DeleteHistoricalProgressNotifications(_ProgressNotifierMongoRepository);
                }
            });

            t.Start();

            await t;

            return result;
        }

        #endregion      
    }
}
