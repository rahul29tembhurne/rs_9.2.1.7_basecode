﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Observer;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using EntityFramework.Extensions;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public class UserService : BaseService, IUserService
    {
        #region Private Variables
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;
        private ApplicationRoleManager _roleManager;
        private readonly IZnodeRepository<ZnodeUser> _userRepository;
        private readonly IZnodeRepository<AspNetZnodeUser> _aspNetZnodeuserRepository;
        private readonly IZnodeRepository<AspNetUser> _aspNetUserRepository;
        private readonly IZnodeRepository<ZnodeUserProfile> _accountProfileRepository;
        private readonly IZnodeRepository<ZnodeAccountUserPermission> _accountUserPermissionRepository;
        private readonly IZnodeRepository<ZnodeDepartmentUser> _departmentUserRepository;
        private readonly IZnodeRepository<ZnodeUserPortal> _userPortalRepository;
        private readonly IZnodeRepository<ZnodeAccessPermission> _accessPermissionRepository;
        private readonly IZnodeRepository<ZnodeAccountUserOrderApproval> _accountUserOrderApprovalRepository;
        private readonly IZnodeRepository<ZnodeAccountPermissionAccess> _accountPermissionAccessRepository;
        private readonly IZnodeRepository<ZnodePortalProfile> _portalProfileRepository;
        private readonly IZnodeRepository<ZnodeProfile> _profileRepository;
        private readonly IZnodeRepository<ZnodeUserProfile> _userProfileRepository;
        private readonly IZnodeRepository<ZnodeAddress> _addressRepository;
        private readonly IZnodeRepository<ZnodeUserAddress> _userAddressRepository;
        private readonly IZnodeRepository<ZnodeDomain> _domainRepository;
        private readonly IZnodeRepository<ZnodeAccountProfile> _accountAssociatedProfileRepository;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IZnodeRepository<ZnodePasswordLog> _passwordLogRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderRepository;
        private readonly IZnodeRepository<ZnodeAccountPermission> _accountPermissionRepository;
        #endregion

        #region Public Constructors
        public UserService()
        {
            _userRepository = new ZnodeRepository<ZnodeUser>();
            _accountProfileRepository = new ZnodeRepository<ZnodeUserProfile>();
            _accountUserPermissionRepository = new ZnodeRepository<ZnodeAccountUserPermission>();
            _departmentUserRepository = new ZnodeRepository<ZnodeDepartmentUser>();
            _userPortalRepository = new ZnodeRepository<ZnodeUserPortal>();
            _aspNetZnodeuserRepository = new ZnodeRepository<AspNetZnodeUser>();
            _aspNetUserRepository = new ZnodeRepository<AspNetUser>();
            _accessPermissionRepository = new ZnodeRepository<ZnodeAccessPermission>();
            _accountUserOrderApprovalRepository = new ZnodeRepository<ZnodeAccountUserOrderApproval>();
            _accountPermissionAccessRepository = new ZnodeRepository<ZnodeAccountPermissionAccess>();
            _portalProfileRepository = new ZnodeRepository<ZnodePortalProfile>();
            _profileRepository = new ZnodeRepository<ZnodeProfile>();
            _userProfileRepository = new ZnodeRepository<ZnodeUserProfile>();
            _addressRepository = new ZnodeRepository<ZnodeAddress>();
            _userAddressRepository = new ZnodeRepository<ZnodeUserAddress>();
            _domainRepository = new ZnodeRepository<ZnodeDomain>();
            _accountAssociatedProfileRepository = new ZnodeRepository<ZnodeAccountProfile>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _passwordLogRepository = new ZnodeRepository<ZnodePasswordLog>();
            _orderRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            _accountPermissionRepository = new ZnodeRepository<ZnodeAccountPermission>();
        }

        public UserService(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        #endregion

        #region Public Properties
        //Get value of ApplicationSignInManager which is used for the application.
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.Request.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        //Get value of ApplicationUserManager which is used for the application.
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //Get value of ApplicationRoleManager which is used for the application.
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.Current.Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        #endregion

        #region Public Methods
        public virtual UserModel Login(int portalId, UserModel model, out int? errorCode, NameValueCollection expand = null)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            errorCode = null;
            //Validate the Login user, and return the user details.
            ApplicationUser user = GetAspNetUserDetails(ref model, portalId);

            //This method is used to login the User having valid username and password.
            SignInStatus result = SignInManager.PasswordSignIn(user.UserName, model.User.Password, model.User.RememberMe, true);

            if (Equals(result, SignInStatus.Success))
            {
                //Check Password is expired or not.
                ZnodeUserAccountBase.CheckLastPasswordChangeDate(user.Id, out errorCode);

                CheckUserPortalAccessForLogin(model.PortalId.GetValueOrDefault(), (_userRepository.Table.FirstOrDefault(x => x.AspNetUserId == user.Id)?.UserId).GetValueOrDefault());

                //Bind user details required after login.
                return BindDetails(model, user, expand);
            }

            //Throw the Exceptions for Invalid Login.
            InvalidLoginError(user);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return model;
        }

        //Get paged user account list
        public virtual UserListModel GetUserList(int loggedUserAccountId, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { loggedUserAccountId = loggedUserAccountId });

            bool isApproval, isWebstore, isCustomerEditMode;
            string currentUserName, currentRoleName;
            int portalId;
            filters = GetValuesFromFilter(loggedUserAccountId, filters, out isApproval, out isWebstore, out currentUserName, out currentRoleName, out portalId, out isCustomerEditMode);

            if (portalId < 1)
                portalId = (_userPortalRepository.Table?.Where(x => x.UserId == loggedUserAccountId)?.Select(x => x.PortalId)?.FirstOrDefault()) ?? 0;

            ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            //get value for pagination
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            IZnodeViewRepository<UserModel> objStoredProc = new ZnodeViewRepository<UserModel>();

            //SP parameters
            objStoredProc.SetParameter("@RoleName", currentRoleName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@UserName", currentUserName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", null, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter("@IsCallOnSite", isWebstore, ParameterDirection.Input, DbType.Boolean);
            objStoredProc.SetParameter("@PortalId", portalId > 0 ? portalId : 0, ParameterDirection.Input, DbType.Int32);

            List<UserModel> accountList;

            accountList = objStoredProc.ExecuteStoredProcedureList("Znode_AdminUsers  @RoleName,@UserName,@WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT,@IsCallOnSite,@PortalId", 6, out pageListModel.TotalRowCount)?.ToList();

            UserListModel list = new UserListModel();

            if (accountList?.Count > 0)
                list.Users = accountList;

            //Remove all the users from list who is having role 'AccountUser'.
            RemoveAcccountUsers(isApproval, list);

            //Sets the user's portal Id.
            SetAssignedPortal(list.Users.FirstOrDefault(), isCustomerEditMode);

            list.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return list;
        }

        //Get the Account By Account Id
        public virtual UserModel GetUserById(int userId, NameValueCollection expands, int portalId = 0)
        {
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { userId = userId, portalId = portalId });

            if (userId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.AccountIdNotLessThanOne);

            //This method is used to get the account details.
            ZnodeUser user = portalId > 0 ? _userRepository.Table.FirstOrDefault(x => x.UserId == userId && x.ZnodeUserPortals.Select(y => y.PortalId == portalId).FirstOrDefault()) :
                                            _userRepository.Table.Where(x => x.UserId == userId).Include(x => x.ZnodeUserPortals)?.FirstOrDefault();

            //If user is global level, get its details.
            if (portalId > 0 && IsNull(user))
            {
                user = _userRepository.Table.Where(x => x.UserId == userId).Include(x => x.ZnodeUserPortals)?.FirstOrDefault();

                if (IsNotNull(user) && user.ZnodeUserPortals?.FirstOrDefault()?.PortalId.GetValueOrDefault() > 0 && user.ZnodeUserPortals?.FirstOrDefault()?.PortalId != portalId)
                    return null;
            }

            //Check if user is guest user for gift card creation/ updation.
            if (IsNotNull(expands) && expands[ZnodeConstant.IsGuestUserForGiftCard]?.Length > 0 && IsNull(user?.AspNetUserId))
                return null;

            if (IsNotNull(user))
            {
                ApplicationUser userDetails = UserManager.FindById(user.AspNetUserId);

                //Map ZnodeUser Entity to User Model.
                UserModel accountDetails = UserMap.ToModel(user, string.Empty);

                //In the case of anonymous user, skip below checks.
                if (IsNotNull(userDetails))
                {
                    //Sets the portal ids.
                    SetPortal(accountDetails);

                    //Sets the customer details.
                    SetCustomerDetails(userDetails, accountDetails, user);
                }
                else
                {
                    accountDetails.IsGuestUser = true;
                }

                //For global level user
                if ((accountDetails?.PortalId < 0) || IsNull(accountDetails?.PortalId))
                    accountDetails.PortalId = portalId;

                return BindUserDetails(accountDetails, expands);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return null;
        }

        //Get the User Account Details by its UserName
        public virtual UserModel GetUserByUsername(string username, int portalId, bool isSocialLogin = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { username = username, portalId = portalId });

            if (string.IsNullOrEmpty(username))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.UserNameNotEmpty);

            //Get the AspNet Znode user.
            AspNetZnodeUser aspnetZnodeUser = LoginHelper.GetUserInfo(username, portalId);

            if (IsNull(aspnetZnodeUser))
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.UserNotPresent, username));

            ApplicationUser user = UserManager.FindByName(aspnetZnodeUser.AspNetZnodeUserId);

            if (IsNull(user))
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.UserNotFound, username));

            //This method is used to get the account details.
            ZnodeUser znodeuser = _userRepository.GetEntity(FilterClauseForUserId(user).WhereClause, FilterClauseForUserId(user).FilterValues);

            //Map ZnodeUser Entity to User Model.
            UserModel model = UserMap.ToModel(znodeuser, user.Email);
            if (IsNotNull(model.User) && IsNotNull(user.Roles) && user.Roles.Count > 0)
                model.User.RoleId = user.Roles?.Select(s => s.RoleId)?.FirstOrDefault();

            //check whether the current login user has Customer user and B2B role or not.
            model.IsAdminUser = IsAdminUser(user);

            NameValueCollection expands = new NameValueCollection();
            expands.Add(FilterKeys.Profiles, FilterKeys.Profiles);

            BindUserDetails(model, expands);

            //Bind user details required after login.
            model = BindDetails(model, user, expands);

            model.UserName = aspnetZnodeUser.UserName;

            //Throw the Exceptions for Invalid social Login.
            if (isSocialLogin && UserManager.IsLockedOut(user.Id))
            {
                ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Login failed", null);
                throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return model;
        }

        public virtual UserModel ChangePassword(int portalId, UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

            if (IsNull(model.User))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.LoginUserModelNotNull);

            //Get the AspNet Znode user.
            AspNetZnodeUser znodeUser = LoginHelper.GetUser(model, (model.PortalId > 0) ? model.PortalId.Value : portalId);

            if (IsNull(znodeUser))
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.UserNameUnavailable, model.UserName));

            //This method is used to find the User having valid username.
            ApplicationUser user = UserManager.FindByNameAsync(znodeUser.AspNetZnodeUserId).Result;

            if (IsNull(user))
                throw new ZnodeException(ErrorCodes.InvalidData, "User " + model.User.Username + " not found.");
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return PasswordVerification((model.PortalId > 0) ? model.PortalId.Value : portalId, model, user);
        }

        public virtual bool IsDefaultAdminPasswordReset()
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeEncryption encryption = new ZnodeEncryption();
            string encrytpedPassword = encryption.EncryptData(ZnodeConstant.DefaultPassword);
            string AspNetUserId = (from zu in _aspNetZnodeuserRepository.Table
                                   join upwdl in _aspNetUserRepository.Table on zu.AspNetZnodeUserId equals upwdl.UserName
                                   where zu.UserName == ZnodeConstant.DefaultAdminUser
                                   select upwdl.Id).FirstOrDefault();
            ZnodeLogging.LogMessage("AspNetUserId:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, AspNetUserId);

            //Check if admin is logging in for the first time, if yes then skip condition check for reset password popup.
            int? errorCode;
            ZnodeUserAccountBase.CheckLastPasswordChangeDate(AspNetUserId, out errorCode);
            if (errorCode == 2) return true;

            //Check if admin is still using the default password.
            string currentPassword = _passwordLogRepository.Table.Where(x => x.UserId == AspNetUserId).ToList()?.LastOrDefault()?.Password;
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return !string.Equals(currentPassword, encrytpedPassword, StringComparison.InvariantCultureIgnoreCase);
        }

        //Bulk password reset functionality.
        public virtual bool BulkResetPassword(ParameterModel accountIds)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            bool isPasswordReset = true;
            StringBuilder failedUserNames = new StringBuilder();
            List<ZnodeUser> accountList = GetUsers(accountIds);
            ZnodeLogging.LogMessage("accountList list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, accountList?.Count());
            if (accountList?.Count > 0)
            {
                foreach (var account in accountList)
                {
                    try
                    {
                        if (IsNull(account))
                            throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.UserModelNotNull);

                        ApplicationUser userDetails = UserManager.FindById(account.AspNetUserId);
                        if (IsNull(userDetails))
                            throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ApplicationUserNotNull);
                        //This method is used to find the User having valid user name.
                        ApplicationUser user = UserManager.FindByNameAsync(userDetails.UserName).Result;

                        if (IsNull(user))
                            throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.UserNotFound);

                        //This method is used to verify the user with password stored in database.
                        if (UserManager.IsLockedOut(user.Id))
                            throw new ZnodeException(ErrorCodes.AccountLocked, Admin_Resources.AccountLocked);

                        //This method is used to generate password reset token.
                        string passwordResetToken = UserManager.GeneratePasswordResetToken(user.Id);
                        //Reset Password Link in Email - Start

                        //Get the User name for Znode user.
                        string userName = LoginHelper.GetUserById(userDetails.UserName)?.UserName;

                        int portalId = _userPortalRepository.Table.FirstOrDefault(x => x.UserId == account.UserId)?.PortalId ?? PortalId;

                        //Send the Reset Password Email
                        SendResetPasswordMail(userDetails, passwordResetToken, userName, portalId);
                    }
                    catch (ZnodeException)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        isPasswordReset = false;
                        failedUserNames.Append($"{account.MiddleName} ");
                        ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Error);
                    }
                }
                if (!string.IsNullOrEmpty(Convert.ToString(failedUserNames)))
                    throw new ZnodeException(ErrorCodes.ErrorResetPassword, Convert.ToString(failedUserNames));
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return isPasswordReset;
        }

        //Single reset password functionality.
        public virtual UserModel ForgotPassword(int portalId, UserModel model, bool isUserCreateFromAdmin = false, bool isAdminUser = false)
        {
            ZnodeLogging.LogMessage("Execution Started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

            if (IsNull(model.User))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.LoginUserModelNotNull);

            portalId = (model.PortalId > 0) ? model.PortalId.Value : PortalId;
            ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            //Get the AspNet Znode user.
            AspNetZnodeUser znodeUser = LoginHelper.GetUser(model, portalId);

            if (IsNull(znodeUser))
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.UserNameUnavailable, model.User));

            //This method is used to find the User having valid username.
            ApplicationUser user = UserManager.FindByNameAsync(znodeUser.AspNetZnodeUserId).Result;

            if (IsNull(user))
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.UserNotFound, model.User));

            //This method is used to verify the userwith password stored in database.
            if (UserManager.IsLockedOut(user.Id))
                throw new ZnodeException(ErrorCodes.AccountLocked, Admin_Resources.AccountLocked);

            if (portalId < 1)
            {
                // Get the portal id.
                SetAssignedPortal(model, true);
                if (model.PortalId < 1)
                    model.PortalId = PortalId;
            }
            else
                model.PortalId = portalId;

            //Reset Password Link in Email - Start
            SendResetPassword(model, user, isUserCreateFromAdmin, isAdminUser);
            ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            return model;
        }

        //Verify reset password link status.
        public virtual int? VerifyResetPasswordLinkStatus(int portalId, UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

            //Get the AspNet Znode user.
            AspNetZnodeUser aspnetZnodeUser = LoginHelper.GetUserInfo(model.User.Username, (model.PortalId > 0) ? model.PortalId.Value : portalId);

            if (IsNull(aspnetZnodeUser))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ProvideValidUserName);

            int? statusCode = null;

            //Get the application user.
            ApplicationUser user = UserManager.FindByName(aspnetZnodeUser.AspNetZnodeUserId);
            if (IsNotNull(user))
                //This method is used to verify the reset password token is valid or not.
                statusCode = UserManager.VerifyUserToken(user.Id, "ResetPassword", model.User.PasswordToken) ? ErrorCodes.ResetPasswordContinue : ErrorCodes.ResetPasswordLinkExpired;
            ZnodeLogging.LogMessage("Output Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { statusCode = statusCode });

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return statusCode;
        }

        //Method to lock or unlock a user account.
        //(accountIds of users and lockUser specifies if user account has to be locked or not).
        public virtual bool EnableDisableUser(ParameterModel userIds, bool lockUser)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            bool isAllowedToLock = false;
            List<ZnodeUser> list = GetUsers(userIds);
            if (list?.Count > 0)
            {
                ZnodeLogging.LogMessage("ZnodeUser list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, list?.Count());
                int failedOperations = 0;
                foreach (ZnodeUser accountDetails in list)
                {
                    bool isError = EnableDisableUser(accountDetails, lockUser, out isAllowedToLock);

                    if (isError)
                        failedOperations++;
                }

                if (failedOperations > 0)
                {
                    throw new ZnodeException(ErrorCodes.LockOutEnabled, Admin_Resources.LockedOutDisableForAccount);
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return isAllowedToLock;
        }

        //Create New Admin User.
        public virtual UserModel CreateAdminUser(UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("AccountIds:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.AccountId);

            //Save the Login Details for the user.
            ApplicationUser user = CreateOwinUser(model);
            bool isUserCreateFromAdmin = false;
            if (IsNotNull(user))
            {
                //Checks if role name is null, then create normal user, else create admin user.
                if (!string.IsNullOrEmpty(model.RoleName))
                    UserManager.AddToRole(user.Id, model.RoleName);

                //Save account details.
                ZnodeUser account = _userRepository.Insert(model.ToEntity<ZnodeUser>());

                if (IsNotNull(account))
                {
                    ZnodeLogging.LogMessage(account?.AccountId > 0 ? Admin_Resources.SuccessUserDataSave : Admin_Resources.ErrorUserDataSave, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

                    //Insert Portal Mapping for the Admin user.
                    InsertUserPortals(model.PortalIds, account.UserId, false);

                    //Insert profile in AccountProfile table.
                    _accountProfileRepository.Insert(new ZnodeUserProfile { UserId = account.UserId, ProfileId = null, IsDefault = true });
                }

                // Get the portal id.
                SetAssignedPortal(model, false);
                if (!model.IsWebStoreUser)
                {
                    isUserCreateFromAdmin = true;
                    ForgotPassword((model.PortalId > 0) ? model.PortalId.Value : PortalId, model, isUserCreateFromAdmin, true/*flag for admin user*/);
                }

                //map account model to account entity.
                if (!SendEmailToStoreAdministrator(model.FirstName, model.User.Password, account.Email, (model.PortalId > 0) ? model.PortalId.Value : PortalId, model.LocaleId, false, model.IsWebStoreUser, isUserCreateFromAdmin))
                {
                    UserModel accountModel = UserMap.ToModel(account, user.Email);
                    accountModel.IsEmailSentFailed = true;
                    return accountModel;
                }
                return UserMap.ToModel(account, user.Email);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return new UserModel();
        }

        //Update user account data.
        public virtual bool UpdateUserData(UserModel userModel, bool webStoreUser)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("AccountIds:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, userModel.AccountId);

            if (IsNull(userModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

            if (userModel.UserId > 0)
            {
                //Update roles in AspNetUserRoles table.
                List<string> roles = UserManager.GetRoles(userModel.AspNetUserId).ToList();
                ZnodeLogging.LogMessage("roles list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, roles?.Count());
                if (!webStoreUser)
                {
                    if (IsNotNull(roles))
                    {
                        if (!Equals(roles.FirstOrDefault(), userModel.RoleName))
                        {
                            UserManager.RemoveFromRoles(userModel.AspNetUserId, roles.ToArray());
                            UserManager.AddToRole(userModel.AspNetUserId, userModel.RoleName);
                            ZnodeLogging.LogMessage(Admin_Resources.SuccessUpdateRole, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                        }
                    }
                    else
                    {
                        UserManager.AddToRole(userModel.AspNetUserId, userModel.RoleName);
                        ZnodeLogging.LogMessage(Admin_Resources.SuccessUpdateRole, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                    }
                }

                bool updatedAccount = UpdateUser(userModel, webStoreUser);

                ZnodeLogging.LogMessage(updatedAccount ? Admin_Resources.SuccessUpdateData : Admin_Resources.ErrorUpdateData, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

                //Get user details by UserId.
                ApplicationUser user = UserManager.FindById(userModel.AspNetUserId);
                if (!webStoreUser)
                    InsertUserPortals(userModel.PortalIds, userModel.UserId, true);

                //Update email in AspNetUsers table.
                if (!Equals(user?.Email, userModel.Email))
                    user.Email = userModel.Email;

                if (updatedAccount)
                {
                    IdentityResult applicationUserManager = UserManager.Update(user);
                    ZnodeLogging.LogMessage(applicationUserManager.Succeeded ? Admin_Resources.SuccessUpdateAccount : Admin_Resources.ErrorUpdateAccount, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                    return true;
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return false;
        }

        public virtual bool DeleteUser(ParameterModel accountIds)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("AccountIds:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, accountIds.Ids);

            if (Equals(accountIds, null) || string.IsNullOrEmpty(accountIds.Ids))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserIdNotLessThanOne);

            int status = 0;
            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter("UserId", accountIds.Ids, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
            IList<View_ReturnBoolean> deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_DeleteUserDetails @UserId,@Status OUT", 1, out status);
            ZnodeLogging.LogMessage("deleteResult list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, deleteResult?.Count());
            if (deleteResult.FirstOrDefault().Status.Value)
            {
                ZnodeLogging.LogMessage(Admin_Resources.SuccessUserDelete, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                return true;
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorDeleteAccount, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                throw new ZnodeException(ErrorCodes.AssociationDeleteError, Admin_Resources.ErrorDeleteSomeRecord);
            }
        }

        //Create New Customer User.
        public virtual UserModel CreateCustomer(int portalId, UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId, AccountId = model.AccountId });

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

            //Set the email as an username for external login.
            if (model.IsSocialLogin)
                SetUserNameForSocialLoginUser(model);

            //Create geust user account.
            if (model.IsGuestUser)
                return CreateGuestUserAccount(model);

            model.PortalId = DefaultGlobalConfigSettingHelper.AllowGlobalLevelUserCreation ? (int?)null : model.PortalId;

            //Save the user login Details.
            ApplicationUser user = CreateOwinUser(model);
            if (IsNotNull(user))
            {
                //Save the user Account, Profile Details 
                ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                UserModel userdataModel = InsertUserData(model, user, model.User.Password);

                //Avoid log password for user created from admin site,For forcing user to reset the password for the first time login.
                SetRequestHeaderForPasswordLog(null, userdataModel.UserId);
                ZnodeUserAccountBase.LogPassword(Convert.ToString(userdataModel.AspNetUserId), model.User.Password);
                //Save recently password change date.
                ZnodeUserAccountBase.SetPasswordChangedDate(userdataModel.AspNetUserId);
                return userdataModel;

            }
            else
                throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.LoginInalreadyExist);
        }

        public virtual bool UpdateCustomer(UserModel userModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = userModel?.UserId });

            if (IsNull(userModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

            if (userModel.UserId > 0)
            {
                //Check whether the Registerd default profile present for the portal.
                CheckRegisterProfile(userModel);

                //Todo: Need to replace Hardcoded code.
                if (Equals(userModel.PermissionCode, ZnodePermissionCodeEnum.DNRA.ToString()) || (Equals(userModel.PermissionCode, ZnodePermissionCodeEnum.ARA.ToString())) || (Equals(userModel.RoleName, ZnodeRoleEnum.Administrator.ToString())) || (Equals(userModel.RoleName, ZnodeRoleEnum.Manager.ToString())))
                    userModel.BudgetAmount = 0;

                // Update account details in account table.
                if (!string.IsNullOrEmpty(userModel.AspNetUserId))
                    ZnodeLogging.LogMessage(_userRepository.Update(userModel.ToEntity<ZnodeUser>()) ? Admin_Resources.SuccessUpdateData : Admin_Resources.ErrorUpdateData, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                else
                {
                    //Method to update guest user account.
                    UpdateGuestUserAccount(userModel);
                    if (userModel.IsEmailSentFailed) { return false; } else { return true; }
                }

                //Get user details by UserId.
                ApplicationUser user = UserManager.FindById(userModel.AspNetUserId);

                //Updates into AspnetUserRole
                UpdateUserRole(userModel, user);

                //Update email in AspNetUsers table.
                if (!Equals(user?.Email, userModel.Email))
                    user.Email = userModel.Email;

                if (userModel.AccountId > 0)
                {
                    InsertIntoUserDepartment(userModel, true);

                    //Update profile on converting normal user to B2B user.
                    UpdateProfileForAccountAssociatedUser(userModel);

                    //Save the Account user permissions.
                    SaveAccountUserPermission(userModel);

                    //Get the portal id of account.
                    userModel.PortalId = GetAccountPortalId(userModel.AccountId.GetValueOrDefault());
                }

                InsertIntoUserPortal(userModel.PortalId, userModel.UserId, true);
                IdentityResult applicationUserManager = UserManager.Update(user);

                ZnodeLogging.LogMessage(applicationUserManager.Succeeded ? Admin_Resources.SuccessUpdateAccount : Admin_Resources.ErrorUpdateAccount, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                return applicationUserManager.Succeeded;
            }
            return false;
        }

        //Gets the assigned portals to user.
        public virtual UserPortalModel GetPortalIds(string aspNetUserId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { aspNetUserId = aspNetUserId });

            if (string.IsNullOrEmpty(aspNetUserId))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.AspNetUserIdNotNull);

            IList<ZnodeUserPortal> userPortals = (from userPortal in _userPortalRepository.Table
                                                  join user in _userRepository.Table on userPortal.UserId equals user.UserId
                                                  where user.AspNetUserId == aspNetUserId
                                                  select userPortal).ToList();
            ZnodeLogging.LogMessage("userPortals list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, userPortals?.Count());

            if (userPortals?.Count > 0)
            {
                IPortalService _portalService = GetService<IPortalService>();
                return new UserPortalModel { AspNetUserId = aspNetUserId, PortalIds = IsNull(userPortals.Select(x => x.PortalId).FirstOrDefault()) ? _portalService.GetPortalList(null, null, null, null)?.PortalList?.Select(x => x.PortalId.ToString())?.ToArray() : userPortals.Select(x => x.PortalId.ToString())?.ToArray() };
            }
            return null;
        }

        //Save portal ids against the user.
        public virtual bool SavePortalsIds(UserPortalModel userPortalModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            if (IsNull(userPortalModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

            return IsNotNull(InsertUserPortals(userPortalModel.PortalIds, userPortalModel.UserId, true));
        }

        //Get the Customer Profiles based on the Portal & User Id.
        public virtual List<ProfileModel> GetCustomerProfile(int userId, int portalId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { userId = userId, portalId = portalId });

            if (userId <= 0)
                return new List<ProfileModel> { GetDefaultAnonymousProfile(portalId) };

            //Get Customer profile,associated to the user.
            return GetCustomerProfileByUserId(userId);
        }

        //Sign up for news letter.
        public virtual bool SignUpForNewsLetter(NewsLetterSignUpModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            if (string.IsNullOrEmpty(model?.Email))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.EmailNotNull);

            //Check if user already subscribed for news letter.
            if (!IsUserAlreadyExist(model.Email.Trim()))
            {
                ZnodeUser insertUser = InsertIntoZnodeUser(model);
                if (insertUser?.UserId > 0)
                {
                    //Insert portal in UserPortal table.
                    _userPortalRepository.Insert(new ZnodeUserPortal { UserId = insertUser.UserId, PortalId = PortalId });

                    ZnodeAddress insertAddress = InsertIntoAddress();
                    if (insertAddress?.AddressId > 0)
                        InsertIntoZnodeUserAddress(insertUser, insertAddress);
                    InsertIntoUserProfile(insertUser);
                    ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                    return true;
                }
            }
            else
                throw new ZnodeException(ErrorCodes.UserNameUnavailable, Admin_Resources.EmailAddressAlreadyExists);
            return false;
        }

        #region Social Login
        //Login the user from social login.
        public virtual UserModel SocialLogin(SocialLoginModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            if (IsNull(model) || IsNull(model.LoginInfo))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

            SignInStatus result = SignInManager.ExternalSignIn(model.LoginInfo, model.IsPersistent);

            //Register user in application.
            if (Equals(SignInStatus.Success, result))
            {
                //Get the AspNet Znode user.
                AspNetZnodeUser aspnetZnodeUser = LoginHelper.GetUserInfo(model.LoginInfo.Email, model.PortalId);

                return IsNull(aspnetZnodeUser) ? GetUserByUsername(model.UserName, model.PortalId, true) : GetUserByUsername(model.LoginInfo.Email, model.PortalId, true);
            }
            else if (Equals(SignInStatus.Failure, result))
                throw new ZnodeException(ErrorCodes.AtLeastSelectOne, SignInStatus.Failure.ToString());
            else
                throw new ZnodeException(ErrorCodes.LoginFailed, string.Format(Admin_Resources.LogInFail, model.UserName));
        }

        //Get the login providers.
        public virtual List<SocialDomainModel> GetLoginProviders()
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            IZnodeRepository<ZnodePortalLoginProvider> _portalLoginProvider = new ZnodeRepository<ZnodePortalLoginProvider>();

            NameValueCollection expands = new NameValueCollection();
            expands.Add(ZnodePortalLoginProviderEnum.ZnodeLoginProvider.ToString(), ZnodePortalLoginProviderEnum.ZnodeLoginProvider.ToString());
            expands.Add(ZnodePortalLoginProviderEnum.ZnodeDomain.ToString(), ZnodePortalLoginProviderEnum.ZnodeDomain.ToString());

            //Get all the login providers.
            List<ZnodePortalLoginProvider> allConfiguration = _portalLoginProvider.GetEntityList(string.Empty, GetSocialLoginExpands(expands)).ToList();
            ZnodeLogging.LogMessage("allConfiguration list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, allConfiguration?.Count());

            if (allConfiguration?.Count > 0)
            {
                //Get the login providers domain wise.
                List<ZnodePortalLoginProvider> domainWiseProvider = allConfiguration.GroupBy(x => x.DomainId).Select(x => x.First()).ToList();
                if (domainWiseProvider?.Count > 0)
                    //Map the login provider data domain wise.
                    return MapLoginProvider(allConfiguration, domainWiseProvider);
            }
            ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            return null;
        }
        #endregion

        #endregion

        #region Private Methods

        //Update profile on converting normal user to B2B user.
        private void UpdateProfileForAccountAssociatedUser(UserModel userModel)
        {
            //Delete all existing profiles of the user.
            DeleteExistingProfiles(userModel);

            //Get profiles of associated account.
            List<ZnodeAccountProfile> accountProfiles = _accountAssociatedProfileRepository.Table.Where(x => x.AccountId == userModel.AccountId).ToList();
            ZnodeLogging.LogMessage("accountProfiles list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, accountProfiles?.Count());
            if (accountProfiles?.Count > 0)
            {
                //Insert all the profiles of account associated to user.
                List<ZnodeUserProfile> entiesToinsert = new List<ZnodeUserProfile>();
                foreach (var item in accountProfiles)
                    entiesToinsert.Add(new ZnodeUserProfile() { UserId = userModel.UserId, ProfileId = Convert.ToInt32(item.ProfileId), IsDefault = item.IsDefault });

                ZnodeLogging.LogMessage("entiesToinsert list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, entiesToinsert?.Count());

                //Insert user profiles.
                ZnodeLogging.LogMessage(IsNotNull(_userProfileRepository.Insert(entiesToinsert)) ? Admin_Resources.SuccessUserProfileInsert : Admin_Resources.ErrorUserProfileInsert, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            }
        }

        //Delete all existing profiles of the user.
        private void DeleteExistingProfiles(UserModel userModel)
        {
            //Create tuple to generate where clause.  
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodeUserEnum.UserId.ToString(), ProcedureFilterOperators.Equals, userModel?.UserId.ToString()));

            //Delete existing profiles of the user.
            ZnodeLogging.LogMessage(_userProfileRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause) ? Admin_Resources.SuccessUserProfileDelete : Admin_Resources.ErrorUserProfileDelete, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        private bool SendEmailToStoreAdministrator(string firstName, string newPassword, string email, int portalId, int localeId, bool isCustomerUser = false, bool isWebstoreUser = false, bool isUserCreateFromAdmin = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId, email = email });

            string emailTemplateCode = isWebstoreUser ? "NewUserAccountWebstore" : "NewUserAccount";
            //Method to get Email Template Details by Code.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(emailTemplateCode, portalId, localeId);

            if (IsNotNull(emailTemplateMapperModel))
            {
                string subject = emailTemplateMapperModel?.Subject;
                string senderEmail, messageText;
                //Generate Email Message Content Based on the Email Template.
                GenerateActivationEmail(firstName, newPassword, emailTemplateMapperModel.Descriptions, portalId, isCustomerUser, out senderEmail, ref subject, out messageText);
                try
                {
                    //We can set email to bcc user on the basis of emailTemplateMapperModel.IsEnableBcc flag
                    if (!isUserCreateFromAdmin)
                        ZnodeEmail.SendEmail(portalId, email, senderEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, string.Empty), subject, messageText, true);
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginCreateSuccess, firstName, string.Empty, null, ex.Message, null);
                    return false;
                }
                return true;
            }
            return false;
        }

        //Create the New User. Mapped Entries in Znode User & Owin User.
        private ApplicationUser CreateOwinUser(UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            if (IsNotNull(model) && IsNotNull(model.User))
            {
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { User = model?.User, AccountId = model?.AccountId });

                //If the user belongs to account get the portal id of the account.
                if (model.AccountId > 0)
                    model.PortalId = GetAccountPortalId(model.AccountId.GetValueOrDefault());

                //Check whether the Registerd default profile present for the portal.
                CheckRegisterProfile(model);

                model.Email = string.IsNullOrEmpty(model.User.Email) ? model.Email : model.User.Email;
                if (model.User.Username?.Any() ?? false)
                {
                    //Check if the user name already exists or not.
                    if (LoginHelper.IsUserExists(model.User.Username, model.PortalId.GetValueOrDefault()))
                        throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.LoginNamesAlreadyExist);

                    //Create the User in Znode Mapper to allow portal level user creation.
                    AspNetZnodeUser znodeUser = LoginHelper.CreateUser(model);
                    if (IsNotNull(znodeUser) && !string.IsNullOrEmpty(znodeUser.AspNetZnodeUserId))
                    {
                        string znodeUserName = znodeUser.AspNetZnodeUserId;
                        ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { znodeUserName = znodeUserName });

                        ApplicationUser user = new ApplicationUser { UserName = znodeUserName, Email = model.User.Email };

                        //Get the Password for the user.
                        string password = GetPassword(model);
                        IdentityResult result = UserManager.Create(user, password);
                        if (result.Succeeded)
                        {
                            user = UserManager.FindByName(znodeUserName);
                            
                            //Save recently password change date.
                            ZnodeUserAccountBase.SetPasswordChangedDate(user.Id);
                            model.User.UserId = user.Id;
                            model.AspNetUserId = user.Id;
                            model.User.Password = password;
                            if (IsNotNull(model.ExternalLoginInfo?.Login))
                            {
                                result = UserManager.AddLogin(user.Id, model.ExternalLoginInfo.Login);
                                if (result.Succeeded)
                                    SignInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                            }
                            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                            return user;
                        }
                    }
                    throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.LoginNamesAlreadyExist);
                }
            }
            return null;
        }

        //Check for available Registered profile for the current portal.
        private void CheckRegisterProfile(UserModel model)
        {
            ZnodeLogging.LogMessage("Profile Id:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ProfileId);
            if (model.ProfileId <= 0)
            {
                int portalId = model.PortalId > 0 ? model.PortalId.Value : PortalId;

                //Check the Registered Profile for the Portal.
                int profileId = GetCurrentProfileId(portalId);
                if (profileId <= 0)
                    throw new ZnodeException(ErrorCodes.ProfileNotPresent, Admin_Resources.ErrorUserAccountDeleteConfigureDefaultProfile);
                model.ProfileId = profileId;
            }
        }

        //Generate the Unique Password for the new user.
        private static string GenerateNewPassword()
          => GetUniqueKey(8);

        private void InvalidLoginError(ApplicationUser user)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, user?.Id);
            if (!Equals(user, null))
            {
                if (UserManager.IsLockedOut(user.Id))
                {
                    ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Login failed", null);
                    throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
                }
                else
                {
                    //Gets current failed password atttemt count for setting the message.
                    int inValidAttemtCount = user.AccessFailedCount;

                    //Gets Maximum failed password atttemt count from web.config
                    int maxInvalidPasswordAttemptCount = UserManager.MaxFailedAccessAttemptsBeforeLockout;
                    ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { inValidAttemtCount = inValidAttemtCount, maxInvalidPasswordAttemptCount = maxInvalidPasswordAttemptCount });

                    if (inValidAttemtCount > 0 && maxInvalidPasswordAttemptCount > 0)
                    {
                        if (maxInvalidPasswordAttemptCount <= inValidAttemtCount)
                        {
                            ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorLogin, null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
                        }

                        //Set warning error at (MaxFailureAttemptCount - 2) & (MaxFailureAttemptCount - 1) attempt.
                        if (Equals((maxInvalidPasswordAttemptCount - inValidAttemtCount), 2))
                        {
                            ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorLogin, null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.TwoAttemptsToAccountLocked, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
                        }
                        else if (Equals((maxInvalidPasswordAttemptCount - inValidAttemtCount), 1))
                        {
                            ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorLogin, null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.OneAttemptToAccountLocked, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
                        }
                        else
                        {
                            ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorUsernameAndPassword, null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
                        }
                    }
                    else
                    {
                        ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorUsernameAndPassword, null);
                        throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
                    }
                }
            }
            else
            {
                ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, Admin_Resources.ErrorUsernameAndPassword, null);
                throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, ZnodeLogging.Components.Admin.ToString(), HttpStatusCode.Unauthorized);
            }
        }

        //This method will send account activation email to relevant user.        
        private void SendAccountActivationEmail(string firstName, string email, int portalId, bool isLock)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { firstName = firstName, email = email, portalId = portalId });

            EmailTemplateMapperModel emailTemplateMapperModel = !isLock ? GetEmailTemplateByCode("AccountActivation", portalId)
                                                                        : GetEmailTemplateByCode("AccountDeActivation", portalId);
            PortalModel portalModel = GetCustomPortalDetails(portalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                string messageText = ReplaceTokenWithMessageText("#BillingFirstName#", firstName, emailTemplateMapperModel.Descriptions);
                messageText = ReplaceTokenWithMessageText("#StoreLogo#", portalModel?.StoreLogo, emailTemplateMapperModel.Descriptions);

                //These method is used to set null to unwanted keys.
                messageText = SetNullToUnWantedKeys(messageText);
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { messageText = messageText });

                string bcc = string.Empty;
                try
                {
                    if (IsNotNull(portalModel) && portalId > 0)
                        ZnodeEmail.SendEmail(portalId, email, portalModel?.AdministratorEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, bcc), $"{portalModel?.StoreName} - {emailTemplateMapperModel?.Subject}", messageText, true);
                    else
                        ZnodeEmail.SendEmail(email, ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, bcc), $"{ZnodeConfigManager.SiteConfig.StoreName} - {emailTemplateMapperModel?.Subject}", messageText, true);
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginCreateSuccess, firstName, string.Empty, null, ex.Message, null);
                }
            }
        }


        public ZnodePortal GetPortalDetails(int portalId)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            IZnodeRepository<ZnodePortal> _znodePortalRepository = new ZnodeRepository<ZnodePortal>();
            return _znodePortalRepository.Table.FirstOrDefault(x => x.PortalId == portalId);
        }

        private static string GetPassword(UserModel model)
            => IsNull(model.User) ? GenerateNewPassword() : (string.IsNullOrEmpty(model.User.Password)) ? GenerateNewPassword() : model.User.Password;

        //Set passwordResetUrl and send mail.
        private void SendResetPasswordMail(ApplicationUser userDetails, string passwordResetToken, string userName, int portalId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { userName = userName, portalId = portalId });

            //check whether the current login user has B2B role or not.
            bool isAdminUser = IsAdminUser(userDetails);

            string baseUrl = !isAdminUser ? GetDomains(portalId) : ZnodeApiSettings.AdminWebsiteUrl;

            //Get Portal Details.
            PortalModel portalModel = GetCustomPortalDetails(portalId);

            //Set the Http protocol based on the portal setting.
            baseUrl = !isAdminUser ? (IsNotNull(portalModel) && portalModel.IsEnableSSL) ? $"https://{baseUrl}" : $"http://{baseUrl}" : baseUrl;

            string passwordResetUrl = $"{baseUrl}/User/ResetPassword?passwordToken={WebUtility.UrlEncode(passwordResetToken)}&userName={WebUtility.UrlEncode(userName)}";
            string passwordResetLink = $"<a href=\"{passwordResetUrl}\"> here</a>";

            // Genrate Reset Password Email Content.
            string subject = string.Empty;
            //Set to True to send this email in HTML format.
            bool isHtml = false;
            bool isEnableBcc = false;
            try
            {
                string messageText = GenerateResetPasswordEmail(userName, passwordResetUrl, passwordResetLink, portalId, out subject, out isHtml, out isEnableBcc);
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { messageText = messageText });

                if (!string.IsNullOrEmpty(messageText))
                    //This method is used to send an email.          
                    if (IsNotNull(portalModel) && portalId > 0)
                        ZnodeEmail.SendEmail(portalId, userDetails.Email, portalModel?.AdministratorEmail, ZnodeEmail.GetBccEmail(isEnableBcc, portalId, string.Empty), subject, messageText, isHtml);
                    else
                        ZnodeEmail.SendEmail(userDetails.Email, ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeEmail.GetBccEmail(isEnableBcc, portalId, string.Empty), subject, messageText, isHtml);
                else
                    throw new ZnodeException(ErrorCodes.EmailTemplateDoesNotExists, Admin_Resources.ErrorResetPasswordLinkReset);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Error);
                throw new ZnodeException(ErrorCodes.ErrorSendResetPasswordLink, Admin_Resources.ErrorSendResetPasswordLink);
            }
        }

        //Get domain name on the basis of portal id.
        private string GetDomains(int portalId)
           => _domainRepository.Table.Where(x => x.PortalId == portalId && x.ApplicationType == ZnodeConstant.WebStore && x.IsActive && x.IsDefault)?.Select(x => x.DomainName)?.FirstOrDefault().ToString();

        //Password varification.
        private UserModel PasswordVerification(int portalId, UserModel model, ApplicationUser user)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            bool isResetPassword = !string.IsNullOrEmpty(model.User.PasswordToken);

            //This method is used to verify user.
            if (!isResetPassword && SignInManager.HasBeenVerified())
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorCurrentPassword);

            //This method is used to verify the reset password token is valid or not.
            if (isResetPassword && !UserManager.VerifyUserToken(user.Id, "ResetPassword", model.User.PasswordToken))
                throw new ZnodeException(ErrorCodes.ProcessingFailed, Admin_Resources.ResetPasswordLinkExpired);

            bool verified = (isResetPassword) ? true : ZnodeUserAccountBase.VerifyNewPassword(user.Id, model.User.NewPassword);

            if (verified)
            {
                // Update/Reset the password for this user
                var result = (isResetPassword)
                    //This method is used to reset the password having valid user Id, password token and new password.
                    ? UserManager.ResetPassword(user.Id, model.User.PasswordToken, model.User.NewPassword)

                    //This method is used to change the password having valid user Id, old password and new password.
                    : UserManager.ChangePassword(user.Id, model.User.Password, model.User.NewPassword);
                if (result.Succeeded)
                {
                    // Log password
                    SetRequestHeaderForPasswordLog(user.Id, 0);
                    ZnodeUserAccountBase.LogPassword(user.Id, model.User.NewPassword);
                    model.User.Password = model.User.NewPassword;

                    //Save recently password change date.
                    ZnodeUserAccountBase.SetPasswordChangedDate(user.Id);
                    int? errorCode = null;
                    return Login(portalId, model, out errorCode);
                }
                throw new ZnodeException(ErrorCodes.InvalidData, result.Errors.FirstOrDefault());
            }
            throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.EnterNewPassword);
        }

        //Check whether the current login user has Customer user and B2B role or not.
        protected virtual bool IsAdminUser(ApplicationUser user)
        {
            List<string> lstRole = GetRoles();

            //Get roles by user id.
            IList<string> roles = UserManager.GetRoles(user.Id.ToString());
            ZnodeLogging.LogMessage("roles list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, roles?.Count());
            return IsNotNull(roles) ? !roles.Any(x => lstRole.Contains(x)) : false;
        }

        //Get roles.
        private List<string> GetRoles()
        {
            List<string> lstRole = new List<string>();
            lstRole.Add(ZnodeRoleEnum.User.ToString());
            lstRole.Add(ZnodeRoleEnum.Customer.ToString());
            lstRole.Add(ZnodeRoleEnum.Administrator.ToString());
            lstRole.Add(ZnodeRoleEnum.Manager.ToString());
            ZnodeLogging.LogMessage("lstRole list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, lstRole?.Count());

            return lstRole;
        }

        //Get Account details of various account ids.
        private List<ZnodeUser> GetUsers(ParameterModel accountIds)
        {
            FilterCollection filterList = new FilterCollection();
            filterList.Add(new FilterTuple(ZnodeUserEnum.UserId.ToString(), ProcedureFilterOperators.In, accountIds.Ids));

            return _userRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filterList.ToFilterDataCollection()).WhereClause).ToList();
        }

        //Enable Disable user account.
        public virtual bool EnableDisableUser(ZnodeUser accountDetails, bool lockUser, out bool isAllowedToLock)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            isAllowedToLock = true;

            //Get current user details.
            ApplicationUser user = UserManager.FindById(accountDetails.AspNetUserId.ToString());
            if (IsNotNull(user))
            {
                int? portalId = _userPortalRepository.Table.FirstOrDefault(x => x.UserId == accountDetails.UserId)?.PortalId;
                // Gets the portal ids.
                portalId = ((portalId < 1) || IsNull(portalId)) ? PortalId : portalId;
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

                IdentityResult result = new IdentityResult();
                //Code for unlocking user account.
                if (UserManager.IsLockedOut(user.Id) && !lockUser)
                {
                    user.LockoutEndDateUtc = null;
                    result = UserManager.Update(user);
                }
                else if (lockUser)
                    //Code for locking user account.
                    result = UserManager.SetLockoutEndDate(user.Id, DateTimeOffset.MaxValue);

                if (IsNotNull(portalId))
                    SendAccountActivationEmail(accountDetails.FirstName, user.Email, portalId.Value, lockUser);
                if (result.Succeeded)
                    isAllowedToLock = true;
            }
            ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            return false;
        }

        //Get Filter clause for Users Id.
        protected virtual EntityWhereClauseModel FilterClauseForUserId(ApplicationUser user)
        {
            FilterCollection filters = new FilterCollection();
            //Create tupple to generate where clause.             
            filters.Add(new FilterTuple(ZnodeUserEnum.AspNetUserId.ToString(), ProcedureFilterOperators.Contains, user.Id.ToString()));

            //Generating where clause to get account details.             
            return DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
        }

        private void GenerateActivationEmail(string firstName, string newPassword, string templateContent, int portalId, bool isCustomerUser, out string senderEmail, ref string subject, out string messageText)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            string portalName = ZnodeConfigManager.SiteConfig.StoreName;
            senderEmail = ZnodeConfigManager.SiteConfig.AdminEmail;
            ZnodeLogging.LogMessage("Portal Name:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, portalName);

            string vendorLoginUrl = $"{HttpContext.Current.Request.Url.Scheme}://{ZnodeConfigManager.DomainConfig.DomainName}";
            if (vendorLoginUrl == $"{HttpContext.Current.Request.Url.Scheme}://")
                vendorLoginUrl = ZnodeApiSettings.AdminWebsiteUrl;

            //Get Portal details as well as the Domain Url based on the WEbStore Application Type.
            PortalModel model = GetCustomPortalDetails(portalId);

            //Get Portal Details in case of Customer User
            if (isCustomerUser && IsNotNull(model))
            {
                portalName = model.StoreName;
                senderEmail = model.AdministratorEmail;
                vendorLoginUrl = $"{HttpContext.Current.Request.Url.Scheme}://{model.DomainUrl}";
            }
            subject = $"{portalName} - {subject}";

            //Set Parameters for the Email Templates to be get replaced.
            EmailTemplateHelper.SetParameter("#FirstName#", firstName);
            EmailTemplateHelper.SetParameter("#UserName#", firstName);
            EmailTemplateHelper.SetParameter("#Url#", vendorLoginUrl);
            EmailTemplateHelper.SetParameter("#Password#", newPassword);
            EmailTemplateHelper.SetParameter("#StoreLogo#", model.StoreLogo);

            //Replace the Email Template Keys, based on the passed email template parameters.
            messageText = EmailTemplateHelper.ReplaceTemplateTokens(templateContent);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

        }

        //These method is used to set null to unwanted keys.
        private static string SetNullToUnWantedKeys(string messageText)
        {
            //It is used to find keys in between special characters.
            List<string> list = Regex.Matches(messageText, @"#\w*#")
                                    .Cast<Match>()
                                     .Select(m => m.Value)
                                        .ToList();

            ZnodeLogging.LogMessage("list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, list?.Count());
            foreach (string key in list)
                messageText = ReplaceTokenWithMessageText(key, null, messageText);

            ZnodeLogging.LogMessage("Output Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { messageText = messageText });

            return messageText;
        }

        //Insert data for guest user.
        private void InsertDataForGuestUser(UserModel userModel, ApplicationUser user, ZnodeUser account)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            //Create B2B Customer.
            CreateB2BCustomer(userModel, account, user);

            //Insert for the user portal.
            InsertIntoUserPortal(userModel.PortalId, userModel.UserId, true);

            // Gets the portal ids.
            SetAssignedPortal(userModel, true);

            //Set Display name 
            SetAddressName(userModel.UserId);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

        }

        //Get the department user entity.
        private ZnodeDepartmentUser GetDepartmentUserEntity(UserModel userModel)
            => new ZnodeDepartmentUser { DepartmentId = userModel.DepartmentId, DepartmentUserId = Convert.ToInt32(userModel.DepartmentUserId), UserId = userModel.UserId };

        //Save the account permissions for user.
        private void SaveAccountUserPermission(UserModel userModel)
        {
            ZnodeLogging.LogMessage("AccountPermissionAccessId:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, userModel?.AccountPermissionAccessId);
            //Update Account Permission Access user if alrady exists else create.
            if (userModel.AccountPermissionAccessId > 0)
            {
                ZnodeAccountUserPermission accountUserPermission = _accountUserPermissionRepository.Table.FirstOrDefault(x => x.UserId == userModel.UserId);
                if (IsNotNull(accountUserPermission))
                    // update account permission and approval
                    UpdateAccountPermission(userModel, accountUserPermission);
                else
                    _accountUserPermissionRepository.Insert(new ZnodeAccountUserPermission { AccountPermissionAccessId = userModel.AccountPermissionAccessId.GetValueOrDefault(), UserId = userModel.UserId });

            }
        }

        //Method to update account permission and approval
        private void UpdateAccountPermission(UserModel userModel, ZnodeAccountUserPermission accountUserPermission)
        {
            accountUserPermission.AccountPermissionAccessId = userModel.AccountPermissionAccessId.GetValueOrDefault();
            //Update Permission.
            _accountUserPermissionRepository.Update(accountUserPermission);
        }

        // Insert into UserPortal table. Delete the existing mapping and insert the new mapping.
        private IEnumerable<ZnodeUserPortal> InsertUserPortals(string[] portalIds, int userId, bool isUpdate)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { userId = userId, portalIdsCount = portalIds?.Count() });

            //If the call is from update then delete the existing mapping.
            if (isUpdate)
            {
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeUserPortalEnum.UserId.ToString(), FilterOperators.Equals, userId.ToString()));
                EntityWhereClauseModel whereClause = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                //Delete the existing mapping.
                _userPortalRepository.Delete(whereClause.WhereClause, whereClause.FilterValues);
            }

            if (portalIds?.Count() > 0)
            {
                List<ZnodeUserPortal> userPortalList = new List<ZnodeUserPortal>();
                //Prepare a list of entities to insert.
                foreach (string portalId in portalIds)
                    userPortalList.Add(new ZnodeUserPortal { UserId = userId, PortalId = Convert.ToInt32(portalId) > 0 ? Convert.ToInt32(portalId) : (int?)null });
                ZnodeLogging.LogMessage("userPortalList list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, userPortalList?.Count());
                ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                return _userPortalRepository.Insert(userPortalList);
            }
            return null;
        }

        private void InsertIntoUserPortal(int? portalId, int userId, bool isUpdate)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId, userId = userId });

            //if the global level user creation is set to true then set portal id to null.
            if (DefaultGlobalConfigSettingHelper.AllowGlobalLevelUserCreation)
                portalId = null;

            //If the call is from update then delete the existing mapping.
            if (isUpdate)
            {
                if (_userPortalRepository.Table.Any(x => x.UserId == userId && x.PortalId == portalId))
                    return;

                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeUserPortalEnum.UserId.ToString(), FilterOperators.Equals, userId.ToString()));
                EntityWhereClauseModel whereClause = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                //Delete the existing mapping.
                _userPortalRepository.Delete(whereClause.WhereClause, whereClause.FilterValues);
                ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            }

            //Inserts into user portal.
            ZnodeLogging.LogMessage(_userPortalRepository.Insert(new ZnodeUserPortal { UserId = userId, PortalId = portalId > 0 ? portalId : (int?)null })?.UserPortalId > 0
                ? Admin_Resources.SuccessUserPortalInsert : Admin_Resources.ErrorUserPortalInsert, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Check if IsApprovalList exists in filters and remove it.
        private void IsRoleNameExists(FilterCollection filters, ref bool isApproval, ref string currentUserName, ref string currentRoleName)
        {
            if (filters.Exists(x => x.FilterName == FilterKeys.IsApprovalList))
            {
                isApproval = true;
                filters.RemoveAll(x => x.FilterName == FilterKeys.IsApprovalList);
                currentRoleName = string.Empty;
                currentUserName = string.Empty;
            }
        }

        //Check if IsApprovalList exists in filters and remove it.
        private bool IsCustomerEditMode(FilterCollection filters)
        {
            if (filters.Exists(x => x.FilterName == FilterKeys.CustomerEditMode))
            {
                filters.RemoveAll(x => x.FilterName == FilterKeys.CustomerEditMode);
                return true;
            }
            return false;
        }

        //Create B2B customers.
        private void CreateB2BCustomer(UserModel model, ZnodeUser account, ApplicationUser user)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //Checks if role name is null, then create normal user with role Customer, else create admin user.
            if (LoginHelper.IsAccountCustomer(model))
            {
                InsertIntoUserDepartment(model);

                //Insert all the profiles of the account to which user is associated.
                InsertUserProfile(model, account);

                //Inserts into AspnetUserRole
                UserManager.AddToRole(user.Id, model.RoleName);
            }
            else
                UserManager.AddToRole(user.Id, ZnodeRoleEnum.Customer.ToString());

            if (string.IsNullOrEmpty(model.PermissionCode))
            {
                model.PermissionCode = ZnodePermissionCodeEnum.DNRA.ToString();
                model.AccountPermissionAccessId = GetDNRAAccountPermissionAccessId();
            }

            //Insert the access permission for user.
            InsertAccessPermissionForUser(model, account);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Get DNRA account permission access id.
        private int? GetDNRAAccountPermissionAccessId()
            => Convert.ToInt32(_accountPermissionAccessRepository.Table.Join(_accountPermissionRepository.Table.Where(o => !o.AccountId.HasValue && o.AccountPermissionName == ZnodeConstant.DNRAAccountPermissionAccessName),
                                                                                 o => o.AccountPermissionId,
                                                                                 ob => ob.AccountPermissionId,
                                                                                 (o, ob) => o)?.FirstOrDefault()?.AccountPermissionAccessId);

        //Insert the access permission for user
        protected virtual void InsertAccessPermissionForUser(UserModel model, ZnodeUser account)
        {
            if (model.AccountPermissionAccessId > 0)
                _accountUserPermissionRepository.Insert(new ZnodeAccountUserPermission { UserId = account.UserId, AccountPermissionAccessId = model.AccountPermissionAccessId.GetValueOrDefault() });

            if (!IsDoesNotRequirePermission(model.PermissionCode))
                _accountUserOrderApprovalRepository.Insert(new ZnodeAccountUserOrderApproval() { UserId = account.UserId });
        }

        //Updates into AspnetUserRole
        private void UpdateUserRole(UserModel userModel, ApplicationUser user)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            string oldRoleName = UserManager.GetRoles(user.Id).FirstOrDefault();
            ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { oldRoleName = oldRoleName });

            if (userModel.RoleName != ZnodeConstant.PleaseSelectLabel && IsNotNull(userModel.RoleName) && !Equals(oldRoleName, userModel.RoleName) && IsNotNull(oldRoleName))
            {
                UserManager.RemoveFromRole(user.Id, oldRoleName);
                UserManager.AddToRole(user.Id, userModel.RoleName);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Checks whether permission for user is does not require permission or not.
        protected virtual bool IsDoesNotRequirePermission(string permissionCode)
            => !string.IsNullOrEmpty(permissionCode) && Equals(permissionCode, ZnodePermissionCodeEnum.DNRA.ToString());

        //Method to insert User data.
        private UserModel InsertUserData(UserModel model, ApplicationUser user, string password)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

            bool isUserCreateFromAdmin = false;
            //Save account details.
            ZnodeUser account = _userRepository.Insert(model.ToEntity<ZnodeUser>());
            ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { UserId = account?.UserId });

            if (account?.UserId > 0)
            {
                model.UserId = account.UserId;
                ZnodeLogging.LogMessage(account?.UserId > 0 ? Admin_Resources.SuccessUserDataSave : Admin_Resources.ErrorUserDataSave, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

                if (!LoginHelper.IsAccountCustomer(model))
                    _accountProfileRepository.Insert(new ZnodeUserProfile { UserId = account.UserId, ProfileId = model.ProfileId, IsDefault = true });

                //Create B2B Customer.
                CreateB2BCustomer(model, account, user);

                //Insert for the user portal.
                InsertIntoUserPortal(model.PortalId, account.UserId, false);

                // Gets the portal ids.
                SetAssignedPortal(model, true);
                //Create link for new register user which is register from admin side.
                //For that we send new admin user 
                if (!model.IsWebStoreUser)
                {
                    isUserCreateFromAdmin = true;
                    ForgotPassword((model.PortalId > 0) ? model.PortalId.Value : PortalId, model, isUserCreateFromAdmin);
                }
                //If firstName is empty,then bind username (at the time of new user creation from webstore).
                model.FirstName = model?.FirstName ?? model?.User?.Username;
                //map account model to account entity.
                if (!SendEmailToStoreAdministrator(model.FirstName, password, account.Email, (model.PortalId > 0) ? model.PortalId.Value : PortalId, model.LocaleId, true, model.IsWebStoreUser, isUserCreateFromAdmin))
                {
                    UserModel accountModel = UserMap.ToModel(account, user.Email, !string.IsNullOrEmpty(model.User?.Username) ? model.User.Username : model.UserName);
                    accountModel.IsEmailSentFailed = true;
                    return accountModel;
                }
                ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                return UserMap.ToModel(account, user.Email, !string.IsNullOrEmpty(model.User.Username) ? model.User?.Username : model.UserName);
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorUserDataSave, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                throw new ZnodeException(ErrorCodes.ExceptionalError, Admin_Resources.ErrorUserCreate);
            }
        }

        //Insert all the profiles of the account to which user is associated.
        private void InsertUserProfile(UserModel model, ZnodeUser account)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            if (account.AccountId > 0)
            {
                List<ZnodeAccountProfile> associatedProfile = _accountAssociatedProfileRepository.Table.Where(x => x.AccountId == account.AccountId).ToList();

                if (associatedProfile?.Count > 0)
                {
                    //Get all the profiles of user associated account.
                    List<int?> accountProfileIds = associatedProfile.Select(x => x.ProfileId).ToList();
                    ZnodeLogging.LogMessage("accountProfileIds list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, accountProfileIds?.Count());
                    if (accountProfileIds?.Count > 0)
                    {
                        int defaultProfile = associatedProfile.Where(x => x.IsDefault)?.FirstOrDefault()?.ProfileId ?? 0;
                        ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { defaultProfile = defaultProfile });

                        //Create entities to insert
                        List<ZnodeUserProfile> entiesToinsert = new List<ZnodeUserProfile>();
                        foreach (int? item in accountProfileIds)
                            entiesToinsert.Add(new ZnodeUserProfile() { UserId = account.UserId, ProfileId = Convert.ToInt32(item), IsDefault = false });

                        //If account profile contains a profile already present in user then simply set it as IsDefault else add it to the entities to be inserted.
                        if (defaultProfile > 0 && accountProfileIds.Contains(defaultProfile))
                            entiesToinsert.Where(x => x.ProfileId == defaultProfile).ToList().ForEach(x => x.IsDefault = true);
                        else
                            entiesToinsert.Add(new ZnodeUserProfile() { UserId = account.UserId, ProfileId = defaultProfile > 0 ? defaultProfile : model.ProfileId, IsDefault = true });
                        ZnodeLogging.LogMessage("entiesToinsert list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, entiesToinsert?.Count());

                        ZnodeLogging.LogMessage(IsNotNull(_accountProfileRepository.Insert(entiesToinsert)) ? Admin_Resources.SuccessUserProfileInsert : Admin_Resources.ErrorUserProfileInsert, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                    }
                }
            }
            else
            {
                _accountProfileRepository.Insert(new ZnodeUserProfile { UserId = account.UserId, ProfileId = model.ProfileId, IsDefault = true });
            }
        }

        //Get expands and add them to navigation properties
        private List<string> GetExpands(NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            List<string> navigationProperties = new List<string>();
            if (expands?.HasKeys() ?? false)
            {
                foreach (string key in expands.Keys)
                {
                    //Add expand keys
                    if (key.Equals(ZnodeAccountUserOrderApprovalEnum.ZnodeUser.ToString(), StringComparison.InvariantCultureIgnoreCase)) SetExpands(ZnodeAccountUserOrderApprovalEnum.ZnodeUser.ToString(), navigationProperties);
                    if (Equals(key, FilterKeys.WishLists)) SetExpands(ZnodeUserEnum.ZnodeUserWishLists.ToString(), navigationProperties);
                    if (Equals(key, FilterKeys.Profiles)) SetExpands(ZnodeUserEnum.ZnodeUserProfiles.ToString(), navigationProperties);
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return navigationProperties;
        }

        //Get expands and add them to navigation properties.
        private List<string> GetSocialLoginExpands(NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            List<string> navigationProperties = new List<string>();
            if (expands?.HasKeys() ?? false)
            {
                foreach (string key in expands.Keys)
                {
                    //Add expand keys
                    if (key.Equals(ZnodePortalLoginProviderEnum.ZnodeLoginProvider.ToString(), StringComparison.InvariantCultureIgnoreCase)) SetExpands(ZnodePortalLoginProviderEnum.ZnodeLoginProvider.ToString(), navigationProperties);
                    if (key.Equals(ZnodePortalLoginProviderEnum.ZnodeDomain.ToString(), StringComparison.InvariantCultureIgnoreCase)) SetExpands(ZnodePortalLoginProviderEnum.ZnodeDomain.ToString(), navigationProperties);
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return navigationProperties;
        }

        //Get the Login User Details
        private ApplicationUser GetAspNetUserDetails(ref UserModel model, int portalId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

            if (IsNull(model.User))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.LoginUserModelNotNull);

            //Get the User Details from Znode Mapper
            AspNetZnodeUser znodeUser = LoginHelper.GetUser(model, (model?.PortalId > 0) ? Convert.ToInt32(model.PortalId) : portalId);
            if (IsNull(znodeUser))
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.ErrorUserNotExist, model.User.Username));

            //Bind the Aspnet Znode User Id.
            model.AspNetZnodeUserId = znodeUser.AspNetZnodeUserId;

            //Get the Login User Details from Owin Mapper.
            var user = UserManager.FindByName(znodeUser.AspNetZnodeUserId);
            if (IsNull(user))
                throw new ZnodeException(ErrorCodes.InvalidData, string.Format(Admin_Resources.ErrorUserExist, model.User.Username));
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return user;
        }

        //Sends the reset password.
        private void SendResetPassword(UserModel model, ApplicationUser user, bool isUserCreateFromAdmin = false, bool isAdminUser = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //This method is used to generate password reset token.
            string passwordResetToken = UserManager.GeneratePasswordResetToken(user.Id);
            string baseUrl = string.IsNullOrEmpty(model.BaseUrl) ? ZnodeApiSettings.AdminWebsiteUrl : model.BaseUrl;
            if (isUserCreateFromAdmin && isAdminUser != true)
            {
                //Get Portal details as well as the Domain Url based on the WEbStore Application Type.
                PortalModel portalModel = GetCustomPortalDetails(model.PortalId.Value);
                baseUrl = (IsNotNull(portalModel)) ? $"{HttpContext.Current.Request.Url.Scheme}://{portalModel.DomainUrl}" : ZnodeApiSettings.AdminWebsiteUrl;
            }
            //Generate the Password Reset Url
            string passwordResetUrl = $"{baseUrl}/User/ResetPassword?passwordToken={WebUtility.UrlEncode(passwordResetToken)}&userName={WebUtility.UrlEncode(model.User.Username)}";
            string subject = string.Empty;
            bool isHtml = false;
            bool isEnableBcc = false;
            //Generate Reset Password Email Content.

            try
            {
                string messageText = GenerateResetPasswordEmail(string.IsNullOrEmpty(model.UserName) ? model.User.Username : model.UserName, passwordResetUrl, $"<a href=\"{passwordResetUrl}\"> here</a>", model.PortalId.Value, out subject, out isHtml, out isEnableBcc, model.LocaleId, isUserCreateFromAdmin);
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info, new { messageText = messageText });

                if (!string.IsNullOrEmpty(messageText))
                    //This method is used to send an email.
                    if (model.PortalId.Value > 0)
                        ZnodeEmail.SendEmail(model.PortalId.Value, model.User.Email, ZnodeEmailBase.SMTPUserName, ZnodeEmail.GetBccEmail(isEnableBcc, model.PortalId.Value, string.Empty), subject, messageText, isHtml);
                    else
                        ZnodeEmail.SendEmail(model.User.Email, ZnodeEmailBase.SMTPUserName, ZnodeEmail.GetBccEmail(isEnableBcc, model.PortalId.Value, string.Empty), subject, messageText, isHtml);
                else
                    throw new ZnodeException(ErrorCodes.EmailTemplateDoesNotExists, Admin_Resources.ErrorResetPasswordLinkReset);
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Warning);
                model.IsEmailSentFailed = true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Error);
                model.IsEmailSentFailed = true;
                throw new ZnodeException(ErrorCodes.ErrorSendResetPasswordLink, Admin_Resources.ErrorSendResetPasswordLink);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Save the portal for the account.
        private void InsertIntoUserDepartment(UserModel model, bool isUpdate = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = model.AccountId });

            if (isUpdate)
            {
                //If record exists then return.
                if (_departmentUserRepository.Table.Any(x => x.UserId == model.UserId && x.DepartmentId == model.DepartmentId))
                    return;

                //Delete the existing entry.
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeDepartmentUserEnum.UserId.ToString(), ProcedureFilterOperators.Equals, model.UserId.ToString()));

                ZnodeLogging.LogMessage(_departmentUserRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClause(filters.ToFilterDataCollection()))
                    ? string.Format(Admin_Resources.SuccessDeleteUserDepartment, model.DepartmentName) : string.Format(Admin_Resources.ErrorDeleteUserDepartment, model.DepartmentName), ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            }

            if (model.DepartmentId > 0)
                ZnodeLogging.LogMessage(_departmentUserRepository.Insert(GetDepartmentUserEntity(model))?.DepartmentUserId > 0
                    ? string.Format(Admin_Resources.SuccessInsertUserDepartment, model.DepartmentName) : string.Format(Admin_Resources.ErrorInsertUserDepartment, model.DepartmentName), ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Get the Default Anonymous Profile Details based on the Portal
        private ProfileModel GetDefaultAnonymousProfile(int portalId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            //Get the Default Anonymous Profile Id associated to the Portal.
            ZnodePortalProfile portalProfile = _portalProfileRepository.Table.Where(x => x.PortalId == portalId && x.IsDefaultAnonymousProfile)?.FirstOrDefault();

            //Get the Profile based on the Profile Id.
            if (IsNotNull(portalProfile))
            {
                ProfileModel profile = _profileRepository.Table.Where(x => x.ProfileId == portalProfile.ProfileId)?.FirstOrDefault()?.ToModel<ProfileModel>();
                if (IsNotNull(profile))
                    profile.IsDefault = portalProfile.IsDefaultAnonymousProfile;
                return profile;
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return null;
        }

        //Get the Default Register Profile Details based on the Portal
        private ProfileModel GetDefaultRegisteredProfile(int portalId)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });

            //Get the Default Anonymous Profile Id associated to the Portal.
            var portalProfile = _portalProfileRepository.Table.Where(x => x.PortalId == portalId && x.IsDefaultRegistedProfile)?.FirstOrDefault();

            //Get the Profile based on the Profile Id.
            if (IsNotNull(portalProfile))
                return _profileRepository.Table.Where(x => x.ProfileId == portalProfile.ProfileId)?.FirstOrDefault()?.ToModel<ProfileModel>();

            return null;
        }

        //Get the Profiles Associated to the Customer based on the User Id.
        private List<ProfileModel> GetCustomerProfileByUserId(int userId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { userId = userId });

            //Get the Profiles based on userId.
            return (from userProfile in _userProfileRepository.Table
                    join profile in _profileRepository.Table on userProfile.ProfileId equals profile.ProfileId
                    where userProfile.UserId == userId
                    select new ProfileModel()
                    {
                        ProfileId = profile.ProfileId,
                        ProfileName = profile.ProfileName,
                        Weighting = profile.Weighting,
                        TaxExempt = profile.TaxExempt,
                        DefaultExternalAccountNo = profile.DefaultExternalAccountNo,
                        ShowOnPartnerSignup = profile.ShowOnPartnerSignup,
                        IsDefault = userProfile.IsDefault
                    })?.ToList();
        }

        //Get the account's portal Id.
        private int? GetAccountPortalId(int accountId)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { accountId = accountId });

            IZnodeRepository<ZnodePortalAccount> _portalAccountRepository = new ZnodeRepository<ZnodePortalAccount>();
            return _portalAccountRepository.Table.FirstOrDefault(x => x.AccountId == accountId)?.PortalId;
        }

        //Bind the User Details based on the expand collection
        protected virtual UserModel BindUserDetails(UserModel model, NameValueCollection expands)
        {
            if (IsNotNull(expands) && expands.HasKeys())
                ExpandProfiles(expands, model);
            GetUserAccountCatalogDetails(model);
            return model;
        }
        //Set User Profile Expand.
        private void ExpandProfiles(NameValueCollection expands, UserModel user)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            if (!string.IsNullOrEmpty(expands.Get(FilterKeys.Profiles)))
            {
                //Get the List of Profile associated with the user.
                user.Profiles = (from userProfile in _userProfileRepository.Table
                                 join profile in _profileRepository.Table on userProfile.ProfileId equals profile.ProfileId
                                 where userProfile.UserId == user.UserId
                                 select new ProfileModel
                                 {
                                     IsDefault = userProfile.IsDefault,
                                     ProfileId = profile.ProfileId,
                                     ProfileName = profile.ProfileName,
                                     Weighting = profile.Weighting,
                                     ShowOnPartnerSignup = profile.ShowOnPartnerSignup,
                                     TaxExempt = profile.TaxExempt,
                                     DefaultExternalAccountNo = profile.DefaultExternalAccountNo
                                 }).ToList();


            }
        }

        /// <summary>
        /// This method will return the Profile Id on the basis of portal id.
        /// </summary>
        /// <param name="portalId">integer Portal Id</param>
        /// <returns>Returns the profile id</returns>
        private int GetCurrentProfileId(int portalId)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = portalId });
            int profileID = 0;
            //Get the Register Profile Details based on the portalId.
            ProfileModel profile = GetDefaultRegisteredProfile(portalId);

            if (HelperUtility.IsNotNull(profile))
                profileID = profile.ProfileId;
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return profileID;
        }

        //Generate Reset Password Email Content based on the Email Template
        private string GenerateResetPasswordEmail(string firstName, string resetUrl, string resetLink, int portalId, out string subject, out bool isHtml, out bool isEnableBcc, int localeId = 0, bool isUserCreateFromAdmin = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { firstName = firstName, portalId = portalId });

            // Send email to the user
            string messageText = string.Empty;
            subject = "Reset Password Notification";
            isHtml = false;
            isEnableBcc = false;
            //Method to get Email Template Details.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode((isUserCreateFromAdmin ? "NewUserAccount" : "ResetPassword"), portalId, localeId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                string storeLogo = GetCustomPortalDetails(portalId)?.StoreLogo;
                ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info, new { storeLogo = storeLogo });

                //Set Parameters for the Email Templates to be get replaced.
                SetParameterOfEmailTemplate(firstName, resetUrl, resetLink, storeLogo);

                //Replace the Email Template Keys, based on the passed email template parameters.
                messageText = EmailTemplateHelper.ReplaceTemplateTokens(emailTemplateMapperModel.Descriptions);
                subject = emailTemplateMapperModel.Subject;
                isHtml = true;
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return messageText;
        }

        //Set Parameters for the Email Templates to be get replaced.
        private static void SetParameterOfEmailTemplate(string firstName, string resetUrl, string resetLink, string storeLogo)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { firstName = firstName });

            EmailTemplateHelper.SetParameter("#FirstName#", firstName);
            EmailTemplateHelper.SetParameter("#UserName#", firstName);
            EmailTemplateHelper.SetParameter("#Link#", resetLink);
            EmailTemplateHelper.SetParameter("#Url#", resetUrl);
            EmailTemplateHelper.SetParameter("#StoreLogo#", storeLogo);
        }

        //Get the assigned portal id for user.
        private void SetAssignedPortal(UserModel userModel, bool isCustomerEditMode)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //Check for userId greater than 0 and sent form customer edit mode.
            if (userModel?.UserId > 0 && isCustomerEditMode)
                SetPortal(userModel);

            if (userModel?.UserId < 1 && !isCustomerEditMode && userModel.PortalIds.Any())
                userModel.PortalId = Convert.ToInt32(userModel.PortalIds.FirstOrDefault());
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        private void SetAddressName(int userId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { userId = userId });

            if (userId > 0)
            {
                AddressModel addressModel = GetAddressByUserId(userId);

                //Sets the IsDefaultBilling and IsDefaultShipping flag to false in database for Customer.
                AddressHelper.SetAddressFlagsToFalse(addressModel);

                _addressRepository.Update(addressModel.ToEntity<ZnodeAddress>());
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

        }

        //Sets the portal ids.
        private void SetPortal(UserModel userModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //If the user belongs to the account then fetch the account's portal Id otherwise user portal id.
            if (userModel.AccountId > 0)
                userModel.PortalId = GetAccountPortalId(Convert.ToInt32(userModel.AccountId));
            else
            {
                userModel.PortalIds = _userPortalRepository.Table.Where(x => x.UserId == userModel.UserId)?.Select(x => x.PortalId.ToString())?.ToArray();
                userModel.PortalId = string.IsNullOrEmpty(userModel.PortalIds?.FirstOrDefault()) ? (int?)null : Convert.ToInt32(userModel.PortalIds?.FirstOrDefault());
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Sets the customer details.
        private void SetCustomerDetails(ApplicationUser userDetails, UserModel accountDetails, ZnodeUser account)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //Get the role Id.
            if (userDetails.Roles?.Count > 0)
                accountDetails.RoleId = userDetails.Roles.Select(x => x.RoleId).FirstOrDefault();
            //Get the role name.
            IRoleService role = GetService<IRoleService>();
            accountDetails.RoleName = role.GetRole(accountDetails?.RoleId)?.Name;

            //Set the user details
            accountDetails.User.UserId = userDetails.Id;
            accountDetails.User.Username = LoginHelper.GetUserById(userDetails.UserName)?.UserName;
            accountDetails.UserName = accountDetails.User.Username;
            accountDetails.User.Email = userDetails.Email;

            //Check for is b2b customer.

            if (LoginHelper.IsAccountCustomer(accountDetails))
            {
                //Get the user permission.
                UserModel accessPermissionEntity = GetUserAccessPermission(account.UserId);

                if (IsNotNull(accessPermissionEntity))
                {
                    accountDetails.PermissionCode = accessPermissionEntity.PermissionCode;
                    accountDetails.PermissionsName = accessPermissionEntity.PermissionsName;
                    accountDetails.AccountPermissionAccessId = accessPermissionEntity.AccountPermissionAccessId;
                    accountDetails.AccountUserPermissionId = accessPermissionEntity.AccountUserPermissionId;
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Remove all the users from list who is having role 'AccountUser'.
        private void RemoveAcccountUsers(bool isApproval, UserListModel list)
        {
            if (isApproval && list.Users.Count > 0)
                list.Users.RemoveAll(x => x.RoleName == ZnodeConstant.AccountUser);
        }

        //Get User data and bind to update model.
        private UserModel GetAndBindUserData(UserModel userModel)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodeUserEnum.UserId.ToString(), ProcedureFilterOperators.Equals, userModel.UserId.ToString()));
            filters.Add(new FilterTuple(FilterKeys.IsWebstore, ProcedureFilterOperators.Equals, "true"));
            UserModel userdata = GetUserList(userModel.UserId, null, filters, null, null).Users.FirstOrDefault();
            if (IsNotNull(userdata))
            {
                userdata.UserName = userModel.UserName;
                userdata.FirstName = userModel.FirstName;
                userdata.LastName = userModel.LastName;
                userdata.PhoneNumber = userModel.PhoneNumber;
                userdata.Email = userModel.Email;
                userdata.EmailOptIn = userModel.EmailOptIn;
                userdata.CustomerPaymentGUID = userModel?.CustomerPaymentGUID;
                userdata.ExternalId = userModel.ExternalId;
                userdata.Custom1 = userModel.Custom1;
                userdata.Custom2 = userModel.Custom2;
                userdata.Custom3 = userModel.Custom3;
                userdata.Custom4 = userModel.Custom4;
                userdata.Custom5 = userModel.Custom5;
            }
            return userdata;
        }

        //Check if user already subscribed for news letter.
        private bool IsUserAlreadyExist(string emailId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { emailId = emailId });

            List<UserModel> userData = (from user in _userRepository.Table
                                        join userPortal in _userPortalRepository.Table on user.UserId equals userPortal.UserId
                                        where user.Email == emailId && userPortal.PortalId == PortalId && user.EmailOptIn
                                        select new UserModel()
                                        {
                                            PortalId = userPortal.PortalId,
                                            EmailOptIn = user.EmailOptIn
                                        }).ToList();
            if (userData?.Count > 0)
                return (userData?.FirstOrDefault().PortalId == PortalId && userData?.FirstOrDefault().EmailOptIn == true);

            return false;
        }

        //Get Address by userId.
        private AddressModel GetAddressByUserId(int userId)
         => (from user in _userRepository.Table
             join userAddress in _userAddressRepository.Table on user.UserId equals userAddress.UserId
             join address in _addressRepository.Table on userAddress.AddressId equals address.AddressId
             where user.UserId == userId
             select new AddressModel
             {
                 Address1 = address.Address1,
                 Address2 = address.Address2,
                 Address3 = address.Address3,
                 CountryName = address.CountryName,
                 CityName = address.CityName,
                 AddressId = address.AddressId,
                 AlternateMobileNumber = address.AlternateMobileNumber,
                 DisplayName = address.DisplayName ?? ZnodeConstant.DefaultAddress,
                 ExternalId = address.ExternalId,
                 FaxNumber = address.FaxNumber,
                 FirstName = address.FirstName,
                 IsActive = address.IsActive,
                 LastName = address.LastName,
                 Mobilenumber = address.Mobilenumber,
                 PhoneNumber = address.PhoneNumber,
                 PostalCode = address.PostalCode,
                 StateName = address.StateName,
                 UserId = userId,
                 IsDefaultBilling = true,
                 IsDefaultShipping = true
             }).FirstOrDefault();


        //Add entry in ZnodeUserProfile table.
        private void InsertIntoUserProfile(ZnodeUser insertUser)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeUserProfile insertUserProfile = _userProfileRepository.Insert(new ZnodeUserProfile() { UserId = insertUser.UserId, ProfileId = GetDefaultAnonymousProfile(PortalId).ProfileId });
            ZnodeLogging.LogMessage(insertUserProfile?.UserProfileID > 0 ? string.Format(Admin_Resources.SuccessSaveProfileEntryForEmailSubscription, insertUserProfile?.UserProfileID) : Admin_Resources.ErrorSaveProfileEntryForEmailSubscription, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Add entry in ZnodeUserAddress table.
        private void InsertIntoZnodeUserAddress(ZnodeUser insertUser, ZnodeAddress insertAddress)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeUserAddress insertUserAddress = _userAddressRepository.Insert(new ZnodeUserAddress() { AddressId = insertAddress.AddressId, UserId = insertUser.UserId });
            ZnodeLogging.LogMessage(insertUserAddress?.UserAddressId > 0 ? string.Format(Admin_Resources.SuccessSaveAddressEntryForEmailSubscription, insertUserAddress?.UserAddressId) : Admin_Resources.ErrorSaveAddressEntryForEmailSubscription, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Add entry in ZnodeAddress table.
        private ZnodeAddress InsertIntoAddress()
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeAddress insertAddress = _addressRepository.Insert(new ZnodeAddress() { StateName = string.Empty, PostalCode = string.Empty, CityName = string.Empty });
            ZnodeLogging.LogMessage(insertAddress?.AddressId > 0 ? string.Format(Admin_Resources.SuccessSaveAddressDataForEmailSubscription, insertAddress?.AddressId) : Admin_Resources.ErrorAddressUserDataForEmailSubscription, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return insertAddress;
        }

        //Add entry in ZnodeUser table.
        private ZnodeUser InsertIntoZnodeUser(NewsLetterSignUpModel model)
        {
            ZnodeUser insertUser = _userRepository.Insert(new ZnodeUser() { Email = model.Email, EmailOptIn = true });
            ZnodeLogging.LogMessage(insertUser?.UserId > 0 ? string.Format(Admin_Resources.SuccessSaveUserDataForEmailSubscription, insertUser.UserId) : Admin_Resources.ErrorSaveUserDataForEmailSubscription, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return insertUser;
        }

        //Bind user datails required after login.
        protected virtual UserModel BindDetails(UserModel model, ApplicationUser user, NameValueCollection expand)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            //Gets the User Details by User Id.
            model = GetUserByUserId(user.Id, expand);
            //Get role id.
            model.User.RoleId = user.Roles.Select(s => s.RoleId).FirstOrDefault();
            //check whether the current login user has Customer user and B2B role or not.
            model.IsAdminUser = IsAdminUser(user);
            //Get the role name.

            IRoleService role = GetService<IRoleService>();
            model.RoleName = role.GetRole(model.User.RoleId)?.Name;

            //Get user address list.
            model.Addresses = GetAddressList(model);
            //Get user access permissiom.
            model.PermissionCode = GetUserAccessPermission(model.UserId)?.PermissionCode;
            model.ProfileId = Convert.ToInt32(model.Profiles?.Select(x => x.ProfileId).FirstOrDefault());

            GetUserAccountCatalogDetails(model);

            //Get user contact number and external id.
            ZnodeUser userDetails = _userRepository.Table.FirstOrDefault(x => x.AspNetUserId == model.User.UserId);
            if (IsNotNull(userDetails))
            {
                model.PhoneNumber = userDetails.PhoneNumber;
                model.ExternalId = userDetails.ExternalId;
                model.BudgetAmount = userDetails.BudgetAmount;
            }
            ZnodeLogging.LogMessage("ExternalId:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, model.ExternalId);
            ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return GetUsersAdditionalAttributes(model);
        }

        //Get the details of global attribute associated with store and user.
        private UserModel GetUsersAdditionalAttributes(UserModel model)
        {
            model.PortalId = PortalId;
            GetGlobalDetails(model);
            return model;

        }

        //gets the budget amount for the user
        private void GetGlobalDetails(UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = model.AccountId });

            List<GlobalAttributeValuesModel> allUserAttributes = GetGlobalLevelAttributeList(model.UserId, ZnodeConstant.User);
            List<GlobalAttributeValuesModel> allStoreAttributes = GetGlobalLevelAttributeList(model.PortalId.GetValueOrDefault(), ZnodeConstant.Store);
            ZnodeLogging.LogMessage("List Count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { allUserAttributes = allUserAttributes?.Count(), allStoreAttributesCount = allStoreAttributes?.Count() });

            model.BillingAccountNumber = allUserAttributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.BillingAccountNumber)?.AttributeValue;
            model.PerOrderLimit = Convert.ToDecimal(allUserAttributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.PerOrderLimit)?.AttributeValue);
            model.AnnualOrderLimit = Convert.ToDecimal(allUserAttributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.PerOrderAnnualLimit)?.AttributeValue);
            int annualOrderLimitStartMonth = model.PortalId.GetValueOrDefault() > 0 ? Convert.ToInt32(allStoreAttributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.PerOrderAnnualLimitStartMonth && x.AttributeValue != null)?.AttributeValue) : 1;
            ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { annualOrderLimitStartMonth = annualOrderLimitStartMonth });

            model.AnnualBalanceOrderAmount = CalculateUserBalanceAmount(annualOrderLimitStartMonth, model.UserId, model.AnnualOrderLimit);
            model.UserGlobalAttributes = allUserAttributes;
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Calculate the annual balance amount for the user
        private decimal CalculateUserBalanceAmount(int annualOrderLimitStartMonth, int userId, decimal annualOrderLimit)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { annualOrderLimitStartMonth = annualOrderLimitStartMonth, userId = userId, annualOrderLimit = annualOrderLimit });

            if (annualOrderLimitStartMonth > 0)
            {
                var startDate = new DateTime(DateTime.Now.Year, annualOrderLimitStartMonth, 1);
                var endDate = startDate.AddMonths(12).AddSeconds(-1);

                if (startDate > DateTime.Now)
                    return 0;

                decimal? orderTotalforYear = _orderRepository.Table.Where(x => x.UserId == userId && x.OrderDate >= startDate && x.OrderDate <= endDate)?.Sum(y => y.Total);
                decimal limit = annualOrderLimit - orderTotalforYear.GetValueOrDefault();
                ZnodeLogging.LogMessage("Order limit:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, limit);
                ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

                return limit <= 0 ? 0 : limit;

            }
            else
                return 0;
        }

        //Get user account catalog details
        private void GetUserAccountCatalogDetails(UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = model?.AccountId });

            GetPublishCatalogId(model);

            IZnodeRepository<ZnodeAccount> _accountRepository = new ZnodeRepository<ZnodeAccount>();

            if (model.AccountId > 0)
            {
                ZnodeAccount accountDetails = _accountRepository.GetById(model.AccountId.GetValueOrDefault());

                if (accountDetails?.PublishCatalogId > 0)
                    model.PublishCatalogId = accountDetails?.PublishCatalogId;
                else if (accountDetails?.ParentAccountId > 0)
                {
                    ZnodeAccount parentAccountDetails = _accountRepository.GetById(accountDetails.ParentAccountId.GetValueOrDefault());
                    if (parentAccountDetails?.PublishCatalogId > 0)
                        model.PublishCatalogId = parentAccountDetails.PublishCatalogId;
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        private void GetPublishCatalogId(UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = model?.AccountId });

            int? portalCatalogId = _portalCatalogRepository.Table.Where(x => x.PortalId == model.PortalId)?.FirstOrDefault()?.PublishCatalogId;

            ZnodeLogging.LogMessage("PublishCatalogId:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, portalCatalogId);

            if (portalCatalogId > 0)
                model.PublishCatalogId = portalCatalogId;
        }

        //Get address list.
        private List<AddressModel> GetAddressList(UserModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = model?.AccountId });

            FilterCollection filters = new FilterCollection();

            List<string> navigationProperties = new List<string>();
            navigationProperties.Add(ZnodeAccountAddressEnum.ZnodeAddress.ToString());

            //If login user is B2B get account address list else get customer address list.
            if (LoginHelper.IsAccountCustomer(model) && model.AccountId > 0)
            {
                filters.Add(ZnodeAccountEnum.AccountId.ToString(), FilterOperators.Equals, model.AccountId.ToString());

                IZnodeRepository<ZnodeAccountAddress> _accountAddressRepository = new ZnodeRepository<ZnodeAccountAddress>();
                return AccountMap.ToListModel(_accountAddressRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause, navigationProperties))?.AddressList;
            }
            else
            {
                filters.Add(ZnodeUserEnum.UserId.ToString(), FilterOperators.Equals, model.UserId.ToString());
                ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
                return UserMap.ToAddressListModel(_userAddressRepository.GetEntityList(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause, navigationProperties), null)?.AddressList;
            }

        }

        //Create guest user account.
        private UserModel CreateGuestUserAccount(UserModel userModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = userModel?.AccountId });

            //Save account details.
            ZnodeUser account = _userRepository.Insert(userModel?.ToEntity<ZnodeUser>());
            if (account?.UserId > 0 && IsNotNull(userModel))
            {
                userModel.UserId = account.UserId;
                ZnodeLogging.LogMessage(account.UserId > 0 ? Admin_Resources.SuccessUserDataSave : Admin_Resources.ErrorUserDataSave, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

                //Insert portal in UserPortal table.
                _userPortalRepository.Insert(new ZnodeUserPortal { UserId = account.UserId, PortalId = userModel.PortalId });

                //Insert profile in AccountProfile table.
                _accountProfileRepository.Insert(new ZnodeUserProfile { UserId = account.UserId, ProfileId = userModel.ProfileId, IsDefault = true });
                ZnodeLogging.LogMessage("User Id:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, userModel?.UserId);
                return account.ToModel<UserModel>();

            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return new UserModel();
        }

        //Map the login provider data domain wise.
        private List<SocialDomainModel> MapLoginProvider(List<ZnodePortalLoginProvider> allConfiguration, List<ZnodePortalLoginProvider> domainWise)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            List<SocialDomainModel> socialDomainList = new List<SocialDomainModel>();
            //Maps the fields into model.
            foreach (ZnodePortalLoginProvider item in domainWise)
            {
                SocialDomainModel socialDomainModel = new SocialDomainModel();
                socialDomainModel.DomainId = item.DomainId;
                socialDomainModel.PortalId = item.PortalId;
                socialDomainModel.DomainName = item.ZnodeDomain?.DomainName;
                socialDomainModel.SocialTypeList = allConfiguration.Where(x => x.DomainId == item.DomainId).Select(type => new SocialTypeModel() { Key = string.IsNullOrEmpty(type.ProviderKey) ? ZnodeLogging.Components.Admin.ToString() : type.ProviderKey.Trim(), SecretKey = string.IsNullOrEmpty(type.SecretKey) ? string.Empty : type.SecretKey.Trim(), ProviderName = type.ZnodeLoginProvider?.Name }).ToList();
                socialDomainList.Add(socialDomainModel);
            }
            ZnodeLogging.LogMessage("socialDomainList list count:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, socialDomainList?.Count());
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return socialDomainList;
        }

        //Get portal id from filters.
        private int GetPortalId(FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            int portalId = 0;
            if (filters.Any(x => string.Equals(x.FilterName, FilterKeys.PortalId, StringComparison.CurrentCultureIgnoreCase)))
            {
                Int32.TryParse(filters.FirstOrDefault(x => string.Equals(x.FilterName, FilterKeys.PortalId, StringComparison.CurrentCultureIgnoreCase))?.FilterValue, out portalId);
                filters.RemoveAll(x => x.FilterName.Equals(FilterKeys.PortalId, StringComparison.InvariantCultureIgnoreCase));
            }
            ZnodeLogging.LogMessage("Portal Id:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, portalId);

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return portalId;
        }

        //Add guest user filter.
        private FilterCollection AddGuestUserFilter(FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            string filterValue = filters.Find(x => x.FilterName.Equals(FilterKeys.IsGuestUser, StringComparison.InvariantCultureIgnoreCase))?.FilterValue;
            filterValue = Convert.ToBoolean(filterValue) ? FilterKeys.ActiveTrue : FilterKeys.ActiveFalse;
            filters.RemoveAll(x => x.FilterName.Equals(FilterKeys.IsGuestUser, StringComparison.InvariantCultureIgnoreCase));
            filters.Add(FilterKeys.IsGuestUser, FilterOperators.Equals, filterValue);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return filters;
        }

        //Set the email as an username for external login.
        private void SetUserNameForSocialLoginUser(UserModel model)
        {
            //If the user not exits by email then store email as a username else not.
            if (!string.IsNullOrEmpty(model.User.Email) && !LoginHelper.IsUserExists(model.Email, model.PortalId.GetValueOrDefault()))
                model.User.Username = model.Email;
        }

        //Update user data in user table.
        public bool UpdateUser(UserModel userModel, bool webStoreUser)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = userModel.AccountId });

            bool updatedAccount = false;
            if (webStoreUser)
            {
                //Get User data and bind to update model.
                UserModel userdata = GetAndBindUserData(userModel);


                var _entity = userdata.ToEntity<ZnodeUser>();

                if (IsNotNull(userdata))
                    updatedAccount = _userRepository.Update(_entity);

                if (updatedAccount)
                    new ERPInitializer<ZnodeUser>(_entity, "EmailUpdate");
            }
            else
                updatedAccount = _userRepository.Update(userModel.ToEntity<ZnodeUser>());
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return updatedAccount;
        }

        //Gets the User Details by User Id.
        private UserModel GetUserByUserId(string userId, NameValueCollection expand)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { userId = userId });

            if (!string.IsNullOrEmpty(userId))
            {
                //Create tupple to generate where clause.
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeUserEnum.AspNetUserId.ToString(), ProcedureFilterOperators.Contains, userId));

                //Generating where clause to get account details.
                EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                List<string> navigationProperties = GetExpands(expand);

                //This method is used to get the user details.
                ZnodeUser account = _userRepository.GetEntity(whereClauseModel.WhereClause, navigationProperties, whereClauseModel.FilterValues);
                if (IsNotNull(account))
                    //Bind the User details based on the expand collection
                    return BindUserDetails(UserMap.ToModel(account, string.Empty), expand);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return new UserModel();
        }

        public UserModel GetUserAccessPermission(int userId)
        {
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { userId = userId });

            IZnodeRepository<ZnodeAccountUserPermission> _userPermission = new ZnodeRepository<ZnodeAccountUserPermission>();

            //Get the user permission.
            return (from userPermission in _userPermission.Table
                    join accountPermission in _accountPermissionAccessRepository.Table on userPermission.AccountPermissionAccessId equals accountPermission.AccountPermissionAccessId
                    join accessPermission in _accessPermissionRepository.Table on accountPermission.AccessPermissionId equals accessPermission.AccessPermissionId
                    where userPermission.UserId == userId
                    select new UserModel()
                    {
                        PermissionCode = accessPermission.PermissionCode,
                        AccountPermissionAccessId = accountPermission.AccountPermissionAccessId,
                        PermissionsName = accessPermission.PermissionsName,
                        AccountUserPermissionId = userPermission.AccountUserPermissionId
                    })?.FirstOrDefault();
        }

        //Convert shopper to admin functionality.
        public virtual UserModel ConvertShopperToAdmin(UserModel model)
        {
            ZnodeLogging.LogMessage("Execution Started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { portalId = model.PortalId });

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.UserModelNotNull);

            if (IsNull(model.User))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.LoginUserModelNotNull);

            //Get user details.
            var aspNetZnodeUserId = LoginHelper.GetUser(model, null)?.AspNetZnodeUserId;
            ApplicationUser user = UserManager.FindByName(aspNetZnodeUserId);
            model.UserId = IsNotNull(_userRepository.Table.FirstOrDefault(x => x.Email.Equals(model.UserName))) ? _userRepository.Table.FirstOrDefault(x => x.Email.Equals(model.UserName)).UserId : 0;

            //Updates role into AspnetUserRole
            UpdateUserRole(model, user);

            //To convert shopper to admin, update accountId to null for the user.
            var userAccount = _userRepository.Table.FirstOrDefault(x => x.UserId == model.UserId);
            if (IsNotNull(userAccount))
            {
                userAccount.FirstName = model.FirstName;
                userAccount.LastName = model.LastName;
                userAccount.PhoneNumber = model.LastName;
                userAccount.AccountId = null;
                _userRepository.Update(userAccount);
            }

            //associate the user to given portal ids. 
            var userPortals = _userPortalRepository.Table.Where(x => x.UserId == model.UserId)?.ToList();
            if (userPortals.Count > 0)
            {
                _userPortalRepository.Delete(userPortals);
            }

            if (model.PortalIds?.Length > 0)
            {
                foreach (var portalId in model.PortalIds)
                {
                    var userPortal = new ZnodeUserPortal() { UserId = model.UserId };
                    if (portalId.Equals("0"))
                        userPortal.PortalId = null;//If potalId not specified.
                    else
                        userPortal.PortalId = Convert.ToInt32(portalId);
                    _userPortalRepository.Insert(userPortal);
                }
            }
            //update portal id as null in AspNetZnodeUser
            var aspnetznodeUser = _aspNetZnodeuserRepository.Table.FirstOrDefault(x => x.AspNetZnodeUserId == aspNetZnodeUserId);
            if (IsNotNull(aspnetznodeUser))
            {
                aspnetznodeUser.PortalId = null;
                _aspNetZnodeuserRepository.Update(aspnetznodeUser);
            }

            //update the User profile with default anonymous profile.
            var userProfile = _userProfileRepository.Table.FirstOrDefault(x => x.UserId == model.UserId);
            if (IsNotNull(userProfile))
            {
                 userProfile.ProfileId = _portalProfileRepository.Table.FirstOrDefault(x => x.IsDefaultRegistedProfile == true)?.ProfileId;
                _userProfileRepository.Update(userProfile);
            }

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return model;
        } 

        //Method to update guest user account.
        private void UpdateGuestUserAccount(UserModel userModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, new { AccountId = userModel.AccountId });

            //Save the user login Details.
            ApplicationUser user = CreateOwinUser(userModel);

            //Save account details.
            _userRepository.Update(userModel.ToEntity<ZnodeUser>());

            ZnodeUser account = _userRepository.GetById(userModel.UserId);
            ZnodeLogging.LogMessage("UserId:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose,userModel?.UserId );
            if (userModel?.UserId > 0)
            {
                ZnodeLogging.LogMessage(userModel?.UserId > 0 ? Admin_Resources.SuccessUpdateUserData : Admin_Resources.ErrorUpdateAccountData, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);

                //Insert data for guest user.
                InsertDataForGuestUser(userModel, user, account);

                //map account model to account entity.
                if (!SendEmailToStoreAdministrator(userModel.FirstName, userModel.User.Password, userModel.Email, (userModel.PortalId > 0) ? userModel.PortalId.Value : PortalId, userModel.LocaleId, true))
                {
                    UserModel accountModel = UserMap.ToModel(account, user.Email);
                    accountModel.IsEmailSentFailed = true;
                }
                 UserMap.ToModel(account, user.Email);
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorUpdateAccountData, string.Empty, TraceLevel.Info);
                throw new ZnodeException(ErrorCodes.ExceptionalError, Admin_Resources.ErrorUpdateUser);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
        }

        //Get the values from filter.
        private FilterCollection GetValuesFromFilter(int loggedUserAccountId, FilterCollection filters, out bool isApproval, out bool isWebstore, out string currentUserName, out string currentRoleName, out int portalId, out bool isCustomerEditMode)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input Parameter:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose,new { loggedUserAccountId = loggedUserAccountId });

            isApproval = false;
            isWebstore = filters.Any(x => string.Equals(x.FilterName, FilterKeys.IsWebstore, StringComparison.CurrentCultureIgnoreCase));
            bool IsStoreAdmin = filters.Any(x => string.Equals(x.FilterName, FilterKeys.IsStoreAdmin, StringComparison.CurrentCultureIgnoreCase));

            filters.RemoveAll(x => x.FilterName.Equals(FilterKeys.IsWebstore, StringComparison.InvariantCultureIgnoreCase));
            filters.RemoveAll(x => x.FilterName.Equals(FilterKeys.IsStoreAdmin, StringComparison.InvariantCultureIgnoreCase));

            UserModel userModel = GetUserById(loggedUserAccountId, null);
            currentUserName = userModel?.User?.Username;
            currentRoleName = IsStoreAdmin ? userModel?.RoleName : string.Empty;
            //Check if filter contains guest user.
            if (filters.Any(x => x.FilterName.Equals(FilterKeys.IsGuestUser, StringComparison.InvariantCultureIgnoreCase)))
                filters = AddGuestUserFilter(filters);

            //Get portal id from filters.
            portalId = GetPortalId(filters);

            //Check if rolename exists in filters and remove it.
            IsRoleNameExists(filters, ref isApproval, ref currentUserName, ref currentRoleName);

            //Check if filters contains customereditmode and remove it.
            isCustomerEditMode = IsCustomerEditMode(filters);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return filters;
        }

        /// <summary>
        /// Set user id in Header specifally in case of new user created from webstore or password reset through link send on mail
        /// </summary>
        /// <param name="AspNetUserId"> Parameter used to get userId from AspNetUserId</param>
        private void SetRequestHeaderForPasswordLog(string AspNetUserId = null, int userId = 0)
        {
            if (HelperMethods.GetLoginUserId() <= 0)
            {
                if (!string.IsNullOrEmpty(AspNetUserId))
                {
                    userId = _userRepository.Table.Where(x => x.AspNetUserId == AspNetUserId).Select(x => x.UserId).FirstOrDefault();
                }
                if (userId != null && userId > 0)
                {
                    HttpContext.Current.Request.Headers.Add("Znode-UserId", Convert.ToString(userId));
                }
            }
        }
        #endregion
    }
}