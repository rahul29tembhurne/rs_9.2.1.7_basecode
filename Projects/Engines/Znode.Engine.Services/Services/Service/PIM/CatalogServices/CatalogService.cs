﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Helper;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Observer;
using Znode.Libraries.Resources;
using EntityFramework.Extensions;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using System.Threading.Tasks;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
using System.Runtime.Remoting.Messaging;
using Znode.Libraries.Search;

namespace Znode.Engine.Services
{
    public class CatalogService : BaseService, ICatalogService
    {
        #region Private Variables

        private readonly IZnodeRepository<ZnodePimCatalog> _pimCatalogRepository;
        private readonly IZnodeRepository<ZnodePimCatalogCategory> _pimCatalogCategoryRepository;
        private readonly IZnodeRepository<ZnodePimCategoryHierarchy> _pimCategoryHierarchyRepository;
        private readonly IZnodeRepository<ZnodePublishCatalogLog> _publishCatalogLogRepository;
        private readonly IZnodeRepository<ZnodePublishedXml> _publishedXmlRepository;
        private readonly IMongoRepository<CatalogEntity> _catalogMongoRepository;
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private readonly IMongoRepository<ProductEntity> _productMongoRepository;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IZnodeRepository<ZnodePublishCatalog> _publishCatalogRepository;
        private readonly IMongoRepository<SeoEntity> _seoProductMongoRepository;
        private readonly IZnodeRepository<ZnodeCMSSEODetail> _seoDetailRepository;
        private readonly ProductAssociationHelper productAssociationHelper = null;
        private readonly IMongoRepository<VersionEntity> _versionRepository;
        private readonly IZnodeRepository<ZnodeSearchIndexServerStatu> _searchIndexServerStatusRepository;
        #endregion Private Variables

        #region Constructor

        public CatalogService()
        {
            _pimCatalogRepository = new ZnodeRepository<ZnodePimCatalog>();
            _pimCatalogCategoryRepository = new ZnodeRepository<ZnodePimCatalogCategory>();
            _pimCategoryHierarchyRepository = new ZnodeRepository<ZnodePimCategoryHierarchy>();
            _publishedXmlRepository = new ZnodeRepository<ZnodePublishedXml>();
            _publishCatalogLogRepository = new ZnodeRepository<ZnodePublishCatalogLog>();
            _categoryMongoRepository = new MongoRepository<CategoryEntity>();
            _catalogMongoRepository = new MongoRepository<CatalogEntity>();
            _productMongoRepository = new MongoRepository<ProductEntity>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _publishCatalogRepository = new ZnodeRepository<ZnodePublishCatalog>();
            _seoProductMongoRepository = new MongoRepository<SeoEntity>();
            _seoDetailRepository = new ZnodeRepository<ZnodeCMSSEODetail>();
            productAssociationHelper = new ProductAssociationHelper();
            _versionRepository = new MongoRepository<VersionEntity>();
            _searchIndexServerStatusRepository = new ZnodeRepository<ZnodeSearchIndexServerStatu>();
        }

        #endregion Constructor

        #region Public Methods

        //Get a list of Catalogs.
        public virtual CatalogListModel GetCatalogs(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to set parameters of SP Znode_GetCatalogList:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            IZnodeViewRepository<CatalogModel> objStoredProc = new ZnodeViewRepository<CatalogModel>();
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);

            List<CatalogModel> publishCatalogLogs = objStoredProc.ExecuteStoredProcedureList("Znode_GetCatalogList @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT", 4, out pageListModel.TotalRowCount)?.ToList();
            ZnodeLogging.LogMessage("publishCatalogLogs list count:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { publishCatalogLogsListCount = publishCatalogLogs?.Count });

            CatalogListModel publishCatalogLogList = new CatalogListModel { Catalogs = publishCatalogLogs };
            publishCatalogLogList.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return publishCatalogLogList;
        }

        //Get a Catalog using catalogId.
        public virtual CatalogModel GetCatalog(int pimCatalogId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { pimCatalogId = pimCatalogId });
            if (pimCatalogId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, PIM_Resources.ErrorCatalogIdLessThanOne);
            CatalogModel catalogModel = _pimCatalogRepository.GetById(pimCatalogId).ToModel<CatalogModel>();

            BindDefaultPortal(catalogModel);
            return catalogModel;
        }

        //Bind Default Portal to Catalog
        private void BindDefaultPortal(CatalogModel catalogModel)
        {
            ZnodeRepository<ZnodePortal> portalRepository = new ZnodeRepository<ZnodePortal>();
            catalogModel.DefaultStore = portalRepository.Table.FirstOrDefault(m => m.PortalId == catalogModel.PortalId)?.StoreName;
        }

        //Creates a new Catalog
        public virtual CatalogModel CreateCatalog(CatalogModel catalogModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if (IsNull(catalogModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);

            ZnodePimCatalog catalog = _pimCatalogRepository.Insert(catalogModel.ToEntity<ZnodePimCatalog>());
            ZnodeLogging.LogMessage(catalog?.PimCatalogId > 0 ? string.Format(PIM_Resources.SuccessCreateCatalog, catalogModel.CatalogName) : string.Format(PIM_Resources.ErrorCreateCatalog, catalogModel.CatalogName), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return catalog?.PimCatalogId > 0 ? catalog.ToModel<CatalogModel>() : null;
        }

        //Updates an already existing Catalog.
        public virtual bool UpdateCatalog(CatalogModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, PIM_Resources.ErrorCatalogModelNull);
            ZnodeLogging.LogMessage("Input parameter:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { CatalogModel = model });
            bool status = _pimCatalogRepository.Update(model.ToEntity<ZnodePimCatalog>());
            ZnodeLogging.LogMessage(status ? string.Format(PIM_Resources.SuccessUpdateCatalog, model.CatalogName) : string.Format(PIM_Resources.ErrorUpdateCatalog, model.CatalogName), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return status;
        }

        //Copy the existing Catalog.
        public virtual bool CopyCatalog(CatalogModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter("CatalogId", model.PimCatalogId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("AccountId", GetLoginUserId(), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("CatalogName", model.CatalogName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("CopyAllData", model.CopyAllData, ParameterDirection.Input, DbType.Boolean);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
            int status = 0;
            IList<View_ReturnBoolean> deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_CopyPimCatalog @CatalogId, @AccountId, @CatalogName, @CopyAllData, @Status OUT", 4, out status);
            if (deleteResult.FirstOrDefault()?.Status.Value ?? false)
            {
                ZnodeLogging.LogMessage(string.Format(PIM_Resources.SuccessCopyCatalog, model.CatalogName), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                return true;
            }
            else
            {
                ZnodeLogging.LogMessage(string.Format(PIM_Resources.ErrorCopyCatalog, model.CatalogName), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                throw new ZnodeException(ErrorCodes.AlreadyExist, PIM_Resources.AlreadyExistCatalogName);
            }

        }

        //Deletes a Catalog using catalogId.
        public virtual bool DeleteCatalog(CatalogDeleteModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if (string.IsNullOrEmpty(model?.Ids))
                throw new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.ErrorCatalogIdNull);
            ZnodeLogging.LogMessage("Catalog Ids to delete catalog:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, model?.Ids);
            View_ReturnBooleanWithMessage deleteResult = null;
            IZnodeViewRepository<View_ReturnBooleanWithMessage> objStoredProc = new ZnodeViewRepository<View_ReturnBooleanWithMessage>();
            objStoredProc.SetParameter("PimCatalogIds", model.Ids, ParameterDirection.Input, DbType.String);

            // Delete Publish and PIM Catalog
            if (model.IsDeletePublishCatalog)
            {
                objStoredProc.SetParameter("IsDeleteFromPublish", true, ParameterDirection.Input, DbType.Boolean);
                deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_DeletePimCatalog  @PimCatalogIds, @IsDeleteFromPublish")?.FirstOrDefault();
                if (IsNotNull(deleteResult?.MessageDetails))
                    PreviewHelper.DeletePublishCatalogFromMongo(Query<CatalogEntity>.In(x => x.ZnodeCatalogId, deleteResult.MessageDetails.Split(',')?.Select(int.Parse).ToList()),0,true);
            }
            else
                //Delete Pim catalog
                deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_DeletePimCatalog @PimCatalogIds")?.FirstOrDefault();

            ZnodeLogging.LogMessage(deleteResult?.Status.GetValueOrDefault() ?? false ? PIM_Resources.SuccessDeleteCatalog : PIM_Resources.ErrorDeleteCatalog, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return deleteResult?.Status.GetValueOrDefault() ?? false;
        }

        //Get tree structure of categories.
        public virtual ContentPageTreeModel GetCatgoryTreeNode(CatalogAssociationModel catalogAssociationModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter catalogAssociationModel properties:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { CatalogId = catalogAssociationModel?.CatalogId, ProfileCatalogId = catalogAssociationModel?.ProfileCatalogId, LocaleId = catalogAssociationModel?.LocaleId });
            if (catalogAssociationModel?.CatalogId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.ErrorCatalogIdLessThanOne);

            IZnodeViewRepository<TreeModel> objStoredProc = new ZnodeViewRepository<TreeModel>();
            if (IsNotNull(catalogAssociationModel))
            {
                objStoredProc.SetParameter(ZnodePimCatalogEnum.PimCatalogId.ToString(), catalogAssociationModel.CatalogId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter(ZnodeLocaleEnum.LocaleId.ToString(), catalogAssociationModel.LocaleId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter(ZnodeProfileCatalogEnum.ProfileCatalogId.ToString(), catalogAssociationModel.ProfileCatalogId, ParameterDirection.Input, DbType.Int32);
            }

            //Get all path from database.
            List<ContentPageTreeModel> list = objStoredProc.ExecuteStoredProcedureList("ZNode_GetCategoryHierarchy @PimCatalogId,@LocaleId,@ProfileCatalogId")?.Select(x => new ContentPageTreeModel
            {
                Text = x.CategoryValue,
                Id = x.PimCategoryHierarchyId,
                ParentId = x.ParentPimCategoryHierarchyId.GetValueOrDefault(),
                DisplayOrder = x.DisplayOrder,
                PimCategoryId = x.PimCategoryId.GetValueOrDefault()
            }).ToList();
            ZnodeLogging.LogMessage("ContentPageTreeModel list count:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { ContentPageTreeModelListCount = list?.Count });
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            //Convert path to parent child pattern.
            return GetAllNode(list).FirstOrDefault();
        }

        //Get Associated Catalog Hierarchy list on the basis of pim product Id.
        public virtual List<CatalogTreeModel> GetAssociatedCatalogHierarchy(int pimProductId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { pimProductId = pimProductId });

            if (pimProductId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.ErrorCatalogIdGreaterThanOne);

            IZnodeViewRepository<TreeModel> objStoredProc = new ZnodeViewRepository<TreeModel>();
            objStoredProc.SetParameter(ZnodePimProductEnum.PimProductId.ToString(), pimProductId, ParameterDirection.Input, DbType.Int32);

            //Get Catalog from database.
            List<ContentPageTreeModel> catalogList = objStoredProc.ExecuteStoredProcedureList("Znode_GetCatalogCategoryHierarchy @PimProductId")?.Select(x => new ContentPageTreeModel
            {
                Text = x.CatalogName,
                PimCatalogId = x.PimCatalogId,
                CategoryValue = x.CategoryValue
            }).ToList();
            ZnodeLogging.LogMessage("catalogList list count:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { catalogListCount = catalogList?.Count });
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            //Convert path to parent child pattern.
            return GetCatalogTreeNodes(catalogList);
        }

        //Get Catalog Tree Nodes
        protected virtual List<CatalogTreeModel> GetCatalogTreeNodes(List<ContentPageTreeModel> catalogList)
        {
            List<CatalogTreeModel> CatalogHierarchyList = new List<CatalogTreeModel>();
            if (catalogList?.Count > 0)
            {
                foreach (var item in catalogList)
                {
                    if (!CatalogHierarchyList.Any(m => m.CatalogName == item.Text))
                    {
                        CatalogTreeModel catalogtreeModel = GetCatalogNode(catalogList, _publishCatalogRepository, item);
                        CatalogHierarchyList.Add(catalogtreeModel);
                    }
                }
            }
            return CatalogHierarchyList;
        }

        //Get Single Catalog Nodes
        protected virtual CatalogTreeModel GetCatalogNode(List<ContentPageTreeModel> catalogList, IZnodeRepository<ZnodePublishCatalog> _publishCatalogRepository, ContentPageTreeModel item)
        {
            CatalogTreeModel catalogtreeModel = new CatalogTreeModel();
            catalogtreeModel.CatalogName = item.Text;
            List<ContentPageTreeModel> categories = catalogList.Where(m => m.PimCatalogId == item.PimCatalogId).Select(m => new ContentPageTreeModel { Text = m.CategoryValue }).ToList();
            ZnodeLogging.LogMessage("categories list count:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { categoriesListCount = categories?.Count });
            catalogtreeModel.Children = categories;
            catalogtreeModel.IsCatalogPublished = _publishCatalogRepository.Table.Any(m => m.PimCatalogId == item.PimCatalogId);
            return catalogtreeModel;
        }

        //Associate categories to catalog.
        public virtual bool AssociateCategoriesAndProductsToCatalog(CatalogAssociationModel catalogAssociationModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            if (IsNull(catalogAssociationModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);

            ZnodeLogging.LogMessage("Input parameter:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { catalogAssociationModel = catalogAssociationModel });

            //Associate categories to catalog for profile if ProfileCatalogId is greater than 0.
            if (catalogAssociationModel.ProfileCatalogId > 0)
                return AssociateCategoriesToCatalogForProfile(catalogAssociationModel);

            string[] categoryIds = !string.IsNullOrEmpty(catalogAssociationModel.CategoryIds) ? catalogAssociationModel.CategoryIds.Split(',') : null;
            string[] productIds = !string.IsNullOrEmpty(catalogAssociationModel.ProductIds) ? catalogAssociationModel.ProductIds.Split(',') : null;
            List<ZnodePimCategoryHierarchy> categoryHierarchy;
            if (string.IsNullOrEmpty(catalogAssociationModel.ProductIds))
            {
                categoryHierarchy = AssociatedChildCategories(catalogAssociationModel.CatalogId, catalogAssociationModel.PimCategoryHierarchyId, categoryIds);
                if (categoryHierarchy?.Count > 0)
                {
                    //Get all products associated to each category.
                    IList<ZnodePimCategoryProduct> categoryAssociatedProducts = GetAssociatedProducts(catalogAssociationModel.CategoryIds);
                    ZnodeLogging.LogMessage(PIM_Resources.SuccessGetCategoryAssociatedProducts, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

                    List<ZnodePimCatalogCategory> categoriesAssociatedToCatalog = AssociateCategoriesToCatalog(catalogAssociationModel.CatalogId, GetCategoryHierarchyIdDictionary(categoryIds, categoryHierarchy), categoryAssociatedProducts);
                    ZnodeLogging.LogMessage(PIM_Resources.SuccessAssociateCategoriesorProducts, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                    return categoriesAssociatedToCatalog?.Count > 0;
                }
            }
            else
                return AssociateProductsToCatalog(catalogAssociationModel.CatalogId, catalogAssociationModel.CategoryId.GetValueOrDefault(), catalogAssociationModel.PimCategoryHierarchyId, productIds);

            return false;
        }

        // Get associated categories.
        public virtual CatalogAssociateCategoryListModel GetAssociatedCategories(FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            int localeId = 0;
            if (IsNotNull(filters))
            {
                localeId = Convert.ToInt32(filters.Where(filterTuple => filterTuple.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower())?.FirstOrDefault()?.Item3);
                ZnodeLogging.LogMessage("localeId:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { localeId = localeId });
                filters.Remove(filters.Where(filterTuple => filterTuple.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower())?.FirstOrDefault());
            }
            CatalogAssociateCategoryListModel categoryListModel = GetXmlCategory(filters, sorts, page);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return categoryListModel;
        }

        //Get locale id from filter otherwise set default
        public static int GetLocaleId(FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            int localeId = Convert.ToInt32(DefaultGlobalConfigSettingHelper.Locale);
            ZnodeLogging.LogMessage("localeId:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { localeId = localeId });
            //Checking For LocaleId exists in Filters Or Not
            if (filters.Exists(x => x.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower()))
            {
                localeId = Convert.ToInt32(filters.Where(x => x.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower()).Select(x => x.FilterValue).FirstOrDefault());
                filters.RemoveAll(x => x.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower());
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return localeId;
        }

        //Unassociate categories and product from catalog.
        public virtual bool UnAssociateCategoryFromCatalog(CatalogAssociationModel catalogAssociationModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if (IsNull(catalogAssociationModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);
            ZnodeLogging.LogMessage("Input parameter:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { catalogAssociationModel = catalogAssociationModel });

            bool isUnAssociate = false;

            //Remove the category from profile catalog
            if (catalogAssociationModel.ProfileCatalogId > 0)
                isUnAssociate = UnAssociateCategoryFromProfile(catalogAssociationModel);

            //Remove the categories.
            else if (IsNotNull(catalogAssociationModel.PimCategoryHierarchyId) && catalogAssociationModel.ProfileCatalogId <= 0)
                isUnAssociate = UnAssociateCategories(catalogAssociationModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return isUnAssociate;
        }

        public virtual bool IsCategoryMerchandise { get => Convert.ToBoolean(ZnodeApiSettings.IsCategoryMerchandise); }

        //Unassociate categories and product from catalog.
        public virtual bool UnAssociateProductFromCatalog(CatalogAssociationModel catalogAssociationModel)
        {
            ZnodeLogging.LogMessage("Input parameter:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { catalogAssociationModel = catalogAssociationModel });

            if (!IsCategoryMerchandise)
                return UnassociateProductOnCategoryMerchandise(catalogAssociationModel);
            else
            {
                productAssociationHelper.UnassociateCatalogProductfromAllCatalog(catalogAssociationModel);
                return productAssociationHelper.UnassociateProductFromCategory(catalogAssociationModel);
            }

        }

        protected virtual bool UnassociateProductOnCategoryMerchandise(CatalogAssociationModel catalogAssociationModel)
        {
            if (Equals(catalogAssociationModel, null))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelNotNull);
            ZnodeLogging.LogMessage("Input parameter:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { catalogAssociationModel = catalogAssociationModel });

            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter(ZnodeProfileCatalogEnum.ProfileCatalogId.ToString(), catalogAssociationModel.ProfileCatalogId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("PimCatalogId", catalogAssociationModel.CatalogId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("PimCategoryId", catalogAssociationModel.CategoryId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("PimProductId", catalogAssociationModel.ProductIds, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter(ZnodePimCategoryHierarchyEnum.PimCategoryHierarchyId.ToString(), catalogAssociationModel.PimCategoryHierarchyId, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Boolean);
            int status = 0;
            IList<View_ReturnBoolean> deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_DeletePimCatalogProducts @ProfileCatalogId, @PimCatalogId, @PimCategoryId, @PimProductId, @PimCategoryHierarchyId, @Status OUT", 4, out status);
            if (deleteResult.FirstOrDefault().Status.Value)
            {
                ZnodeLogging.LogMessage(Admin_Resources.SuccessRemoveProduct, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                return true;
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorRemoveProduct, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                return false;
            }
        }

        public virtual ProductDetailsListModel GetCategoryAssociatedProducts(CatalogAssociationModel catalogAssociationModel, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { catalogAssociationModel = catalogAssociationModel });

            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            int portalId = GetPortalId(filters);
            SetProductStatusFilter(filters);

            string whereClause = ProductService.GenerateXMLWhereClauseForSP(filters.ToFilterDataCollection());
            ZnodeLogging.LogMessage("pageListModel and whereClause to get ProductDetailsModel list:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { pageListModel = pageListModel?.ToDebugString(), whereClause = whereClause });
            string attributeCode = GetAttributeCodes(filters);
            IZnodeViewRepository<ProductDetailsModel> objStoredProc = new ZnodeViewRepository<ProductDetailsModel>();
            objStoredProc.SetParameter("@WhereClause", whereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter(ZnodeLocaleEnum.LocaleId.ToString(), catalogAssociationModel.LocaleId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodePimCategoryEnum.PimCategoryId.ToString(), catalogAssociationModel.CategoryId.GetValueOrDefault(), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodePimCatalogEnum.PimCatalogId.ToString(), catalogAssociationModel.CatalogId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@IsAssociated", catalogAssociationModel.IsAssociated, ParameterDirection.Input, DbType.Boolean);
            objStoredProc.SetParameter("@AttributeCode", attributeCode, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter(ZnodeProfileCatalogEnum.ProfileCatalogId.ToString(), catalogAssociationModel.ProfileCatalogId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodePimCategoryHierarchyEnum.PimCategoryHierarchyId.ToString(), catalogAssociationModel.PimCategoryHierarchyId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PortalId", portalId.ToString(), ParameterDirection.Input, DbType.Int32);
            //List of all products associated to category.
            List<ProductDetailsModel> list = objStoredProc.ExecuteStoredProcedureList("ZNode_GetCatalogCategoryProducts @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT,@LocaleId,@PimCategoryId,@PimCatalogId,@IsAssociated,@ProfileCatalogId,@AttributeCode,@PimCategoryHierarchyId,@PortalId", 4, out pageListModel.TotalRowCount)?.ToList();
            ZnodeLogging.LogMessage("ProductDetailsModel list count:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { ProductDetailsModelListCount = list?.Count });

            ProductDetailsListModel listModel = new ProductDetailsListModel { ProductDetailList = list };
            listModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return listModel;
        }

        //Get details(Display order, active status, etc.)of category associated to catalog.
        public virtual CatalogAssociateCategoryModel GetAssociateCategoryDetails(CatalogAssociateCategoryModel catalogAssociateCategoryModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter catalogAssociateCategoryModel properties:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { PimCatalogId = catalogAssociateCategoryModel?.PimCatalogId, PimCategoryHierarchyId = catalogAssociateCategoryModel?.PimCategoryHierarchyId });

            //check for pimCatalogId, pimCategoryId greater than 1.
            if (HelperUtility.IsNotNull(catalogAssociateCategoryModel) && catalogAssociateCategoryModel.PimCatalogId < 1 && catalogAssociateCategoryModel.PimCategoryHierarchyId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, PIM_Resources.ErrorCatalogorCategoryIdLessThanOne);

            ZnodeRepository<ZnodePimAttribute> _pimAttributeRepository = new ZnodeRepository<ZnodePimAttribute>();
            ZnodeRepository<ZnodePimCategoryAttributeValue> _pimCategoryAttributeValueRepository = new ZnodeRepository<ZnodePimCategoryAttributeValue>();
            ZnodeRepository<ZnodePimCategoryAttributeValueLocale> _pimCategoryAttributeValueLocaleRepository = new ZnodeRepository<ZnodePimCategoryAttributeValueLocale>();

            return (from category in _pimCategoryHierarchyRepository.Table
                    where category.PimCatalogId == catalogAssociateCategoryModel.PimCatalogId && category.PimCategoryHierarchyId == catalogAssociateCategoryModel.PimCategoryHierarchyId
                    from categoryAttribute in _pimAttributeRepository.Table
                    join categoryAttributeValue in _pimCategoryAttributeValueRepository.Table
                    on categoryAttribute.PimAttributeId equals categoryAttributeValue.PimAttributeId
                    join categoryAttributeValueLocale in _pimCategoryAttributeValueLocaleRepository.Table
                    on categoryAttributeValue.PimCategoryAttributeValueId equals categoryAttributeValueLocale.PimCategoryAttributeValueId
                    where categoryAttribute.AttributeCode == "CategoryName" && categoryAttributeValue.PimCategoryId == category.PimCategoryId && categoryAttributeValueLocale.LocaleId == catalogAssociateCategoryModel.LocaleId
                    select new CatalogAssociateCategoryModel()
                    {
                        PimCatalogId = category.PimCatalogId,
                        ParentPimCategoryHierarchyId = category.ParentPimCategoryHierarchyId,
                        PimCategoryId = category.PimCategoryId,
                        CategoryValue = categoryAttributeValueLocale.CategoryValue,
                        DisplayOrder = category.DisplayOrder,
                        IsActive = category.IsActive,
                        ActivationDate = category.ActivationDate,
                        ExpirationDate = category.ExpirationDate,
                        PimCategoryHierarchyId = category.PimCategoryHierarchyId,
                    })?.FirstOrDefault();
        }

        //Update details(Display order, active status, etc.)of category associated to catalog.
        public virtual bool UpdateAssociateCategoryDetails(CatalogAssociateCategoryModel catalogAssociateCategoryModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            //Check if model is null.
            if (HelperUtility.IsNull(catalogAssociateCategoryModel))
                throw new ZnodeException(ErrorCodes.NullModel, ZnodeConstant.NullModelError);
            ZnodeLogging.LogMessage("CatalogAssociateCategoryModel details:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, catalogAssociateCategoryModel);
            if (catalogAssociateCategoryModel.UpdateDisplayOrder)
                return UpdateDisplayOrder(catalogAssociateCategoryModel);

            catalogAssociateCategoryModel.ParentPimCategoryHierarchyId = catalogAssociateCategoryModel.ParentPimCategoryHierarchyId == 0 ? null : catalogAssociateCategoryModel.ParentPimCategoryHierarchyId;

            if (catalogAssociateCategoryModel.PimCategoryHierarchyId < 1)
            {
                //set filters to get category hierarchy.
                FilterCollection filters = SetFiltersToSetDisplayOrder(catalogAssociateCategoryModel);

                //Generate whereclause.
                EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                ZnodeLogging.LogMessage("whereClauseModel to get catalogAssociateCategoryEntityList:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, whereClauseModel?.WhereClause);
                //Get the list of selected and its previous category.
                IList<ZnodePimCategoryHierarchy> catalogAssociateCategoryEntityList = _pimCategoryHierarchyRepository.GetEntityList(whereClauseModel.WhereClause);

                //Get data for selected category.
                ZnodePimCategoryHierarchy catalogAssociateCategoryEntity = catalogAssociateCategoryEntityList.Where(x => x.PimCategoryId == catalogAssociateCategoryModel.PimCategoryId)?.FirstOrDefault();

                if (catalogAssociateCategoryModel.CategoryId > 0)
                {
                    //Get data for parent category.
                    ZnodePimCategoryHierarchy catalogAssociateCategory = catalogAssociateCategoryEntityList.Where(x => x.PimCategoryId == catalogAssociateCategoryModel.CategoryId)?.FirstOrDefault();

                    if (IsNotNull(catalogAssociateCategory))
                    {
                        //sete display order.
                        if (catalogAssociateCategoryModel.IsMoveUp)
                        {
                            catalogAssociateCategoryModel.DisplayOrder = catalogAssociateCategory.DisplayOrder > 1 ? catalogAssociateCategory.DisplayOrder - 1 : catalogAssociateCategoryEntity?.DisplayOrder;
                            catalogAssociateCategoryModel.ParentPimCategoryHierarchyId = catalogAssociateCategory.ParentPimCategoryHierarchyId;
                        }
                        else if (catalogAssociateCategoryModel.IsMoveDown)
                        {
                            catalogAssociateCategoryModel.DisplayOrder = catalogAssociateCategory.DisplayOrder == 999 ? catalogAssociateCategory.DisplayOrder : catalogAssociateCategory.DisplayOrder + 1;
                            catalogAssociateCategoryModel.ParentPimCategoryHierarchyId = catalogAssociateCategory.ParentPimCategoryHierarchyId;
                        }
                    }
                }

                if (IsNotNull(catalogAssociateCategoryEntity))
                {
                    catalogAssociateCategoryModel.PimCategoryHierarchyId = catalogAssociateCategoryEntity.PimCategoryHierarchyId;
                    catalogAssociateCategoryModel.IsActive = catalogAssociateCategoryEntity.IsActive;
                    catalogAssociateCategoryModel.ExpirationDate = catalogAssociateCategoryEntity.ExpirationDate;
                    catalogAssociateCategoryModel.ActivationDate = catalogAssociateCategoryEntity.ActivationDate;
                }
            }

            bool isUpdated = _pimCategoryHierarchyRepository.Update(catalogAssociateCategoryModel.ToEntity<ZnodePimCategoryHierarchy>());

            ZnodeLogging.LogMessage(isUpdated ? string.Format(PIM_Resources.SuccessUpdateAssociateCategoryDetails, catalogAssociateCategoryModel.PimCategoryId) : PIM_Resources.ErrorUpdateAssociateCategoryDetails, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return isUpdated;
        }

        //Move category.
        public virtual bool MoveCategory(CatalogAssociateCategoryModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if (HelperUtility.IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, PIM_Resources.ErrorCatalogAssociateCategoryModelNull);

            if (IsCategorySame(model.PimCategoryHierarchyId, model.ParentPimCategoryHierarchyId.GetValueOrDefault()))
                throw new ZnodeException(ErrorCodes.AlreadyExist, PIM_Resources.ErrorCategoryExists);

            ZnodeLogging.LogMessage("Input parameter catalogAssociateCategoryModel properties:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { PimCatalogId = model?.PimCatalogId, PimCategoryHierarchyId = model?.PimCategoryHierarchyId, ParentPimCategoryHierarchyId = model?.ParentPimCategoryHierarchyId });

            ZnodePimCategoryHierarchy znodePimCategoryHierarchy = _pimCategoryHierarchyRepository.Table.FirstOrDefault(x => x.PimCatalogId == model.PimCatalogId && x.PimCategoryHierarchyId == model.PimCategoryHierarchyId);

            if (IsNotNull(znodePimCategoryHierarchy))
            {
                model.ParentPimCategoryHierarchyId = model.ParentPimCategoryHierarchyId == 1 ? null : model.ParentPimCategoryHierarchyId;

                //Assign value to parent media path id.
                if (IsNotNull(_pimCategoryHierarchyRepository.Table.FirstOrDefault(x => x.PimCatalogId == model.PimCatalogId & x.ParentPimCategoryHierarchyId == model.ParentPimCategoryHierarchyId & x.PimCategoryId == znodePimCategoryHierarchy.PimCategoryId)))
                    throw new ZnodeException(ErrorCodes.AlreadyExist, PIM_Resources.ErrorCategoryExists);

                znodePimCategoryHierarchy.ParentPimCategoryHierarchyId = model.ParentPimCategoryHierarchyId != 1 ? model.ParentPimCategoryHierarchyId : null;
                znodePimCategoryHierarchy.DisplayOrder = GetMaxDisplayOrder(model.PimCatalogId.GetValueOrDefault(), znodePimCategoryHierarchy.ParentPimCategoryHierarchyId) + 75000;

                bool result = _pimCategoryHierarchyRepository.Update(znodePimCategoryHierarchy);
                ZnodeLogging.LogMessage(result ? PIM_Resources.SuccessMoveCategory : PIM_Resources.ErrorMoveCategory, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                return result;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return false;
        }

        //Gets the attribute code form filters.
        protected static string GetAttributeCodes(FilterCollection filters)
        {
            if (filters?.Count > 0)
            {
                string attributeCode = string.Join(",", filters.Select(x => x.FilterName).ToArray());
                if (!string.IsNullOrEmpty(attributeCode) && attributeCode.Contains("|"))
                    attributeCode = attributeCode.Replace('|', ',');
                return attributeCode;
            }
            return string.Empty;
        }

        //publish a Catalog using catalogId.
        public virtual PublishedModel Publish(int pimCatalogId, string revisionType)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { pimCatalogId = pimCatalogId, revisionType = revisionType });

            Guid jobId = Guid.NewGuid();

            List<CatalogEntity> catalogEntity = GetCatalogEntity(pimCatalogId, jobId);
            string catalogPublishProgressMessage = catalogEntity.Count() > 0 ? catalogEntity.FirstOrDefault().CatalogName + " Catalog" : "Catalog";


            catalogEntity.ForEach(x => x.RevisionType = revisionType == "NONE" ? ZnodePublishStatesEnum.PRODUCTION.ToString() : revisionType);

            PreviewHelper.LogProgress(jobId, catalogPublishProgressMessage, 5);
            try
            {           
            if (IsNotNull(catalogEntity))
            {
                if (!string.Equals(revisionType, ZnodePublishStatesEnum.PRODUCTION.ToString()))
                {
                    HttpContext httpContext = HttpContext.Current;
                    Action threadWorker = delegate ()
                    {
                        HttpContext.Current = httpContext;
                        jobId = Guid.NewGuid();
                        try
                        {
                            PreviewCatalog(catalogEntity, revisionType == "NONE" ? ZnodePublishStatesEnum.PRODUCTION.ToString() : revisionType, jobId, catalogPublishProgressMessage);
                            PreviewHelper.LogProgress(jobId, catalogPublishProgressMessage, 100, true);
                            //Clear Cache of portal after catalog publish.
                            ClearCacheAfterPublish(catalogEntity.Any() ? catalogEntity.FirstOrDefault().ZnodeCatalogId : 0);
                        }
                        catch (Exception ex)
                        {
                            ZnodeLogging.LogMessage(ex.StackTrace, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                            throw ex;
                        }
                    };
                    AsyncCallback cb = new AsyncCallback(PublishSuccessCallBack);
                    threadWorker.BeginInvoke(cb, null);                    
                }
                else
                {
                        HttpContext httpContext = HttpContext.Current;
                        Action threadWorker= delegate ()
                        {
                            try
                            {
                                HttpContext.Current = httpContext;
                                jobId = Guid.NewGuid();
                                //Preview catalog 
                                PreviewCatalog(catalogEntity, ZnodePublishStatesEnum.PREVIEW.ToString(), jobId, catalogPublishProgressMessage);
                                //Create new version for preview.
                                catalogEntity = GetCatalogEntity(pimCatalogId, jobId);

                                PreviewHelper.LogProgress(jobId, catalogPublishProgressMessage, 85);

                                if (IsNotNull(catalogEntity))
                                {
                                    //Publish catalog 
                                    PublishCatalogForPreview(catalogEntity, jobId, catalogPublishProgressMessage);
                                }
                                PreviewHelper.LogProgress(jobId, catalogPublishProgressMessage, 100, true);

                                //Clear Cache of portal after catalog publish.
                                ClearCacheAfterPublish(catalogEntity.Any() ? catalogEntity.FirstOrDefault().ZnodeCatalogId : 0);
                            }
                            catch (Exception ex)
                            {
                                ZnodeLogging.LogMessage(ex.StackTrace, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                                throw ex;
                            }
                        };                        
                        AsyncCallback cb = new AsyncCallback(PublishSuccessCallBack);
                        threadWorker.BeginInvoke(cb, null);                        
                    }
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                return new PublishedModel { IsPublished = true, ErrorMessage = Admin_Resources.SuccessPublish };
            }
            else
                return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.ErrorPublished };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private void PublishSuccessCallBack(IAsyncResult ar)
        {
            AsyncResult result = ar as AsyncResult;  
        }
        public virtual List<CatalogEntity> GetCatalogEntity(int pimCatalogId, Guid jobId, bool isCategoryPublishInProcess = false)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            try
            {
                List<int> LocaleIds = GetActiveLocaleList().Select(x => x.LocaleId).ToList();
                ZnodeLogging.LogMessage("LocaleIds list count:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, LocaleIds?.Count);
                IZnodeViewRepository<CatalogEntity> objStoredProc = new ZnodeViewRepository<CatalogEntity>();
                objStoredProc.SetParameter("@PimCatalogId", pimCatalogId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("@UserId", GetLoginUserId(), ParameterDirection.Input, DbType.Int32);

                objStoredProc.SetParameter("@PublishTimeoutSeconds", PublishTimeoutSeconds(), ParameterDirection.Input, DbType.Double);
                objStoredProc.SetParameter("@Status", null, ParameterDirection.Output, DbType.Boolean);
                if (isCategoryPublishInProcess)
                    objStoredProc.SetParameter("@IsCategoryPublishInProcess", true, ParameterDirection.Input, DbType.Boolean);



                int status = 0;
                DataTable localeIds = ConvertKeywordListToDataTable(LocaleIds);
                List<CatalogEntity> catalogEntity = null;
                if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
                {
                    objStoredProc.SetParameter("@LocaleId", localeIds?.ToJson(), ParameterDirection.Input, DbType.String);
                    if (isCategoryPublishInProcess)
                        catalogEntity = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishCatalogWithJSON @PimCatalogId,@UserId,@Status OUT,@PublishTimeoutSeconds,@LocaleId,@IsCategoryPublishInProcess", 2, out status).ToList();
                    else
                        catalogEntity = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishCatalogWithJSON @PimCatalogId,@UserId,@Status OUT,@PublishTimeoutSeconds,@LocaleId", 2, out status).ToList();
                }
                else
                {
                    objStoredProc.SetTableValueParameter("@LocaleId", localeIds, ParameterDirection.Input, SqlDbType.Structured, "dbo.TransferId");
                    if (isCategoryPublishInProcess)
                        catalogEntity = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishCatalog @PimCatalogId,@UserId,@Status OUT,@PublishTimeoutSeconds,@LocaleId,@IsCategoryPublishInProcess", 2, out status).ToList();
                    else
                        catalogEntity = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishCatalog @PimCatalogId,@UserId,@Status OUT,@PublishTimeoutSeconds,@LocaleId", 2, out status).ToList();
                }

                if (catalogEntity.Count < 1)
                    throw new ZnodeException(ErrorCodes.NotPermited, PIM_Resources.ErrorPublishCatalog);

                return catalogEntity;
            }
            catch (Exception ex)
            {
                PreviewHelper.LogProgress(jobId, "Catalog", 100, false, true, ex);
                throw ex;
            }
        }

        [Obsolete]
        //Unused method
        //Publish Product SEO Details by SEOCode.
        public void PublishCategorySEOBySEOCode(List<string> SEOCodes)
        {
            //Publish SEO Locale Detail to mongo
            string seoCodes = string.Join(", ", SEOCodes);
            IZnodeViewRepository<SeoEntity> objStoredProc = new ZnodeViewRepository<SeoEntity>();
            objStoredProc.SetParameter("@PortalId", 0, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@IsBrand", 0, ParameterDirection.Input, DbType.Boolean);
            objStoredProc.SetParameter("@SeoCode", seoCodes, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@SeoType", ZnodeConstant.Category, ParameterDirection.Input, DbType.String);
            List<SeoEntity> getCategorySeoLocaleDetailList;//code review point
            getCategorySeoLocaleDetailList = objStoredProc.ExecuteStoredProcedureList("Znode_GetSeoDetailsForPublish @PortalId,@IsBrand, @SeoCode, @SeoType").ToList();
            if (getCategorySeoLocaleDetailList?.Count > 0)
            {
                _seoProductMongoRepository.DeleteByQuery(Query<SeoEntity>.In(pr => pr.SEOCode, SEOCodes));
                _seoProductMongoRepository.Create(getCategorySeoLocaleDetailList);
                UpdateSeoPublishStatusBySEOCode(SEOCodes);
            }
        }

        //Publish catalog category associated products.
        public virtual PublishedModel PublishCategoryProducts(int pimCatalogId, int pimCategoryHierarchyId, string revisionType)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { pimCatalogId = pimCatalogId, pimCategoryHierarchyId = pimCategoryHierarchyId, revisionType = revisionType });

            //Check catalog publish already in progress or not. if yes we throw exception. 
            CheckCatalogPublishAlreadyInProgress();

            //Get unique job id. On the basis of job id we show the publish notification / progress bar.
            var jobId = GetUniqueJobId();

            //Get category progress message on the basis of pim catalog id.
            string categoryPublishProgressMessage = GetcategoryPublishProgressMessage(pimCatalogId);

            if (string.Equals(revisionType, ZnodePublishStatesEnum.PRODUCTION.ToString()))
                //Publish category products for preview and production.
                return PublishCategoryProductsForPreviewAndProduction(pimCatalogId, pimCategoryHierarchyId, jobId, categoryPublishProgressMessage);
            else
                //Publish category products for preview.(In case preview setting is off then this method will be used.)
                return PublishCategoryProductsForPreview(pimCatalogId, pimCategoryHierarchyId, revisionType, jobId, categoryPublishProgressMessage);
        }

        //Get unique job id. On the basis of job id we show the publish notification / progress bar.
        protected virtual Guid GetUniqueJobId()
            => Guid.NewGuid();

        //Check catalog publish already in progress or not. if yes we throw exception. 
        private void CheckCatalogPublishAlreadyInProgress()
        {
            bool isCatalogCategoryPublishInProgress = _publishCatalogLogRepository.Table.Any(x => x.IsCatalogPublished == null);

            if (isCatalogCategoryPublishInProgress)
                throw new ZnodeException(ErrorCodes.CategoryPublishError, PIM_Resources.ErrorPublishCatalogCategory);
        }

        //Publish category products for preview.
        protected virtual PublishedModel PublishCategoryProductsForPreview(int pimCatalogId, int pimCategoryHierarchyId, string revisionType, Guid jobId, string categoryPublishProgressMessage)
        {
            PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 10);

            HttpContext httpContext = HttpContext.Current;
            Thread thread = new Thread(new ThreadStart(() =>
            {
                HttpContext.Current = httpContext;

                ZnodePublishStatesEnum PublishStateEnum = (ZnodePublishStatesEnum)Enum.Parse(typeof(ZnodePublishStatesEnum), revisionType == "NONE" ? ZnodePublishStatesEnum.PRODUCTION.ToString() : revisionType, true);

                DataSet resultDataSet = GetPublishCategoryProduct(pimCatalogId, pimCategoryHierarchyId, PublishStateEnum, out int status, jobId, categoryPublishProgressMessage);

                if (IsNull(resultDataSet))
                    return;

                //Fetch the data of products associated for the perticular category.
                List<ProductAssociationPublishModel> productAssociationList = resultDataSet.Tables[2].AsEnumerable().Select(dataRow => new ProductAssociationPublishModel { PublishCategoryId = dataRow.Field<int>("PublishCategoryId"), PublishProductId = dataRow.Field<int>("PublishProductId"), PublishCatalogId = dataRow.Field<int>("PublishCatalogId"), previousVersionId = dataRow.Field<int>("VersionId"), LocaleId = dataRow.Field<int?>("LocaleId") ?? 0 }).ToList();

                categoryPublishProgressMessage = categoryPublishProgressMessage + " " + "(Product Count : " + productAssociationList?.Where(x => x.PublishCategoryId != 0)?.Count() + ")";

                PreviewCategoryProducts(pimCategoryHierarchyId, resultDataSet, status, revisionType == "NONE" ? ZnodePublishStatesEnum.PRODUCTION.ToString() : revisionType, jobId, categoryPublishProgressMessage, productAssociationList);

                PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 80);

                //Clear Cache of portal after catalog category publish.
                ClearCacheAfterPublish(int.Parse(resultDataSet.Tables[2].Rows[0]["PublishCatalogId"].ToString()));

                PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 100, true);
            }));
            thread.Start();

            return new PublishedModel { IsPublished = true, ErrorMessage = Admin_Resources.SuccessPublish };
        }

        //Publish category products for preview and production.
        protected virtual PublishedModel PublishCategoryProductsForPreviewAndProduction(int pimCatalogId, int pimCategoryHierarchyId, Guid jobId, string categoryPublishProgressMessage)
        {
            PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 10);
            HttpContext httpContext = HttpContext.Current;
            Thread thread = new Thread(new ThreadStart(() =>
            {
                HttpContext.Current = httpContext;

                //Get Publish category products details for perticular catalog and category.
                DataSet resultDataSet = GetPublishCategoryProduct(pimCatalogId, pimCategoryHierarchyId, ZnodePublishStatesEnum.PREVIEW, out int status, jobId, categoryPublishProgressMessage);

                if (IsNull(resultDataSet))
                    return;

                //Fetch the data of products associated for the perticular category.
                List<ProductAssociationPublishModel> productAssociationList = resultDataSet.Tables[2].AsEnumerable().Select(dataRow => new ProductAssociationPublishModel { PublishCategoryId = dataRow.Field<int>("PublishCategoryId"), PublishProductId = dataRow.Field<int>("PublishProductId"), PublishCatalogId = dataRow.Field<int>("PublishCatalogId"), previousVersionId = dataRow.Field<int>("VersionId"), LocaleId = dataRow.Field<int?>("LocaleId") ?? 0 }).ToList();

                categoryPublishProgressMessage = categoryPublishProgressMessage + " " + "(Product Count : " + productAssociationList?.Where(x=>x.PublishCategoryId !=0)?.Count() + ")";

                PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 30);

                //Publish category products for preview.
                PreviewCategoryProducts(pimCategoryHierarchyId, resultDataSet, status, ZnodePublishStatesEnum.PREVIEW.ToString(), jobId, categoryPublishProgressMessage, productAssociationList);

                //Publish category products for production.
                PublishCategoryProduct(pimCatalogId, resultDataSet, jobId, categoryPublishProgressMessage, productAssociationList);

                PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 90);

                //Clear Cache of portal after catalog category publish.
                ClearCacheAfterPublish(int.Parse(resultDataSet.Tables[2].Rows[0]["PublishCatalogId"].ToString()));

                PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 100, true);
            }));
            thread.Start();

            return new PublishedModel { IsPublished = true};
        }

        //Get category publish progress message.
        private string GetcategoryPublishProgressMessage(int pimCatalogId)
        {
            string catalogName = _pimCatalogRepository.GetById(pimCatalogId)?.CatalogName;
            ZnodeLogging.LogMessage("CatalogName:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, catalogName);
            return IsNotNull(catalogName) ? "Category from " + catalogName + " catalog" : "Category";
        }

        //Publish category products for preview.
        protected virtual PublishedModel PreviewCategoryProducts(int pimCategoryHierarchyId, DataSet resultDataSet, int status, string revisionType, Guid jobId, string categoryPublishProgressMessage, List<ProductAssociationPublishModel> productAssociationList)
        {
            if (resultDataSet?.Tables?.Count > 0 && resultDataSet?.Tables[2].Rows.Count > 0)
            {
                ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

                PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 30);
                //Get Publish Catalog Id.
                int publishCatalogId = int.Parse(resultDataSet.Tables[2].Rows[0]["PublishCatalogId"].ToString());

                try
                {
                    //Publish Preview category products and delete old data from mongo.
                    PublishCatalogCategoryProductsForPreview(pimCategoryHierarchyId, resultDataSet, revisionType, jobId, productAssociationList, publishCatalogId);

                    PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 60);

                    //Set category publish progress done by setting IsCatalogPublished and IsCategoryPublished to true
                    SetCategoryPublishDone(publishCatalogId, revisionType);

                }
                catch (Exception ex)
                {
                    //Set category publish progress failed by setting IsCatalogPublished and IsCategoryPublished to false.
                    SetCategoryPublishFailed(publishCatalogId, revisionType);
                    PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 100, false, true, ex);
                    ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    return new PublishedModel { IsPublished = false };
                }
            }
            else
            {
                if (status == 0)
                    return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.ErrorPublished };
                else if (status == 1)
                    throw new ZnodeException(ErrorCodes.InvalidData, "Catalog must be in publish state before publishing the category associated products.");
                else if (status == 2)
                    throw new ZnodeException(ErrorCodes.NotPermited, "Catalog publish is in process. Please retry after the process is completed.");
            }
            return new PublishedModel { IsPublished = true };
        }

        //Publish Preview category products and delete old data from mongo.
        protected virtual void PublishCatalogCategoryProductsForPreview(int pimCategoryHierarchyId, DataSet resultDataSet, string revisionType, Guid jobId, List<ProductAssociationPublishModel> productAssociationList, int publishCatalogId)
        {
            //Fetch the category ids which we need to delete.
            List<int> publishCategoryIds = resultDataSet.Tables[0].AsEnumerable().Select(x => x.Field<int>("PublishCategoryId")).ToList();

            //Get catalog category entities on the basis of category xml list and convert it into entity list.
            List<CategoryEntity> categoryEntitiesToPublish = GetCatalogCategoriesEntities(resultDataSet);

            foreach (LocaleModel localeModel in GetActiveLocaleList())
            {
                int versionId = productAssociationList.FirstOrDefault(x => x.LocaleId == localeModel.LocaleId).previousVersionId;
                List<int> publishProductIds = productAssociationList.Where(x => x.LocaleId == localeModel.LocaleId).Select(x => x.PublishProductId).ToList();
                long publishStartTime = DateTime.Now.Ticks;
                List<ProductEntity> productListForSearch = new List<ProductEntity>();

                //Publish catalog category products data as per the new temporary version id.
                PublishCategoryProductPreviewDataForTempVersion(pimCategoryHierarchyId, revisionType, (ZnodePublishStatesEnum)Enum.Parse(typeof(ZnodePublishStatesEnum), revisionType), publishCatalogId, publishCategoryIds, localeModel, versionId, publishProductIds, publishStartTime, productListForSearch);

                //Publish catalog categories for temporary version.
                PublishCatalogCategoriesForTempVersion(categoryEntitiesToPublish, localeModel, publishStartTime);

                //Update temporary version id with actual version id.Once update done then we get duplicate records on webstore till next delete operation.
                UpdateTempVersionIdWithActualVersionId(publishCatalogId, publishCategoryIds, versionId, publishProductIds, publishStartTime);

                //Deleting products and associated products configurations and Delete category or subcategory If a user deletes the category or subcategory from catalog tree and then associated same category for publishing.
                DeleteOldCatalogCategoriesProductData(resultDataSet, publishCatalogId, publishCategoryIds, versionId, publishProductIds, publishStartTime);

                //Update PublishStartTime field to null.
                UpdatePublishStartTimeFieldToNull(publishCatalogId, publishCategoryIds, versionId, publishProductIds, publishStartTime, true);

                //Save Products in Search (Elastic)
                PublishHelper.SaveInSearchForCatalogCategoryProducts(productListForSearch.Where(x => x.ZnodeCategoryIds != 0 && publishCategoryIds.Contains(x.ZnodeCategoryIds)).ToList(), publishCatalogId, publishCategoryIds, revisionType);
            }
        }

        // Update PublishStartTime field to null.
        protected virtual void UpdatePublishStartTimeFieldToNull(int publishCatalogId, List<int> publishCategoryIds, int versionId, List<int> publishProductIds, long publishStartTime, bool isAllowTempVersionIdToNull = false)
         => PreviewHelper.SwapVersionIdInMongoEntities(publishStartTime, versionId, publishCatalogId, publishCategoryIds, publishProductIds, isAllowTempVersionIdToNull);

        //Update temporary version id with actual version id.
        protected virtual void UpdateTempVersionIdWithActualVersionId(int publishCatalogId, List<int> publishCategoryIds, int versionId, List<int> publishProductIds, long publishStartTime)
            => PreviewHelper.SwapVersionIdInMongoEntities(publishStartTime, versionId, publishCatalogId, publishCategoryIds, publishProductIds);

        //Publish catalog categories for temporary version.
        protected virtual void PublishCatalogCategoriesForTempVersion(List<CategoryEntity> categoryEntitiesToPublish, LocaleModel localeModel, long publishStartTime)
            =>  PublishHelper.SaveCategoryInMongoForTempVersion(categoryEntitiesToPublish?.Where(x => x.LocaleId == localeModel.LocaleId)?.ToList(), publishStartTime, true);

        //Deleting products and associated products configurations and Delete category or subcategory If a user deletes the category or subcategory from catalog tree and then associated same category for publishing.
        protected virtual void DeleteOldCatalogCategoriesProductData(DataSet resultDataSet, int publishCatalogId, List<int> publishCategoryIds, int versionId, List<int> publishProductIds, long publishStartTime)
            => Parallel.Invoke(
                    () => { DeleteAssociatedProductConfigurations(publishCatalogId, publishCategoryIds, publishProductIds, versionId); },
                    () => { DeleteCatalogCategories(resultDataSet, publishCatalogId, versionId); });

        //Update existing version id data with temp / dummy version id and newly inserted version id data with actual version id.
        protected virtual void UpdateExistingVersionId(int publishCatalogId, List<int> publishCategoryIds, int actualVersionId, List<int> publishProductIds, long publishStartTime, bool isAllowTempVersionIdToNull = false)
          => PreviewHelper.SwapVersionIdInMongoEntities(publishStartTime, actualVersionId, publishCatalogId, publishCategoryIds, publishProductIds, isAllowTempVersionIdToNull);

        //Publish catalog category products data for temporary version as per the new temporary version id.
        protected virtual void PublishCategoryProductPreviewDataForTempVersion(int pimCategoryHierarchyId, string revisionType, ZnodePublishStatesEnum znodePublishState, int publishCatalogId, List<int> publishCategoryIds, LocaleModel localeModel, int versionId, List<int> publishProductIds, long publishStartTime, List<ProductEntity> productListForSearch)
        {
            //Get total publish Products.
            IQueryable<ZnodePublishedXml> znodePublishedXmls = _publishedXmlRepository.Table.Where(x => x.PublishCatalogLogId == versionId && x.IsProductXML == true).Where(x => publishProductIds.Contains(x.PublishedId ?? 0));
            //Get products in chunks.
            List<List<ZnodePublishedXml>> publishedXmlsListInChunks = SplitCollectionIntoChunks(znodePublishedXmls.ToList(), !string.IsNullOrEmpty(ZnodeApiSettings.ProductPublishChunkSize) ? Convert.ToInt32(ZnodeApiSettings.ProductPublishChunkSize) : ZnodeConstant.ProductPublishChunk);

            //Publish product data in mongo with temp version.
            Parallel.For(0, publishedXmlsListInChunks.Count(), new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, index =>
            {
                //Save products in mongo with category publish call with temporary version id and with publish start time.
                List<ProductEntity> productEntityList = PublishHelper.SaveProductInMongoWithTempVersion(publishedXmlsListInChunks[index].Select(x => x.PublishedXML), revisionType, true, publishStartTime);

                // Replace temporary version id with original version id for elastic index.
                productEntityList.ForEach(u => { u.VersionId = versionId; });

                // Add product list for creating index in later operation.
                productListForSearch.AddRange(productEntityList);
            });

            //Publish Associated Products like (Addons, Group Products,Bundle Products and Configurable Products) Configuration with temp version.
            PublishHelper.PublishAssociatedProductsConfiguration(publishCatalogId, "@PublishCatalogId", pimCategoryHierarchyId, "@PimCategoryHierarchyId", GetLoginUserId(), versionId, znodePublishState, publishStartTime);
        }

        //Publish category products (For Production).In this we copy all data on the basis of preview version id to production version id.
        protected virtual PublishedModel PublishCategoryProduct(int pimCatalogId, DataSet resultDataSet, Guid jobId, string categoryPublishProgressMessage, List<ProductAssociationPublishModel> productAssociationList)
        {
            if (resultDataSet?.Tables[1].Rows.Count > 0)
            {
                PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 70);

                List<int> publishCategoryIds = resultDataSet.Tables[0].AsEnumerable().Select(x => x.Field<int>("PublishCategoryId")).ToList();
                int publishCatalogId = int.Parse(resultDataSet.Tables[2].Rows[0]["PublishCatalogId"].ToString());
                try
                {
                    //Set category publish progress in progress by setting IsCatalogPublished and IsCategoryPublished to null.
                    SetCategoryPublishInProgress(publishCatalogId, ZnodePublishStatesEnum.PRODUCTION.ToString());

                    //Publish category product for production and delete old data from mongo.
                    PublishCategoryProductForProduction(resultDataSet, jobId, productAssociationList, publishCategoryIds, publishCatalogId);

                    if (resultDataSet.Tables[0].Rows.Count > 0)
                    {
                        int PublishCategoryId = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["PublishCategoryId"]);
                        PreviewHelper.GetPimCategorysByCatalogIdAndSetStatus(0, (byte)ZnodePublishStatesEnum.PRODUCTION, 1, PublishCategoryId);
                    }

                    //Set category publish progress done by setting IsCatalogPublished and IsCategoryPublished to true.
                    SetCategoryPublishDone(publishCatalogId, ZnodePublishStatesEnum.PRODUCTION.ToString());

                    return new PublishedModel { IsPublished = true };
                }
                catch (Exception ex)
                {
                    //Set category publish progress failed by setting IsCatalogPublished and IsCategoryPublished to false.
                    SetCategoryPublishFailed(publishCatalogId, ZnodePublishStatesEnum.PRODUCTION.ToString());
                    PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 100, false, true, ex);
                    ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    return new PublishedModel { IsPublished = false };
                }
            }
            else
            {
                if (resultDataSet?.Tables[0].Rows.Count > 0)
                {
                    int PublishCategoryId = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["PublishCategoryId"]);
                    PreviewHelper.GetPimCategorysByCatalogIdAndSetStatus(0, (byte)ZnodePublishStatesEnum.PRODUCTION, 1, PublishCategoryId);
                }
                return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.ErrorPublished };
            }
        }

        //Set is category publish flag on category publish.
        protected virtual void SetIsCategoryPublishedFlagOnCategoryPublish(int publishCatalogId,string revisionType, bool? isCategoryPublish)
        {
            foreach (LocaleModel localeModel in GetActiveLocaleList())
            {
                if (localeModel?.LocaleId > 0 && publishCatalogId > 0)
                {
                    int versionId = PreviewHelper.GetVersionId(publishCatalogId, revisionType, Convert.ToInt32(localeModel?.LocaleId));
                    ZnodePublishCatalogLog znodePublishCatalogLog = _publishCatalogLogRepository.Table.FirstOrDefault(x => x.PublishCatalogLogId == versionId);

                    if (IsNotNull(znodePublishCatalogLog))
                    {
                        znodePublishCatalogLog.IsCategoryPublished = isCategoryPublish;
                        znodePublishCatalogLog.IsCatalogPublished = isCategoryPublish;

                        _publishCatalogLogRepository.Update(znodePublishCatalogLog);
                    }
                }
            }
        }

        //Set category publish progress in progress by setting IsCatalogPublished and IsCategoryPublished to null.
        protected virtual void SetCategoryPublishInProgress(int publishCatalogId, string revisionType)
          =>  SetIsCategoryPublishedFlagOnCategoryPublish(publishCatalogId, revisionType, null);

        //Set category publish progress done by setting IsCatalogPublished and IsCategoryPublished to true.
        protected virtual void SetCategoryPublishDone(int publishCatalogId, string revisionType)
          => SetIsCategoryPublishedFlagOnCategoryPublish(publishCatalogId, revisionType, true);

        //Set category publish progress failed by setting IsCatalogPublished and IsCategoryPublished to false.
        protected virtual void SetCategoryPublishFailed(int publishCatalogId, string revisionType)
         => SetIsCategoryPublishedFlagOnCategoryPublish(publishCatalogId, revisionType, false);

        //Publish category product for production and delete old data from mongo.
        protected virtual void PublishCategoryProductForProduction(DataSet resultDataSet, Guid jobId, List<ProductAssociationPublishModel> productAssociationList, List<int> publishCategoryIds, int publishCatalogId)
        {
            foreach (LocaleModel localeModel in GetActiveLocaleList())
            {
                int localeId = localeModel.LocaleId;
                if (localeId > 0 && publishCatalogId > 0)
                {
                    int versionId = PreviewHelper.GetVersionId(publishCatalogId, ZnodePublishStatesEnum.PRODUCTION.ToString(), localeId);

                    int previewVersionId = PreviewHelper.GetVersionId(publishCatalogId, ZnodePublishStatesEnum.PREVIEW.ToString(), localeId);

                    List<int> publishProductIds = productAssociationList.Where(x => x.LocaleId == localeModel.LocaleId).Select(x => x.PublishProductId).ToList();

                    long publishStartTime = DateTime.Now.Ticks;

                    //Copy category product data from catalog on the basis of preview version id on the basis of revision type and locale id.
                    CopyCatalogCategoryProductForProduction(publishCatalogId, publishCategoryIds, previewVersionId, versionId, versionId, localeId, publishStartTime, publishProductIds);

                    //Update temporary version id with actual version id.Once update done then we get duplicate records on webstore till next delete operation.
                    UpdateTempVersionIdWithActualVersionId(publishCatalogId, publishCategoryIds, versionId, publishProductIds, publishStartTime);

                    //Deleting products and associated products configurations and Delete category or subcategory If a user deletes the category or subcategory from catalog tree and then associated same category for publishing.
                    DeleteOldCatalogCategoriesProductData(resultDataSet, publishCatalogId, publishCategoryIds, versionId, publishProductIds, publishStartTime);

                    //Update PublishStartTime field to null.
                    UpdatePublishStartTimeFieldToNull(publishCatalogId, publishCategoryIds, versionId, publishProductIds, publishStartTime, true);

                    //Save indexes for catalog category products for production.
                    SaveInSearchForCatalogCategoryProductsForProduction(publishCategoryIds, publishCatalogId, localeId, versionId);
                }
            }
        }

        //Save indexes for catalog category products for production.
        private void SaveInSearchForCatalogCategoryProductsForProduction(List<int> publishCategoryIds, int publishCatalogId, int localeId, int versionId)
        {
            List<ProductEntity> productListForSearch = new List<ProductEntity>();

            List<ProductEntity> productEntities = _productMongoRepository.GetEntityList(Query.And(
                Query<ProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                Query<ProductEntity>.In(x => x.ZnodeCategoryIds, publishCategoryIds),
                Query<ProductEntity>.EQ(x => x.VersionId, versionId),
                Query<ProductEntity>.EQ(x => x.LocaleId, localeId)), true);

            productListForSearch.AddRange(productEntities);

            ProductAttributeMapper.MergeSimpleProductAttributes(productEntities);
            //Save Products in Search (Elastic)
            PublishHelper.SaveInSearchForCatalogCategoryProducts(productListForSearch, publishCatalogId, publishCategoryIds, ZnodePublishStatesEnum.PRODUCTION.ToString());
        }

        [Obsolete]
        //Reference in unused method
        //Update SEO product publish status
        private void UpdateSeoPublishStatusBySEOCode(List<string> seoCodes)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("SeoCodes:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, seoCodes);
            List<ZnodeCMSSEODetail> cMSSEODetailList = _seoDetailRepository.Table.Where(x => seoCodes.Contains(x.SEOCode)).ToList();
            foreach (var cMSSEODetail in cMSSEODetailList)
            {
                cMSSEODetail.IsPublish = true;
                _seoDetailRepository.Update(cMSSEODetail);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
        }

        //Delete publish Catalog along with associated category and products
        public virtual bool DeletePublishCatalog(int publishCatalogId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { publishCatalogId = publishCatalogId });

            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter("publishCatalogId", publishCatalogId.ToString(), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("IsDeleteCatalogId", true, ParameterDirection.Input, DbType.Boolean);

            //Delete Publish Catalog and its associated items
            View_ReturnBoolean deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_DeletePublishCatalog @publishCatalogId,@IsDeleteCatalogId")?.FirstOrDefault();

            if (deleteResult?.Status.GetValueOrDefault() ?? false)
                //Delete Publish Catalog and its associated items from mongo DB
                PreviewHelper.DeletePublishCatalogFromMongo(Query<CatalogEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId));

            ZnodeLogging.LogMessage(deleteResult?.Status.GetValueOrDefault() ?? false ? PIM_Resources.SuccessDeletePublishCatalog : PIM_Resources.ErrorDeletePublishCatalog, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return deleteResult?.Status.GetValueOrDefault() ?? false;
        }

        //Get a Catalog Publish Status.
        public virtual PublishCatalogLogListModel GetCatalogPublishStatus(FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to set parameters of SP Znode_GetPublishStatus:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            IZnodeViewRepository<PublishCatalogLogModel> objStoredProc = new ZnodeViewRepository<PublishCatalogLogModel>();
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);

            List<PublishCatalogLogModel> publishCatalogLogs = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishStatus @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT", 4, out pageListModel.TotalRowCount)?.ToList();
            ZnodeLogging.LogMessage("publishCatalogLogs list count:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { publishCatalogLogsListCount = publishCatalogLogs?.Count });

            PublishCatalogLogListModel publishCatalogLogList = new PublishCatalogLogListModel { PublishCatalogLogList = publishCatalogLogs };
            publishCatalogLogList.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return publishCatalogLogList;
        }

        public virtual void CopyAssociatedProductsConfiguration(int versionId, int publishCatalogId, int previewVersionId, int localeId = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { versionId = versionId, publishCatalogId = publishCatalogId, previewVersionId = previewVersionId, localeId = localeId });
            //Publish Group Products 
            CopyGroupProducts(versionId, publishCatalogId, previewVersionId);

            //Publish Bundle Products
            CopyBundleProducts(versionId, publishCatalogId, previewVersionId);

            //Publish Configurable Products
            CopyConfigurableProducts(versionId, publishCatalogId, previewVersionId);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
        }

        //Update Display order product associated to catalog.
        public virtual bool UpdateCatalogCategoryProduct(CatalogAssociationModel catalogAssociationModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { catalogAssociationModel = catalogAssociationModel });

            if (IsCategoryMerchandise)
                return UpdateProductOnCategoryMerchandise(catalogAssociationModel);
            else
                return UpdateProduct(catalogAssociationModel);
        }

        protected virtual bool UpdateProductOnCategoryMerchandise(CatalogAssociationModel catalogAssociationModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            //Check if model is null.
            if (IsNull(catalogAssociationModel))
                throw new ZnodeException(ErrorCodes.NullModel, ZnodeConstant.NullModelError);
            IZnodeRepository<ZnodePimCatalogCategory> _pimCatalogCategoryRepository = new ZnodeRepository<ZnodePimCatalogCategory>();
            IZnodeRepository<ZnodePimCategoryHierarchy> _pimCategoryHierarchyRepository = new ZnodeRepository<ZnodePimCategoryHierarchy>();
            int? categoryId = _pimCategoryHierarchyRepository.Table
                            .FirstOrDefault(m => m.PimCategoryHierarchyId == catalogAssociationModel.PimCategoryHierarchyId)
                            ?.PimCategoryId;

            ZnodeLogging.LogMessage("categoryId:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { categoryId = categoryId });

            List<int?> pimCategoryHierarchyIds = _pimCatalogCategoryRepository.Table
                                            .Where(m => m.PimCategoryId == categoryId
                                             && m.PimCatalogId == catalogAssociationModel.CatalogId
                                             && m.PimProductId == catalogAssociationModel.ProductId
                                             && m.PimCategoryHierarchyId != 0)
                                            .Select(a => a.PimCategoryHierarchyId).ToList();
            bool isUpdated = false;
            pimCategoryHierarchyIds.ForEach(m =>
            {

                ZnodePimCatalogCategory znodePimCatalogCategory = _pimCatalogCategoryRepository.Table
                                                                    .FirstOrDefault(x => x.PimCatalogId == catalogAssociationModel.CatalogId
                                                                    && x.PimCategoryHierarchyId == m
                                                                    && x.PimProductId == catalogAssociationModel.ProductId);

                //Assign value to DisplayOrder.
                isUpdated = UpdateCatalogCategory(catalogAssociationModel, _pimCatalogCategoryRepository, isUpdated, znodePimCatalogCategory);
            });
            productAssociationHelper.UpdateDisplayOrderOfCategory(categoryId, catalogAssociationModel.ProductId, catalogAssociationModel.DisplayOrder);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return isUpdated;
        }

        protected virtual bool UpdateCatalogCategory(CatalogAssociationModel catalogAssociationModel, IZnodeRepository<ZnodePimCatalogCategory> _pimCatalogCategoryRepository, bool isUpdated, ZnodePimCatalogCategory znodePimCatalogCategory)
        {
            if (znodePimCatalogCategory != null)
            {
                znodePimCatalogCategory.DisplayOrder = catalogAssociationModel.DisplayOrder;

                isUpdated = _pimCatalogCategoryRepository.Update(znodePimCatalogCategory);

                ZnodeLogging.LogMessage(isUpdated ? string.Format(PIM_Resources.SuccessUpdateAssociateProductDetails, catalogAssociationModel.ProductId) : PIM_Resources.ErrorUpdateAssociateProductDetails, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            }

            return isUpdated;
        }

        protected virtual bool UpdateProduct(CatalogAssociationModel catalogAssociationModel)
        {
            bool isUpdated = false;
            if (IsNull(catalogAssociationModel))
                throw new ZnodeException(ErrorCodes.NullModel, ZnodeConstant.NullModelError);

            ZnodePimCatalogCategory znodePimCatalogCategory = _pimCatalogCategoryRepository.Table.Where(x => x.PimCatalogId == catalogAssociationModel.CatalogId & x.PimCategoryHierarchyId == catalogAssociationModel.PimCategoryHierarchyId & x.PimProductId == catalogAssociationModel.ProductId)?.FirstOrDefault();

            isUpdated = isUpdated = UpdateCatalogCategory(catalogAssociationModel, _pimCatalogCategoryRepository, isUpdated, znodePimCatalogCategory);

            ZnodeLogging.LogMessage(isUpdated ? string.Format(PIM_Resources.SuccessUpdateAssociateProductDetails, catalogAssociationModel.ProductId) : PIM_Resources.ErrorUpdateAssociateProductDetails, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);

            return isUpdated;
        }

        //If catalog publish is true then create index for its all associated stores.
        public void AssociatedPortalCreateIndex(int publishCatalogId, string revisionType = null)
        {
            try
            {
                ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                ZnodeLogging.LogMessage("Input parameter:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { publishCatalogId = publishCatalogId, revisionType = revisionType });

                ISearchService searchService = GetService<ISearchService>();

                FilterCollection filter = new FilterCollection() { new FilterTuple(ZnodeCatalogIndexEnum.PublishCatalogId.ToString(), FilterOperators.Equals, publishCatalogId.ToString()) };

                PortalIndexModel portalIndex = searchService.GetCatalogIndexData(null, filter);
                if (portalIndex?.CatalogIndexId > 0)
                {
                    portalIndex.CreatedBy = GetLoginUserId();
                    portalIndex.ModifiedBy = GetLoginUserId();
                    portalIndex.RevisionType = revisionType;
                    searchService.InsertCreateIndexData(portalIndex);
                    if (!CheckIndexCreationSucceed(Convert.ToInt32(portalIndex?.SearchCreateIndexMonitorId)))
                        throw new ZnodeException(ErrorCodes.CreationFailed, "Create Index failed.");
                    ZnodeLogging.LogMessage(string.Format(PIM_Resources.SuccessCreateIndexForPortal, publishCatalogId), ZnodeLogging.Components.Portal.ToString(), TraceLevel.Info);
                }
                else
                {
                    PortalIndexModel portalIndexModel = searchService.InsertCreateIndexData(new PortalIndexModel() { IndexName = PublishHelper.GetIndexName(_publishCatalogRepository.GetById(publishCatalogId)?.CatalogName), PublishCatalogId = publishCatalogId, CreatedBy = GetLoginUserId(), ModifiedBy = GetLoginUserId(), RevisionType = revisionType });
                    if (!CheckIndexCreationSucceed(Convert.ToInt32(portalIndex?.SearchCreateIndexMonitorId)))
                        throw new ZnodeException(ErrorCodes.CreationFailed, "Create Index failed.");
                    ZnodeLogging.LogMessage(string.Format(PIM_Resources.SuccessCreateIndexForPortal, publishCatalogId), ZnodeLogging.Components.Portal.ToString(), TraceLevel.Info);
                }
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Warning);
                throw;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(string.Format(PIM_Resources.ErrorCreateIndexForPortal, publishCatalogId), ZnodeLogging.Components.Portal.ToString(), TraceLevel.Error);
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Portal.ToString(), TraceLevel.Error);
            }
        }



        //Get Portal id from filter otherwise set default
        protected virtual int GetPortalId(FilterCollection filters)
        {
            int PortalId = 0;
            if (filters.Exists(x => x.Item1 == ZnodePortalEnum.PortalId.ToString().ToLower()))
            {
                PortalId = Convert.ToInt32(filters.Where(x => x.Item1 == ZnodePortalEnum.PortalId.ToString().ToLower()).Select(x => x.FilterValue).FirstOrDefault());
                filters.RemoveAll(x => x.Item1 == ZnodePortalEnum.PortalId.ToString().ToLower());
            }
            ZnodeLogging.LogMessage("PortalId:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, PortalId);
            return PortalId;
        }

        //Check whether or not index creation completed or not. 
        protected virtual bool CheckIndexCreationSucceed(int searchCreateIndexMonitorId)
        {
            var searchIndexServerStatus = _searchIndexServerStatusRepository.Table.FirstOrDefault(x => x.SearchIndexMonitorId == searchCreateIndexMonitorId);
            if (!string.IsNullOrEmpty(searchIndexServerStatus?.Status) && !searchIndexServerStatus.Status.Equals("Complete", StringComparison.InvariantCultureIgnoreCase))
                return false;
            return true;
        }

        //Set Product status filter
        protected virtual void SetProductStatusFilter(FilterCollection filters)
        {
            bool isActive = false;
            if (Equals(filters?.Exists(x => x.Item1.ToLower() == ZnodeConstant.IsActive.ToLower()), true))
            {
                isActive = Equals(filters?.FirstOrDefault(x => x.Item1.ToLower() == ZnodeConstant.IsActive.ToLower()).FilterValue?.Contains("true"), true);
                filters?.RemoveAll(x => x.Item1.ToLower() == ZnodeConstant.IsActive.ToLower());
            }
            if(isActive)
                filters?.Add(new FilterTuple(ZnodeConstant.IsActive.ToLower(), FilterOperators.Equals, $"'{isActive}'".ToLower()));
        }

        #endregion Public Methods

        #region Private Methods

        //Update display order
        private bool UpdateDisplayOrder(CatalogAssociateCategoryModel catalogAssociateCategoryModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodePimCategoryHierarchy categoryHierarchy = _pimCategoryHierarchyRepository.Table.FirstOrDefault(x => x.PimCatalogId == catalogAssociateCategoryModel.PimCatalogId && x.PimCategoryHierarchyId == catalogAssociateCategoryModel.PimCategoryHierarchyId);
            if (IsNotNull(categoryHierarchy))
            {
                categoryHierarchy.DisplayOrder = catalogAssociateCategoryModel.DisplayOrder;
                ZnodeLogging.LogMessage("CatalogAssociateCategoryModel with: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { DisplayOrder = catalogAssociateCategoryModel?.DisplayOrder, PimCatalogId = catalogAssociateCategoryModel?.PimCatalogId, PimCategoryHierarchyId = catalogAssociateCategoryModel?.PimCategoryHierarchyId });
                bool isUpdated = _pimCategoryHierarchyRepository.Update(categoryHierarchy);

                ZnodeLogging.LogMessage(isUpdated ? string.Format(PIM_Resources.SuccessUpdateAssociateCategoryDetails, catalogAssociateCategoryModel.PimCategoryId) : PIM_Resources.ErrorUpdateAssociateCategoryDetails, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                return isUpdated;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return false;
        }

        //Get Publish category products details for perticular catalog and category.
        private DataSet GetPublishCategoryProduct(int pimCatalogId, int pimCategoryHierarchyId, ZnodePublishStatesEnum PublishState, out int status, Guid jobId, string categoryPublishProgressMessage)
        {
            int? publishCatalogId = 0;
            try
            {
                ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                ZnodeLogging.LogMessage("pimCatalogId, pimCategoryHierarchyId, PublishState:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { pimCatalogId, pimCategoryHierarchyId, PublishState });
                if (_publishCatalogLogRepository.Table.Any(x => x.PimCatalogId == pimCatalogId))
                {
                    status = 0;

                    publishCatalogId = _publishCatalogRepository.Table?.Where(x => x.PimCatalogId == pimCatalogId)?.FirstOrDefault()?.PublishCatalogId;

                    //Set category publish progress in progress by setting IsCatalogPublished and IsCategoryPublished to null
                    SetCategoryPublishInProgress(Convert.ToInt32(publishCatalogId), PublishState.ToString());

                    List<int> localeIds = GetActiveLocaleList().Select(x => x.LocaleId).ToList();

                    DataSet resultDataSet = new DataSet();

                    ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
                    executeSpHelper.GetParameter("@PimCatalogId", pimCatalogId, ParameterDirection.Input, SqlDbType.Int);
                    executeSpHelper.GetParameter("@PimCategoryHierarchyId", pimCategoryHierarchyId, ParameterDirection.Input, SqlDbType.Int);
                    executeSpHelper.GetParameter("@UserId", GetLoginUserId(), ParameterDirection.Input, SqlDbType.Int);
                    executeSpHelper.GetParameter("@Status", status, ParameterDirection.Output, SqlDbType.Int);
                    executeSpHelper.GetParameter("@PublishStateId", (byte)PublishState, ParameterDirection.Input, SqlDbType.Int);

                    DataTable localeIdsData = ConvertKeywordListToDataTable(localeIds);
                    if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
                    {
                        executeSpHelper.GetParameter("@LocaleId", localeIdsData?.ToJson(), ParameterDirection.Input, SqlDbType.NVarChar);
                        resultDataSet = executeSpHelper.GetSPResultInDataSet("Znode_GetPublishCategoryProductsWithJSON", 4, out status);
                    }
                    else
                    {
                        executeSpHelper.SetTableValueParameter("@LocaleId", localeIdsData, ParameterDirection.Input, SqlDbType.Structured, "dbo.TransferId");
                        resultDataSet = executeSpHelper.GetSPResultInDataSet("Znode_GetPublishCategoryProducts", 4, out status);
                    }

                    //Checks weather status is 0, if yes then set required message and marked IsCatalogPublished Flag to false.
                    if (status == 0 && resultDataSet.Tables.Count ==1)
                    {
                        PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 100, false, true, new ZnodeException(ErrorCodes.InvalidData, resultDataSet.Tables[0].Rows[0]["MessageDetails"].ToString()));
                        //Set category publish progress failed by setting IsCatalogPublished and IsCategoryPublished to false.
                        SetCategoryPublishFailed(Convert.ToInt32(publishCatalogId), PublishState.ToString());
                        return null;
                    }

                    ZnodeLogging.LogMessage("SP out parameter: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { status = status });
                    ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                    return resultDataSet;
                }
                else
                {
                    status = 0;
                    PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 100, false, true, new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.ErrorPublishCategoryProduct));
                    return null;
                }
            }
            catch (Exception ex)
            {
                status = 0;
                //Set category publish progress failed by setting IsCatalogPublished and IsCategoryPublished to false.
                SetCategoryPublishFailed(Convert.ToInt32(publishCatalogId), PublishState.ToString());
                PreviewHelper.LogProgress(jobId, categoryPublishProgressMessage, 100, false, true, ex);
                return null;
            }
        }

        //Copy category product data from catalog on the basis of preview version id on the basis of revision type and locale id.
        protected virtual void CopyCatalogCategoryProductForProduction(int publishCatalogId, List<int> publishCategoryIds, int previousVersionId, int currentVersionId, int prevProdVersion, int localeId, long publishStartTime, List<int> publishProductIds)
        {
            ZnodeLogging.LogMessage("Execution start:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            Parallel.Invoke(new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },
                    //Copy the data of Category with temperory version.
                    () => {  PreviewHelper.CopyEntitiesForPreviewAndProductionOnCategoryPublishWithTempVersion<CategoryEntity>(publishStartTime, Query.And(Query<CategoryEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),Query<CategoryEntity>.In(x => x.ZnodeCategoryId, publishCategoryIds),
                        Query<CategoryEntity>.EQ(x => x.VersionId, previousVersionId),Query<CategoryEntity>.EQ(x => x.LocaleId, localeId)), new MongoRepository<CategoryEntity>());},
                    //Copy the data of product with temperory version..
                    () => {  PreviewHelper.CopyEntitiesForPreviewAndProductionOnCategoryPublishWithTempVersion<ProductEntity>(publishStartTime, Query.And(Query<ProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),Query<ProductEntity>.In(x => x.ZnodeProductId, publishProductIds),
                        Query<ProductEntity>.EQ(x => x.VersionId, previousVersionId),Query<ProductEntity>.EQ(x => x.LocaleId, localeId)), new MongoRepository<ProductEntity>());},
                    //Copy the data of Group Product with temperory version..
                    () => {  PreviewHelper.CopyEntitiesForPreviewAndProductionOnCategoryPublishWithTempVersion<GroupProductEntity>(publishStartTime, Query.And(Query<GroupProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                        Query<GroupProductEntity>.EQ(x => x.VersionId, previousVersionId)), new MongoRepository<GroupProductEntity>());},
                    //Copy the data of Bundle Product with temperory version..
                    () => {  PreviewHelper.CopyEntitiesForPreviewAndProductionOnCategoryPublishWithTempVersion<BundleProductEntity>(publishStartTime, Query.And(Query<BundleProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                        Query<BundleProductEntity>.EQ(x => x.VersionId, previousVersionId)), new MongoRepository<BundleProductEntity>()); },
                    //Copy the data of Add On Product with temperory version..
                    () => {  PreviewHelper.CopyEntitiesForPreviewAndProductionOnCategoryPublishWithTempVersion<AddonEntity>(publishStartTime, Query.And(Query<AddonEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                        Query<AddonEntity>.EQ(x => x.VersionId, previousVersionId),Query<AddonEntity>.EQ(x => x.LocaleId, localeId)), new MongoRepository<AddonEntity>());},
                    //Copy the data of Configurable Product with temperory version..
                    () => {  PreviewHelper.CopyEntitiesForPreviewAndProductionOnCategoryPublishWithTempVersion<ConfigurableProductEntity>(publishStartTime, Query.And(Query<ConfigurableProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                        Query<ConfigurableProductEntity>.EQ(x => x.VersionId, previousVersionId)), new MongoRepository<ConfigurableProductEntity>());});

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //Return List of categories on basis of where clause
        private CatalogAssociateCategoryListModel GetXmlCategory(FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            //Get locale Id.
            int localeId = GetLocaleId(filters);
            int profileCatalogId, pimCatalogId, pimCategoryId, pimCategoryHierarchyId;
            string isAssociated;
            //Set filters for associated categories for profile.
            SetFiltersForAssociatedCategoriesForProfile(filters, out profileCatalogId, out isAssociated, out pimCatalogId, out pimCategoryId, out pimCategoryHierarchyId);

            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            string whereClause = ProductService.GenerateXMLWhereClauseForSP(filters.ToFilterDataCollection());

            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            ZnodeLogging.LogMessage("Whereclause:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, whereClause);
            objStoredProc.GetParameter("@WhereClause", whereClause, ParameterDirection.Input, SqlDbType.NVarChar);
            objStoredProc.GetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, SqlDbType.NVarChar);
            objStoredProc.GetParameter("@RowsCount", pageListModel.TotalRowCount, ParameterDirection.Output, SqlDbType.Int);
            objStoredProc.GetParameter("@LocaleId", localeId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter(ZnodeProfileCatalogEnum.PimCatalogId.ToString(), pimCatalogId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@IsAssociated", isAssociated, ParameterDirection.Input, SqlDbType.NVarChar);
            objStoredProc.GetParameter(ZnodeProfileCatalogEnum.ProfileCatalogId.ToString(), profileCatalogId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter(ZnodePimCategoryEnum.PimCategoryId.ToString(), pimCategoryId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter(ZnodePimCategoryHierarchyEnum.PimCategoryHierarchyId.ToString(), pimCategoryHierarchyId, ParameterDirection.Input, SqlDbType.Int);

            ZnodeLogging.LogMessage("Znode_GetPimCatalogAssociatedCategory SP parameters: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { whereClause = whereClause, pageListModel = pageListModel?.ToDebugString(),
                localeId = localeId, pimCatalogId = pimCatalogId, isAssociated = isAssociated, profileCatalogId = profileCatalogId, pimCategoryId = pimCategoryId, pimCategoryHierarchyId = pimCategoryHierarchyId });
            var ds = objStoredProc.GetSPResultInDataSet("Znode_GetPimCatalogAssociatedCategory");

            CatalogAssociateCategoryListModel categoryListModel = Xmldataset(ds, out pageListModel.TotalRowCount);
            categoryListModel.BindPageListModel(pageListModel);

            return categoryListModel;
        }

        //Convert dataset to dynamic list
        private CatalogAssociateCategoryListModel Xmldataset(DataSet ds, out int recoredCount)
        {
            // out pageListModel.TotalRowCount
            recoredCount = 0;
            if (IsNotNull(ds) && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                var xml = Convert.ToString(ds.Tables[0]?.Rows[0]["CategoryXML"]);

                if (!string.IsNullOrEmpty(xml))
                {
                    var columns = ds.Tables[1];

                    var _columnlist = GetDictionary(columns);

                    if (!string.IsNullOrEmpty(ds.Tables[2].Rows[0]["RowsCount"].ToString()))
                        recoredCount = Convert.ToInt32(ds.Tables[2].Rows[0]["RowsCount"].ToString());

                    ZnodeLogging.LogMessage("recoredCount: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { recoredCount = recoredCount });
                    XDocument xmlDoc = XDocument.Parse(xml);

                    dynamic root = new ExpandoObject();

                    XmlToDynamic.Parse(root, xmlDoc.Elements().First());

                    var _list = new List<dynamic>();
                    if (!(root.MainCategory.Category is List<dynamic>))
                        _list.Add(root.MainCategory.Category);
                    else
                        _list = (List<dynamic>)root.MainCategory.Category;

                    return new CatalogAssociateCategoryListModel { AttributeColumnName = _columnlist, XmlDataList = _list };
                }
            }
            return new CatalogAssociateCategoryListModel();
        }

        //Get dictionary of datatable
        private Dictionary<string, object> GetDictionary(DataTable dt)
        {
            return dt.AsEnumerable()
              .ToDictionary<DataRow, string, object>(row => row.Field<string>(0),
                                        row => row.Field<object>(1));
        }

        //Get the tree with its child.
        protected virtual List<ContentPageTreeModel> GetAllNode(List<ContentPageTreeModel> childTreeNodes)
        {
            if (IsNotNull(childTreeNodes))
            {
                foreach (ContentPageTreeModel node in childTreeNodes)
                {
                    //find all child folder and add to list
                    List<ContentPageTreeModel> child = childTreeNodes.Where(x => x.ParentId == node.Id && x.Id != node.Id)?.ToList();
                    node.Children = IsNotNull(node.Children) ? node.Children : new List<ContentPageTreeModel>();
                    node.Children.AddRange(GetAllNode(child));
                }
                ZnodeLogging.LogMessage("ChildTreeNodes count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { ChildTreeNodesCount = childTreeNodes?.Count });
                return childTreeNodes;
            }
            return new List<ContentPageTreeModel>();
        }

        private IList<ZnodePimCategoryProduct> GetAssociatedProducts(string categoryIds)
        {
            if (!string.IsNullOrEmpty(categoryIds))
            {
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodePimCategoryEnum.PimCategoryId.ToString(), FilterOperators.In, categoryIds));
                string whereClause = DynamicClauseHelper.GenerateDynamicWhereClause(filters.ToFilterDataCollection());

                ZnodeLogging.LogMessage("WhereClause to get associated products: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { WhereClause = whereClause });
                IZnodeRepository<ZnodePimCategoryProduct> _pimCategoryProductRepository = new ZnodeRepository<ZnodePimCategoryProduct>();
                return _pimCategoryProductRepository.GetEntityList(whereClause);
            }
            return null;
        }

        protected virtual List<ZnodePimCatalogCategory> AssociateCategoriesToCatalog(int catalogId, Dictionary<int, int> categoryHierarchyIds, IList<ZnodePimCategoryProduct> categoryAssociatedProducts)
        {
            {
                List<ZnodePimCatalogCategory> catalogCategory = new List<ZnodePimCatalogCategory>();
                if (categoryHierarchyIds?.Count > 0)
                {
                    ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { catalogId = catalogId, categoryHierarchyIdsCount = categoryHierarchyIds?.Count, categoryAssociatedProductsCount = categoryAssociatedProducts?.Count });
                    foreach (var category in categoryHierarchyIds)
                    {
                        List<int> productIds = categoryAssociatedProducts?.Where(x => x.PimCategoryId == category.Key)?.Select(x => x.PimProductId)?.ToList();

                        if (productIds?.Count > 0)
                        {
                            if (IsCategoryMerchandise)
                            {
                                //Add items to catalog category list.
                                foreach (int product in productIds)
                                {
                                    int? DisplayOrder = categoryAssociatedProducts?.FirstOrDefault(m => m.PimCategoryId == category.Key
                                                                                   && m.PimProductId == product)
                                                                                 ?.DisplayOrder;
                                    catalogCategory.Add(new ZnodePimCatalogCategory { PimCatalogId = catalogId, PimCategoryId = category.Key, DisplayOrder = DisplayOrder, PimCategoryHierarchyId = category.Value, PimProductId = product });
                                }
                            }
                            else
                            {
                                foreach (int product in productIds)
                                    catalogCategory.Add(new ZnodePimCatalogCategory { PimCatalogId = catalogId, PimCategoryId = category.Key, PimCategoryHierarchyId = category.Value, PimProductId = product, DisplayOrder = ZnodeConstant.DisplayOrder });
                            }
                        }
                        else
                            catalogCategory.Add(new ZnodePimCatalogCategory { PimCatalogId = catalogId, PimCategoryId = category.Key, DisplayOrder = 999, PimCategoryHierarchyId = category.Value });
                    }
                    return _pimCatalogCategoryRepository.Insert(catalogCategory)?.ToList();
                }
                return null;
            }
        }

        protected virtual bool AssociateProductsToCatalog(int catalogId, int categoryId, int pimCategoryHierarchyId, string[] productIds)
        {
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { catalogId = catalogId, categoryId = categoryId, pimCategoryHierarchyId = pimCategoryHierarchyId, productIds = productIds });
            if (!IsCategoryMerchandise)
                return AssociateProductToCatalogOnCategoryMerchandise(catalogId, categoryId, pimCategoryHierarchyId, productIds);
            else
                return productAssociationHelper.AssociateProductToCatalog(catalogId, categoryId, pimCategoryHierarchyId, productIds);

        }

        protected virtual bool AssociateProductToCatalogOnCategoryMerchandise(int catalogId, int categoryId, int pimCategoryHierarchyId, string[] productIds)
        {
            List<ZnodePimCatalogCategory> catalogCategoryProducts = new List<ZnodePimCatalogCategory>();

            if (productIds?.Count() > 0)
            {
                //Add items to catalog category list with default display order of 999.
                foreach (string productId in productIds)
                    catalogCategoryProducts.Add(new ZnodePimCatalogCategory { PimCatalogId = catalogId, PimCategoryId = categoryId, PimCategoryHierarchyId = pimCategoryHierarchyId, PimProductId = Convert.ToInt32(productId), DisplayOrder =ZnodeConstant.DisplayOrder });
            }

            IEnumerable<ZnodePimCatalogCategory> associatedProducts = _pimCatalogCategoryRepository.Insert(catalogCategoryProducts);
            return associatedProducts?.FirstOrDefault()?.PimCatalogCategoryId > 0;
        }

        private List<ZnodePimCategoryHierarchy> AssociatedChildCategories(int catalogId, int? parentCategory, string[] categoryIds)
        {
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { catalogId = catalogId, parentCategory = parentCategory, categoryIds = categoryIds });
            bool isAssociate = false;
            List<ZnodePimCategoryHierarchy> categoryHierarchy = new List<ZnodePimCategoryHierarchy>();
            if (categoryIds?.Length > 0)
            {
                int maxDisplayOrder = GetMaxDisplayOrder(catalogId, parentCategory);
                foreach (string categoryId in categoryIds)
                {
                    maxDisplayOrder += 75000;
                    categoryHierarchy.Add(new ZnodePimCategoryHierarchy { PimCatalogId = catalogId, ParentPimCategoryHierarchyId = parentCategory > 0 ? parentCategory : null, PimCategoryId = Convert.ToInt32(categoryId), IsActive = true, DisplayOrder = maxDisplayOrder });
                }

                categoryHierarchy = _pimCategoryHierarchyRepository.Insert(categoryHierarchy)?.ToList();
                ZnodeLogging.LogMessage("CategoryHierarchy list count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { categoryHierarchyListCount = categoryHierarchy?.Count });
            }
            return categoryHierarchy;
        }

        //Get the maximum display order of category.
        private int GetMaxDisplayOrder(int catalogId, int? parentCategory)
        {
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { catalogId = catalogId, parentCategory = parentCategory });
            parentCategory = parentCategory > 0 ? parentCategory : null;
            return _pimCategoryHierarchyRepository.Table.Where(x => x.PimCatalogId == catalogId && x.ParentPimCategoryHierarchyId == parentCategory)?.Max(x => x.DisplayOrder) ?? 0;
        }

        //Set filters for associated categories for profile.
        private void SetFiltersForAssociatedCategoriesForProfile(FilterCollection filters, out int profileCatalogId, out string isAssociated, out int pimCatalogId, out int pimCategoryId, out int pimCategoryHierarchyId)
        {
            profileCatalogId = Convert.ToInt32(filters.Where(filterTuple => filterTuple.Item1 == ZnodeProfileCatalogEnum.ProfileCatalogId.ToString().ToLower())?.FirstOrDefault()?.Item3);
            isAssociated = (filters.Where(filterTuple => filterTuple.Item1 == FilterKeys.IsAssociated.ToLower().ToString())?.FirstOrDefault()?.Item3);
            pimCatalogId = Convert.ToInt32(filters.Where(filterTuple => filterTuple.Item1 == ZnodeProfileCatalogEnum.PimCatalogId.ToString().ToLower())?.FirstOrDefault()?.Item3);
            pimCategoryId = Convert.ToInt32(filters.Where(filterTuple => filterTuple.Item1 == ZnodePimCategoryEnum.PimCategoryId.ToString().ToLower())?.FirstOrDefault()?.Item3);
            pimCategoryHierarchyId = Convert.ToInt32(filters.FirstOrDefault(filterTuple => filterTuple.Item1 == ZnodePimCategoryHierarchyEnum.PimCategoryHierarchyId.ToString().ToLower())?.Item3);

            ZnodeLogging.LogMessage("Out parameters: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { profileCatalogId = profileCatalogId, isAssociated = isAssociated, pimCatalogId = pimCatalogId, pimCategoryId = pimCategoryId, pimCategoryHierarchyId = pimCategoryHierarchyId });
            filters.Remove(filters.Where(filterTuple => filterTuple.Item1 == ZnodeProfileCatalogEnum.ProfileCatalogId.ToString().ToLower())?.FirstOrDefault());
            filters.RemoveAll(x => x.Item1.Equals(FilterKeys.IsAssociated.ToLower(), StringComparison.InvariantCultureIgnoreCase));
            filters.Remove(filters.Where(filterTuple => filterTuple.Item1 == ZnodeProfileCatalogEnum.PimCatalogId.ToString().ToLower())?.FirstOrDefault());
            filters.Remove(filters.Where(filterTuple => filterTuple.Item1 == ZnodePimCategoryEnum.PimCategoryId.ToString().ToLower())?.FirstOrDefault());
            filters.Remove(filters.FirstOrDefault(filterTuple => filterTuple.Item1 == ZnodePimCategoryHierarchyEnum.PimCategoryHierarchyId.ToString().ToLower()));
        }

        //Unassocite category hierarchy from catalog.
        private bool UnAssociateCategories(CatalogAssociationModel catalogAssociationModel)
        {
            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter(ZnodePimCatalogEnum.PimCatalogId.ToString(), catalogAssociationModel.CatalogId, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter(ZnodePimCategoryHierarchyEnum.PimCategoryHierarchyId.ToString(), string.Join(",", catalogAssociationModel.PimCategoryHierarchyIds), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);

            ZnodeLogging.LogMessage("CatalogId and PimCategoryHierarchyIds to unassociate categories: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { CatalogId = catalogAssociationModel?.CatalogId, PimCategoryHierarchyIds = catalogAssociationModel?.PimCategoryHierarchyIds });
            int status = 0;
            IList<View_ReturnBoolean> deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_DeletePimCategoryHierarchy @PimCategoryHierarchyId, @PimCatalogId, @Status OUT", 2, out status);
            if (deleteResult.FirstOrDefault().Status.Value)
            {
                ZnodeLogging.LogMessage(PIM_Resources.SuccessUnassociateCategories, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                return true;
            }
            else
            {
                ZnodeLogging.LogMessage(PIM_Resources.ErrorUnassociateCategories, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                return false;
            }
        }

        //Set filters to get unassociated categories of catalog.
        private FilterCollection SetFilterToUnAssociateCategoryProducts(CatalogAssociationModel catalogAssociationModel)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodePimCategoryHierarchyEnum.PimCatalogId.ToString(), FilterOperators.Equals, catalogAssociationModel.CatalogId.ToString()));

            if (!string.IsNullOrEmpty(catalogAssociationModel.CategoryIds))
                filters.Add(new FilterTuple(ZnodePimCategoryEnum.PimCategoryId.ToString(), FilterOperators.In, catalogAssociationModel.CategoryIds));
            else if (!string.IsNullOrEmpty(catalogAssociationModel.ProductIds))
            {
                filters.Add(new FilterTuple(ZnodePimCatalogCategoryEnum.PimCategoryId.ToString(), FilterOperators.Equals, Convert.ToString(catalogAssociationModel.CategoryId)));
                filters.Add(new FilterTuple(ZnodePimProductEnum.PimProductId.ToString(), FilterOperators.In, catalogAssociationModel.ProductIds));
            }
            return filters;
        }

        //Set filters to get unassociated categories of catalog.
        private FilterCollection SetFilterToUnAssociateCategoryProductsFromProfileCatalog(CatalogAssociationModel catalogAssociationModel)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodeProfileCatalogCategoryEnum.ProfileCatalogCategoryId.ToString(), FilterOperators.In, catalogAssociationModel.ProfileCatalogCategoryIds.ToString()));
            return filters;
        }

        //Set filters to get display order of category.
        private FilterCollection SetFiltersToSetDisplayOrder(CatalogAssociateCategoryModel catalogAssociateCategoryModel)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodePimCategoryHierarchyEnum.PimCatalogId.ToString(), FilterOperators.Equals, catalogAssociateCategoryModel.PimCatalogId.ToString()));

            if (catalogAssociateCategoryModel.CategoryId > 0)
                filters.Add(new FilterTuple(ZnodePimCategoryHierarchyEnum.PimCategoryId.ToString(), FilterOperators.In, $"{catalogAssociateCategoryModel.PimCategoryId},{catalogAssociateCategoryModel.CategoryId}"));
            else
                filters.Add(new FilterTuple(ZnodePimCategoryHierarchyEnum.PimCategoryId.ToString(), FilterOperators.Equals, catalogAssociateCategoryModel.PimCategoryId.ToString()));

            ZnodeLogging.LogMessage("FiltersToSetDisplayOrder ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { FiltersToSetDisplayOrder = filters });
            return filters;
        }

        //Remove the categories from Catalog.
        private bool UnAssociateCategoryFromProfile(CatalogAssociationModel catalogAssociationModel)
        {
            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter(ZnodeProfileCatalogEnum.ProfileCatalogId.ToString(), catalogAssociationModel.ProfileCatalogId, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter(ZnodePimCatalogEnum.PimCatalogId.ToString(), catalogAssociationModel.CatalogId, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter(ZnodePimCategoryHierarchyEnum.PimCategoryHierarchyId.ToString(), string.Join(",", catalogAssociationModel.PimCategoryHierarchyIds), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
            ZnodeLogging.LogMessage("SP parameters to unassociate category from profile: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { ProfileCatalogId = catalogAssociationModel?.ProfileCatalogId, CatalogId = catalogAssociationModel?.CatalogId, PimCategoryHierarchyIds = catalogAssociationModel?.PimCategoryHierarchyIds });
            int status = 0;
            IList<View_ReturnBoolean> deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_DeleteProfileCatalogCategory @ProfileCatalogId, @PimCatalogId,@PimCategoryHierarchyId, @Status OUT", 3, out status);
            if (deleteResult.FirstOrDefault().Status.Value)
            {
                ZnodeLogging.LogMessage(PIM_Resources.SuccessUnassociateCategoriesFromProfile, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                return true;
            }
            else
            {
                ZnodeLogging.LogMessage(PIM_Resources.ErrorUnassociateCategoriesFromProfile, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                return false;
            }
        }

        private Dictionary<int, int> GetCategoryHierarchyIdDictionary(string[] categoryIds, List<ZnodePimCategoryHierarchy> categoryHierarchy)
        {
            Dictionary<int, int> categoryHierarchyIds = new Dictionary<int, int>();
            for (int index = 0; index < categoryIds.Length; index++)
                categoryHierarchyIds.Add(Convert.ToInt32(categoryIds[index]), categoryHierarchy[index].PimCategoryHierarchyId);

            return categoryHierarchyIds;
        }

        //Add new version to version entity
        private void CreateMongoVersion(string revisionType, int versionId, int publishCatalogId, int localeId = 0)
        {

            new MongoRepository<VersionEntity>().Create(new VersionEntity
            {
                VersionId = versionId,
                ZnodeCatalogId = publishCatalogId,
                RevisionType = revisionType,
                LocaleId = localeId,
                IsPublishSuccess = false
            });
        }

        //This method will set the flag IsPublisSuccess of versionEntity to true.
        //TODO: To add a parameter here and make it not hardcoded, so that the flag can be set to true in all cases.
        private void UpdateIsPublishedFlag(int versionId, bool isPublishSuccess = false)
        {
            IMongoRepository<VersionEntity> versionRepository = new MongoRepository<VersionEntity>();

            VersionEntity entities = versionRepository.GetEntity(Query<VersionEntity>.EQ(x => x.VersionId, versionId), true);
            if (IsNotNull(entities))
            {
                entities.IsPublishSuccess = isPublishSuccess;
                versionRepository.UpdateEntity(Query<VersionEntity>.EQ(x => x.Id, entities.Id), entities);
            }
        }

        //Delete Previous version catalog
        private void DeleteMongoVersion(int previousVersionId, int publishCatalogId)
        {
            //Delete Previous version catalog
            PreviewHelper.DeletePublishCatalogFromMongo(Query.And(
                Query<CatalogEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId)
                ), previousVersionId);

            //Delete previous version Seo data 
            PreviewHelper.DeleteRecords(new MongoRepository<SeoEntity>(), new MongoRepository<_LogSeoEntity>(), previousVersionId,
                    Query<VersionEntity>.EQ(x => x.VersionId, previousVersionId));
        }

        private void CreateNewMongoVersion(string revisionType, int versionId, int publishCatalogId, int localeId = 0)
        {
            ZnodeLogging.LogMessage("VersionId,PublishCatalogId,LocaleId:", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { versionId, publishCatalogId, localeId });
            //Add new version to version entity
            new MongoRepository<VersionEntity>().Create(new VersionEntity
            {
                VersionId = versionId,
                ZnodeCatalogId = publishCatalogId,
                RevisionType = revisionType,
                LocaleId = localeId,
                IsPublishSuccess = true
            });
        }

        //Delete catalog categories.
        private void DeleteCatalogCategories(DataSet resultDataSet, int publishCatalogId, int actualVersionId)
        {
            if (resultDataSet?.Tables[0].Rows.Count > 0)
            {
                List<int> publishCategoryIdsForDelete = resultDataSet.Tables[0].AsEnumerable().Select(r => r.Field<int>("PublishCategoryId")).Distinct().ToList();

                ZnodeLogging.LogMessage("publishCategoryIdsForDelete: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { publishCategoryIdsForDelete = publishCategoryIdsForDelete });
                PreviewHelper.DeleteRecords(new MongoRepository<CategoryEntity>(), new MongoRepository<_LogCategoryEntity>(), actualVersionId, Query.And(
                  Query<CategoryEntity>.EQ(c => c.ZnodeCatalogId, publishCatalogId),
                  Query<CategoryEntity>.In(c => c.ZnodeCategoryId, publishCategoryIdsForDelete),
                  Query<CategoryEntity>.EQ(c => c.VersionId, actualVersionId),
                  Query<MongoEntity>.EQ(c => c.PublishStartTime, null)));
            }

        }

        //Get catalog category entities on the basis of category xml list and convert it into entity list.
        private List<CategoryEntity> GetCatalogCategoriesEntities(DataSet resultDataSet)
        {
            if (!(resultDataSet?.Tables[1].Rows.Count > 0))
                return new List<CategoryEntity>();

            List<string> categoryXmlList = resultDataSet.Tables[1].AsEnumerable().Select(dataRow => dataRow.Field<string>("CategoryXml")).ToList();
            List<CategoryEntity> categoryEntities = ConvertListOfXMLStringToListModel<CategoryEntity>(categoryXmlList);

            return categoryEntities;
        }

        //Delete Associated Product Configurations
        private void DeleteAssociatedProductConfigurations(int publishCatalogId, List<int> publishCategoryIds, List<int> publishProductIds, int actualVersionId/*, long publishStartTime*/)
        {
            DeleteProductByCategoryIds(publishCatalogId, publishCategoryIds, actualVersionId);
            DeleteProductByProductIds(publishCatalogId, publishProductIds, actualVersionId);

            IMongoQuery query = Query.And(Query<AddonEntity>.In(pr => pr.ZnodeProductId, publishProductIds), Query<MongoEntity>.EQ(x => x.PublishStartTime, null));

            ZnodeLogging.LogMessage("Mongo query to delete publish addons, group products, configurable products, bundle products: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { Query = query });
            //Delete Publish Addons
            PreviewHelper.DeleteRecords(new MongoRepository<AddonEntity>(), new MongoRepository<_LogAddonEntity>(), actualVersionId, query);

            //Delete Publish Group Products
            PreviewHelper.DeleteRecords(new MongoRepository<GroupProductEntity>(), new MongoRepository<_LogGroupProductEntity>(), actualVersionId, query);

            //Delete Publish Configurable Products
            PreviewHelper.DeleteRecords(new MongoRepository<ConfigurableProductEntity>(), new MongoRepository<_LogConfigurableProductEntity>(), actualVersionId, query);

            //Delete Publish Bundle Products
            PreviewHelper.DeleteRecords(new MongoRepository<BundleProductEntity>(), new MongoRepository<_LogBundleProductEntity>(), actualVersionId, query);
        }

        // Delete product by product Ids
        protected virtual void DeleteProductByProductIds(int publishCatalogId, List<int> publishProductIds, int actualVersionId)
        {
            PreviewHelper.DeleteRecords(_productMongoRepository, new MongoRepository<_LogProductEntity>(), actualVersionId, Query.And(
                        Query<ProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                        Query<ProductEntity>.EQ(x => x.VersionId, actualVersionId),
                        Query<ProductEntity>.EQ(x => x.PublishStartTime, null),
                        Query<ProductEntity>.In(x => x.ZnodeProductId, publishProductIds)
                        ));
        }

        // Delete product by category Ids
        protected virtual void DeleteProductByCategoryIds(int publishCatalogId, List<int> publishCategoryIds, int actualVersionId)
        {
            PreviewHelper.DeleteRecords(_productMongoRepository, new MongoRepository<_LogProductEntity>(), actualVersionId, Query.And(
                        Query<ProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                        Query<ProductEntity>.EQ(x => x.VersionId, actualVersionId),
                        Query<ProductEntity>.EQ(x => x.PublishStartTime, null),
                        Query<ProductEntity>.In(x => x.ZnodeCategoryIds, publishCategoryIds)
                        ));
        }

        //Copy preview associate product data into publish.
        private void CopyAddons(int versionId, int publishCatalogId, int previewVersionId, int localeId)
           => PreviewHelper.CopyAssociatedProductsToMongo<AddonEntity>(versionId, Query.And(
                            Query<AddonEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                            Query<AddonEntity>.EQ(x => x.VersionId, previewVersionId),
                            Query<AddonEntity>.EQ(x => x.LocaleId, localeId)
                            ), new MongoRepository<AddonEntity>());

        //Copy preview associate product data into publish.
        private void CopyGroupProducts(int versionId, int publishCatalogId, int previewVersionId)
           => PreviewHelper.CopyAssociatedProductsToMongo<GroupProductEntity>(versionId, Query.And(
                            Query<GroupProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                            Query<GroupProductEntity>.EQ(x => x.VersionId, previewVersionId)
                            ), new MongoRepository<GroupProductEntity>());

        //Copy preview product data into publish.
        private void CopyBundleProducts(int versionId, int publishCatalogId, int previewVersionId)
           => PreviewHelper.CopyAssociatedProductsToMongo<BundleProductEntity>(versionId, Query.And(
                            Query<BundleProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                            Query<BundleProductEntity>.EQ(x => x.VersionId, previewVersionId)
                            ), new MongoRepository<BundleProductEntity>());

        //Copy preview associate product data into publish.
        private void CopyConfigurableProducts(int versionId, int publishCatalogId, int previewVersionId)
           => PreviewHelper.CopyAssociatedProductsToMongo<ConfigurableProductEntity>(versionId, Query.And(
                            Query<ConfigurableProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                            Query<ConfigurableProductEntity>.EQ(x => x.VersionId, previewVersionId)
                            ), new MongoRepository<ConfigurableProductEntity>());

        //Copy preview catalog data into publish.
        private void CopyCatalogData(int versionId, int publishCatalogId, int previewVersionId, int localeId = 0)
        {
            PreviewHelper.CopyEntitiesForPreview<CatalogEntity, _LogCatalogEntity>(versionId, Query.And(
                           Query<CatalogEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                           Query<CatalogEntity>.EQ(x => x.VersionId, previewVersionId),
                           Query<CatalogEntity>.EQ(x => x.LocaleId, localeId)
                           ), new MongoRepository<CatalogEntity>());

            PreviewHelper.CopyEntitiesForPreview<CatalogAttributeEntity, _LogCatalogAttributeEntity>(versionId, Query.And(
                           Query<CatalogAttributeEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                           Query<CatalogAttributeEntity>.EQ(x => x.VersionId, previewVersionId),
                           Query<CatalogAttributeEntity>.EQ(x => x.LocaleId, localeId)
                           ), new MongoRepository<CatalogAttributeEntity>());

        }

        //Copy preview catalog data into publish.
        private void CopyProductData(int versionId, int publishCatalogId, int previewVersionId, int localeId = 0)
        => PreviewHelper.CopyEntitiesForPreview<ProductEntity, _LogProductEntity>(versionId, Query.And(
                            Query<ProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                            Query<ProductEntity>.EQ(x => x.VersionId, previewVersionId),
                            Query<ProductEntity>.EQ(x => x.LocaleId, localeId)
                            ), new MongoRepository<ProductEntity>());

        //Copy preview catalog data into publish.
        private void CopyCatagoryData(int versionId, int publishCatalogId, int previewVersionId, int localeId)
        => PreviewHelper.CopyEntitiesForPreview<CategoryEntity, _LogCategoryEntity>(versionId, Query.And(
                            Query<CategoryEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                            Query<CategoryEntity>.EQ(x => x.VersionId, previewVersionId),
                            Query<CategoryEntity>.EQ(x => x.LocaleId, localeId)
                            ), new MongoRepository<CategoryEntity>());

        //Copy preview catalog data into publish.
        private void CopySEOData(int versionId, int publishCatalogId, int previewVersionId, int localeId = 0)
        {
            List<int> portalIds = _portalCatalogRepository.Table.Where(x => x.PublishCatalogId == publishCatalogId)?.Select(x => x.PortalId)?.ToList();
            if (!Equals(portalIds, null))
            {
                ZnodeLogging.LogMessage("Portal Ids: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { portalIds = portalIds });
                foreach (int portalId in portalIds)
                    PreviewHelper.CopyEntitiesForPreview<SeoEntity, _LogSeoEntity>(versionId, Query.And(
                                Query<SeoEntity>.EQ(x => x.VersionId, previewVersionId),
                                Query<SeoEntity>.EQ(x => x.LocaleId, localeId)
                                ), new MongoRepository<SeoEntity>());
            }
            ZnodePublishStatesEnum znodePublishStateEnum = (ZnodePublishStatesEnum)Enum.Parse(typeof(ZnodePublishStatesEnum), ZnodePublishStatesEnum.PRODUCTION.ToString());
            UpdateStatusForSEO(znodePublishStateEnum, publishCatalogId);
        }

        private void UpdateStatusForSEO(ZnodePublishStatesEnum targetContentState, int publishCatalogId)
        {
            List<int> portalIds = _portalCatalogRepository.Table.Where(x => x.PublishCatalogId == publishCatalogId)?.Select(x => x.PortalId)?.ToList();
            ZnodeLogging.LogMessage("portalIds and publishCatalogId to get portalIds: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { portalIds = portalIds, publishCatalogId = publishCatalogId });
            if (!Equals(portalIds, null))
            {
                foreach (int portalId in portalIds)
                {
                    HelperMethods.CurrentContext.ZnodeCMSSEODetails.Where(x => string.IsNullOrEmpty(x.SEOUrl) && x.PortalId == portalId).Update(y => new ZnodeCMSSEODetail() { PublishStateId = (byte)ZnodePublishStatesEnum.DRAFT });
                    HelperMethods.CurrentContext.ZnodeCMSSEODetails.Where(x => !string.IsNullOrEmpty(x.SEOUrl) && x.PortalId == portalId).Update(y => new ZnodeCMSSEODetail() { PublishStateId = (byte)targetContentState });
                }
            }
        }

        //Publish Catalog and delete old version data of catalog
        protected virtual void PreviewCatalog(List<CatalogEntity> catalogEntity, string revisionType, Guid jobId, string catalogPublishProgressMessage)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { catalogEntityListCount = catalogEntity?.Count, revisionType = revisionType });
            int previousVersionId = 0;
            int publishCatalogId = catalogEntity.Any() ? catalogEntity.FirstOrDefault().ZnodeCatalogId : 0;
            try
            {
                //Save Catalog in Mongo
                SaveCatalogInMongo(catalogEntity);
                PreviewHelper.LogProgress(jobId, catalogPublishProgressMessage, 10);

                //save Category in mongo
                SaveCategoryInMongo(catalogEntity);
                PreviewHelper.LogProgress(jobId, catalogPublishProgressMessage, 30);

                //Save Products in mongo
                SaveProductInMongo(catalogEntity, revisionType);
                PreviewHelper.LogProgress(jobId, catalogPublishProgressMessage, 50);

                //Save new Attribute Configurations Related to Catalog in mongo
                SaveAttributeConfigurations(catalogEntity);
                List<int> previousVersionIds = new List<int>();
                foreach (CatalogEntity catalog in catalogEntity)
                {
                    previousVersionId = PreviewHelper.GetVersionId(catalog.ZnodeCatalogId, revisionType, catalog.LocaleId);

                    //Save SEO configuration in mongo
                    SaveSEOInMongo(catalog.ZnodeCatalogId, catalog.LocaleId, catalog.VersionId, previousVersionId, revisionType);

                    // Save previous version Ids
                    previousVersionIds.Add(previousVersionId);
                    //Add new version to version entity
                    CreateMongoVersion(revisionType, catalog.VersionId, publishCatalogId, catalog.LocaleId);

                    //Set catalog publish status to true
                    PreviewHelper.SetPublishStatus(catalog.VersionId, true, revisionType);

                    PreviewHelper.CreatePreviewLog(revisionType, catalog.VersionId, previousVersionId, publishCatalogId, "Catalog", catalog.LocaleId, GetLocaleName(catalog.LocaleId));
                }

                PreviewHelper.LogProgress(jobId, catalogPublishProgressMessage, 60);

                //If catalog publish is true then create index for its all associated stores.
                //TODO
                //Need to omit flag
                if (ZnodeApiSettings.CreateIndexAfterPublish)
                {
                    AssociatedPortalCreateIndex(publishCatalogId, revisionType);
                }

                foreach (CatalogEntity catalog in catalogEntity)
                    //Method to set IsPublishSuccess of VersionEntity to true based on VersionId
                    UpdateIsPublishedFlag(catalog.VersionId, true);

                foreach (int priorVersionId in previousVersionIds)
                        //Delete Previous version catalog
                        DeleteMongoVersion(priorVersionId, publishCatalogId);
                                    
                PreviewHelper.LogProgress(jobId, catalogPublishProgressMessage, 80);
            }
            catch (Exception ex)
            {
                foreach (CatalogEntity catalog in catalogEntity)
                {
                    //Delete Current wip version catalog
                    PreviewHelper.DeletePublishCatalogFromMongo(Query.And(
                     Query<CatalogEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                     Query<CatalogEntity>.EQ(x => x.VersionId, catalog.VersionId)
                     ), catalog.VersionId);

                    //Delete previous version Seo data 
                    PreviewHelper.DeleteRecords(new MongoRepository<SeoEntity>(), new MongoRepository<_LogSeoEntity>(), catalog.VersionId,
                            Query<VersionEntity>.EQ(x => x.VersionId, catalog.VersionId));

                    //Set Current wip catalog publish status to false
                    PreviewHelper.SetPublishStatus(catalog.VersionId, false, ZnodePublishStatesEnum.PUBLISH_FAILED.ToString());
                }

                PreviewHelper.LogProgress(jobId, catalogPublishProgressMessage, 100, false, true, ex);
                ZnodeLogging.LogMessage(ex.StackTrace, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                throw ex;
            }
        }

        //Clear Cache of portal after catalog publish.
        private void ClearCacheAfterPublish(int publishCatalogId)
        {
            if (publishCatalogId == 0) return;

            ZnodeLogging.LogMessage("Input parameter: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { publishCatalogId = publishCatalogId });
            List<ZnodePortalCatalog> associatedPortalId = _portalCatalogRepository.Table.Where(portalCatalog => portalCatalog.PublishCatalogId == publishCatalogId)?.ToList();
            List<int> ids = associatedPortalId.Select(x => x.PortalId).ToList();
            string portalIds = string.Join(",", ids);
            ClearCacheHelper.ClearCacheAfterPublish(portalIds);
        }

        //Save Publish catalog info in to MONGO DB
        private void SaveCatalogInMongo(List<CatalogEntity> catalogEntity)
        {
            try
            {
                ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                foreach (CatalogEntity catalog in catalogEntity)
                    catalog.ProfileIds = !string.IsNullOrEmpty(catalog.TempProfileIds) ? ((catalog.TempProfileIds)?.Split(',').Select<string, int>(int.Parse)?.ToArray()) : null;

                //Create Catalog collection
                _catalogMongoRepository.Create(catalogEntity);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Catalog : "+ catalogEntity.FirstOrDefault().CatalogName + " - Failed to publish Catalog.", string.Empty, TraceLevel.Error);
                throw ex;
            }
        }

        //Save Publish category info in to MONGO DB
        protected virtual void SaveCategoryInMongo(List<CatalogEntity> catalogEntity)
        {
            try
            {
                ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();

                executeSpHelper.GetParameter("@PublishCatalogId", catalogEntity.FirstOrDefault().ZnodeCatalogId, ParameterDirection.Input, SqlDbType.Int);
                executeSpHelper.GetParameter("@UserId", GetLoginUserId(), ParameterDirection.Input, SqlDbType.Int);
                executeSpHelper.GetParameter("@VersionId", catalogEntity.FirstOrDefault().VersionId, ParameterDirection.Input, SqlDbType.Int);

                DataTable localeIdsData = ConvertKeywordListToDataTable(catalogEntity.Select(x => x.LocaleId).ToList());
                if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
                {
                    executeSpHelper.GetParameter("@LocaleId", localeIdsData?.ToJson(), ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetSPResultInDataSet("Znode_GetPublishCategoryWithJSON");
                }
                else
                {
                    executeSpHelper.SetTableValueParameter("@LocaleId", localeIdsData, ParameterDirection.Input, SqlDbType.Structured, "dbo.TransferId");
                    executeSpHelper.GetSPResultInDataSet("Znode_GetPublishCategory");
                }

                foreach (CatalogEntity catalog in catalogEntity)
                {
                    //Get total publish categories count
                    int totalPublishedCategories = _publishedXmlRepository.Table.Where(x => x.PublishCatalogLogId == catalog.VersionId && x.IsCategoryXML == true).Count();
                    int lastRecordId = 0;

                    for (int index = 0; index < totalPublishedCategories; index += 100)
                    {
                        //Get categories in chunks of 100
                        List<ZnodePublishedXml> publishedXmlList = _publishedXmlRepository.Table.Where(x => x.PublishCatalogLogId == catalog.VersionId && x.PublishedXmlId > lastRecordId && x.IsCategoryXML == true).Take(100).ToList();

                        //set last category PublishXmlId
                        lastRecordId = publishedXmlList.Last().PublishedXmlId;

                        //Save categories in Mongo
                        PublishHelper.SaveCategoryInMongo(ConvertListOfXMLStringToListModel<CategoryEntity>(publishedXmlList.Select(x => x.PublishedXML)));
                    }
                }
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Catalog : " + catalogEntity.FirstOrDefault().CatalogName + " - Failed to publish Categories.", string.Empty, TraceLevel.Error);
                throw ex;
            }

        }

        //Associate categories to profile.
        private bool AssociateCategoriesToCatalogForProfile(CatalogAssociationModel catalogAssociationModel)
        {
            IZnodeViewRepository<CatalogAssociationModel> objStoredProc = new ZnodeViewRepository<CatalogAssociationModel>();
            objStoredProc.SetParameter(ZnodeProfileCatalogEnum.ProfileCatalogId.ToString(), catalogAssociationModel.ProfileCatalogId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodeProfileCatalogEnum.PimCatalogId.ToString(), catalogAssociationModel.CatalogId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("UserId", GetLoginUserId(), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("PimCategoryId", catalogAssociationModel.CategoryIds, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("PimCategoryHierarchyId", string.Join(",", catalogAssociationModel.PimCategoryHierarchyId), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
            int status = 0;

            ZnodeLogging.LogMessage("SP parameters: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { ProfileCatalogId = catalogAssociationModel?.ProfileCatalogId, CatalogId = catalogAssociationModel?.CatalogId, CategoryIds = catalogAssociationModel.CategoryIds, PimCategoryHierarchyId = catalogAssociationModel?.PimCategoryHierarchyId });
            objStoredProc.ExecuteStoredProcedureList("Znode_InsertUpdateProfileCatalog null,@PimCatalogId,@UserId, @Status OUT,@PimCategoryId,@ProfileCatalogId,null,@PimCategoryHierarchyId", 5, out status);
            if (status == 1)
            {
                ZnodeLogging.LogMessage(PIM_Resources.SuccessAssociateCategoriesToCatalog, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                return true;
            }
            else
            {
                ZnodeLogging.LogMessage(PIM_Resources.ErrorAssociateCategoriesToCatalog, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                return false;
            }
        }

        //Create Publish product info in to MONGO DB
        protected virtual void SaveProductInMongo(List<CatalogEntity> catalogEntity, string revisionType)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            int userId = GetLoginUserId();
            try
            {
                CatalogEntity entity = catalogEntity?.FirstOrDefault();
                ZnodePublishStatesEnum znodePublishStateEnum = (ZnodePublishStatesEnum)Enum.Parse(typeof(ZnodePublishStatesEnum), entity?.RevisionType);
                CreateJobToPublishProduct(catalogEntity, entity, userId, znodePublishStateEnum);
                WaitForJobToPublishProduct(entity);

                foreach (CatalogEntity catalog in catalogEntity)
                {
                    //Get total publish Products count
                    int totalPublishedProducts = _publishedXmlRepository.Table.Where(x => x.PublishCatalogLogId == catalog.VersionId && x.IsProductXML == true).Count();
                    int lastRecordId = 0;
                    for (int index = 0; index < totalPublishedProducts; index += 200)
                    {
                        List<ZnodePublishedXml> publishedXmlList = SaveCatalogProductsInMongo(catalog, out lastRecordId, lastRecordId);

                        PublishHelper.SaveProductInMongo(publishedXmlList.Select(x => x.PublishedXML), revisionType);
                    }
                }

                if (catalogEntity.Count > 0)
                    //Publish Associated Products like (Addons, Group Products,Bundle Products and Configurable Products) Configuration
                    PreviewHelper.PublishAssociatedProductsConfiguration(catalogEntity.FirstOrDefault().ZnodeCatalogId, "@PublishCatalogId", userId, catalogEntity.FirstOrDefault().VersionId, catalogEntity.Select(x => x.LocaleId).ToList(), znodePublishStateEnum);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // This method is used to created sql job for publish
        protected virtual void CreateJobToPublishProduct(List<CatalogEntity> catalogEntity, CatalogEntity entity, int userId, ZnodePublishStatesEnum znodePublishStateEnum)
        {
            try
            {
                ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
                executeSpHelper.GetParameter("@PublishCatalogId", entity?.ZnodeCatalogId, ParameterDirection.Input, SqlDbType.Int);
                executeSpHelper.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);
                executeSpHelper.GetParameter("@VersionId", entity?.VersionId, ParameterDirection.Input, SqlDbType.Int);
                executeSpHelper.GetParameter("@PublishStateId", (byte)znodePublishStateEnum, ParameterDirection.Input, SqlDbType.Int);
                DataTable localeIds = ConvertKeywordListToDataTable(catalogEntity.Select(x => x.LocaleId).ToList());
                if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
                {
                    executeSpHelper.GetParameter("@LocaleId", localeIds?.ToJson(), ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetSPResultInDataSet("Znode_GetPublishProcessStartWithJSON");
                }
                else
                {
                    executeSpHelper.SetTableValueParameter("@LocaleId", localeIds, ParameterDirection.Input, SqlDbType.Structured, "dbo.TransferId");
                    executeSpHelper.GetSPResultInDataSet("Znode_GetPublishProcessStart");
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Catalog : " + entity.CatalogName + " - Failed to create sql job.", string.Empty, TraceLevel.Error);
                throw new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, "Failed to create job");

            }
        }

        // This method is used to check job is finished publish product  
        protected virtual void WaitForJobToPublishProduct(CatalogEntity entity)
        {
            try
            {
                bool isPublishProductsSaved = true;
                ZnodePublishCatalogLog publishCatalogLog = null;
                int productPublishSleepTime = Convert.ToInt32(ZnodeApiSettings.ProductPublishSleepTime);
                int productPublishMaxTime = Convert.ToInt32(ZnodeApiSettings.ProductPublishMaxTime);
                int counter = 0;
                do
                {
                    //Sleep the thread.
                    Thread.Sleep(productPublishSleepTime);

                    //used to close the thread after publish product maximum time.
                    counter++;

                    int versionId = entity.VersionId;

                    publishCatalogLog = _publishCatalogLogRepository.Table.FirstOrDefault(x => x.PublishCatalogLogId == versionId);

                    //Throw exception will break the publish process and delete the in process catalog publish data.
                    if (counter > productPublishMaxTime)
                        throw new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, "Failed to publish products.");

                    //isPublishProductsSaved set to false when publish products are saved.
                    if (publishCatalogLog?.IsProductPublished == false)
                    {
                        ZnodeLogging.LogMessage("Catalog : " + entity.CatalogName + " - Failed to publish products.", string.Empty, TraceLevel.Error);
                        throw new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, "Catalog : " + entity.CatalogName +  " - Failed to publish products.");
                    }
                    if (publishCatalogLog?.IsProductPublished ?? false)
                        isPublishProductsSaved = false;
                }
                while (isPublishProductsSaved);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        protected virtual List<ZnodePublishedXml> SaveCatalogProductsInMongo(CatalogEntity catalog, out int lastRecordId, int PublishedXmlId)
        {
            //Get products in chunks of 100
            List<ZnodePublishedXml> publishedXmlList = _publishedXmlRepository.Table.Where(x => x.PublishCatalogLogId == catalog.VersionId && x.PublishedXmlId > PublishedXmlId && x.IsProductXML == true).Take(200).ToList();

            lastRecordId = publishedXmlList.Any()?
                 //set last product PublishXmlId
                 publishedXmlList.Last().PublishedXmlId : 0;

            ZnodeLogging.LogMessage("publishedXmlList count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { publishedXmlListCount = publishedXmlList?.Count });
            return publishedXmlList;
        }

        //Save new Attribute Configurations Related to Catalog in mongo
        private void SaveAttributeConfigurations(List<CatalogEntity> catalogEntities)
        {
            if (IsNotNull(catalogEntities))
            {
                ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                IMongoRepository<CatalogAttributeEntity> catalogAttributeEntity = new MongoRepository<CatalogAttributeEntity>();

                //Delete Existing Catalog Attribute Configuration
                    catalogAttributeEntity.DeleteByQuery(Query.And(
                    Query<CatalogAttributeEntity>.EQ(pr => pr.ZnodeCatalogId, catalogEntities.FirstOrDefault().ZnodeCatalogId),
                    Query<VersionEntity>.EQ(x => x.VersionId, catalogEntities.FirstOrDefault().VersionId),
                    Query<VersionEntity>.EQ(x => x.RevisionType, catalogEntities.FirstOrDefault().RevisionType)));

                //Get All Attributes associated with catalog
                IZnodeViewRepository<CatalogAttributeListModel> objStoredProc = new ZnodeViewRepository<CatalogAttributeListModel>();
                objStoredProc.SetParameter("@PublishCatalogId", catalogEntities.FirstOrDefault().ZnodeCatalogId, ParameterDirection.Input, DbType.Int32);
                IList<CatalogAttributeListModel> catalogAttributes = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishProductAttribute @PublishCatalogId");
                
                List<CatalogAttributeEntity> catalogAttributeList = new List<CatalogAttributeEntity>();
                
                catalogAttributes.GroupBy(s => new { s.AttributeCode, s.LocaleId }).Select(x => new { x.Key.AttributeCode, x.Key.LocaleId }).Distinct().ToList().ForEach(
                    x => {
                        CatalogAttributeEntity catalogAttribute = new CatalogAttributeEntity();
                        CatalogAttributeListModel catalogAttributeModel = catalogAttributes.FirstOrDefault(w => w.AttributeCode == x.AttributeCode && w.LocaleId == x.LocaleId);
                        
                        catalogAttribute.ZnodeCatalogId = catalogAttributeModel.ZnodeCatalogId;
                        catalogAttribute.AttributeCode = catalogAttributeModel.AttributeCode;
                        catalogAttribute.AttributeTypeName = catalogAttributeModel.AttributeTypeName;
                        catalogAttribute.IsPromoRuleCondition = catalogAttributeModel.IsPromoRuleCondition;
                        catalogAttribute.IsComparable = catalogAttributeModel.IsComparable;
                        catalogAttribute.IsHtmlTags = catalogAttributeModel.IsHtmlTags;
                        catalogAttribute.IsFacets = catalogAttributeModel.IsFacets;
                        catalogAttribute.IsUseInSearch = catalogAttributeModel.IsUseInSearch;
                        catalogAttribute.IsPersonalizable = catalogAttributeModel.IsPersonalizable;
                        catalogAttribute.IsConfigurable = catalogAttributeModel.IsConfigurable;
                        catalogAttribute.AttributeName = catalogAttributeModel.AttributeName;
                        catalogAttribute.LocaleId = catalogAttributeModel.LocaleId;
                        catalogAttribute.DisplayOrder = catalogAttributeModel.DisplayOrder;
                        catalogAttribute.SelectValues = catalogAttributes.Where(w => w.AttributeCode == x.AttributeCode && w.LocaleId == x.LocaleId).Select(s => new SelectAttributeValuesEntity  {value= s.AttributeDefaultValue,displayorder= s.DefaultValueDisplayOrder }).ToList();
                        catalogAttributeList.Add(catalogAttribute);
                    });


                //Save Attributes Associated to catalog in mongo
                foreach (CatalogEntity catalog in catalogEntities)
                catalogAttributeList.Where(x => x.LocaleId == catalog.LocaleId).ToList().ForEach(x => x.VersionId = catalog.VersionId);

                if (catalogAttributes.Count > 0)
                    catalogAttributeEntity.Create(catalogAttributeList);

            }
        }

        //Publish SEO configuration
        private void SaveSEOInMongo(int publishCatalogId, int localeId, int currentlyPreviewedVersionId, int previousPreviewVersionId, string revisionType)
        {
            IWebSiteService webSiteService = GetService<IWebSiteService>();

            SEODetailsEnum[] seoTypes = new SEODetailsEnum[] { SEODetailsEnum.Product, SEODetailsEnum.Category, SEODetailsEnum.Brand };

            ZnodePublishStatesEnum znodePublishStateEnum = (ZnodePublishStatesEnum)Enum.Parse(typeof(ZnodePublishStatesEnum), revisionType);

            List<int> portalIds = _portalCatalogRepository.Table.Where(x => x.PublishCatalogId == publishCatalogId)?.Select(x => x.PortalId)?.ToList();
            if(!Equals(portalIds, null))
            {
                foreach (int portalId in portalIds)
                {
                    webSiteService.SaveSeoToMongoCatalog(portalId, localeId, currentlyPreviewedVersionId, previousPreviewVersionId, znodePublishStateEnum, seoTypes);
                    UpdateStatusForSEO(znodePublishStateEnum, publishCatalogId);
                }
            }
        }

        //Get Publish Timeout in Seconds
        private double PublishTimeoutSeconds()
                => TimeSpan.FromMilliseconds(Convert.ToInt32(ZnodeApiSettings.ProductPublishSleepTime) * Convert.ToInt32(ZnodeApiSettings.ProductPublishMaxTime)).TotalSeconds;

        //Publish Catalog and delete old version data of catalog
        private void PublishCatalogForPreview(List<CatalogEntity> catalogEntity, Guid jobId, string catalogPublishProgressMessage)
        {
            int publishCatalogId = 0;
            List<int> previousVersionIds = new List<int>();
            try
            {
                foreach (CatalogEntity catalog in catalogEntity)
                {
                    int versionId = catalog.VersionId;

                    publishCatalogId = catalog.ZnodeCatalogId;

                    int previewVersionId = PreviewHelper.GetVersionId(publishCatalogId, ZnodePublishStatesEnum.PREVIEW.ToString(), catalog.LocaleId);

                    ZnodeLogging.LogMessage("Input parameters for copy operation: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { versionId = versionId, publishCatalogId = publishCatalogId, previewVersionId = previewVersionId, LocaleId = catalog?.LocaleId });

                    //Copy preview version data to production version data.
                    CopyPreviewVersionDataToProductionVersion(publishCatalogId, catalog, versionId, previewVersionId);

                    // Save previous version Ids
                    previousVersionIds.Add(PreviewHelper.GetVersionId(publishCatalogId, ZnodePublishStatesEnum.PRODUCTION.ToString(),catalog.LocaleId));
                    // Manage Mongo version create new.
                    CreateMongoVersion(ZnodePublishStatesEnum.PRODUCTION.ToString(), versionId, publishCatalogId, catalog.LocaleId);

                    //Set catalog publish status to true
                    PreviewHelper.SetPublishStatus(versionId, true, ZnodePublishStatesEnum.PRODUCTION.ToString());

                    PreviewHelper.CreatePreviewLog(ZnodePublishStatesEnum.PRODUCTION.ToString(), versionId, previewVersionId, publishCatalogId, "Catalog", catalog.LocaleId, GetLocaleName(catalog.LocaleId));

                }

                //If catalog publish is true then create index for its all associated stores.
                //TODO
                //Need to omit 
                if (ZnodeApiSettings.CreateIndexAfterPublish)
                {
                    AssociatedPortalCreateIndex(publishCatalogId, ZnodePublishStatesEnum.PRODUCTION.ToString());
                }

                foreach (CatalogEntity catalog in catalogEntity)
                    //Method to set IsPublishSuccess of VersionEntity to true based on VersionId
                    UpdateIsPublishedFlag(catalog.VersionId, true);

                //Delete previous version
                foreach (int priorVersionId in previousVersionIds)
                        DeleteMongoVersion(priorVersionId, publishCatalogId);

                PreviewHelper.LogProgress(jobId, catalogPublishProgressMessage, 90);
            }
            catch (Exception ex)
            {
                foreach (CatalogEntity catalog in catalogEntity)
                {
                    //Delete Current wip version catalog
                    PreviewHelper.DeletePublishCatalogFromMongo(Query.And(
                     Query<CatalogEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
                     Query<CatalogEntity>.EQ(x => x.VersionId, catalog.VersionId)
                     ), catalog.VersionId);

                    //Delete previous version Seo data 
                    PreviewHelper.DeleteRecords(new MongoRepository<SeoEntity>(), new MongoRepository<_LogSeoEntity>(), catalog.VersionId,
                    Query<VersionEntity>.EQ(x => x.VersionId, catalog.VersionId));

                    //Set Current wip catalog publish status to false
                    PreviewHelper.SetPublishStatus(catalog.VersionId, false, ZnodePublishStatesEnum.PUBLISH_FAILED.ToString());
                }
                PreviewHelper.LogProgress(jobId, catalogPublishProgressMessage, 100, false, true, ex);
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Portal.ToString(), TraceLevel.Error);
                throw ex;
            }
        }

        //Copy preview version data to production version data.
        protected virtual void CopyPreviewVersionDataToProductionVersion(int publishCatalogId, CatalogEntity catalog, int versionId, int previewVersionId)
        {
            Parallel.Invoke(new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },

                   //Create new copy of preview data.
                   () => {  CopyCatalogData(versionId, publishCatalogId, previewVersionId, catalog.LocaleId);  },

                   //Create new copy of preview data. 
                   () => {  CopyCatagoryData(versionId, publishCatalogId, previewVersionId, catalog.LocaleId);},

                   //Create new copy of preview data for product.
                   () => {  CopyProductData(versionId, publishCatalogId, previewVersionId, catalog.LocaleId);   },

                   //Create new copy of preview data for SEO.
                   () => {  CopySEOData(versionId, publishCatalogId, previewVersionId, catalog.LocaleId);  },

                   //Publish Addons
                   () => {  CopyAddons(versionId, publishCatalogId, previewVersionId, catalog.LocaleId);   },

                   //Create new copy of preview data for associated product.
                   () => {  CopyAssociatedProductsConfiguration(versionId, publishCatalogId, previewVersionId);  });
        }

        private bool IsCategorySame(int pimHierarchyId, int pimParentHierarchyId)
        {
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { pimHierarchyId = pimHierarchyId, pimParentHierarchyId = pimParentHierarchyId });
            return (from categoryHierarchy in _pimCategoryHierarchyRepository.Table
                    join parentCategoryHierarchy in _pimCategoryHierarchyRepository.Table on categoryHierarchy.PimCategoryId equals parentCategoryHierarchy.PimCategoryId
                    where categoryHierarchy.PimCategoryHierarchyId == pimHierarchyId && parentCategoryHierarchy.PimCategoryHierarchyId == pimParentHierarchyId
                    select categoryHierarchy)?.Count() > 0;
        }

        //Converts Searchable Attributes List to Data Table
        private static DataTable ConvertKeywordListToDataTable(List<int> localeIds)
        {
            ZnodeLogging.LogMessage("LocaleIds: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, localeIds);
            DataTable table = new DataTable("TransferId");
            table.Columns.Add("Id");

            foreach (int model in localeIds)
                table.Rows.Add(model);

            return table;
        }

        private PublishedModel BindErrorMessage(DataSet resultDataSet)
            => new PublishedModel { IsPublished = false, ErrorMessage = resultDataSet.Tables[0].Rows[0]["MessageDetails"].ToString() };

        private List<string> RevisionTypeListForProduction()
            => new List<string>
                {
                    ZnodePublishStatesEnum.PREVIEW.ToString(),
                    ZnodePublishStatesEnum.PRODUCTION.ToString()
                };
        #endregion Private Methods
    }
}