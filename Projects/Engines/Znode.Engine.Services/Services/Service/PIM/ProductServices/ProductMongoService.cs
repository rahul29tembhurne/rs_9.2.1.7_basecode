﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Engine.Services
{
    public class ProductMongoService: IProductMongo
    {
        private readonly IMongoRepository<ProductEntity> _productMongoRepository;

        public ProductMongoService()
        {
            _productMongoRepository = new MongoRepository<ProductEntity>();
        }

        /// <summary>
        ///  Method Gets all publish products satisfying the query
        /// </summary>
        /// <param name="query">Query to get products</param>
        /// <returns>ProductEntity List</returns>
        public virtual List<ProductEntity> GetPublishProducts(IMongoQuery query)
        => _productMongoRepository.Table.MongoCollection.Find(query).ToList();

        /// <summary>
        ///  Method Gets Publish Product by publish Category Id and locale Id 
        /// </summary>
        /// <param name="categoryId">Publish Category Id to get Publish Product</param>
        /// <param name="localeId">Locale Id to get Publish Product</param>
        /// <returns></returns>
        public virtual List<ProductEntity> GetProductsByCategoryId(int categoryId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters categoryId and localeId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { categoryId, localeId });

            var query = Query.And(
                 Query<CategoryEntity>.EQ(pr => pr.LocaleId, localeId),
               Query<ProductEntity>.Where(pr => pr.ZnodeCategoryIds.Equals(categoryId))
           );
            ZnodeLogging.LogMessage("Mongo query get publish products by publish category Id and localeId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, query);
            return _productMongoRepository.Table.MongoCollection.Find(query)?.ToList();
        }

        /// <summary>
        ///  Method Gets Publish Product Details
        /// </summary>
        /// <param name="productId">Product Id to get Product</param>
        /// <param name="localeId">Locale Id to get locale specific Product</param>
        /// <returns>ProductEntity</returns>
        public virtual ProductEntity GetProduct(int productId, int localeId, int? catalogVersionId = null)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters productId, localeId and catalogVersionId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { productId, localeId, catalogVersionId });

            var query = Query.And(
               Query<ProductEntity>.EQ(pr => pr.ZnodeProductId, productId),
               Query<ProductEntity>.EQ(pr => pr.LocaleId, localeId)
           );

            if (catalogVersionId.HasValue && catalogVersionId.Value > 0)
                query = Query.And(query, Query<ProductEntity>.EQ(pr => pr.VersionId, catalogVersionId));

            ZnodeLogging.LogMessage("Mongo query get publish product details: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, query);
            return _productMongoRepository.Table.MongoCollection.FindOne(query);
        }

        /// <summary>
        /// Get Publish Product Name by ProductId and Locale Id
        /// </summary>
        /// <param name="productId">Product Id to get Product</param>
        /// <param name="localeId">Locale Id to get locale specific Product</param>
        /// <returns>Publish product Name</returns>
        public virtual string GetProductName(int productId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters productId and localeId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { productId, localeId });

            var query = Query.And(
               Query<ProductEntity>.EQ(pr => pr.ZnodeProductId, productId),
               Query<CategoryEntity>.EQ(pr => pr.LocaleId, localeId)
           );
            ZnodeLogging.LogMessage("Mongo query get publish product name: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, query);
            return _productMongoRepository.Table.MongoCollection.FindOne(query)?.Name;
        }
    }
}
