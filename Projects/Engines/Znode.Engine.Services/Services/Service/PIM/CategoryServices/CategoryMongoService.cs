﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Engine.Services
{
    public class CategoryMongoService: ICategoryMongo
    {

        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;

        public CategoryMongoService()
        {
            _categoryMongoRepository = new MongoRepository<CategoryEntity>();
        }

        /// <summary>
        /// Get Publish Category Details
        /// </summary>
        /// <param name="categoryId">Category Id to get category</param>
        /// <param name="localeId">Locale Id to get Locale specific category</param>
        /// <returns>CategoryEntity</returns>
        public virtual CategoryEntity GetCategory(int categoryId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters categoryId and localeId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { categoryId, localeId });

            var query = Query.And(
                                 Query<CategoryEntity>.EQ(pr => pr.ZnodeCategoryId, categoryId),
                                 Query<CategoryEntity>.EQ(pr => pr.LocaleId, localeId));
            ZnodeLogging.LogMessage("Mongo query get publish category details: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, query);
            return _categoryMongoRepository.Table.MongoCollection.FindOne(query);
        }

        /// <summary>
        ///  Method Gets Publish Parent Category by publish Catalog Id and localeId
        /// </summary>
        /// <param name="catalogId">Publish Catalog Id to get Publish Category</param>
        /// <param name="localeId">Locale Id to get Publish Category</param>
        /// <returns>CategoryEntity List</returns>
        public virtual List<CategoryEntity> GetCategoryByCatalogId(int catalogId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters catalogId and localeId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { catalogId, localeId });

            var query = Query.And(
                 Query<CategoryEntity>.EQ(pr => pr.ZnodeCatalogId, catalogId),
                                 Query<CategoryEntity>.EQ(pr => pr.ZnodeParentCategoryIds, null),
                                 Query<CategoryEntity>.EQ(pr => pr.LocaleId, localeId));
            ZnodeLogging.LogMessage("Mongo query get publish parent category by publish catalog Id and localeId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, query);
            return _categoryMongoRepository.Table.MongoCollection.Find(query).ToList();
        }

        /// <summary>
        ///  Method Gets all publish categories satisfying the query
        /// </summary>
        /// <param name="query">Query to get categories</param>
        /// <returns>CategoryEntity List</returns>
        public virtual List<CategoryEntity> GetPublishCategories(IMongoQuery query)
        => _categoryMongoRepository.Table.MongoCollection.Find(query).ToList();

        /// <summary>
        ///  Method Gets Publish Category by publish Catalog Id, Publish Category Id and locale Id
        /// </summary>
        /// <param name="catalogId">Publish Catalog Id to get Publish Category</param>
        /// <param name="categoryId">Publish Category Id to get Publish Category</param>
        /// <param name="localeId">Locale Id to get Publish Category</param>
        /// <returns>CategoryEntity List</returns>
        public virtual List<CategoryEntity> GetCategory(int catalogId, int categoryId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters catalogId, categoryId and localeId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { catalogId, categoryId, localeId });

            var query = Query.And(
                                 Query<CategoryEntity>.EQ(pr => pr.ZnodeCatalogId, catalogId),
                                 Query<CategoryEntity>.NE(pr => pr.ZnodeParentCategoryIds, null),
                                 Query<CategoryEntity>.Where(pr => pr.ZnodeParentCategoryIds.Contains(categoryId)),
                                 Query<CategoryEntity>.EQ(pr => pr.LocaleId, localeId));
            ZnodeLogging.LogMessage("Mongo query get publish category by publish catalog Id,publish category Id and localeId: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, query);
            return _categoryMongoRepository.Table.MongoCollection.Find(query)?.ToList();
        }
    }
}

