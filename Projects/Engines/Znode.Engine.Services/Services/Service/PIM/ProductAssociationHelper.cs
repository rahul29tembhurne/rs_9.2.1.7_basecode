﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.Engine.Services
{
    public class ProductAssociationHelper
    {
        private readonly IZnodeRepository<ZnodePimCategoryHierarchy> _pimCategoryHierarchyRepository;
        private readonly IZnodeRepository<ZnodePimCategoryProduct> _pimCategoryProductRepository;
        private readonly IZnodeRepository<ZnodePimCatalogCategory> _pimCatalogCategoryRepository;
        public ProductAssociationHelper()
        {
            _pimCategoryHierarchyRepository = new ZnodeRepository<ZnodePimCategoryHierarchy>();
            _pimCategoryProductRepository = new ZnodeRepository<ZnodePimCategoryProduct>();
            _pimCatalogCategoryRepository = new ZnodeRepository<ZnodePimCatalogCategory>();
        }

        public virtual bool UnAssociateProductFromCatalog(CatalogAssociationModel catalogAssociationModel)
        {
            if (Equals(catalogAssociationModel, null))
                throw new ZnodeException(ErrorCodes.NullModel, PIM_Resources.ErrorModelNull);
            List<int> categoryHierarchyIds = GetCategoryHierarchyIds(catalogAssociationModel.CategoryId, catalogAssociationModel.CatalogId);
            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = null;
            IList<View_ReturnBoolean> deleteResult = null;

            foreach (int categoryHierarchyId in categoryHierarchyIds)
            {
                objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
                objStoredProc.SetParameter(ZnodeProfileCatalogEnum.ProfileCatalogId.ToString(), catalogAssociationModel.ProfileCatalogId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("PimCatalogId", catalogAssociationModel.CatalogId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("PimCategoryId", catalogAssociationModel.CategoryId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("PimProductId", catalogAssociationModel.ProductIds, ParameterDirection.Input, DbType.String);
                objStoredProc.SetParameter(ZnodePimCategoryHierarchyEnum.PimCategoryHierarchyId.ToString(), categoryHierarchyId, ParameterDirection.Input, DbType.String);
                objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Boolean);
                int status = 0;
                deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_DeletePimCatalogProducts @ProfileCatalogId, @PimCatalogId, @PimCategoryId, @PimProductId, @PimCategoryHierarchyId, @Status OUT", 4, out status);

                if (!deleteResult.FirstOrDefault().Status.Value)
                {
                    ZnodeLogging.LogMessage(string.Format(PIM_Resources.ErrorProductRemove, categoryHierarchyId), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
                }
            }

            ZnodeLogging.LogMessage(PIM_Resources.SuccessProductRemove, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return true;
        }

        //Associate Product to Category
        public virtual bool AssociateProductToCategory(List<CategoryProductModel> categoryProductModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if (IsNull(categoryProductModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorModelNull);

            if (categoryProductModel.Count < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.ErrorCategoryProductModelCount);

            int categoryId = Convert.ToInt32(categoryProductModel[0].PimCategoryId.ToString());
            ZnodeLogging.LogMessage("categoryId to get associatedProductList: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, categoryId);

            var associatedProductList = _pimCategoryProductRepository.Table?.Where(x => x.PimCategoryId == categoryId).Select(y => y.PimProductId);

            categoryProductModel = associatedProductList.Any() ? categoryProductModel?.Where(i => !associatedProductList.Contains(i.PimProductId)).ToList() : categoryProductModel;

            if (categoryProductModel.Count == 0)
                return true;

            IEnumerable<ZnodePimCategoryProduct> associateCategoryProducts = _pimCategoryProductRepository.Insert(categoryProductModel.ToEntity<ZnodePimCategoryProduct>().ToList());
            AssociateProductCatalogCategory(categoryProductModel);
            ZnodeLogging.LogMessage("associateCategoryProducts list count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, associateCategoryProducts?.Count());
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return associateCategoryProducts?.Count() > 0;
        }

        //Associate Product to Catalog's Categories.
        public virtual void AssociateProductCatalogCategory(List<CategoryProductModel> categoryProductModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            List<ZnodePimCatalogCategory> catalogCategoryProducts = new List<ZnodePimCatalogCategory>();
            IEnumerable<ZnodePimCatalogCategory> pimCatalogCategoryList = null;

            int? id = categoryProductModel[0].PimCategoryId;
            var categoryHierarchyIds = _pimCategoryHierarchyRepository.Table
                                            .Where(m => m.PimCategoryId == id)
                                            .ToList();
            List<int?> pimCatalogIds = _pimCategoryHierarchyRepository.Table
                                            .Where(m => m.PimCategoryId == id)
                                            .Select(a => a.PimCatalogId).Distinct().ToList();
            List<int> hierarchyIds = new List<int>();
            ZnodeLogging.LogMessage("PimCategoryId to get _pimCatalogCategoriesList: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, id);
            List<ZnodePimCatalogCategory> _pimCatalogCategoriesList = _pimCatalogCategoryRepository.Table.Where(m => m.PimCategoryId == id).ToList();
            ZnodeLogging.LogMessage("pimCatalogIds and pimCatalogCategoriesList count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { pimCatalogIdsCount = pimCatalogIds?.Count, pimCatalogCategoriesListCount = _pimCatalogCategoriesList?.Count });
            foreach (int? catalogId in pimCatalogIds)
            {
                catalogCategoryProducts = new List<ZnodePimCatalogCategory>();
                hierarchyIds = categoryHierarchyIds.Where(m => m.PimCatalogId == catalogId).Select(m => m.PimCategoryHierarchyId).ToList();
                categoryProductModel.ForEach(m =>
                {
                    catalogCategoryProducts.Add(new ZnodePimCatalogCategory { PimCatalogId = catalogId, PimCategoryId = m.PimCategoryId, PimProductId = m.PimProductId, DisplayOrder = Admin_Resources.DefaultDisplayOrder.ToInteger() });
                });

                foreach (int categoryHierarchyId in hierarchyIds)
                {
                    catalogCategoryProducts.ForEach(m =>
                    {
                        m.PimCategoryHierarchyId = categoryHierarchyId;
                    });

                    if (!_pimCatalogCategoriesList.Any(m => m.PimCategoryHierarchyId == categoryHierarchyId
                                                    && catalogCategoryProducts.Any(n => n.PimProductId == m.PimProductId && m.PimCatalogId == catalogId)))
                        pimCatalogCategoryList = _pimCatalogCategoryRepository.Insert(catalogCategoryProducts);
                    if (pimCatalogCategoryList?.FirstOrDefault()?.PimCatalogCategoryId <= 0)
                        ZnodeLogging.LogMessage(string.Format(PIM_Resources.ErrorAssociateProductToCatalog, catalogId, categoryHierarchyId), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
        }

        public virtual List<int> GetCategoryHierarchyIds(int? categoryId, int catalogId)
        {
            return _pimCategoryHierarchyRepository.Table
                                            ?.Where(m => m.PimCategoryId == categoryId
                                             && m.PimCatalogId == catalogId
                                             && m.PimCategoryHierarchyId != 0)
                                            ?.Select(a => a.PimCategoryHierarchyId)?.ToList();
        }

        public void UpdateDisplayOrderOfCategory(int? categoryId, int? productId, int displayOrder)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("categoryId, productId and displayOrder: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { categoryId, productId, displayOrder });
            ZnodePimCategoryProduct znodePimCategoryProduct = _pimCategoryProductRepository.Table
                                                            .FirstOrDefault(m => m.PimProductId == productId
                                                             && m.PimCategoryId == categoryId);
            bool isUpdated = false;
            if (znodePimCategoryProduct != null)
            {
                znodePimCategoryProduct.DisplayOrder = displayOrder;
                isUpdated = _pimCategoryProductRepository.Update(znodePimCategoryProduct);
                ZnodeLogging.LogMessage(isUpdated ? string.Format(PIM_Resources.SuccessAssociatedProductUpdate, productId) : PIM_Resources.ErrorUpdateAssociateProductDetails, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
        }

        public virtual bool UnassociateProductFromCategory(CatalogAssociationModel catalogAssociationModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            List<ZnodePimCategoryProduct> pimCategoryProductList = _pimCategoryProductRepository.Table.Where(m => m.PimCategoryId == catalogAssociationModel.CategoryId).ToList();
            string[] productIds = !string.IsNullOrEmpty(catalogAssociationModel.ProductIds) ? catalogAssociationModel.ProductIds.Split(',') : null;
            string pimCategoryProductIds = string.Empty;
            if (productIds.Count() > 0)
            {
                foreach (string productId in productIds)
                {
                    pimCategoryProductIds = string.Concat(pimCategoryProductIds, pimCategoryProductList.Where(m => m.PimProductId == Convert.ToInt32(productId))
                                            .Select(m => m.PimCategoryProductId).FirstOrDefault().ToString(), ",");
                }
                return UnassociateProductFromCategory(new ParameterModel() { Ids = pimCategoryProductIds.TrimEnd(',') });
            }
            return false;
        }

        /// <summary>
        /// Unassociate Product from Category
        /// </summary>
        /// <param name="PimCategoryProductId"></param>
        /// <returns></returns>
        public virtual bool UnassociateProductFromCategory(ParameterModel PimCategoryProductId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            if (PimCategoryProductId.Ids.Count() < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, PIM_Resources.ErrorPimCategoryProductIdLessThanOne);

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodePimCategoryProductEnum.PimCategoryProductId.ToString(), ProcedureFilterOperators.In, PimCategoryProductId.Ids.ToString()));

            ZnodeLogging.LogMessage("PimCategoryProductIds to be unassociated: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, PimCategoryProductId?.Ids?.ToString());
            return _pimCategoryProductRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause);
        }

        /// <summary>
        /// Delete Product from all associated catalog
        /// </summary>
        /// <param name="pimCategoryProductId"></param>
        public virtual void DeleteProductfromAllCatalog(ParameterModel pimCategoryProductId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            string[] productIds = !string.IsNullOrEmpty(pimCategoryProductId.Ids) ? pimCategoryProductId.Ids.Split(',') : null;
            List<ZnodePimCategoryProduct> pimCategoryProductList = _pimCategoryProductRepository.Table.ToList();
            List<ZnodePimCategoryProduct> pimProductIds = new List<ZnodePimCategoryProduct>();
            foreach (string productId in productIds)
            {
                pimProductIds.Add(pimCategoryProductList
                             .Where(m => m.PimCategoryProductId == Convert.ToInt32(productId))
                             .Select(m => new ZnodePimCategoryProduct()
                             {
                                 PimProductId = m.PimProductId,
                                 PimCategoryId = m.PimCategoryId
                             }).FirstOrDefault());
            }
            ZnodeLogging.LogMessage("Lists count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { pimCategoryProductListCount = pimCategoryProductList?.Count, pimProductIdsCount = pimProductIds?.Count });
            if (pimProductIds?.Count > 0)
            {
                string pimProductId = string.Join(",", pimProductIds.Select(m => m.PimProductId.ToString()).ToArray());
                int categoryId = pimProductIds[0].PimCategoryId;
                List<int?> pimCatalogIds = _pimCategoryHierarchyRepository.Table
                                         .Where(m => m.PimCategoryId == categoryId)
                                         .Select(a => a.PimCatalogId).Distinct().ToList();

                CatalogAssociationModel catalogAssociationModel = new CatalogAssociationModel()
                {
                    CategoryId = categoryId,
                    ProductIds = pimProductId
                };

                pimCatalogIds.ForEach(m =>
                {
                    catalogAssociationModel.CatalogId = m.GetValueOrDefault();
                    if (catalogAssociationModel.CatalogId > 0)
                        UnAssociateProductFromCatalog(catalogAssociationModel);
                });
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
        }

        /// <summary>
        /// Unassociate product from all catalog when delete from any catalog
        /// </summary>
        /// <param name="catalogAssociationModel"></param>
        public virtual void UnassociateCatalogProductfromAllCatalog(CatalogAssociationModel catalogAssociationModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            int? pimCategoryId = catalogAssociationModel.CategoryId;
            string pimProductId = catalogAssociationModel.ProductIds.ToString();
            List<int?> pimCatalogIds = _pimCategoryHierarchyRepository.Table
                                        .Where(m => m.PimCategoryId == pimCategoryId)
                                        .Select(a => a.PimCatalogId).Distinct().ToList();

            ZnodeLogging.LogMessage("pimCatalogIds list: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pimCatalogIds);
            CatalogAssociationModel catalogAssociationNewModel = new CatalogAssociationModel()
            {
                CategoryId = pimCategoryId,
                ProductIds = pimProductId
            };

            pimCatalogIds.ForEach(m =>
            {
                catalogAssociationModel.CatalogId = m.GetValueOrDefault();
                if (catalogAssociationModel.CatalogId > 0)
                    UnAssociateProductFromCatalog(catalogAssociationModel);
            });
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
        }
        public virtual bool IsCategoryMerchandise { get => Convert.ToBoolean(ZnodeApiSettings.IsCategoryMerchandise); }

        public virtual bool AssociateProductToCatalog(int catalogId, int categoryId, int pimCategoryHierarchyId, string[] productIds)
        {
            if (IsCategoryMerchandise)
                return AssociateProductToCatalog(categoryId, productIds);
            else
                 return AssociateProductOnCategoryMerchandise(catalogId, categoryId, productIds);
        }

        public virtual bool AssociateProductToCatalog(int categoryId, string[] productIds)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            List<ZnodePimCatalogCategory> catalogCategoryProducts = new List<ZnodePimCatalogCategory>();
            List<ZnodePimCategoryProduct> categoryProductModelList = new List<ZnodePimCategoryProduct>();
            IEnumerable<ZnodePimCatalogCategory> associatedProducts = null;
            List<int> _pimProductIdList = _pimCategoryProductRepository.Table?.Where(m => m.PimCategoryId == categoryId)?.Select(m => m.PimProductId)?.ToList();
            List<ZnodePimCatalogCategory> _pimCatalogCategoriesList = _pimCatalogCategoryRepository.Table?.Where(m => m.PimCategoryId == categoryId)?.ToList();
            
            if (productIds?.Count() > 0)
            {
                List<int?> pimCatalogIds = _pimCategoryHierarchyRepository.Table
                                      ?.Where(m => m.PimCategoryId == categoryId)
                                      ?.Select(a => a.PimCatalogId)?.Distinct()?.ToList();

                ZnodeLogging.LogMessage("pimCatalogIds list: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, pimCatalogIds);
                pimCatalogIds.ForEach(c =>
                {
                    List<int> categoryHierarchyIds = GetCategoryHierarchyIds(categoryId, c.GetValueOrDefault());
                    //Add items to catalog category list with default display order of 999.
                    categoryHierarchyIds.ForEach(m =>
                    {
                        catalogCategoryProducts = new List<ZnodePimCatalogCategory>();
                        foreach (string Id in productIds)
                        {
                            int productId = Convert.ToInt32(Id);
                            if (!_pimCatalogCategoriesList.Any(n => n.PimProductId == productId && n.PimCategoryHierarchyId == m && n.PimCatalogId == c.GetValueOrDefault()))
                            {
                                catalogCategoryProducts.Add(new ZnodePimCatalogCategory { PimCatalogId = c.GetValueOrDefault(), PimCategoryId = categoryId, PimCategoryHierarchyId = m, PimProductId = productId, DisplayOrder =ZnodeConstant.DisplayOrder });
                            }
                            categoryProductModelList.Add(new ZnodePimCategoryProduct { PimCategoryId = categoryId, PimProductId = productId, Status = true, DisplayOrder = Admin_Resources.DefaultDisplayOrder.ToInteger() });
                        }

                        associatedProducts = _pimCatalogCategoryRepository.Insert(catalogCategoryProducts);
                        if (associatedProducts?.FirstOrDefault()?.PimCatalogCategoryId <= 0)
                            ZnodeLogging.LogMessage(string.Format(PIM_Resources.ErrorAssociateProductCatalogCategoryHierarchy, m), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    });
                });

                //Associate at category level
                categoryProductModelList = categoryProductModelList
                                            .GroupBy(m => m.PimProductId)
                                            .Select(n => n.First())
                                            .ToList();

                ZnodeLogging.LogMessage("Lists count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { PimProductIdList = _pimProductIdList?.Count, PimCatalogCategoriesListCount = _pimCatalogCategoriesList?.Count, CategoryProductModelListCount = categoryProductModelList?.Count });
                foreach (ZnodePimCategoryProduct categoryProductModel in categoryProductModelList)
                {
                    if (!_pimProductIdList.Any(n => n == categoryProductModel.PimProductId))
                        _pimCategoryProductRepository.Insert(categoryProductModel);
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return associatedProducts?.FirstOrDefault()?.PimCatalogCategoryId > 0;
        }

        protected virtual bool AssociateProductOnCategoryMerchandise(int catalogId, int categoryId, string[] productIds)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            List<ZnodePimCatalogCategory> catalogCategoryProducts = new List<ZnodePimCatalogCategory>();
            List<ZnodePimCategoryProduct> categoryProductModelList = new List<ZnodePimCategoryProduct>();
            List<int> categoryHierarchyIds = GetCategoryHierarchyIds(categoryId, catalogId);
            IEnumerable<ZnodePimCatalogCategory> associatedProducts = null;
            List<int> _pimProductIdList = _pimCategoryProductRepository.Table.Where(m => m.PimCategoryId == categoryId).Select(m => m.PimProductId).ToList();
            List<ZnodePimCatalogCategory> _pimCatalogCategoriesList = _pimCatalogCategoryRepository.Table.Where(m => m.PimCategoryId == categoryId).ToList();
            ZnodeLogging.LogMessage("Lists count: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new { categoryHierarchyIdsCount = categoryHierarchyIds?.Count, _pimProductIdListCount = _pimProductIdList?.Count, PimCatalogCategoriesListCount = _pimCatalogCategoriesList?.Count });
            if (productIds?.Count() > 0)
            {
                //Add items to catalog category list with default display order of 999.
                categoryHierarchyIds.ForEach(m =>
                {
                    catalogCategoryProducts = new List<ZnodePimCatalogCategory>();
                    foreach (string Id in productIds)
                    {
                        int productId = Convert.ToInt32(Id);
                        if (!_pimCatalogCategoriesList.Any(n => n.PimProductId == productId && n.PimCategoryHierarchyId == m && n.PimCatalogId == catalogId))
                        {
                            catalogCategoryProducts.Add(new ZnodePimCatalogCategory { PimCatalogId = catalogId, PimCategoryId = categoryId, PimCategoryHierarchyId = m, PimProductId = productId, DisplayOrder = ZnodeConstant.DisplayOrder });
                        }
                        categoryProductModelList.Add(new ZnodePimCategoryProduct { PimCategoryId = categoryId, PimProductId = productId, Status = true, DisplayOrder = ZnodeConstant.DisplayOrder });
                    }

                    associatedProducts = _pimCatalogCategoryRepository.Insert(catalogCategoryProducts);
                    if (associatedProducts?.FirstOrDefault()?.PimCatalogCategoryId <= 0)
                        ZnodeLogging.LogMessage(string.Format(PIM_Resources.ErrorAssociateProductCatalogCategoryHierarchy, m), string.Empty, TraceLevel.Error);
                });
                //Associate at category level

                categoryProductModelList = categoryProductModelList
                                            .GroupBy(m => m.PimProductId)
                                            .Select(n => n.First())
                                            .ToList();
                foreach (ZnodePimCategoryProduct categoryProductModel in categoryProductModelList)
                {
                    if (!_pimProductIdList.Any(n => n == categoryProductModel.PimProductId))
                        _pimCategoryProductRepository.Insert(categoryProductModel);
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return associatedProducts?.FirstOrDefault()?.PimCatalogCategoryId > 0;
        }

        public virtual bool AssociatePimProductToCategory(List<CategoryProductModel> categoryProductModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            ZnodePimCategoryProduct associateCategoryProducts = null;
            List<CategoryProductModel> eachCategoryProductModel = new List<CategoryProductModel>();
            int productId = Convert.ToInt32(categoryProductModel?.FirstOrDefault()?.PimProductId);

            categoryProductModel?.ForEach(eachCategory => {

                int? categoryId = eachCategory?.PimCategoryId;
                List<int> existCategory = CheckProductAssociateToCategory(categoryId, productId);

                if (IsNotNull(existCategory) && existCategory.Count > 0)
                {
                    ZnodeLogging.LogMessage(string.Format(PIM_Resources.ProductAlreadyPresentInCategory, eachCategory?.PimCategoryId), string.Empty, TraceLevel.Error);
                    
                }
                else
                {
                    eachCategoryProductModel?.Add(eachCategory);
                    associateCategoryProducts = _pimCategoryProductRepository.Insert(eachCategory.ToEntity<ZnodePimCategoryProduct>());

                    if (associateCategoryProducts?.PimCategoryProductId <= 0)
                        ZnodeLogging.LogMessage(string.Format(PIM_Resources.ErrorAssociateProductToCategory, eachCategory?.PimCategoryId), string.Empty, TraceLevel.Error);

                    AssociateProductCatalogCategory(eachCategoryProductModel);
                    eachCategoryProductModel.Remove(eachCategoryProductModel?.FirstOrDefault());
                }
            });

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Info);
            return IsNotNull(associateCategoryProducts) ? IsNotNull(associateCategoryProducts) : true;
        }

        public virtual List<int> CheckProductAssociateToCategory(int? pimCategoryId, int productId)
        {
            ZnodeLogging.LogMessage("pimCategoryId and productId to get associatedProductList: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, new object[] { pimCategoryId, productId });
            List<int> associatedProductList = null;
            associatedProductList = _pimCategoryProductRepository.Table?.Where(x => x.PimCategoryId == pimCategoryId && x.PimProductId == productId)
                                    ?.Select(y => y.PimProductId).ToList();

            ZnodeLogging.LogMessage("associatedProductList: ", ZnodeLogging.Components.PIM.ToString(), TraceLevel.Verbose, associatedProductList);
            return associatedProductList;
        }
    }
}