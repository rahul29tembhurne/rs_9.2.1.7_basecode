﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.MongoDB.Data.DataModel;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public class WebStoreWidgetService : BaseService, IWebStoreWidgetService
    {
        #region Private Variables
        private readonly IMongoRepository<WidgetSliderBannerEntity> _sliderBannerMongoRepository;
        private readonly IMongoRepository<WidgetProductEntity> _widgetProductMongoRepository;
        private readonly IMongoRepository<WidgetTitleEntity> _linkMongoRepository;
        private readonly IMongoRepository<WidgetCategoryEntity> _widgetCategoryMongoRepository;
        private readonly IMongoRepository<ProductEntity> _productMongoRepository;
        private readonly IPublishProductService _publishProductService;
        private readonly IMongoRepository<TextWidgetEntity> _widgetTextEntity;
        private readonly IMongoRepository<WidgetBrandEntity> _widgetBrandMongoRepository;
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private readonly IZnodeRepository<ZnodeFormBuilder> _formBuilderRepository;
        private readonly IZnodeRepository<ZnodeCMSFormWidgetConfiguration> _formWidgetConfigurationRepository;
        private readonly IMongoRepository<SearchWidgetEntity> _searcWidgteMongoRepository;
        private readonly IZnodeRepository<ZnodePublishProduct> _publishProductRepository;
        private readonly IZnodeRepository<ZnodePimLinkProductDetail> _linkDetailRepository;
        #endregion

        #region Constructor

        public WebStoreWidgetService()
        {
            _sliderBannerMongoRepository = new MongoRepository<WidgetSliderBannerEntity>(WebstoreVersionId);
            _widgetProductMongoRepository = new MongoRepository<WidgetProductEntity>(WebstoreVersionId);
            _linkMongoRepository = new MongoRepository<WidgetTitleEntity>(WebstoreVersionId);
            _widgetCategoryMongoRepository = new MongoRepository<WidgetCategoryEntity>(WebstoreVersionId);
            _productMongoRepository = new MongoRepository<ProductEntity>(WebstoreVersionId);
            _publishProductService = GetService<IPublishProductService>();
            _widgetTextEntity = new MongoRepository<TextWidgetEntity>(WebstoreVersionId);
            _widgetBrandMongoRepository = new MongoRepository<WidgetBrandEntity>(WebstoreVersionId);
            _categoryMongoRepository = new MongoRepository<CategoryEntity>(GetCatalogVersionId());
            _formBuilderRepository = new ZnodeRepository<ZnodeFormBuilder>();
            _formWidgetConfigurationRepository = new ZnodeRepository<ZnodeCMSFormWidgetConfiguration>();
            _searcWidgteMongoRepository = new MongoRepository<SearchWidgetEntity>(WebstoreVersionId);
            _publishProductRepository = new ZnodeRepository<ZnodePublishProduct>();
            _linkDetailRepository = new ZnodeRepository<ZnodePimLinkProductDetail>();
        }

        #endregion

        #region Public Methods
        //Get slider details.
        public virtual CMSWidgetConfigurationModel GetSlider(WebStoreWidgetParameterModel parameter)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter WebStoreWidgetParameterModel: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, parameter);

            CMSWidgetConfigurationModel model = _sliderBannerMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(CreateFilterCollection(parameter).ToFilterMongoCollection()), false).ToModel<CMSWidgetConfigurationModel>()?.LastOrDefault();
            ZnodeLogging.LogMessage("CMSWidgetSliderBannerId value of CMSWidgetConfigurationModel: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, model?.CMSWidgetSliderBannerId);

            if (model?.SliderBanners?.Count > 0)
                model.SliderBanners = model.SliderBanners.Where(x => (x.ActivationDate == null || x.ActivationDate.GetValueOrDefault().Date <= HelperUtility.GetDate()) && (x.ExpirationDate == null || x.ExpirationDate.GetValueOrDefault().Date >= HelperUtility.GetDate()))?.OrderBy(y => y.BannerSequence).ToList();
            ZnodeLogging.LogMessage("SliderBanners count of CMSWidgetConfigurationModel: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, model?.SliderBanners?.Count);

            return HelperUtility.IsNotNull(model) ? model : new CMSWidgetConfigurationModel();
        }

        //Get Product list widget with product details.
        public virtual WebStoreWidgetProductListModel GetProducts(WebStoreWidgetParameterModel parameter, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            List<WidgetProductEntity> productListWidgetEntity = _widgetProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(CreateFilterCollectionForProducts(parameter).ToFilterMongoCollection()));
            ZnodeLogging.LogMessage("productListWidgetEntity list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, productListWidgetEntity?.Count());
            IMongoQuery query = Query.And(
            Query<CategoryEntity>.EQ(pr => pr.ZnodeCatalogId, parameter.PublishCatalogId),
            Query<CategoryEntity>.EQ(pr => pr.IsActive, true),
             Query<CategoryEntity>.EQ(pr => pr.LocaleId, parameter.LocaleId));
            ZnodeLogging.LogMessage("Mongo query to get categoryListWidgetEntity list: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, query);

            List<CategoryEntity> categoryListWidgetEntity = _categoryMongoRepository.GetEntityList(query).ToList();
            foreach (CategoryEntity item in categoryListWidgetEntity)
            {
                if ((item.ActivationDate == null || item.ActivationDate.GetValueOrDefault().Date <= HelperUtility.GetDate()) && (item.ExpirationDate == null || item.ExpirationDate.GetValueOrDefault().Date >= HelperUtility.GetDate()))
                    parameter.CategoryIds += item.ZnodeCategoryId.ToString() + ",";
            }

            PublishProductListModel model = GetPublishProducts(expands, ProductListFilters(parameter, string.Join(",", productListWidgetEntity?.OrderBy(x => x.DisplayOrder)?.Select(x => x.SKU))), null, null);
            ZnodeLogging.LogMessage("Count of PublishProducts list in PublishProductListModel returned from method GetPublishProducts: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, model?.PublishProducts?.Count);

            WebStoreWidgetProductListModel listModel = ToWebstoreWidgetProductListModel(model?.PublishProducts, productListWidgetEntity);

            listModel.DisplayName = parameter.DisplayName;
            ZnodeLogging.LogMessage("DisplayName property of WebStoreWidgetProductListModel", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, listModel?.DisplayName);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return listModel;
        }

        //Get publish products.
        public virtual PublishProductListModel GetPublishProducts(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
            => _publishProductService.GetPublishProductList(expands, filters, sorts, page);

        //Get link widget data.
        public virtual LinkWidgetConfigurationListModel GetLinkWidget(WebStoreWidgetParameterModel parameter)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter WebStoreWidgetParameterModel: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, parameter);

            ZnodeLogging.LogMessage("Service.GetLinkWidget -> Filter Param:" + MongoQueryHelper.GenerateDynamicWhereClause(CreateFilterCollection(parameter).ToFilterMongoCollection()), "Cache Issue", System.Diagnostics.TraceLevel.Info);

            List<WidgetTitleEntity> model = _linkMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(CreateFilterCollection(parameter).ToFilterMongoCollection()));
            ZnodeLogging.LogMessage("WidgetTitleEntity list count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, model?.Count);

            //Check validity of content pages. 
            CheckContentPages(model, parameter.ProfileId, parameter.PortalId);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Info);
            return new LinkWidgetConfigurationListModel() { LinkWidgetConfigurationList = model?.Count > 0 ? model?.ToModel<LinkWidgetConfigurationModel>().ToList() : new List<LinkWidgetConfigurationModel>() };
        }

        //Get Category list widget with category details.
        public virtual WebStoreWidgetCategoryListModel GetCategories(WebStoreWidgetParameterModel parameter)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter WebStoreWidgetParameterModel: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, parameter);

            List<IMongoQuery> mongoQuery = new List<IMongoQuery>();
            mongoQuery.Add(Query.EQ("TypeOFMapping", BsonRegularExpression.Create(new Regex($"^{parameter.TypeOfMapping}$", RegexOptions.IgnoreCase))));
            mongoQuery.Add(Query.EQ(ZnodeCMSWidgetCategoryEnum.WidgetsKey.ToString(), BsonRegularExpression.Create(new Regex($"^{parameter.WidgetKey}$", RegexOptions.IgnoreCase))));
            mongoQuery.Add(Query.EQ("MappingId", parameter.CMSMappingId));
            ZnodeLogging.LogMessage("Mongo query to get categoryListWidgetEntity list: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, mongoQuery);
            List<WidgetCategoryEntity> categoryListWidgetEntity = _widgetCategoryMongoRepository.GetEntityList(Query.And(mongoQuery));
            ZnodeLogging.LogMessage("categoryListWidgetEntity count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, categoryListWidgetEntity?.Count);

            WebStoreWidgetCategoryListModel listModel = new WebStoreWidgetCategoryListModel();

            if (categoryListWidgetEntity?.Count > 0)
            {
                listModel.Categories = categoryListWidgetEntity?.OrderBy(x => x.DisplayOrder)?.ToModel<WebStoreWidgetCategoryModel>().ToList();

                IPublishCategoryService _publishCategoryService = GetService<IPublishCategoryService>();

                NameValueCollection expands = new NameValueCollection();
                expands.Add(ZnodeConstant.SEO, ZnodeConstant.SEO);

                //Get category data from mongo service.
                PublishCategoryListModel categoryListEntity = _publishCategoryService.GetPublishCategoryList(expands, CategoryListFilters(parameter, listModel), new NameValueCollection(), null);

                //Bind PublishCategoryModel. 
                listModel.Categories.ForEach(item => item.PublishCategoryModel = categoryListEntity?.PublishCategories.FirstOrDefault(x => x.CategoryCode == item.CategoryCode));

                listModel.Categories = listModel.Categories.OrderBy(x => x.DisplayOrder).ToList();
            }
            ZnodeLogging.LogMessage("Categories list of WebStoreWidgetCategoryListModel count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, listModel?.Categories?.Count);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return listModel;
        }

        //Get list of link products.
        public virtual WebStoreLinkProductListModel GetLinkProductList(WebStoreWidgetParameterModel parameter, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter WebStoreWidgetParameterModel: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, parameter);

            List<ProductEntity> model = _productMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(LinkProductListFilters(parameter).ToFilterMongoCollection()), true);

            IEnumerable<AttributeEntity> attributeLinkProduct = model.SelectMany(x => x.Attributes.Where(y => y.AttributeTypeName == "Link"));

            WebStoreLinkProductListModel webStoreLinkProductListModel = new WebStoreLinkProductListModel();
            List<WebStoreLinkProductModel> linkProductList = new List<WebStoreLinkProductModel>();

            //Map link product data.
            MapLinkProducts(parameter, attributeLinkProduct, linkProductList, expands);
            webStoreLinkProductListModel.LinkProductList = linkProductList;
            ZnodeLogging.LogMessage("LinkProductList of WebStoreLinkProductListModel count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, webStoreLinkProductListModel?.LinkProductList?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return webStoreLinkProductListModel;
        }

        //get tag manager data
        public virtual CMSTextWidgetConfigurationModel GetTagManager(WebStoreWidgetParameterModel parameter)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter WebStoreWidgetParameterModel: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, parameter);
            var where = MongoQueryHelper.GenerateDynamicWhereClause(CreateFilterCollection(parameter).ToFilterMongoCollection());
            ZnodeLogging.LogMessage("WhereClause generated to get tag manager data: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, where);
            var entity = _widgetTextEntity.GetEntity(where);
            ZnodeLogging.LogMessage("Executed ", ZnodeLogging.Components.Customers.ToString(), TraceLevel.Info);
            return ToTextWidget(entity);
        }

        //Get brand list widget with brand details.
        public virtual WebStoreWidgetBrandListModel GetBrands(WebStoreWidgetParameterModel parameter)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter WebStoreWidgetParameterModel: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, parameter);

            List<IMongoQuery> mongoQuery = new List<IMongoQuery>();
            mongoQuery.Add(Query.EQ("TypeOFMapping", BsonRegularExpression.Create(new Regex($"^{parameter.TypeOfMapping}$", RegexOptions.IgnoreCase))));
            mongoQuery.Add(Query.EQ(ZnodeCMSWidgetBrandEnum.WidgetsKey.ToString(), BsonRegularExpression.Create(new Regex($"^{parameter.WidgetKey}$", RegexOptions.IgnoreCase))));
            mongoQuery.Add(Query.EQ("MappingId", parameter.CMSMappingId));
            ZnodeLogging.LogMessage("Mongo query to get brandListWidgetEntity: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, mongoQuery);

            List<WidgetBrandEntity> brandListWidgetEntity = _widgetBrandMongoRepository.GetEntityList(Query.And(mongoQuery));

            WebStoreWidgetBrandListModel listModel = new WebStoreWidgetBrandListModel();

            if (brandListWidgetEntity?.Count > 0)
            {
                listModel.Brands = brandListWidgetEntity?.OrderBy(x => x.DisplayOrder)?.ToModel<WebStoreWidgetBrandModel>().ToList();

                IBrandService _brandService = GetService<IBrandService>();

                //Get brand data from mongo service.
                BrandListModel brandListModel = _brandService.GetBrandList(null, BrandListFilters(parameter, listModel), new NameValueCollection(), null);

                //Bind BrandModel. 
                listModel.Brands.ForEach(item => item.BrandModel = brandListModel?.Brands.FirstOrDefault(x => x.BrandId == item.BrandId));

            }
            ZnodeLogging.LogMessage("BrandsList count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, listModel?.Brands?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return listModel;
        }

        // Get Form Configuration By CMSMappingId.
        public virtual WebStoreWidgetFormParameters GetFormConfigurationByCMSMappingId(int mappingId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters mappingId and localeId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new object[] { mappingId, localeId });

            WebStoreWidgetFormParameters formParameters = null;
            if (mappingId > 0)
            {
                formParameters = (from frm in _formBuilderRepository.Table
                                  join cms in _formWidgetConfigurationRepository.Table on frm.FormBuilderId equals cms.FormBuilderId
                                  where cms.CMSMappingId == mappingId && cms.LocaleId == localeId
                                  select new WebStoreWidgetFormParameters
                                  {
                                      LocaleId = cms.LocaleId,
                                      FormCode = frm.FormCode,
                                      CMSMappingId = mappingId,
                                      FormBuilderId = frm.FormBuilderId
                                  })?.FirstOrDefault();

            }
            ZnodeLogging.LogMessage("formParameters to be returned: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, formParameters);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return formParameters ?? new WebStoreWidgetFormParameters();
        }

        public virtual WebStoreWidgetSearchModel GetSearchWidgetData(WebStoreSearchWidgetParameterModel parameter, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter WebStoreSearchWidgetParameterModel: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, parameter);

            List<IMongoQuery> mongoQuery = new List<IMongoQuery>();
            mongoQuery.Add(Query.EQ("TypeOFMapping", BsonRegularExpression.Create(new Regex($"^{parameter.TypeOfMapping}$", RegexOptions.IgnoreCase))));
            mongoQuery.Add(Query.EQ(ZnodeCMSWidgetBrandEnum.WidgetsKey.ToString(), BsonRegularExpression.Create(new Regex($"^{parameter.WidgetKey}$", RegexOptions.IgnoreCase))));
            mongoQuery.Add(Query.EQ("MappingId", parameter.CMSMappingId));
            mongoQuery.Add(Query.EQ("LocaleId", parameter.LocaleId));

            ZnodeLogging.LogMessage("Mongo query to get SearchWidgetEntity: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, mongoQuery);

            SearchWidgetEntity SearchWidgetEntity = _searcWidgteMongoRepository.GetEntity(Query.And(mongoQuery));
            SearchRequestModel requestModel = parameter as SearchRequestModel;
            requestModel.Keyword = SearchWidgetEntity?.SearchKeyword;
            requestModel.IsFacetList = true;
            requestModel.IsSearchWidget = true;
            KeywordSearchModel SearchResult = GetService<ISearchService>().FullTextSearch(requestModel, expands, filters, sorts, page);
            WebStoreWidgetSearchModel model = new WebStoreWidgetSearchModel();
            model.Products = SearchResult.Products;
            model.Facets = SearchResult.Facets;
            model.TotalProductCount = SearchResult.TotalProductCount;
            ZnodeLogging.LogMessage("Products list, Facets list and TotalProductCount of WebStoreWidgetSearchModel count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new { ProductsListCount = model?.Products?.Count, FacetsListCount = model?.Facets?.Count, TotalProductCount = model?.TotalProductCount });
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return model;
        }

        //tag manager mapping
        private CMSTextWidgetConfigurationModel ToTextWidget(TextWidgetEntity entity)
        {
            if (!Equals(entity, null))
            {
                return new CMSTextWidgetConfigurationModel
                {
                    Text = entity.Text
                };
            }
            return new CMSTextWidgetConfigurationModel();
        }
        #endregion

        #region Private Methods
        //Generate filters for Web Store Widget Parameters.
        private FilterCollection CreateFilterCollection(WebStoreWidgetParameterModel parameter)
        => new FilterCollection() {
                new FilterTuple("MappingId", FilterOperators.Equals, parameter.CMSMappingId.ToString()),
                new FilterTuple(ZnodeCMSWidgetSliderBannerEnum.WidgetsKey.ToString(), FilterOperators.Is, parameter.WidgetKey),
                new FilterTuple(ZnodeCMSWidgetSliderBannerEnum.TypeOFMapping.ToString(), FilterOperators.Like, parameter.TypeOfMapping),
                new FilterTuple(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, parameter.LocaleId.ToString())
            };

        private FilterCollection CreateFilterCollectionForProducts(WebStoreWidgetParameterModel parameter)
        => new FilterCollection() {
                new FilterTuple("MappingId", FilterOperators.Equals, parameter.CMSMappingId.ToString()),
                new FilterTuple(ZnodeCMSWidgetSliderBannerEnum.WidgetsKey.ToString(), FilterOperators.Is, parameter.WidgetKey),
                new FilterTuple(ZnodeCMSWidgetSliderBannerEnum.TypeOFMapping.ToString(), FilterOperators.Like, parameter.TypeOfMapping),
          };

        //Generate filters to render product list.
        private FilterCollection ProductListFilters(WebStoreWidgetParameterModel parameter, string SKUs)
        {
            FilterCollection filter = new FilterCollection();
            filter.Add(new FilterTuple(WebStoreEnum.SKU.ToString(), FilterOperators.In, SKUs));
            filter.Add(new FilterTuple(FilterKeys.LocaleId, FilterOperators.Equals, parameter.LocaleId.ToString()));
            filter.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, parameter.PortalId.ToString()));
            filter.Add(new FilterTuple(ZnodeLocaleEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue));
            filter.Add(FilterKeys.ProductIndex, FilterOperators.Equals, ZnodeConstant.DefaultPublishProductIndex.ToString());
            filter.Add(new FilterTuple(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, parameter.PublishCatalogId.ToString()));
            if (!string.IsNullOrEmpty(parameter.CategoryIds))
                filter.Add(new FilterTuple(FilterKeys.CategoryIds, FilterOperators.In, parameter.CategoryIds?.TrimEnd(',')));
            if (parameter.ProductProfileId > 0)
                filter.Add(new FilterTuple(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(parameter.ProductProfileId)));
            return filter;
        }

        //Generate filters to render category list.
        private FilterCollection CategoryListFilters(WebStoreWidgetParameterModel parameter, WebStoreWidgetCategoryListModel listModel)
        {
            FilterCollection filter = new FilterCollection();
            filter.Add(new FilterTuple(WebStoreEnum.CategoryCode.ToString(), FilterOperators.In, string.Join(",", listModel.Categories.Select(x => x.CategoryCode))));
            filter.Add(new FilterTuple(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, parameter.PublishCatalogId.ToString()));
            filter.Add(new FilterTuple(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, parameter.LocaleId.ToString()));
            filter.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, parameter.PortalId.ToString()));
            filter.Add(new FilterTuple(ZnodeLocaleEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue));
            if (parameter.ProductProfileId > 0)
                filter.Add(new FilterTuple(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(parameter.ProductProfileId)));
            return filter;
        }

        //Generate filters to render brand list.
        private FilterCollection BrandListFilters(WebStoreWidgetParameterModel parameter, WebStoreWidgetBrandListModel listModel)
        {
            FilterCollection filter = new FilterCollection();
            filter.Add(new FilterTuple(ZnodeCMSWidgetBrandEnum.BrandId.ToString(), FilterOperators.In, string.Join(",", listModel.Brands.Select(x => x.BrandId))));
            filter.Add(new FilterTuple(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, parameter.LocaleId.ToString()));
            return filter;
        }

        //Generate filters to render product list.
        private FilterCollection LinkProductListFilters(WebStoreWidgetParameterModel parameter)
        {
            FilterCollection filter = new FilterCollection();
            filter.Add(new FilterTuple(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, parameter.CMSMappingId.ToString()));
            filter.Add(new FilterTuple(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, parameter.LocaleId.ToString()));
            int? versionIds = GetCatalogVersionId(Convert.ToInt32(parameter.PublishCatalogId), Convert.ToInt32(parameter.LocaleId));
            ZnodeLogging.LogMessage("Catalog versionIds returned from method GetCatalogVersionId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, versionIds);

            filter.Add(FilterKeys.VersionId, FilterOperators.Equals, versionIds.HasValue ? versionIds.Value.ToString() : "0");

            SetProductIndexFilter(filter);
            if (parameter.ProductProfileId > 0)
                filter.Add(new FilterTuple(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(parameter.ProductProfileId)));
            return filter;
        }

        //Filters for link products.
        private FilterCollection LinkProductFilters(WebStoreWidgetParameterModel parameter, AttributeEntity item)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.In, item.AttributeValues);
            filters.Add(FilterKeys.LocaleId, FilterOperators.Equals, parameter.LocaleId.ToString());
            filters.Add(FilterKeys.PortalId, FilterOperators.Equals, parameter.PortalId.ToString());
            filters.Add(ZnodeLocaleEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue);
            filters.Add(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, Convert.ToString(parameter.PublishCatalogId));
            return filters;
        }

        //Map link product data.
        public void MapLinkProducts(WebStoreWidgetParameterModel parameter, IEnumerable<AttributeEntity> attributeLinkProduct, List<WebStoreLinkProductModel> linkProductList, NameValueCollection expands)
        {
            foreach (var item in attributeLinkProduct)
            {
                if (!string.IsNullOrEmpty(item.AttributeValues))
                {
                    WebStoreLinkProductModel data = new WebStoreLinkProductModel();

                    PublishProductListModel publishProductListModel = GetPublishProducts(expands, LinkProductFilters(parameter, item), null, null);

                    data.AttributeName = item.AttributeName;

                    //Code  is done for showing display order
                    int? pimProductId = _publishProductRepository.Table.FirstOrDefault(x => x.PublishProductId == parameter.CMSMappingId)?.PimProductId;

                    List<ZnodePimLinkProductDetail> pimLinkProductDetail = _linkDetailRepository.Table.Where(x => x.PimParentProductId == pimProductId)?.ToList();

                    IEnumerable<int?> producIds = pimLinkProductDetail.Select(x => x.PimProductId);

                    List<ZnodePublishProduct> productlist = _publishProductRepository.Table.Where(x => producIds.Contains(x.PimProductId)).ToList();


                    publishProductListModel.PublishProducts.ForEach(product =>
                    {
                        ZnodePublishProduct publishProduct = productlist
                                    .FirstOrDefault(productdata => productdata.PublishProductId == product.PublishProductId);

                        ZnodePimLinkProductDetail linkDetails = pimLinkProductDetail.FirstOrDefault(x => x.PimProductId == publishProduct.PimProductId);

                        product.DisplayOrder = linkDetails.DisplayOrder;
                    });

                    data.PublishProduct.AddRange(publishProductListModel.PublishProducts.OrderBy(x => x.DisplayOrder));
                    linkProductList.Add(data);
                }
            }
        }

        //Mapping data from List<PublishProductModel> model to widget product model.
        private WebStoreWidgetProductListModel ToWebstoreWidgetProductListModel(List<PublishProductModel> model, List<WidgetProductEntity> productListWidgetEntity)
        {
            WebStoreWidgetProductListModel webStoreWidgetProductListModel = new WebStoreWidgetProductListModel();
            webStoreWidgetProductListModel.Products = new List<WebStoreWidgetProductModel>();

            foreach (PublishProductModel data in model)
            {
                WebStoreWidgetProductModel webStoreWidgetProductModel = new WebStoreWidgetProductModel();
                webStoreWidgetProductModel.WebStoreProductModel = MapData(data, productListWidgetEntity);
                webStoreWidgetProductListModel.Products.Add(webStoreWidgetProductModel);

            }
            ZnodeLogging.LogMessage("webStoreWidgetProductListModel list count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, webStoreWidgetProductListModel?.Products?.Count);
            webStoreWidgetProductListModel.Products = webStoreWidgetProductListModel.Products?.OrderBy(x => x.WebStoreProductModel.DisplyOrder).ToList();
            return webStoreWidgetProductListModel;
        }

        //Map PublishProductModel to WebStoreProductModel.
        private WebStoreProductModel MapData(PublishProductModel model, List<WidgetProductEntity> productListWidgetEntity)
        {
            return new WebStoreProductModel
            {
                Attributes = model.Attributes,
                LocaleId = model.LocaleId,
                CatalogId = model.PublishedCatalogId,
                PublishProductId = model.PublishProductId,
                Name = model.Name,
                RetailPrice = model.RetailPrice,
                SalesPrice = model.SalesPrice,
                PromotionalPrice = model.PromotionalPrice,
                SKU = model.SKU,
                ProductReviews = model.ProductReviews,
                CurrencyCode = model.CurrencyCode,
                CultureCode = model.CultureCode,
                ImageMediumPath = model.ImageMediumPath,
                ImageSmallPath = model.ImageSmallPath,
                ImageSmallThumbnailPath = model.ImageSmallThumbnailPath,
                ImageThumbNailPath = model.ImageThumbNailPath,
                ImageLargePath = model.ImageLargePath,
                SEODescription = model.SEODescription,
                SEOTitle = model.SEOTitle,
                Rating = model.Rating,
                TotalReviews = model.TotalReviews,
                SEOUrl = model.SEOUrl,
                SEOKeywords = model.SEOKeywords,
                GroupProductPriceMessage = model.GroupProductPriceMessage,
                Promotions = model.Promotions,
                DisplyOrder = productListWidgetEntity?.Where(x => x.ZnodeProductId == model.PublishProductId)?.Select(x => x.DisplayOrder)?.FirstOrDefault(),
            };
        }

        //Get Associated Group Products.
        private void GetAssociatedGroupProducts(PublishProductModel productModel)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, productModel.PublishProductId.ToString());

            IMongoRepository<GroupProductEntity> _groupProductMongoRepository = new MongoRepository<GroupProductEntity>(GetCatalogVersionId());

            //get group product entity.
            GroupProductEntity groupProducts = _groupProductMongoRepository.GetEntity(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()));
            if (HelperUtility.IsNotNull(groupProducts))
            {
                filters.Clear();

                //Associated product ids.
                filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, groupProducts?.AssociatedZnodeProductId.ToString());
                filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, productModel.LocaleId.ToString());

                IMongoRepository<ProductEntity> _ProductMongoRepository = new MongoRepository<ProductEntity>(GetCatalogVersionId());
                IPublishProductHelper helper = GetService<IPublishProductHelper>();
                //get associated product list.
                List<ProductEntity> products = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()));

                List<WebStoreGroupProductModel> groupProductList = products?.ToModel<WebStoreGroupProductModel>().ToList();

                //Map Price and Inventory of group products.
                MapPriceAndInventory(groupProductList, helper.GetPricingBySKUs(groupProductList.Select(x => x.SKU), PortalId, GetLoginUserId(), GetProfileId()));

                WebStoreGroupProductModel groupProduct = groupProductList?.Where(x => x.SalesPrice != null)?.OrderBy(y => (y.SalesPrice != null) ? y.SalesPrice : y.RetailPrice)?.FirstOrDefault();
                if (HelperUtility.IsNotNull(groupProduct))
                {
                    productModel.SalesPrice = groupProduct.SalesPrice;
                    productModel.RetailPrice = groupProduct.RetailPrice;
                }
            }
        }

        //Get Associated configurable Product.
        private PublishProductModel GetAssociatedConfigurableProduct(int productId, int localeId, int? catalogVersionId)
        {
            IPublishProductHelper publishProductHelper = GetService<IPublishProductHelper>();
            //Get configurable product entity.
            List<ConfigurableProductEntity> configEntiy = publishProductHelper.GetConfigurableProductEntity(productId, catalogVersionId);

            //Get associated product list.
            List<ProductEntity> associatedProducts = publishProductHelper.GetAssociatedProducts(productId, localeId, catalogVersionId, configEntiy);
            if (associatedProducts?.Count > 0)
            {
                //Get first product from list of associated products 
                PublishProductModel publishProduct = associatedProducts.FirstOrDefault().ToModel<PublishProductModel>();
                IPublishProductHelper helper = GetService<IPublishProductHelper>();
                //Get expands associated to Product.
                helper.GetProductPriceData(publishProduct, PortalId, GetLoginUserId(), GetProfileId());
                return publishProduct;
            }
            return new PublishProductModel();
        }

        //Map Price and Inventory of group products.
        private void MapPriceAndInventory(List<WebStoreGroupProductModel> groupProducts, List<PriceSKUModel> priceList)
        {
            if (priceList?.Count > 0)
            {
                groupProducts.ForEach(product =>
                {
                    PriceSKUModel productSKU = priceList
                                .Where(productdata => productdata.SKU == product.SKU)
                                ?.FirstOrDefault();
                    if (HelperUtility.IsNotNull(productSKU))
                    {
                        product.SalesPrice = productSKU.SalesPrice;
                        product.RetailPrice = productSKU.RetailPrice;
                        product.CurrencyCode = productSKU.CurrencyCode;
                        product.CultureCode = productSKU.CultureCode;
                        product.CurrencySuffix = productSKU.CurrencySuffix;
                    }
                });
            }
        }

        //Check validity of content pages.
        private void CheckContentPages(List<WidgetTitleEntity> pages, int? profileId, int portalId)
        {
            ZnodeLogging.LogObject(pages.GetType(), pages, "Cache Issue");

            IZnodeRepository<ZnodeCMSSEOType> _seoType = new ZnodeRepository<ZnodeCMSSEOType>();
            IZnodeRepository<ZnodeCMSSEODetail> _seoDetail = new ZnodeRepository<ZnodeCMSSEODetail>();
            IMongoRepository<ContentPageConfigEntity> _contentPageMongoEntity = new MongoRepository<ContentPageConfigEntity>(WebstoreVersionId);

            pages.ForEach(x =>
            {
                if (!string.IsNullOrEmpty(x.Url) && x.Url.StartsWith("/") && (x.Url.Count(y => y == '/') == 1))
                {
                    //Get the seo name from url removing the '/'.
                    string _seoname = x.Url.Substring(1);
                    if (!_seoname.Contains('/'))
                    {

                        string seoCode = (from seoDetail in _seoDetail.Table
                                          join seoType in _seoType.Table on seoDetail.CMSSEOTypeId equals seoType.CMSSEOTypeId
                                          where seoType.Name.ToLower() == ZnodeConstant.ContentPage.ToLower() && seoDetail.PortalId == portalId && seoDetail.SEOUrl == _seoname
                                          select seoDetail.SEOCode)?.FirstOrDefault();
                        ZnodeLogging.LogMessage("seoCode generated: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, seoCode);

                        List<IMongoQuery> mongoQuery = new List<IMongoQuery>();
                        mongoQuery.Add(Query<ContentPageConfigEntity>.EQ(d => d.PageName, seoCode));
                        mongoQuery.Add(Query<ContentPageConfigEntity>.EQ(d => d.IsActive, true));

                        ZnodeLogging.LogMessage("Mongo query to get contentPageEntity: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, mongoQuery);

                        ContentPageConfigEntity contentPageEntity = _contentPageMongoEntity.GetEntity(Query.And(mongoQuery));

                        if (HelperUtility.IsNotNull(contentPageEntity))
                        {
                            if ((HelperUtility.IsNull(contentPageEntity.ActivationDate) || contentPageEntity.ActivationDate.GetValueOrDefault().Date <= HelperUtility.GetDate())
                                && (HelperUtility.IsNull(contentPageEntity.ExpirationDate) || (contentPageEntity.ExpirationDate.GetValueOrDefault().Date >= HelperUtility.GetDate()))
                                && (HelperUtility.IsNull(contentPageEntity.ProfileId) || contentPageEntity.ProfileId.Contains(profileId)) && contentPageEntity.IsActive)
                                x.IsActive = true;
                        }
                    }
                }
                else
                    x.IsActive = true;
            });
        }
        #endregion
    }
}