﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public partial class ContentPageService
    {
        #region Public Methods
        //Get Content page list.
        public virtual WebStoreContentPageListModel GetContentPagesList(FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            string localeId = filters.Find(x => x.FilterName == FilterKeys.LocaleId)?.Item3;
            ZnodeLogging.LogMessage("localeId generated: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, localeId);

            WebStoreContentPageListModel listModel = new WebStoreContentPageListModel();

            List<ContentPageConfigEntity> contentPageList = GetContentPageConfigData(filters);

            GetTextWidgetData(localeId, listModel, contentPageList, filters.Find(x => x.FilterName == FilterKeys.PortalId)?.Item3);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return listModel;
        }
        #endregion

        #region Private Methods
        //Get Text Widget data for content page.
        private void GetTextWidgetData(string localeId, WebStoreContentPageListModel listModel, List<ContentPageConfigEntity> contentPageList, string portalId)
        {
            FilterCollection filterWidget = new FilterCollection()
            {
              new FilterTuple(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, localeId),
               new FilterTuple(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, portalId)};

            listModel.ContentPageList = contentPageList?.ToModel<WebStoreContentPageModel>().ToList();

            List<TextWidgetEntity> widgetEntity = _cmsTextWidgetMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filterWidget.ToFilterMongoCollection()));

            GetContentPageData(Convert.ToInt32(localeId), listModel.ContentPageList, portalId, widgetEntity);
        }

        //Get content page data .
        private List<ContentPageConfigEntity> GetContentPageConfigData(FilterCollection filters)
        {
            //Remove if filter contains Activation and Expiration date of content page.
            filters.RemoveAll(x => x.FilterName.Equals(ZnodeCMSContentPageEnum.ActivationDate.ToString(), StringComparison.InvariantCultureIgnoreCase));
            filters.RemoveAll(x => x.FilterName.Equals(ZnodeCMSContentPageEnum.ExpirationDate.ToString(), StringComparison.InvariantCultureIgnoreCase));

            //Get Portal and profile id from filter.
            int profileId, portalId;
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.PortalId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out portalId);
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(WebStoreEnum.ProfileIds.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out profileId);

            //Get or query for profile id.
            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query<ContentPageConfigEntity>.EQ(d => d.ProfileId, profileId));
            query.Add(Query<ContentPageConfigEntity>.EQ(d => d.ProfileId, null));

            List<IMongoQuery> query2 = new List<IMongoQuery>();
            query2.Add(Query<ContentPageConfigEntity>.EQ(d => d.PortalId, portalId));
            query2.Add(Query<ContentPageConfigEntity>.EQ(d => d.IsActive, true));

            IMongoQuery query3 = Query.And(Query.And(query2), Query.Or(query));
            ZnodeLogging.LogMessage("Mongo query to get ContentPageConfigEntity list: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, query3);
            List<ContentPageConfigEntity> list = _cmsContentPageMongoRepository.GetEntityList(query3);
            ZnodeLogging.LogMessage("ContentPageConfigEntity list count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, list?.Count);
            return GetFilterDateReult(list);
        }

        //Get Content page seo data
        private void GetContentPageData(int localeId, List<WebStoreContentPageModel> contentPageList, string portalId, List<TextWidgetEntity> widgetEntity)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter portalId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, portalId);

            ISEOService seoService = GetService<ISEOService>();
            List<SeoEntity> seosettings = seoService.GetPublishSEOSettingList(ZnodeConstant.ContentPage, Convert.ToInt32(portalId), localeId);

            ZnodeCMSPortalSEOSetting portalSeoSetting = seoService.GetPortalSeoDefaultSetting(portalId);

            //Assign content page seo details.
            if (contentPageList?.Count > 0)
            {
                contentPageList.ForEach(contentPage =>
                {
                    //Get Content page Text.
                    TextWidgetEntity textWidgetEntity = widgetEntity.FirstOrDefault(widget => widget.MappingId == contentPage.ContentPageId && widget.TypeOFMapping == ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString());
                    if (HelperUtility.IsNotNull(textWidgetEntity))
                    {
                        contentPage.Text = textWidgetEntity.Text;
                        contentPage.WidgetsKey = textWidgetEntity.WidgetsKey;
                        contentPage.TypeOFMapping = textWidgetEntity.TypeOFMapping;
                        contentPage.MappingId = textWidgetEntity.MappingId;
                    }

                    //Get content Page seo.
                    SeoEntity seo = seosettings
                                .FirstOrDefault(seoDetail => seoDetail.SEOCode == contentPage.PageName);

                    if (HelperUtility.IsNotNull(seo))
                    {
                        contentPage.SEOUrl = seo.SEOUrl;
                        contentPage.SEODescription = GetSeoDetails(seo.SEODescription, portalSeoSetting?.ContentDescription, contentPage);
                        contentPage.SEOTitle = GetSeoDetails(seo.SEOTitle, portalSeoSetting?.CategoryTitle, contentPage);
                        contentPage.SEOKeywords = GetSeoDetails(seo.SEOKeywords, portalSeoSetting?.ContentKeyword, contentPage);
                        contentPage.CanonicalURL = seo.CanonicalURL ?? string.Empty;
                        contentPage.RobotTag = seo.RobotTag ?? string.Empty;
                    }
                });
            }

            foreach (TextWidgetEntity entity in widgetEntity)
            {
                contentPageList?.Add(new WebStoreContentPageModel
                {
                    Text = entity.Text,
                    WidgetsKey = entity.WidgetsKey,
                    TypeOFMapping = entity.TypeOFMapping,
                    MappingId = entity.MappingId,
                    ContentPageId = entity.MappingId,
                    LocaleId = entity.LocaleId,
                });
            }
            ZnodeLogging.LogMessage("contentPageList count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, contentPageList?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
        }
        #endregion

        //Get SEO according to portal default setting.
        private static string GetSeoDetails(string actualSEOSettings, string siteConfigSEOSettings, WebStoreContentPageModel entity)
        {
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new { actualSEOSettings = actualSEOSettings, siteConfigSEOSettings = siteConfigSEOSettings });
            string seoDetailsText = actualSEOSettings;
            if (string.IsNullOrEmpty(actualSEOSettings) && !string.IsNullOrEmpty(siteConfigSEOSettings))
            {
                string seoDetails = siteConfigSEOSettings;
                seoDetails = seoDetails.Replace(ZnodeConstant.DefaultSEOName, entity.PageName);
                seoDetailsText = seoDetails;
            }
            //Default condition if no default seo setting is present.
            else if (string.IsNullOrEmpty(actualSEOSettings) && string.IsNullOrEmpty(siteConfigSEOSettings))
                seoDetailsText = entity.PageName;
            ZnodeLogging.LogMessage("seoDetailsText to be returned: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, seoDetailsText);
            return seoDetailsText;
        }

        private List<ContentPageConfigEntity> GetFilterDateReult(List<ContentPageConfigEntity> list) =>
        list.Where(x => (x.ActivationDate == null || x.ActivationDate.GetValueOrDefault().Date <= HelperUtility.GetDate()) && (x.ExpirationDate == null || x.ExpirationDate.GetValueOrDefault().Date >= HelperUtility.GetDate())).ToList();
    }
}
