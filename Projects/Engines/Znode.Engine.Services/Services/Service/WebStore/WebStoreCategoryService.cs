﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public partial class CategoryService
    {
        #region Public Methods
        //Get list of published Categories.
        public virtual WebStoreCategoryListModel GetCategoryDetails(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            //Get catalog id from filter.
            int catalogId;
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(WebStoreEnum.ZnodeCatalogId.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out catalogId);

            //Get portal id from filter.          
            var portalId = Convert.ToInt32(filters.Find(x => x.Item1 == FilterKeys.PortalId)?.Item3);
            var znodeCatalogId = Convert.ToInt32(filters.Find(x => x.Item1 == FilterKeys.ZnodeCatalogId.ToLower())?.Item3);
            var publishCatalogId = Convert.ToString(_portalCatalogRepository.Table.FirstOrDefault(x => x.PortalId == portalId)?.PublishCatalogId);

            //Get locale id from filters
            int localeId;
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.LocaleId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out localeId);

            //Get profile id from filters.
            int profileId;
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.ProfileIds, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out profileId);

            ZnodeLogging.LogMessage("catalogId, portalId, znodeCatalogId, publishCatalogId, localeId and profileId generated from filters: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new object[] { catalogId, portalId, znodeCatalogId, publishCatalogId, localeId, profileId });

            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

            ReplaceFilterKeys(ref filters);

            //Get catalog current version id.
            int? versionId = GetCatalogVersionId(catalogId, localeId);
            ZnodeLogging.LogMessage("catalog current versionId returned from method GetCatalogVersionId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, versionId);

            //Add catalog version id to filters.
            filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, Convert.ToString(versionId));

            if (HelperUtility.IsNotNull(sorts))
                ReplaceSortKeys(ref sorts);

            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel generated to get categoryEntityList: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());

            int count = 0;
            List<CategoryEntity> categoryEntityList = _CategoryMongoRepository.GetPagedList(pageListModel.MongoWhereClause, pageListModel.MongoOrderBy, pageListModel.PagingStart, pageListModel.PagingLength, out count, !Equals(znodeCatalogId, publishCatalogId));
            ZnodeLogging.LogMessage("categoryEntityList count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, categoryEntityList?.Count);

            //Filter list by expiration date and activation date.
            categoryEntityList = GetFilterDateReult(categoryEntityList);

            WebStoreCategoryListModel webStoreCategoryListModel = new WebStoreCategoryListModel { Categories = categoryEntityList?.ToModel<WebStoreCategoryModel>().ToList() };

            //Get Category list of catalog.
            List<CategoryEntity> categoryList = GetCatalogCategoryList(localeId, catalogId, versionId, profileId, pageListModel.MongoOrderBy);
            ZnodeLogging.LogMessage("Count of category list of catalog returned from method GetCatalogCategoryList: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, categoryList?.Count);

            //Filter list by expiration date and activation date.
            categoryList = GetFilterDateReult(categoryList);
            ZnodeLogging.LogMessage("Filtered category list by expiration date and activation date count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, categoryList?.Count);

            //Get List of SubCategory associated to category ID.
            if (webStoreCategoryListModel?.Categories.Count > 0)
                GetPublishedSubCategories(webStoreCategoryListModel.Categories, pageListModel, localeId, Convert.ToString(portalId), profileId, pageListModel.MongoOrderBy, categoryList);

            //Get Seo data for category         
            GetCategorySeoDetails(webStoreCategoryListModel, Convert.ToString(portalId), localeId, versionId);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return webStoreCategoryListModel;
        }

        #endregion

        #region Private Method
        //Get list of published SubCategories.
        private void GetPublishedSubCategories(List<WebStoreCategoryModel> webStoreCategoryListModel, PageListModel paging, int localeId, string portalId, int profileId, IMongoSortBy sort, List<CategoryEntity> categoryList)
        {
            if(webStoreCategoryListModel?.Count>0)
            {
                Parallel.ForEach(webStoreCategoryListModel, x =>
                {
                    //Get Sub catgory of main category
                    List<CategoryEntity> SubCategories = categoryList?.Where(y => HelperUtility.IsNotNull(y.ZnodeParentCategoryIds) && y.IsActive ? profileId > 0 ? y.ZnodeParentCategoryIds.Contains(x.PublishCategoryId) && (y.ProfileIds?.Contains(profileId) ?? false) : y.ZnodeParentCategoryIds.Contains(x.PublishCategoryId) : false).ToList();
                    List<CategoryEntity> parentCategory = categoryList?.Where(y => (x.ZnodeParentCategoryIds?.Count() > 0 && y.IsActive) ? profileId > 0 ? x.ZnodeParentCategoryIds.Contains(y.ZnodeCategoryId) && (y.ProfileIds?.Contains(profileId) ?? false) : x.ZnodeParentCategoryIds.Contains(y.ZnodeCategoryId) : false).ToList();
                    x.SubCategories = SubCategories?.Count > 0 ? SubCategories?.ToModel<WebStoreCategoryModel>().ToList() : new List<WebStoreCategoryModel>();
                    x.ParentCategory = parentCategory?.Count > 0 ? parentCategory?.ToModel<PublishCategoryModel>().ToList() : new List<PublishCategoryModel>();
                    if (HelperUtility.IsNotNull(x.ParentCategory) && x.ParentCategory.Any())
                        GetParentCategoryList(x.ParentCategory, categoryList);

                    GetPublishedSubCategories(x.SubCategories, null, localeId, portalId, profileId, sort, categoryList);
                });
            }
            
        }

        //Get parent category hierarchy.
        private void GetParentCategoryList(List<PublishCategoryModel> parentCategory, List<CategoryEntity> webStoreCategoryListModel)
        {
            if (parentCategory.FirstOrDefault()?.ZnodeParentCategoryIds.Length > 0)
            {
                parentCategory.FirstOrDefault().ParentCategory = webStoreCategoryListModel.Where(y => y.ZnodeCategoryId == parentCategory.FirstOrDefault().ZnodeParentCategoryIds[0])?.ToModel<PublishCategoryModel>().ToList();
                GetParentCategoryList(parentCategory.FirstOrDefault().ParentCategory, webStoreCategoryListModel);
            }
        }

        //Get seo details for category
        private void GetCategorySeoDetails(WebStoreCategoryListModel categoryList, string portalId, int localeId, int? versionId = 0)
        {
            ISEOService seoService = GetService<ISEOService>();
            AssignSEODetails(categoryList.Categories, seoService.GetPublishSEOSettingList(ZnodeConstant.Category, Convert.ToInt32(portalId), localeId, versionId), seoService.GetPortalSeoDefaultSetting(portalId));
        }

        //Assign SEO data to categories and subcategories.
        private void AssignSEODetails(List<WebStoreCategoryModel> categories, List<SeoEntity> seosettings, ZnodeCMSPortalSEOSetting seoDefaultSetting)
        {
            if (categories?.Count > 0)
            {
                Parallel.ForEach(categories, category =>
                {
                    string categoryCode = category.Attributes?.FirstOrDefault(x => x.AttributeCode == "CategoryCode")?.AttributeValues;
                    ZnodeLogging.LogMessage("categoryCode generated: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, categoryCode);

                    SeoEntity seoDetails = seosettings
                                 .FirstOrDefault(seoDetail => seoDetail.SEOCode == categoryCode);
                    category.SEODetails = new WebStoreSEOModel();
                    if (HelperUtility.IsNotNull(seoDetails))
                    {
                        category.SEODetails.SEOPageName = seoDetails.SEOUrl;
                        category.SEODetails.SEOKeywords = seoDetails.SEOKeywords;
                        category.SEODetails.SEODescription = seoDetails.SEODescription;
                        category.SEODetails.SEOTitle = seoDetails.SEOTitle;
                    }
                    if (category?.ParentCategory?.Count > 0)
                        GetParentCategorySEO(category.ParentCategory?.FirstOrDefault(), seosettings);
                    AssignSEODetails(category.SubCategories, seosettings, seoDefaultSetting);
                });               
            }
        }

        //Get Parent category seo details.
        private void GetParentCategorySEO(PublishCategoryModel category, List<SeoEntity> seosettings)
        {
            string categoryCode = category.Attributes?.FirstOrDefault(x => x.AttributeCode == "CategoryCode")?.AttributeValues;
            SeoEntity seoDetails = seosettings
                              .FirstOrDefault(seoDetail => seoDetail.SEOCode == categoryCode);
            if (HelperUtility.IsNotNull(seoDetails))
            {
                category.SEOUrl = seoDetails.SEOUrl;
                category.SEOKeywords = seoDetails.SEOKeywords;
                category.SEODescription = seoDetails.SEODescription;
                category.SEOTitle = seoDetails.SEOTitle;
            }
            if (category?.ParentCategory?.Count > 0)
                GetParentCategorySEO(category.ParentCategory?.FirstOrDefault(), seosettings);

        }

        //Replace filter keys for mongo.
        private void ReplaceFilterKeys(ref FilterCollection filters)
        {
            foreach (FilterTuple tuple in filters)
            {
                if (tuple.Item1 == WebStoreEnum.ZnodeCatalogId.ToString().ToLower()) ReplaceFilterKeyName(ref filters, WebStoreEnum.ZnodeCatalogId.ToString().ToLower(), WebStoreEnum.ZnodeCatalogId.ToString());
                if (tuple.Item1 == WebStoreEnum.ZnodeParentCategoryIds.ToString().ToLower()) ReplaceFilterKeyName(ref filters, WebStoreEnum.ZnodeParentCategoryIds.ToString().ToLower(), WebStoreEnum.ZnodeParentCategoryIds.ToString());
                if (tuple.Item1 == WebStoreEnum.ProfileIds.ToString().ToLower()) ReplaceFilterKeyName(ref filters, WebStoreEnum.ProfileIds.ToString().ToLower(), WebStoreEnum.ProfileIds.ToString());
                if (tuple.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower()) ReplaceFilterKeyName(ref filters, ZnodeLocaleEnum.LocaleId.ToString().ToLower(), ZnodeLocaleEnum.LocaleId.ToString());
                if (tuple.Item1 == WebStoreEnum.ZnodeCategoryId.ToString().ToLower()) ReplaceFilterKeyName(ref filters, WebStoreEnum.ZnodeCategoryId.ToString().ToLower(), WebStoreEnum.ZnodeCategoryId.ToString());
                if (tuple.Item1 == WebStoreEnum.IsActive.ToString().ToLower()) ReplaceFilterKeyName(ref filters, WebStoreEnum.IsActive.ToString().ToLower(), WebStoreEnum.IsActive.ToString());
            }
        }
        //Replace Sort Keys
        private void ReplaceSortKeys(ref NameValueCollection sorts)
        {
            foreach (string key in sorts.Keys)
            {
                if (string.Equals(key, FilterKeys.DisplayOrder, StringComparison.OrdinalIgnoreCase)) { ReplaceSortKeyName(ref sorts, FilterKeys.DisplayOrder, ZnodePortalAddressEnum.DisplayOrder.ToString()); }
                if (string.Equals(key, WebStoreEnum.ZnodeCategoryId.ToString(), StringComparison.OrdinalIgnoreCase)) { ReplaceSortKeyName(ref sorts, WebStoreEnum.ZnodeCategoryId.ToString(), WebStoreEnum.ZnodeCategoryId.ToString()); }
            }
        }

        //Get catgory list of catalog.
        private List<CategoryEntity> GetCatalogCategoryList(int localeId, int catalogId, int? versionId, int profileId, IMongoSortBy orderBy)
        {
            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query<CategoryEntity>.EQ(d => d.ZnodeCatalogId, catalogId));
            query.Add(Query<CategoryEntity>.EQ(d => d.LocaleId, localeId));
            if (versionId > 0)
                query.Add(Query<CategoryEntity>.EQ(d => d.VersionId, versionId));
            if (profileId > 0)
                query.Add(Query<CategoryEntity>.Where(d => d.ProfileIds.Contains(profileId)));

            ZnodeLogging.LogMessage("Mongo query to get category list of catalog: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, query);

            return _CategoryMongoRepository.GetEntityList(Query.And(query), orderBy);
        }

        //Filter list by expiration date and activation date.
        private List<CategoryEntity> GetFilterDateReult(List<CategoryEntity> list) =>
         list.Where(x => (x.ActivationDate == null || x.ActivationDate.GetValueOrDefault().Date <= HelperUtility.GetDate()) && (x.ExpirationDate == null || x.ExpirationDate.GetValueOrDefault().Date >= HelperUtility.GetDate())).ToList();

        #endregion
    }
}
