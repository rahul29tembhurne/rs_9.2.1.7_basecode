﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Engine.Services
{
    public partial class ManageMessageService
    {
        //To Do
        //Get message by message key, area and portal id.
        public virtual ManageMessageModel GetMessage(NameValueCollection expands, FilterCollection filters) => new ManageMessageModel();

        // Get messages list by locale id and portal id.
        public virtual ManageMessageListModel GetMessages(NameValueCollection expands, FilterCollection filters, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);

            IMongoQuery MongoWhereClause = MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection());
            PageListModel pageListModel = new PageListModel(filters, null, null);
            ZnodeLogging.LogMessage("pageListModel generated to get messages list: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());

            //Get portalId
            string portalId =  filters?.FirstOrDefault(x => x.FilterName.ToLower() == FilterKeys.PortalId)?.FilterValue;
            ZnodeLogging.LogMessage("Generated portalId value: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, portalId);

            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query.And(Query<MessageEntity>.EQ(pr => pr.LocaleId, localeId)));
            query.Add(Query.And(Query<MessageEntity>.EQ(pr => pr.PortalId, Convert.ToInt32(portalId))));

            List<MessageEntity> messages = _cmsMessageMongoRepository.GetPagedList(Query.And(query), pageListModel.MongoOrderBy, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount);

            query = new List<IMongoQuery>();
            query.Add(Query.And(Query<GlobalMessageEntity>.EQ(pr => pr.LocaleId, localeId)));

            List<GlobalMessageEntity> gmessages = _cmsGlobalMessageMongoRepository.GetPagedList(Query.And(query), pageListModel.MongoOrderBy, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount);

            var resultmessages = gmessages.Where(gm => gm.LocaleId == localeId && !messages.Any(mes => mes.MessageKey == gm.MessageKey && mes.LocaleId == gm.LocaleId));

            foreach (var resultmessage in resultmessages)
            {
                messages.Add(new MessageEntity() {
                    LocaleId = resultmessage.LocaleId,
                    MessageKey = resultmessage.MessageKey,
                    Message = resultmessage.Message,
                    PortalId = null,                    
                });
            }
            ZnodeLogging.LogMessage("MessageEntity and GlobalMessageEntity list count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, messages?.Count);

            return new ManageMessageListModel() { ManageMessages = messages?.ToModel<ManageMessageModel>()?.ToList() };
        }
    }

}
