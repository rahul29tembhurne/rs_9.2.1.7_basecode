﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Tasks;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public class ERPTaskSchedulerService : BaseService, IERPTaskSchedulerService
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodeERPTaskScheduler> _erpTaskSchedulerRepository;
        private readonly IERPConfiguratorService _service;
        private readonly IZnodeRepository<ZnodeERPConfigurator> _eRPConfiguratorRepository;
        readonly ERPTaskScheduler _eRPTaskScheduler;
        private readonly IZnodeRepository<ZnodeSearchIndexMonitor> _searchIndexMonitorRepository;
        #endregion

        #region Constructor
        public ERPTaskSchedulerService()
        {
            _erpTaskSchedulerRepository = new ZnodeRepository<ZnodeERPTaskScheduler>();
            _service = GetService<IERPConfiguratorService>();
            _eRPTaskScheduler = new ERPTaskScheduler();
            _eRPConfiguratorRepository = new ZnodeRepository<ZnodeERPConfigurator>();
            _searchIndexMonitorRepository = new ZnodeRepository<ZnodeSearchIndexMonitor>();
        }
        #endregion

        #region Public Methods
        // Get ERPTaskScheduler list from database.
        public virtual ERPTaskSchedulerListModel GetERPTaskSchedulerList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            filters.Add(ZnodeERPTaskSchedulerEnum.ERPConfiguratorId.ToString(), ProcedureFilterOperators.Equals, _service.GetActiveERPClassId().ToString());
            //maps the entity list to model
            ZnodeLogging.LogMessage("pageListModel to get ERP task schedular list: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            IList<ZnodeERPTaskScheduler> erpTaskSchedulerList = _erpTaskSchedulerRepository.GetPagedList(pageListModel.EntityWhereClause.WhereClause, pageListModel.OrderBy, null, pageListModel.EntityWhereClause.FilterValues, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("erpTaskSchedulerList count: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, erpTaskSchedulerList?.Count);
            ERPTaskSchedulerListModel listModel = new ERPTaskSchedulerListModel();
            listModel.ERPTaskSchedulerList = erpTaskSchedulerList?.Count > 0 ? erpTaskSchedulerList.ToModel<ERPTaskSchedulerModel>().ToList() : new List<ERPTaskSchedulerModel>();
            //Set for pagination
            listModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            return listModel;
        }

        //Create erpTaskScheduler.
        public virtual ERPTaskSchedulerModel Create(ERPTaskSchedulerModel erpTaskSchedulerModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            if (IsNull(erpTaskSchedulerModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelCanNotBeNull);

            if (erpTaskSchedulerModel.ERPTaskSchedulerId == 0 && IsSchedulerNameAlreadyExist(erpTaskSchedulerModel.SchedulerName) && !erpTaskSchedulerModel.IsAssignTouchPoint)
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorSchedulerNameExists, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.ErrorSchedulerNameExists);
            }

            int status;
            IList<View_ReturnBoolean> createResult;
            if (erpTaskSchedulerModel.IsAssignTouchPoint)
            {
                List<String> touchPointNamesList = new List<String>(erpTaskSchedulerModel.TouchPointName.Split(','));
                foreach (var item in touchPointNamesList)
                {
                    erpTaskSchedulerModel.TouchPointName = item;
                    InsertERPTaskScheduler(erpTaskSchedulerModel, out status, out createResult);
                    if (status == 1)
                        ZnodeLogging.LogMessage(Admin_Resources.SuccessERPTaskSchedulerSave, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                    else
                        ZnodeLogging.LogMessage(Admin_Resources.ErrorERPTaskSchedulerSave, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                }
                return erpTaskSchedulerModel;
            }
            else
                InsertERPTaskScheduler(erpTaskSchedulerModel, out status, out createResult);

            ZnodeLogging.LogMessage("ERPTaskSchedulerId and createResult list count: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, new object[] { erpTaskSchedulerModel?.ERPTaskSchedulerId, createResult?.Count } );
            if (status == 1 && (erpTaskSchedulerModel.SchedulerType == ZnodeConstant.Scheduler || erpTaskSchedulerModel.SchedulerCallFor == ZnodeConstant.SearchIndex || erpTaskSchedulerModel.SchedulerCallFor == ZnodeConstant.ProductFeed))
                return CreateScheduler(erpTaskSchedulerModel, createResult);

            if (erpTaskSchedulerModel.SchedulerType != ZnodeConstant.Scheduler && erpTaskSchedulerModel.SchedulerCallFor == ZnodeConstant.ERP)
            {
                //Delete ERP task scheduler from server
                ZnodeLogging.LogMessage("TaskSchedulerNames to be deleted: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, erpTaskSchedulerModel?.SchedulerName?.Split(',').ToList());
                bool schedulerStatus = _eRPTaskScheduler.DeleteScheduleTask(erpTaskSchedulerModel.SchedulerName.Split(',').ToList());
                ZnodeLogging.LogMessage(Admin_Resources.SuccessERPTaskSchedulerDelete, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return erpTaskSchedulerModel;
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorERPTaskSchedulerSave, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return erpTaskSchedulerModel;
            }
        }


        //Get erpTaskScheduler by erpTaskScheduler id.
        public virtual ERPTaskSchedulerModel GetERPTaskScheduler(int erpTaskSchedulerId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            if (erpTaskSchedulerId <= 0)
                return null;
            IZnodeViewRepository<ERPTaskSchedulerModel> erpTaskSchedulerModel = new ZnodeViewRepository<ERPTaskSchedulerModel>();
            ZnodeLogging.LogMessage("erpTaskSchedulerId to get ERPTaskScheduler: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, erpTaskSchedulerId);
            erpTaskSchedulerModel.SetParameter("erpTaskSchedulerId", erpTaskSchedulerId, ParameterDirection.Input, DbType.Int32);
            var erpTaskScheduler = erpTaskSchedulerModel.ExecuteStoredProcedureList("Znode_GetERPTaskSchedulerDetail @ERPTaskSchedulerId");
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            return erpTaskScheduler?.FirstOrDefault();
        }

        //Delete erpTaskScheduler.
        public virtual bool Delete(ParameterModel erpTaskSchedulerIds)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            if (IsNull(erpTaskSchedulerIds) || string.IsNullOrEmpty(erpTaskSchedulerIds.Ids))
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorERPTaskSchedulerIdLessThanOne);

            //Get ERP Scheduler name from Id's
            int[] erpTaskSchedulerIdsArray = erpTaskSchedulerIds.Ids.Split(',').Select(int.Parse).ToArray();
            var TaskSchedulerNames = _erpTaskSchedulerRepository.Table.Where(item => erpTaskSchedulerIdsArray.Contains(item.ERPTaskSchedulerId)).Select(x => x.SchedulerName).ToList();
            //Delete ERP task scheduler from database
            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter(ZnodeERPTaskSchedulerEnum.ERPTaskSchedulerId.ToString(), erpTaskSchedulerIds.Ids, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Boolean);
            int status = 0;
            ZnodeLogging.LogMessage("ERP task scheduler with Ids to be deleted: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, erpTaskSchedulerIds?.Ids);
            objStoredProc.ExecuteStoredProcedureList("Znode_DeleteERPTaskScheduler @ERPTaskSchedulerId,  @Status OUT", 1, out status);
            if (status == 1)
            {
                //Delete ERP task scheduler from server
                ZnodeLogging.LogMessage("TaskSchedulerNames to be deleted: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, TaskSchedulerNames);
                bool schedulerStatus = _eRPTaskScheduler.DeleteScheduleTask(TaskSchedulerNames);
                ZnodeLogging.LogMessage(Admin_Resources.SuccessERPTaskSchedulerDelete, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return true;
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorERPTaskSchedulerDelete, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
                return false;
            }
        }

        // Trigger action for the task.
        public virtual string TriggerSchedulerTask(string erpTaskSchedulerId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            string data = string.Empty;
            IZnodeViewRepository<ERPTaskSchedulerModel> erpTaskSchedulerModel = new ZnodeViewRepository<ERPTaskSchedulerModel>();
            erpTaskSchedulerModel.SetParameter("erpTaskSchedulerId", erpTaskSchedulerId, ParameterDirection.Input, DbType.Int32);
            ZnodeLogging.LogMessage("erpTaskSchedulerId to get ERPTaskScheduler: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, erpTaskSchedulerId);
            var erpTaskScheduler = erpTaskSchedulerModel.ExecuteStoredProcedureList("Znode_GetERPTaskSchedulerDetail @ERPTaskSchedulerId");

            if (!Equals(erpTaskScheduler, null))
            {
                string conectorTouchPoints = erpTaskScheduler.Select(x => x.TouchPointName).FirstOrDefault();
                ZnodeLogging.LogMessage("conectorTouchPoints: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, conectorTouchPoints);
                //Get current active class name
                IERPConfiguratorService eRPConfiguratorService = GetService<IERPConfiguratorService>();
                Assembly assembly = Assembly.Load("Znode.Engine.ERPConnector");
                Type className = assembly.GetTypes().FirstOrDefault(g => g.Name == eRPConfiguratorService.GetActiveERPClassName());

                //Create Instance of active class
                object instance = Activator.CreateInstance(className);

                //Get Method Information from class
                MethodInfo info = className.GetMethod(conectorTouchPoints);

                //Calling method with null parameter
                info.Invoke(instance, null);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            return data;
        }

        //Method for get erpConfiguratorId from TouchPointName
        public virtual int GetSchedulerIdByTouchPointName(string ERPTouchPointName, int erpConfiguratorId, string schedulerCallFor)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("ERPTouchPointName, erpConfiguratorId and schedulerCallFor: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, new object[] { ERPTouchPointName, erpConfiguratorId, schedulerCallFor });
            if (schedulerCallFor == ZnodeConstant.ERP)
            {
                erpConfiguratorId = erpConfiguratorId == 0 ? _eRPConfiguratorRepository.Table.Where(x => x.IsActive).Select(g => g.ERPConfiguratorId).FirstOrDefault() : erpConfiguratorId;
                return _erpTaskSchedulerRepository.Table.Where(x => x.ERPConfiguratorId == erpConfiguratorId && x.TouchPointName == ERPTouchPointName && !string.IsNullOrEmpty(x.SchedulerName)).Select(g => g.ERPTaskSchedulerId).FirstOrDefault();
            }
            else
                return _erpTaskSchedulerRepository.Table.Where(x => x.TouchPointName == ERPTouchPointName && !string.IsNullOrEmpty(x.SchedulerName)).Select(g => g.ERPTaskSchedulerId).FirstOrDefault();
        }

        //Enable/Disable task scheduler from db and from windows service as well.
        public virtual bool EnableDisableTaskScheduler(int ERPTaskSchedulerId, bool isActive)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            bool isTaskSchedulerUpdated = false;

            if (ERPTaskSchedulerId == 0)
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorTaskSchedulerExists, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error);
                throw new ZnodeException(ErrorCodes.NotFound, Admin_Resources.ErrorTaskSchedulerExists);
            }

            ZnodeLogging.LogMessage("ERPTaskSchedulerId to get ERPTaskSchedulerModel: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, ERPTaskSchedulerId);
            ERPTaskSchedulerModel eRPTaskSchedulerModel = GetERPTaskScheduler(ERPTaskSchedulerId);

            //Update task scheduler
            eRPTaskSchedulerModel.IsEnabled = isActive;
            isTaskSchedulerUpdated = _erpTaskSchedulerRepository.Update(eRPTaskSchedulerModel.ToEntity<ZnodeERPTaskScheduler>());
            if (isTaskSchedulerUpdated)
            {
                ZnodeLogging.LogMessage("SchedulerName and state to enable disable schedule task: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, new object[] { eRPTaskSchedulerModel?.SchedulerName, isActive });
                _eRPTaskScheduler.EnableDisableScheduleTask(eRPTaskSchedulerModel.SchedulerName, isActive);
            }  

            if (isActive)
                ZnodeLogging.LogMessage(isTaskSchedulerUpdated ? Admin_Resources.SuccessTaskSchedulerEnable : Admin_Resources.ErrorTaskSchedulerEnable, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            else
                ZnodeLogging.LogMessage(isTaskSchedulerUpdated ? Admin_Resources.SuccessTaskSchedulerDisable : Admin_Resources.ErrorTaskSchedulerDisable, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            return isTaskSchedulerUpdated;
        }

        #endregion

        #region Private Methods
        // Check scheduler Name is exist or not. 
        private bool IsSchedulerNameAlreadyExist(string schedulerName)
        => _erpTaskSchedulerRepository.Table.Any(x => x.SchedulerName == schedulerName);

        private void InsertERPTaskScheduler(ERPTaskSchedulerModel erpTaskSchedulerModel, out int status, out IList<View_ReturnBoolean> createResult)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            if (erpTaskSchedulerModel.ERPTaskSchedulerId == 0)
            {
                erpTaskSchedulerModel.ERPConfiguratorId = _eRPConfiguratorRepository.Table.Where(x => x.IsActive).Select(g => g.ERPConfiguratorId).FirstOrDefault();
                erpTaskSchedulerModel.ERPTaskSchedulerId = _erpTaskSchedulerRepository.Table.Where(x => x.ERPConfiguratorId == erpTaskSchedulerModel.ERPConfiguratorId && x.TouchPointName == erpTaskSchedulerModel.TouchPointName).Select(g => g.ERPTaskSchedulerId).FirstOrDefault();
            }

            var xmlData = HelperUtility.ToXML(erpTaskSchedulerModel);
            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter("SchedulerXML", xmlData, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("UserId", HelperMethods.GetLoginUserId(), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Boolean);
            status = 0;
            createResult = objStoredProc.ExecuteStoredProcedureList("Znode_InsertUpdateERPScheduler @SchedulerXML,@UserId, @status OUT", 2, out status);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
        }

        //Set Scheduler Parameters
        private void SetSchedulerParameters(ERPTaskSchedulerModel erpTaskSchedulerModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            string apiDomainUrl = $"{HttpContext.Current.Request.Url.Scheme + "://"}{HttpContext.Current.Request.Url.Authority}";

            string schedulerPath = GetSchedulerPath();

             string tokenvalue = HttpContext.Current.Request.Headers["Token"] == null ? "0" : HttpContext.Current.Request.Headers["Token"];
            erpTaskSchedulerModel.ExePath = !string.IsNullOrEmpty(schedulerPath) && File.Exists(schedulerPath) ? schedulerPath : System.Web.Hosting.HostingEnvironment.MapPath(ZnodeConstant.schedulerPath);

            switch (erpTaskSchedulerModel.SchedulerCallFor)
            {
                case ZnodeConstant.SearchIndex:
                    ZnodeSearchIndexMonitor searchIndexMonitor;
                    searchIndexMonitor = SearchIndexMonitorInsert(erpTaskSchedulerModel);
                    SearchHelper searchHelper = new SearchHelper();
                    var searchIndexServerStatusId = searchHelper.CreateSearchIndexServerStatus(new SearchIndexServerStatusModel()
                    {
                        SearchIndexMonitorId = searchIndexMonitor.SearchIndexMonitorId,
                        ServerName = Environment.MachineName,
                        Status = ZnodeConstant.SearchIndexStartedStatus
                    }).SearchIndexServerStatusId;

                    erpTaskSchedulerModel.ExeParameters = $"{erpTaskSchedulerModel.SchedulerCallFor} {erpTaskSchedulerModel.CatalogId} {erpTaskSchedulerModel.IndexName} {erpTaskSchedulerModel.CatalogIndexId} {searchIndexMonitor.SearchIndexMonitorId} SchedulerInUse {apiDomainUrl} {GetLoginUserId()} {searchIndexServerStatusId} PRODUCTION {HttpContext.Current.Request.Headers["Authorization"]?.Replace("Basic ", "")} {tokenvalue} {ZnodeApiSettings.RequestTimeout}";
                    break;
                case ZnodeConstant.ERP:
                    erpTaskSchedulerModel.ExeParameters = $"{erpTaskSchedulerModel.SchedulerCallFor} {erpTaskSchedulerModel.ERPTaskSchedulerId.ToString()} {PortalId} {apiDomainUrl} {erpTaskSchedulerModel.SchedulerName} {GetLoginUserId()} {HttpContext.Current.Request.Headers["Authorization"]?.Replace("Basic ", "")} {tokenvalue} {ZnodeApiSettings.RequestTimeout}";
                    break;
                case ZnodeConstant.ProductFeed:
                    SetProductFeedParameter(erpTaskSchedulerModel, GetLoginUserId());
                    erpTaskSchedulerModel.ExeParameters = $"{erpTaskSchedulerModel.SchedulerCallFor} {erpTaskSchedulerModel.ExeParameters}";
                    break;
                case ZnodeConstant.PublishCatalog:
                    erpTaskSchedulerModel.ExeParameters = GetEXEParameterForPublishCatalog(erpTaskSchedulerModel, apiDomainUrl);
                    break;
            }
            ZnodeLogging.LogMessage("ExeParameters value: ", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Verbose, erpTaskSchedulerModel?.ExeParameters);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
        }

        //Insert into ZnodeSearch indexMonitor.
        private ZnodeSearchIndexMonitor SearchIndexMonitorInsert(ERPTaskSchedulerModel portalIndexModel)
        {
            return _searchIndexMonitorRepository.Insert(new ZnodeSearchIndexMonitor()
            {
                SourceId = 0,
                CatalogIndexId = portalIndexModel.CatalogIndexId,
                SourceType = "CreateIndex",
                SourceTransactionType = "INSERT",
                AffectedType = "CreateIndex",
                CreatedBy = portalIndexModel.CreatedBy,
                CreatedDate = portalIndexModel.CreatedDate,
                ModifiedBy = portalIndexModel.ModifiedBy,
                ModifiedDate = portalIndexModel.ModifiedDate
            });
        }

        //Get excutable parameter for publish catalog
        private string GetEXEParameterForPublishCatalog(ERPTaskSchedulerModel erpTaskSchedulerModel, string apiDomainUrl)
        {
            string[] args = erpTaskSchedulerModel.TouchPointName.Split('_');
            string tokenvalue = HttpContext.Current.Request.Headers["Token"] == null ? "0" : HttpContext.Current.Request.Headers["Token"];
            return $"{erpTaskSchedulerModel.SchedulerCallFor} {args[1]} \"{args[2]}\" {apiDomainUrl} {GetLoginUserId()} {HttpContext.Current.Request.Headers["Authorization"]?.Replace("Basic ", "")} {tokenvalue} {ZnodeApiSettings.RequestTimeout}";
        }

        private ERPTaskSchedulerModel CreateScheduler(ERPTaskSchedulerModel erpTaskSchedulerModel, IList<View_ReturnBoolean> createResult)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage(Admin_Resources.SuccessERPTaskSchedulerSave, ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            bool isCreatedRecord = false;

            if (erpTaskSchedulerModel.ERPTaskSchedulerId == 0)
                isCreatedRecord = true;

            erpTaskSchedulerModel.ERPTaskSchedulerId = createResult.FirstOrDefault().Id;
            ParameterModel erpTaskSchedulerIds = new ParameterModel();
            erpTaskSchedulerIds.Ids = Convert.ToString(erpTaskSchedulerModel.ERPTaskSchedulerId);
            bool schedulerStatus = false;
            try
            {
                SetSchedulerParameters(erpTaskSchedulerModel);
                schedulerStatus = _eRPTaskScheduler.ScheduleTask(erpTaskSchedulerModel);

                if (!erpTaskSchedulerModel.IsEnabled)
                    _eRPTaskScheduler.EnableDisableScheduleTask(erpTaskSchedulerModel.SchedulerName, erpTaskSchedulerModel.IsEnabled);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(string.Format(Admin_Resources.ErrorInScheduleTask, ex.StackTrace), ZnodeLogging.Components.ERP.ToString(), TraceLevel.Error, ex);
            }
            finally
            {
                if (!schedulerStatus && isCreatedRecord)
                    Delete(erpTaskSchedulerIds);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
            return erpTaskSchedulerModel;
        }

        private static void SetProductFeedParameter(ERPTaskSchedulerModel erpTaskSchedulerModel,int userId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);

            IProductFeedService productFeedService = GetService<IProductFeedService>();

            NameValueCollection expands = SetExpandsForProductFeed();

            string tokenvalue = HttpContext.Current.Request.Headers["Token"] == null ? "0" : HttpContext.Current.Request.Headers["Token"];
            ProductFeedModel productFeedModel = productFeedService.GetProductFeed(Convert.ToInt32(erpTaskSchedulerModel.TouchPointName.Split('_')[1]), expands);
            if (IsNotNull(productFeedModel))
                erpTaskSchedulerModel.ExeParameters = $"{productFeedModel.ProductFeedId}#{productFeedModel.LocaleId}#{productFeedModel.ProductFeedTimeStampName}#{productFeedModel.ProductFeedPriority}#{productFeedModel.ProductFeedSiteMapTypeCode}#{productFeedModel.ProductFeedTypeCode}#{productFeedModel.Title}#{productFeedModel.Link}#{productFeedModel.Description}#{productFeedModel.FileName}#{productFeedModel.Stores}#{erpTaskSchedulerModel.DomainName}#{erpTaskSchedulerModel.SchedulerName}#{userId}#{HttpContext.Current.Request.Headers["Authorization"]?.Replace("Basic ", "")}#{tokenvalue}#{ZnodeApiSettings.RequestTimeout}";
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.ERP.ToString(), TraceLevel.Info);
        }

        private static NameValueCollection SetExpandsForProductFeed()
        {
            NameValueCollection expands = new NameValueCollection();
            expands.Add(ZnodeProductFeedEnum.ZnodeProductFeedPriority.ToString().ToLower(), ZnodeProductFeedEnum.ZnodeProductFeedPriority.ToString().ToLower());
            expands.Add(ZnodeProductFeedEnum.ZnodeProductFeedSiteMapType.ToString().ToLower(), ZnodeProductFeedEnum.ZnodeProductFeedSiteMapType.ToString().ToLower());
            expands.Add(ZnodeProductFeedEnum.ZnodeProductFeedTimeStamp.ToString().ToLower(), ZnodeProductFeedEnum.ZnodeProductFeedTimeStamp.ToString().ToLower());
            expands.Add(ZnodeProductFeedEnum.ZnodeProductFeedType.ToString().ToLower(), ZnodeProductFeedEnum.ZnodeProductFeedType.ToString().ToLower());
            return expands;
        }
        #endregion

    }
}
