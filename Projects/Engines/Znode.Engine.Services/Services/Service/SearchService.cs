﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.ElasticSearch;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Search;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
using Znode.Libraries.Resources;

namespace Znode.Engine.Services
{
    public class SearchService : BaseService, ISearchService
    {
        #region Private Variables.
        private readonly IMongoRepository<ContentPageConfigEntity> _contentMongoRepository;
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private readonly IMongoRepository<ProductEntity> _productMongoRepository;
        private readonly IZnodeRepository<ZnodeSearchGlobalProductBoost> _globalProductBoostRepository;
        private readonly IZnodeRepository<ZnodeSearchGlobalProductCategoryBoost> _globalProductCategoryBoostRepository;
        private readonly IZnodeRepository<ZnodeSearchDocumentMapping> _documentMappingRepository;
        private readonly IZnodeRepository<ZnodeCatalogIndex> _catalogIndexRepository;
        private readonly IZnodeRepository<ZnodeSearchIndexMonitor> _searchIndexMonitorRepository;
        private readonly IZnodeRepository<ZnodeSearchIndexServerStatu> _searchIndexServerStatusRepository;
        private readonly IZnodeRepository<ZnodeSearchKeywordsRedirect> _searchKeywordsRedirectRepository;
        private readonly IPublishProductHelper publishProductHelper;
        private readonly IZnodeRepository<ZnodeSearchSynonym> _searchSynonymsRepository;
        private const string FieldType = "field";
        private const string CategoryType = "category";
        private const string ProductType = "product";
        public static string SKU { get; } = "sku";
        public static string Width { get; } = "width";
        public static string Height { get; } = "height";
        private readonly IMongoRepository<SeoEntity> _seoProductMongoRepository;
        private readonly IMongoRepository<SeoEntity> _seoCMSMongoRepository;
        private readonly IZnodeRepository<ZnodePublishStateApplicationTypeMapping> _publishStateMappingRepository;
        private readonly IZnodeRepository<ZnodePublishState> _publishStateRepository;

        #endregion

        #region Constructor
        public SearchService()
        {
            ObjectFactory.Initialize(x =>
            {
                x.For<IZnodeSearchProvider>().Use<ElasticSearchProvider>();
                x.For<IZnodeSearchRequest>().Use<ElasticSearchRequest>();
            });

            _contentMongoRepository = new MongoRepository<ContentPageConfigEntity>();
            _contentMongoRepository = new MongoRepository<ContentPageConfigEntity>(WebstoreVersionId);
            _categoryMongoRepository = new MongoRepository<CategoryEntity>(GetCatalogVersionId());
            _productMongoRepository = new MongoRepository<ProductEntity>(GetCatalogVersionId());
            _globalProductBoostRepository = new ZnodeRepository<ZnodeSearchGlobalProductBoost>();
            _globalProductCategoryBoostRepository = new ZnodeRepository<ZnodeSearchGlobalProductCategoryBoost>();
            _documentMappingRepository = new ZnodeRepository<ZnodeSearchDocumentMapping>();
            _catalogIndexRepository = new ZnodeRepository<ZnodeCatalogIndex>();
            _searchIndexMonitorRepository = new ZnodeRepository<ZnodeSearchIndexMonitor>();
            _searchIndexServerStatusRepository = new ZnodeRepository<ZnodeSearchIndexServerStatu>();
            _searchSynonymsRepository = new ZnodeRepository<ZnodeSearchSynonym>();
            _searchKeywordsRedirectRepository = new ZnodeRepository<ZnodeSearchKeywordsRedirect>();
            publishProductHelper = ZnodeDependencyResolver.GetService<IPublishProductHelper>();
            _seoProductMongoRepository = new MongoRepository<SeoEntity>(GetCatalogVersionId());
             _seoCMSMongoRepository = new MongoRepository<SeoEntity>(WebstoreVersionId);
            _publishStateMappingRepository = new ZnodeRepository<ZnodePublishStateApplicationTypeMapping>();
            _publishStateRepository = new ZnodeRepository<ZnodePublishState>();
        }
        #endregion

        #region Public Methods

        #region Indexing methods
        //Gets the portal index data.
        public virtual PortalIndexModel GetCatalogIndexData(NameValueCollection expands, FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            PageListModel pageListModel = new PageListModel(filters, null, null);
            ZnodeLogging.LogMessage("WhereClause to get catalog index data: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, pageListModel?.EntityWhereClause.WhereClause);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);

            return _catalogIndexRepository.GetEntity(pageListModel.EntityWhereClause.WhereClause, GetExpands(expands))?.ToModel<PortalIndexModel, ZnodeCatalogIndex>();
        }

        //Inserts data for creating index by checking revision type.
        public virtual PortalIndexModel InsertCreateIndexDataByRevisionTypes(PortalIndexModel portalIndexModel)
        {
            portalIndexModel.RevisionType = String.IsNullOrEmpty(portalIndexModel.RevisionType) ? ZnodePublishStatesEnum.PRODUCTION.ToString() : portalIndexModel.RevisionType;

            if (portalIndexModel.RevisionType == ZnodePublishStatesEnum.PRODUCTION.ToString() && GetIsPreviewEnabled())
            {
                List<string> RevisionTypes = new List<string>() { ZnodePublishStatesEnum.PREVIEW.ToString(), ZnodePublishStatesEnum.PRODUCTION.ToString() };

                foreach (string revisionType in RevisionTypes)
                {
                    portalIndexModel.RevisionType = revisionType;
                    portalIndexModel = InsertCreateIndexData(portalIndexModel);
                }

                return portalIndexModel;
            }

            return InsertCreateIndexData(portalIndexModel);
        }

        //Inserts data for creating index.
        public virtual PortalIndexModel InsertCreateIndexData(PortalIndexModel portalIndexModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            if (IsNull(portalIndexModel))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorPortalIndexModelNull);

            ZnodeSearchIndexMonitor searchIndexMonitor;

            SearchHelper searchHelper = new SearchHelper();

            int searchIndexServerStatusId = 0;

            string catalogName = portalIndexModel.CatalogName;

            //Allow to insert only if data does not exists.
            if (portalIndexModel.CatalogIndexId < 1)
            {
                //Check if index name is already used by another store.
                string indexName = _catalogIndexRepository.Table.Where(x => x.IndexName == portalIndexModel.IndexName).Select(s => s.IndexName)?.FirstOrDefault() ?? string.Empty;

                if (!string.IsNullOrEmpty(indexName) || new LoadDefaultData().IsIndexExists(portalIndexModel.IndexName))
                    throw new ZnodeException(ErrorCodes.DuplicateSearchIndexName, Admin_Resources.ErrorIndexNameIsInUse);
                portalIndexModel.CatalogName = catalogName;
                string revisionType = portalIndexModel.RevisionType;
                //Save index name in database.
                portalIndexModel = _catalogIndexRepository.Insert(portalIndexModel.ToEntity<ZnodeCatalogIndex>())?.ToModel<PortalIndexModel>();
                portalIndexModel.RevisionType = revisionType;
                catalogName = portalIndexModel.CatalogName;
                //Create index monitor entry.
                searchIndexMonitor = SearchIndexMonitorInsert(portalIndexModel);
                ZnodeLogging.LogMessage("SearchIndexMonitorId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchIndexMonitor?.SearchIndexMonitorId);

                if (portalIndexModel?.CatalogIndexId > 0 && searchIndexMonitor.SearchIndexMonitorId > 0)
                {
                    portalIndexModel.SearchCreateIndexMonitorId = searchIndexMonitor.SearchIndexMonitorId;

                    ZnodeLogging.LogMessage(Admin_Resources.SuccessSearchIndexCreate, ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);

                    //Start status for creating index for server name saved.   

                    searchIndexServerStatusId = searchHelper.CreateSearchIndexServerStatus(new SearchIndexServerStatusModel()
                    {
                        SearchIndexMonitorId = searchIndexMonitor.SearchIndexMonitorId,
                        ServerName = Environment.MachineName,
                        Status = ZnodeConstant.SearchIndexStartedStatus
                    }).SearchIndexServerStatusId;

                    CallSearchIndexer(portalIndexModel, searchIndexMonitor.CreatedBy, searchIndexServerStatusId);
                    portalIndexModel.StoreName = catalogName;
                    return portalIndexModel;
                }

                ZnodeLogging.LogMessage(string.Format(Admin_Resources.ErrorCreatingLogForIndexCreationForPortalId, portalIndexModel.PortalId), ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            }
            else
                searchIndexMonitor = CreateSearchIndexMonitorEntry(portalIndexModel);

            //Start status for creating index for server name saved.  
            searchIndexServerStatusId = searchHelper.CreateSearchIndexServerStatus(new SearchIndexServerStatusModel()
            {
                SearchIndexMonitorId = searchIndexMonitor.SearchIndexMonitorId,
                ServerName = Environment.MachineName,
                Status = ZnodeConstant.SearchIndexStartedStatus
            }).SearchIndexServerStatusId;


            portalIndexModel.SearchCreateIndexMonitorId = searchIndexMonitor.SearchIndexMonitorId;
            CallSearchIndexer(portalIndexModel, searchIndexMonitor.CreatedBy, searchIndexServerStatusId);
            ZnodeLogging.LogMessage("PortalIndexModel with PortalIndexId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, portalIndexModel?.PortalIndexId);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return portalIndexModel;
        }

        //Create search index  
        public virtual void CreateIndex(string indexName, string revisionType, int catalogId, int searchIndexMonitorId, int searchIndexServerStatusId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage(string.Format(Admin_Resources.IndexingStarted, indexName), ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("revisionType, catalogId, searchIndexMonitorId, searchIndexServerStatusId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { revisionType, catalogId, searchIndexMonitorId, searchIndexServerStatusId });
            try
            {
                long indexstartTime = DateTime.Now.Ticks;

                new LoadDefaultData().IndexingDefaultData(indexName, new SearchParameterModel() { CatalogId = catalogId, IndexStartTime = indexstartTime, SearchIndexMonitorId = searchIndexMonitorId, SearchIndexServerStatusId = searchIndexServerStatusId, revisionType = revisionType, ActiveLocales = GetActiveLocaleList() });
                ZnodeLogging.LogMessage(string.Format(Admin_Resources.IndexingStarted, indexName), ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
                //Delete PIM deleted products from search index.
                DeleteProductData(indexName, revisionType, indexstartTime);
            }
            catch (Exception ex)
            {
                SearchHelper searchHelper = new SearchHelper();
                searchHelper.UpdateSearchIndexServerStatus(new SearchIndexServerStatusModel() { SearchIndexServerStatusId = searchIndexServerStatusId, SearchIndexMonitorId = searchIndexMonitorId, ServerName = Environment.MachineName, Status = ZnodeConstant.SearchIndexFailedStatus });
                ZnodeLogging.LogMessage(string.Format(Admin_Resources.ErrorIndexingForIndex, indexName, ex.Message), ZnodeLogging.Components.Search.ToString(), TraceLevel.Error, ex);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Get list of Create index monitor.
        public virtual SearchIndexMonitorListModel GetSearchIndexMonitorList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            SearchIndexMonitorListModel searchIndexMonitorList;

            sorts = new NameValueCollection();
            sorts.Add(ZnodeSearchIndexMonitorEnum.SearchIndexMonitorId.ToString(), "desc");

            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            IZnodeViewRepository<SearchIndexMonitorModel> objStoredProc = new ZnodeViewRepository<SearchIndexMonitorModel>();

            //SP parameters
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            ZnodeLogging.LogMessage("pageListModel to get serverStatusList: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            IList<SearchIndexMonitorModel> serverStatusList = objStoredProc.ExecuteStoredProcedureList("Znode_GetCreateIndexServerStatus  @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT", 4, out pageListModel.TotalRowCount);
            searchIndexMonitorList = new SearchIndexMonitorListModel { SearchIndexMonitorList = serverStatusList.ToList() };
            searchIndexMonitorList.BindPageListModel(pageListModel);

            ZnodeLogging.LogMessage("searchIndexMonitorList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchIndexMonitorList?.SearchIndexMonitorList?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchIndexMonitorList;
        }

        //Get List of Create Index Server status.
        public virtual SearchIndexServerStatusListModel GetSearchIndexServerStatusList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            SearchIndexServerStatusListModel searchIndexServerStatusList;

            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            ZnodeLogging.LogMessage("pageListModel to get searchIndexServerStatusList: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            List<ZnodeSearchIndexServerStatu> searchIndexServerStatusEntityList = _searchIndexServerStatusRepository.GetPagedList(pageListModel.EntityWhereClause.WhereClause, pageListModel.OrderBy, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount).ToList();

            searchIndexServerStatusList = new SearchIndexServerStatusListModel { SearchIndexServerStatusList = searchIndexServerStatusEntityList.ToModel<SearchIndexServerStatusModel>().ToList() };
            searchIndexServerStatusList.BindPageListModel(pageListModel);

            ZnodeLogging.LogMessage("searchIndexServerStatusList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchIndexServerStatusList?.SearchIndexServerStatusList?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchIndexServerStatusList;
        }

        //Delete unused products from search index.
        public virtual bool DeleteProductData(string indexName, string revisionType, long indexstartTime)
            => new LoadDefaultData().DeleteProductDataByRevisionType(indexName, revisionType, indexstartTime);

        public virtual bool DeleteProduct(string indexName, string znodeProductIds, string revisionType)
             => new LoadDefaultData().DeleteProduct(indexName, znodeProductIds, revisionType);

        public virtual bool DeleteProduct(string indexName, IEnumerable<object> znodeProductIds, string revisionType, string versionId)
              => new LoadDefaultData().DeleteProduct(indexName, znodeProductIds, revisionType, versionId);

        public virtual void CreateProduct(string indexName, List<ProductEntity> productEntities)
            => new LoadDefaultData().CreateDocuments(indexName, productEntities);

        //Create index for category and products.
        public virtual bool CreateIndexForCategoryProducts(CategoryProductSearchParameterModel categoryProductSearchParameterModel)
            => new LoadDefaultData().CreateIndexForCategoryProducts(categoryProductSearchParameterModel);

        public virtual bool DeleteIndex(int catalogIndexId)
        {
            ZnodeLogging.LogMessage("catalogIndexId to delete index: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, catalogIndexId);
            ZnodeCatalogIndex catalogIndex = _catalogIndexRepository.GetById(catalogIndexId);
            return new LoadDefaultData().DeleteIndex(catalogIndex.IndexName);
        }

        public virtual bool RenameIndex(int catalogIndexId, string oldIndexName, string newIndexName)
        {
            ZnodeLogging.LogMessage("Rename index based on : ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { catalogIndexId, oldIndexName, newIndexName });
            return new LoadDefaultData().RenameIndexData(catalogIndexId, oldIndexName, newIndexName);
        }

        //Delete category/document from given index.
        public virtual bool DeleteCategoryForGivenIndex(string indexName, int categoryId)
            => new LoadDefaultData().DeleteCategoryForGivenIndex(indexName, categoryId);

        //Delete catalog category products/documents from given index.
        public virtual bool DeleteCatalogCategoryProducts(string indexName, int publishCatalogId, List<int> publishCategoryIds, string revisionType, string versionId)
            => new LoadDefaultData().DeleteCatalogCategoryProducts(indexName, publishCatalogId, publishCategoryIds, revisionType, versionId);

        #endregion

        #region Search Boosting
        //Saves product, category and field level boost.
        public virtual bool SaveBoostVales(BoostDataModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            bool saveResult = false;
            ZnodeLogging.LogMessage("BoostDataModel to save boost values: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, model);
            switch (model.BoostType.ToLower())
            {
                case ProductType:
                    saveResult = SaveProductBoostValues(model);
                    break;
                case CategoryType:
                    saveResult = SaveProductCategoryBoostValues(model);
                    break;
                case FieldType:
                    saveResult = SaveFieldsBoostValues(model);
                    break;
            }

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return saveResult;
        }

        //Deletes boost value if it is removed.
        public virtual bool DeleteBoostValue(BoostDataModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            bool isDeleted = false;
            ZnodeLogging.LogMessage("BoostDataModel with Id to delete boost value: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, model?.ID);
            if (IsNotNull(model))
            {
                switch (model?.BoostType?.ToLower())
                {
                    case ProductType:
                        isDeleted = DeleteProductBoostValues(model.ID);
                        break;
                    case CategoryType:
                        isDeleted = DeleteProductCategoryBoostValues(model.ID);
                        break;
                    case FieldType:
                        isDeleted = DeleteFieldsBoostValues(model.ID);
                        break;
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return isDeleted;
        }

        //Gets list of global product boost.
        public virtual SearchGlobalProductBoostListModel GetGlobalProductBoostList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            SearchGlobalProductBoostListModel searchGlobalProductBoostList = null;

            SetLocaleFilterIfNotPresent(ref filters);

            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            
            IZnodeViewRepository<SearchGlobalProductBoostModel> objStoredProc = new ZnodeViewRepository<SearchGlobalProductBoostModel>();

            //SP parameters
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            ZnodeLogging.LogMessage("pageListModel to get publishProductList: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            IList<SearchGlobalProductBoostModel> publishProductList = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishProductDetail  @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT", 4, out pageListModel.TotalRowCount);
            searchGlobalProductBoostList = new SearchGlobalProductBoostListModel { SearchGlobalProductBoostList = publishProductList.ToList() };
            searchGlobalProductBoostList.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("publishProductList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, publishProductList?.Count);
            SetProductBoostValue(searchGlobalProductBoostList);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchGlobalProductBoostList;
        }

        //Gets list of category level boost.
        public virtual SearchGlobalProductCategoryBoostListModel GetGlobalProductCategoryBoostList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            SearchGlobalProductCategoryBoostListModel searchGlobalProductCategoryBoostList = null;

            SetLocaleFilterIfNotPresent(ref filters);

            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);


            IZnodeViewRepository<SearchGlobalProductCategoryBoostModel> objStoredProc = new ZnodeViewRepository<SearchGlobalProductCategoryBoostModel>();

            //SP parameters
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            ZnodeLogging.LogMessage("pageListModel to get publishProductCategoryList: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            IList<SearchGlobalProductCategoryBoostModel> publishProductCategoryList = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishCategoryDetail  @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT", 4, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("publishProductCategoryList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, publishProductCategoryList?.Count);
            searchGlobalProductCategoryBoostList = new SearchGlobalProductCategoryBoostListModel { SearchGlobalProductCategoryList = publishProductCategoryList.ToList() };
            searchGlobalProductCategoryBoostList.BindPageListModel(pageListModel);

            if (searchGlobalProductCategoryBoostList.SearchGlobalProductCategoryList.Count > 0)
                SetProductCatgoryBoostValue(searchGlobalProductCategoryBoostList);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchGlobalProductCategoryBoostList;
        }

        //Gets field level boost.
        public virtual SearchDocumentMappingListModel GetFieldBoostList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ReplaceFilterKeys(ref filters);

            int catalogId = Convert.ToInt32(filters.Find(x => x.FilterName.ToLower() == FilterKeys.ZnodeCatalogId.ToLower()).FilterValue);

            for (int index = 0; index < sorts.Keys.Count; index++)
                if (Equals(sorts.Keys.Get(index), "propertyname")) { ReplaceSortKeyName(ref sorts, "propertyname", "AttributeCode"); }

            SearchDocumentMappingListModel listModel = new SearchDocumentMappingListModel();

            filters.Add(new FilterTuple(FilterKeys.IsUseInSearch, FilterOperators.Equals, "true"));

            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            List<string> searchableFields = GetSearchableAttributes(pageListModel);

            //Getting searchable fields with boost values.
            searchableFields.ForEach(field => listModel.SearchDocumentMappingList.Add(new SearchDocumentMappingModel() { PropertyName = field }));

            if (searchableFields.Count > 0)
                SetFieldBoostValues(listModel, searchableFields, catalogId);

            listModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("SearchDocumentMappingList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, listModel?.SearchDocumentMappingList?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return listModel;
        }
        #endregion

        #region Elastic search
        //Get search results.
        public virtual KeywordSearchModel FullTextSearch(SearchRequestModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            IAttributeSwatchHelper attributeSwatchHelper = GetService<IAttributeSwatchHelper>();
            string swatchAttributeCode = attributeSwatchHelper.GetAttributeSwatchCodeExapnds(expands);
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            var searchProvider = ObjectFactory.GetInstance<IZnodeSearchProvider>();

            //Get catalog current version id.
            ZnodeLogging.LogMessage("CatalogId and LocaleIdto get version Id: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { model?.CatalogId, model?.LocaleId });
            int? versionId = GetCatalogVersionId(model.CatalogId, model.LocaleId);
            ZnodeLogging.LogMessage("versionId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, versionId);

            //Add catalog version id to filters.
            filters.Add(WebStoreEnum.VersionId.ToString().ToLower(), FilterOperators.Equals, Convert.ToString(versionId));
            int publishCatalogId = Convert.ToInt32(filters.FirstOrDefault(m => string.Compare(m.FilterName, WebStoreEnum.ZnodeCatalogId.ToString(), true) == 0)?.Item3);
            bool isAllowIndexing = IsAllowIndexing(publishCatalogId);

            IZnodeSearchRequest searchRequest = GetZnodeSearchRequest(model, filters, sorts,false,isAllowIndexing);

            GetFacetExpands(expands, searchRequest);

            IZnodeSearchResponse searchResponse = null;

            //if it is getting list of facets and category id is also 0 then add product index filter in search filter.
            if (model.CategoryId < 1)
                searchRequest.CatalogIdLocalIdDictionary.Add("productindex", new List<string>() { "1" });

            string sortName = searchRequest.SortCriteria.FirstOrDefault()?.SortName.ToString()?.ToLower();

            //Method to get search response.
            searchResponse = GetSearchResponse(model, expands, ref sorts, searchProvider, searchRequest, sortName);

            //Log the keyword searched.
            if (!string.IsNullOrEmpty(model.Keyword) && !model.IsFacetList)
                ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.KeywordSearch, model.Keyword.ToLower());

            //If suggestions are fetched when product count is 0.
            GetResultFromSuggestions(model, filters, sorts, searchProvider, ref searchRequest, ref searchResponse);

            //Converts search response to keyword search model.
            KeywordSearchModel searchResult = IsNotNull(searchResponse) ? GetKeywordSearchModel(searchResponse) : new KeywordSearchModel();

            if (!isAllowIndexing)
                BindProductDetails(model, expands, searchResponse, searchResult);

            if (IsNotNull(searchResult.Products) && !string.IsNullOrEmpty(swatchAttributeCode))
                attributeSwatchHelper.GetAssociatedConfigurableProducts(searchResult, model, swatchAttributeCode);


            ZnodeLogging.LogMessage(
                model.IsFacetList
                    ? $"Facet Query:{searchResponse.RequestBody} and productCount {searchResult.Products?.Count}"
                    : $"FullTextQuery:{searchResponse.RequestBody}and productCount {searchResult.Products?.Count}", ZnodeLogging.Components.Search.ToString(),
                TraceLevel.Info);

            ZnodeLogging.LogObject(typeof(KeywordSearchModel), searchResult, "searchResult");
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchResult;
        }

        public void BindProductDetails(SearchRequestModel model, NameValueCollection expands, IZnodeSearchResponse searchResponse, KeywordSearchModel searchResult)
        {
            //Get required input parameters to get the data of products.
            DataTable productDetails = IsNotNull(searchResult.Products) ? GetProductFiltersForSP(searchResponse.ProductDetails) : null;


            GetExpands(model, expands, searchResult, productDetails);
        }

        public bool IsAllowIndexing(int publishCatalogId)
        {
            IZnodeRepository<ZnodePimCatalog> _pimCatalogRepository = new ZnodeRepository<ZnodePimCatalog>();
            IZnodeRepository<ZnodePublishCatalog> _publishCatalogRepository = new ZnodeRepository<ZnodePublishCatalog>();
            int pimCatalogId = _publishCatalogRepository.Table.FirstOrDefault(m => m.PublishCatalogId == publishCatalogId)?.PimCatalogId ?? 0;
            if (pimCatalogId > 0)
                return _pimCatalogRepository.Table.FirstOrDefault(n => n.PimCatalogId == pimCatalogId)?.IsAllowIndexing ?? false;
            return false;
        }

        //Gets facet search result.
        public virtual KeywordSearchModel FacetSearch(SearchRequestModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            var searchProvider = ObjectFactory.GetInstance<IZnodeSearchProvider>();

            ValidateCatalogIdAndLocaleId(model);

            IZnodeSearchRequest searchRequest = GetZnodeSearchRequest(model, filters, sorts);

            IZnodeSearchResponse searchResponse = null;

            //if it is getting list of facets and category id is also 0 then add product index filter in search filter.
            if (!model.IsFacetList && model.CategoryId < 1)
                searchRequest.CatalogIdLocalIdDictionary.Add("productindex", new List<string>() { "1" });

            string sortName = searchRequest.SortCriteria.FirstOrDefault()?.SortName.ToString()?.ToLower();

            //Method to get search response.
            searchResponse = GetSearchResponse(model, expands, ref sorts, searchProvider, searchRequest, sortName, true);

            //Log the keyword searched.
            if (!string.IsNullOrEmpty(model.Keyword) && !model.IsFacetList)
                ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.KeywordSearch, model.Keyword.ToLower());

            //If suggestions are fetched when product count is 0.
            GetResultFromSuggestions(model, filters, sorts, searchProvider, ref searchRequest, ref searchResponse);

            //Converts search response to keyword search model.
            KeywordSearchModel searchResult = IsNotNull(searchResponse) ? GetKeywordSearchModel(searchResponse) : new KeywordSearchModel();

            //get expands associated to Product if the call is not for facets.
            if (!model.IsFacetList)
            {
                GetExpands(model, expands, searchResult);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchResult;
        }

        public KeywordSearchModel GetSearchProfileProducts(SearchProfileModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            IZnodeRepository<ZnodeSearchQueryType> _searchQueryType = new ZnodeRepository<ZnodeSearchQueryType>();

            List<ZnodeSearchQueryType> queryType = _searchQueryType.Table.Where(x => x.SearchQueryTypeId == model.SearchQueryTypeId || x.SearchQueryTypeId == model.SearchSubQueryTypeId)?.ToList();
            ZnodeLogging.LogMessage("queryType list count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, queryType?.Count);
            var searchProvider = ObjectFactory.GetInstance<IZnodeSearchProvider>();

            IZnodeSearchRequest searchRequest = GetZnodeSearchProfileRequest(model, queryType);
            IZnodeSearchResponse searchResponse = null;
            searchResponse = searchProvider.FullTextSearch(searchRequest);
            //Converts search response to keyword search model.
            KeywordSearchModel searchResult = IsNotNull(searchResponse) ? GetKeywordSearchModel(searchResponse) : new KeywordSearchModel();

            MediaConfigurationModel configurationModel = new MediaConfigurationService().GetDefaultMediaConfiguration();
            string serverPath = GetMediaServerUrl(configurationModel);

            searchResult.Products?.ForEach(
                  x =>
                  {
                      string imageName = x.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues;
                      x.ImageSmallPath = $"{serverPath}Thumbnail/{imageName}";
                  });

            ZnodeLogging.LogMessage(string.Format(Admin_Resources.FullTextQuery, model.QueryTypeName, searchResponse.RequestBody), ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);

            return searchResult;
        }

        private IZnodeSearchRequest GetZnodeSearchProfileRequest(SearchProfileModel model, List<ZnodeSearchQueryType> queryType)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            if (IsNull(model))
                return null;

            var searchRequest = ObjectFactory.Container.GetInstance<IZnodeSearchRequest>();
            searchRequest.SearchText = model.SearchText;
            searchRequest.IndexName = GetCatalogIndexName(model.PublishCatalogId);
            searchRequest.CatalogId = model.PublishCatalogId;
            searchRequest.LocaleId = 1;
            searchRequest.PageFrom = 1;
            searchRequest.PageSize = 200;
            searchRequest.QueryTypeName = queryType.FirstOrDefault(x => x.SearchQueryTypeId == model.SearchQueryTypeId)?.QueryTypeName;
            searchRequest.SubQueryType = queryType.FirstOrDefault(x => x.SearchQueryTypeId == model.SearchSubQueryTypeId)?.QueryTypeName;
            searchRequest.QueryClass = queryType.FirstOrDefault(x => x.SearchQueryTypeId == model.SearchQueryTypeId)?.QueryBuilderClassName;
            searchRequest.Operator = model.Operator;
            Dictionary<string, List<string>> filterAndClause = new Dictionary<string, List<string>>();

            //Default filters for search  
            if (model.PublishCatalogId > 0) filterAndClause.Add("znodecatalogid", new List<string>() { model.PublishCatalogId.ToString() });
            filterAndClause.Add("localeid", new List<string>() { "1" });
            filterAndClause.Add("isactive", new List<string>() { "true" });
            filterAndClause.Add("productindex", new List<string>() { "1" });
            filterAndClause.Add("versionid", new List<string>() { Convert.ToString(GetCatalogVersionId(model.PublishCatalogId, ZnodePublishStatesEnum.PRODUCTION)) });

            searchRequest.CatalogIdLocalIdDictionary = filterAndClause;
            searchRequest.SearchableAttibute = GetSearableAttributeofProfile(model.SearchableAttributesList);
            searchRequest.FeatureList = MapSearchProfileFeature(model.FeaturesList); 
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchRequest;
        }

        public List<ElasticSearchAttributes> GetSearableAttributeofProfile(List<SearchAttributesModel> searchableAttributesList)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            List<ElasticSearchAttributes> list = new List<ElasticSearchAttributes>();
            if (searchableAttributesList?.Count > 0)
            {
                foreach (var test in searchableAttributesList)
                    list.Add(new ElasticSearchAttributes { AttributeCode = test.AttributeCode, BoostValue = test.BoostValue });
            }
            ZnodeLogging.LogMessage("ElasticSearchAttributes list count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, list?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return list;
        }

        //Gets search suggestions for a keyword.
        public virtual KeywordSearchModel GetKeywordSearchSuggestion(SearchRequestModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            var searchProvider = ObjectFactory.GetInstance<IZnodeSearchProvider>();

            ZnodeLogging.LogMessage("CatalogId and LocaleId to get version Id: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { model?.CatalogId, model?.LocaleId });
            int? versionId = GetCatalogVersionId(model.CatalogId, model.LocaleId);
            ZnodeLogging.LogMessage("versionId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, versionId);
            filters.Add(WebStoreEnum.VersionId.ToString().ToLower(), FilterOperators.Equals, Convert.ToString(versionId));

            var searchRequest = GetZnodeSearchRequest(model, filters, sorts, true);
            searchRequest.GetFacets = false;

            IZnodeSearchResponse suggestions = searchProvider.SuggestTermsFor(searchRequest);

            KeywordSearchModel searchResult = IsNotNull(suggestions) ? GetKeywordSearchModel(suggestions) : new KeywordSearchModel();
            if (!model.IsAutocomplete && IsNotNull(searchResult))
            {
                if (searchResult.Products?.Count > 0)
                    //get expands associated to Product
                    publishProductHelper.GetDataFromExpands(model.PortalId, GetExpands(expands), searchResult.Products, model.LocaleId, GetLoginUserId(), versionId ?? 0, GetProfileId());

                ZnodeLogging.LogMessage("portalId and productList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { model?.PortalId, searchResult?.Products?.Count });
                GetProductImagePathForSuggestions(model.PortalId, searchResult.Products);
                //set stored based In Stock, Out Of Stock, Back Order Message.
                SetPortalBasedDetails(model.PortalId, searchResult.Products);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchResult;
        }

        #endregion

        //Get Seo Url details.
        public virtual SEOUrlModel GetSEOUrlDetails(string seoUrl, FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);

            int portalId;
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.PortalId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out portalId);

            int localeId;
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(ZnodeLocaleEnum.LocaleId.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out localeId);

            int publishCatalogId;
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(ZnodePimCatalogEnum.PimCatalogId.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out publishCatalogId);

            int? catalogVersionId = GetCatalogVersionId(publishCatalogId);
            filters.Add(new FilterTuple(FilterKeys.VersionId, FilterOperators.Equals, Convert.ToString(catalogVersionId)));

            if (localeId <= 0)
            {
                IZnodeRepository<ZnodePortalLocale> _portalLocaleRepository = new ZnodeRepository<ZnodePortalLocale>();
                localeId = (_portalLocaleRepository.Table.FirstOrDefault(x => x.PortalId == portalId)?.LocaleId).GetValueOrDefault();
                filters.RemoveAll(x => x.FilterName.Equals(ZnodeLocaleEnum.LocaleId.ToString(), StringComparison.InvariantCultureIgnoreCase));
                filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, localeId.ToString());
            }

            //Remove portal id filter.
            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

            // Bind SEO type Name and its SeoId.
            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query.And(Query<SeoEntity>.EQ(pr => pr.SEOUrl, seoUrl.ToLower())));
            query.Add(Query.Or(Query<SeoEntity>.EQ(pr => pr.PortalId, portalId), Query<SeoEntity>.EQ(pr => pr.PortalId, null)));

            ZnodeLogging.LogMessage("query to get SEO URL model: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, query);

            SEOUrlModel model = _seoProductMongoRepository.GetEntity(Query.And(query)).ToModel<SEOUrlModel>();

            if (IsNull(model))
                model = _seoCMSMongoRepository.GetEntity(Query.And(query)).ToModel<SEOUrlModel>();

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return (IsNotNull(model?.SeoCode) && !string.IsNullOrEmpty(model?.Name)) ? GetSEOTypes(filters, model) : new SEOUrlModel();
        }

        //Gets expands for products.
        public virtual void GetExpands(SearchRequestModel model, NameValueCollection expands, KeywordSearchModel searchResult, DataTable tableDetails = null)
        {
            if (searchResult?.Products?.Count > 0)
            {
                int? versionId = GetCatalogVersionId(model.CatalogId, model.LocaleId);
                publishProductHelper.GetDataFromExpands(model.PortalId, GetExpands(expands), searchResult.Products, model.LocaleId, GetLoginUserId(), versionId ?? 0, GetProfileId());
                GetRequiredProductDetails(searchResult, tableDetails, GetLoginUserId(), model.PortalId);
            }
        }

        //Get expands and add them to navigation properties
        public List<string> GetExpands(NameValueCollection expands)
        {
            List<string> navigationProperties = new List<string>();
            if (IsNotNull(expands) && expands.HasKeys())
            {
                foreach (string key in expands.Keys)
                {
                    //check if expand key is present or not and add it to navigation properties.
                    if (Equals(key, ZnodeConstant.Promotions)) SetExpands(ZnodeConstant.Promotions, navigationProperties);
                    if (Equals(key, ZnodeConstant.Inventory)) SetExpands(ZnodeConstant.Inventory, navigationProperties);
                    if (Equals(key, ZnodeConstant.ProductTemplate)) SetExpands(ZnodeConstant.ProductTemplate, navigationProperties);
                    if (Equals(key, ZnodeConstant.ProductReviews)) SetExpands(ZnodeConstant.ProductReviews, navigationProperties);
                    if (Equals(key, ZnodeConstant.Pricing)) SetExpands(ZnodeConstant.Pricing, navigationProperties);
                    if (Equals(key, ZnodeConstant.SEO)) SetExpands(ZnodeConstant.SEO, navigationProperties);
                    if (Equals(key, ZnodeConstant.AddOns)) SetExpands(ZnodeConstant.AddOns, navigationProperties);
                    if (Equals(key, ZnodeConstant.ConfigurableAttribute)) SetExpands(ZnodeConstant.ConfigurableAttribute, navigationProperties);
                    if (string.Equals(key, ZnodePortalUnitEnum.ZnodePortal.ToString(), StringComparison.CurrentCultureIgnoreCase)) SetExpands(ZnodePortalUnitEnum.ZnodePortal.ToString(), navigationProperties);
                    if (string.Equals(key, ZnodeCatalogIndexEnum.ZnodePublishCatalog.ToString(), StringComparison.CurrentCultureIgnoreCase)) SetExpands(ZnodeCatalogIndexEnum.ZnodePublishCatalog.ToString(), navigationProperties);
                    if (Equals(key, ZnodeConstant.AssociatedProducts)) SetExpands(ZnodeConstant.AssociatedProducts, navigationProperties);

                }
            }
            return navigationProperties;
        }

        //Converts a strings first character to lower-case.
        public virtual string FirstCharToLower(string input)
        {
            if (!string.IsNullOrEmpty(input))
                input = input.First().ToString().ToLower() + input.Substring(1);
            return input;
        }

        //Get all Attributes Codes where IsSearchable Flag is true From Mongo Db
        private List<string> GetSearchableAttributes(PageListModel pageListModel)
        {
            List<string> catalogAttributes = new MongoRepository<CatalogAttributeEntity>()
                                        .GetPagedList(pageListModel.MongoWhereClause, pageListModel.MongoOrderBy, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount)?.Select(x => x.AttributeCode)
                                        ?.Distinct()?.ToList();
            if (IsNotNull(catalogAttributes))
                pageListModel.TotalRowCount = catalogAttributes.Count;
            ZnodeLogging.LogMessage("catalogAttributes list: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, catalogAttributes);
            return catalogAttributes;
        }

        #region Synonyms
        //Create synonyms for serach.
        public virtual SearchSynonymsModel CreateSearchSynonyms(SearchSynonymsModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            //Check whether the model is null or not.
            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorSearchSynonymsModelNull);

            //Save data into synonyms table and maps updated values in model.
            ZnodeSearchSynonym entity = _searchSynonymsRepository.Insert(model.ToEntity<ZnodeSearchSynonym>());

            if (IsNotNull(entity))
            {
                ZnodeLogging.LogMessage(string.Format(Admin_Resources.SuccessSynonymsWithIdCreate, model.SearchSynonymsId), ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
                return entity.ToModel<SearchSynonymsModel>();
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage(Admin_Resources.ErrorSynonymsCreate, ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return model;
        }

        //Get synonyms id for search.
        public virtual SearchSynonymsModel GetSearchSynonyms(int searchSynonymsId, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            if (searchSynonymsId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorSearchSynonymsIdGreaterThanOne);

            //Get search synonyms entity data & maps it into search synonyms model.
            SearchSynonymsModel searchSynonymsModel = _searchSynonymsRepository.GetById(searchSynonymsId)?.ToModel<SearchSynonymsModel>();

            ZnodeLogging.LogMessage("searchSynonymsModel: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchSynonymsModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchSynonymsModel;
        }

        //Update search synonyms data for serach.
        public virtual bool UpdateSearchSynonyms(SearchSynonymsModel searchSynonymsModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            if (IsNull(searchSynonymsModel))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorSearchSynonymsModelNull);

            if (searchSynonymsModel.SearchSynonymsId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.IdCanNotBeLessThanOne);

            ZnodeLogging.LogMessage("searchSynonymsModel to be updated: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchSynonymsModel);
            bool isUpdated = _searchSynonymsRepository.Update(searchSynonymsModel.ToEntity<ZnodeSearchSynonym>());

            //Returns true if data updated successfully.
            if (isUpdated)
            {
                ZnodeLogging.LogMessage(Admin_Resources.SuccessSearchSynonymsDataUpdate, ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
                return true;
            }
            ZnodeLogging.LogMessage(Admin_Resources.ErrorSearchSynonymsDataUpdate, ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return false;
        }

        //Get list of synonyms for search.
        public virtual SearchSynonymsListModel GetSearchSynonymsList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            ZnodeLogging.LogMessage("pageListModel to get searchSynonymsEntityList: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            List<ZnodeSearchSynonym> searchSynonymsEntityList = _searchSynonymsRepository.GetPagedList(pageListModel.EntityWhereClause.WhereClause, pageListModel.OrderBy, pageListModel.EntityWhereClause.FilterValues, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount)?.ToList();
            ZnodeLogging.LogMessage("searchSynonymsEntityList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchSynonymsEntityList?.Count);

            SearchSynonymsListModel searchSynonymsList = searchSynonymsEntityList?.Count > 0 ? new SearchSynonymsListModel { SynonymsList = searchSynonymsEntityList.ToModel<SearchSynonymsModel>()?.ToList() } : new SearchSynonymsListModel { SynonymsList = new List<SearchSynonymsModel>() };
            searchSynonymsList.BindPageListModel(pageListModel);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchSynonymsList;
        }

        //Delete synonyms by id.
        public virtual bool DeleteSearchSynonyms(ParameterModel searchSynonymsIds)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            //Check synonyms ids.
            if (string.IsNullOrEmpty(searchSynonymsIds?.Ids))
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorSynonymsIdLessThanOne);

            //Generates filter clause for multiple synonyms ids.
            FilterCollection filter = new FilterCollection();
            filter.Add(new FilterTuple(ZnodeSearchSynonymEnum.SearchSynonymsId.ToString(), ProcedureFilterOperators.In, searchSynonymsIds.Ids));

            //Returns true if synonyms deleted successfully else return false.
            ZnodeLogging.LogMessage("searchSynonymsIds to be deleted: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchSynonymsIds?.Ids);
            bool isDeleted = _searchSynonymsRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection()).WhereClause);
            ZnodeLogging.LogMessage(isDeleted ? Admin_Resources.SuccessSynonymsDelete : Admin_Resources.ErrorSynonymsDelete, ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return isDeleted;
        }

        //Write synonyms.txt for search.
        public bool WriteSearchFile(int publishCatalogId, bool isSynonymsFile)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            if (publishCatalogId > 0)
            {
                string indexName = _catalogIndexRepository.Table.FirstOrDefault(x => x.PublishCatalogId == publishCatalogId)?.IndexName;

                if (string.IsNullOrEmpty(indexName))
                    throw new ZnodeException(ErrorCodes.NotFound, string.Format(Admin_Resources.ErrorIndexExistForCatalogId, publishCatalogId));

                ZnodeLogging.LogMessage("publishCatalogId, indexName, isSynonymsFile to write sysnonyms file: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { isSynonymsFile, indexName, isSynonymsFile });
                return new LoadDefaultData().WriteSynonymsFile(publishCatalogId, indexName, isSynonymsFile);
            }
            return false;
        }
        #endregion

        #region Search Keywords Redirect
        //Get catalog keywords redirect list.
        public virtual SearchKeywordsRedirectListModel GetCatalogKeywordsRedirectList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            ZnodeLogging.LogMessage("pageListModel to keywordsEntityList: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            List<ZnodeSearchKeywordsRedirect> keywordsEntityList = _searchKeywordsRedirectRepository.GetPagedList(pageListModel.EntityWhereClause.WhereClause, pageListModel.OrderBy, pageListModel.EntityWhereClause.FilterValues, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount)?.ToList();
            ZnodeLogging.LogMessage("keywordsEntityList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, keywordsEntityList?.Count);

            SearchKeywordsRedirectListModel searchKeywordsRedirectList = keywordsEntityList?.Count > 0 ? new SearchKeywordsRedirectListModel { KeywordsList = keywordsEntityList.ToModel<SearchKeywordsRedirectModel>()?.ToList() } : new SearchKeywordsRedirectListModel { KeywordsList = new List<SearchKeywordsRedirectModel>() };

            searchKeywordsRedirectList.BindPageListModel(pageListModel);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchKeywordsRedirectList;
        }

        // Creates keywords and its redirected url for search.
        public virtual SearchKeywordsRedirectModel CreateSearchKeywordsRedirect(SearchKeywordsRedirectModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            //Check whether the model is null or not.
            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorSearchKeywordModelNull);

            if (IsNull(model.PublishCatalogId) || model.PublishCatalogId == 0)
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.PublishCatalogIdLessThanOne);

            //Save data into synonyms table and maps updated values in model.
            ZnodeSearchKeywordsRedirect entity = _searchKeywordsRedirectRepository.Insert(model.ToEntity<ZnodeSearchKeywordsRedirect>());

            if (IsNotNull(entity))
            {
                ZnodeLogging.LogMessage(string.Format(Admin_Resources.SuccessKeywordsWithIdCreate, model.SearchKeywordsRedirectId), ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
                return entity.ToModel<SearchKeywordsRedirectModel>();
            }
            ZnodeLogging.LogMessage(Admin_Resources.ErrorKeywordsCreate, ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return model;
        }

        //Get keywords details for search.
        public virtual SearchKeywordsRedirectModel GetSearchKeywordsRedirect(int searchKeywordsRedirectId, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            if (searchKeywordsRedirectId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorSearchKeywordsIdGreaterThanOne);

            //Get search keywords entity data & maps it into search keywords model.
            ZnodeLogging.LogMessage("searchKeywordsRedirectId to get searchKeywordsRedirectModel: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchKeywordsRedirectId);
            SearchKeywordsRedirectModel searchKeywordsRedirectModel = _searchKeywordsRedirectRepository.GetById(searchKeywordsRedirectId)?.ToModel<SearchKeywordsRedirectModel>();

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchKeywordsRedirectModel;
        }

        //Update keywords for search.
        public virtual bool UpdateSearchKeywordsRedirect(SearchKeywordsRedirectModel searchKeywordsModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            if (IsNull(searchKeywordsModel))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorSearchKeywordModelNull);

            if (searchKeywordsModel.SearchKeywordsRedirectId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.IdCanNotBeLessThanOne);

            ZnodeLogging.LogMessage("searchKeywordsModel with Id to be updated: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchKeywordsModel?.SearchKeywordsRedirectId);
            bool isUpdated = _searchKeywordsRedirectRepository.Update(searchKeywordsModel.ToEntity<ZnodeSearchKeywordsRedirect>());

            //Returns true if data updated successfully.
            if (isUpdated)
            {
                ZnodeLogging.LogMessage(Admin_Resources.SuccessSearchKeywordsDataUpdate, ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
                return true;
            }
            ZnodeLogging.LogMessage(Admin_Resources.ErrorSearchKeywordsDataUpdate, ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return false;
        }

        //Delete keywords by ids.
        public virtual bool DeleteSearchKeywordsRedirect(ParameterModel searchKeywordsRedirectIds)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            //Check keywords ids.
            if (string.IsNullOrEmpty(searchKeywordsRedirectIds?.Ids))
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorKeywordsIdLessThanOne);

            //Generates filter clause for multiple keywords ids.
            FilterCollection filter = new FilterCollection();
            filter.Add(new FilterTuple(ZnodeSearchKeywordsRedirectEnum.SearchKeywordsRedirectId.ToString(), ProcedureFilterOperators.In, searchKeywordsRedirectIds.Ids));

            //Returns true if keywords deleted successfully else return false.
            ZnodeLogging.LogMessage("searchKeywordsRedirectIds to be deleted: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchKeywordsRedirectIds?.Ids);
            bool isDeleted = _searchKeywordsRedirectRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection()).WhereClause);
            ZnodeLogging.LogMessage(isDeleted ? Admin_Resources.SuccessKeywordsDelete : Admin_Resources.ErrorKeywordsDelete, ZnodeLogging.Components.Search.ToString());
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return isDeleted;
        }
        #endregion
        #endregion

        #region Private Methods
        //Get required input parameters to get the data of products.
        public virtual DataTable GetProductFiltersForSP(List<dynamic> products)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("products list count to get product filters for SP: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, products?.Count);
            DataTable table = new DataTable("ProductTable");
            DataColumn productId = new DataColumn("Id") { DataType = typeof(int), AllowDBNull = false };
            table.Columns.Add(productId);
            table.Columns.Add("ProductType", typeof(string));
            table.Columns.Add("OutOfStockOptions", typeof(string));
            table.Columns.Add("SKU", typeof(string));

            foreach (var item in products)
                table.Rows.Add(Convert.ToInt32(item["znodeproductid"]), Convert.ToString(item["producttype"]), Convert.ToString(item["outofstockoptions"]), Convert.ToString(item["sku"]));

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return table;
        }

        //Get details of category products.
        private void GetRequiredProductDetails(KeywordSearchModel searchResult, DataTable tableDetails, int userId = 0, int portalId = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            IZnodeViewRepository<PublishCategoryProductDetailModel> objStoredProc = new ZnodeViewRepository<PublishCategoryProductDetailModel>();
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@LocaleId", Convert.ToInt32(DefaultGlobalConfigSettingHelper.Locale), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@UserId", userId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@currentUtcDate", HelperUtility.GetDateTime().Date, ParameterDirection.Input, DbType.String);
            IList<PublishCategoryProductDetailModel> productDetails = null;

            ZnodeLogging.LogMessage("userId and portalId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { userId, portalId });

            if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
            {
                objStoredProc.SetParameter("@ProductDetailsFromWebStore", tableDetails?.ToJson(), ParameterDirection.Input, DbType.String);
                //Gets the entity list according to where clause, order by clause and pagination
                productDetails = objStoredProc.ExecuteStoredProcedureList("Znode_GetProductInfoForWebStoreWithJSON @PortalId,@LocaleId,@UserId,@ProductDetailsFromWebStore,@currentUtcDate");
            }
            else
            {
                objStoredProc.SetTableValueParameter("@ProductDetailsFromWebStore", tableDetails, ParameterDirection.Input, SqlDbType.Structured, "dbo.ProductDetailsFromWebStore");
                //Gets the entity list according to where clause, order by clause and pagination
                productDetails = objStoredProc.ExecuteStoredProcedureList("Znode_GetProductInfoForWebStore @PortalId,@LocaleId,@UserId,@ProductDetailsFromWebStore,@currentUtcDate");
            }
            //Bind product details.
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            BindProductDetails(searchResult, portalId, productDetails);
        }

        //Bind product details.
        public virtual void BindProductDetails(KeywordSearchModel searchResult, int portalId, IList<PublishCategoryProductDetailModel> productDetails)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            
            IImageHelper imageHelper = GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", portalId));

            ZnodeLogging.LogMessage("portalId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, portalId);

            searchResult?.Products?.ForEach(product =>
            {
                PublishCategoryProductDetailModel productSKU = productDetails?
                            .FirstOrDefault(productdata => productdata.SKU == product.SKU);

                if (IsNotNull(productSKU))
                {
                    product.SalesPrice = productSKU.SalesPrice;
                    product.RetailPrice = productSKU.RetailPrice;
                    product.CurrencyCode = productSKU.CurrencyCode;
                    product.CultureCode = productSKU.CultureCode;
                    product.CurrencySuffix = productSKU.CurrencySuffix;
                    product.Quantity = productSKU.Quantity;
                    product.ReOrderLevel = productSKU.ReOrderLevel;
                    product.Rating = productSKU.Rating;
                    product.TotalReviews = productSKU.TotalReviews;
                    product.ImageSmallPath = imageHelper.GetImageHttpPathSmall(product.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues);
                }
            });
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Get the type of SEO whether Product, Category or Content Page.
        private SEOUrlModel GetSEOTypes(FilterCollection filters, SEOUrlModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            //Get the type of SEO whether Product, Category or Content Page.
            switch (model.Name)
            {
                case ZnodeConstant.Product:
                    ProductSeoType(filters, model);
                    break;
                case ZnodeConstant.Category:
                    CategorySeoType(filters, model);
                    break;
                case ZnodeConstant.ContentPage:
                    ContentPageSeoType(model);
                    break;
                case ZnodeConstant.Brand:
                    BrandSeoType(model);
                    break;
                case ZnodeConstant.BlogNews:
                    BlogNewsSeoType(model);
                    break;
                default:
                    break;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return model;
        }

        private void BrandSeoType(SEOUrlModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            IZnodeRepository<ZnodeBrandDetail> _brandRepository = new ZnodeRepository<ZnodeBrandDetail>();
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodeBrandDetailEnum.BrandCode.ToString(), FilterOperators.Is, model.SeoCode.ToString()));

            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());

            ZnodeLogging.LogMessage("WhereClause to get brand details: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, whereClauseModel?.WhereClause);
            ZnodeBrandDetail brand = _brandRepository.GetEntity(whereClauseModel.WhereClause, whereClauseModel.FilterValues);

            if (IsNotNull(brand))
            {
                model.BrandId = brand.BrandId;
                model.BrandName = brand.BrandCode;
                model.IsActive = brand.IsActive;
                model.SEOId = brand.BrandId;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Get BlogNews Id if Seo belongs to BlogNews.
        private void BlogNewsSeoType(SEOUrlModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            IZnodeRepository<ZnodeBlogNew> _blogNewsRepository = new ZnodeRepository<ZnodeBlogNew>();
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodeBlogNewEnum.BlogNewsCode.ToString(), FilterOperators.Is, model.SeoCode));
            var whereClause = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
            ZnodeLogging.LogMessage("whereClause to get blog news: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, whereClause?.WhereClause);
            ZnodeBlogNew blogNews = _blogNewsRepository.GetEntity(whereClause.WhereClause, whereClause.FilterValues);
            if (IsNotNull(blogNews))
            {
                model.BrandId = blogNews.BlogNewsId;
                model.IsActive = blogNews.IsBlogNewsActive.Value;
                model.SEOId = blogNews.BlogNewsId;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Get ContentPage Id and ContentPage Name if Seo belongs to ContentPage.
        private void ContentPageSeoType(SEOUrlModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            List<IMongoQuery> query = new List<IMongoQuery>();
            ZnodeLogging.LogMessage("SeoCode and PortalId to generate query: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { model?.SeoCode, PortalId });
            query.Add(Query.And(Query<ContentPageConfigEntity>.EQ(pr => pr.PageName, model.SeoCode)));
            query.Add(Query.And(Query<ContentPageConfigEntity>.EQ(pr => pr.PortalId, PortalId)));

            ContentPageConfigEntity content = _contentMongoRepository.GetEntity(Query.And(query));

            if (IsNotNull(content))
            {
                model.ContentPageId = content.ContentPageId;
                model.ContentPageName = content.PageName;
                model.IsActive = true;
                model.SEOId = content.ContentPageId;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Get Category Id and Category Name if Seo belongs to Category.
        private void CategorySeoType(FilterCollection filters, SEOUrlModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            string localeId = filters.Find(x => x.FilterName.ToLower() == ZnodeLocaleEnum.LocaleId.ToString().ToLower())?.FilterValue;
            string catalogId = filters.Find(x => x.FilterName.ToLower() == ZnodePimCatalogEnum.PimCatalogId.ToString().ToLower())?.FilterValue;

            ZnodeLogging.LogMessage("localeId and catalogId to create query: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { localeId, catalogId });
            List<IMongoQuery> mongoQuery = new List<IMongoQuery>();
            mongoQuery.Add(Query.And(Query<CategoryEntity>.EQ(pr => pr.CategoryCode, model.SeoCode)));

            mongoQuery.Add(Query.And(Query<CategoryEntity>.EQ(pr => pr.LocaleId, int.Parse(localeId))));
            mongoQuery.Add(Query.And(Query<CategoryEntity>.EQ(pr => pr.ZnodeCatalogId, int.Parse(catalogId))));

            filters.Add(new FilterTuple(FilterKeys.ZnodeCategoryId, FilterOperators.Equals, model.SEOId.ToString()));
            ReplaceFilterKeys(ref filters);
            ZnodeLogging.LogMessage("mongoQuery to get category entity: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, mongoQuery);
            CategoryEntity category = _categoryMongoRepository.GetEntity(Query.And(mongoQuery), true);

            if (IsNotNull(category))
            {
                model.CategoryId = category.ZnodeCategoryId;
                model.CategoryName = category.Name;

                //Make category De-Active,if category is De-Active./ActivationDate is greater than current date./ExpirationDate is less than current date.
                if (category.IsActive &&
                (category.ActivationDate == null || category.ActivationDate.GetValueOrDefault().Date <= HelperUtility.GetDate())
                  && (category.ExpirationDate == null || category.ExpirationDate.GetValueOrDefault().Date >= HelperUtility.GetDate()))
                {
                    model.IsActive = category.IsActive;
                }
                else
                    model.IsActive = false;

                model.SEOId = category.ZnodeCategoryId;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Replaces filter key.
        private void ReplaceFilterKeys(ref FilterCollection filters)
        {
            foreach (FilterTuple tuple in filters)
            {
                if (string.Equals(tuple.Item1, FilterKeys.LocaleId, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.LocaleId, FilterKeys.MongoLocaleId); }
                if (string.Equals(tuple.Item1, FilterKeys.ZnodeCatalogId, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.ZnodeCatalogId.ToLower(), FilterKeys.ZnodeCatalogId); }
                if (string.Equals(tuple.Item1, FilterKeys.CatalogId, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.CatalogId.ToLower(), FilterKeys.ZnodeCatalogId); }
                if (string.Equals(tuple.Item1, ZnodeSearchDocumentMappingEnum.PublishCatalogId.ToString(), StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, ZnodeSearchDocumentMappingEnum.PublishCatalogId.ToString().ToLower(), FilterKeys.ZnodeCatalogId); }
                if (string.Equals(tuple.Item1, FilterKeys.PropertyName, StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, FilterKeys.PropertyName, FilterKeys.AttributeCode); }
                if (string.Equals(tuple.Item1, $"{FilterKeys.PropertyName}|", StringComparison.OrdinalIgnoreCase)) { ReplaceFilterKeyName(ref filters, $"{FilterKeys.PropertyName}|", FilterKeys.AttributeCode); }
            }
        }

        //Get Product Id and Product Name if Seo belongs to Product.
        private void ProductSeoType(FilterCollection filters, SEOUrlModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("SeoCode to set filters: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, model?.SeoCode);
            string catalogId = filters.Find(x => x.FilterName.ToLower() == ZnodePimCatalogEnum.PimCatalogId.ToString().ToLower())?.FilterValue;

            filters.Add(new FilterTuple(FilterKeys.SKU, FilterOperators.Equals, model.SeoCode));
            filters.Add(new FilterTuple(FilterKeys.VersionId, FilterOperators.Equals, GetCatalogVersionId(Convert.ToInt32(catalogId)).ToString()));
            ReplaceFilterKeys(ref filters);
            ProductEntity product = _productMongoRepository.GetEntity(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()), true);
            if (IsNotNull(product))
            {
                model.ProductId = product.ZnodeProductId;
                model.ProductName = product.Name;
                model.IsActive = product.IsActive;
                model.SEOId = product.ZnodeProductId;
                model.SeoCode = product.SKU;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Creates znode search request.
        public virtual IZnodeSearchRequest GetZnodeSearchRequest(SearchRequestModel model, FilterCollection filters, NameValueCollection sorts, bool isSuggestionList = false, bool isAllowIndexing=false)
        {
            if (IsNull(model))
                return null;

            var searchRequest = ObjectFactory.Container.GetInstance<IZnodeSearchRequest>();

            List<SearchItemRuleModel> boostItems = new List<SearchItemRuleModel>();

            SearchProfileModel searchProfile = GetSearchProfileData(model, boostItems);

            searchRequest.CatalogId = model.CatalogId;
            searchRequest.LocaleId = model.LocaleId;
            searchRequest.CategoryId = model.CategoryId;
            searchRequest.InnerSearchKeywords = (model.InnerSearchKeywords != null) ? model.InnerSearchKeywords : null;
            searchRequest.Facets = (model.RefineBy != null) ? model.RefineBy : null;
            searchRequest.ExternalIdEnabled = model.ExternalIdEnabled;
            searchRequest.ExternalIdNotNull = model.ExternalIdNotNullCheck;
            searchRequest.IndexName = GetCatalogIndexName(model.CatalogId);
            searchRequest.SearchText = IsNotNull(model.Keyword) ? model.Keyword.Trim().ToLower() : string.Empty;
            searchRequest.PageFrom = (model.PageNumber == 0) ? 1 : Convert.ToInt32(model.PageNumber);
            searchRequest.PageSize = Convert.ToInt32(model.PageSize);
            searchRequest.PostTags = "</div>";
            searchRequest.PreTags = "<div style = 'color:red' >";
            searchRequest.CatalogIdLocalIdDictionary = GetDefaultSearchableFieldsFilter(model, filters, isSuggestionList);
            searchRequest.NumberOfAggregationSize = model.NumberOfAggregationSize;
            searchRequest.boostField = model.CategoryId > 0 ? ZnodeConstant.catgoryBoost : ZnodeConstant.productBoost;
            searchRequest.SortCriteria = GetSort(sorts, model.CategoryId, model.Keyword);
            searchRequest.IsBrandSearch = model.IsBrandSearch;
            searchRequest.GetFacets = model.IsFacetList;
            //To do: This value will be user specific.
            searchRequest.SuggestionTermCount = 1;
            searchRequest.QueryTypeName = searchProfile.QueryTypeName;
            searchRequest.QueryClass = searchProfile.QueryBuilderClassName;
            searchRequest.SubQueryType = searchProfile.SubQueryType;
            searchRequest.Operator = searchProfile.Operator;
            searchRequest.AttributeList = GetCatalogAttributeList(model.CatalogId, model.LocaleId).ToModel<SearchAttributes>().ToList();
            searchRequest.SearchableAttibute = GetSearableAttributeofProfile(searchProfile.SearchableAttributesList);
            searchRequest.FacetableAttribute = GetSearableAttributeofProfile(searchProfile.FacetAttributesList);
            searchRequest.FeatureList = MapSearchProfileFeature(searchProfile.FeaturesList);
            searchRequest.FieldValueFactors = searchProfile.FieldValueFactors;
            searchRequest.BoostAndBuryItemLists = MapSearchBoostAndBuryItem(boostItems);
            searchRequest.IsAllowIndexing = isAllowIndexing;
            return searchRequest;
        }

        private List<ElasticSearchBoostAndBuryItemList> MapSearchBoostAndBuryItem(List<SearchItemRuleModel> boostItems)
        {
            List<ElasticSearchBoostAndBuryItemList> list = new List<ElasticSearchBoostAndBuryItemList>();

            if (boostItems?.Count > 0)
            {
                foreach (SearchItemRuleModel item in boostItems)
                    list.Add(new ElasticSearchBoostAndBuryItemList
                    {
                        SearchItemKeyword = item.SearchItemKeyword,
                        SearchItemCondition = item.SearchItemCondition,
                        SearchItemValue = item.SearchItemValue,
                        SearchItemBoostValue = item.SearchItemBoostValue,
                        IsItemForAll = item.IsItemForAll,
                        SearchCatalogRuleId = item.SearchCatalogRuleId
                    });
            }

            return list;
        }

        private List<ElasticSearchFeature> MapSearchProfileFeature(List<SearchFeatureModel> featuresList)
        {
            List<ElasticSearchFeature> list = new List<ElasticSearchFeature>();

            if (featuresList?.Count > 0)
            {
                foreach (SearchFeatureModel feature in featuresList)
                    list.Add(new ElasticSearchFeature
                    {
                        FeatureCode = feature.FeatureCode,
                        SearchFeatureValue = feature.SearchFeatureValue,
                        FeatureName = feature.FeatureName,
                    });
            }
            ZnodeLogging.LogMessage("ElasticSearchFeature list count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, list?.Count);
            return list;
        }

        //Gets the sort criteria.
        public virtual List<SortCriteria> GetSort(NameValueCollection sortCollection, int categoryId, string searchTerm)
        {
            if (sortCollection.HasKeys())
            {
                foreach (var key in sortCollection.AllKeys)
                {
                    var value = sortCollection.Get(key);

                    if (Equals(key, SortCriteria.SortNameEnum.ProductName.ToString().ToLower()))
                        return new List<SortCriteria>() { new SortCriteria() { SortDirection = (Equals(value, Constants.SortKeys.Ascending)) ? SortCriteria.SortDirectionEnum.ASC : SortCriteria.SortDirectionEnum.DESC, SortName = SortCriteria.SortNameEnum.ProductName } };

                    if (Equals(key, SortCriteria.SortNameEnum.Price.ToString().ToLower()))
                        return new List<SortCriteria>() { new SortCriteria() { SortDirection = (Equals(value, Constants.SortKeys.Ascending)) ? SortCriteria.SortDirectionEnum.ASC : SortCriteria.SortDirectionEnum.DESC, SortName = SortCriteria.SortNameEnum.Price } };

                    if (Equals(key, SortCriteria.SortNameEnum.HighestRated.ToString().ToLower()))
                        return new List<SortCriteria>() { new SortCriteria() { SortDirection = (Equals(value, Constants.SortKeys.Ascending)) ? SortCriteria.SortDirectionEnum.ASC : SortCriteria.SortDirectionEnum.DESC, SortName = SortCriteria.SortNameEnum.HighestRated } };

                    if (Equals(key, SortCriteria.SortNameEnum.MostReviewed.ToString().ToLower()))
                        return new List<SortCriteria>() { new SortCriteria() { SortDirection = (Equals(value, Constants.SortKeys.Ascending)) ? SortCriteria.SortDirectionEnum.ASC : SortCriteria.SortDirectionEnum.DESC, SortName = SortCriteria.SortNameEnum.MostReviewed } };

                    if (Equals(key, SortCriteria.SortNameEnum.OutOfStock.ToString().ToLower()))
                        return new List<SortCriteria>() { new SortCriteria() { SortDirection = (Equals(value, Constants.SortKeys.Ascending)) ? SortCriteria.SortDirectionEnum.ASC : SortCriteria.SortDirectionEnum.DESC, SortName = SortCriteria.SortNameEnum.OutOfStock } };

                    if (Equals(key, SortCriteria.SortNameEnum.InStock.ToString().ToLower()))
                        return new List<SortCriteria>() { new SortCriteria() { SortDirection = (Equals(value, Constants.SortKeys.Ascending)) ? SortCriteria.SortDirectionEnum.ASC : SortCriteria.SortDirectionEnum.DESC, SortName = SortCriteria.SortNameEnum.InStock } };
                }
            }

            if (categoryId > 0 && string.IsNullOrEmpty(searchTerm))
                return new List<SortCriteria>() { new SortCriteria() { SortName = SortCriteria.SortNameEnum.DisplayOrder, SortDirection = SortCriteria.SortDirectionEnum.ASC }, new SortCriteria() { SortName = SortCriteria.SortNameEnum.ProductName, SortDirection = SortCriteria.SortDirectionEnum.ASC } };
            else
                return new List<SortCriteria>();
        }

        //Gets expands for search.
        public void GetFacetExpands(NameValueCollection expands, IZnodeSearchRequest request)
        {
            ExpandCategories(expands, request);
        }

        //Gets category expands for category(If category collection is required in seach response.)
        private void ExpandCategories(NameValueCollection expands, IZnodeSearchRequest request)
        {
            //To do: Constants will be replaced later.
            if (!string.IsNullOrEmpty(expands.Get("Categories")))
            {
                request.GetCategoriesHeirarchy = true;
            }
        }

        //Converts search response to keyword search model.
        public KeywordSearchModel GetKeywordSearchModel(IZnodeSearchResponse response, IZnodeSearchRequest request = null)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            KeywordSearchModel model = new KeywordSearchModel();

            string output = JsonConvert.SerializeObject(response.ProductDetails);

            List<SearchProductModel> productList = JsonConvert.DeserializeObject<List<SearchProductModel>>(output);

            if (productList?.Count > 0)
                model.Products = productList.Count > 0 ? productList : new List<SearchProductModel>();

            if (response.ProductIds?.Count > 0 && model.Products?.Count > 0)
                model.Products = productList.OrderBy(d => response.ProductIds.IndexOf(d.ZnodeProductId)).ToList();

            if (response.Facets?.Count > 0)
                model.Facets = new List<SearchFacetModel>(response.Facets.
                    Select(x => new SearchFacetModel
                    {
                        DisplayOrder = x.DisplayOrder,
                        AttributeName = x.AttributeName,
                        AttributeCode = x.AttributeCode,
                        ControlTypeId = x.ControlTypeID,
                        AttributeValues = new List<SearchFacetValueModel>(x.AttributeValues.
                        Select(y => new SearchFacetValueModel
                        {
                            Label = y.AttributeValue,
                            AttributeValue = y.AttributeValue,
                            FacetCount = y.FacetCount,
                            DisplayOrder =y.displayorder                           
                        }).OrderBy(y => y.DisplayOrder).ToList())
                    }).OrderBy(x => x.DisplayOrder));

            if (response.CategoryItems?.Count > 0)
                model.Categories = GetCategoryFacetsFromResponse(response.CategoryItems);

            model.TotalProductCount = response.TotalProductCount;
            model.SuggestTerm = response.SuggestionTerm;
            model.IsSearchFromSuggestion = response.IsSearchFromSuggestion;
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return model;
        }

        //Converts the category collection in search request into search category model.
        private List<SearchCategoryModel> GetCategoryFacetsFromResponse(List<IZNodeSearchCategoryItem> categories)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            List<SearchCategoryModel> list = new List<SearchCategoryModel>();

            if (categories?.Count > 0)
            {
                foreach (var category in categories)
                {
                    list.Add(new SearchCategoryModel()
                    {
                        CategoryId = category.CategoryID,
                        CategoryName = category.Name,
                        Count = category.Count.GetValueOrDefault(),
                        SEOUrl = category.SEOUrl,
                        ParentCategories = IsNull(category.ParentCategory) ? null : GetCategoryFacetsFromResponse(category.ParentCategory)
                    });
                }
            }
            ZnodeLogging.LogMessage("Search category list count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, list?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return list;
        }

        //Save Product level boost values        
        private bool SaveProductBoostValues(BoostDataModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            bool saveResult = true;

            if (model.ID > 0)
            {
                ZnodeSearchGlobalProductBoost globalProductBoostEntity = _globalProductBoostRepository.Table.Where(x => x.SearchGlobalProductBoostId == model.ID).Select(x => x).AsEnumerable().FirstOrDefault();
                globalProductBoostEntity.Boost = model.Boost;
                saveResult = _globalProductBoostRepository.Update(globalProductBoostEntity);
            }
            else
                saveResult = _globalProductBoostRepository.Insert(new ZnodeSearchGlobalProductBoost { Boost = model.Boost, PublishCatalogId = model.CatalogId, PublishProductId = model.PublishProductId }).SearchGlobalProductBoostId > 0;

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return saveResult;
        }

        // Save category level boost values        
        private bool SaveProductCategoryBoostValues(BoostDataModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            var saveResult = true;

            if (model.ID > 0)
            {
                ZnodeSearchGlobalProductCategoryBoost globalProductCategoryBoostEntity = _globalProductCategoryBoostRepository.Table.Where(x => x.SearchGlobalProductCategoryBoostId == model.ID).Select(x => x).AsEnumerable().FirstOrDefault();
                globalProductCategoryBoostEntity.Boost = model.Boost;
                saveResult = _globalProductCategoryBoostRepository.Update(globalProductCategoryBoostEntity);
            }
            else
                saveResult = _globalProductCategoryBoostRepository.Insert(new ZnodeSearchGlobalProductCategoryBoost { Boost = model.Boost, PublishCatalogId = model.CatalogId, PublishProductId = model.PublishProductId, PublishCategoryId = model.PublishCategoryId }).SearchGlobalProductCategoryBoostId > 0;

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return saveResult;
        }

        // Save Field level boost values
        private bool SaveFieldsBoostValues(BoostDataModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            bool saveResult = true;

            if (model.ID > 0)
            {
                ZnodeSearchDocumentMapping fieldBoostEntity = _documentMappingRepository.Table.Where(x => x.SearchDocumentMappingId == model.ID).Select(x => x).AsEnumerable().FirstOrDefault();
                fieldBoostEntity.Boost = model.Boost;
                saveResult = _documentMappingRepository.Update(fieldBoostEntity);
            }
            else
                saveResult = _documentMappingRepository.Insert(new ZnodeSearchDocumentMapping { Boost = model.Boost, PropertyName = model.PropertyName, PublishCatalogId = model.CatalogId }).SearchDocumentMappingId > 0;

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return saveResult;
        }

        //Delete Field level boost if it is removed.
        private bool DeleteFieldsBoostValues(int documentmappingRepositoryId)
        {
            FilterCollection deleteFilter = new FilterCollection() { new FilterTuple(ZnodeSearchDocumentMappingEnum.SearchDocumentMappingId.ToString(), FilterOperators.Equals, documentmappingRepositoryId.ToString()) };
            return _documentMappingRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(deleteFilter.ToFilterDataCollection()).WhereClause);
        }

        //Delete product category boost if it is removed.
        private bool DeleteProductCategoryBoostValues(int searchGlobalProductCategoryBoostId)
        {
            FilterCollection deleteFilter = new FilterCollection() { new FilterTuple(ZnodeSearchGlobalProductCategoryBoostEnum.SearchGlobalProductCategoryBoostId.ToString(), FilterOperators.Equals, searchGlobalProductCategoryBoostId.ToString()) };
            return _globalProductCategoryBoostRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(deleteFilter.ToFilterDataCollection()).WhereClause);
        }

        //Delete product boost if it is removed.
        private bool DeleteProductBoostValues(int searchGlobalProductBoostId)
        {
            FilterCollection deleteFilter = new FilterCollection() { new FilterTuple(ZnodeSearchGlobalProductBoostEnum.SearchGlobalProductBoostId.ToString(), FilterOperators.Equals, searchGlobalProductBoostId.ToString()) };
            return _globalProductBoostRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(deleteFilter.ToFilterDataCollection()).WhereClause);
        }

        //Gets default and clause filter terms.
        public virtual Dictionary<string, List<string>> GetDefaultSearchableFieldsFilter(SearchRequestModel model, FilterCollection filters, bool isSuggestionList)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            Dictionary<string, List<string>> filterAndClause = new Dictionary<string, List<string>>();

            //Default filters for search

            if (model.CategoryId > 0) filterAndClause.Add("categoryid", new List<string>() { model.CategoryId.ToString() });

            filters.RemoveAll(filter => filter.FilterName == FilterKeys.ProductIndex);
            filters.RemoveAll(filter => filter.FilterName == "isfacet");
            filters.RemoveAll(filter => filter.FilterName == "portalid");
            filters.RemoveAll(filter => filter.FilterName == "znodecategoryid");

            //To do: code for all operator.
            foreach (FilterTuple filter in filters)
            {
                switch (filter.FilterOperator)
                {
                    case FilterOperators.Equals:
                        filterAndClause.Add(filter.FilterName.ToLower(), new List<string>() { filter.FilterValue });
                        break;
                    case FilterOperators.In:
                        filterAndClause.Add(filter.FilterName.ToLower(), filter.FilterValue.Split(',').ToList());
                        break;
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return filterAndClause;
        }

        // Validate catalog id and locale id.
        private void ValidateCatalogIdAndLocaleId(SearchRequestModel model)
        {
            if (model.CatalogId <= 0)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorCatalogIdLessThanZero);
            if (model.LocaleId <= 0)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorLocaleIdLessThanZero);
            if (model.PortalId <= 0)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorPortalIdLessThanZero);
        }

        //Insert into ZnodeSearch indexMonitor.
        private ZnodeSearchIndexMonitor SearchIndexMonitorInsert(PortalIndexModel portalIndexModel)
        {
            return _searchIndexMonitorRepository.Insert(new ZnodeSearchIndexMonitor()
            {
                SourceId = 0,
                CatalogIndexId = portalIndexModel.CatalogIndexId,
                SourceType = "CreateIndex",
                SourceTransactionType = "INSERT",
                AffectedType = "CreateIndex",
                CreatedBy = portalIndexModel.CreatedBy,
                CreatedDate = portalIndexModel.CreatedDate,
                ModifiedBy = portalIndexModel.ModifiedBy,
                ModifiedDate = portalIndexModel.ModifiedDate
            });
        }

        private ZnodeSearchIndexMonitor CreateSearchIndexMonitorEntry(PortalIndexModel portalIndexModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("PortalIndexModel with PortalIndexId: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, portalIndexModel?.PortalIndexId);
            //Get Catalog index data by id.
            var getCatalogIndexDetail = (from aa in _catalogIndexRepository.Table
                                         where aa.CatalogIndexId == portalIndexModel.CatalogIndexId
                                         select new PortalIndexModel()
                                         {
                                             CatalogIndexId = portalIndexModel.CatalogIndexId,
                                             PublishCatalogId = aa.PublishCatalogId,
                                             IndexName = aa.IndexName,

                                         }).FirstOrDefault();
            //Check if same as earlier
            if (getCatalogIndexDetail.IndexName == portalIndexModel.IndexName)
            {
                return SearchIndexMonitorInsert(portalIndexModel);
            }
            else
            {
                //Check if index name is already used by another store.
                string indexName = _catalogIndexRepository.Table.Where(x => x.IndexName == portalIndexModel.IndexName).Select(s => s.IndexName)?.FirstOrDefault() ?? string.Empty;

                if (!string.IsNullOrEmpty(indexName) || new LoadDefaultData().IsIndexExists(portalIndexModel.IndexName))
                    throw new ZnodeException(ErrorCodes.DuplicateSearchIndexName, Admin_Resources.ErrorIndexNameIsInUse);
                bool renameStatus = RenameIndex(portalIndexModel.CatalogIndexId, getCatalogIndexDetail.IndexName, portalIndexModel.IndexName);
                if (renameStatus)
                    _catalogIndexRepository.Update(new ZnodeCatalogIndex { CatalogIndexId = getCatalogIndexDetail.CatalogIndexId, PublishCatalogId = getCatalogIndexDetail.PublishCatalogId, IndexName = portalIndexModel.IndexName });

                else
                {
                    ZnodeSearchIndexMonitor searchIndexMonitor;
                    searchIndexMonitor = SearchIndexMonitorInsert(portalIndexModel);
                    SearchHelper searchHelper = new SearchHelper();
                    int searchIndexServerStatusId = 0;
                    searchIndexServerStatusId = searchHelper.CreateSearchIndexServerStatus(new SearchIndexServerStatusModel()
                    {
                        SearchIndexMonitorId = searchIndexMonitor.SearchIndexMonitorId,
                        ServerName = Environment.MachineName,
                        Status = ZnodeConstant.SearchIndexStartedStatus
                    }).SearchIndexServerStatusId;
                    CreateIndex(portalIndexModel.IndexName, portalIndexModel.RevisionType, portalIndexModel.CatalogIndexId, searchIndexMonitor.SearchIndexMonitorId, searchIndexServerStatusId);
                    RenameIndex(portalIndexModel.CatalogIndexId, getCatalogIndexDetail.IndexName, portalIndexModel.IndexName);
                    _catalogIndexRepository.Update(new ZnodeCatalogIndex { CatalogIndexId = getCatalogIndexDetail.CatalogIndexId, PublishCatalogId = getCatalogIndexDetail.PublishCatalogId, IndexName = portalIndexModel.IndexName });
                }
                return SearchIndexMonitorInsert(portalIndexModel);
            }
        }

        //Sets products boost value.
        private void SetProductBoostValue(SearchGlobalProductBoostListModel searchGlobalProductBoostList)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            string publishProductIds = string.Join(",", searchGlobalProductBoostList.SearchGlobalProductBoostList.Select(x => x.PublishProductId).ToList());

            if (!string.IsNullOrEmpty(publishProductIds))
            {
                FilterCollection productBoostFilter = new FilterCollection() { new FilterTuple(ZnodeSearchGlobalProductBoostEnum.PublishProductId.ToString(), FilterOperators.In, publishProductIds) };

                EntityWhereClauseModel whereClause = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(productBoostFilter.ToFilterDataCollection());

                var productBoostList = _globalProductBoostRepository.GetEntityList(whereClause.WhereClause).ToList();

                if (productBoostList.Count > 0)
                {
                    searchGlobalProductBoostList.SearchGlobalProductBoostList.ForEach(searchGlobalProductBoost => searchGlobalProductBoost.Boost = (productBoostList.Where(productBoost => productBoost.PublishProductId == searchGlobalProductBoost.PublishProductId).Select(x => x.Boost).FirstOrDefault()));
                    searchGlobalProductBoostList.SearchGlobalProductBoostList.ForEach(searchGlobalProductBoost => searchGlobalProductBoost.SearchGlobalProductBoostId = (productBoostList.Where(productBoost => productBoost.PublishProductId == searchGlobalProductBoost.PublishProductId).Select(x => x.SearchGlobalProductBoostId).FirstOrDefault()));
                }
            }
            ZnodeLogging.LogMessage("SearchGlobalProductBoostList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchGlobalProductBoostList?.SearchGlobalProductBoostList?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Sets category boost value.
        private void SetProductCatgoryBoostValue(SearchGlobalProductCategoryBoostListModel searchGlobalProductCategoryBoostList)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            StringBuilder productCategoryFilter = new StringBuilder("(");
            for (int i = 0; i < searchGlobalProductCategoryBoostList.SearchGlobalProductCategoryList.Count; i++)
            {
                productCategoryFilter.Append($"({ZnodeSearchGlobalProductCategoryBoostEnum.PublishCategoryId.ToString()}={searchGlobalProductCategoryBoostList.SearchGlobalProductCategoryList[i].PublishCategoryId} and {ZnodeSearchGlobalProductCategoryBoostEnum.PublishProductId.ToString()}={searchGlobalProductCategoryBoostList.SearchGlobalProductCategoryList[i].PublishProductId})");
                if (i < searchGlobalProductCategoryBoostList.SearchGlobalProductCategoryList.Count - 1)
                    productCategoryFilter.Append(" or ");
            }
            productCategoryFilter.Append(")");

            string productCategoryWhereClause = productCategoryFilter.ToString();

            if (!string.IsNullOrEmpty(productCategoryWhereClause))
            {
                var productCategoryBoostList = _globalProductCategoryBoostRepository.GetEntityList(productCategoryWhereClause).ToList();

                if (productCategoryBoostList.Count > 0)
                {
                    searchGlobalProductCategoryBoostList.SearchGlobalProductCategoryList.ForEach(searchGlobalProductCategoryBoost => searchGlobalProductCategoryBoost.Boost = (productCategoryBoostList.Where(productBoost => productBoost.PublishProductId == searchGlobalProductCategoryBoost.PublishProductId && productBoost.PublishCategoryId == searchGlobalProductCategoryBoost.PublishCategoryId).Select(x => x.Boost).FirstOrDefault()));
                    searchGlobalProductCategoryBoostList.SearchGlobalProductCategoryList.ForEach(searchGlobalProductCategoryBoost => searchGlobalProductCategoryBoost.SearchGlobalProductCategoryBoostId = (productCategoryBoostList.Where(productBoost => productBoost.PublishProductId == searchGlobalProductCategoryBoost.PublishProductId && productBoost.PublishCategoryId == searchGlobalProductCategoryBoost.PublishCategoryId).Select(x => x.SearchGlobalProductCategoryBoostId).FirstOrDefault()));
                }

            }
            ZnodeLogging.LogMessage("SearchGlobalProductCategoryList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, searchGlobalProductCategoryBoostList?.SearchGlobalProductCategoryList?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Sets field boost value.
        private void SetFieldBoostValues(SearchDocumentMappingListModel listModel, List<string> searchableFields, int catalogId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            FilterCollection searchableFieldFilter = new FilterCollection() { new FilterTuple(ZnodeSearchDocumentMappingEnum.PropertyName.ToString(), FilterOperators.In, string.Join(",", searchableFields.Select(x => $"\"{x}\"").ToList())),
                                                                              new FilterTuple(ZnodeSearchDocumentMappingEnum.PublishCatalogId.ToString(), FilterOperators.Equals, catalogId.ToString())};

            EntityWhereClauseModel whereClause = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(searchableFieldFilter.ToFilterDataCollection());
            ZnodeLogging.LogMessage("WhereClause to get fieldBoostList: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, whereClause?.WhereClause);
            var fieldBoostList = _documentMappingRepository.GetEntityList(whereClause.WhereClause).ToList();

            if (fieldBoostList.Count > 0)
            {
                listModel.SearchDocumentMappingList.ForEach(searchFieldBoost => searchFieldBoost.Boost = (fieldBoostList.Where(fieldBoost => fieldBoost.PropertyName == searchFieldBoost.PropertyName).Select(x => x.Boost).FirstOrDefault()));
                listModel.SearchDocumentMappingList.ForEach(searchFieldBoost => searchFieldBoost.SearchDocumentMappingId = (fieldBoostList.Where(fieldBoost => fieldBoost.PropertyName == searchFieldBoost.PropertyName).Select(x => x.SearchDocumentMappingId).FirstOrDefault()));
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Calls application to create Index.
        private void CallSearchIndexer(PortalIndexModel portalIndexModel, int userId, int searchIndexServerStatusId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("userId and searchIndexServerStatusId to call search indexer: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { userId, searchIndexServerStatusId });
            ZnodeLogging.LogMessage(string.Format(Admin_Resources.SearchIndexerCalled, portalIndexModel.IndexName), ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            portalIndexModel.RevisionType = String.IsNullOrEmpty(portalIndexModel.RevisionType) ? "PRODUCTION" : portalIndexModel.RevisionType;
            ProcessStartInfo info = new ProcessStartInfo();
            string schedulerPath = GetSchedulerPath();
            info.FileName = !string.IsNullOrEmpty(schedulerPath) && File.Exists(schedulerPath) ? schedulerPath : System.Web.Hosting.HostingEnvironment.MapPath(ZnodeConstant.schedulerPath);


            string tokenvalue = HttpContext.Current.Request.Headers["Token"] == null ? "0" : HttpContext.Current.Request.Headers["Token"];
            string apiDomainUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/";

            info.Arguments = $"Indexer {portalIndexModel.PublishCatalogId} {portalIndexModel.IndexName}  {portalIndexModel.CatalogIndexId} {portalIndexModel.SearchCreateIndexMonitorId} Manually {apiDomainUrl} {userId} {searchIndexServerStatusId} {portalIndexModel.RevisionType} {HttpContext.Current.Request.Headers["Authorization"]?.Replace("Basic ", "")}  {tokenvalue} {ZnodeApiSettings.RequestTimeout}";
            info.UseShellExecute = false;

            Process process = Process.Start(info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            process.WaitForExit();
        }

        //Method to check if Preview mode is on or not.
        private bool GetIsPreviewEnabled()
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.GlobalSettings.ToString(), TraceLevel.Info);
            bool iSPreviewEnabled = (from PSATM in _publishStateMappingRepository.Table
                                                join PS in _publishStateRepository.Table on PSATM.PublishStateId equals PS.PublishStateId
                                                where PSATM.IsActive && PS.IsActive && PSATM.IsEnabled == true && PS.PublishStateCode == ZnodePublishStatesEnum.PREVIEW.ToString()
                                                select PSATM.IsEnabled
                                                ).Any();                                            

            ZnodeLogging.LogMessage("Is publish state preview is enabled : ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, iSPreviewEnabled);
            return iSPreviewEnabled;
        }

        //Get Image Path For Category List.
        protected virtual void GetProductImagePathForSuggestions(int portalId, List<SearchProductModel> productList)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            if (portalId > 0)
            {
                string imageName = string.Empty;       
                IImageHelper imageHelper = GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", portalId)); 
                productList?.ForEach(
                    x =>
                    {
                        imageName = x.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues;
                        x.ImageSmallThumbnailPath = imageHelper.GetImageHttpPathSmallThumbnail(imageName);
                    });
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Set stored based In Stock, Out Of Stock, Back Order Message.
        private void SetPortalBasedDetails(int portalId, List<SearchProductModel> searchProductModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            IPortalService _portalService = GetService<IPortalService>();
            PortalModel portalDetails = _portalService.GetPortal(portalId, null);
            searchProductModel?.ForEach(
                   x =>
                   {
                       x.InStockMessage = portalDetails.InStockMsg;
                       x.OutOfStockMessage = portalDetails.OutOfStockMsg;
                       x.BackOrderMessage = portalDetails.BackOrderMsg;
                   });
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        private IZnodeSearchResponse GetPriceSortedSearchResponse(SearchRequestModel model, NameValueCollection sorts, IZnodeSearchProvider searchProvider, IZnodeSearchRequest searchRequest, string isInStock = "-1")
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            IZnodeSearchResponse searchResponse = searchProvider.FullTextSearch(searchRequest);
            DataTable productDetails = GetProductTypes(searchResponse.ProductDetails);
            //Create sku page.
            NameValueCollection priceSkuPage = new NameValueCollection();
            priceSkuPage.Add("index", searchRequest.PageFrom.ToString());
            priceSkuPage.Add("size", (Equals(searchRequest.PageSize, -1) ? int.MaxValue : searchRequest.PageSize).ToString());

            ReplaceSortKeyName(ref sorts, "price", "retailprice");
            IPriceService _priceService = GetService<IPriceService>();
            PriceSKUListModel skuPrice = _priceService.GetPagedPriceSKU(null, new FilterCollection() { new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, model.PortalId.ToString()) }, sorts, priceSkuPage, productDetails, isInStock);
            ZnodeLogging.LogMessage("PriceSKUList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, skuPrice?.PriceSKUList?.Count);
            var SoretdIDs = skuPrice.PriceSKUList.Select(x => x.PublishProductId).ToList();
            if (SoretdIDs.Count > 0)
                searchResponse.ProductIds = SoretdIDs;
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchResponse;
        }

        private DataTable GetProductTypes(List<dynamic> products)
        {
            DataTable table = new DataTable("ProductTable");
            DataColumn productId = new DataColumn("Id");
            productId.DataType = typeof(int);
            productId.AllowDBNull = false;
            table.Columns.Add(productId);
            table.Columns.Add("ProductType", typeof(string));
            table.Columns.Add("OutOfStockOptions", typeof(string));

            foreach (var item in products)
                table.Rows.Add(Convert.ToInt32(item["znodeproductid"]), Convert.ToString(item["producttype"]), Convert.ToString(item["outofstockoptions"]));

            return table;
        }

        //Gets search result from search.
        protected void GetResultFromSuggestions(SearchRequestModel model, FilterCollection filters, NameValueCollection sorts, IZnodeSearchProvider searchProvider, ref IZnodeSearchRequest searchRequest, ref IZnodeSearchResponse searchResponse)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            if (model.UseSuggestion && !string.IsNullOrEmpty(searchResponse?.SuggestionTerm) && searchResponse.ProductDetails?.Count <= 0)
            {
                model.Keyword = searchResponse.SuggestionTerm;
                searchRequest = GetZnodeSearchRequest(model, filters, sorts);
                if (!model.IsFacetList && model.CategoryId < 1)
                    searchRequest.CatalogIdLocalIdDictionary.Add("productindex", new List<string>() { "1" });
                searchResponse = searchProvider.FullTextSearch(searchRequest);
                searchResponse.IsSearchFromSuggestion = true;
                searchResponse.SuggestionTerm = model.Keyword;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
        }

        //Method to get search response.
        public virtual IZnodeSearchResponse GetSearchResponse(SearchRequestModel model, NameValueCollection expands, ref NameValueCollection sorts, IZnodeSearchProvider searchProvider, IZnodeSearchRequest searchRequest, string sortName, bool isFacetSearch = false)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            IZnodeSearchResponse searchResponse;
            ZnodeLogging.LogMessage("sortName and isFacetSearch: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { sortName, isFacetSearch });
            switch (sortName)
            {
                case ZnodeConstant.Price:
                    if(searchRequest.IsAllowIndexing)
                        searchResponse = searchProvider.FullTextSearch(searchRequest);
                    else
                    searchResponse = GetPriceSortedSearchResponse(model, sorts, searchProvider, searchRequest, "-1");
                    break;
                case ZnodeConstant.OutOfStock:
                    searchResponse = GetPriceSortedSearchResponse(model, sorts, searchProvider, searchRequest, "0");
                    break;
                case ZnodeConstant.InStock:
                    searchResponse = GetPriceSortedSearchResponse(model, sorts, searchProvider, searchRequest, "1");
                    break;
                default:
                    //All other conditions
                    if (IsNotNull(expands))
                        GetFacetExpands(expands, searchRequest);
                    searchResponse = searchProvider.FullTextSearch(searchRequest);
                    break;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchResponse;
        }

        public string GetCatalogIndexName(int cataLogId)
        {
            ZnodeLogging.LogMessage("cataLogId to get index name: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, cataLogId);
            string cacheKey = $"CatalogIndexName_{cataLogId}";
            string indexName = Equals(HttpRuntime.Cache[cacheKey], null)
               ? GetCatalogIndexNameFromDB(cataLogId)
               : ((string)HttpRuntime.Cache.Get(cacheKey));
            return indexName;
        }

        private string GetCatalogIndexNameFromDB(int publishCatalogId)
        {
            ZnodeLogging.LogMessage("publishCatalogId to get catalog index name: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, publishCatalogId);
            FilterCollection filter = new FilterCollection() { new FilterTuple(ZnodeCatalogIndexEnum.PublishCatalogId.ToString(), FilterOperators.Equals, publishCatalogId.ToString()) };
            EntityWhereClauseModel whereClause = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection());
            return _catalogIndexRepository.GetEntity(whereClause.WhereClause)?.IndexName;
        }

        public virtual List<CatalogAttributeEntity> GetCatalogAttributeList(int cataLogId, int localeId)
        {
            ZnodeLogging.LogMessage("cataLogId and localeId to get attributeList: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { cataLogId, localeId });
            string cacheKey = $"CatalogAttribute_{cataLogId}_{localeId}";
            List<CatalogAttributeEntity> attributeList = Equals(HttpRuntime.Cache[cacheKey], null)
               ? GetCatalogAttributeListFromDB(cataLogId, localeId, cacheKey)
               : (List<CatalogAttributeEntity>)HttpRuntime.Cache.Get(cacheKey);
            ZnodeLogging.LogMessage("attributeList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, attributeList?.Count);
            return attributeList;
        }

        private List<CatalogAttributeEntity> GetCatalogAttributeListFromDB(int cataLogId, int localeId, string cacheKey)
        {
            ZnodeLogging.LogMessage("cataLogId, localeId and cacheKey to get attributeList: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, new object[] { cataLogId, localeId, cacheKey });
            IMongoRepository<CatalogAttributeEntity> _catalogAttributeEntity = new MongoRepository<CatalogAttributeEntity>(GetCatalogVersionId());
            List<IMongoQuery> query = new List<IMongoQuery>();
            query.Add(Query<CatalogAttributeEntity>.EQ(pr => pr.ZnodeCatalogId, cataLogId));
            query.Add(Query<CatalogAttributeEntity>.EQ(pr => pr.LocaleId, localeId));
            query.Add(Query<CatalogAttributeEntity>.EQ(pr => pr.VersionId, GetCatalogVersionId(cataLogId, localeId)));
            List<CatalogAttributeEntity> attributeList = _catalogAttributeEntity.GetEntityList(Query.And(query), true);
            HttpRuntime.Cache.Insert(cacheKey, attributeList);
            ZnodeLogging.LogMessage("attributeList count: ", ZnodeLogging.Components.Search.ToString(), TraceLevel.Verbose, attributeList?.Count);
            return attributeList;
        }


        public SearchProfileModel GetSearchProfileData(SearchRequestModel model, List<SearchItemRuleModel> boostItems)
        {
            string cacheKey = $"SearchProfileData";
            SearchProfileModel profileData = Equals(HttpRuntime.Cache[cacheKey], null)
               ? GetSearchProfileDataFromDB(model, boostItems)
               : ((SearchProfileModel)HttpRuntime.Cache.Get(cacheKey));
            return profileData;
        }

        private SearchProfileModel GetSearchProfileDataFromDB(SearchRequestModel model, List<SearchItemRuleModel> boostItems)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@Keyword", model.Keyword, ParameterDirection.Input, SqlDbType.NVarChar);
            objStoredProc.GetParameter("@ProfileId", model.ProfileId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@PublishCatalogId", model.CatalogId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@PortalId", model.PortalId, ParameterDirection.Input, SqlDbType.Int);
            DataSet dataSet = objStoredProc.GetSPResultInDataSet("Znode_GetWebStoreSearchProfileTrigger");

            SearchProfileModel searchProfile = new SearchProfileModel();

            if (dataSet.Tables?.Count > 0)
            {
                List<SearchAttributesModel> searchAttributes = dataSet.Tables[1]?.AsEnumerable().Select(m => new SearchAttributesModel()
                {

                    AttributeCode = m.Field<string>("AttributeCode"),
                    BoostValue = m.Field<int?>("BoostValue"),
                    IsFacets = m.Field<bool>("IsFacets"),
                    IsUseInSearch = m.Field<bool>("IsUseInSearch"),
                }).ToList();

                List<SearchFeatureModel> featureList = dataSet.Tables[0]?.AsEnumerable().Select(m => new SearchFeatureModel()
                {

                    FeatureName = m.Field<string>("FeatureName"),
                    FeatureCode = m.Field<string>("FeatureCode"),
                    SearchFeatureId = m.Field<int>("SearchFeatureId"),
                    SearchFeatureValue = m.Field<string>("SearchFeatureValue"),
                }).ToList();

                List<SearchAttributesModel> facets = dataSet.Tables[2]?.AsEnumerable().Select(m => new SearchAttributesModel()
                {

                    AttributeCode = m.Field<string>("AttributeCode"),
                    BoostValue = m.Field<int?>("BoostValue"),
                    IsFacets = m.Field<bool>("IsFacets"),
                    IsUseInSearch = m.Field<bool>("IsUseInSearch"),
                }).ToList();

                DataTable table = dataSet.Tables[3];

                foreach (DataRow row in table.Rows)
                {
                    searchProfile.ProfileName = Convert.ToString(row["ProfileName"]);
                    searchProfile.QueryTypeName = Convert.ToString(row["QueryTypeName"]);
                    searchProfile.SubQueryType = Convert.ToString(row["SubQueryTypeName"]);
                    searchProfile.QueryBuilderClassName = Convert.ToString(row["QueryBuilderClassName"]);
                    searchProfile.Operator = Convert.ToString(row["Operator"]);
                }

                List<KeyValuePair<string, int>> FieldList = dataSet.Tables[4]?.AsEnumerable().Select(m => new KeyValuePair<string, int>(m.Field<string>("FieldName"), m.Field<int>("FieldValueFactor"))).ToList();

                List<SearchItemRuleModel> boostItemList = dataSet.Tables[5]?.AsEnumerable().Select(m => new SearchItemRuleModel()
                {
                    SearchCatalogRuleId = m.Field<int>("SearchCatalogRuleId"),
                    SearchItemKeyword = m.Field<string>("SearchItemKeyword"),
                    SearchItemCondition = m.Field<string>("SearchItemCondition"),
                    SearchItemValue = m.Field<string>("SearchItemValue"),
                    SearchItemBoostValue = m.Field<int?>("SearchItemBoostValue"),
                    IsItemForAll = m.Field<bool>("IsItemForAll")
                }).ToList();

                searchProfile.FeaturesList = featureList;
                searchProfile.FacetAttributesList = facets;
                searchProfile.SearchableAttributesList = searchAttributes;
                searchProfile.FieldValueFactors = FieldList;
                boostItems.AddRange(boostItemList);
            }
            ZnodeLogging.LogMessage(string.Format(Admin_Resources.SearchProfile, searchProfile.ProfileName), ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Search.ToString(), TraceLevel.Info);
            return searchProfile;
        }
    }
    #endregion
}
