﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Observer;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Resources;

namespace Znode.Engine.Services
{
    public class SliderService : BaseService, ISliderService
    {
        #region Private Variable
        private readonly IZnodeRepository<ZnodeCMSSlider> _cmsSliderRepository;
        private readonly IZnodeRepository<ZnodeCMSSliderBanner> _cmsSliderBannerRepository;
        private readonly IZnodeRepository<ZnodeCMSSliderBannerLocale> _cmsSliderBannerLocaleRepository;
        private readonly IZnodeRepository<ZnodeCMSWidgetSliderBanner> _cmsWidgetSliderBannerRepository;
        private readonly IMongoRepository<WidgetSliderBannerEntity> _cmsWidgetSliderBannerMongoRepository;
        private readonly IMongoRepository<_LogWidgetSliderBannerEntity> log_cmsWidgetSliderBannerMongoRepository;
        private readonly IZnodeRepository<ZnodePublishPortalLog> _publishPortalLogRepository;
        #endregion

        #region Constructor
        public SliderService()
        {
            _cmsSliderRepository = new ZnodeRepository<ZnodeCMSSlider>();
            _cmsSliderBannerRepository = new ZnodeRepository<ZnodeCMSSliderBanner>();
            _cmsSliderBannerLocaleRepository = new ZnodeRepository<ZnodeCMSSliderBannerLocale>();
            _cmsWidgetSliderBannerRepository = new ZnodeRepository<ZnodeCMSWidgetSliderBanner>();
            _cmsWidgetSliderBannerMongoRepository = new MongoRepository<WidgetSliderBannerEntity>(WebstoreVersionId);
            log_cmsWidgetSliderBannerMongoRepository = new MongoRepository<_LogWidgetSliderBannerEntity>();
            _publishPortalLogRepository = new ZnodeRepository<ZnodePublishPortalLog>();
        }
        #endregion

        #region Public Methods
        #region Slider
        //Get Slider List
        public virtual SliderListModel GetSliders(FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            //Set filter, sort and paging parameters.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            SliderListModel listModel = new SliderListModel();

            ZnodeLogging.LogMessage("pageListModel to get slider list : ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            List<ZnodeCMSSlider> sliders = _cmsSliderRepository.GetPagedList(pageListModel.EntityWhereClause.WhereClause, pageListModel.OrderBy, new List<string> { "ZnodePublishState" }, pageListModel.EntityWhereClause.FilterValues, pageListModel.PagingStart, pageListModel.PagingLength, out pageListModel.TotalRowCount).ToList();
            
            listModel.Sliders = sliders.Select(x => new SliderModel
            {
                CMSSliderId = x.CMSSliderId,
                IsPublished = x.IsPublished,
                Name = x.Name,
                PublishStatus = x.ZnodePublishState?.DisplayName,
                PublishStateId = x.PublishStateId
            }).ToList();
            listModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("slider list count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, listModel?.Sliders?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return listModel;
        }

        //Create slider.
        public virtual SliderModel CreateSlider(SliderModel sliderModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (HelperUtility.IsNull(sliderModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelCanNotBeNull);

            //Check if highlight name already exists.
            if (_cmsSliderRepository.Table.Count(x => Equals(x.Name.Trim(), sliderModel.Name.Trim())) > 0)
                throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.ErrorSliderExists);

            ZnodeCMSSlider sliderToInsert = sliderModel.ToEntity<ZnodeCMSSlider>();

            if (HelperUtility.IsNotNull(sliderToInsert))
                sliderToInsert.PublishStateId = (byte)ZnodePublishStatesEnum.NOT_PUBLISHED;

            //Create new slider and return it.
            ZnodeCMSSlider slider = _cmsSliderRepository.Insert(sliderToInsert);

            ZnodeLogging.LogMessage((slider?.CMSSliderId < 0) ? Admin_Resources.ErrorSliderInsert : string.Format(Admin_Resources.SuccessSliderInserted, slider?.CMSSliderId), ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (HelperUtility.IsNotNull(slider))
                return slider.ToModel<SliderModel>();

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return sliderModel;
        }

        //Get slider by cmsSliderId.
        public virtual SliderModel GetSlider(int cmsSliderId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            SliderModel model = new SliderModel();
            if (cmsSliderId > 0)
            {
                ZnodeLogging.LogMessage("cmsSliderId to get slider: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, cmsSliderId);
                model = (from cmsSlider in _cmsSliderRepository.Table
                         join cmsWidgetSlider in _cmsWidgetSliderBannerRepository.Table on cmsSlider.CMSSliderId equals cmsWidgetSlider.CMSSliderId into widgetSliderDetail
                         from cmsWidgetSliderDetail in widgetSliderDetail.DefaultIfEmpty()
                         where cmsSlider.CMSSliderId == cmsSliderId
                         select new SliderModel
                         {
                             CMSSliderId = cmsSliderId,
                             Name = cmsSlider.Name,
                             IsWidgetAssociated = cmsWidgetSliderDetail.WidgetsKey != null
                         }).FirstOrDefault();
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return model;
        }
        //Update Slider.
        public virtual bool UpdateSlider(SliderModel sliderModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (HelperUtility.IsNull(sliderModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelCanNotBeNull);

            //Check if highlight name already exists.
            if (_cmsSliderRepository.Table.Count(x => Equals(x.Name.Trim(), sliderModel.Name.Trim()) && x.CMSSliderId != sliderModel.CMSSliderId) > 0)
                throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.ErrorSliderExists);

            if (sliderModel.CMSSliderId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorCMSSliderIdLessThanOne);

            if (Equals(sliderModel.PublishStateId, null) || sliderModel.PublishStateId == 0)
            {
                sliderModel.PublishStateId = (byte)ZnodePublishStatesEnum.DRAFT;
            }
            //Update slider
            bool isSliderUpdated = _cmsSliderRepository.Update(sliderModel.ToEntity<ZnodeCMSSlider>());
            ZnodeLogging.LogMessage(isSliderUpdated ? string.Format(Admin_Resources.SuccessSliderUpdated, sliderModel?.CMSSliderId) : Admin_Resources.ErrorSliderUpdate, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return isSliderUpdated;
        }

        //Delete slider.
        public virtual bool DeleteSlider(ParameterModel cmsPortalSliderIds)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (HelperUtility.IsNull(cmsPortalSliderIds) || string.IsNullOrEmpty(cmsPortalSliderIds.Ids))
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorCMSPortalSliderIdLessThanOne);

            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter(ZnodeCMSSliderEnum.CMSSliderId.ToString(), cmsPortalSliderIds.Ids, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
            int status = 0;
            ZnodeLogging.LogMessage("Slider with Ids to be deleted: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, cmsPortalSliderIds?.Ids);
            IList<View_ReturnBoolean> deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_DeleteCMSSlider @CMSSliderId,  @Status OUT", 1, out status);
            if (deleteResult.FirstOrDefault().Status.Value)
            {
                ZnodeLogging.LogMessage(Admin_Resources.SuccessSliderDeleted, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return true;
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorSliderDeleted, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                throw new ZnodeException(ErrorCodes.AssociationDeleteError, Admin_Resources.ErrorDeleteSlider);
            }
        }

        //Publish a slider using sliderId.
        [Obsolete("To be discontinued in one of the upcoming versions.")]
        public virtual PublishedModel PublishSlider(ParameterModel cmsPortalSliderId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (HelperUtility.IsNull(cmsPortalSliderId) || string.IsNullOrEmpty(cmsPortalSliderId.Ids))
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorCMSPortalSliderIdLessThanOne);
            int cmsSliderId = 0;
            Int32.TryParse(cmsPortalSliderId.Ids, out cmsSliderId);

            IList<ZnodeCMSWidgetSliderBanner> widgetSliderList;
            ZnodeLogging.LogMessage("cmsSliderId to get widgetSliderList: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, cmsSliderId);
            widgetSliderList = _cmsWidgetSliderBannerRepository.Table.Where(x => x.CMSSliderId == cmsSliderId)?.ToList();
            ZnodeLogging.LogMessage("widgetSliderList count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, widgetSliderList?.Count);

            if (widgetSliderList?.Count > 0)
            {
                int userId = GetLoginUserId();
                PublishWidgetSliderBanner(cmsSliderId, userId);

                ZnodeLogging.LogMessage(string.Format(Admin_Resources.SuccessSliderPublished, cmsSliderId), ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

                return new PublishedModel { IsPublished = true, ErrorMessage = Admin_Resources.SuccessPublish };
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorSliderPublishAsItIsNotAssociatedWithWidget, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorSliderPublishAsItIsNotAssociatedWithWidget);
            }
        }

        public virtual PublishedModel PublishSlider(string cmsPortalSliderId, int portalId, int localeId, string targetPublishState = null, bool takeFromDraftFirst = false)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            bool result = false;

            if (string.IsNullOrEmpty(cmsPortalSliderId) || string.IsNullOrWhiteSpace(cmsPortalSliderId))
                throw new ZnodeException(ErrorCodes.InvalidEntityPassedDuringPublish, Api_Resources.InvalidEntityMessageDuringPublish);

            PublishProcessor processor = new PublishProcessor();

            IEnumerable<int> portalIds = portalId < 1 ? GetPublishedUserPortal() : new List<int>() { portalId };
            
            if (HelperUtility.IsNotNull(portalIds) && portalIds.Any())
            {
                portalIds = DataExistsForAvailablePortals(portalIds, cmsPortalSliderId);
                
                if (HelperUtility.IsNotNull(portalIds) && portalIds.Any())
                    foreach (int portalIdToPublishFor in portalIds)
                    {
                        result = processor.PublishIndividualCMSEntity(cmsPortalSliderId, portalIdToPublishFor, CopyEntityWithinMongo, CopyEntityFromSQLToMongo, localeId, targetPublishState, takeFromDraftFirst);
                        if (!result)
                            break;
                    }
                else
                    throw new ZnodeException(ErrorCodes.StoreNotPublishedForAssociatedEntity, string.Format(Api_Resources.StoreAssociatedWithEntityNotPublished, "banner slider"));
            }
            else
                return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.ErrorPublishNoAccessToPrePublishedPortal };

            return new PublishedModel { IsPublished = result, ErrorMessage = result ? String.Empty : Api_Resources.GenericExceptionMessageDuringPublish };
        }
        #endregion

        #region Banner
        //Gets the list of banner for selected slider.
        public virtual BannerListModel GetBannerList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            IZnodeViewRepository<BannerModel> objStoredProc = new ZnodeViewRepository<BannerModel>();

            //SP parameters
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);

            ZnodeLogging.LogMessage("pageListModel to get bannerlist: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            IList<BannerModel> bannerlist = objStoredProc.ExecuteStoredProcedureList("Znode_GetCMSSliderBannerPath  @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT", 4, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("bannerlist count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, bannerlist?.Count);

            BannerListModel listModel = new BannerListModel { Banners = bannerlist?.ToList() };
            listModel.BindPageListModel(pageListModel);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return listModel;
        }

        //Create banner.
        public virtual BannerModel CreateBanner(BannerModel bannerModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (HelperUtility.IsNull(bannerModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelCanNotBeNull);

            //Create new banner and return it.
            ZnodeCMSSliderBanner banner = _cmsSliderBannerRepository.Insert(bannerModel.ToEntity<ZnodeCMSSliderBanner>());
            if (banner?.CMSSliderBannerId > 0)
            {
                ZnodeLogging.LogMessage(Admin_Resources.SuccessBannerInserted, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                bannerModel.CMSSliderBannerId = banner.CMSSliderBannerId;
                ZnodeCMSSliderBannerLocale bannerLocale = _cmsSliderBannerLocaleRepository.Insert(GetSliderBannerLocaleEntity(bannerModel));
                if (bannerLocale?.CMSSliderBannerLocaleId > 0)
                {
                    ZnodeLogging.LogMessage("CMSSliderId to update publish slider status: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, bannerModel?.CMSSliderId);
                    UpdatePublishSliderStatus(bannerModel.CMSSliderId, ZnodePublishStatesEnum.DRAFT, false);
                }
                ZnodeLogging.LogMessage(bannerLocale?.CMSSliderBannerLocaleId > 0
                    ? string.Format(Admin_Resources.SuccessBannerLocaleInserted, bannerLocale?.CMSSliderBannerLocaleId) : Admin_Resources.ErrorBannerLocaleInsert, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

                return bannerModel;
            }

            ZnodeLogging.LogMessage(Admin_Resources.ErrorBannerInsert, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return null;
        }

        //Get banner by cmsSliderBannerId.
        public virtual BannerModel GetBanner(int cmsSliderBannerId, FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (cmsSliderBannerId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorSliderBannerIdLessThanOne);

            ZnodeLogging.LogMessage("cmsSliderBannerId to get sliderBannerEntity: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, cmsSliderBannerId);
            ZnodeCMSSliderBanner sliderBannerEntity = _cmsSliderBannerRepository.GetEntity(GetWhereClauseForSliderBannerId(cmsSliderBannerId).WhereClause, new List<string> { "ZnodeCMSSliderBannerLocales" });

            if (HelperUtility.IsNull(sliderBannerEntity))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ModelNotNull);

            //Maps to model.
            BannerModel model = sliderBannerEntity.ToModel<BannerModel>();

            //Sets the properties of banner model.
            SetBannerModel(sliderBannerEntity, model, GetLocaleId(filters));

            //Binds the slider name.
            model.Name = _cmsSliderRepository.Table.Where(x => x.CMSSliderId == model.CMSSliderId)?.FirstOrDefault().Name;

            //GetMediaPath method is used to get media path from media Id.
            model.MediaPath = model.MediaId > 0 ? GetMediaPath(model) : string.Empty;
            ZnodeLogging.LogMessage("BannerModel with CMSSliderBannerId and CMSSliderId: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new { CMSSliderBannerId = model.CMSSliderBannerId, CMSSliderId = model.CMSSliderId });
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return model;
        }

        //Update banner.
        public virtual bool UpdateBanner(BannerModel bannerModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (HelperUtility.IsNull(bannerModel))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ModelNotNull);

            if (bannerModel.CMSSliderBannerId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.IdCanNotBeLessThanOne);

            //Update banner
            ZnodeLogging.LogMessage("BannerModel with CMSSliderBannerId to be updated: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, bannerModel?.CMSSliderBannerId);
            bool isBannerUpdated = _cmsSliderBannerRepository.Update(bannerModel.ToEntity<ZnodeCMSSliderBanner>());

            //Save the data into slider banner locale.
            SaveInSliderBannerLocale(bannerModel);
            if (isBannerUpdated)
            {
                UpdatePublishSliderStatus(bannerModel.CMSSliderId, ZnodePublishStatesEnum.DRAFT, false);
            }

            ZnodeLogging.LogMessage(isBannerUpdated ? Admin_Resources.SuccessBannerUpdated : Admin_Resources.ErrorBannerUpdate, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return isBannerUpdated;
        }

        //Delete banner.
        public virtual bool DeleteBanner(ParameterModel cmsSliderBannerIds)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (HelperUtility.IsNull(cmsSliderBannerIds) || string.IsNullOrEmpty(cmsSliderBannerIds.Ids))
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorCMSSliderBannerIdLessThanOne);

            //Generates filter clause for multiple cmsSliderBannerIds.
            FilterCollection filter = new FilterCollection();
            filter.Add(new FilterTuple(ZnodeCMSSliderBannerEnum.CMSSliderBannerId.ToString(), ProcedureFilterOperators.In, cmsSliderBannerIds.Ids));
            int cmsSliderBannerId = 0;
            Int32.TryParse(cmsSliderBannerIds.Ids, out cmsSliderBannerId);
            ZnodeLogging.LogMessage("cmsSliderBannerId to get cmsSliderBanner: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, cmsSliderBannerId);
            ZnodeCMSSliderBanner cmsSliderBanner = _cmsSliderBannerRepository.GetById(cmsSliderBannerId);
            //Returns true if banner locale deleted sucessfully else return false.
            ZnodeLogging.LogMessage("CMS slider banner locale with Ids to be deleted: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, cmsSliderBannerIds?.Ids);
            bool IsDeleted = _cmsSliderBannerLocaleRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection()).WhereClause);
            ZnodeLogging.LogMessage(IsDeleted ? Admin_Resources.SuccessBannerLocaleDeleted : Admin_Resources.ErrorBannerLocaleDelete, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            //Returns true if banner deleted sucessfully else return false.
            IsDeleted = _cmsSliderBannerRepository.Delete(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection()).WhereClause);
            if (IsDeleted)
            {
                UpdatePublishSliderStatus(cmsSliderBanner.CMSSliderId, ZnodePublishStatesEnum.DRAFT, false);
            }
            ZnodeLogging.LogMessage(IsDeleted ? Admin_Resources.SuccessBannerDeleted : Admin_Resources.ErrorBannerDelete, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return IsDeleted;
        }
        #endregion

        #endregion

        #region Private Method
        private bool CopyEntityWithinMongo(string id, WebStoreEntity existingWebstoreCopy, ZnodePublishStatesEnum targetPublishState, int additionalParameter = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            int cmsSliderId = int.Parse(id);

            bool isSuccessful = false;
            Exception exception = new Exception();

            int sourceVersionId = existingWebstoreCopy.VersionId;

            int portalId = existingWebstoreCopy.PortalId;
            int localeId = existingWebstoreCopy.LocaleId;

            ZnodeLogging.LogMessage("cmsSliderId, portalId, sourceVersionId and localeId to get sliderBannersToPublish list: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { cmsSliderId, portalId, sourceVersionId, localeId });

            List<WidgetSliderBannerEntity> sliderBannersToPublish = _cmsWidgetSliderBannerMongoRepository.GetEntityList(Query.And(
                        Query<WidgetSliderBannerEntity>.EQ(x => x.SliderId, cmsSliderId),
                        Query<WidgetSliderBannerEntity>.EQ(x => x.PortalId, portalId),
                        Query<WidgetSliderBannerEntity>.EQ(x => x.VersionId, sourceVersionId),
                        Query<WidgetSliderBannerEntity>.EQ(x => x.LocaleId, localeId)), true);
            ZnodeLogging.LogMessage("sliderBannersToPublish list count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, sliderBannersToPublish?.Count);

            if (HelperUtility.IsNotNull(sliderBannersToPublish) && sliderBannersToPublish.Count > 0)
            {
                ZnodePublishStatesEnum sourceContentState;
                Enum.TryParse(existingWebstoreCopy.PublishState, true, out sourceContentState);

                ZnodePublishPortalLog znodePublishPortalLog = null;

                ZnodePublishPortalLog previewedPortalLog = _publishPortalLogRepository.Table?.Where(x => x.PortalId == portalId)?.ToArray()?.LastOrDefault();
                if (HelperUtility.IsNotNull(previewedPortalLog))
                    znodePublishPortalLog = _publishPortalLogRepository.Insert(
                        new ZnodePublishPortalLog { PublishStateId = previewedPortalLog.PublishStateId, PortalId = portalId, IsPortalPublished = previewedPortalLog.IsPortalPublished });

                int currentPublishedVersionId = 0;
                if (HelperUtility.IsNotNull(znodePublishPortalLog))
                    currentPublishedVersionId = znodePublishPortalLog.PublishPortalLogId;

                ZnodeLogging.LogMessage("currentPublishedVersionId: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, currentPublishedVersionId);
                WebStoreEntity previousPublishedWebstore = PreviewHelper.GetWebstore(portalId, targetPublishState, localeId);

                try
                {
                    if (HelperUtility.IsNotNull(previousPublishedWebstore))
                    {
                        PreviewHelper.CopyAllCMSEntitiesToVersion(previousPublishedWebstore.VersionId, currentPublishedVersionId, portalId, targetPublishState);

                        foreach (WidgetSliderBannerEntity sliderBanner in sliderBannersToPublish)
                        {
                            sliderBanner.VersionId = currentPublishedVersionId;

                            if (_cmsWidgetSliderBannerMongoRepository.GetEntityList(Query.And(
                                Query<WidgetSliderBannerEntity>.EQ(x => x.SliderId, cmsSliderId),
                                Query<WidgetSliderBannerEntity>.EQ(x => x.PortalId, portalId),
                                Query<WidgetSliderBannerEntity>.EQ(x => x.VersionId, currentPublishedVersionId),
                                Query<WidgetSliderBannerEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                ), true).Count > 0)
                            {
                                _cmsWidgetSliderBannerMongoRepository.UpdateEntity(Query.And(
                                    Query<WidgetSliderBannerEntity>.EQ(x => x.SliderId, cmsSliderId),
                                    Query<WidgetSliderBannerEntity>.EQ(x => x.PortalId, portalId),
                                    Query<WidgetSliderBannerEntity>.EQ(x => x.VersionId, currentPublishedVersionId),
                                    Query<WidgetSliderBannerEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                    ), sliderBanner, true);
                            }
                            else
                            {
                                //If not already created in the target publish state.
                                WidgetSliderBannerEntity sliderToCreate = new WidgetSliderBannerEntity
                                {
                                    AutoPlay = sliderBanner.AutoPlay,
                                    AutoplayHoverPause = sliderBanner.AutoplayHoverPause,
                                    AutoplayTimeOut = sliderBanner.AutoplayTimeOut,
                                    Id = ObjectId.GenerateNewId(),
                                    LocaleId = sliderBanner.LocaleId,
                                    MappingId = sliderBanner.MappingId,
                                    Navigation = sliderBanner.Navigation,
                                    PortalId = portalId,
                                    SliderBanners = sliderBanner.SliderBanners,
                                    SliderId = cmsSliderId,
                                    TransactionStyle = sliderBanner.TransactionStyle,
                                    Type = sliderBanner.Type,
                                    TypeOFMapping = sliderBanner.TypeOFMapping,
                                    VersionId = currentPublishedVersionId,
                                    WidgetsKey = sliderBanner.WidgetsKey,
                                    WidgetSliderBannerId = sliderBanner.WidgetSliderBannerId
                                };
                                _cmsWidgetSliderBannerMongoRepository.Create(sliderToCreate);
                            }
                        }

                         var clearCacheInitializer = new ZnodeEventNotifier<List<WidgetSliderBannerEntity>>(sliderBannersToPublish);

                        isSuccessful = true;
                    }
                    else
                    {
                        throw new ZnodeException(ErrorCodes.StoreNotPublishedForAssociatedEntity, string.Format(Api_Resources.StoreAssociatedWithEntityNotPublished, "banner slider"));
                    }
                }
                catch (ZnodeException ex)
                {
                    isSuccessful = false;
                    RollbackPublishedSliderBanner(ex, currentPublishedVersionId);
                    exception = ex;
                }
                catch (MongoException ex)
                {
                    isSuccessful = false;
                    RollbackPublishedSliderBanner(ex, currentPublishedVersionId);
                    exception = new ZnodeException(ErrorCodes.MongoExceptionDuringPublish, Api_Resources.MongoExceptionMessageDuringPublish);
                }
                catch (System.Data.Entity.Core.EntityException ex)
                {
                    isSuccessful = false;
                    RollbackPublishedSliderBanner(ex, currentPublishedVersionId);
                    exception = new ZnodeException(ErrorCodes.EntityExceptionDuringPublish, Api_Resources.EntityExceptionMessageDuringPublish);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    isSuccessful = false;
                    RollbackPublishedSliderBanner(ex, currentPublishedVersionId);
                    exception = new ZnodeException(ErrorCodes.SQLExceptionDuringPublish, Api_Resources.SQLExceptionMessageDuringPublish);
                }
                catch (Exception ex)
                {
                    isSuccessful = false;
                    RollbackPublishedSliderBanner(ex, currentPublishedVersionId);
                    exception = new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, Api_Resources.GenericExceptionMessageDuringPublish);
                }

                if (!isSuccessful)
                    throw exception;
                else
                {
                    UpdatePublishSliderStatus(cmsSliderId, targetPublishState, true);

                    //Delete previous published copy from mongo at last.
                    if (HelperUtility.IsNotNull(previousPublishedWebstore))
                        PreviewHelper.DeleteAllCMSEntitiesFromMongo(previousPublishedWebstore.VersionId, null, true);

                    PreviewHelper.CreatePreviewLog(targetPublishState.ToString(), currentPublishedVersionId, previousPublishedWebstore.VersionId, cmsSliderId, "Banner slider", localeId, GetLocaleName(localeId));

                    return true;
                }
            }
            else
            {
                //Skipping publish for a portal - locale pair which does not have any banner sliders associated.
                return true;
            }
        }

        private bool CopyEntityFromSQLToMongo(string id, int portalId, int localeId, ZnodePublishStatesEnum targetPublishState, ref bool breakWithSuccess, int additionalParameter = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            int cmsSliderId = int.Parse(id);
            bool isSuccessful = false;
            IEnumerable<WidgetSliderBannerEntity> publishedSliderBanner = null;
            Exception exception = new Exception();

            List<WidgetSliderBannerEntity> sliderBannersToPublish = GetSliderBannersToPublish(cmsSliderId, portalId, 0, localeId);
            ZnodeLogging.LogMessage("sliderBannersToPublish list count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, sliderBannersToPublish?.Count);

            if (HelperUtility.IsNotNull(sliderBannersToPublish) && sliderBannersToPublish.Any())
            {
                WebStoreEntity existingWebstoreCopy = PreviewHelper.GetWebstore(portalId, targetPublishState, localeId);

                if (HelperUtility.IsNotNull(existingWebstoreCopy))
                {
                    int sourceVersionId = existingWebstoreCopy.VersionId;

                    ZnodePublishPortalLog znodePublishPortalLog = null;

                    ZnodePublishPortalLog previewedPortalLog = _publishPortalLogRepository.Table?.Where(x => x.PortalId == portalId)?.ToArray()?.LastOrDefault();
                    if (HelperUtility.IsNotNull(previewedPortalLog))
                        znodePublishPortalLog = _publishPortalLogRepository.Insert(
                            new ZnodePublishPortalLog { PublishStateId = previewedPortalLog.PublishStateId, PortalId = portalId, IsPortalPublished = previewedPortalLog.IsPortalPublished });

                    int currentPublishedVersionId = 0;
                    if (HelperUtility.IsNotNull(znodePublishPortalLog))
                        currentPublishedVersionId = znodePublishPortalLog.PublishPortalLogId;

                    ZnodeLogging.LogMessage("currentPublishedVersionId: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, currentPublishedVersionId);

                    try
                    {
                        if (HelperUtility.IsNotNull(existingWebstoreCopy))
                        {
                            sliderBannersToPublish.ForEach(x => x.VersionId = currentPublishedVersionId);

                            //Check if the configuration data is found for locale in the current iteration.
                            if (HelperUtility.IsNotNull(sliderBannersToPublish) && sliderBannersToPublish.Any())
                            {
                                _cmsWidgetSliderBannerMongoRepository.Create(sliderBannersToPublish);

                                publishedSliderBanner = sliderBannersToPublish;

                                var clearCacheInitializer = new ZnodeEventNotifier<List<WidgetSliderBannerEntity>>(sliderBannersToPublish);

                                isSuccessful = true;
                            }
                            else
                            {
                                throw new ZnodeException(ErrorCodes.EntityNotFoundDuringPublish, string.Format(Api_Resources.EntityNotFoundMessageDuringPublish, "banner slider"));
                            }
                        }
                        else
                        {
                            throw new ZnodeException(ErrorCodes.StoreNotPublishedForAssociatedEntity, string.Format(Api_Resources.StoreAssociatedWithEntityNotPublished, "banner slider"));
                        }
                    }
                    catch (ZnodeException ex)
                    {
                        isSuccessful = false;
                        RollbackPublishedSliderBanner(ex, currentPublishedVersionId);
                        exception = ex;
                    }
                    catch (MongoException ex)
                    {
                        isSuccessful = false;
                        RollbackPublishedSliderBanner(ex, currentPublishedVersionId);
                        exception = new ZnodeException(ErrorCodes.MongoExceptionDuringPublish, Api_Resources.MongoExceptionMessageDuringPublish);
                    }
                    catch (System.Data.Entity.Core.EntityException ex)
                    {
                        isSuccessful = false;
                        RollbackPublishedSliderBanner(ex, currentPublishedVersionId);
                        exception = new ZnodeException(ErrorCodes.EntityExceptionDuringPublish, Api_Resources.EntityExceptionMessageDuringPublish);
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        isSuccessful = false;
                        RollbackPublishedSliderBanner(ex, currentPublishedVersionId);
                        exception = new ZnodeException(ErrorCodes.SQLExceptionDuringPublish, Api_Resources.SQLExceptionMessageDuringPublish);
                    }
                    catch (Exception ex)
                    {
                        isSuccessful = false;
                        RollbackPublishedSliderBanner(ex, currentPublishedVersionId);
                        exception = new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, Api_Resources.GenericExceptionMessageDuringPublish);
                    }

                    if (!isSuccessful)
                        throw exception;
                    else
                    {
                        if (HelperUtility.IsNotNull(publishedSliderBanner) && publishedSliderBanner.Any())
                        {
                            PreviewHelper.DeleteRecords(_cmsWidgetSliderBannerMongoRepository, log_cmsWidgetSliderBannerMongoRepository, sourceVersionId, Query.And(
                                        Query<WidgetSliderBannerEntity>.EQ(x => x.PortalId, portalId),
                                        Query<WidgetSliderBannerEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                    ));

                            //Copy rest of the entities as it is for the new version.
                            PreviewHelper.CopyAllCMSEntitiesToVersion(sourceVersionId, currentPublishedVersionId, portalId, targetPublishState);

                            UpdatePublishSliderStatus(cmsSliderId, targetPublishState, true);

                            //Delete previous published copy from mongo at last.
                            if (HelperUtility.IsNotNull(existingWebstoreCopy))
                                PreviewHelper.DeleteAllCMSEntitiesFromMongo(sourceVersionId, null, true);

                            PreviewHelper.CreatePreviewLog(targetPublishState.ToString(), currentPublishedVersionId, sourceVersionId, cmsSliderId, "Banner slider", localeId, GetLocaleName(localeId));
                        }

                        return true;
                    }
                }
                else
                {
                    throw new ZnodeException(ErrorCodes.StoreNotPublishedForAssociatedEntity, string.Format(Api_Resources.StoreAssociatedWithEntityNotPublished, "banner slider"));
                }
            }
            else
            {
                //Skipping publish for a portal - locale pair which does not have any banner sliders associated.
                breakWithSuccess = true;
                return true;
            }
        }

        private void RollbackPublishedSliderBanner(Exception ex, int versionToRollback)
        {
            ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
            PreviewHelper.DeleteAllCMSEntitiesFromMongo(versionToRollback);
        }

        private List<WidgetSliderBannerEntity> GetSliderBannersToPublish(int cmsSliderId, int portalId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            int userId = GetLoginUserId();

            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter("@PortalId", portalId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@CMSSliderId", cmsSliderId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@LocaleId", localeId, ParameterDirection.Input, SqlDbType.Int);
            ZnodeLogging.LogMessage("portalId, userId, cmsSliderId and localeId to get CMSWidgetSliderBanner list: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, userId, cmsSliderId, localeId });
            DataSet data = executeSpHelper.GetSPResultInDataSet("Znode_GetCMSWidgetSliderBanner");
           
            //Clear cache call
            List<WidgetSliderBannerEntity> entities = new List<WidgetSliderBannerEntity>();

            //Convert Dataset to entity
            foreach (DataRow row in data.Tables[0].Rows)
            {
                WidgetSliderBannerEntity entity = HelperUtility.ConvertXMLStringToModel<WidgetSliderBannerEntity>(Convert.ToString(row["ReturnXML"]));
                entity.VersionId = versionId;
                entity.Id = ObjectId.GenerateNewId();
                entity.LocaleId = localeId;
                entity.PortalId = portalId;
                entities.Add(entity);
            }

            ZnodeLogging.LogMessage("WidgetSliderBannerEntity list count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, entities?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return entities;
        }

        private List<int> DataExistsForAvailablePortals(IEnumerable<int> portalIds, string cmsSliderId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            List<int> availableForPortals = new List<int>();
            int cmsSliderIdInt = Convert.ToInt32(cmsSliderId);
            foreach (int portalId in portalIds)
            {
                if (GetSliderBannersToPublish(cmsSliderIdInt, portalId, 0, 0).Count > 0)
                {
                    availableForPortals.Add(portalId);
                }
            }

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return availableForPortals;
        }

        //Get where clause for Slider banner id.
        private EntityWhereClauseModel GetWhereClauseForSliderBannerId(int sliderBannerId)
            => DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(new FilterCollection { new FilterTuple(ZnodeCMSSliderBannerEnum.CMSSliderBannerId.ToString(), ProcedureFilterOperators.Equals, sliderBannerId.ToString()) }.ToFilterDataCollection());

        //Get the locale id from filters.
        private static int GetLocaleId(FilterCollection filters)
        {
            int localeId = 0;
            if (filters?.Count > 0)
            {
                Int32.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(ZnodeLocaleEnum.LocaleId.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out localeId);
                filters.RemoveAll(x => x.FilterName.Equals(ZnodeLocaleEnum.LocaleId.ToString(), StringComparison.InvariantCultureIgnoreCase));
            }
            return localeId;
        }

        //Save the data into slider banner locale.
        private void SaveInSliderBannerLocale(BannerModel bannerModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            //Get the banner locale.
            ZnodeCMSSliderBannerLocale bannerLocale = _cmsSliderBannerLocaleRepository.Table.Where(x => x.CMSSliderBannerId == bannerModel.CMSSliderBannerId && x.LocaleId == bannerModel.LocaleId)?.FirstOrDefault();

            //Locale present for that banner the update the entry else create the entry.
            if (HelperUtility.IsNotNull(bannerLocale))
            {
                bannerLocale.Description = bannerModel.Description;
                bannerLocale.ImageAlternateText = bannerModel.ImageAlternateText;
                bannerLocale.Title = bannerModel.Title;
                bannerLocale.MediaId = bannerModel.MediaId;
                bannerLocale.ButtonLabelName = bannerModel.ButtonLabelName;
                bannerLocale.ButtonLink = bannerModel.ButtonLink;

                ZnodeLogging.LogMessage(_cmsSliderBannerLocaleRepository.Update(bannerLocale)
                    ? Admin_Resources.SuccessSliderLocaleBannerUpdated : Admin_Resources.ErrorSliderLocaleBannerUpdate, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            }
            else
                ZnodeLogging.LogMessage(_cmsSliderBannerLocaleRepository.Insert(GetSliderBannerLocaleEntity(bannerModel))?.CMSSliderBannerLocaleId > 0
                    ? Admin_Resources.SuccessSliderLocaleBannerInserted : Admin_Resources.ErrorSliderLocaleBannerInsert, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
        }

        //Get the slider banner locale entity.
        private static ZnodeCMSSliderBannerLocale GetSliderBannerLocaleEntity(BannerModel bannerModel)
            => new ZnodeCMSSliderBannerLocale
            {
                LocaleId = bannerModel.LocaleId,
                CMSSliderBannerId = bannerModel.CMSSliderBannerId,
                Description = bannerModel.Description,
                ImageAlternateText = bannerModel.ImageAlternateText,
                ButtonLabelName = bannerModel.ButtonLabelName,
                ButtonLink = bannerModel.ButtonLink,
                Title = bannerModel.Title,
                MediaId = bannerModel.MediaId
            };

        //Sets the properties of banner model.
        private void SetBannerModel(ZnodeCMSSliderBanner sliderBannerEntity, BannerModel model, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            //Get the slider nammer locale entity.
            ZnodeCMSSliderBannerLocale sliderBannerLocale = sliderBannerEntity.ZnodeCMSSliderBannerLocales.Where(x => x.CMSSliderBannerId == sliderBannerEntity.CMSSliderBannerId && x.LocaleId == localeId)?.FirstOrDefault();

            //Gets slider banner default locale details for the locale other than dafault if it doesn't have its localised details.
            if (!Equals(localeId, GetDefaultLocaleId()) && HelperUtility.IsNull(sliderBannerLocale))
                sliderBannerLocale = sliderBannerEntity.ZnodeCMSSliderBannerLocales.Where(x => x.CMSSliderBannerId == sliderBannerEntity.CMSSliderBannerId && x.LocaleId == GetDefaultLocaleId())?.FirstOrDefault();

            ZnodeLogging.LogMessage("ZnodeCMSSliderBannerLocale and ZnodeCMSSliderBanner with Ids respectively: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { sliderBannerLocale?.CMSSliderBannerLocaleId, sliderBannerEntity?.CMSSliderBannerId });
            //Set the properties.
            if (HelperUtility.IsNotNull(sliderBannerLocale))
            {
                model.Description = sliderBannerLocale.Description;
                model.ImageAlternateText = sliderBannerLocale.ImageAlternateText;
                model.LocaleId = sliderBannerLocale.LocaleId;
                model.Title = sliderBannerLocale.Title;
                model.MediaId = sliderBannerLocale.MediaId;
                model.ButtonLabelName = sliderBannerLocale.ButtonLabelName;
                model.ButtonLink = sliderBannerLocale.ButtonLink;

                if (model.MediaId > 0)
                {
                    IZnodeRepository<ZnodeMedia> _znodeMediaRepository = new ZnodeRepository<ZnodeMedia>();
                    model.FileName = _znodeMediaRepository.Table.FirstOrDefault(x => x.MediaId == model.MediaId)?.FileName;
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
        }

        //This method is used to get media path from media Id.
        private string GetMediaPath(BannerModel model)
        {
            if(HelperUtility.IsNotNull(model))
            {
                IMediaManagerServices mediaService = GetService<IMediaManagerServices>();
                MediaManagerModel mediaData = mediaService.GetMediaByID(Convert.ToInt32(model?.MediaId), null);
                model.MediaPath = HelperUtility.IsNotNull(mediaData) ? mediaData.MediaServerThumbnailPath : string.Empty;
            }
            return model.MediaPath;
        }

        private void PublishWidgetSliderBanner(int cmsSliderId, int userId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            int portalId = 0;
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter("@PortalId", portalId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@CMSSliderId", cmsSliderId, ParameterDirection.Input, SqlDbType.Int);
            ZnodeLogging.LogMessage("portalId, userId and cmsSliderId to get CMSWidgetSliderBanner: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, userId, cmsSliderId });
            DataSet data = executeSpHelper.GetSPResultInDataSet("Znode_GetCMSWidgetSliderBanner");
            _cmsWidgetSliderBannerMongoRepository.DeleteByQuery(Query<WidgetSliderBannerEntity>.EQ(pr => pr.SliderId, cmsSliderId));

            PublishHelper.SaveToMongo<WidgetSliderBannerEntity>(data, _cmsWidgetSliderBannerMongoRepository);

            UpdatePublishSliderStatus(cmsSliderId, true);

            //Clear cache call
            List<WidgetSliderBannerEntity> entites = new List<WidgetSliderBannerEntity>();

            //Convert Dataset to entity
            foreach (DataRow row in data.Tables[0].Rows)
            {
                WidgetSliderBannerEntity entity = HelperUtility.ConvertXMLStringToModel<WidgetSliderBannerEntity>(Convert.ToString(row["ReturnXML"]));
                entites.Add(entity);
            }
            ZnodeLogging.LogMessage("WidgetSliderBannerEntity list count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, entites?.Count);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            var clearCacheInitializer = new ZnodeEventNotifier<List<WidgetSliderBannerEntity>>(entites);
        }

        [Obsolete("To be discontinued in one of the upcoming versions.")]
        private void UpdatePublishSliderStatus(int cmsSliderId, bool isPublished)
        => _cmsSliderRepository.Update(new ZnodeCMSSlider
        {
            CMSSliderId = cmsSliderId,
            Name = _cmsSliderRepository.Table.Where(x => x.CMSSliderId == cmsSliderId)?.FirstOrDefault().Name,
            IsPublished = isPublished
        });

        private void UpdatePublishSliderStatus(int cmsSliderId, ZnodePublishStatesEnum targetPublishState, bool isPublished)
        => _cmsSliderRepository.Update(new ZnodeCMSSlider
        {
            CMSSliderId = cmsSliderId,
            Name = _cmsSliderRepository.Table.Where(x => x.CMSSliderId == cmsSliderId)?.FirstOrDefault().Name,
            IsPublished = isPublished,
            PublishStateId = isPublished ? (byte)targetPublishState : (byte)ZnodePublishStatesEnum.DRAFT
        });

        private IEnumerable<int> GetPublishedUserPortal()
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            IEnumerable<int> result = new List<int>();

            IEnumerable<int> availablePortalsForCurrentUser = GetAvailablePortals()?.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)?.Select(x => int.Parse(x));
            IEnumerable<int> publishedPortals = GetPublishedPortals()?.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)?.Select(x => int.Parse(x));
            
            if (HelperUtility.IsNotNull(availablePortalsForCurrentUser))
            {
                result = availablePortalsForCurrentUser.Intersect(publishedPortals);
            }

            ZnodeLogging.LogMessage("PublishedUserPortal list: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, result);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return result;
        }

        #endregion
    }
}
