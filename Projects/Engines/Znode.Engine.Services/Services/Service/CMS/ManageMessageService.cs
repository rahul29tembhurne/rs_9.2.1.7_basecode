﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Observer;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Resources;

namespace Znode.Engine.Services
{
    public partial class ManageMessageService : BaseService, IManageMessageService
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodeCMSMessage> _cmsMessageRepository;
        private readonly IZnodeRepository<ZnodeCMSPortalMessage> _cmsPortalMessageRepository;
        private readonly IZnodeRepository<ZnodeCMSMessageKey> _cmsMessageKeyRepository;
        private readonly IZnodeRepository<ZnodeCMSArea> _cmsAreaRepository;
        private readonly IZnodeRepository<ZnodePublishPortalLog> _publishPortalLogRepository;
        private readonly IMongoRepository<MessageEntity> _cmsMessageMongoRepository;
        private readonly IMongoRepository<GlobalMessageEntity> _cmsGlobalMessageMongoRepository;
        private readonly IMongoRepository<_LogMessageEntity> log_cmsMessageMongoRepository;
        #endregion

        #region Constructor
        public ManageMessageService()
        {
            _cmsMessageRepository = new ZnodeRepository<ZnodeCMSMessage>();
            _cmsMessageKeyRepository = new ZnodeRepository<ZnodeCMSMessageKey>();
            _cmsPortalMessageRepository = new ZnodeRepository<ZnodeCMSPortalMessage>();
            _cmsAreaRepository = new ZnodeRepository<ZnodeCMSArea>();
            _publishPortalLogRepository = new ZnodeRepository<ZnodePublishPortalLog>();
            _cmsMessageMongoRepository = new MongoRepository<MessageEntity>(WebstoreVersionId);
            _cmsGlobalMessageMongoRepository = new MongoRepository<GlobalMessageEntity>(GlobalVersionId);
            log_cmsMessageMongoRepository = new MongoRepository<_LogMessageEntity>();
        }
        #endregion

        #region Manage Messages
        //Get ManageMessage list.
        public virtual ManageMessageListModel GetManageMessages(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel generated to get manage message list: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());

            //Get value of localeId from filter.         
            int localeId = Convert.ToInt32(string.IsNullOrEmpty(filters.Find(x => x.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower())?.Item3) ? GetDefaultLocaleId().ToString() : filters.Find(x => x.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower())?.Item3);
            ZnodeLogging.LogMessage("localeId generated from filter: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, localeId);

            if (!Equals(filters.Find(x => x.Item1 == ZnodeLocaleEnum.LocaleId.ToString().ToLower())?.Item1, ZnodeLocaleEnum.LocaleId.ToString().ToLower()))
                filters.Add(new FilterTuple("LocaleId", ProcedureFilterOperators.Equals, DefaultGlobalConfigSettingHelper.Locale));

            filters.RemoveAll(x => x.FilterName.Equals(ZnodeLocaleEnum.LocaleId.ToString(), StringComparison.InvariantCultureIgnoreCase));

            //Bind the Filter conditions for the authorized portal access.
            
            //BindUserPortalFilter(ref filters);
            IZnodeViewRepository<ManageMessageModel> objStoredProc = new ZnodeViewRepository<ManageMessageModel>();

            //SP parameters
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter("@LocaleId", localeId, ParameterDirection.Input, DbType.Int32);

            IList<ManageMessageModel> manageMessageEntityList = objStoredProc.ExecuteStoredProcedureList("Znode_GetManageMessagelist @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT,@LocaleId", 4, out pageListModel.TotalRowCount);
            ZnodeLogging.LogMessage("manageMessageEntityList count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, manageMessageEntityList?.Count);

            ManageMessageListModel manageMessageListModel = new ManageMessageListModel { ManageMessages = manageMessageEntityList?.ToList() };

            manageMessageListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return manageMessageListModel;
        }

        //Create new ManageMessage.
        public virtual ManageMessageModel CreateManageMessage(ManageMessageModel manageMessageModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (HelperUtility.IsNull(manageMessageModel))
                throw new ZnodeException(ErrorCodes.NullModel, ZnodeConstant.NullModelError);

            ZnodeLogging.LogMessage("Input parameter manageMessageModel: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, manageMessageModel);

            //If LocaleId is zero then get default locale.
            if (manageMessageModel.LocaleId == 0)
                manageMessageModel.LocaleId = GetDefaultLocaleId();

            //Get comma seprated string of ids for portal and area.
            string portalIds = string.Join(",", manageMessageModel?.PortalIds);

            IZnodeViewRepository<View_ReturnBooleanWithMessage> objStoredProc = new ZnodeViewRepository<View_ReturnBooleanWithMessage>();
            objStoredProc.SetParameter("PortalIds", portalIds, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter(ZnodeCMSMessageKeyEnum.MessageKey.ToString(), manageMessageModel.MessageKey, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("Description", manageMessageModel.Message, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter(ZnodeLocaleEnum.LocaleId.ToString(), manageMessageModel.LocaleId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodeUserEnum.UserId.ToString(), GetLoginUserId(), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodeCMSMessageEnum.CMSMessageId.ToString(), 0, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodeCMSMessageKeyEnum.CMSMessageKeyId.ToString(), 0, ParameterDirection.Input, DbType.Int32);
            if (HelperUtility.IsNotNull(manageMessageModel.MessageTag))
                objStoredProc.SetParameter(ZnodeCMSMessageKeyEnum.MessageTag.ToString(), manageMessageModel.MessageTag, ParameterDirection.Input, DbType.String);
            else
                objStoredProc.SetParameter(ZnodeCMSMessageKeyEnum.MessageTag.ToString(), DBNull.Value, ParameterDirection.Input, DbType.String);

            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);

            View_ReturnBooleanWithMessage output = objStoredProc.ExecuteStoredProcedureList("Znode_InsertManageMessages @PortalIds,@MessageKey,@MessageTag,@Description,@LocaleId,@UserId,@CMSMessageId,@CMSMessageKeyId,@Status OUT")?.FirstOrDefault();

            if (Equals(output?.MessageDetails, "Successful"))
            {
                manageMessageModel.CMSMessageId = output.Id;
                manageMessageModel.CMSMessageKeyId = output.Id;
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return manageMessageModel;
            }
            else if (HelperUtility.IsNotNull(output) && output.MessageDetails.Contains("Is Already Exists"))
                throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.MessageKeyAlreadyExist);
            else
            {
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return manageMessageModel;
            }
        }

        //Get ManageMessage details.
        public virtual ManageMessageModel GetManageMessage(ManageMessageMapperModel manageMessageMapperModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter manageMessageMapperModel: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, manageMessageMapperModel);

            if (manageMessageMapperModel?.CMSMessageKeyId > 0)
            {
                // Get all details of manage messages associated with PortalId and CMSMessageKey.
                ManageMessageModel manageMessageDetails = GetManageMessageForLocaleId(manageMessageMapperModel);

                if (HelperUtility.IsNotNull(manageMessageDetails))
                    return manageMessageDetails;
                else
                {
                    //if message details are not available for locale id passed then get messages for default localeId.
                    int localeId = manageMessageMapperModel.LocaleId;
                    manageMessageMapperModel.LocaleId = GetDefaultLocaleId();
                    manageMessageDetails = GetManageMessageForLocaleId(manageMessageMapperModel);
                    manageMessageDetails.CMSMessageId = !Equals(localeId, manageMessageMapperModel.LocaleId) ? 0 : manageMessageMapperModel.CMSMessageId;
                    if (HelperUtility.IsNotNull(manageMessageDetails))
                    {
                        ZnodeLogging.LogMessage("manageMessageDetails to be returned: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, manageMessageDetails);
                        return manageMessageDetails;
                    }
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return new ManageMessageModel();
        }

        //Update the ManageMessage.
        public virtual bool UpdateManageMessage(ManageMessageModel manageMessageModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            if (HelperUtility.IsNull(manageMessageModel))
                throw new ZnodeException(ErrorCodes.NullModel, ZnodeConstant.NullModelError);

            ZnodeLogging.LogMessage("Input parameter manageMessageModel: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, manageMessageModel);
            //If LocaleId is zero then get default locale.
            if (manageMessageModel.LocaleId == 0)
                manageMessageModel.LocaleId = GetDefaultLocaleId();

            IZnodeViewRepository<View_ReturnBooleanWithMessage> objStoredProc = new ZnodeViewRepository<View_ReturnBooleanWithMessage>();
            objStoredProc.SetParameter("PortalIds", manageMessageModel.PortalId, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter(ZnodeCMSMessageKeyEnum.MessageKey.ToString(), manageMessageModel.MessageKey, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("Description", manageMessageModel.Message, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter(ZnodeLocaleEnum.LocaleId.ToString(), manageMessageModel.LocaleId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodeUserEnum.UserId.ToString(), GetLoginUserId(), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodeCMSMessageEnum.CMSMessageId.ToString(), manageMessageModel.CMSMessageId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter(ZnodeCMSMessageKeyEnum.CMSMessageKeyId.ToString(), manageMessageModel.CMSMessageKeyId, ParameterDirection.Input, DbType.Int32);
            if (HelperUtility.IsNotNull(manageMessageModel.MessageTag))
                objStoredProc.SetParameter(ZnodeCMSMessageKeyEnum.MessageTag.ToString(), manageMessageModel.MessageTag, ParameterDirection.Input, DbType.String);
            else
                objStoredProc.SetParameter(ZnodeCMSMessageKeyEnum.MessageTag.ToString(), DBNull.Value, ParameterDirection.Input, DbType.String);

            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);

            View_ReturnBooleanWithMessage output = objStoredProc.ExecuteStoredProcedureList("Znode_InsertManageMessages @PortalIds,@MessageKey,@MessageTag,@Description,@LocaleId,@UserId,@CMSMessageId,@CMSMessageKeyId,@Status OUT")?.FirstOrDefault();

            if (Equals(output?.MessageDetails, "Successful"))
            {
                var portalIds = manageMessageModel.PortalId.ToString().Split(',');
                foreach (var portalId in portalIds)
                {
                    if (!portalId.Equals("0"))
                        BatchUpdateContentBlockPreviewStatus(manageMessageModel.CMSMessageKeyId.Value, Convert.ToInt32(portalId), manageMessageModel.LocaleId.Value, ZnodePublishStatesEnum.DRAFT, true);
                    else
                    {
                        BatchUpdateContentBlockPreviewStatus(manageMessageModel.CMSMessageKeyId.Value, null, manageMessageModel.LocaleId.Value, ZnodePublishStatesEnum.DRAFT, true);
                    }
                }
                return true;
            }
            else
                return false;
        }

        //Delete ManageMessage.
        public virtual bool DeleteManageMessage(ParameterModel cmsManageMessageId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter cmsManageMessageId to delete manage message: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, cmsManageMessageId?.Ids);

            IZnodeViewRepository<View_ReturnBooleanWithMessage> objStoredProc = new ZnodeViewRepository<View_ReturnBooleanWithMessage>();
            objStoredProc.SetParameter(FilterKeys.MessageIds, cmsManageMessageId.Ids, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter(FilterKeys.Status, null, ParameterDirection.Output, DbType.Int32);
            int status = 0;
            var messages = objStoredProc.ExecuteStoredProcedureList("Znode_DeleteManageMessages @MessageIds, @Status OUT", 1, out status);
            if (messages.FirstOrDefault().Status.Value)
            {
                foreach (var message in messages)
                {
                    _cmsGlobalMessageMongoRepository.DeleteByQuery(Query.And(Query<MessageEntity>.EQ(x => x.MessageKey, message.MessageDetails)),true);
                }
                ZnodeLogging.LogMessage(Admin_Resources.SuccessManageMessagesDelete, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return true;
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorManageMessagesDelete, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return false;
            }
        }

        [Obsolete("To be discontinued in one of the upcoming versions.")]
        //Publish the manage message
        public virtual PublishedModel PublishManageMessage(ContentPageParameterModel contentPageParameterModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter contentPageParameterModel: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, contentPageParameterModel);
            try
            {
                return PublishContentBlockMessage(contentPageParameterModel);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                return new PublishedModel { IsPublished = false, ErrorMessage = "Failed to publish." };
            }
        }

        //Publish the manage message
        public virtual PublishedModel PublishManageMessage(string cmsMessageKeyId, int portalId, int localeId = 0, string targetPublishState = null, bool takeFromDraftFirst = false)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters cmsMessageKeyId, portalId, localeId, targetPublishState, takeFromDraftFirst: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { cmsMessageKeyId, portalId, localeId, targetPublishState, takeFromDraftFirst });

            bool result = false;

            if (string.IsNullOrEmpty(cmsMessageKeyId) || string.IsNullOrWhiteSpace(cmsMessageKeyId))
                throw new ZnodeException(ErrorCodes.InvalidEntityPassedDuringPublish, Api_Resources.InvalidEntityMessageDuringPublish);

            PublishProcessor processor = new PublishProcessor();

            result = processor.PublishIndividualCMSEntity(cmsMessageKeyId, portalId, CopyEntityWithinMongo, CopyEntityFromSQLToMongo, localeId, targetPublishState, takeFromDraftFirst);

            return new PublishedModel { IsPublished = result, ErrorMessage = result ? String.Empty : Api_Resources.GenericExceptionMessageDuringPublish };
        }

        #endregion

        #region Private Methods
        private bool CopyEntityWithinMongo(string id, WebStoreEntity existingWebstoreCopy, ZnodePublishStatesEnum targetPublishState, int additionalParameter = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new { id = id, targetPublishState = targetPublishState, additionalParameter = additionalParameter, LocaleId = existingWebstoreCopy?.LocaleId, PortalId = existingWebstoreCopy?.PortalId });

            int cmsMessageKeyId = int.Parse(id);

            bool isSuccessful = false;
            Exception exception = new Exception();

            int sourceVersionId = existingWebstoreCopy.VersionId;

            int portalId = existingWebstoreCopy.PortalId;
            int localeId = existingWebstoreCopy.LocaleId;

            ZnodePublishStatesEnum sourceContentState;
            Enum.TryParse(existingWebstoreCopy.PublishState, true, out sourceContentState);

            ZnodePublishPortalLog znodePublishPortalLog = null;

            ZnodePublishPortalLog previewedPortalLog = _publishPortalLogRepository.Table?.Where(x => x.PortalId == portalId)?.ToArray()?.LastOrDefault();
            if (HelperUtility.IsNotNull(previewedPortalLog))
                znodePublishPortalLog = _publishPortalLogRepository.Insert(
                    new ZnodePublishPortalLog { PublishStateId = previewedPortalLog.PublishStateId, PortalId = portalId, IsPortalPublished = previewedPortalLog.IsPortalPublished });

            int currentPublishedVersionId = 0;
            if (HelperUtility.IsNotNull(znodePublishPortalLog))
                currentPublishedVersionId = znodePublishPortalLog.PublishPortalLogId;

            WebStoreEntity previousPublishedWebstore = PreviewHelper.GetWebstore(portalId, targetPublishState, localeId);

            try
            {
                if (HelperUtility.IsNotNull(previousPublishedWebstore))
                {
                    PreviewHelper.CopyAllCMSEntitiesToVersion(previousPublishedWebstore.VersionId, currentPublishedVersionId, portalId, targetPublishState);

                    string messageKey = _cmsMessageKeyRepository.Table.Where(x => x.CMSMessageKeyId == cmsMessageKeyId).FirstOrDefault()?.MessageKey;

                    //Content Blocks
                    List<MessageEntity> messagesToCopy = _cmsMessageMongoRepository.GetEntityList(Query.And(
                        Query<MessageEntity>.EQ(x => x.MessageKey, messageKey),
                        Query<MessageEntity>.EQ(x => x.PortalId, portalId),
                        Query<MessageEntity>.EQ(x => x.VersionId, sourceVersionId),
                        Query<MessageEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)), true);

                    foreach (MessageEntity message in messagesToCopy)
                    {
                        message.VersionId = currentPublishedVersionId;

                        if (_cmsMessageMongoRepository.GetEntityList(Query.And(
                            Query<MessageEntity>.EQ(x => x.MessageKey, messageKey),
                            Query<MessageEntity>.EQ(x => x.PortalId, portalId),
                            Query<MessageEntity>.EQ(x => x.VersionId, currentPublishedVersionId),
                            Query<MessageEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                            ), true).Count > 0)
                        {
                            _cmsMessageMongoRepository.UpdateEntity(Query.And(
                                Query<MessageEntity>.EQ(x => x.MessageKey, messageKey),
                                Query<MessageEntity>.EQ(x => x.PortalId, portalId),
                                Query<MessageEntity>.EQ(x => x.VersionId, currentPublishedVersionId),
                                Query<MessageEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                ), message, true);
                        }
                        else
                        {
                            //Creating a new entry if the existing version of same publish state doesn't already have it.
                            message.Id = ObjectId.GenerateNewId();
                            _cmsMessageMongoRepository.Create(message);
                        }
                        var clearCacheInitializer = new ZnodeEventNotifier<MessageEntity>(message);
                    }
                    isSuccessful = true;
                }
                else
                {
                    throw new ZnodeException(ErrorCodes.StoreNotPublishedForAssociatedEntity, string.Format(Api_Resources.StoreAssociatedWithEntityNotPublished, "content block"));
                }
            }
            catch (ZnodeException ex)
            {
                isSuccessful = false;
                RollbackPublishedContentBlock(ex, currentPublishedVersionId);
                exception = ex;
            }
            catch (MongoException ex)
            {
                isSuccessful = false;
                RollbackPublishedContentBlock(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.MongoExceptionDuringPublish, Api_Resources.MongoExceptionMessageDuringPublish);
            }
            catch (System.Data.Entity.Core.EntityException ex)
            {
                isSuccessful = false;
                RollbackPublishedContentBlock(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.EntityExceptionDuringPublish, Api_Resources.EntityExceptionMessageDuringPublish);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                isSuccessful = false;
                RollbackPublishedContentBlock(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.SQLExceptionDuringPublish, Api_Resources.SQLExceptionMessageDuringPublish);
            }
            catch (Exception ex)
            {
                isSuccessful = false;
                RollbackPublishedContentBlock(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, Api_Resources.GenericExceptionMessageDuringPublish);
            }

            if (!isSuccessful)
                throw exception;
            else
            {
                BatchUpdateContentBlockPreviewStatus(cmsMessageKeyId, portalId, localeId, targetPublishState, true);

                //Delete previous published copy from mongo at last.
                if (HelperUtility.IsNotNull(previousPublishedWebstore))
                    PreviewHelper.DeleteAllCMSEntitiesFromMongo(previousPublishedWebstore.VersionId, null, true);

                PreviewHelper.CreatePreviewLog(targetPublishState.ToString(), currentPublishedVersionId, previousPublishedWebstore.VersionId, cmsMessageKeyId, "Content block", localeId, GetLocaleName(localeId));
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return true;
            }
        }

        private bool CopyEntityFromSQLToMongo(string id, int portalId, int localeId, ZnodePublishStatesEnum targetPublishState, ref bool breakWithSuccess, int additionalParameter = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters id, portalId, localeId, targetPublishState, additionalParameter: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { id, portalId, localeId, targetPublishState, additionalParameter });

            int cmsMessageKeyId = int.Parse(id);

            bool isSuccessful = false;
            Exception exception = new Exception();
            List<MessageEntity> messagesToPublish = null;

            WebStoreEntity existingWebstoreCopy = PreviewHelper.GetWebstore(portalId, targetPublishState, localeId);
            ZnodeLogging.LogMessage("LocaleId and VersionId properties of existingWebstoreCopy: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { existingWebstoreCopy?.LocaleId, existingWebstoreCopy?.VersionId });

            if (HelperUtility.IsNotNull(existingWebstoreCopy))
            {
                int sourceVersionId = existingWebstoreCopy.VersionId;

                ZnodePublishPortalLog znodePublishPortalLog = null;

                ZnodePublishPortalLog previewedPortalLog = _publishPortalLogRepository.Table?.Where(x => x.PortalId == portalId)?.ToArray()?.LastOrDefault();
                if (HelperUtility.IsNotNull(previewedPortalLog))
                    znodePublishPortalLog = _publishPortalLogRepository.Insert(
                        new ZnodePublishPortalLog { PublishStateId = previewedPortalLog.PublishStateId, PortalId = portalId, IsPortalPublished = previewedPortalLog.IsPortalPublished });

                int currentPublishedVersionId = 0;
                if (HelperUtility.IsNotNull(znodePublishPortalLog))
                    currentPublishedVersionId = znodePublishPortalLog.PublishPortalLogId;

                try
                {
                    if (HelperUtility.IsNotNull(existingWebstoreCopy))
                    {
                        messagesToPublish = GetMessagesToPublish(cmsMessageKeyId, portalId, currentPublishedVersionId, existingWebstoreCopy.LocaleId).ToList();
                        ZnodeLogging.LogMessage("messagesToPublish list count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, messagesToPublish?.Count);

                        //Check if the configuration data is found for locale in the current iteration.
                        if (HelperUtility.IsNotNull(messagesToPublish) && messagesToPublish.Any())
                        {
                            messagesToPublish.ForEach(x => x.Id = ObjectId.GenerateNewId());

                            _cmsMessageMongoRepository.Create(messagesToPublish);
                            foreach (MessageEntity messageEntity in messagesToPublish)
                            {
                                var clearCacheInitializer = new ZnodeEventNotifier<MessageEntity>(messageEntity);
                            }
                            isSuccessful = true;
                        }
                        else
                        {
                            throw new ZnodeException(ErrorCodes.EntityNotFoundDuringPublish, string.Format(Api_Resources.EntityNotFoundMessageDuringPublish, "content block"));
                        }
                    }
                    else
                    {
                        throw new ZnodeException(ErrorCodes.StoreNotPublishedForAssociatedEntity, string.Format(Api_Resources.StoreAssociatedWithEntityNotPublished, "content block"));
                    }
                }
                catch (ZnodeException ex)
                {
                    isSuccessful = false;
                    RollbackPublishedContentBlock(ex, currentPublishedVersionId);
                    exception = ex;
                }
                catch (MongoException ex)
                {
                    isSuccessful = false;
                    RollbackPublishedContentBlock(ex, currentPublishedVersionId);
                    exception = new ZnodeException(ErrorCodes.MongoExceptionDuringPublish, Api_Resources.MongoExceptionMessageDuringPublish);
                }
                catch (System.Data.Entity.Core.EntityException ex)
                {
                    isSuccessful = false;
                    RollbackPublishedContentBlock(ex, currentPublishedVersionId);
                    exception = new ZnodeException(ErrorCodes.EntityExceptionDuringPublish, Api_Resources.EntityExceptionMessageDuringPublish);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    isSuccessful = false;
                    RollbackPublishedContentBlock(ex, currentPublishedVersionId);
                    exception = new ZnodeException(ErrorCodes.SQLExceptionDuringPublish, Api_Resources.SQLExceptionMessageDuringPublish);
                }
                catch (Exception ex)
                {
                    isSuccessful = false;
                    RollbackPublishedContentBlock(ex, currentPublishedVersionId);
                    exception = new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, Api_Resources.GenericExceptionMessageDuringPublish);
                }

                if (!isSuccessful)
                    throw exception;
                else
                {
                    if (HelperUtility.IsNotNull(messagesToPublish) && messagesToPublish.Count > 0)
                    {
                        PreviewHelper.DeleteRecords(_cmsMessageMongoRepository, log_cmsMessageMongoRepository, sourceVersionId, Query.And(
                                        Query<MessageEntity>.In(x => x.MessageKey, messagesToPublish.Select(s => s.MessageKey)),
                                        Query<MessageEntity>.EQ(x => x.PortalId, portalId),
                                        Query<MessageEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                    ));

                        //Copy rest of the entities as it is for the new version.
                        PreviewHelper.CopyAllCMSEntitiesToVersion(sourceVersionId, currentPublishedVersionId, portalId, targetPublishState);

                        BatchUpdateContentBlockPreviewStatus(cmsMessageKeyId, portalId, localeId, targetPublishState, true);

                        //Delete previous published copy from mongo at last.
                        if (HelperUtility.IsNotNull(existingWebstoreCopy))
                            PreviewHelper.DeleteAllCMSEntitiesFromMongo(sourceVersionId, null, true);

                        PreviewHelper.CreatePreviewLog(targetPublishState.ToString(), currentPublishedVersionId, sourceVersionId, cmsMessageKeyId, "Content block", localeId, GetLocaleName(localeId));

                        ZnodeEventNotifier<MessageEntity> eventNotifier;
                        foreach (MessageEntity message in messagesToPublish)
                            eventNotifier = new ZnodeEventNotifier<MessageEntity>(message);
                    }
                    ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                    return true;
                }
            }
            else
            {
                throw new ZnodeException(ErrorCodes.StoreNotPublishedForAssociatedEntity, string.Format(Api_Resources.StoreAssociatedWithEntityNotPublished, "content block"));
            }
        }

        private void RollbackPublishedContentBlock(Exception ex, int versionToRollback)
        {
            ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
            PreviewHelper.DeleteAllCMSEntitiesFromMongo(versionToRollback);
        }

        private IQueryable<MessageEntity> GetMessagesToPublish(int cmsMessageKeyId, int portalId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Input parameters cmsMessageKeyId, portalId, versionId, localeId: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { cmsMessageKeyId, portalId, versionId, localeId });

            int defaultLocaleId = GetDefaultLocaleId();

            ZnodeLogging.LogMessage("defaultLocaleId returned from GetDefaultLocaleId: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, defaultLocaleId);

            IQueryable<MessageEntity> dataSelectQuery = (from contentblockmessage in _cmsMessageRepository.Table
                                                         join template in new ZnodeRepository<ZnodeCMSPortalMessage>().Table on contentblockmessage.CMSMessageId equals template.CMSMessageId
                                                         join cmsmessagekey in _cmsMessageKeyRepository.Table on template.CMSMessageKeyId equals cmsmessagekey.CMSMessageKeyId
                                                         where cmsmessagekey.CMSMessageKeyId == cmsMessageKeyId && (contentblockmessage.LocaleId == localeId || contentblockmessage.LocaleId == defaultLocaleId) && template.PortalId == portalId
                                                         select new MessageEntity
                                                         {
                                                             CMSMessageId = contentblockmessage.CMSMessageId,
                                                             LocaleId = contentblockmessage.LocaleId,
                                                             PortalId = template.PortalId,
                                                             MessageKey = cmsmessagekey.MessageKey,
                                                             Message = contentblockmessage.Message,
                                                             VersionId = versionId
                                                         });

            IQueryable<MessageEntity> resultsWithPassedLocale = dataSelectQuery.Where(x => x.LocaleId == localeId);
            IQueryable<MessageEntity> resultWithDefaultLocale = dataSelectQuery.Where(x => x.LocaleId == defaultLocaleId);

            dataSelectQuery = resultsWithPassedLocale.Concat(resultWithDefaultLocale.Where(x => !resultsWithPassedLocale.Any(y => y.MessageKey == x.MessageKey)));

            return dataSelectQuery;
        }

        //if message details are not available for locale id passed then get messages for default localeId.
        private ManageMessageModel GetManageMessageForLocaleId(ManageMessageMapperModel manageMessageMapperModel)
        {
            IZnodeViewRepository<ManageMessageModel> getManageMessageForEdit = new ZnodeViewRepository<ManageMessageModel>();
            getManageMessageForEdit.SetParameter(ZnodeCMSPortalMessageEnum.PortalId.ToString(), manageMessageMapperModel.PortalId, ParameterDirection.Input, DbType.String);
            getManageMessageForEdit.SetParameter(ZnodeCMSMessageKeyEnum.CMSMessageKeyId.ToString(), manageMessageMapperModel.CMSMessageKeyId, ParameterDirection.Input, DbType.Int32);
            getManageMessageForEdit.SetParameter(ZnodeLocaleEnum.LocaleId.ToString(), manageMessageMapperModel.LocaleId, ParameterDirection.Input, DbType.Int32);
            getManageMessageForEdit.SetParameter(ZnodeCMSMessageEnum.CMSMessageId.ToString(), manageMessageMapperModel.CMSMessageId, ParameterDirection.Input, DbType.String);
            ManageMessageModel manageMessageDetails = getManageMessageForEdit.ExecuteStoredProcedureList("Znode_GetManageMessageForEdit @PortalId, @CMSMessageKeyId,@LocaleId,@CMSMessageId")?.FirstOrDefault();
            return manageMessageDetails;
        }

        //Publish message with update and delete
        private PublishedModel PublishContentBlockMessage(ContentPageParameterModel contentPageParameterModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            int cmsMessageKeyId = 0;
            //Currently only single cmmessage Id be recieved for publish Parameter model is used for future purpose
            Int32.TryParse(contentPageParameterModel.Ids, out cmsMessageKeyId);

            int portalId = contentPageParameterModel.portalId;

            int? publishPortalLogId = _publishPortalLogRepository.Table.OrderByDescending(x => x.PublishPortalLogId).FirstOrDefault(x => x.IsPortalPublished == true && x.PortalId == portalId)?.PublishPortalLogId;
            ZnodeLogging.LogMessage("publishPortalLogId: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, publishPortalLogId);

            if (publishPortalLogId != null && publishPortalLogId > 0)
            {
                IEnumerable<MessageEntity> contentBlockMessages = GetContentMessage(cmsMessageKeyId, portalId, contentPageParameterModel.localeId, publishPortalLogId.Value);
                ZnodeLogging.LogMessage("contentBlockMessages list count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, contentBlockMessages?.Count());

                foreach (MessageEntity contentBlockMessage in contentBlockMessages)
                {
                    _cmsMessageMongoRepository.DeleteByQuery(Query.And(
                                    Query<MessageEntity>.EQ(pr => pr.MessageKey, contentBlockMessage?.MessageKey),
                                     Query<MessageEntity>.EQ(pr => pr.PortalId, portalId),
                                    Query<MessageEntity>.EQ(pr => pr.LocaleId, contentBlockMessage?.LocaleId),
                                    Query<MessageEntity>.EQ(pr => pr.VersionId, publishPortalLogId)));

                    //Update the publish status
                    UpdatePublishContentBlockStatus(cmsMessageKeyId, contentBlockMessage?.Message, Convert.ToInt32(contentBlockMessage?.LocaleId), true);

                    if (HelperUtility.IsNotNull(contentBlockMessage))
                        _cmsMessageMongoRepository.Create(contentBlockMessage);

                    //Clear cache call
                    var clearCacheInitializer = new ZnodeEventNotifier<MessageEntity>(contentBlockMessage);
                }
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return new PublishedModel { IsPublished = true, ErrorMessage = string.Empty };
            }
            else
            {
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.ErrorStoreAssociatedWithContentBlockPublish };
            }
        }

        //Get content message
        private IEnumerable<MessageEntity> GetContentMessage(int cmsMessageKeyId, int portalId, int localeId, int versionid)
        {
            var temp = (from contentblockmessage in _cmsMessageRepository.Table
                        join template in new ZnodeRepository<ZnodeCMSPortalMessage>().Table on contentblockmessage.CMSMessageId equals template.CMSMessageId
                        join cmsmessagekey in _cmsMessageKeyRepository.Table on template.CMSMessageKeyId equals cmsmessagekey.CMSMessageKeyId
                        where cmsmessagekey.CMSMessageKeyId == cmsMessageKeyId && (localeId > 0 ? contentblockmessage.LocaleId == localeId : true) && template.PortalId == portalId
                        select new MessageEntity
                        {
                            CMSMessageId = contentblockmessage.CMSMessageId,
                            LocaleId = contentblockmessage.LocaleId,
                            PortalId = template.PortalId,
                            MessageKey = cmsmessagekey.MessageKey,
                            Message = contentblockmessage.Message,
                            VersionId = versionid
                        });
            return temp;
        }

        [Obsolete("To be discontinued in one of the upcoming versions.")]
        //Update the publish status
        private void UpdatePublishContentBlockStatus(int cmsMessageKeyId, string message, int localeId, bool isPortalPublished)
         => _cmsMessageRepository.Update(new ZnodeCMSMessage
         {
             CMSMessageId = cmsMessageKeyId,
             Message = message,
             LocaleId = localeId,
             IsPublished = isPortalPublished
         });

        private void BatchUpdateContentBlockPreviewStatus(int cmsMessageKeyId, int? portalId, int localeId, ZnodePublishStatesEnum targetPublishState, bool isPortalPublished)
        {
            ZnodeLogging.LogMessage("Input parameters cmsMessageKeyId, portalId, localeId, targetPublishState, isPortalPublished: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { cmsMessageKeyId, portalId, localeId, targetPublishState, isPortalPublished });

            List<ZnodeCMSMessage> messages = (from message in _cmsMessageRepository.Table
                                              join portalMessage in _cmsPortalMessageRepository.Table on message.CMSMessageId equals portalMessage.CMSMessageId
                                              join messagekey in _cmsMessageKeyRepository.Table on portalMessage.CMSMessageKeyId equals messagekey.CMSMessageKeyId
                                              where portalMessage.CMSMessageKeyId == cmsMessageKeyId && portalMessage.PortalId == portalId && message.LocaleId == localeId
                                              select message).ToList();
            ZnodeLogging.LogMessage("messages list count: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, messages?.Count);

            if (HelperUtility.IsNotNull(messages) && messages.Count > 0)
            {
                messages.ForEach(x => x.PublishStateId = isPortalPublished ? (byte)targetPublishState : (byte)ZnodePublishStatesEnum.DRAFT);
                _cmsMessageRepository.BatchUpdate(messages);
            }
        }
        #endregion
    }
}
