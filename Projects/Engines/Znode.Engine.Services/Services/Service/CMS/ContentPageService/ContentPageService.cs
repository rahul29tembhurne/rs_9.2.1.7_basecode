﻿ using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Observer;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using Znode.Engine.Services.Helper;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public partial class ContentPageService : BaseService, IContentPageService
    {
        #region Private Variable
        private readonly IZnodeRepository<ZnodeCMSContentPage> _contentPageRepository;
        private readonly IZnodeRepository<ZnodeCMSContentPageGroup> _contentPageGroup;
        private readonly IZnodeRepository<ZnodeCMSContentPageGroupLocale> _contentPageGroupLocale;
        private readonly IZnodeRepository<ZnodeCMSSEODetail> _cmsSeoDetailRepository;
        private readonly IZnodeRepository<ZnodeCMSSEOType> _cmsSeoTypeRepository;
        private readonly IZnodeRepository<ZnodeCMSSEODetailLocale> _cmsSeoDetailLocaleRepository;
        private readonly IZnodeRepository<View_GetContentPageDetails> _viewGetContentPageDetailsRepository;
        private readonly IZnodeRepository<ZnodeCMSContentPageGroupMapping> _contentPageGroupMapping;
        private readonly IZnodeRepository<ZnodePublishPortalLog> _publishPortalLogRepository;
        private readonly IZnodeRepository<ZnodeCMSContentPagesLocale> _contentPagesLocaleRepository;
        private readonly IZnodeRepository<ZnodeCMSWidgetProduct> _cmsWidgetProductRepository;



        private readonly IMongoRepository<ContentPageConfigEntity> _cmsContentPageMongoRepository;
        private readonly IMongoRepository<TextWidgetEntity> _cmsTextWidgetMongoRepository;
        private readonly IMongoRepository<SearchWidgetEntity> _cmsSearchWidgetMongoRepository;
        private readonly IMongoRepository<SeoEntity> _seoMongoRepository;
        private readonly IMongoRepository<WidgetProductEntity> _cmsWidgetProductMongoRepository;
        private readonly IMongoRepository<WidgetTitleEntity> _cmsWidgetTitleMongoRepository;
        private readonly IMongoRepository<WidgetSliderBannerEntity> _cmsWidgetSliderBannerMongoRepository;

        private readonly IMongoRepository<_LogContentPageConfigEntity> log_cmsContentPageMongoRepository;
        private readonly IMongoRepository<_LogTextWidgetEntity> log_cmsTextWidgetMongoRepository;
        private readonly IMongoRepository<_LogSearchWidgetEntity> log_cmsSearchWidgetMongoRepository;
        private readonly IMongoRepository<_LogSeoEntity> log_seoMongoRepository;

        #endregion

        #region Constructor
        public ContentPageService()
        {
            _contentPageRepository = new ZnodeRepository<ZnodeCMSContentPage>();
            _contentPageGroup = new ZnodeRepository<ZnodeCMSContentPageGroup>();
            _contentPageGroupLocale = new ZnodeRepository<ZnodeCMSContentPageGroupLocale>();
            _cmsSeoDetailRepository = new ZnodeRepository<ZnodeCMSSEODetail>();
            _cmsSeoTypeRepository = new ZnodeRepository<ZnodeCMSSEOType>();
            _cmsSeoDetailLocaleRepository = new ZnodeRepository<ZnodeCMSSEODetailLocale>();
            _viewGetContentPageDetailsRepository = new ZnodeRepository<View_GetContentPageDetails>();
            _contentPageGroupMapping = new ZnodeRepository<ZnodeCMSContentPageGroupMapping>();
            _publishPortalLogRepository = new ZnodeRepository<ZnodePublishPortalLog>();
            _contentPagesLocaleRepository = new ZnodeRepository<ZnodeCMSContentPagesLocale>();
             _cmsWidgetProductRepository = new ZnodeRepository<ZnodeCMSWidgetProduct>();

            _cmsContentPageMongoRepository = new MongoRepository<ContentPageConfigEntity>(WebstoreVersionId);
            _cmsTextWidgetMongoRepository = new MongoRepository<TextWidgetEntity>(WebstoreVersionId);
            _cmsSearchWidgetMongoRepository = new MongoRepository<SearchWidgetEntity>(WebstoreVersionId);
            _seoMongoRepository = new MongoRepository<SeoEntity>(WebstoreVersionId);
            _cmsWidgetProductMongoRepository = new MongoRepository<WidgetProductEntity>();
            _cmsWidgetTitleMongoRepository = new MongoRepository<WidgetTitleEntity>();
            _cmsWidgetSliderBannerMongoRepository = new MongoRepository<WidgetSliderBannerEntity>();

            log_cmsContentPageMongoRepository = new MongoRepository<_LogContentPageConfigEntity>();
            log_cmsTextWidgetMongoRepository = new MongoRepository<_LogTextWidgetEntity>();
            log_cmsSearchWidgetMongoRepository = new MongoRepository<_LogSearchWidgetEntity>();
            log_seoMongoRepository = new MongoRepository<_LogSeoEntity>();
        }
        #endregion

        #region Public Methods
        #region Content Page

        //Create Content page.
        public virtual ContentPageModel CreateContentPage(ContentPageModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel,Admin_Resources.ModelCanNotBeNull);

            //Checks if the content page name already exist for current portal id.
            if (IsPageNameAvailable(model.PageName, model.PortalId))
                throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.ErrorPageNameExistsInDatabase);

            ZnodeLogging.LogMessage("ContentPageModel with PageName and PortalId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { model?.PageName, model?.PortalId });

            //Check if seo name already exists.
            if (IsSeoNameAvailable(model))
                throw new ZnodeException(ErrorCodes.SEOUrlAlreadExists, Admin_Resources.ErrorSEONameExistsInDatabase);

            IZnodeViewRepository<ContentPageModel> objStoredProc = new ZnodeViewRepository<ContentPageModel>();
            objStoredProc.SetParameter("ContentPageXML", ToXML(model), ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("UserId", GetLoginUserId(), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
            int status = 0;
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            return objStoredProc.ExecuteStoredProcedureList("Znode_InsertUpdateContentPage @ContentPageXML,@Status OUT, @UserId ", 1, out status)?.FirstOrDefault();

        }

        //Get list of content pages.
        public virtual ContentPageListModel GetContentPageList(FilterCollection filters, NameValueCollection expands, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            ReplaceSortKeys(ref sorts);

            string localeId = filters.Find(x => string.Equals(x.FilterName, ZnodeCMSContentPageGroupLocaleEnum.LocaleId.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Item3;
            filters.RemoveAll(x => x.FilterName == FilterKeys.LocaleId);

            localeId = string.IsNullOrEmpty(localeId) ? "0" : localeId;

            //Bind the Filter conditions for the authorized portal access.
            BindUserPortalFilter(ref filters);
            //Bind the Filter, sorts & Paging details.
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            ZnodeLogging.LogMessage("pageListModel to set SP parameters:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { pageListModel?.ToDebugString()});

            IZnodeViewRepository<ContentPageModel> objStoredProc = new ZnodeViewRepository<ContentPageModel>();
            //SP parameters
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", null, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter("@LocaleId", localeId, ParameterDirection.Input, DbType.Int32);

            ContentPageListModel listModel = new ContentPageListModel();
            //SP Call
            List<ContentPageModel> contentPageList = objStoredProc.ExecuteStoredProcedureList("Znode_GetCMSContentPagesFolderDetails @WhereClause,@Rows,@PageNo,@Order_By,@RowCount OUT,@LocaleId", 4, out pageListModel.TotalRowCount)?.ToList();
            ZnodeLogging.LogMessage("contentPageList count:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPageList?.Count });

            listModel.ContentPageList = contentPageList?.Count > 0 ? contentPageList : new List<ContentPageModel>();

            listModel.BindPageListModel(pageListModel);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return listModel;
        }

        //Get Content Page by Static Page Id.
        public virtual ContentPageModel GetContentPage(FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            int contentPageID = GetContentPageId(filters);
            ZnodeLogging.LogMessage("contentPageID returned from GetContentPageId method:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPageID });

            if (contentPageID < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorContentPageIdLessThan1);

            int localeId;
            Int32.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(FilterKeys.LocaleId, StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out localeId);

            //Get all the information related to content page.
            ContentPageModel contentPageModel = GetContentPageInformation(contentPageID, localeId);
            ZnodeLogging.LogMessage("contentPageModel with content page ID returned from GetContentPageInformation:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPageModel?.CMSContentPagesId });

            if (IsNotNull(contentPageModel))
            {
                //Set the template information.
                SetTemplateInformation(contentPageModel);

                IZnodeRepository<ZnodeCMSContentPagesProfile> _cmsContentPagesProfile = new ZnodeRepository<ZnodeCMSContentPagesProfile>();
                contentPageModel.ProfileIds = string.Join(",", _cmsContentPagesProfile?.Table?.Where(x => x.CMSContentPagesId == contentPageID)?.Select(x => x.ProfileId));
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return contentPageModel;
            }
            throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidData);
        }

        //Update Content Page.
        public virtual bool UpdateContentPage(ContentPageModel contentPageModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (IsNull(contentPageModel))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ModelCanNotBeNull);

            if (contentPageModel.CMSContentPagesId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorContentPageIdLessThan1);

            ZnodeLogging.LogMessage("contentPageModel with content page ID :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPageModel?.CMSContentPagesId });

            string contentPageXML = ToXML(contentPageModel);

            IZnodeViewRepository<ContentPageModel> objStoredProc = new ZnodeViewRepository<ContentPageModel>();
            objStoredProc.SetParameter("ContentPageXML", contentPageXML, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("UserId", GetLoginUserId(), ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
            int status = 0;
            objStoredProc.ExecuteStoredProcedureList("Znode_InsertUpdateContentPage @ContentPageXML,@Status OUT, @UserId ", 1, out status);

            if (!Equals(contentPageModel.SEOUrl, contentPageModel.OldSEOURL))
            {
                SEODetailsModel seoDetailsModel = new SEODetailsModel() { SEOUrl = contentPageModel.SEOUrl, OldSEOURL = contentPageModel.OldSEOURL, CMSContentPagesId = contentPageModel.CMSContentPagesId, PortalId = contentPageModel.PortalId, CMSSEOTypeId = 3, IsRedirect = contentPageModel.IsRedirect };
                SEORedirectUrlHelper.CreateUrlRedirect(seoDetailsModel);
            }
            UpdateContentPageAfterPublish(contentPageModel.CMSContentPagesId, 0, false);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            return status > 0;
        }

        //Delete Content Page.
        public virtual bool DeleteContentPage(ParameterModel contentPageIds)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (IsNull(contentPageIds) || string.IsNullOrEmpty(contentPageIds.Ids))
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorContentPageIdLessThan1);

            ZnodeLogging.LogMessage("contentPageIds to be deleted :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPageIds?.Ids });

            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();

            //SP paramters.
            objStoredProc.SetParameter(ZnodeCMSContentPageEnum.CMSContentPagesId.ToString(), contentPageIds.Ids, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
            int status = 0;
            objStoredProc.ExecuteStoredProcedureList("Znode_DeleteContentPage @CMSContentPagesId,  @Status OUT", 1, out status);
            if (status == 1)
            {
                ZnodeLogging.LogMessage(String.Format(Admin_Resources.SuccessContentPageDelete,contentPageIds.Ids), ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return true;
            }
            else
            {
                throw new ZnodeException(ErrorCodes.AssociationDeleteError, String.Format(Admin_Resources.ErrorContentPageDelete, contentPageIds.Ids));
            }
        }

        [Obsolete("To be discontinued in upcoming versions.")]
        //Publish CMS Content Page
        public virtual PublishedModel PublishContentPageBySEO(ContentPageParameterModel parameterModel)
        {
            IZnodeRepository<ZnodeCMSSEOType> _seoTypeRepository = new ZnodeRepository<ZnodeCMSSEOType>();
            ISEOService seoservice = GetService<ISEOService>();

            if (IsNull(parameterModel) || String.IsNullOrEmpty(parameterModel.Ids))
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorContentPageIdLessThan1);
            try
            {
                int userId = GetLoginUserId();
                int contentPageId = Convert.ToInt32(parameterModel.Ids);
                int portalId = _contentPageRepository.Table.FirstOrDefault(o => o.CMSContentPagesId == contentPageId).PortalId;
                int? cmsSEOTypeId = _seoTypeRepository.Table.FirstOrDefault(seoType => seoType.Name.Equals(ZnodeConstant.ContentPage))?.CMSSEOTypeId;
                int? versionId = _publishPortalLogRepository.Table.OrderByDescending(x => x.PublishPortalLogId).FirstOrDefault(x => x.IsPortalPublished == true && x.PortalId == portalId)?.PublishPortalLogId;
                if (versionId != null && versionId > 0 && cmsSEOTypeId != null)
                {
                    int publishPortalLogId = versionId.Value;
                    IZnodeRepository<ZnodeCMSContentPagesProfile> contentPagesProfileRepository = new ZnodeRepository<ZnodeCMSContentPagesProfile>();
                    
                    ContentPageConfigEntity ContentPageConfigconfiguration = GetContentPageConfigEntity(contentPageId, portalId, publishPortalLogId, parameterModel.localeId);
                    
                    //Get porfiles associated to content pages
                    ZnodeCMSContentPagesProfile contentPagesProfile =
                        contentPagesProfileRepository.Table.Where(x => x.CMSContentPagesId == contentPageId).FirstOrDefault();
                    
                    //Profiled Associated to content page
                    int?[] profileId = new int?[1] { contentPagesProfile?.ProfileId };
                    ContentPageConfigconfiguration.ProfileId = profileId;

                    //Delete published content page data.
                    DeleteContentPagePublishedData(portalId, publishPortalLogId, contentPageId, parameterModel.localeId);
                   
                    //Publishing content page data.
                    _cmsContentPageMongoRepository.Create(ContentPageConfigconfiguration);
                    
                    //Delete the published data.
                    DeleteContentPageWidgetPublishedData(portalId, publishPortalLogId, contentPageId, parameterModel.localeId);
                   
                    //Publish Text widget data.
                    PublishTextWidgetConfiguration(portalId, userId, publishPortalLogId, contentPageId);
                    
                    //Publish the search widget data.
                    PublishSearchWidgetConfiguration(portalId, userId, publishPortalLogId, contentPageId, parameterModel.localeId);
                   
                    //Publish SEO for the 
                    seoservice.PublishBySEOCode(ContentPageConfigconfiguration.PageName, portalId, parameterModel.localeId, cmsSEOTypeId.Value);
                    
                    //Updating true flag for after publising content page
                    UpdateContentPageAfterPublish(contentPageId, true);


                    return new PublishedModel { IsPublished = true, ErrorMessage = ContentPageConfigconfiguration.IsActive ? String.Empty : Admin_Resources.SuccessContentPagePublish };
                }
                else
                {
                    return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.StoreAssociatedWithContentPageNotPublished };
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                if (ex.Message == Admin_Resources.ErrorSEOURLNull)
                    return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.ErrorPagePublish };
                else
                    return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.ErrorPublish };
            }
        }


        //Publish CMS Content Page
        [Obsolete]
        public virtual PublishedModel PublishContentPage(ContentPageParameterModel parameterModel)
        {
            IZnodeRepository<ZnodeCMSSEOType> _seoTypeRepository = new ZnodeRepository<ZnodeCMSSEOType>();
            ISEOService seoservice = GetService<ISEOService>();

            if (IsNull(parameterModel) || String.IsNullOrEmpty(parameterModel.Ids))
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.ErrorContentPageIdLessThan1);
            try
            {
                int userId = GetLoginUserId();
                int contentPageId = Convert.ToInt32(parameterModel.Ids);
                int portalId = _contentPageRepository.Table.FirstOrDefault(o => o.CMSContentPagesId == contentPageId).PortalId;
                int? cmsSEOTypeId = _seoTypeRepository.Table.FirstOrDefault(seoType => seoType.Name.Equals(ZnodeConstant.ContentPage))?.CMSSEOTypeId;
                int? versionId = _publishPortalLogRepository.Table.OrderByDescending(x => x.PublishPortalLogId).FirstOrDefault(x => x.IsPortalPublished == true && x.PortalId == portalId)?.PublishPortalLogId;
                if (versionId != null && versionId > 0 && cmsSEOTypeId != null)
                {
                    int publishPortalLogId = versionId.Value;
                    IZnodeRepository<ZnodeCMSContentPagesProfile> contentPagesProfileRepository = new ZnodeRepository<ZnodeCMSContentPagesProfile>();
                    ContentPageConfigEntity ContentPageConfigconfiguration = GetContentPageConfigEntity(contentPageId, portalId, publishPortalLogId, parameterModel.localeId);

                    //Get porfiles associated to content pages
                    ZnodeCMSContentPagesProfile contentPagesProfile =
                        contentPagesProfileRepository.Table.Where(x => x.CMSContentPagesId == contentPageId).FirstOrDefault();

                    //Profiled Associated to content page
                    int?[] profileId = new int?[1] { contentPagesProfile?.ProfileId };
                    ContentPageConfigconfiguration.ProfileId = profileId;

                    //Delete published content page data.
                    DeleteContentPagePublishedData(portalId, publishPortalLogId, contentPageId, parameterModel.localeId);
                   
                    //Publishing content page data.
                    _cmsContentPageMongoRepository.Create(ContentPageConfigconfiguration);
                   
                    //Delete the published data.
                    DeleteContentPageWidgetPublishedData(portalId, publishPortalLogId, contentPageId, parameterModel.localeId);
                    
                    //Publish Text widget data.
                    PublishTextWidgetConfiguration(portalId, userId, publishPortalLogId, contentPageId);
                    
                    //Publish the search widget data.
                    PublishSearchWidgetConfiguration(portalId, userId, publishPortalLogId, contentPageId, parameterModel.localeId);
                    
                    //Publish SEO for the 
                    seoservice.Publish(contentPageId.ToString(), portalId, parameterModel.localeId, cmsSEOTypeId.Value);
                   
                    //Updating true flag for after publising content page
                    UpdateContentPageAfterPublish(contentPageId, true);
                   
                    return new PublishedModel { IsPublished = true, ErrorMessage = ContentPageConfigconfiguration.IsActive ? String.Empty : Admin_Resources.SuccessContentPagePublish };
                }
                else
                {
                    return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.StoreAssociatedWithContentPageNotPublished };
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
                if (ex.Message == Admin_Resources.ErrorSEOURLNull)
                    return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.ErrorPagePublish };
                else
                    return new PublishedModel { IsPublished = false, ErrorMessage = Admin_Resources.ErrorPublish };
            }
        }

        public virtual PublishedModel PublishContentPage(int contentPageId, int portalId, int localeId = 0, string targetPublishState = null, bool takeFromDraftFirst = false)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters contentPageId, localeId, portalId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPageId, localeId, portalId });

            bool result = false;

            if (contentPageId < 1)
                throw new ZnodeException(ErrorCodes.InvalidEntityPassedDuringPublish, Api_Resources.InvalidEntityMessageDuringPublish);

            if (portalId <= 0)
                portalId = _contentPageRepository.Table.FirstOrDefault(o => o.CMSContentPagesId == contentPageId).PortalId;

            PublishProcessor processor = new PublishProcessor();

            result = processor.PublishIndividualCMSEntity(contentPageId.ToString(), portalId, CopyEntityWithinMongo, CopyEntityFromSQLToMongo, localeId, targetPublishState, takeFromDraftFirst);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return new PublishedModel { IsPublished = result, ErrorMessage = result ? String.Empty : Api_Resources.GenericExceptionMessageDuringPublish };
        }

        #endregion

        #region Tree
        // Gets content page tree.
        public virtual ContentPageTreeModel GetTreeNode()
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            IZnodeViewRepository<View_CMSContentFolder> objStoredProc = new ZnodeViewRepository<View_CMSContentFolder>();

            //Get all path from database.
            List<ContentPageTreeModel> list = objStoredProc.ExecuteStoredProcedureList("Znode_GetCMSContentFolder")?.Select(x => new ContentPageTreeModel
            {
                Text = x.Name,
                Id = x.CMSContentPageGroupId,
                ParentId = x.ParentCMSContentPageGroupId.GetValueOrDefault()
            }).ToList();

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            //Convert path to parent child pattern.
            return GetAllNode(list).FirstOrDefault();
        }

        // Add new folder.
        public virtual bool AddFolder(ContentPageFolderModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorContentPageFolderModelNull);

            ZnodeCMSContentPageGroup entity = _contentPageGroup.Insert(model.ToEntity<ZnodeCMSContentPageGroup>());
            ZnodeLogging.LogMessage("Inserted ContentPageFolderModel with CMSContentPageGroupId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { entity?.CMSContentPageGroupId });

            if (entity.CMSContentPageGroupId > 0)
            {
                model.CMSContentPageGroupId = entity.CMSContentPageGroupId;
                if (model.LocaleId == 0)
                    model.LocaleId = GetDefaultLocaleId();
                return _contentPageGroupLocale.Insert(model.ToEntity<ZnodeCMSContentPageGroupLocale>())?.CMSContentPageGroupLocaleId > 0;
            }
            return false;
        }

        // Rename the exsting folder.
        public virtual bool RenameFolder(ContentPageFolderModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorContentPageFolderModelNull);

            if (model.CMSContentPageGroupId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData,Admin_Resources.ErrorContentPageIdLessThan1);

            ZnodeLogging.LogMessage("ContentPageFolderModel with CMSContentPageGroupId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { model?.CMSContentPageGroupId });

            ZnodeCMSContentPageGroupLocale groupLocale = _contentPageGroupLocale.Table.Where(x => x.CMSContentPageGroupId == model.CMSContentPageGroupId)?.FirstOrDefault();

            if (IsNotNull(groupLocale))
            {
                groupLocale.Name = model.Code;
                _contentPageGroupLocale.Update(groupLocale);
                return true;
            }
            return false;
        }

        // Delete the exsting folders.
        public virtual bool DeleteFolder(ParameterModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Ids to be deleted:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { model?.Ids });

            if (string.IsNullOrEmpty(model?.Ids))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorContentPageGroupIdLessThan1);

            int status = 0;
            IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
            objStoredProc.SetParameter(ZnodeCMSContentPageGroupEnum.CMSContentPageGroupId.ToString(), model.Ids, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("Status", null, ParameterDirection.Output, DbType.Int32);
            IList<View_ReturnBoolean> deleteResult = objStoredProc.ExecuteStoredProcedureList("Znode_DeleteContentPageGroups @CMSContentPageGroupId, @Status OUT", 1, out status);
            ZnodeLogging.LogMessage("deleteResult count:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { deleteResult?.Count });

            if (deleteResult.FirstOrDefault().Status.Value)
            {
                ZnodeLogging.LogMessage(Admin_Resources.SuccessContentPageGroupDelete, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return true;
            }
            else
            {
                ZnodeLogging.LogMessage(Admin_Resources.ErrorContentPageGroupDelete, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return false;
            }
        }

        //Move Content Page folder.
        public virtual bool MoveContentPagesFolder(ContentPageFolderModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorContentPageFolderModelNull);

            ZnodeLogging.LogMessage("ContentPageFolderModel with CMSContentPageGroupId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { model?.CMSContentPageGroupId });

            bool result = false;
            ZnodeCMSContentPageGroup znodeCMSContentPageGroup = _contentPageGroup.Table.Where(x => x.CMSContentPageGroupId == model.CMSContentPageGroupId)?.FirstOrDefault();
            if(HelperUtility.IsNotNull(znodeCMSContentPageGroup))
            {
                znodeCMSContentPageGroup.ParentCMSContentPageGroupId = model.ParentCMSContentPageGroupId;
                result = _contentPageGroup.Update(znodeCMSContentPageGroup);
            }
            ZnodeLogging.LogMessage(result ? Admin_Resources.SuccessMoveFolder : Admin_Resources.ErrorMoveFolder, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return result;
        }

        //Move pages from one folder to another.
        public virtual bool MovePageToFolder(AddPagetoFolderModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorAddPagetoFolderModelNull);
            
                //Generate where clause for pages to be moved.
                string whereClauseForPage = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(GetWhereClauseForPageIds(model.PageIds)).WhereClause;
                 ZnodeLogging.LogMessage("Where clause generated for pages to be moved:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { whereClauseForPage });

                //Get list of pages of those page ids.
                List<ZnodeCMSContentPageGroupMapping> pageList = _contentPageGroupMapping.GetEntityList(whereClauseForPage, new List<string>() { "ZnodeCMSContentPage" }).ToList();
                if (IsNull(pageList) || pageList.FindAll(m => m.CMSContentPageGroupId == model.FolderId)?.Count > 0)
                    return false;

                bool result = true;
                string whereClauseForPagePathId = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(GetWhereClauseForPagePathId(model.FolderId)).WhereClause;

                //Get all pages within the folder.
                IList<ZnodeCMSContentPageGroupMapping> pageListInFolder = _contentPageGroupMapping.GetEntityList(whereClauseForPagePathId, new List<string>() { "ZnodeCMSContentPage" });
                ZnodeLogging.LogMessage("Count of pages within the folder :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { pageListInFolder?.Count });

                 List<string> pageIdsToRemove = new List<string>();
                 ZnodeLogging.LogMessage("pageIdsToRemove count : ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { pageIdsToRemove?.Count });

                //Check if that page is already present in that folder.
                foreach (ZnodeCMSContentPageGroupMapping page in pageList)
                {
                    if (IsNotNull(pageListInFolder.FirstOrDefault(x => x.ZnodeCMSContentPage.PageName == page.ZnodeCMSContentPage.PageName)))
                        pageIdsToRemove.Add(Convert.ToString(page.CMSContentPagesId));
                }

                //Remove pages from that folder and move to assigned folder.
                if (pageIdsToRemove.Count > 0)
                {
                    result = false;
                    List<string> allPageIds = model.PageIds.Split(',').ToList<string>();
                    if (allPageIds.Count == 1 && pageIdsToRemove.Count == 1)
                        return false;
                    else
                        whereClauseForPage = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(GetWhereClauseForPageIds(string.Join(",", allPageIds.Except(pageIdsToRemove).ToList()))).WhereClause;
                    pageList = _contentPageGroupMapping.GetEntityList(whereClauseForPage).ToList();
                }

                pageList.ForEach(x => x.CMSContentPageGroupId = model.FolderId);
                pageList.ForEach(x => _contentPageGroupMapping.Update(x));
                ZnodeLogging.LogMessage(Admin_Resources.SuccessPagesMovedFolder, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
                return result;
            

        }
        #endregion
        #endregion

        #region Private Methods

        private bool CopyEntityWithinMongo(string id, WebStoreEntity existingWebstoreCopy, ZnodePublishStatesEnum targetPublishState, int additionalParameter = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            int contentPageId = int.Parse(id);
            ZnodeLogging.LogMessage("contentPageId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPageId });

            bool isSuccessful = false;
            Exception exception = new Exception();

            int sourceVersionId = existingWebstoreCopy.VersionId;

            int portalId = existingWebstoreCopy.PortalId;
            int localeId = existingWebstoreCopy.LocaleId;
            ZnodePublishStatesEnum sourceContentState;
            Enum.TryParse(existingWebstoreCopy.PublishState, true, out sourceContentState);

            int? cmsSEOTypeId = _cmsSeoTypeRepository.Table.FirstOrDefault(seoType => seoType.Name.Equals(ZnodeConstant.ContentPage))?.CMSSEOTypeId;
            ZnodeLogging.LogMessage("sourceVersionId, portalId, cmsSEOTypeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { sourceVersionId, portalId, cmsSEOTypeId });

            ZnodePublishPortalLog znodePublishPortalLog = null;

            ZnodePublishPortalLog previewedPortalLog = _publishPortalLogRepository.Table?.Where(x => x.PortalId == portalId)?.ToArray()?.LastOrDefault();
            if (HelperUtility.IsNotNull(previewedPortalLog))
                znodePublishPortalLog = _publishPortalLogRepository.Insert(
                    new ZnodePublishPortalLog { PublishStateId = previewedPortalLog.PublishStateId, PortalId = portalId, IsPortalPublished = previewedPortalLog.IsPortalPublished });

            int currentPublishedVersionId = 0;
            if (IsNotNull(znodePublishPortalLog))
            {
                currentPublishedVersionId = znodePublishPortalLog.PublishPortalLogId;
                ZnodeLogging.LogMessage("currentPublishedVersionId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { currentPublishedVersionId });
            }
            WebStoreEntity previousPublishedWebstore = PreviewHelper.GetWebstore(portalId, targetPublishState, localeId);

            WebStoreEntity previousSourceContentWebstore = PreviewHelper.GetWebstore(portalId, sourceContentState, localeId);

            try
            {
                if (IsNotNull(previousPublishedWebstore))
                {
                    PreviewHelper.CopyAllCMSEntitiesToVersion(previousSourceContentWebstore.VersionId, currentPublishedVersionId, portalId, targetPublishState);

                    //Content Page Configuration.
                    ContentPageConfigEntity contentPageToCopy = _cmsContentPageMongoRepository.GetEntity(Query.And(
                        Query<ContentPageConfigEntity>.EQ(x => x.ContentPageId, contentPageId),
                        Query<ContentPageConfigEntity>.EQ(x => x.VersionId, sourceVersionId),
                        Query<ContentPageConfigEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)), true);

                    contentPageToCopy.VersionId = currentPublishedVersionId;

                    if (_cmsContentPageMongoRepository.GetEntityList(Query.And(
                        Query<ContentPageConfigEntity>.EQ(x => x.ContentPageId, contentPageId),
                        Query<ContentPageConfigEntity>.EQ(x => x.VersionId, currentPublishedVersionId),
                        Query<ContentPageConfigEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                        ), true).Count > 0)
                    {
                        _cmsContentPageMongoRepository.UpdateEntity(Query.And(
                            Query<ContentPageConfigEntity>.EQ(x => x.ContentPageId, contentPageId),
                            Query<ContentPageConfigEntity>.EQ(x => x.VersionId, currentPublishedVersionId),
                            Query<ContentPageConfigEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                            ), contentPageToCopy, true);
                    }
                    else
                    {
                        //Create new if not already exists in one of the older versions in the same publish state.
                        contentPageToCopy.Id = ObjectId.GenerateNewId();
                        _cmsContentPageMongoRepository.Create(contentPageToCopy);
                    }

                    //SEO Configuration.
                    if (cmsSEOTypeId.HasValue)
                    {
                        SeoEntity seoEntityToCopy = _seoMongoRepository.GetEntity(Query.And(
                            Query<SeoEntity>.EQ(x => x.SEOCode, contentPageToCopy.PageName),
                            Query<SeoEntity>.EQ(x => x.VersionId, sourceVersionId),
                            Query<SeoEntity>.EQ(x => x.CMSSEOTypeId, cmsSEOTypeId),
                            Query<SeoEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                            ), true);

                        if (IsNotNull(seoEntityToCopy))
                        {
                            seoEntityToCopy.VersionId = currentPublishedVersionId;

                            if (_seoMongoRepository.GetEntityList(Query.And(
                                Query<SeoEntity>.EQ(x => x.SEOCode, contentPageToCopy.PageName),
                                Query<SeoEntity>.EQ(x => x.VersionId, currentPublishedVersionId),
                                Query<SeoEntity>.EQ(x => x.CMSSEOTypeId, cmsSEOTypeId),
                                Query<SeoEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                ), true).Count > 0)
                            {
                                _seoMongoRepository.UpdateEntity(Query.And(
                                    Query<SeoEntity>.EQ(x => x.SEOCode, contentPageToCopy.PageName),
                                    Query<SeoEntity>.EQ(x => x.VersionId, currentPublishedVersionId),
                                    Query<SeoEntity>.EQ(x => x.CMSSEOTypeId, cmsSEOTypeId),
                                    Query<SeoEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                    ), seoEntityToCopy, true);
                            }
                            else
                            {
                                //Create new if not already exists in one of the older versions in the same publish state.
                                seoEntityToCopy.Id = ObjectId.GenerateNewId();
                                _seoMongoRepository.Create(seoEntityToCopy);
                            }
                        }
                    }

                    //Search Widget configuration.
                    SearchWidgetEntity searchWidgetToCopy = _cmsSearchWidgetMongoRepository.GetEntity(Query.And(
                        Query<SearchWidgetEntity>.EQ(x => x.VersionId, sourceVersionId),
                        Query<SearchWidgetEntity>.EQ(x => x.MappingId, contentPageId),
                        Query<SearchWidgetEntity>.EQ(x => x.TypeOFMapping, ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString()),
                        Query<SearchWidgetEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                        ), true);

                    if (IsNotNull(searchWidgetToCopy))
                    {
                        searchWidgetToCopy.VersionId = currentPublishedVersionId;

                        if (_cmsSearchWidgetMongoRepository.GetEntityList(Query.And(
                            Query<SearchWidgetEntity>.EQ(x => x.VersionId, currentPublishedVersionId),
                            Query<SearchWidgetEntity>.EQ(x => x.MappingId, contentPageId),
                            Query<SearchWidgetEntity>.EQ(x => x.TypeOFMapping, ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString()),
                            Query<SearchWidgetEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                            ), true).Count > 0)
                        {
                            _cmsSearchWidgetMongoRepository.UpdateEntity(Query.And(
                                Query<SearchWidgetEntity>.EQ(x => x.VersionId, currentPublishedVersionId),
                                Query<SearchWidgetEntity>.EQ(x => x.MappingId, contentPageId),
                                Query<SearchWidgetEntity>.EQ(x => x.TypeOFMapping, ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString()),
                                Query<SearchWidgetEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                ), searchWidgetToCopy, true);
                        }
                        else
                        {
                            //Create new if not already exists in one of the older versions in the same publish state.
                            searchWidgetToCopy.Id = ObjectId.GenerateNewId();
                            _cmsSearchWidgetMongoRepository.Create(searchWidgetToCopy);
                        }
                    }

                    //Text Widget configuration.
                    TextWidgetEntity textWidgetToCopy = _cmsTextWidgetMongoRepository.GetEntity(Query.And(
                        Query<TextWidgetEntity>.EQ(x => x.VersionId, sourceVersionId),
                        Query<TextWidgetEntity>.EQ(x => x.MappingId, contentPageId),
                        Query<TextWidgetEntity>.EQ(x => x.TypeOFMapping, ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString()),
                        Query<TextWidgetEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                        ), true);

                    if (IsNotNull(textWidgetToCopy))
                    {
                        textWidgetToCopy.VersionId = currentPublishedVersionId;

                        if (_cmsTextWidgetMongoRepository.GetEntityList(Query.And(
                            Query<TextWidgetEntity>.EQ(x => x.VersionId, currentPublishedVersionId),
                            Query<TextWidgetEntity>.EQ(x => x.MappingId, contentPageId),
                            Query<TextWidgetEntity>.EQ(x => x.TypeOFMapping, ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString()),
                            Query<TextWidgetEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                            ), true).Count > 0)
                        {
                            _cmsTextWidgetMongoRepository.UpdateEntity(Query.And(
                                Query<TextWidgetEntity>.EQ(x => x.VersionId, currentPublishedVersionId),
                                Query<TextWidgetEntity>.EQ(x => x.MappingId, contentPageId),
                                Query<TextWidgetEntity>.EQ(x => x.TypeOFMapping, ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString()),
                                Query<TextWidgetEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                ), textWidgetToCopy, true);
                        }
                        else
                        {
                            //Create new if not already exists in one of the older versions in the same publish state.
                            textWidgetToCopy.Id = ObjectId.GenerateNewId();
                            _cmsTextWidgetMongoRepository.Create(textWidgetToCopy);
                        }
                    }

                    isSuccessful = true;
                }
                else
                {
                    throw new ZnodeException(ErrorCodes.StoreNotPublishedForAssociatedEntity, string.Format(Api_Resources.StoreAssociatedWithEntityNotPublished, "content page"));
                }
            }
            catch (ZnodeException ex)
            {
                isSuccessful = false;
                RollbackContentPagePublish(ex, currentPublishedVersionId);
                exception = ex;
            }
            catch (MongoException ex)
            {
                isSuccessful = false;
                RollbackContentPagePublish(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.MongoExceptionDuringPublish, Api_Resources.MongoExceptionMessageDuringPublish);
            }
            catch (System.Data.Entity.Core.EntityException ex)
            {
                isSuccessful = false;
                RollbackContentPagePublish(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.EntityExceptionDuringPublish, Api_Resources.EntityExceptionMessageDuringPublish);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                isSuccessful = false;
                RollbackContentPagePublish(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.SQLExceptionDuringPublish, Api_Resources.SQLExceptionMessageDuringPublish);
            }
            catch (Exception ex)
            {
                isSuccessful = false;
                RollbackContentPagePublish(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, Api_Resources.GenericExceptionMessageDuringPublish);
            }

            if (!isSuccessful)
                throw exception;
            else
            {
                UpdateContentPageAfterPublish(contentPageId, targetPublishState, true);

                if (cmsSEOTypeId.HasValue)
                    UpdateSeoDetailAfterPublish(contentPageId, targetPublishState, cmsSEOTypeId.Value, true, portalId);

                

                //Delete previous published copy from mongo at last.
                if (IsNotNull(previousPublishedWebstore))
                    PreviewHelper.DeleteAllCMSEntitiesFromMongo(previousPublishedWebstore.VersionId, null, false);

                PreviewHelper.CreatePreviewLog(targetPublishState.ToString(), currentPublishedVersionId, previousPublishedWebstore.VersionId, contentPageId, "Content page", localeId, GetLocaleName(localeId));
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

                return true;
            }
        }

        private bool CopyEntityFromSQLToMongo(string id, int portalId, int localeId, ZnodePublishStatesEnum targetPublishState, ref bool breakWithSuccess, int additionalParameter = 0)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters contentPageId, portalId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { id, portalId, localeId });

            int contentPageId = int.Parse(id);
            bool isSuccessful = false;
            SeoEntity publishedSeoEntity = null;
            Exception exception = new Exception();

            WebStoreEntity existingWebstoreCopy = PreviewHelper.GetWebstore(portalId, targetPublishState, localeId);

            if (HelperUtility.IsNotNull(existingWebstoreCopy))
            {
                int sourceVersionId = existingWebstoreCopy.VersionId;

                int userId = GetLoginUserId();

                int? cmsSEOTypeId = _cmsSeoTypeRepository.Table.FirstOrDefault(seoType => seoType.Name.Equals(ZnodeConstant.ContentPage))?.CMSSEOTypeId;

                ZnodeLogging.LogMessage("sourceVersionId, userId, cmsSEOTypeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { sourceVersionId, userId, cmsSEOTypeId });

                ZnodePublishPortalLog znodePublishPortalLog = null;

                ZnodePublishPortalLog previewedPortalLog = _publishPortalLogRepository.Table?.Where(x => x.PortalId == portalId)?.ToArray()?.LastOrDefault();
                if (HelperUtility.IsNotNull(previewedPortalLog))
                    znodePublishPortalLog = _publishPortalLogRepository.Insert(
                        new ZnodePublishPortalLog { PublishStateId = previewedPortalLog.PublishStateId, PortalId = portalId, IsPortalPublished = previewedPortalLog.IsPortalPublished });

                int currentPublishedVersionId = 0;
                if (IsNotNull(znodePublishPortalLog))
                {
                    currentPublishedVersionId = znodePublishPortalLog.PublishPortalLogId;
                    ZnodeLogging.LogMessage("currentPublishedVersionId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { currentPublishedVersionId });
                }


                ContentPageConfigEntity ContentPageConfiguration = GetContentPageConfigEntity(contentPageId, portalId, currentPublishedVersionId, existingWebstoreCopy.LocaleId);

                try
                {
                    if (IsNotNull(existingWebstoreCopy))
                    {
                        //Publish Content Page Configuration.
                        IZnodeRepository<ZnodeCMSContentPagesProfile> contentPagesProfileRepository = new ZnodeRepository<ZnodeCMSContentPagesProfile>();

                        //Check if the configuration data is found for locale in the current iteration.
                        if (IsNotNull(ContentPageConfiguration))
                        {
                            //Get porfiles associated to content pages
                            List<ZnodeCMSContentPagesProfile> contentPagesProfiles = contentPagesProfileRepository.Table.Where(x => x.CMSContentPagesId == contentPageId).ToList();

                            //Profiled Associated to content page. If all profiles were selected, the ProfileId to be set "null" in mongo.
                            int?[] profileId = contentPagesProfiles?.Count > 0 ? contentPagesProfiles.Where(x => x.ProfileId.HasValue)?.Select(x => x.ProfileId)?.ToArray() : null;
                            ContentPageConfiguration.ProfileId = profileId?.Length > 0 ? profileId : null;

                            ContentPageConfiguration.VersionId = currentPublishedVersionId;
                            ContentPageConfiguration.Id = ObjectId.GenerateNewId();
                            ContentPageConfiguration.LocaleId = existingWebstoreCopy.LocaleId;

                            _cmsContentPageMongoRepository.Create(ContentPageConfiguration);

                            //Publish SEO Configuration.
                            if (cmsSEOTypeId.HasValue)
                            {
                                SeoEntity seoEntity = GetSEOAndLocaleDetail(ContentPageConfiguration.PageName, cmsSEOTypeId.Value, portalId, localeId, currentPublishedVersionId);
                                if (IsNotNull(seoEntity) && !string.IsNullOrEmpty(seoEntity?.SEOUrl) && !IsSeoUrlExistsOnUpdate(seoEntity?.SEOUrl, seoEntity.CMSSEODetailId, seoEntity.PortalId.GetValueOrDefault()))
                                {
                                    _seoMongoRepository.Create(seoEntity);

                                    publishedSeoEntity = seoEntity;
                                }
                            }

                            //Publish Text widget data.
                            PublishTextWidgetConfiguration(portalId, userId, currentPublishedVersionId, contentPageId, existingWebstoreCopy.LocaleId);
                            ZnodeLogging.LogMessage("Publish Text widget data- PublishTextWidgetConfiguration executed:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

                            //Publish the search widget data.
                            PublishSearchWidgetConfiguration(portalId, userId, currentPublishedVersionId, contentPageId, existingWebstoreCopy.LocaleId);
                            ZnodeLogging.LogMessage("Publish search widget data- PublishSearchWidgetConfiguration executed:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

                            //Publish products widget data.
                            SaveWidgetProductToMongo(portalId, contentPageId, currentPublishedVersionId);
                            ZnodeLogging.LogMessage("Publish Text widget data- SaveWidgetProductToMongo executed:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

                            //Publish banner silder data
                            SaveWidgetSliderBannerToMongo(portalId, contentPageId, userId, 0, currentPublishedVersionId, existingWebstoreCopy.LocaleId);
                            ZnodeLogging.LogMessage("Publish products widget data- SaveWidgetSliderBannerToMongo executed:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

                            SaveTitleWidgetToMongo(portalId, contentPageId, currentPublishedVersionId, existingWebstoreCopy.LocaleId);
                            ZnodeLogging.LogMessage("SaveTitleWidgetToMongo executed:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

                            isSuccessful = true;
                        }
                        else
                        {
                            throw new ZnodeException(ErrorCodes.EntityNotFoundDuringPublish, string.Format(Api_Resources.EntityNotFoundMessageDuringPublish, "content page"));
                        }
                    }
                    else
                    {
                        throw new ZnodeException(ErrorCodes.StoreNotPublishedForAssociatedEntity, string.Format(Api_Resources.StoreAssociatedWithEntityNotPublished, "content page"));
                    }
                }
                catch (ZnodeException ex)
                {
                    isSuccessful = false;
                    RollbackContentPagePublish(ex, currentPublishedVersionId);
                    exception = ex;
                }
                catch (MongoException ex)
                {
                    isSuccessful = false;
                    RollbackContentPagePublish(ex, currentPublishedVersionId);
                    exception = new ZnodeException(ErrorCodes.MongoExceptionDuringPublish, Api_Resources.MongoExceptionMessageDuringPublish);
                }
                catch (System.Data.Entity.Core.EntityException ex)
                {
                    isSuccessful = false;
                    RollbackContentPagePublish(ex, currentPublishedVersionId);
                    exception = new ZnodeException(ErrorCodes.EntityExceptionDuringPublish, Api_Resources.EntityExceptionMessageDuringPublish);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    isSuccessful = false;
                    RollbackContentPagePublish(ex, currentPublishedVersionId);
                    exception = new ZnodeException(ErrorCodes.SQLExceptionDuringPublish, Api_Resources.SQLExceptionMessageDuringPublish);
                }
                catch (Exception ex)
                {
                    isSuccessful = false;
                    RollbackContentPagePublish(ex, currentPublishedVersionId);
                    exception = new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, Api_Resources.GenericExceptionMessageDuringPublish);
                }

                if (!isSuccessful)
                    throw exception;
                else
                {
                    if (IsNotNull(ContentPageConfiguration))
                    {
                        //Delete previous content page entities.
                        PreviewHelper.DeleteRecords(_cmsContentPageMongoRepository, sourceVersionId, Query.And(
                                        Query<ContentPageConfigEntity>.EQ(x => x.ContentPageId, contentPageId),
                                        Query<ContentPageConfigEntity>.EQ(x => x.PortalId, portalId),
                                        Query<ContentPageConfigEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                    ));

                        if (IsNotNull(publishedSeoEntity))
                        {
                            //Delete previous seo entities.
                            PreviewHelper.DeleteRecords(_seoMongoRepository, sourceVersionId, Query.And(
                                        Query<SeoEntity>.EQ(x => x.SEOCode, ContentPageConfiguration.PageName),
                                        Query<SeoEntity>.EQ(x => x.CMSSEOTypeId, cmsSEOTypeId),
                                        Query<SeoEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                    ));

                            UpdateSeoDetailAfterPublish(publishedSeoEntity.CMSSEODetailId, publishedSeoEntity.CMSSEOTypeId, publishedSeoEntity.SEOCode, publishedSeoEntity.SEOId, publishedSeoEntity.MetaInformation, portalId, publishedSeoEntity.SEOUrl, targetPublishState, true);

                        }

                        //Delete previous text widget entities.
                        PreviewHelper.DeleteRecords(_cmsTextWidgetMongoRepository, sourceVersionId, Query.And(
                                    Query<TextWidgetEntity>.EQ(x => x.MappingId, contentPageId),
                                    Query<TextWidgetEntity>.EQ(x => x.TypeOFMapping, ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString()),
                                    Query<TextWidgetEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                    ));

                        //Delete previous search widget entities.
                        PreviewHelper.DeleteRecords(_cmsSearchWidgetMongoRepository, sourceVersionId, Query.And(
                                    Query<SearchWidgetEntity>.EQ(x => x.MappingId, contentPageId),
                                    Query<SearchWidgetEntity>.EQ(x => x.TypeOFMapping, ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString()),
                                    Query<SearchWidgetEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                    ));

                        //Delete previous slider widget data widget entities.
                        PreviewHelper.DeleteAllPreviousRecords(_cmsWidgetProductMongoRepository, sourceVersionId, Query.And(
                                    Query<WidgetProductEntity>.EQ(x => x.MappingId, contentPageId),
                                    Query<WidgetProductEntity>.EQ(x => x.TypeOFMapping, ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString())
                                    ));

                        //Delete previous slider widget data widget entities.
                        PreviewHelper.DeleteAllPreviousRecords(_cmsWidgetSliderBannerMongoRepository, sourceVersionId, Query.And(
                                    Query<WidgetSliderBannerEntity>.EQ(x => x.MappingId, contentPageId),
                                    Query<WidgetSliderBannerEntity>.EQ(x => x.TypeOFMapping, ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString())
                                    ));

                        //Delete previous slider widget data widget entities.
                        PreviewHelper.DeleteAllPreviousRecords(_cmsWidgetTitleMongoRepository, sourceVersionId, Query.And(
                                    Query<WidgetTitleEntity>.EQ(x => x.MappingId, contentPageId),
                                    Query<WidgetTitleEntity>.EQ(x => x.TypeOFMapping, ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString()),
                                    Query<TextWidgetEntity>.EQ(x => x.LocaleId, existingWebstoreCopy.LocaleId)
                                    ));

                        //Copy rest of the entities as it is for the new version.
                        PreviewHelper.CopyAllCMSEntitiesToVersion(sourceVersionId, currentPublishedVersionId, portalId, targetPublishState);

                        //Update Content Page publish status.
                        UpdateContentPageAfterPublish(contentPageId, targetPublishState, true);
                        ZnodeLogging.LogMessage("Executed UpdateContentPageAfterPublish.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

                        //Delete previous published copy from mongo at last.
                        if (IsNotNull(existingWebstoreCopy))
                            PreviewHelper.DeleteAllCMSEntitiesFromMongo(sourceVersionId, null, false);

                        PreviewHelper.CreatePreviewLog(targetPublishState.ToString(), currentPublishedVersionId, sourceVersionId, contentPageId, "Content page", localeId, GetLocaleName(localeId));

                        ClearCacheHelper.ClearCacheAfterPublish(Convert.ToString(portalId));
                    }
                    ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

                    return true;
                }
            }
            else
            {
                throw new ZnodeException(ErrorCodes.StoreNotPublishedForAssociatedEntity, string.Format(Api_Resources.StoreAssociatedWithEntityNotPublished, "content page"));
            }
        }

        private void RollbackContentPagePublish(Exception ex, int versionToRollback)
        {
            ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
            PreviewHelper.DeleteAllCMSEntitiesFromMongo(versionToRollback);
        }

        //Returns true if url is present in SEO Details table.
        private bool IsSeoUrlExistsOnUpdate(string url, int? id, int portalId)
        {
            List<ZnodeCMSSEODetail> entityList = _cmsSeoDetailRepository.Table.Where(x => x.SEOUrl != null && x.SEOUrl == url.Trim() && x.PortalId == portalId)?.Select(x => x)?.ToList();

            //If the url not exits.
            if (IsNull(entityList) || entityList?.Count == 0) return false;

            if (entityList.Any(x => x.CMSSEODetailId == id)) return false;

            return true;
        }

        private SeoEntity GetSEOAndLocaleDetail(string seoCode, int seoTypeId, int portalId, int localeId, int versionId)
        {
            ZnodeLogging.LogMessage("Input parameters seoCode, seoTypeId, portalId, localeId, versionId ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { seoCode, seoTypeId, portalId, localeId, versionId });
            int defaultLocaleId = GetDefaultLocaleId();

            IQueryable<SeoEntity> dataSelectQuery = (from seoDetail in _cmsSeoDetailRepository.Table
                    from seoDetailLocale in _cmsSeoDetailLocaleRepository.Table.Where(seoDetailLocale => seoDetail.CMSSEODetailId == seoDetailLocale.CMSSEODetailId && (seoDetailLocale.LocaleId == localeId || seoDetailLocale.LocaleId == defaultLocaleId)).DefaultIfEmpty()
                    where seoDetail.SEOCode == seoCode && seoDetail.PortalId == portalId && seoDetail.CMSSEOTypeId == seoTypeId && (seoDetailLocale.LocaleId == localeId || seoDetailLocale.LocaleId == defaultLocaleId)
                    select new SeoEntity()
                    {
                        SEOTypeName = _cmsSeoTypeRepository.Table.Where(x => x.CMSSEOTypeId == seoTypeId).Select(x => x.Name).FirstOrDefault(),
                        CMSSEODetailLocaleId = !Equals(seoDetailLocale, null) ? seoDetailLocale.CMSSEODetailLocaleId : 0,
                        CMSSEODetailId = !Equals(seoDetailLocale, null) ? seoDetail.CMSSEODetailId : 0,
                        IsRedirect = seoDetail.IsRedirect,
                        CMSSEOTypeId = seoDetail.CMSSEOTypeId,
                        LocaleId = seoDetailLocale.LocaleId,
                        MetaInformation = seoDetail.MetaInformation,
                        SEOId = !Equals(seoDetailLocale, null) ? seoDetail.SEOId : 0,
                        SEOCode = seoCode,
                        SEODescription = !Equals(seoDetailLocale, null) ? seoDetailLocale.SEODescription : string.Empty,
                        SEOKeywords = !Equals(seoDetailLocale, null) ? seoDetailLocale.SEOKeywords : string.Empty,
                        SEOTitle = !Equals(seoDetailLocale, null) ? seoDetailLocale.SEOTitle : string.Empty,
                        SEOUrl = !Equals(seoDetailLocale, null) ? seoDetail.SEOUrl.ToLower() : string.Empty,
                        PortalId = portalId,
                        VersionId = versionId
                    });

            IQueryable<SeoEntity> resultsWithPassedLocale = dataSelectQuery.Where(x => x.LocaleId == localeId);
            IQueryable<SeoEntity> resultWithDefaultLocale = dataSelectQuery.Where(x => x.LocaleId == defaultLocaleId);

            dataSelectQuery = resultsWithPassedLocale.Concat(resultWithDefaultLocale.Where(x => !resultsWithPassedLocale.Any(y => y.SEOCode == x.SEOCode && y.SEOUrl == x.SEOUrl)));
            return dataSelectQuery?.FirstOrDefault();
        }

        [Obsolete("To be discontinued in upcoming versions.")]
        private void UpdateContentPageAfterPublish(int contentPageId, bool value)
        {
            //Updating the IsPublished flag for Content Page
            var entity = _contentPageRepository.GetById(contentPageId);
            entity.IsPublished = value;
            _contentPageRepository.Update(entity);
         
        }

        private void UpdateContentPageAfterPublish(int contentPageId, ZnodePublishStatesEnum publishState, bool status)
        {
            var entity = _contentPageRepository.GetById(contentPageId);
            entity.PublishStateId = status ? (byte)publishState : (byte)ZnodePublishStatesEnum.DRAFT;
            _contentPageRepository.Update(entity);
        }

        private void UpdateSeoDetailAfterPublish(int cmsSeoDetailId, int seoTypeId, string seoCode, int? seoId, string metaInformation, int portalId, string seoUrl, ZnodePublishStatesEnum publishState, bool status)
        {
            ZnodeLogging.LogMessage("Input parameters cmsSeoDetailId, seoTypeId, seoCode, seoId, metaInformation, portalId, seoUrl :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { cmsSeoDetailId, seoTypeId, seoCode, seoId, metaInformation, portalId, seoUrl });

            _cmsSeoDetailRepository.Update(new ZnodeCMSSEODetail
            {
                SEOId = seoId,
                CMSSEODetailId = cmsSeoDetailId,
                CMSSEOTypeId = seoTypeId,
                SEOCode = seoCode,
                MetaInformation = metaInformation,
                PortalId = portalId,
                SEOUrl = seoUrl,
                PublishStateId = status ? (byte)publishState : (byte)ZnodePublishStatesEnum.DRAFT
            });
        }

        private void UpdateSeoDetailAfterPublish(int contentPageId, ZnodePublishStatesEnum publishState, int cmsSEOTypeId, bool status, int portalId)
        {
            var entity = _cmsSeoDetailRepository.Table.Where(x => x.SEOId == contentPageId && x.PortalId == portalId && x.CMSSEOTypeId == cmsSEOTypeId).FirstOrDefault();

            if (IsNotNull(entity))
            {
                entity.PublishStateId = status ? (byte)publishState : (byte)ZnodePublishStatesEnum.DRAFT;
                _cmsSeoDetailRepository.Update(entity);
            }
        }

        private void PublishTextWidgetConfiguration(int portalId, int userId, int versionId, int contentPagesId, int? localeId = null)
           => PublishHelper.SaveToMongo<TextWidgetEntity>(PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetTextWidgetConfiguration", null, "@CMSMappingId", contentPagesId, "@LocaleId", localeId.HasValue ? localeId.Value : 0), _cmsTextWidgetMongoRepository, versionId);

        private void PublishSearchWidgetConfiguration(int portalId, int userId, int versionId, int contentPagesId, int? localeId = null)
          => PublishHelper.SaveToMongo<SearchWidgetEntity>(PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetSearchWidgetConfiguration", null, "@CMSMappingId", contentPagesId, "@LocaleId", localeId.HasValue ? localeId.Value : 0), _cmsSearchWidgetMongoRepository, versionId);

        //Publish CMS Widget Product to mongo
        private void SaveWidgetProductToMongo(int portalId, int contentPageId, int versionId)
        {
            List<WidgetProductEntity> widgetProductConfigurations = _cmsWidgetProductRepository.Table.Where(
                                          x => (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && contentPageId.Equals(x.CMSMappingId)))?.ToModel<WidgetProductEntity>()?.ToList();

            if(widgetProductConfigurations?.Count > 0)
                _cmsWidgetProductMongoRepository.Create(widgetProductConfigurations.Select(w => { w.PortalId = portalId; w.VersionId = versionId; return w; }).ToList());
        }

        //Publish CMS Widget Title to mongo
        private void SaveTitleWidgetToMongo(int portalId, int contentPageId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Input parameters  portalId, contentPageId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, contentPageId, versionId, localeId });

            IZnodeViewRepository<WidgetTitleEntity> objStoredProc = new ZnodeViewRepository<WidgetTitleEntity>();

            //SP parameters for getting date to be publish.
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@LocaleId", localeId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@CMSContentPagesId", contentPageId, ParameterDirection.Input, DbType.Int32);

            List<WidgetTitleEntity> widgetTitleConfigurations;
            widgetTitleConfigurations = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishCMSWidgetTitle  @PortalId, @LocaleId,@CMSContentPagesId").ToList();

            widgetTitleConfigurations?.ForEach(x => x.VersionId = versionId);
            if(widgetTitleConfigurations?.Count > 0)
                _cmsWidgetTitleMongoRepository.Create(widgetTitleConfigurations);
        }

        //Publish CMS Widget Slider Banner to mongo
        protected void SaveWidgetSliderBannerToMongo(int portalId,int contentPageId,int UserId,int CMSSliderId, int versionId, int localeId)
        {            
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            //SP parameters for getting date to be publish.
            executeSpHelper.GetParameter("@PortalId", portalId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@UserId", UserId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@CMSSliderId", CMSSliderId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@LocaleId", localeId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@CMSContentPagesId", contentPageId, ParameterDirection.Input, SqlDbType.Int);  
            DataSet dataSet = executeSpHelper.GetSPResultInDataSet("Znode_GetCMSWidgetSliderBanner");
            
            PublishHelper.SaveToMongo<WidgetSliderBannerEntity>(dataSet, _cmsWidgetSliderBannerMongoRepository, versionId);
        }

        private void DeleteContentPagePublishedData(int portalId, int versionId, int contentPageId, int localId)
        {
            DeletePublishConfiguration(Query.And(
                   Query<ContentPageConfigEntity>.EQ(p => p.PortalId, portalId),
                   Query<ContentPageConfigEntity>.EQ(x => x.VersionId, versionId),
                   Query<ContentPageConfigEntity>.EQ(x => x.ContentPageId, contentPageId),
                   Query<ContentPageConfigEntity>.EQ(x => x.LocaleId, localId)
                   ));
        }

        private void DeleteContentPageWidgetPublishedData(int portalId, int versionId, int mappingId, int localId)
        {
            DeletePublishTextWidgetConfiguration(Query.And(
                   Query<TextWidgetEntity>.EQ(p => p.PortalId, portalId),
                   Query<TextWidgetEntity>.EQ(x => x.VersionId, versionId),
                   Query<TextWidgetEntity>.EQ(x => x.MappingId, mappingId),
                   Query<TextWidgetEntity>.EQ(x => x.LocaleId, localId)
                   ));

            DeletePublishSearchWidgetConfiguration(Query.And(
                   Query<SearchWidgetEntity>.EQ(p => p.PortalId, portalId),
                   Query<SearchWidgetEntity>.EQ(x => x.VersionId, versionId),
                   Query<SearchWidgetEntity>.EQ(x => x.MappingId, mappingId)
                   ));
        }

        //Get specific content page based on content page id and associated to portalId 
        private ContentPageConfigEntity GetContentPageConfigEntity(int contentPageId, int portalId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Input parameters contentPageId, portalId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPageId, portalId, versionId, localeId });

            int defaultLocaleId = GetDefaultLocaleId();
            IQueryable<ContentPageConfigEntity> dataSelectQuery = (from contentpage in _contentPageRepository.Table
                                                   join template in new ZnodeRepository<ZnodeCMSTemplate>().Table on contentpage.CMSTemplateId equals template.CMSTemplateId
                                                   join contentPagesLocale in _contentPagesLocaleRepository.Table on contentpage.CMSContentPagesId equals contentPagesLocale.CMSContentPagesId
                                                   where contentpage.PortalId == portalId && contentpage.CMSContentPagesId == contentPageId &&
                                                   (contentPagesLocale.LocaleId == localeId || contentPagesLocale.LocaleId == defaultLocaleId)
                                                   select new ContentPageConfigEntity
                                                   {
                                                       ContentPageId = contentpage.CMSContentPagesId,
                                                       PortalId = portalId,
                                                       FileName = template.FileName,
                                                       PageName = contentpage.PageName,
                                                       PageTitle = contentPagesLocale.PageTitle,
                                                       ActivationDate = contentpage.ActivationDate,
                                                       ExpirationDate = contentpage.ExpirationDate,
                                                       VersionId = versionId,
                                                       LocaleId = contentPagesLocale.LocaleId.HasValue ? contentPagesLocale.LocaleId.Value : 0,
                                                       IsActive = contentpage.IsActive
                                                   });

            IQueryable<ContentPageConfigEntity> resultsWithPassedLocale = dataSelectQuery.Where(x => x.LocaleId == localeId);
            IQueryable<ContentPageConfigEntity> resultWithDefaultLocale = dataSelectQuery.Where(x => x.LocaleId == defaultLocaleId);

            dataSelectQuery = resultsWithPassedLocale.Concat(resultWithDefaultLocale.Where(x => !resultsWithPassedLocale.Any(y => y.ContentPageId == x.ContentPageId)));
           
            return dataSelectQuery?.FirstOrDefault();
        }

        //Get WhereClause For Media Path Id.
        private FilterDataCollection GetWhereClauseForPagePathId(int folderId)
        {
            FilterCollection filterList = new FilterCollection();
            FilterTuple filterTuple = new FilterTuple(ZnodeCMSContentPageGroupEnum.CMSContentPageGroupId.ToString(), ProcedureFilterOperators.Equals, folderId.ToString());
            filterList.Add(filterTuple);
            return filterList.ToFilterDataCollection();
        }

        //Get WhereClause For MediaIds.
        private FilterDataCollection GetWhereClauseForPageIds(string pageIds)
        {
            FilterCollection filterList = new FilterCollection();
            FilterTuple filterTuple = new FilterTuple(ZnodeCMSContentPageEnum.CMSContentPagesId.ToString(), ProcedureFilterOperators.In, pageIds);
            filterList.Add(filterTuple);
            return filterList.ToFilterDataCollection();
        }

        //Get the tree with its child.
        private List<ContentPageTreeModel> GetAllNode(List<ContentPageTreeModel> mediaPath)
        {
            if (IsNotNull(mediaPath))
            {
                foreach (ContentPageTreeModel item in mediaPath)
                {
                    //find all chid folder and add to list
                    List<ContentPageTreeModel> child = mediaPath.Where(x => x.ParentId == item.Id).ToList();
                    item.Children = new List<ContentPageTreeModel>();
                    item.Children.AddRange(GetAllNode(child));
                }
                ZnodeLogging.LogMessage("mediaPath:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { mediaPath });

                return mediaPath;
            }
            return new List<ContentPageTreeModel>();
        }

        //Check if content page name already exists for current portal id.
        private bool IsPageNameAvailable(string pageName, int portalId)
         => _contentPageRepository.Table.Any(x => x.PageName == pageName.Trim() && x.PortalId == portalId);

        //Check if seo name already exists.
        private bool IsSeoNameAvailable(ContentPageModel model)
        {
            if (!string.IsNullOrEmpty(model?.SEOUrl))
            {
                //Linq for checking the SEO url is present in local table.
                return Convert.ToBoolean((from seoDetail in _cmsSeoDetailRepository.Table
                                          join cmsSeoDetailsLocale in _cmsSeoDetailLocaleRepository.Table on seoDetail.CMSSEODetailId equals cmsSeoDetailsLocale.CMSSEODetailId
                                          where seoDetail.CMSSEOTypeId == (int)SEODetailsEnum.Content_Page && cmsSeoDetailsLocale.LocaleId == model.LocaleId
                                          select new { seoDetail.SEOUrl, seoDetail.PortalId }
                           )?.Any(a => a.SEOUrl == model.SEOUrl.Trim() && a.PortalId == model.PortalId));
            }
            return false;
        }

        //Get all the information related to content page.
        private ContentPageModel GetContentPageInformation(int contentPageId, int localeId)
        {
            ContentPageModel ContentPageModel = new ContentPageModel();
            //Get the information related to content page according to contentPageId and LocaleId.
            ContentPageModel = GetContentPageInformationForLocaleId(contentPageId, localeId);

            return IsNull(ContentPageModel) ? GetContentPageInformationForLocaleId(contentPageId, GetDefaultLocaleId())
                                            : ContentPageModel;
        }

        //Get all the information related to content page from view.
        private ContentPageModel GetContentPageInformationForLocaleId(int contentPageId, int localeId)
        => (_viewGetContentPageDetailsRepository.Table.Where(x => x.CMSContentPagesId == contentPageId && x.LocaleId == localeId))?.FirstOrDefault()?.ToModel<ContentPageModel>();

        //Get  template model by templateId.
        private TemplateModel GetTemplateInformation(int templateId)
        {
            IZnodeRepository<ZnodeCMSTemplate> _znodeCMSTemplate = new ZnodeRepository<ZnodeCMSTemplate>();
            return _znodeCMSTemplate.Table.Where(template => template.CMSTemplateId == templateId)?.FirstOrDefault()?.ToModel<TemplateModel>();
        }

        //Replace the key name of sort to get sorted data from mongo db 
        private void ReplaceSortKeys(ref NameValueCollection sorts)
        {
            for (int index = 0; index < sorts.Keys.Count; index++)
            {
                if (sorts.Keys.Get(index) == FilterKeys.ItemName) { ReplaceSortKeyName(ref sorts, FilterKeys.ItemName, ZnodeCMSContentPageEnum.PageName.ToString().ToLower()); }
                if (sorts.Keys.Get(index) == FilterKeys.ItemId) { ReplaceSortKeyName(ref sorts, FilterKeys.ItemId, ZnodeCMSContentPageEnum.CMSContentPagesId.ToString().ToLower()); }
            }
        }

        //Set the template information.
        private void SetTemplateInformation(ContentPageModel contentPageModel)
        {
            TemplateModel templateModel = GetTemplateInformation(Convert.ToInt32(contentPageModel.CMSTemplateId));
            if (IsNotNull(templateModel))
            {
                contentPageModel.PageTemplateName = templateModel.Name;
                contentPageModel.PageTemplateFileName = templateModel.FileName;
            }
        }

        //Get the contentPageId from filters.
        private static int GetContentPageId(FilterCollection filters)
        {
            int contentPageId = 0;
            if (filters?.Count > 0)
                Int32.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(ZnodeCMSContentPageEnum.CMSContentPagesId.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out contentPageId);
            return contentPageId;
        }

        //Delete Already Published Content Page Configurations
        private void DeletePublishConfiguration(IMongoQuery query)
        {
            //Delete CMS Content Page Configuration
            _cmsContentPageMongoRepository.DeleteByQuery(query, true);
        }

        //Delete Already Published Content Page Configurations
        private void DeletePublishTextWidgetConfiguration(IMongoQuery query)
        {
            //Delete CMS Text Widget
            _cmsTextWidgetMongoRepository.DeleteByQuery(query, true);
        }

        //Delete CMS Searh Widget.
        private void DeletePublishSearchWidgetConfiguration(IMongoQuery mongoQuery) =>
            //Delete CMS Searh Widget.
            _cmsSearchWidgetMongoRepository.DeleteByQuery(mongoQuery, true);
        #endregion
    }
}
