﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Resources;
using System.Diagnostics;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public abstract class BaseService : ZnodeBusinessBase
    {
        #region Properties
        protected int PortalId
        {
            get
            {
                string domainName = GetPortalDomainName() ?? HttpContext.Current.Request.ServerVariables.Get("HTTP_HOST").Trim();
                return Convert.ToInt32(ZnodeConfigManager.GetSiteConfig(domainName)?.PortalId);
            }
        }

        protected int? WebstoreVersionId
        {
            get { return GetWebstoreVersionId(); }
        }

        protected int? GlobalVersionId
        {
            get { return GetGlobalVersionId(); }
        }

        protected int PublishStateId
        {
            get { return (byte)GetPortalPublishState(); }
        }
        #endregion

        #region Protected Methods

        //Adds expands to navigation properties
        protected void SetExpands(string navProp, List<string> navigationProperties)
        {
            navigationProperties.Add(navProp);
        }

        //Set default Page Size & Index based on Page Collection.
        protected void SetPaging(NameValueCollection page, out int pagingStart, out int pagingLength)
        {
            // We use int.MaxValue for the paging length to ensure we always get total results back
            pagingStart = 1;
            pagingLength = int.MaxValue;

            // Only do if both index and size are given
            if (!Equals(page, null) && page.HasKeys() && !string.IsNullOrEmpty(page.Get(PageKeys.Index)) && !string.IsNullOrEmpty(page.Get(PageKeys.Size)))
            {
                pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
                pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            }
        }

        //Get User ID of logged in user.
        protected int GetLoginUserId()
        {
            const string headerUserId = "Znode-UserId";
            int userId = 0;
            var headers = HttpContext.Current.Request.Headers;
            int.TryParse(headers[headerUserId], out userId);
            ZnodeLogging.LogMessage("userId: ", string.Empty, TraceLevel.Verbose, userId);
            return userId;
        }

        //Get Profile Id of logged in user.
        protected int GetProfileId()
        {
            const string headerProfileId = "Znode-ProfileId";
            int profileId = 0;
            var headers = HttpContext.Current.Request.Headers;
            int.TryParse(headers[headerProfileId], out profileId);
            ZnodeLogging.LogMessage("profileId: ", string.Empty, TraceLevel.Verbose, profileId);
            return profileId;
        }

        /// <summary>
        /// Get Email template Details by Code.
        /// </summary>
        /// <param name="code"></param>
        protected EmailTemplateMapperModel GetEmailTemplateByCode(string code, int portalId = 0, int localeId = 0)
        {
            //Set Default Locale, in case no locale exists.
            if (localeId == 0)
                localeId = GetDefaultLocaleId();

            List<EmailTemplateMapperModel> emailTemplates = GetEmailTemplateList(code, portalId, localeId);

            if (!emailTemplates.Any())
            {
                localeId = GetDefaultLocaleId();

                List<EmailTemplateMapperModel> emailTemplatesByDefaultLocale = GetEmailTemplateList(code, portalId, localeId);
                return emailTemplatesByDefaultLocale.FirstOrDefault();
            }
            //Get the Configured Email Template Details based on the Template Code & Locale.
            return emailTemplates.FirstOrDefault();
        }

        private  List<EmailTemplateMapperModel> GetEmailTemplateList(string code, int portalId, int localeId)
        {
            ZnodeRepository<ZnodeEmailTemplateArea> _emailTemplateAreaRepository = new ZnodeRepository<ZnodeEmailTemplateArea>();
            ZnodeRepository<ZnodeEmailTemplateMapper> _emailTemplateMapperRepository = new ZnodeRepository<ZnodeEmailTemplateMapper>();
            ZnodeRepository<ZnodeEmailTemplate> _emailTemplateRepository = new ZnodeRepository<ZnodeEmailTemplate>();
            ZnodeRepository<ZnodeEmailTemplateLocale> _emailTemplateLocaleRepository = new ZnodeRepository<ZnodeEmailTemplateLocale>();

            return (from emailTemplateArea in _emailTemplateAreaRepository.Table
                    join emailTemplateMapper in _emailTemplateMapperRepository.Table on emailTemplateArea.EmailTemplateAreasId equals emailTemplateMapper.EmailTemplateAreasId
                    join emailTemplate in _emailTemplateRepository.Table on emailTemplateMapper.EmailTemplateId equals emailTemplate.EmailTemplateId
                    join emailTemplateLocale in _emailTemplateLocaleRepository.Table on emailTemplate.EmailTemplateId equals emailTemplateLocale.EmailTemplateId
                    where emailTemplateArea.Code == code && emailTemplateMapper.PortalId == portalId && emailTemplateMapper.IsActive && emailTemplateLocale.LocaleId == localeId
                    select new EmailTemplateMapperModel()
                    {
                        Code = emailTemplateArea.Code,
                        EmailTemplateId = emailTemplate.EmailTemplateId,
                        EmailTemplateAreasId = emailTemplateArea.EmailTemplateAreasId,
                        TemplateName = emailTemplate.TemplateName,
                        Descriptions = emailTemplateLocale.Content,
                        Subject = emailTemplateLocale.Subject,
                        IsEnableBcc = emailTemplateMapper.IsEnableBcc,
                    }).ToList();
        }

        /// <summary>
        /// Replace filter key name 
        /// </summary>
        /// <param name="filters">Reference of filter collection</param>
        /// <param name="keyName">Old Key Name</param>
        /// <param name="newKeyName">New key name</param>
        protected void ReplaceFilterKeyName(ref FilterCollection filters, string keyName, string newKeyName)
        {
            FilterCollection tempCollection = new FilterCollection();

            for (int i = 0; i < filters.Count; i++)
            {
                FilterTuple tuple = filters[i];
                if (Equals(tuple.Item1.ToLower(), keyName.ToLower()))
                    tempCollection.Insert(i, new FilterTuple(newKeyName, tuple.Item2, tuple.Item3));
                else
                    tempCollection.Insert(i, tuple);
            }
            ZnodeLogging.LogMessage("filter key and new filter key name: ", string.Empty, TraceLevel.Verbose, new object[] { newKeyName, newKeyName });
            filters = tempCollection;
        }

        /// <summary>
        /// Replace sort key name
        /// </summary>
        /// <param name="sort">Sort collection</param>
        /// <param name="keyName">Old key name</param>
        /// <param name="newKeyName">New key name</param>
        protected void ReplaceSortKeyName(ref NameValueCollection sort, string keyName, string newKeyName)
        {
            if (HelperUtility.IsNotNull(sort))
            {
                NameValueCollection newCollection = new NameValueCollection();

                for (int index = 0; index < sort.Keys.Count; index++)
                {
                    if (sort.Keys.Get(index) == keyName)
                        newCollection.Add(newKeyName, sort.GetValues(index)?.FirstOrDefault());
                }
                ZnodeLogging.LogMessage("Sort key and new sort key name: ", string.Empty, TraceLevel.Verbose, new object[] { newKeyName, newKeyName });
                sort = newCollection;
            }
        }

        //Get list of published portals.
        protected string GetPublishedPortals()
        {
            string publishedPortal = string.Empty;

            IMongoRepository<WebStoreEntity> _webstoreEntity = new MongoRepository<WebStoreEntity>();

            IEnumerable<int> portals = _webstoreEntity.GetEntityList(Query.Empty, true)?.Select(s => s.PortalId).Distinct();

            publishedPortal = string.Join(",", portals);

            ZnodeLogging.LogMessage("publishedPortal: ", string.Empty, TraceLevel.Verbose, publishedPortal);
            return publishedPortal;
        }

        //Get the Authorized portal Ids for the login user.       
        protected string GetAvailablePortals(int userId = 0)
        {
            string portalAccess = string.Empty;

            //Get User associated portals.
            ZnodeLogging.LogMessage("userId to get user portal list ", string.Empty, TraceLevel.Verbose, userId);
            List<ZnodeUserPortal> userPortals = GetUserPortal(userId);
            ZnodeLogging.LogMessage("userPortals list count: ", string.Empty, TraceLevel.Verbose, userPortals?.Count);

            if (HelperUtility.IsNotNull(userPortals) && userPortals.Count > 0)
            {
                if (userPortals.Any(x => x.PortalId == null))
                {
                    IZnodeRepository<ZnodePortal> _portalRepository = new ZnodeRepository<ZnodePortal>();
                    List<int> portalIds = _portalRepository.Table.Select(x => x.PortalId).ToList();
                    if (portalIds.Count > 0)
                        portalAccess = string.Join(",", portalIds);
                }
                else
                    portalAccess = string.Join(",", userPortals.Select(x => x.PortalId));
            }

            ZnodeLogging.LogMessage("portalAccess: ", string.Empty, TraceLevel.Verbose, portalAccess);
            return portalAccess;
        }

        //Bind the Filter conditions for the authorized portal access.
        protected void BindUserPortalFilter(ref FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            if (HelperUtility.IsNull(filters))
                filters = new FilterCollection();

            //Get Authorized Portal Access for login User.
            string portalAccess = GetAvailablePortals();

            //Checking for portal id already exists in filters or not. 
            if (!filters.Exists(x => x.Item1.ToLower() == ZnodePortalEnum.PortalId.ToString().ToLower()))
            {
                //Set the filters for Authorized portal access, for the login user.
                if (!string.IsNullOrEmpty(portalAccess))
                    filters.Add(new FilterTuple(ZnodePortalEnum.PortalId.ToString(), ProcedureFilterOperators.In, portalAccess));
            }
            ZnodeLogging.LogMessage("Execution done.", string.Empty, TraceLevel.Info);
        }

        //Returns the default content state to be used as the final destination for published content.
        //Falls back to PRODUCTION in case the default publish state could not be acquired from database.
        protected ZnodePublishStatesEnum GetDefaultPublishState()
        {
            if (Equals(HttpRuntime.Cache["DefaultPublishState"], null))
            {
                ZnodePublishStatesEnum publishState = FetchDefaultPublishState();
                HttpRuntime.Cache.Insert("DefaultPublishState", publishState);
            }

            return (ZnodePublishStatesEnum)HttpRuntime.Cache.Get("DefaultPublishState");
        }

        //Get Content state mapped to supplied application type.
        protected ZnodePublishStatesEnum GetPublishStateFromApplicationType(ApplicationTypesEnum applicationType)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            List<PublishStateMappingModel> applicationTypeMappings = GetAvailablePublishStateMappings();
            ZnodeLogging.LogMessage("applicationTypeMappings list count: ", string.Empty, TraceLevel.Verbose, applicationTypeMappings?.Count);

            ZnodePublishStatesEnum publishState = 0;

            if (HelperUtility.IsNotNull(applicationTypeMappings))
            {
                string publishStateCode = applicationTypeMappings.Where(x => x.ApplicationType == applicationType.ToString() && x.IsEnabled)?.FirstOrDefault()?.PublishState;

                if (!string.IsNullOrEmpty(publishStateCode))
                    Enum.TryParse(publishStateCode, out publishState);
            }

            ZnodeLogging.LogMessage("publishState: ", string.Empty, TraceLevel.Verbose, publishState);
            ZnodeLogging.LogMessage("Execution done.", string.Empty, TraceLevel.Info);
            return publishState;
        }

        //Get Portal Details required for sending email content. Also get the configured domain url based on the WebStore Application type.
        protected PortalModel GetCustomPortalDetails(int portalId)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            PortalModel model = new PortalModel();
            if (portalId > 0)
            {
                IZnodeRepository<ZnodePortal> znodePortal = new ZnodeRepository<ZnodePortal>();
                IZnodeRepository<ZnodeDomain> znodePortalDomain = new ZnodeRepository<ZnodeDomain>();
                ZnodeLogging.LogMessage("portalId to get storeLogo: ", string.Empty, TraceLevel.Verbose, portalId);
                string storeLogo = GetStoreLogoPath(portalId);
                ZnodeLogging.LogMessage("storeLogo: ", string.Empty, TraceLevel.Verbose, storeLogo);

                model = (from portal in znodePortal.Table
                         join domain in znodePortalDomain.Table.Where(x => x.PortalId == portalId && x.ApplicationType == ApplicationTypesEnum.WebStore.ToString() && x.IsActive && x.IsDefault) on portal.PortalId equals domain.PortalId into pf
                         from portalDomain in pf.DefaultIfEmpty()
                         where portal.PortalId == portalId
                         select new PortalModel
                         {
                             StoreName = portal.StoreName,
                             AdministratorEmail = portal.AdminEmail,
                             DomainUrl = portalDomain.DomainName,
                             StoreLogo = storeLogo,
                             IsEnableSSL = portal.UseSSL,
                         }).FirstOrDefault();
            }
            ZnodeLogging.LogMessage("Execution done.", string.Empty, TraceLevel.Info);
            return model;
        }

        // Get SKUs by product Id(s).
        protected List<View_PimProductAttributeValue> GetSKUList(string PimProductIds)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            IZnodeRepository<View_PimProductAttributeValue> _viewLoadManageProductRepository = new ZnodeRepository<View_PimProductAttributeValue>();
            FilterCollection filter = new FilterCollection
            {
                new FilterTuple(View_PimProductAttributeValueEnum.PimProductId.ToString(), FilterOperators.In, PimProductIds?.ToString()),
                new FilterTuple(View_PimProductAttributeValueEnum.AttributeCode.ToString(), FilterOperators.Is, ZnodeConstant.ProductSKU)
            };
            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection());
            ZnodeLogging.LogMessage("PimProductIds to get SKU list: ", string.Empty, TraceLevel.Verbose, PimProductIds?.ToString());
            ZnodeLogging.LogMessage("Execution done.", string.Empty, TraceLevel.Info);
            return _viewLoadManageProductRepository.GetEntityList(whereClauseModel.WhereClause, whereClauseModel.FilterValues)?.ToList();
        }

        //Get current catalog version id by catalog id.
        protected int? GetCatalogVersionId(int publishCatalogId = 0, int localeId = 0, int portalId = 0)
        {
            var headers = HttpContext.Current.Request.Headers;

            if (!string.IsNullOrEmpty(headers["Znode-Locale"]))
                int.TryParse(headers["Znode-Locale"], out localeId);

            IMongoRepository<VersionEntity> _versionEntity = new MongoRepository<VersionEntity>();

            ZnodePublishStatesEnum contentState = GetPortalPublishState();
           // ZnodeLogging.LogMessage("publishCatalogId, localeId and portalId: ", string.Empty, TraceLevel.Verbose, new object[] { publishCatalogId, localeId, portalId });

            List<VersionEntity> versionEntities =  _versionEntity.GetEntityList(Query.And(
            Query<VersionEntity>.EQ(d => d.ZnodeCatalogId, publishCatalogId > 0 ? publishCatalogId : GetPortalCatalogId()),
            Query<VersionEntity>.EQ(d => d.RevisionType, contentState.ToString()),
            Query<VersionEntity>.EQ(d => d.LocaleId, localeId > 0 ? localeId : GetDefaultLocaleId()),
            Query<VersionEntity>.EQ(d => d.IsPublishSuccess, true)
            ))?.OrderByDescending(x=> x.VersionId).ToList();

            return versionEntities.Count() > 0 ? versionEntities.FirstOrDefault().VersionId : 0;
        }

        //Get current catalog version id by catalog id.
        protected int? GetCatalogVersionId(int publishCatalogId, ZnodePublishStatesEnum contentState)
        {
            IMongoRepository<VersionEntity> _versionEntity = new MongoRepository<VersionEntity>();

            int localeId = GetPortalLocale();

            ZnodeLogging.LogMessage("publishCatalogId, contentState and localeId : ", string.Empty, TraceLevel.Verbose, new object[] { publishCatalogId, contentState, localeId });
            return _versionEntity.GetEntityList(Query.And(
             Query<VersionEntity>.EQ(d => d.ZnodeCatalogId, publishCatalogId > 0 ? publishCatalogId : GetPortalCatalogId()),
             Query<VersionEntity>.EQ(d => d.RevisionType, contentState.ToString()),
             Query<VersionEntity>.EQ(d => d.LocaleId, localeId > 0 ? localeId : GetDefaultLocaleId()),
             Query<VersionEntity>.EQ(d => d.IsPublishSuccess, true)
             ))?.OrderByDescending(z => z.VersionId)?.FirstOrDefault()?.VersionId;
        }

        protected int? GetCatalogVersionForDefaultPublishState(int portalId = 0, int localeId = 0, int? publishCatalogId = null)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            IMongoRepository<VersionEntity> _versionEntity = new MongoRepository<VersionEntity>();

            ZnodePublishStatesEnum contentState = GetDefaultPublishState();

            //Check for portalId and get the catalog associated to that portal.
            if (publishCatalogId < 1 && portalId > 0)
            {
                publishCatalogId = GetPortalCatalogId(portalId);
            }

            IMongoQuery query = Query<VersionEntity>.EQ(d => d.RevisionType, contentState.ToString());

            if (publishCatalogId.HasValue && publishCatalogId.Value > 0)
                query = Query.And(query, Query<VersionEntity>.EQ(d => d.ZnodeCatalogId, publishCatalogId));

            query = Query.And(query, Query<VersionEntity>.EQ(d => d.LocaleId, localeId > 0 ? localeId : GetDefaultLocaleId()));

            int? versionId = _versionEntity.GetEntity(query)?.VersionId;

            ZnodeLogging.LogMessage("portalId, localeId and publishCatalogId to get CatalogVersionId : ", string.Empty, TraceLevel.Verbose, new object[] { portalId, localeId, publishCatalogId });
            ZnodeLogging.LogMessage("Execution done.", string.Empty, TraceLevel.Info);
            return versionId;
        }


        protected int? GetPortalCatalogId(int portalId = 0)
        {
            ZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            ZnodeLogging.LogMessage("portalId and PortalCatalogId: ", string.Empty, TraceLevel.Verbose, portalId);
            return _portalCatalogRepository.Table?.Where(x => x.PortalId == (portalId > 0 ? portalId : this.PortalId))?.FirstOrDefault()?.PublishCatalogId;

        }

        //Get current catalog All version id separeted by comma.
        protected string GetCatalogAllVersionIds()
        {
            IMongoRepository<VersionEntity> _versionEntity = new MongoRepository<VersionEntity>();
            return string.Join(",", _versionEntity.GetEntityList(Query.Empty)?.Select(x => x.VersionId));
        }

        //Get current catalog version id separeted by comma based on revisiontype production.
        protected string GetCatalogAllVersionIds(int localeId)
        {
            IMongoRepository<VersionEntity> _versionEntity = new MongoRepository<VersionEntity>();
            IMongoQuery query = Query<VersionEntity>.EQ(d => d.RevisionType, ZnodePublishStatesEnum.PRODUCTION.ToString());
            query = Query.And(query, Query<VersionEntity>.EQ(d => d.LocaleId, localeId > 0 ? localeId : GetDefaultLocaleId()));
            ZnodeLogging.LogMessage("localeId to get CatalogAllVersionIds: ", string.Empty, TraceLevel.Verbose, localeId);
            return string.Join(",", _versionEntity.GetEntityList(query)?.Select(x => x.VersionId));
        }
        //Check for User Portal Access.
        protected void CheckUserPortalAccess(int portalId)
        {
            ZnodeLogging.LogMessage("portalId to check user portal access: ", string.Empty, TraceLevel.Verbose, portalId);
            if (portalId > 0)
            {
                string portalAccess = GetAvailablePortals();
                if (!string.IsNullOrEmpty(portalAccess))
                {
                    bool hasPortalAccess = portalAccess.Split(',').Any(x => x == portalId.ToString());
                    if (!hasPortalAccess)
                        throw new ZnodeUnauthorizedException(ErrorCodes.NotPermited, Admin_Resources.UserDontHaveTheRequestedPortalAccess, HttpStatusCode.Unauthorized);
                }
            }
        }

        protected void CheckUserPortalAccessForLogin(int portalId, int userId = 0)
        {
            if (portalId > 0)
            {
                ZnodeLogging.LogMessage("portalId and userId to check portal access for login: ", string.Empty, TraceLevel.Verbose, new object[] { portalId, userId });
                string portalAccess = GetAvailablePortals(userId);
                if (!string.IsNullOrEmpty(portalAccess))
                {
                    bool hasPortalAccess = portalAccess.Split(',').Any(x => x == portalId.ToString());
                    if (!hasPortalAccess)
                        throw new ZnodeUnauthorizedException(ErrorCodes.NotPermited, Admin_Resources.UserDontHaveTheRequestedPortalAccess, HttpStatusCode.Unauthorized);
                }
            }
        }

        //Get Active locale list.

        protected static List<LocaleModel> GetActiveLocaleList()
        {
            List<LocaleModel> data = Equals(HttpRuntime.Cache["ActiveLocaleList"], null)
               ? InsertAndGetActiveLocales()
               : (List<LocaleModel>)HttpRuntime.Cache.Get("ActiveLocaleList");

            ZnodeLogging.LogMessage("Active portal list: ", string.Empty, TraceLevel.Verbose, data?.Count);
            return data;
        }



        //Get locale name by locale id.
        protected static string GetLocaleName(int localeId)
        {
            LocaleModel locale = GetActiveLocaleList()?.FirstOrDefault(x => x.LocaleId == localeId);

            ZnodeLogging.LogMessage("Locale name: ", string.Empty, TraceLevel.Verbose, locale?.Name);
            return locale?.Name;
        }

        //Get userName by userId. 
        protected static UserModel GetUserNameByUserId(int userId)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            ZnodeLogging.LogMessage("userId to get user name: ", string.Empty, TraceLevel.Verbose, userId);
            if (userId > 0)
            {
                IZnodeRepository<ZnodeUser> _userRepository = new ZnodeRepository<ZnodeUser>();
                IZnodeRepository<AspNetUser> _aspNetUserRepository = new ZnodeRepository<AspNetUser>();
                IZnodeRepository<AspNetZnodeUser> _aspNetZnodeUserRepository = new ZnodeRepository<AspNetZnodeUser>();

                return (from user in _userRepository.Table
                        join aspNetUser in _aspNetUserRepository.Table on user.AspNetUserId equals aspNetUser.Id
                        join aspNetZnodeUser in _aspNetZnodeUserRepository.Table on aspNetUser.UserName equals aspNetZnodeUser.AspNetZnodeUserId
                        where user.UserId == userId
                        select new UserModel
                        {
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            UserName = aspNetZnodeUser.UserName
                        })?.FirstOrDefault();
            }
            return new UserModel();
        }

        //Get the list of all global attributes for the user
        protected List<GlobalAttributeValuesModel> GetGlobalLevelAttributeList(int entityId, string entityType)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            List<GlobalAttributeValuesModel> userAttributes = new List<GlobalAttributeValuesModel>();
            if (entityId > 0 && !string.IsNullOrEmpty(entityType))
            {
                IZnodeViewRepository<GlobalAttributeValuesModel> globalAttributeValues = new ZnodeViewRepository<GlobalAttributeValuesModel>();
                globalAttributeValues.SetParameter("EntityName", entityType, ParameterDirection.Input, DbType.String);
                globalAttributeValues.SetParameter("GlobalEntityValueId", entityId, ParameterDirection.Input, DbType.Int32);
                ZnodeLogging.LogMessage("entityId and entityType to get user attributes: ", string.Empty, TraceLevel.Verbose, new object[] { entityId, entityType });
                userAttributes = globalAttributeValues.ExecuteStoredProcedureList("Znode_GetGlobalEntityAttributeValue @EntityName, @GlobalEntityValueId").ToList();
            }
            ZnodeLogging.LogMessage("userAttributes list count: ", string.Empty, TraceLevel.Verbose, userAttributes.Count);
            ZnodeLogging.LogMessage("Execution done.", string.Empty, TraceLevel.Info);
            return userAttributes;
        }

        //Get the Media Server Url
        protected static string GetMediaServerUrl(MediaConfigurationModel configuration)
        {
            if (HelperUtility.IsNotNull(configuration))
            {
                return string.IsNullOrWhiteSpace(configuration.CDNUrl) ? configuration.URL
                           : configuration.CDNUrl.EndsWith("/") ? configuration.CDNUrl : $"{configuration.CDNUrl}/";
            }
            return string.Empty;
        }

        //Get the scheduler path 
        protected virtual string GetSchedulerPath()
            => Path.IsPathRooted(ZnodeApiSettings.SchdeulerPath) ?
                    $"{ZnodeApiSettings.SchdeulerPath}{((ZnodeApiSettings.SchdeulerPath?.EndsWith("/") ?? false) ? ("") : ("/"))}{ZnodeConstant.schedulerExeName}" :
                    $"{Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, ZnodeApiSettings.SchdeulerPath)}\\{ZnodeConstant.schedulerExeName}";


        //Gets default culture code.
        protected virtual string GetDefaultCulture()
        {
            IDefaultGlobalConfigService defaultGlobalConfigService = new DefaultGlobalConfigService();
            List<DefaultGlobalConfigModel> defaultGlobalSettingData = defaultGlobalConfigService.GetDefaultGlobalConfigList()?.DefaultGlobalConfigs;
            return defaultGlobalSettingData?.Where(x => string.Equals(x.FeatureName, GlobalSettingEnum.Culture.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Select(x => x.FeatureValues)?.FirstOrDefault();
        }

        #endregion

        #region Public Method


        public static int GetDefaultLocaleId()
            => GetActiveLocaleList()?.FirstOrDefault()?.LocaleId ?? 0;

        /// <summary>
        /// Set locale filter if not present
        /// </summary>
        /// <param name="filters">filters</param>
        public void SetLocaleFilterIfNotPresent(ref FilterCollection filters)
        {
            filters = HelperUtility.IsNull(filters) ? new FilterCollection() : filters;

            if (!filters.Any(x => x.FilterName.ToLower() == ZnodeLocaleEnum.LocaleId.ToString().ToLower()))
                filters.Add(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, DefaultGlobalConfigSettingHelper.Locale);
        }

        /// <summary>
        /// Set locale filter if not present
        /// </summary>
        /// <param name="filters">filters</param>
        public void SetVersionFilterIfNotPresent(ref FilterCollection filters)
        {
            filters = HelperUtility.IsNull(filters) ? new FilterCollection() : filters;
            int? versionId = GetCatalogVersionId();
            if (versionId.HasValue && !filters.Any(x => x.FilterName.ToLower() == WebStoreEnum.VersionId.ToString().ToLower()))
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, versionId.Value.ToString());
        }

        //Sets the product index filter while taking unique products from published data.
        public void SetProductIndexFilter(FilterCollection filters)
        {
            if (!filters.Any(filter => (filter.FilterName == Znode.Libraries.ECommerce.Utilities.FilterKeys.ZnodeCategoryIds
                                    && (filter.FilterOperator == FilterOperators.Equals || filter.FilterOperator == FilterOperators.In)
                                    && !string.IsNullOrEmpty(filter.FilterValue.Trim())))
             && !filters.Any(filter => filter.FilterName == Znode.Libraries.ECommerce.Utilities.FilterKeys.ProductIndex))
                filters.Add(new FilterTuple(Znode.Libraries.ECommerce.Utilities.FilterKeys.ProductIndex, FilterOperators.Equals, ZnodeConstant.DefaultPublishProductIndex.ToString()));
        }

        //Sets the category index filter while taking unique categories from published data.
        public void SetCategoryIndexFilter(FilterCollection filters)
        {
            string IsCallFromWebstore = filters.FirstOrDefault(x => x.FilterName.Equals(WebStoreEnum.IsCallFromWebstore.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue;

            if (!Equals(IsCallFromWebstore, ZnodeConstant.TrueValue))
            {
                if (!filters.Any(filter => filter.FilterName == Znode.Libraries.ECommerce.Utilities.FilterKeys.CategoryIndex))
                    filters.Add(new FilterTuple(Znode.Libraries.ECommerce.Utilities.FilterKeys.CategoryIndex, FilterOperators.Equals, ZnodeConstant.DefaultPublishCategoryIndex.ToString()));
            }
            else {
                filters.RemoveAll(x => x.Item1 == WebStoreEnum.IsCallFromWebstore.ToString().ToLower());
            }
        }


        public int GetCurrentVersion(int catalogLogId, int localeId = 0)
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            ZnodePublishCatalogLog znodePublishCatalogLog = null;

            IZnodeRepository<ZnodePublishCatalogLog> _publishCatalogLogRepository = new ZnodeRepository<ZnodePublishCatalogLog>();

            ZnodeLogging.LogMessage("catalogLogId to get publish catalog log: ", string.Empty, TraceLevel.Verbose, catalogLogId);
            ZnodePublishCatalogLog previousPortalLog = _publishCatalogLogRepository.Table.OrderByDescending(x => x.PublishCatalogLogId).FirstOrDefault(x => x.PublishCatalogId == catalogLogId);
            if (HelperUtility.IsNotNull(previousPortalLog))
            {
                znodePublishCatalogLog = _publishCatalogLogRepository.Insert(new ZnodePublishCatalogLog { PublishCatalogId = previousPortalLog.PublishCatalogId, PimCatalogId = previousPortalLog.PimCatalogId, IsCatalogPublished = previousPortalLog.IsCatalogPublished, IsProductPublished = true, LocaleId = localeId, UserId = GetLoginUserId(), LogDateTime = DateTime.Now });
                int currentVersionId = znodePublishCatalogLog.PublishCatalogLogId;
                ZnodeLogging.LogMessage("currentVersionId: ", string.Empty, TraceLevel.Verbose, currentVersionId);
                return currentVersionId;
            }
            else
                return 0;
        }
        #endregion

        #region Private Method

        //Get Webstore version id for current content state.
        private int? GetWebstoreVersionId()
        {
            IMongoRepository<WebStoreEntity> _versionEntity = new MongoRepository<WebStoreEntity>();

            ZnodePublishStatesEnum publishState = GetPortalPublishState();
            int localeId = GetPortalLocale();

            ZnodeLogging.LogMessage("localeId to get webstore version Id: ", string.Empty, TraceLevel.Verbose, localeId);
            return _versionEntity.GetEntity(Query.And(
                Query<WebStoreEntity>.EQ(d => d.PortalId, this.PortalId),
                Query<WebStoreEntity>.EQ(d => d.PublishState, publishState.ToString()),
                Query<WebStoreEntity>.EQ(d => d.LocaleId, localeId)
                ))?.VersionId;
        }

        //Get Webstore version id for current content state.
        private int? GetGlobalVersionId()
        {
            IMongoRepository<GlobalVersionEntity> _versionEntity = new MongoRepository<GlobalVersionEntity>();

            ZnodePublishStatesEnum publishState = GetPortalPublishState();
            int localeId = GetPortalLocale();

            ZnodeLogging.LogMessage("localeId to get webstore version Id: ", string.Empty, TraceLevel.Verbose, localeId);
            return _versionEntity.GetEntity(Query.And(
                Query<GlobalVersionEntity>.EQ(d => d.PublishState, publishState.ToString()),
                Query<GlobalVersionEntity>.EQ(d => d.LocaleId, localeId)
                ))?.VersionId;
        }

        //Get Content State for this portal.
        protected ZnodePublishStatesEnum GetPortalPublishState()
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            const string headerPublishState = "Znode-PublishState";
            ZnodePublishStatesEnum publishState;
            var headers = HttpContext.Current.Request.Headers;
            Enum.TryParse(headers[headerPublishState], true, out publishState);

            if (publishState == 0)
            {
                //If state not found in request header. Try to achieve the same using DomainName header of the same request.
                ApplicationTypesEnum applicationType = GetApplicationTypeForDomain();

                if (applicationType != 0)
                {
                    publishState = GetPublishStateFromApplicationType(applicationType);

                    if (publishState != 0)
                        return publishState;
                }

                //Fall back to the default content state.
                publishState = GetDefaultPublishState();
            }
            ZnodeLogging.LogMessage("Portal publish state: ", string.Empty, TraceLevel.Verbose, publishState);
            ZnodeLogging.LogMessage("Execution done.", string.Empty, TraceLevel.Info);
            return publishState;
        }

        private string GetPortalDomainName()
        {
            const string headerDomainName = "Znode-DomainName";
            var headers = HttpContext.Current.Request.Headers;
            string domainName = headers[headerDomainName];
            ZnodeLogging.LogMessage("domainName: ", string.Empty, TraceLevel.Verbose, domainName);
            return domainName;
        }

        //Get LocaleId for this portal.
        private int GetPortalLocale()
        {
            const string headerLocaleState = "Znode-Locale";
            int localeId;
            var headers = HttpContext.Current.Request.Headers;
            int.TryParse(headers[headerLocaleState], out localeId);

            //Fall back to default locale from the active locale's list.
            if (localeId < 1)
                localeId = GetActiveLocaleList().FirstOrDefault(x => x.IsDefault).LocaleId;

            ZnodeLogging.LogMessage("Portal locale Id: ", string.Empty, TraceLevel.Verbose, localeId);
            return localeId;
        }

        //Get the User Portal Details based on logged in user.
        private List<ZnodeUserPortal> GetUserPortal(int userId = 0)
        {
            IZnodeRepository<ZnodeUserPortal> _userPortalRepository = new ZnodeRepository<ZnodeUserPortal>();

            //Get id for the logged in user.
            if (userId < 1)
                userId = GetLoginUserId();

            //Get the User Portal Details based on logged in user.
            ZnodeLogging.LogMessage("userId to get user portal list: ", string.Empty, TraceLevel.Verbose, userId);
            List<ZnodeUserPortal> userPortals = _userPortalRepository.Table.Where(x => x.UserId == userId).ToList();
            ZnodeLogging.LogMessage("User portal list count: ", string.Empty, TraceLevel.Verbose, userPortals?.Count);

            return userPortals;
        }

        //Get Store Logo Path.
        private string GetStoreLogoPath(int portalId)
        {
            ZnodeLogging.LogMessage("portalId to generate query: ", string.Empty, TraceLevel.Verbose, portalId);
            IMongoQuery query = Query<WebStoreEntity>.EQ(pr => pr.PortalId, portalId);
            string storeLogo = new MongoRepository<WebStoreEntity>(WebstoreVersionId).GetEntity(query)?.WebsiteLogo;
            ZnodeLogging.LogMessage("query to get store logo and store logo path: ", string.Empty, TraceLevel.Verbose, new object[] { query, storeLogo });
            MediaConfigurationModel configurationModel = GetService<IMediaConfigurationService>().GetDefaultMediaConfiguration();
            string serverPath = GetMediaServerUrl(configurationModel);
            string tumbnailPath = $"{serverPath}{configurationModel.ThumbnailFolderName}";
            return CreateImagebyUrl($"{tumbnailPath}/{storeLogo}");
        }

        private string CreateImagebyUrl(string url)
            => string.IsNullOrEmpty(url) ? string.Empty : $"<img src={url} class='img-responsive' style='max-width:150px;'></img></br>";

        //If active locales not present in cache then insert into cache.
        private static List<LocaleModel> InsertAndGetActiveLocales()
        {
            ZnodeRepository<View_GetLocaleDetails> _localeRepository = new ZnodeRepository<View_GetLocaleDetails>();
            List<LocaleModel> locales = new List<LocaleModel>();
            IList<View_GetLocaleDetails> entityList = _localeRepository.GetEntityList("");
            locales = entityList?.ToModel<LocaleModel>()?.ToList();
            ZnodeCacheDependencyManager.Insert("ActiveLocaleList", locales, "View_GetLocaleDetails");
            ZnodeLogging.LogMessage("Active locale list count: ", string.Empty, TraceLevel.Verbose, locales?.Count);
            return locales;
        }

        private static ZnodePublishStatesEnum FetchDefaultPublishState()
        {
            IZnodeRepository<ZnodePublishState> _publishStateRepository = new ZnodeRepository<ZnodePublishState>();
            string publishStateCode = _publishStateRepository.Table.Where(x => x.IsContentState && x.IsDefaultContentState)?.FirstOrDefault()?.PublishStateCode;

            ZnodePublishStatesEnum publishState;

            if (!string.IsNullOrEmpty(publishStateCode) && Enum.TryParse(publishStateCode, true, out publishState))
                return publishState;
            else
                return ZnodePublishStatesEnum.PRODUCTION;
        }

        protected List<PublishStateMappingModel> GetAvailablePublishStateMappings()
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            if (Equals(HttpRuntime.Cache["PublishStateApplicationTypeMappings"], null))
            {
                IZnodeRepository<ZnodePublishStateApplicationTypeMapping> _publishStateMappingRepository = new ZnodeRepository<ZnodePublishStateApplicationTypeMapping>();
                IZnodeRepository<ZnodePublishState> _publishStateRepository = new ZnodeRepository<ZnodePublishState>();

                List<PublishStateMappingModel> publishStateMappings = (from PSATM in _publishStateMappingRepository.Table
                                                                       join PS in _publishStateRepository.Table on PSATM.PublishStateId equals PS.PublishStateId
                                                                       where PSATM.IsActive
                                                                       select new PublishStateMappingModel
                                                                       {
                                                                           PublishStateMappingId = PSATM.PublishStateMappingId,
                                                                           ApplicationType = PSATM.ApplicationType,
                                                                           PublishStateCode = PS.PublishStateCode,
                                                                           Description = PSATM.Description,
                                                                           IsDefault = PS.IsDefaultContentState,
                                                                           IsEnabled = PSATM.IsEnabled,
                                                                           PublishStateId = PSATM.PublishStateId,
                                                                           PublishState = PS.PublishStateCode
                                                                       }).ToList();

                HttpRuntime.Cache.Insert("PublishStateMappings", publishStateMappings);
            }

            ZnodeLogging.LogMessage("Execution done.", string.Empty, TraceLevel.Info);
            return (List<PublishStateMappingModel>)HttpRuntime.Cache.Get("PublishStateMappings");
        }

        protected bool IsWebstorePreviewEnabled()
        {
            return GetAvailablePublishStateMappings()?.Count(x => !x.IsDefault && x.IsEnabled) > 0;
        }

        protected bool NonDefaultPublishStateExists(out ZnodePublishStatesEnum publishState)
        {
            string nonDefaultPublishState = GetAvailablePublishStateMappings()?.Where(x => !x.IsDefault && x.IsEnabled)?.FirstOrDefault()?.PublishStateCode;

            if (!string.IsNullOrEmpty(nonDefaultPublishState))
                return Enum.TryParse(nonDefaultPublishState, out publishState);

            publishState = 0;
            return false;
        }

        protected ApplicationTypesEnum GetApplicationTypeForDomain()
        {
            ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            IZnodeRepository<ZnodeDomain> _domainRepository = new ZnodeRepository<ZnodeDomain>();

            ApplicationTypesEnum applicationType = 0;

            string domainName = GetPortalDomainName();

            if (!string.IsNullOrEmpty(domainName))
            {
                FilterCollection filters = new FilterCollection()
                {
                    new FilterTuple(Znode.Libraries.ECommerce.Utilities.FilterKeys.DomainName, FilterOperators.Equals, "\"" + domainName + "\"")
                };

                ZnodeDomain domain = _domainRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause);

                if (HelperUtility.IsNotNull(domain))
                    Enum.TryParse(domain.ApplicationType, true, out applicationType);
            }
            ZnodeLogging.LogMessage("applicationType: ", string.Empty, TraceLevel.Verbose, applicationType);
            ZnodeLogging.LogMessage("Execution done.", string.Empty, TraceLevel.Info);
            return applicationType;
        }


        //Get the User Portal Details based on logged in user.
        protected List<int> GetPortalByCatalog(int catalogId)
        {
            IZnodeRepository<ZnodePortalCatalog> _catalogPortalRepository = new ZnodeRepository<ZnodePortalCatalog>();

            //Get the User Portal Details based on logged in user.
            ZnodeLogging.LogMessage("catalogId to get portal Ids: ", string.Empty, TraceLevel.Verbose, catalogId);
            List<int> portalIds = _catalogPortalRepository.Table.Where(x => x.PublishCatalogId == catalogId).Select(x => x.PortalId).ToList();
            ZnodeLogging.LogMessage("portalIds: ", string.Empty, TraceLevel.Verbose, portalIds);
            return portalIds;
        }

        //Get the current portal of webstore from header.
        protected int GetLocaleIdFromHeader()
        {
            int localeId = 0;
            var headers = HttpContext.Current.Request.Headers;

            if (!string.IsNullOrEmpty(headers["Znode-Locale"]))
                int.TryParse(headers["Znode-Locale"], out localeId);

            if (localeId == 0)
                localeId = GetDefaultLocaleId();

            ZnodeLogging.LogMessage("localeId: ", string.Empty, TraceLevel.Verbose, localeId);
            return localeId;
        }

        #endregion
    }
}
