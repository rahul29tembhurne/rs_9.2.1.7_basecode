﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Engine.Taxes;
using Znode.Engine.Taxes.Helper;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Observer;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Services
{
    public class OrderService : BaseService, IOrderService
    {
        #region Private Variables

        protected readonly IZnodeRepository<ZnodeAddress> _addressRepository;
        protected readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderDetailsRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _orderLineItemRepository;
        private readonly IZnodeRepository<ZnodeOmsPaymentRefund> _omsPaymentRefundRepository;
        private readonly IZnodeRepository<ZnodeOmsNote> _omsNoteRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderShipment> _orderShipmentRepository;
        private readonly IZnodeRepository<ZnodeOmsOrder> _omsOrderRepository;
        private readonly IZnodeRepository<ZnodePortal> _portalRepository;
        protected readonly IZnodeOrderHelper orderHelper;
        private readonly IZnodeRepository<ZnodeOmsOrderState> _omsOrderStateRepository;
        private readonly IZnodeRepository<ZnodeOmsHistory> _orderHistoryRepository;
        private readonly IZnodeRepository<ZnodeOmsTaxOrderLineDetail> _omsTaxOrderLineDetailRepository;
        private readonly IZnodeRepository<ZnodeShipping> _shippingRepository;
        private readonly IZnodeRepository<ZnodeRmaConfiguration> _rmaConfigurationRepository;
        private readonly IZnodeRepository<ZnodeShippingType> _shippingTypeRepository;
        private readonly IZnodeRepository<ZnodeOmsCustomerShipping> _omsCustomerShippingRepository;
        private readonly IZnodeRepository<ZnodeOmsDownloadableProductKey> _omsDownloadableProductKey;
        private readonly IZnodeRepository<ZnodePimDownloadableProductKey> _pimDownloadableProductKey;
        private readonly IZnodeRepository<ZnodePimDownloadableProduct> _pimDownloadableProduct;
        private readonly IZnodeRepository<ZnodePortalPaymentSetting> _portalPaymentSettingRepository;
        private readonly IZnodeRepository<ZnodeOmsQuote> _znodeOmsQuote;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderState> _orderStateRepository;
        private int _previousOrderStateId = 0;
        private readonly string InProgressOrderState = "IN PROGRESS";
        protected readonly IUserService _userService;
        protected readonly IPaymentSettingService _paymentSettingService;
        private readonly IZnodeRepository<ZnodeOmsCookieMapping> _cookieMappingRepository;
        private readonly IShoppingCartMap _shoppingCartMap;
        private readonly IZnodeRepository<ZnodeOmsQuoteComment> _omsQuoteComment;
        private readonly IZnodeRepository<ZnodeOMSQuoteApproval> _omsQuoteApproval;
        private readonly IOrderInventoryManageHelper _orderInventoryManageHelper;
        private readonly IZnodeRepository<ZnodeUserAddress> _userAddress;
        #endregion Private Variables

        #region Constructor

        public OrderService()
        {
            _addressRepository = new ZnodeRepository<ZnodeAddress>();
            _orderDetailsRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            _omsPaymentRefundRepository = new ZnodeRepository<ZnodeOmsPaymentRefund>();
            _orderLineItemRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
            _omsNoteRepository = new ZnodeRepository<ZnodeOmsNote>();
            orderHelper = ZnodeDependencyResolver.GetService<IZnodeOrderHelper>();
            _orderShipmentRepository = new ZnodeRepository<ZnodeOmsOrderShipment>();
            _omsOrderRepository = new ZnodeRepository<ZnodeOmsOrder>();
            _portalRepository = new ZnodeRepository<ZnodePortal>();
            _omsOrderStateRepository = new ZnodeRepository<ZnodeOmsOrderState>();
            _orderHistoryRepository = new ZnodeRepository<ZnodeOmsHistory>();
            _omsTaxOrderLineDetailRepository = new ZnodeRepository<ZnodeOmsTaxOrderLineDetail>();
            _shippingRepository = new ZnodeRepository<ZnodeShipping>();
            _rmaConfigurationRepository = new ZnodeRepository<ZnodeRmaConfiguration>();
            _shippingTypeRepository = new ZnodeRepository<ZnodeShippingType>();
            _omsCustomerShippingRepository = new ZnodeRepository<ZnodeOmsCustomerShipping>();
            _omsDownloadableProductKey = new ZnodeRepository<ZnodeOmsDownloadableProductKey>();
            _pimDownloadableProductKey = new ZnodeRepository<ZnodePimDownloadableProductKey>();
            _pimDownloadableProduct = new ZnodeRepository<ZnodePimDownloadableProduct>();
            _portalPaymentSettingRepository = new ZnodeRepository<ZnodePortalPaymentSetting>();
            _znodeOmsQuote = new ZnodeRepository<ZnodeOmsQuote>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _userService = GetService<IUserService>();
            _paymentSettingService = GetService<IPaymentSettingService>();
            _cookieMappingRepository = new ZnodeRepository<ZnodeOmsCookieMapping>();
            _orderStateRepository = new ZnodeRepository<ZnodeOmsOrderState>();
            _shoppingCartMap = GetService<IShoppingCartMap>();
            _omsQuoteComment = new ZnodeRepository<ZnodeOmsQuoteComment>();
            _omsQuoteApproval = new ZnodeRepository<ZnodeOMSQuoteApproval>();
            _orderInventoryManageHelper = GetService<IOrderInventoryManageHelper>();
            _userAddress = new ZnodeRepository<ZnodeUserAddress>();
        }

        #endregion Constructor

        #region Public Methods

        //Get order list.
        public virtual OrdersListModel GetOrderList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            //Bind the Filter conditions for the authorized portal access.
            BindUserPortalFilter(ref filters);
            //Replace sort key name.
            if (IsNotNull(sorts))
                ReplaceSortKeys(ref sorts);

            int userId = 0;
            GetUserIdFromFilters(filters, ref userId);

            int fromAdmin = Convert.ToInt32(filters?.Find(x => string.Equals(x.FilterName, Znode.Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase))?.Item3);
            filters?.RemoveAll(x => string.Equals(x.FilterName, Znode.Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase));
            ReplaceFilterKeys(ref filters);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);
            IList<OrderModel> list = GetOrderList(pageListModel, userId, fromAdmin);
            ZnodeLogging.LogMessage("Order list count:",ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, list?.Count);
            OrdersListModel orderListModel = new OrdersListModel() { Orders = list?.ToList() };

            GetCustomerName(userId, orderListModel);
            orderListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderListModel;
        }

        //Get order list by sp.
        public virtual IList<OrderModel> GetOrderList(PageListModel pageListModel, int userId, int fromAdmin)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters to get order list:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { pageListModel = pageListModel?.ToDebugString(), userId = userId, fromAdmin = fromAdmin });
            IZnodeViewRepository<OrderModel> objStoredProc = new ZnodeViewRepository<OrderModel>();
            objStoredProc.SetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@RowCount", pageListModel.TotalRowCount, ParameterDirection.Output, DbType.Int32);
            objStoredProc.SetParameter("@UserId", userId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@IsFromAdmin", fromAdmin, ParameterDirection.Input, DbType.Int32);

            return objStoredProc.ExecuteStoredProcedureList("Znode_GetOmsOrderDetail @WhereClause, @Rows,@PageNo,@Order_By,@RowCount OUT,@UserId,@IsFromAdmin", 4, out pageListModel.TotalRowCount);
        }

        //Get Order Details based on the Expand parameter
        public virtual IList<OrderModel> GetOrderListWithExpands(NameValueCollection expands, PageListModel pageListModel, int userId, int fromAdmin, string storedProcedureName)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters to get order list: " , ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { pageListModel = pageListModel, userId = userId, fromAdmin = fromAdmin, storedProcedureName = storedProcedureName });
            string ExpandParameter = IsNull(expands) ? string.Empty : string.Join(",", GetExpands(expands).ToArray());
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@WhereClause", pageListModel.SPWhereClause, ParameterDirection.Input, SqlDbType.NVarChar);
            objStoredProc.GetParameter("@Rows", pageListModel.PagingLength, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@PageNo", pageListModel.PagingStart, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@Order_By", pageListModel.OrderBy, ParameterDirection.Input, SqlDbType.NVarChar);
            objStoredProc.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@IsFromAdmin", fromAdmin, ParameterDirection.Input, SqlDbType.Int);
            objStoredProc.GetParameter("@Expands", ExpandParameter, ParameterDirection.Input, SqlDbType.NVarChar);
            DateTime datetimeStart = DateTime.UtcNow;
            DataSet dataset = objStoredProc.GetSPResultInDataSet(storedProcedureName);
            DateTime datetimeStop = DateTime.UtcNow;
            ZnodeLogging.LogMessage(string.Format(Admin_Resources.ExecutionTimeStoredProcedure, Convert.ToString((datetimeStop - datetimeStart).TotalSeconds)), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            List<OrderModel> entites = new List<OrderModel>();
            SetOrderListWithExpands(dataset, entites, pageListModel);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return entites;
        }

        //Get group order list.
        public virtual OrdersListModel GetGroupOrderList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            //Bind the Filter conditions for the authorized portal access.
            BindUserPortalFilter(ref filters);
            //Replace sort key name.
            if (HelperUtility.IsNotNull(sorts))
                ReplaceSortKeys(ref sorts);

            int userId = 0;
            GetUserIdFromFilters(filters, ref userId);

            int fromAdmin = Convert.ToInt32(filters?.Find(x => string.Equals(x.FilterName, Znode.Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase))?.Item3);
            filters?.RemoveAll(x => string.Equals(x.FilterName, Znode.Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase));
            ReplaceFilterKeys(ref filters);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            ZnodeLogging.LogMessage("Where condition:",ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, pageListModel?.ToDebugString());
            IList<OrderModel> list = GetOrderListWithExpands(expands, pageListModel, userId, fromAdmin, "Znode_GetOmsGroupOrderListExpandDetail");
            OrdersListModel orderListModel = new OrdersListModel() { Orders = list?.ToList() };
            ZnodeLogging.LogMessage("Order list count:",ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose,list?.Count());
            GetCustomerName(userId, orderListModel);

            orderListModel.BindPageListModel(pageListModel);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderListModel;
        }

        //Get Order Note List.
        public virtual OrderNotesListModel GetOrderNoteList(int omsOrderId, int omsQuoteId)
            => new OrderNotesListModel() { OrderNotes = GetOrderNoteDetails(omsOrderId, omsQuoteId) };

        //Create new order.
        public virtual OrderModel CreateOrder(ShoppingCartModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorShoppingCartModelNull);

            if (IsAllowedTerritories(model))
                throw new ZnodeException(ErrorCodes.AllowedTerritories, Admin_Resources.AllowedTerritoriesError);

            if (model.BillingAddress.AddressId == model.ShippingAddress.AddressId)
                SetBillingShippingFlags(model.BillingAddress.AddressId);

            SubmitOrderModel submitOrderModel = new SubmitOrderModel();

            ParameterModel portalId = new ParameterModel() { Ids = Convert.ToString(model.PortalId) };

            //Get generated unique order number on basis of current date.           
            submitOrderModel.OrderNumber = !string.IsNullOrEmpty(model.OrderNumber) ? model.OrderNumber : GenerateOrderNumber(submitOrderModel, portalId);
            ZnodeLogging.LogMessage("Generated order number: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { GeneratedOrderNumber = submitOrderModel?.OrderNumber });
            return SaveOrder(model, submitOrderModel);
        }

        //To generate unique order number on basis of current date.
        public virtual string GenerateOrderNumber(SubmitOrderModel submitOrderModel, ParameterModel portalId = null)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            var _erpInc = new ERPInitializer<SubmitOrderModel>(submitOrderModel, "GetOrderNumber");
            if (string.IsNullOrEmpty(Convert.ToString(_erpInc.Result)))
            {
                string orderNumber = string.Empty;
                if (!string.IsNullOrEmpty(ZnodeConfigManager.SiteConfig.StoreName))
                    orderNumber = ZnodeConfigManager.SiteConfig.StoreName.Trim().Length > 2 ? ZnodeConfigManager.SiteConfig.StoreName.Substring(0, 2) : ZnodeConfigManager.SiteConfig.StoreName.Substring(0, 1);

                DateTime date = DateTime.Now;
                String strDate = date.ToString("yyMMdd-HHmmss-fff");
                orderNumber += $"-{strDate}";
                return orderNumber.ToUpper();
            }
            else
                return Convert.ToString(_erpInc?.Result);
        }

        //Update existing order.
        public virtual OrderModel UpdateOrder(OrderModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorOrderModelNull);

            if (IsNull(model.OmsOrderId) || model?.OmsOrderId == 0)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorOrderIdNullOrZero);

            if (IsNull(model?.ShoppingCartModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorShoppingCartModelNull);

            if (IsAllowedTerritories(model.ShoppingCartModel))
                throw new ZnodeException(ErrorCodes.AllowedTerritories, Admin_Resources.AllowedTerritoriesError);

            ZnodeLogging.LogMessage("OrderModel with OmsOrderDetailsId and OmsOrderId: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsOrderDetailsId = model?.OmsOrderDetailsId, OmsOrderId = model?.OmsOrderId });
            model.ModifiedBy = HelperMethods.GetLoginUserId();
            //if there is no change in order data then no need to update order
            if (!IsOrderDataUpdated(model))
                return model;

            //to check order state isEditable if order is not editable then update order state
            if (!IsOrderEditable(model) && !IsReturnAllItems(model))
            {
                SaveHistoryAndUpdateOrderState(model, true);
                CancleOrderAmount(model);

                if (!string.IsNullOrEmpty(model.ShoppingCartModel.ShoppingCartItems?.FirstOrDefault()?.TaxTransactionNumber))
                    //Full order return request for CCH ,compensates the transaction for returns or losses.
                    CCHFullReturnRequest(model);
                ZnodeLogging.LogMessage("Order State while updating order:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OrderState = model?.OrderState});
                if (string.Equals(model.OrderState, ZnodeOrderStatusEnum.CANCELLED.ToString(), StringComparison.InvariantCultureIgnoreCase))
                {
                    IZnodeCheckout checkout = GetZnodeCheeckoutModel(model);
                    checkout.ShoppingCart.CancelTaxOrderRequest(model.ShoppingCartModel);
                }
                return model;
            }

            if (IsExistingOrderUpdated(model))
            {
                SaveHistoryAndUpdateOrderState(model, true);
                if (string.Equals(model.OrderState, ZnodeOrderStatusEnum.SHIPPED.ToString(), StringComparison.InvariantCultureIgnoreCase))
                {
                    IZnodeCheckout checkout = GetZnodeCheeckoutModel(model);
                    checkout.ShoppingCart.SubmitTax();
                }
                return model;
            }

            if (IsReturnShipping(model))
            {
                SaveHistoryAndUpdateOrderState(model, true);
                ReturnShippingAmount(model);
                return model;
            }
            if (IsReturnAllItems(model))
            {
                string omsOrderLineItemsIds = GetOrderLineItemsIds(model);
                //This flag is set to 1 if all the items are returned.
                int isRevertAll = 1;
                if (!RevertOrderInventory(model.OmsOrderId, model.UserId, omsOrderLineItemsIds.TrimStart(','), isRevertAll))
                    throw new ZnodeException(ErrorCodes.InternalItemNotUpdated, Admin_Resources.ErrorRevertOrderInventory);
                SetOverDueAmountForReturnAllItem(model);
                SaveHistoryAndUpdateOrderState(model);
                //Bind the ReturnItemLines in shopping cart model.
                model.ShoppingCartModel.ReturnItemList = model.ReturnItemList?.ReturnItemList ?? new List<ReturnOrderLineItemModel>();

                ZnodeLogging.LogMessage("Return Item List count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { ReturnItemListCount = model?.ShoppingCartModel?.ReturnItemList?.Count });

                //Call the tax ReturnOrderLineItem().
                ZnodeTaxManager _taxManager = new ZnodeTaxManager(model.ShoppingCartModel);
                _taxManager.ReturnOrderLineItem(model.ShoppingCartModel);

                return model;
            }

            ZnodeLogging.LogMessage(string.Format(Admin_Resources.UpdateOrderForOrderId, model.OmsOrderId), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            //Manage those items in the order which are not got modified its quantity greater than its previous quantity.
            ManageExistingOrderLineItems(model);

            if (!RevertOrderInventory(model.OmsOrderId, model.UserId))
                throw new ZnodeException(ErrorCodes.InternalItemNotUpdated, Admin_Resources.ErrorRevertOrderInventory);

            MapOrderModelToShoppingCart(model);

            SubmitOrderModel updateModel = SetUpdateOrderData(model);

            if (!model.IsTaxCostEdited)
                CalculateReturnItemTax(model.PortalId, model, updateModel.ReturnOrderLineItems?.ReturnItemList);

            SetCustomerShipping(model);
            model.ShoppingCartModel.IsEmailSend = model.IsEmailSend;
            OrderModel updatedmodel = SaveOrder(model.ShoppingCartModel, updateModel);

            UpdateProductKey(model.ShoppingCartModel, updatedmodel);

            model.OverDueAmount = updatedmodel.OverDueAmount;

            SaveHistoryAndUpdateOrderState(model);
            return updatedmodel;
        }

        /// <summary>
        /// This method will set backordeding true for those items in the order which are not got modified its quantity greter than its previous quantity.
        /// </summary>
        /// <param name="model">OrderModel</param>
        protected virtual void ManageExistingOrderLineItems(OrderModel model)
        {
            //Get the list of items which was already in order
            List<ShoppingCartItemModel> existingShoppingCartItems = model.ShoppingCartModel.ShoppingCartItems.Where(x => x.OmsOrderLineItemsId > 0).ToList();

            //Get list of items which are exsting items and not updated its quanity greater than previous qauntity
            List<ShoppingCartItemModel> backOrderedItems = new List<ShoppingCartItemModel>();

            foreach (ShoppingCartItemModel item in existingShoppingCartItems)
            {
                int? relationType = item.OrderLineItemRelationshipTypeId; //Get relationship type Id
                decimal? prevQuantity = GetPreviousQuantity(item, relationType); //Get the previous quanitity of line item(sku)

                if (item.Quantity <= prevQuantity)
                {
                    backOrderedItems.Add(item);
                }
            }

            if (backOrderedItems.Count > 0)
            {
                _orderInventoryManageHelper.SetBackOrderingForShoppingCart(backOrderedItems);
            }
        }

        /// <summary>
        /// Get Previous quanity of an item in before editing it.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="relationType"></param>
        /// <returns></returns>
        protected decimal? GetPreviousQuantity(ShoppingCartItemModel item, int? relationType)
        {
            decimal? prevQuantity;
            if (relationType == (int)ZnodeCartItemRelationshipTypeEnum.Bundles)
                prevQuantity = _orderLineItemRepository.Table.Where(w => w.ParentOmsOrderLineItemsId == item.OmsOrderLineItemsId)?.FirstOrDefault()?.Quantity;
            else
                prevQuantity = _orderLineItemRepository.Table.Where(w => w.OmsOrderLineItemsId == item.OmsOrderLineItemsId)?.FirstOrDefault()?.Quantity;

            return prevQuantity;
        }

        public virtual string GetOrderLineItemsIds(OrderModel model)
        {
            string omsOrderLineItemsIds = "";
            model.ReturnItemList.ReturnItemList.ForEach(returnItem =>
            {
                string omsOrderLineItemsId;
                omsOrderLineItemsId = model.OrderLineItems.Where(orderLineItem => orderLineItem.OmsOrderLineItemsId == returnItem.OmsOrderLineItemsId
                                                            && orderLineItem.OrderLineItemState != ZnodeOrderStatusEnum.RETURNED.ToString())
                                                            .Select(x => x.OmsOrderLineItemsId).FirstOrDefault().ToString();

                omsOrderLineItemsIds = omsOrderLineItemsIds + "," + omsOrderLineItemsId;
            });
            ZnodeLogging.LogMessage("Order line item Ids:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { omsOrderLineItemsIds = omsOrderLineItemsIds });
            return omsOrderLineItemsIds;
        }

        //Get order details by order id.
        public virtual OrderModel GetOrderById(int orderId, FilterCollection filters, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            OrderModel orderModel = GetOrderByOrderDetails(orderId, string.Empty, filters, expands);

            if (IsNotNull(orderModel?.OrderLineItems) && orderModel.OrderLineItems.Count > 0)
                orderModel.OrderLineItems = orderHelper.FormalizeOrderLineItems(orderModel);
            IAccountQuoteService _accountQuoteService = GetService<IAccountQuoteService>();
            orderModel.QuoteApproverComments = _accountQuoteService.GetApproverComments(orderModel.OmsQuoteId);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderModel;
        }


        //Get order details by order id.
        public virtual OrderModel GetOrderByOrderNumber(string orderNumber, FilterCollection filters, NameValueCollection expands)
        {
            return GetOrderByOrderDetails(0, orderNumber, filters, expands);
        }

        //to create new customer
        public virtual UserAddressModel CreateNewCustomer(UserAddressModel userAddressModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(userAddressModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorUserAddressModelNull);

            if (IsNull(userAddressModel.BillingAddress) && IsNull(userAddressModel.ShippingAddress))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorBillingAndShippingAddressNull);

            ZnodeUser user = new ZnodeUser { Email = userAddressModel.Email };

            //If user Id is less than 1 then create new guest user otherwise add shipping/billing address for user.
            if (userAddressModel.UserId < 1)
                user = CreateNewRegisteredUser(user, userAddressModel);
            else
                AssignShippingBillingAddressUserId(userAddressModel, userAddressModel.UserId);

            //Insert/Update shippin/billing address of user.
            List<ZnodeAddress> userAddress = InsertUpdateUserAddress(userAddressModel);

            //Set Default Shipping/Billing Address of user.
            userAddressModel.BillingAddress = userAddress.Where(x => x.IsDefaultBilling)?.FirstOrDefault()?.ToModel<AddressModel>();
            userAddressModel.ShippingAddress = userAddress.Where(x => x.IsDefaultShipping)?.FirstOrDefault()?.ToModel<AddressModel>();
            ZnodeLogging.LogMessage("BillingAddressId and ShippingAddressId of UserAddressModel: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { BillingAddressId = userAddressModel?.BillingAddress?.AddressId, ShippingAddressId = userAddressModel?.ShippingAddress?.AddressId });
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return userAddressModel;
        }

        //Get order invoice details.
        public virtual OrdersListModel GetOrderDetailsForInvoice(ParameterModel orderIds, NameValueCollection expands, FilterCollection filters = null)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (string.IsNullOrEmpty(orderIds?.Ids))
                return null;

            if (IsNull(filters))
                filters = new FilterCollection();

            filters.Add(new FilterTuple(ZnodeOmsOrderDetailEnum.OmsOrderId.ToString(), FilterOperators.In, orderIds.Ids));
            filters.Add(new FilterTuple(ZnodeOmsOrderDetailEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue));

            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
            ZnodeLogging.LogMessage("WhereClause to get orderList:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { WhereClause = whereClauseModel?.WhereClause });
            List<ZnodeOmsOrderDetail> orderList = _orderDetailsRepository.GetEntityList(whereClauseModel?.WhereClause, new List<string>() { ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString(), ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString() }).ToList();
            ZnodeLogging.LogMessage("orderList Count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderListCount = orderList?.Count });

            OrdersListModel listModel = new OrdersListModel() { Orders = new List<OrderModel>() };

            IPortalService _portalService = GetService<IPortalService>();
            foreach (ZnodeOmsOrderDetail order in orderList)
            {
                OrderModel orderModel = new OrderModel();

                //Map order detail object to OrderModel object.
                orderModel = order.ToModel<OrderModel>();

                orderModel.BillingAddress = order?.ToModel<AddressModel>();
                string BillingcompanyName = _addressRepository.Table.Where(x => x.AddressId == orderModel.BillingAddress.AddressId)?.FirstOrDefault()?.CompanyName;
                orderModel.BillingAddress.CompanyName = BillingcompanyName;
                //Get order details by using expands.
                GetExpands(expands, orderModel);

                //Get ordered shipping address.
                foreach (OrderLineItemModel lineItem in orderModel.OrderLineItems)
                {
                    lineItem.ShippingAddressHtml = GetOrderShipmentAddress(lineItem.ZnodeOmsOrderShipment);
                    //get personalise attributes by omsorderlineitemid
                    lineItem.PersonaliseValueList = orderHelper.GetPersonalisedValueOrderLineItem(lineItem.OmsOrderLineItemsId, false, 0);
                    lineItem.PersonaliseValuesDetail = orderHelper.GetPersonalisedAttributeLineItemDetails(lineItem.PersonaliseValueList, string.Empty);
                }
                orderModel.OrderNumber = orderList?.Select(x => x.ZnodeOmsOrder).Where(x => x.OmsOrderId == order.OmsOrderId)?.Select(x => x.OrderNumber)?.FirstOrDefault();

                List<OrderLineItemModel> addonOrderLineItems = new List<OrderLineItemModel>();

                addonOrderLineItems = orderModel.OrderLineItems.Where(x => x.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns).ToList();
                //Create Single Order Line Item if order is having group product.
                orderModel.OrderLineItems = CreateSingleOrderLineItem(orderModel, true).Where(x => x.ParentOmsOrderLineItemsId != null).ToList();


                if (addonOrderLineItems.Count > 0)
                    AddonPrice(orderModel, addonOrderLineItems);

                //Set order related discounts.
                SetOrderDiscount(orderModel);
                //Get Ordered billing address.

                orderModel.BillingAddressHtml = GetOrderBillingAddress(orderModel);

                //Get portal information
                PortalModel portal = _portalService.GetPortal(order.PortalId, null);
                orderModel.CustomerServiceEmail = portal?.CustomerServiceEmail;
                orderModel.CustomerServicePhoneNumber = portal?.CustomerServicePhoneNumber;

                listModel.Orders.Add(orderModel);
            }

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return listModel;
        }

        //This method use to add the price of addon to main product.
        public virtual void AddonPrice(OrderModel orderModel, List<OrderLineItemModel> addonOrderLineItems)
        {
            foreach (var item in orderModel.OrderLineItems)
            {
                foreach (var addonItem in addonOrderLineItems)
                {
                    if (item.OmsOrderLineItemsId == addonItem.ParentOmsOrderLineItemsId && item.OmsOrderDetailsId == addonItem.OmsOrderDetailsId)
                        item.Price += addonItem.Price;
                }
            }
        }

        //Update Order Payment Status
        public virtual bool UpdateOrderPaymentStatus(int orderId, string paymentStatus, int? paymentStateId = null, int createdBy = 0, int modifiedBy = 0)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (orderId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.OrderIdLessThanOne);

            if (string.IsNullOrEmpty(paymentStatus))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorPaymentStatusNull);

            if (IsNull(paymentStateId))
                paymentStateId = new ZnodeRepository<ZnodeOmsPaymentState>().Table.Where(x => x.Name == paymentStatus)?.FirstOrDefault()?.OmsPaymentStateId;

            if (IsNull(paymentStateId) || paymentStateId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidPaymentState);

            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId, paymentStatus = paymentStatus });
            ZnodeOmsOrderDetail orderDetails = _orderDetailsRepository.Table.Where(x => x.OmsOrderId == orderId && x.IsActive)?.FirstOrDefault();

            if (IsNotNull(orderDetails))
            {
                orderDetails.OmsPaymentStateId = paymentStateId;
                orderDetails.CreatedBy = createdBy;
                orderDetails.ModifiedBy = modifiedBy;
                return _orderDetailsRepository.Update(orderDetails);
            }
            return false;
        }

        //Update Order Tracking Number         
        public virtual bool UpdateTrackingNumber(int orderId, string trackingNumber)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (orderId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.OrderIdLessThanOne);

            if (string.IsNullOrEmpty(trackingNumber))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorTrackingNumberNull);

            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId, trackingNumber = trackingNumber });
            ZnodeOmsOrderDetail orderDetails = _orderDetailsRepository.Table.Where(x => x.OmsOrderId == orderId && x.IsActive)?.FirstOrDefault();

            if (IsNotNull(orderDetails))
            {
                orderDetails.TrackingNumber = trackingNumber;
                return _orderDetailsRepository.Update(orderDetails);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return false;
        }

        //Update Order Tracking Number         
        public virtual bool UpdateTrackingNumberByOrderNumber(string orderNumber, string trackingNumber)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (string.IsNullOrEmpty(orderNumber))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorOrderNumberNull);

            if (string.IsNullOrEmpty(trackingNumber))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorTrackingNumberNull);

            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderNumber = orderNumber, trackingNumber = trackingNumber });
            ZnodeOmsOrderDetail orderDetails = (from znodeorderDetails in _orderDetailsRepository.Table
                                                join znodeOrder in _omsOrderRepository.Table on znodeorderDetails.OmsOrderId equals znodeOrder.OmsOrderId
                                                where znodeOrder.OrderNumber == orderNumber && znodeorderDetails.IsActive
                                                select znodeorderDetails)?.FirstOrDefault();

            if (IsNotNull(orderDetails))
            {
                orderDetails.TrackingNumber = trackingNumber;
                return _orderDetailsRepository.Update(orderDetails);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return false;
        }

        //Update Order Tracking Number
        public virtual bool UpdateBillingAddress(int orderId, AddressModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (orderId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.OrderIdLessThanOne);

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorBillingAddressNull);

            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId, AddressModelWithAddressId = model?.AddressId});
            ZnodeOmsOrderDetail orderDetails = _orderDetailsRepository.Table.Where(x => x.OmsOrderId == orderId && x.IsActive)?.FirstOrDefault();

            if (IsNotNull(orderDetails))
            {
                orderDetails.BillingFirstName = model.FirstName;
                orderDetails.BillingLastName = model.LastName;
                orderDetails.BillingCountry = model.CountryName;
                orderDetails.BillingStateCode = string.IsNullOrEmpty(model.StateCode) ? model.StateName : model.StateCode;
                orderDetails.BillingPostalCode = model.PostalCode;
                orderDetails.BillingPhoneNumber = model.PhoneNumber;
                orderDetails.BillingStreet1 = model.Address1;
                orderDetails.BillingStreet2 = model.Address2;
                orderDetails.BillingCity = model.CityName;
                orderDetails.AddressId = model.AddressId;
                return _orderDetailsRepository.Update(orderDetails);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return false;
        }

        //Update Order Payment Status
        public virtual bool AddOrderNote(OrderNotesModel orderNotesModel)
        {
            if (!string.IsNullOrEmpty(orderNotesModel?.Notes))
            {
                ZnodeOmsNote notes = _omsNoteRepository.Insert(orderNotesModel.ToEntity<ZnodeOmsNote>());
                ZnodeLogging.LogMessage("OrderNotesModel inserted having OmsNotesId: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsNotesId = orderNotesModel?.OmsNotesId });
                orderNotesModel.OmsNotesId = notes.OmsNotesId;
                return orderNotesModel.OmsNotesId > 0;
            }
            return false;
        }

        //Add Refund Payment details
        public virtual bool AddRefundPaymentDetails(OrderItemsRefundModel refundPaymentListModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(refundPaymentListModel?.RefundOrderLineitems?.Count > 0))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorRefundPaymentListModelNull);

            ZnodeLogging.LogMessage("Refund Order Line items count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { RefundOrderLineitemsCount = refundPaymentListModel?.RefundOrderLineitems?.Count});
            List<RefundPaymentModel> refundPayments = _omsPaymentRefundRepository.Insert(refundPaymentListModel?.RefundOrderLineitems.ToEntity<ZnodeOmsPaymentRefund>().ToList())?.ToModel<RefundPaymentModel>()?.ToList();

            ZnodeLogging.LogMessage(IsNotNull(refundPayments?.Count > 0) ? Admin_Resources.SuccessRefundPaymentCreate : Admin_Resources.ErrorRefundPaymentCreate, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return refundPayments?.Count > 0;
        }

        //Get OrderLine Items With Refund payment left
        public virtual OrderItemsRefundModel GetOrderLineItemsWithRefund(int orderDetailsId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Order DetailsId for getting order Line Items With Refund:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderDetailsId = orderDetailsId });
            //Get order Details
            ZnodeOmsOrderDetail orderDetails = _orderDetailsRepository.Table?.Include(x => x.ZnodeOmsOrder)?.SingleOrDefault(x => x.OmsOrderDetailsId == orderDetailsId && x.IsActive);

            //Initialize OrderItemsRefundModel and Map ZnodeOmsOrderDetail
            OrderItemsRefundModel orderItemsRefundModel = MapToOrderItemsRefundModel(orderDetails);

            ZnodeLogging.LogMessage("Order number:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OrderNumber = orderDetails?.ZnodeOmsOrder?.OrderNumber });
            orderItemsRefundModel.OrderNumber = orderDetails?.ZnodeOmsOrder?.OrderNumber;
            //Get all Refund Types in Dictionary
            Dictionary<string, int> refundTypes = GetRefundPaymentType();

            //Get Order Line Items with Refund Details
            orderItemsRefundModel.RefundOrderLineitems.AddRange(GetRefundOrderLineItems(orderDetailsId, refundTypes[ZnodeConstant.PartialRefund.ToUpper()]));

            // Get shipping details if shipping cost greater than Zero
            orderItemsRefundModel.ShippingRefundDetails = (orderDetails?.ShippingCost > 0) ? MapRefundShippingDetails(refundTypes[ZnodeConstant.ShippingRefund.ToUpper()], orderDetails) : new RefundPaymentModel();

            //Map Order total refund details
            orderItemsRefundModel.TotalRefundDetails = MapRefundTotalDetails(refundTypes[ZnodeConstant.TotalRefund.ToUpper()], orderItemsRefundModel, orderDetails);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderItemsRefundModel;
        }

        //to resend order confirmation email
        public virtual bool ResendOrderConfirmationEmail(int orderId, FilterCollection filters, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Order Id:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, orderId);
            bool isEnableBcc = false;
            FilterCollection returnedOrderFilter = filters;
            filters = new FilterCollection();
            OrderModel orderModel = GetOrderById(orderId, filters, expands);
            //Create Single Order Line Item if order is having group product.
            orderModel.OrderLineItems = CreateSingleOrderLineItem(orderModel, true).Where(x => x.ParentOmsOrderLineItemsId != null).ToList();

            returnedOrderFilter.Add(new FilterTuple(Constants.FilterKeys.OmsOrderId, FilterOperators.Equals, orderId.ToString()));
            returnedOrderFilter.Add(new FilterTuple(Constants.FilterKeys.IsActive, FilterOperators.Equals, Convert.ToString(true)));

            OrderModel returnedOrderModel = GetOrderByIdForReturn(orderId, expands, returnedOrderFilter);

            MapReturnedTotal(orderModel, returnedOrderModel);

            //Create Single Order Line Item if returned order is having group product.
            orderModel.ReturnedOrderLineItems = CreateSingleOrderLineItem(returnedOrderModel);
            ZnodeLogging.LogMessage("ReturnedOrderLineItems count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { ReturnedOrderLineItemsCount = orderModel.ReturnedOrderLineItems?.Count });
            orderModel.ShoppingCartModel.Shipping.ShippingName = GetShippingName(orderModel.ShoppingCartModel.Shipping.ShippingName, orderModel.ShoppingCartModel.Shipping.ShippingId);
            // And finally attach the receipt HTML to the order and return
            orderModel.ReceiptHtml = GetHtmlResendReceiptForEmail(orderModel, false, out isEnableBcc);

            return SendOrderReceipt(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, $"{Admin_Resources.TitleOrderReceipt} - {orderModel.OrderNumber}", ZnodeConfigManager.SiteConfig.AdminEmail, string.Empty, orderModel.ReceiptHtml, isEnableBcc);
        }

        //to resend order confirmation email for cart items.
        public virtual bool ResendOrderLineItemConfirmationEmail(int orderId, string omsOrderLineId, NameValueCollection expands, bool isEnableBcc = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (!expands.HasKeys())
                expands = GetOrderExpandForResendMail();

            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId, omsOrderLineId = omsOrderLineId, expands = expands });
            OrderModel orderModel = GetOrderById(orderId, null, expands);

            //Create Single Order Line Item if order is having group product.
            CalculateCartItemForResendMail(orderModel, omsOrderLineId);

            // And finally attach the receipt HTML to the order and return
            orderModel.ReceiptHtml = GetHtmlResendReceiptForEmail(orderModel, true, out isEnableBcc);

            return SendOrderReceipt(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, $"{Admin_Resources.TitleOrderReceipt} - {orderModel.OrderNumber}", ZnodeConfigManager.SiteConfig.AdminEmail, string.Empty, orderModel.ReceiptHtml, isEnableBcc);
        }

        //Get Html Resend Receipt For Email.
        public virtual string GetHtmlResendReceiptForEmail(OrderModel orderModel, bool isFromReturnedReceipt, out bool isEnableBcc)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            foreach (OrderLineItemModel item in orderModel.OrderLineItems)
            {
                item.PersonaliseValueList?.Remove("AllocatedLineItems");
            }

            IZnodeOrderReceipt receipt = GetOrderReceiptInstance(orderModel);

            string templateCode = isFromReturnedReceipt ? ZnodeConstant.OrderReceipt : ZnodeConstant.ResendOrderReceipt;
            if (Equals(orderModel.ReturnedOrderLineItems?.Count, 0))
                templateCode = ZnodeConstant.OrderReceipt;

            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(templateCode, (orderModel.PortalId > 0) ? orderModel.PortalId : PortalId);
            isEnableBcc = false;
            if (HelperUtility.IsNotNull(emailTemplateMapperModel))
            {
                string receiptContent = ShowOrderAdditionalDetails(emailTemplateMapperModel.Descriptions, orderModel.Custom1);
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderResendReceiptHtml(receiptContent));
            }
            return string.Empty;
        }

        //Get payment status list.
        public virtual List<OrderPaymentStateModel> GetOrderPaymentState()
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            IZnodeRepository<ZnodeOmsPaymentState> _omsPaymentStateRepository = new ZnodeRepository<ZnodeOmsPaymentState>();
            return _omsPaymentStateRepository.GetEntityList(string.Empty).ToModel<OrderPaymentStateModel>().ToList();
        }

        //Get order details by order id.
        public virtual OrderModel GetOrderByOrderLineItemId(int orderLineItemId, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            OrderModel orderModel = new OrderModel();
            ZnodeLogging.LogMessage("Order Line Item Id:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, orderLineItemId);
            //Get active order list for an order line.
            ZnodeOmsOrderLineItem orderLineItemDetails = _orderLineItemRepository.Table.Where(w => w.OmsOrderLineItemsId == orderLineItemId && w.IsActive)?.FirstOrDefault();

            //Get order line item data along with addon, configurable, bundle or group data.
            List<ZnodeOmsOrderLineItem> orderData = _orderLineItemRepository.Table.Where(w => w.OmsOrderLineItemsId == orderLineItemId || (w.ParentOmsOrderLineItemsId == orderLineItemDetails.OmsOrderLineItemsId)).ToList();
            List<OrderLineItemModel> orderLineItemModel = orderData?.ToModel<OrderLineItemModel>().ToList();
            orderModel.OrderLineItems = orderLineItemModel;
            ZnodeLogging.LogMessage("OrderLineItems Count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OrderLineItemsCount = orderModel?.OrderLineItems?.Count });
            foreach (OrderLineItemModel lineItem in orderModel.OrderLineItems)
            {
                //get personalise attributes by omsorderlineitemid
                orderHelper.SetPersonalizeDetails(lineItem);
            }

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderModel;
        }

        //to update order status.
        public virtual bool UpdateOrderStatus(OrderStateParameterModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("OrderStateParameterModel with Id: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsOrderId = model?.OmsOrderId });
            bool updated = false;

            ZnodeOmsOrderDetail order = IsValidOrder(model);

            if (IsNotNull(order))
            {
                int previousOrderStateId = order.OmsOrderStateId;
                order.OmsOrderStateId = model.OmsOrderStateId;
                order.TrackingNumber = model.TrackingNumber;
                order.ModifiedDate = GetDateTime();
                List<ZnodeOmsOrderState> orderStateList = _omsOrderStateRepository.GetEntityList(string.Empty)?.ToList();
                ZnodeLogging.LogMessage("orderStateList Count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderStateListCount = orderStateList?.Count });

                if (orderStateList?.Count > 0)
                {
                    if (order.OmsOrderStateId.Equals(orderStateList?.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.SHIPPED.ToString())?.OmsOrderStateId))
                        order.ShipDate = DateTime.Now;
                    else if (order.OmsOrderStateId.Equals(orderStateList?.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.RETURNED.ToString())?.OmsOrderStateId))
                        order.ReturnDate = GetDateTime().Date + GetDateTime().TimeOfDay;
                }

                updated = _orderDetailsRepository.Update(order);
                if (updated)
                    UpdateLineItemState(model.OmsOrderId, previousOrderStateId, model.OmsOrderStateId, order.ShipDate);
                //Cancel tax transaction.
                CancelTaxTransaction(updated, order, orderStateList);

                if (updated && orderHelper.IsSendEmail(order.OmsOrderStateId))
                {
                    OrderModel orderModel = GetOrderById(order.OmsOrderId, null, GetOrderExpands());
                    SendOrderStatusEmail(orderModel);
                    return true;
                }
            }
            return updated;
        }

        /// <summary>
        /// Check quantity with in-stock inventory
        /// </summary>
        /// <param name="checkout"></param>
        /// <param name="inventoryList"></param>
        /// <returns></returns>
        public virtual bool CheckQuantityWithInventory(IZnodeCheckout checkout, List<InventorySKUModel> inventoryList)
        {
            //Check quantity with in-stock inventory
            return checkout.ShoppingCart.CheckWithInStockInventory(inventoryList);
        }

        private ZnodeOmsOrderDetail IsValidOrder(OrderStateParameterModel model)
        {
            if (IsNull(model))
                throw new Exception("Order model cannot be null.");

            if (model.OmsOrderId < 1)
                throw new Exception("Order ID cannot be less than 1.");

            if (IsNull(model.OmsOrderStateId) || model.OmsOrderStateId < 1)
                throw new Exception("Invalid order status.");

            ZnodeOmsOrderDetail order = _orderDetailsRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(CreatFiltersForOrder(model).ToFilterDataCollection())?.WhereClause, GetExpandsForOrderLineItem(GetOrderLineItemExpands()));
            return order;
        }


        private bool UpdateReturnedOrderStatus(OrderStateParameterModel model)
        {
            bool updated = false;

            ZnodeOmsOrderDetail order = IsValidOrder(model);

            if (IsNotNull(order))
            {
                order.OmsOrderStateId = model.OmsOrderStateId;
                order.TrackingNumber = model.TrackingNumber;
                order.ModifiedDate = GetDateTime();
                order.SubTotal = 0;
                order.Total = 0;
                updated = _orderDetailsRepository.Update(order);
            }
            return updated;
        }


        //to update order status.
        public virtual bool UpdateOrderDetailsByOrderNumber(OrderDetailsModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            bool orderUpdated = false;

            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorOrderModelNull);

            if (string.IsNullOrEmpty(model.OmsOrderNumber))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorOrderNumberEmptyOrNull);
            ZnodeLogging.LogMessage("OrderDetailsModel with: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsOrderNumber = model?.OmsOrderNumber, ExternalId = model?.ExternalId, OrderStateName = model?.OrderStateName });

            int omsOrderId = _omsOrderRepository.Table.Where(x => x.OrderNumber == model.OmsOrderNumber).Select(x => x.OmsOrderId).FirstOrDefault();
            if (omsOrderId < 1)
                throw new ZnodeException(ErrorCodes.InvalidData,  Admin_Resources.InvalidOrderNo);

            //Update external id.
            if (!string.IsNullOrEmpty(model.ExternalId))
                orderUpdated = UpdateOMSExternalId(model.OmsOrderNumber, model.ExternalId);

            //Insert order notes.
            if (!string.IsNullOrEmpty(model.OrderNotes))
            {
                _omsNoteRepository.Insert(new ZnodeOmsNote { Notes = model.OrderNotes, OmsOrderDetailsId = _orderDetailsRepository.Table.Where(x => x.OmsOrderId == omsOrderId && x.IsActive).Select(x => x.OmsOrderDetailsId).FirstOrDefault() });
                orderUpdated = true;
            }

            //Update order status.
            if (!string.IsNullOrEmpty(model.OrderStateName) || model.OmsOrderStateId > 0)
            {
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeOmsOrderStateEnum.IsOrderState.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue));
                string whereClause = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause;

                List<ZnodeOmsOrderState> orderStateList = _omsOrderStateRepository.GetEntityList(whereClause)?.ToList();
                //Get the order state id on the basis of entered state code.
                int? omsOrderStateId = !string.IsNullOrEmpty(model.OrderStateName)
                    ? orderStateList?.FirstOrDefault(x => x.OrderStateName.ToLower() == model.OrderStateName.ToLower())?.OmsOrderStateId
                    : IsNull(model.OmsOrderStateId) ? 0 : model.OmsOrderStateId;

                if (IsNotNull(omsOrderStateId))
                    orderUpdated = UpdateOrderStatus(new OrderStateParameterModel { OmsOrderStateId = omsOrderStateId.Value, OmsOrderId = omsOrderId });
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderUpdated;
        }

        public virtual void SendOrderStatusEmail(OrderModel orderModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            foreach (OrderLineItemModel item in orderModel.OrderLineItems)
                item.OrderLineItemCollection.AddRange(orderModel.OrderLineItems.Where(x => x.ParentOmsOrderLineItemsId == item.OmsOrderLineItemsId && x.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.AddOns)?.ToList());

            orderModel.OrderLineItems.RemoveAll(x => x.ParentOmsOrderLineItemsId == null);

            string subject = string.Empty;
            bool isEnableBcc = false;
            if (orderModel.OrderState == ZnodeOrderStatusEnum.CANCELLED.ToString())
            {
                subject = $"{Admin_Resources.CancelledOrderStatusSubject} - {orderModel.OrderNumber}";
                orderModel.ReceiptHtml = GetCancelledOrderReceiptForEmail(orderModel, out isEnableBcc);
            }
            else
            {
                // And finally attach the receipt HTML to the order and return
                subject = $"{Admin_Resources.ShippedOrderStatusSubject} - {orderModel.OrderNumber}";
                orderModel.ReceiptHtml = GetShippingReceiptForEmail(orderModel, out isEnableBcc);
            }
            SendOrderReceipt(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, subject, ZnodeConfigManager.SiteConfig.AdminEmail, string.Empty, orderModel.ReceiptHtml, isEnableBcc);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        public virtual OrderModel CheckInventoryAndMinMaxQuantity(ShoppingCartModel shoppingCartModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(shoppingCartModel))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorShoppingCartModelNull);

            UserAddressModel userDetails = SetUserDetails(shoppingCartModel);

            var znodeShoppingCart = _shoppingCartMap.ToZnodeShoppingCart(shoppingCartModel, userDetails);

            // Create the checkout object
            IZnodeCheckout checkout = CheckoutMap.ToZnodeCheckout(userDetails, znodeShoppingCart);
            string isInventoryInStockMessage = string.Empty;
            Dictionary<int, string> minMaxSelectableQuantity;
            checkout.ShoppingCart.CheckInventoryAndMinMaxQuantity(out isInventoryInStockMessage, out minMaxSelectableQuantity);

            if (!string.IsNullOrEmpty(isInventoryInStockMessage))
                throw new ZnodeException(ErrorCodes.OutOfStockException, Admin_Resources.OutOfStockException);

            if (IsNotNull(minMaxSelectableQuantity) && minMaxSelectableQuantity.Count > 0 && minMaxSelectableQuantity.ContainsKey(ErrorCodes.MinAndMaxSelectedQuantityError))
                throw new ZnodeException(ErrorCodes.MinAndMaxSelectedQuantityError, minMaxSelectableQuantity[ErrorCodes.MinAndMaxSelectedQuantityError]);

            ZnodeLogging.LogMessage("Is available inventory message and MinMaxQuantity flag:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { isInventoryInStockMessage = isInventoryInStockMessage, minMaxSelectableQuantity = minMaxSelectableQuantity });

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return new OrderModel() { IsAvailabelInventoryAndMinMaxQuantity = true };
        }

        //to save order history in database
        public virtual OrderHistoryModel CreateOrderHistory(OrderHistoryModel orderHistoryModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(orderHistoryModel))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorOrderHistoryModelNull);

            if (orderHistoryModel.OmsNotesId == 0)
                orderHistoryModel.OmsNotesId = null;

            if (orderHistoryModel.OrderAmount == 0)
                orderHistoryModel.OrderAmount = null;

            ZnodeLogging.LogMessage("OrderHistoryModel with OmsHistoryId: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsHistoryId = orderHistoryModel?.OmsHistoryId });
            ZnodeOmsHistory orderHistory = _orderHistoryRepository.Insert(orderHistoryModel.ToEntity<ZnodeOmsHistory>());

            ZnodeLogging.LogMessage(IsNotNull(orderHistory) ? Admin_Resources.SuccessOrderHistoryCreate : Admin_Resources.ErrorOrderHistoryCreate, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderHistory?.ToModel<OrderHistoryModel>();
        }

        //Get order state by id
        public virtual OrderStateModel GetOrderStateValueById(int omsOrderStateId)
        {
            ZnodeLogging.LogMessage("Oms order state Id:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, omsOrderStateId);
            var model = _omsOrderStateRepository.Table.Where(x => x.OmsOrderStateId == omsOrderStateId)?.FirstOrDefault();
            return model.ToModel<OrderStateModel>();
        }

        // Send returned order email.
        public virtual bool SendReturnedOrderEmail(int orderId, NameValueCollection expands, FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId, expands = expands, filters = filters });
            OrderModel orderModel = GetOrderByIdForReturn(orderId, expands, filters);
            bool isEnableBcc = false;
            //Create Single Order Line Item if order is having group product.
            orderModel.OrderLineItems = CreateSingleOrderLineItem(orderModel);
            //Set shipping of returned order
            if (orderModel.OrderLineItems?.Any(o => o.IsShippingReturn) == true && orderModel?.OrderLineItems?.Sum(o => Convert.ToDecimal(o.ShippingCost)) > 0)
            {
                orderModel.ShippingCost = orderModel?.OrderLineItems?.Where(o => o.IsShippingReturn)?.Sum(o => Convert.ToDecimal(o.ShippingCost)) ?? 0;
                orderModel.Total = orderModel.Total + orderModel.ShippingCost;
            }

            orderModel.CSRDiscountAmount = 0m;
            orderModel.ShippingDifference = 0m;
            orderModel.DiscountAmount = 0m;
            // And finally attach the receipt HTML to the order and return
            orderModel.ReceiptHtml = GetHtmlResendReceiptForEmail(orderModel, true, out isEnableBcc);

            return SendOrderReceipt(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, $"{Admin_Resources.TitleReturnedOrderReceipt} - {orderModel.OrderNumber}", ZnodeConfigManager.SiteConfig.AdminEmail, string.Empty, orderModel.ReceiptHtml, isEnableBcc);
        }

        // Get order by order id for returned order.
        public virtual OrderModel GetOrderByIdForReturn(int orderId, NameValueCollection expands, FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId, expands = expands, filters = filters });
            ZnodeOmsOrder order = _omsOrderRepository.Table.FirstOrDefault(x => x.OmsOrderId == orderId);

            ZnodeOmsOrderDetail orderDetails = GetOrderDetailsForLineItem(orderId, expands, filters);

            return GetOrderDetails(order, orderDetails, true, false, false, null, true);
        }

        public virtual bool SendPOEmail(SendInvoiceModel sendInvoiceModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeEmail.SendEmail(sendInvoiceModel.PortalId, sendInvoiceModel.ReceiverEmail, ZnodeConfigManager.SiteConfig.AdminEmail, null, $"{Admin_Resources.TitleOrderReceipt} - {sendInvoiceModel.OrderNumber}", sendInvoiceModel.ReceiptHtml, true, "");
            return true;
        }

        //CCH full order return request ,compensates the transaction for returns or losses.
        public virtual void CCHFullReturnRequest(OrderModel model) => new Taxes.STOCCHTax().CCHFullReturnRequest(model.ShoppingCartModel);

        //Calculate tax cost for partially return items
        public virtual void CalculateReturnItemTax(int portalId, OrderModel model, List<ReturnOrderLineItemModel> returnItems)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { portalId = portalId, returnItemsCount = returnItems?.Count });
            IZnodeRepository<ZnodePortalTaxClass> _portalTaxClassRepository = new ZnodeRepository<ZnodePortalTaxClass>();
            IZnodeRepository<ZnodeTaxRule> _taxRuleRepository = new ZnodeRepository<ZnodeTaxRule>();
            IZnodeRepository<ZnodeTaxClass> _taxClassRepository = new ZnodeRepository<ZnodeTaxClass>();
            IZnodeRepository<ZnodeTaxClassSKU> _taxClassSKURepository = new ZnodeRepository<ZnodeTaxClassSKU>();

            List<ReturnOrderLineItemModel> addOnReturnLineItem = GetAddOnReturnItem(model.OrderLineItems, returnItems);
            returnItems.AddRange(addOnReturnLineItem);

            foreach (var item in returnItems)
            {
                bool isSKUTaxExists = (from portalTaxClass in _portalTaxClassRepository.Table
                                       join taxClassSku in _taxClassSKURepository.Table on portalTaxClass.TaxClassId equals taxClassSku.TaxClassId
                                       where portalTaxClass.PortalId == portalId && taxClassSku.SKU == item.SKU
                                       select taxClassSku)?.Count() > 0;

                ZnodeTaxRule taxRuleItem = null;
                if (isSKUTaxExists)
                {
                    taxRuleItem = (from taxRule in _taxRuleRepository.Table
                                   join taxClass in _taxClassRepository.Table on taxRule.TaxClassId equals taxClass.TaxClassId
                                   join portalTaxClass in _portalTaxClassRepository.Table on taxRule.TaxClassId equals portalTaxClass.TaxClassId
                                   join taxClassSku in _taxClassSKURepository.Table on taxClass.TaxClassId equals taxClassSku.TaxClassId
                                   where taxClassSku.SKU == item.SKU && portalTaxClass.PortalId == portalId
                                   select taxRule)?.FirstOrDefault();
                }
                else
                {
                    taxRuleItem = (from taxRule in _taxRuleRepository.Table
                                   join portalTaxClass in _portalTaxClassRepository.Table on taxRule.TaxClassId equals portalTaxClass.TaxClassId
                                   where portalTaxClass.PortalId == portalId && taxRule.Precedence > 0
                                   select taxRule)?.FirstOrDefault();
                }

                if (IsNotNull(taxRuleItem))
                {
                    item.Gst = IsNotNull(taxRuleItem.GST) ? item.ExtendedPrice * (taxRuleItem.GST / 100) : 0;
                    item.Hst = IsNotNull(taxRuleItem.HST) ? item.ExtendedPrice * (taxRuleItem.HST / 100) : 0;
                    item.Pst = IsNotNull(taxRuleItem.PST) ? item.ExtendedPrice * (taxRuleItem.PST / 100) : 0;
                    item.Vat = IsNotNull(taxRuleItem.VAT) ? item.ExtendedPrice * (taxRuleItem.VAT / 100) : 0;
                    item.SalesTax = IsNotNull(taxRuleItem.SalesTax) ? item.ExtendedPrice * (taxRuleItem.SalesTax / 100) : 0;
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //Get AddOns return line item for calculate tax cost.
        public virtual List<ReturnOrderLineItemModel> GetAddOnReturnItem(List<OrderLineItemModel> orderLineItemList, ReturnOrderLineItemModel returnItem)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            List<ReturnOrderLineItemModel> addOnReturnLineItem = new List<ReturnOrderLineItemModel>();
            List<OrderLineItemModel> addOnLineItem = orderLineItemList
                                .Where(orderLineItem => returnItem.AddOnProductSKUs.Contains(orderLineItem.Sku) && orderLineItem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns)?.ToList();

            returnItem.ExtendedPrice = (orderLineItemList.FirstOrDefault(orderLineItem => orderLineItem.OmsOrderLineItemsId == returnItem.OmsOrderLineItemsId)?.Price).GetValueOrDefault();

            if (addOnLineItem?.Count() > 0)
            {
                foreach (var addOnItem in addOnLineItem)
                    addOnReturnLineItem.Add(GetAddOnReturnLineItem(addOnItem, returnItem));
            }
            ZnodeLogging.LogMessage("AddOnReturnLineItem count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, addOnReturnLineItem?.Count);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            return addOnReturnLineItem;
        }

        //Get list of AddOns return line item for calculate tax cost.
        public virtual List<ReturnOrderLineItemModel> GetAddOnReturnItem(List<OrderLineItemModel> orderLineItemList, List<ReturnOrderLineItemModel> returnItemList)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            List<ReturnOrderLineItemModel> addOnReturnLineItem = new List<ReturnOrderLineItemModel>();
            foreach (var returnItem in returnItemList)
            {
                List<OrderLineItemModel> addOnLineItem = orderLineItemList
                                                .Where(orderLineItem => returnItem.AddOnProductSKUs.Contains(orderLineItem.Sku) && orderLineItem.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns)?.ToList();

                if (addOnLineItem?.Count() > 0)
                {
                    foreach (var addOnItem in addOnLineItem)
                        addOnReturnLineItem.Add(GetAddOnReturnLineItem(addOnItem, returnItem));
                }
            }
            ZnodeLogging.LogMessage("AddOn ReturnLineItem count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, addOnReturnLineItem?.Count);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return addOnReturnLineItem;
        }

        //Map Add ons return line items.
        public virtual ReturnOrderLineItemModel GetAddOnReturnLineItem(OrderLineItemModel orderLineItemModel, ReturnOrderLineItemModel returnItem)
             => new ReturnOrderLineItemModel()
             {
                 Description = orderLineItemModel.Description,
                 Quantity = returnItem.Quantity,
                 ShippingCost = orderLineItemModel.ShippingCost.GetValueOrDefault(),
                 SKU = orderLineItemModel.Sku,
                 ProductName = orderLineItemModel.ProductName,
                 IsActive = orderLineItemModel.IsActive,
                 ShipSeperately = orderLineItemModel.ShipSeparately.GetValueOrDefault(),
                 Vat = orderLineItemModel.VAT,
                 Gst = orderLineItemModel.GST,
                 Hst = orderLineItemModel.HST,
                 Pst = orderLineItemModel.PST,
                 ParentOmsOrderLineItemsId = returnItem.ParentOmsOrderLineItemsId,
                 OrderLineItemRelationshipTypeId = (int)ZnodeCartItemRelationshipTypeEnum.AddOns,
                 SalesTax = orderLineItemModel.SalesTax,
                 OmsOrderStatusId = returnItem.OmsOrderStatusId,
                 OmsOrderLineItemsId = orderLineItemModel.OmsOrderLineItemsId,
                 OmsOrderShipmentId = orderLineItemModel.OmsOrderShipmentId,
                 ReasonForReturnId = returnItem.ReasonForReturnId,
                 OrderLineItemStatus = orderLineItemModel.OrderLineItemState,
                 OrderDetailId = orderLineItemModel.OmsOrderDetailsId,
                 ExtendedPrice = orderLineItemModel.Price
             };

        //Check if all the order line items need to return.
        //And if yes then update the status of all return items.
        public virtual bool IsReturnAllItems(OrderModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("OrderModel with OmsOrderDetailsId and OmsOrderId: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsOrderDetailsId = model?.OmsOrderDetailsId, OmsOrderId = model?.OmsOrderId });
            if (model.ShoppingCartModel.ShoppingCartItems?.Count < 1 && model.ReturnItemList?.ReturnItemList?.Count > 0)
            {
                List<ReturnOrderLineItemModel> addOnReturnLineItem = new List<ReturnOrderLineItemModel>();
                model.ReturnItemList.ReturnItemList.ForEach(returnItem =>
                {
                    var lineItem = model.OrderLineItems
                                   .FirstOrDefault(orderLineItem => orderLineItem.OmsOrderLineItemsId == returnItem.OmsOrderLineItemsId);
                    returnItem.OrderDetailId = lineItem.OmsOrderDetailsId;
                    returnItem.OmsOrderShipmentId = lineItem.OmsOrderShipmentId;
                    returnItem.IsActive = lineItem.IsActive;
                    if (returnItem?.GroupProducts?.Count > 0)
                    {
                        returnItem.ExtendedPrice = 0;
                        returnItem.Quantity = 0;
                    }

                    if (returnItem.AddOnLineItemId > 0 || (!string.IsNullOrEmpty(returnItem.AddOnProductSKUs)))
                        addOnReturnLineItem = GetAddOnReturnItem(model.OrderLineItems, returnItem);
                });

                if (addOnReturnLineItem.Count > 0)
                    model.ReturnItemList.ReturnItemList.AddRange(addOnReturnLineItem);

                IsShippingCostReturned(model.OmsOrderId, model.ReturnItemList.ReturnItemList);

                if (model.ReturnItemList.ReturnItemList.Count > 1)
                    return model.ReturnItemList.ReturnItemList.TrueForAll(item => orderHelper.ReturnOrderLineItems(item));
                else
                    return orderHelper.ReturnOrderLineItems((model.ReturnItemList.ReturnItemList.FirstOrDefault()));
            }
            ZnodeLogging.LogMessage("ReturnOrderLineItems count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, model?.ReturnItemList?.ReturnItemList?.Count);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return false;
        }

        //Expands necessary to get OrderDetails.
        public virtual NameValueCollection GetOrderExpands()
        {

            NameValueCollection expands = new NameValueCollection();
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsPaymentState.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsPaymentState.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString().ToLower());
            expands.Add(ExpandKeys.ZnodeShipping, ExpandKeys.ZnodeShipping);
            expands.Add(ExpandKeys.ZnodeUser, ExpandKeys.ZnodeUser);
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsHistories.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsHistories.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsNotes.ToString().ToLower());
            ZnodeLogging.LogMessage("Get order expands parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, expands);
            return expands;
        }

        //Get portal pixel tracking details.
        public virtual void GetPortalPixelTracking(OrderModel orderModel)
        {
            IZnodeRepository<ZnodePortalPixelTracking> _pixelTrackingRepository = new ZnodeRepository<ZnodePortalPixelTracking>();
            ZnodeLogging.LogMessage("PortalId to get PortalTrackingPixel: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { PortalId = orderModel?.PortalId });
            orderModel.PortalTrackingPixel = _pixelTrackingRepository.Table.FirstOrDefault(x => x.PortalId == orderModel.PortalId)?.ToModel<PortalTrackingPixelModel>();
        }

        public virtual OrderModel GetOrderDetails(ZnodeOmsOrder order, ZnodeOmsOrderDetail orderDetail, bool isFromOrderReceipt, bool isOrderHistory, bool isFromReorder, NameValueCollection expands = null, bool isFromReturnLineItem = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            OrderModel orderModel = new OrderModel();
            //null check for order detail object.
            if (IsNotNull(orderDetail))
            {
                ZnodeLogging.LogMessage("OmsOrderDetailsId: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OmsOrderDetailsId = orderDetail?.OmsOrderDetailsId });
                //Map order detail object to OrderMOdel object.
                orderModel = orderDetail.ToModel<OrderModel>();

                //If expand key contains PortalTrackingPixel key then get portal tracking pixel details.
                if (!string.IsNullOrEmpty(expands?[ExpandKeys.PortalTrackingPixel]))
                    GetPortalPixelTracking(orderModel);

                if (!isFromReturnLineItem)
                {
                    List<ZnodeOmsOrderLineItem> orderLineItems = orderHelper.GetOrderLineItemByOrderId(orderDetail.OmsOrderDetailsId).ToList();
                    if (isFromReorder)
                        orderModel.OrderLineItems = orderLineItems.Where(m => m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.AddOns) && m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles))?
                                                        .ToModel<OrderLineItemModel>()?.ToList();
                    else
                        orderModel.OrderLineItems = orderLineItems.Where(m => m.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles))?
                                                        .ToModel<OrderLineItemModel>()?.ToList();
                    if (orderModel.OrderLineItems?.Count > 0)
                    {
                        orderModel.OrderLineItems.ForEach(x =>
                        {
                            x.Description = orderLineItems.FirstOrDefault(m => m.OrderLineItemRelationshipTypeId == Convert.ToInt16(ZnodeCartItemRelationshipTypeEnum.AddOns)
                                                           && m.ParentOmsOrderLineItemsId == x.OmsOrderLineItemsId)?.Description ?? x.Description;
                            x.Quantity = Convert.ToDecimal(ServiceHelper.ToInventoryRoundOff(x.Quantity));
                            ZnodeOmsOrderLineItem lineItem = orderLineItems.FirstOrDefault(y => y.OmsOrderLineItemsId == x.OmsOrderLineItemsId);
                            x.Price += orderLineItems
                                    .Where(z => z.OrderLineItemRelationshipTypeId == (int)ZnodeCartItemRelationshipTypeEnum.AddOns
                                    && z.ParentOmsOrderLineItemsId == x.OmsOrderLineItemsId
                                    ).Sum(z => z.Price);
                            x.OrderWarehouse = orderLineItems.FirstOrDefault(y => y.OmsOrderLineItemsId == x.OmsOrderLineItemsId)?.ZnodeOmsOrderWarehouses.ToModel<OrderWarehouseModel>()?.ToList();
                            if (lineItem.ZnodeOmsOrderAttributes?.Count > 0)
                            {
                                x.Attributes = new List<OrderAttributeModel>();
                                foreach (ZnodeOmsOrderAttribute item in lineItem.ZnodeOmsOrderAttributes)
                                    x.Attributes.Add(new OrderAttributeModel { AttributeCode = item.AttributeCode, AttributeValue = item.AttributeValue, AttributeValueCode = item.AttributeValueCode });
                            }
                            ZnodeOmsOrderLineItem parentLineItem = orderLineItems.FirstOrDefault(y => y.OmsOrderLineItemsId == x.ParentOmsOrderLineItemsId);
                            if (IsNotNull(parentLineItem))
                                x.GroupId = parentLineItem.GroupId;
                        });
                    }
                }
                else
                {
                    orderModel.OrderLineItems = orderDetail.ZnodeOmsOrderLineItems.ToModel<OrderLineItemModel>().ToList() ?? new List<OrderLineItemModel>();
                    orderModel.ReturnedOrderLineItems = IsNotNull(orderModel.ReturnedOrderLineItems) ? orderModel.ReturnedOrderLineItems : new List<OrderLineItemModel>();
                }

                //Map order data from ZnodeOmsOrder.
                orderModel.IsQuoteOrder = order.IsQuoteOrder;
                orderModel.OrderNumber = order.OrderNumber;
                orderModel.OmsQuoteId = order.OMSQuoteId.GetValueOrDefault();
                ZnodeLogging.LogMessage("OrderNumber:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OrderNumber = orderModel.OrderNumber });

                //check for omsOrderDetailsId greater than 0.
                if (orderModel?.OmsOrderDetailsId > 0)
                {
                    //Map order data from ZnodeOmsOrder.
                    orderModel.IsQuoteOrder = order.IsQuoteOrder;
                    orderModel.OrderNumber = order.OrderNumber;

                    //Map Order details data to OrderModel.
                    MapOrderData(orderModel, orderDetail, isOrderHistory);

                    //Get Ordered billing address.
                    orderModel.BillingAddressHtml = GetOrderBillingAddress(orderModel);

                    //set the amount of disserent discount applied during order creation.
                    SetOrderDiscount(orderModel);

                    //Set Rma details for RMA validation checks
                    GetAndMapRmaDetails(orderModel);

                    // Map Customer Shipping
                    CustomerShipping(orderModel);

                    if (IsNotNull(orderModel?.OrderLineItems))
                    {
                        List<ZnodeAddress> addressList = GetOrderShipmentAddressList(orderModel.OrderLineItems.Select(x => x.ZnodeOmsOrderShipment).ToList());
                        List<ZnodeOmsOrderStateShowToCustomer> orderStatusList = GetOrderStatusForCustomerList(orderModel.OrderLineItems);
                        List<ZnodeOmsOrderShipment> orderShipmentList = GetOrderShipmentList(orderModel.OrderLineItems);
                        List<ZnodeOmsPersonalizeItem> personalizeList = GetPersonalisedValueOrderLineItemList(orderModel.OrderLineItems);
                        List<string> downloadableProductkeys = GetDownloadableProductKeyList(orderModel.OrderLineItems?.Select(x => x.Sku)?.Distinct()?.ToList());
                        List<ZnodeOmsOrderLineItemsAdditionalCost> additionalCostList = GetAdditionalCostList(orderModel.OrderLineItems);

                        foreach (OrderLineItemModel lineItem in orderModel.OrderLineItems)
                        {
                            //If expands constains IsWebStoreOrderReciept key and line item contains IsShowToCustomer false, get order status that will be shown to customer.
                            if (!string.IsNullOrEmpty(expands?[ExpandKeys.IsWebStoreOrderReciept]) && !lineItem.IsShowToCustomer)
                                lineItem.OrderLineItemState = GetOrderStatusForCustomer(lineItem.OrderLineItemStateId, orderStatusList);

                            lineItem.ZnodeOmsOrderShipment = orderShipmentList?.FirstOrDefault(x => x.OmsOrderShipmentId == lineItem.OmsOrderShipmentId)?.ToModel<OrderShipmentModel>();

                            lineItem.ShippingAddressHtml = GetOrderShipmentAddress(lineItem.ZnodeOmsOrderShipment, addressList);
                            //get personalise attributes by omsorderlineitemid
                            lineItem.PersonaliseValueList = GetPersonalisedValueOrderLineItem(Convert.ToInt32(lineItem.ParentOmsOrderLineItemsId) > 0 ? Convert.ToInt32(lineItem.ParentOmsOrderLineItemsId) : lineItem.OmsOrderLineItemsId, personalizeList);
                            lineItem.PersonaliseValuesDetail = orderHelper.GetPersonalisedAttributeLineItemDetails(lineItem.PersonaliseValueList, string.Empty);

                            lineItem.DownloadableProductKey = GetProductKey(lineItem.Sku, lineItem.OmsOrderLineItemsId, downloadableProductkeys);

                            lineItem.AdditionalCost = additionalCostList?.Where(x => x.OmsOrderLineItemsId == lineItem.OmsOrderLineItemsId)?.ToDictionary(x => x.KeyName, y => y.KeyValue);
                        }
                    }
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderModel;
        }

        //Get Order Receipt Datails.
        public virtual OrderModel GetOrderReceiptDetails(int orderId)
        {
            ZnodeOmsOrder order = null;

            if (orderId > 0)
                //Get active order by order id.
                order = _omsOrderRepository.Table.FirstOrDefault(x => x.OmsOrderId == orderId);
            ZnodeOmsOrderDetail orderDetail = null;
            OrderModel orderModel = new OrderModel();

            if (IsNotNull(order))
            {

                orderId = order.OmsOrderId;
                List<string> expands = new List<string>();               
                expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString());

                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(Constants.FilterKeys.OmsOrderId, FilterOperators.Equals, orderId.ToString()));
                orderDetail = _orderDetailsRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection())?.WhereClause, expands);
   
                //null check for order detail object.
                if (IsNotNull(orderDetail))
                {
                    //Map order detail object to OrderMOdel object.
                    orderModel = orderDetail.ToModel<OrderModel>();
                    {
                        orderModel.OrderLineItems = orderDetail.ZnodeOmsOrderLineItems.ToModel<OrderLineItemModel>().ToList() ?? new List<OrderLineItemModel>();
                        orderModel.ReturnedOrderLineItems = IsNotNull(orderModel.ReturnedOrderLineItems) ? orderModel.ReturnedOrderLineItems : new List<OrderLineItemModel>();
                    }

                    //Map order data from ZnodeOmsOrder.
                    orderModel.IsQuoteOrder = order.IsQuoteOrder;
                    orderModel.OrderNumber = order.OrderNumber;
                    orderModel.OmsQuoteId = order.OMSQuoteId.GetValueOrDefault();

                    //check for omsOrderDetailsId greater than 0.
                    if (orderModel.OmsOrderDetailsId > 0)
                    {
                        orderModel.CustomerPaymentGUID = orderDetail.ZnodeUser?.CustomerPaymentGUID;
                        orderModel.ShippingId = (orderDetail.ZnodeShipping?.ShippingId).GetValueOrDefault();
                        orderModel.TrackingUrl = orderDetail.ZnodeShipping?.TrackingUrl;
                        int orderShipmentId = (orderModel.OrderLineItems.FirstOrDefault()?.OmsOrderShipmentId).GetValueOrDefault();

                        orderModel.BillingAddress = orderDetail.ToModel<AddressModel>();

                        ZnodeRepository<ZnodeState> _stateRepository = new ZnodeRepository<ZnodeState>();
                        orderModel.BillingAddress.StateCode = _stateRepository.Table.FirstOrDefault(x => x.StateName == orderModel.BillingAddress.StateName)?.StateCode;

                        if (IsNotNull(orderModel.BillingAddress))
                        {
                            ZnodeAddress billing = _addressRepository.Table.FirstOrDefault(x => x.AddressId == orderModel.BillingAddress.AddressId);
                            orderModel.BillingAddress.CompanyName = billing?.CompanyName;
                            //Sets the external id for billing address.
                            orderModel.BillingAddress.ExternalId = billing?.ExternalId;
                        }

                        //Get Ordered billing address.
                        orderModel.BillingAddressHtml = GetOrderBillingAddress(orderModel);

                        //set the amount of disserent discount applied during order creation.
                        SetOrderDiscount(orderModel);

                        //Set Rma details for RMA validation checks
                        GetAndMapRmaDetails(orderModel);

                        // Map Customer Shipping
                        CustomerShipping(orderModel);

                        if (IsNotNull(orderModel.OrderLineItems) && orderModel.OrderLineItems.Count() > 0)
                        {
                            List<ZnodeAddress> addressList = GetOrderShipmentAddressList(orderModel.OrderLineItems.Select(x => x.ZnodeOmsOrderShipment).ToList());
                            List<ZnodeOmsOrderStateShowToCustomer> orderStatusList = GetOrderStatusForCustomerList(orderModel.OrderLineItems);
                            List<ZnodeOmsOrderShipment> orderShipmentList = GetOrderShipmentList(orderModel.OrderLineItems);
                            List<ZnodeOmsPersonalizeItem> personalizeList = GetPersonalisedValueOrderLineItemList(orderModel.OrderLineItems);
                            List<string> downloadableProductkeys = GetDownloadableProductKeyList(orderModel.OrderLineItems?.Select(x => x.Sku)?.Distinct()?.ToList());
                            List<ZnodeOmsOrderLineItemsAdditionalCost> additionalCostList = GetAdditionalCostList(orderModel.OrderLineItems);

                            foreach (OrderLineItemModel lineItem in orderModel.OrderLineItems)
                            {
                                //If expands constains IsWebStoreOrderReciept key and line item contains IsShowToCustomer false, get order status that will be shown to customer.
                                if (!lineItem.IsShowToCustomer)
                                    lineItem.OrderLineItemState = GetOrderStatusForCustomer(lineItem.OrderLineItemStateId, orderStatusList);

                                lineItem.ZnodeOmsOrderShipment = orderShipmentList?.FirstOrDefault(x => x.OmsOrderShipmentId == lineItem.OmsOrderShipmentId)?.ToModel<OrderShipmentModel>();

                                lineItem.ShippingAddressHtml = GetOrderShipmentAddress(lineItem.ZnodeOmsOrderShipment, addressList);
                                //get personalise attributes by omsorderlineitemid
                                lineItem.PersonaliseValueList = GetPersonalisedValueOrderLineItem(Convert.ToInt32(lineItem.ParentOmsOrderLineItemsId) > 0 ? Convert.ToInt32(lineItem.ParentOmsOrderLineItemsId) : lineItem.OmsOrderLineItemsId, personalizeList);
                                lineItem.PersonaliseValuesDetail = orderHelper.GetPersonalisedAttributeLineItemDetails(lineItem.PersonaliseValueList, string.Empty);

                                lineItem.DownloadableProductKey = GetProductKey(lineItem.Sku, lineItem.OmsOrderLineItemsId, downloadableProductkeys);

                                lineItem.AdditionalCost = additionalCostList?.Where(x => x.OmsOrderLineItemsId == lineItem.OmsOrderLineItemsId)?.ToDictionary(x => x.KeyName, y => y.KeyValue);
                            }
                        }
                    }
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderModel;
        }
        //Get Download product key of product
        public string GetProductKey(string sku, decimal quantity, int omsOrderLineItemsId)
        {
            ZnodeLogging.LogMessage("Input parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new {sku = sku, quantity = quantity, omsOrderLineItemsId = omsOrderLineItemsId });
            string productKey = string.Empty;
            bool IsDownloadableSKU = _pimDownloadableProduct.Table.Any(x => x.SKU == sku);

            if (IsDownloadableSKU)
            {
                var productKeyDetails =
                    from omsDownloadableProductKey in _omsDownloadableProductKey.Table
                    join pimDownloadableProductKey in _pimDownloadableProductKey.Table on omsDownloadableProductKey.PimDownloadableProductKeyId equals pimDownloadableProductKey.PimDownloadableProductKeyId
                    join pimDownloadableProduct in _pimDownloadableProduct.Table on pimDownloadableProductKey.PimDownloadableProductId equals pimDownloadableProduct.PimDownloadableProductId
                    where pimDownloadableProduct.SKU == sku && pimDownloadableProductKey.IsUsed && omsDownloadableProductKey.OmsOrderLineItemsId == omsOrderLineItemsId
                    select new { keys = pimDownloadableProductKey.DownloadableProductKey }.keys;

                productKey = string.Join(",", productKeyDetails);
            }
            ZnodeLogging.LogMessage("productKey:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, productKey);

            return productKey;
        }

        public string GetProductKey(string sku, int omsOrderLineItemsId, List<string> downloadableProductkeys)
        {
            string productKey = string.Empty;
            if (omsOrderLineItemsId > 0 && string.IsNullOrEmpty(sku) && downloadableProductkeys?.Count > 0)
            {
                bool IsDownloadableSKU = downloadableProductkeys.Any(x => x == sku);

                if (IsDownloadableSKU)
                {
                    var productKeyDetails =
                        from omsDownloadableProductKey in _omsDownloadableProductKey.Table
                        join pimDownloadableProductKey in _pimDownloadableProductKey.Table on omsDownloadableProductKey.PimDownloadableProductKeyId equals pimDownloadableProductKey.PimDownloadableProductKeyId
                        join pimDownloadableProduct in _pimDownloadableProduct.Table on pimDownloadableProductKey.PimDownloadableProductId equals pimDownloadableProduct.PimDownloadableProductId
                        where pimDownloadableProduct.SKU == sku && pimDownloadableProductKey.IsUsed && omsDownloadableProductKey.OmsOrderLineItemsId == omsOrderLineItemsId
                        select new { keys = pimDownloadableProductKey.DownloadableProductKey }.keys;

                    productKey = string.Join(",", productKeyDetails);
                }
            }
            return productKey;
        }

        //Get order status that will be shown to customer.
        public virtual string GetOrderStatusForCustomer(OrderLineItemModel lineItem)
        {
            IZnodeRepository<ZnodeOmsOrderStateShowToCustomer> _orderStateShowToCustomerRepository = new ZnodeRepository<ZnodeOmsOrderStateShowToCustomer>();
            return _orderStateShowToCustomerRepository.Table.FirstOrDefault(x => x.OmsOrderStateId == lineItem.OrderLineItemStateId)?.OrderStateName;
        }

        protected virtual string GetOrderStatusForCustomer(int? orderLineItemStateId, List<ZnodeOmsOrderStateShowToCustomer> orderStatusList)
        {
            if (orderStatusList?.Count > 0 && orderLineItemStateId > 0)
            {
                return orderStatusList.FirstOrDefault(x => x.OmsOrderStateId == orderLineItemStateId)?.OrderStateName;
            }
            return string.Empty;
        }

        //Map Order details data to OrderModel.
        public virtual void MapOrderData(OrderModel orderModel, ZnodeOmsOrderDetail orderDetail, bool isOrderHistory)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            orderModel.CustomerPaymentGUID = orderDetail?.ZnodeUser?.CustomerPaymentGUID;
            orderModel.ShippingId = (orderDetail?.ZnodeShipping?.ShippingId).GetValueOrDefault();
            orderModel.TrackingUrl = orderDetail?.ZnodeShipping?.TrackingUrl;
            int orderShipmentId = (orderModel.OrderLineItems.FirstOrDefault()?.OmsOrderShipmentId).GetValueOrDefault();

            orderModel.BillingAddress = orderDetail?.ToModel<AddressModel>();

            IList<ZnodeState> stateList = new ZnodeRepository<ZnodeState>().GetEntityList("")?.ToList();
            orderModel.BillingAddress.StateCode = stateList?.FirstOrDefault(x => x.StateName == orderModel?.BillingAddress.StateName)?.StateCode;

            if (IsNotNull(orderModel.BillingAddress))
            {
                ZnodeAddress billing = _addressRepository.Table.FirstOrDefault(x => x.AddressId == orderModel.BillingAddress.AddressId);
                orderModel.BillingAddress.CompanyName = billing?.CompanyName;
                //Sets the external id for billing address.
                orderModel.BillingAddress.ExternalId = billing?.ExternalId;
            }
            orderModel.ShippingAddress = _orderShipmentRepository.Table.FirstOrDefault(x => x.OmsOrderShipmentId == orderShipmentId)?.ToModel<AddressModel>();

            if (IsNotNull(orderModel.ShippingAddress))
            {
                ZnodeAddress shipping = _addressRepository.Table.FirstOrDefault(x => x.AddressId == orderModel.ShippingAddress.AddressId);
                orderModel.ShippingAddress.CompanyName = shipping?.CompanyName;
                //Sets the external id for shipping address.
                orderModel.ShippingAddress.ExternalId = shipping?.ExternalId;
            }
            //Check UserExpand
            GetUserDetails(orderModel.UserId, orderModel);

            if (isOrderHistory)
                MapOrderHistory(orderModel);
            MapPortalData(orderModel);
            MapShoppingCartData(orderModel);
            MapReturnItems(orderModel);
        }

        //Sets the external id in address model.
        public virtual void SetExternalId(AddressModel address)
        {
            if (IsNotNull(address))
            {
                address.ExternalId = GetExternalId(address.AddressId);
                ZnodeLogging.LogMessage("AddressModel with:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { AddressId = address?.AddressId, ExternalId = address?.ExternalId });
            }
        }

        //Gets the external id.
        public virtual string GetExternalId(int addressId)
            => addressId > 0 ? _addressRepository.Table.FirstOrDefault(x => x.AddressId == addressId)?.ExternalId : string.Empty;

        //Map returned order line item.
        public virtual void MapReturnItems(OrderModel orderModel)
        {
            if (IsNotNull(orderModel))
            {
                List<ShoppingCartItemModel> returnCartItems = orderModel.ShoppingCartModel?.ShoppingCartItems.Where(x => x.OrderLineItemStatus == ZnodeOrderStatusEnum.RETURNED.ToString()).ToList();
                MapProductNameForGroupProduct(orderModel, returnCartItems);
                orderModel.ShoppingCartModel?.ShoppingCartItems.RemoveAll(x => x.OrderLineItemStatus == ZnodeOrderStatusEnum.RETURNED.ToString());
                orderModel.ReturnItemList = new ReturnOrderLineItemListModel() { ReturnItemList = GetReturnItemData(returnCartItems) };
                ZnodeLogging.LogMessage("ReturnItemList count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { ReturnItemListCount = orderModel?.ReturnItemList?.ReturnItemList?.Count });
                if (orderModel.ReturnItemList.ReturnItemList?.Count > 0)
                {
                    orderModel.ReturnItemList.SubTotal = orderModel.ReturnItemList.ReturnItemList.Sum(x => x.ExtendedPrice);
                    orderModel.ReturnItemList.ShippingCost = orderModel.ReturnItemList.ReturnItemList.Sum(x => x.ShippingCost);
                    orderModel.ReturnItemList.TaxCost = orderModel.ReturnItemList.ReturnItemList.Sum(x => x.TaxCost);
                    orderModel.ReturnItemList.Total = (orderModel.ReturnItemList.SubTotal + orderModel.ReturnItemList.TaxCost);
                }
            }
        }

        private void MapProductNameForGroupProduct(OrderModel orderModel, List<ShoppingCartItemModel> returnCartItems)
        {
            string productName = orderModel.ShoppingCartModel?.ShoppingCartItems
                                    .Where(p => p.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group)
                                     && p.OrderLineItemStatus != ZnodeOrderStatusEnum.RETURNED.ToString())
                                    .Select(n => n.ProductName)
                                    .FirstOrDefault();
            returnCartItems?.ForEach(m =>
            {
                if (m.GroupProducts?.Count > 0)
                    m.ProductName = productName;
            });
            ZnodeLogging.LogMessage("Return Cart Items details list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { returnCartItemsCount = returnCartItems?.Count });
        }

        //Get returned order line item.
        public virtual List<ReturnOrderLineItemModel> GetReturnItemData(List<ShoppingCartItemModel> returnCartItems)
        {
            List<ReturnOrderLineItemModel> returnItemList = new List<ReturnOrderLineItemModel>();
            if (returnCartItems?.Count > 0)
            {
                returnItemList.AddRange(returnCartItems.Select(x => new ReturnOrderLineItemModel
                {
                    Description = x.Description,
                    ExtendedPrice = x.ExtendedPrice,
                    ProductId = x.ProductId,
                    Quantity = x.Quantity,
                    ShippingCost = x.ShippingCost,
                    ProductDiscountAmount = x.ProductDiscountAmount,
                    ShippingOptionId = x.ShippingOptionId,
                    SKU = x.SKU,
                    UnitPrice = x.UnitPrice,
                    CartDescription = x.CartDescription,
                    CurrencyCode = x.CurrencyCode,
                    CultureCode = x.CultureCode,
                    ImagePath = x.ImagePath,
                    MediaConfigurationId = x.MediaConfigurationId,
                    ProductName = x.ProductName,
                    ProductType = x.ProductType,
                    ImageMediumPath = x.ImageMediumPath,
                    AddOnProductSKUs = x.AddOnProductSKUs,
                    BundleProductSKUs = x.BundleProductSKUs,
                    ConfigurableProductSKUs = x.ConfigurableProductSKUs,
                    GroupProducts = x.GroupProducts,
                    ProductCode = x.ProductCode,
                    TrackingNumber = x.TrackingNumber,
                    UOM = x.UOM,
                    IsEditStatus = x.IsEditStatus,
                    ShipSeperately = x.ShipSeperately,
                    OmsOrderStatusId = x.OmsOrderStatusId,
                    OmsOrderLineItemsId = x.OmsOrderLineItemsId,
                    OrderLineItemStatus = x.OrderLineItemStatus,
                    CustomText = x.CustomText,
                    TaxCost = x.TaxCost,
                    ReasonForReturnId = x.RmaReasonForReturnId,
                    ReasonForReturn = x.RmaReasonForReturn,
                    PersonaliseValuesList = x.PersonaliseValuesList,
                    IsShippingReturn = x.IsShippingReturn
                }).ToList());
            }
            ZnodeLogging.LogMessage("returnItemList count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { returnItemListCount = returnItemList?.Count });
            return returnItemList;
        }

        //Map order history from histroy as well as notes.
        public virtual void MapOrderHistory(OrderModel orderModel)
        {
            //SP call to revert order inventory, update this code once dba provide the sp.
            IZnodeViewRepository<OrderHistoryModel> objStoredProc = new ZnodeViewRepository<OrderHistoryModel>();
            objStoredProc.SetParameter("@OrderId", orderModel.OmsOrderId, ParameterDirection.Input, DbType.Int32);
            IList<OrderHistoryModel> list = objStoredProc.ExecuteStoredProcedureList("Znode_GetOrderHistory @OrderId");
            ZnodeLogging.LogMessage("Order history list count and OmsOrderId to get order history: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OrderHistoryListCount = list?.Count, OmsOrderId = orderModel?.OmsOrderId });
            orderModel.OrderHistoryList.OrderHistoryList = list?.ToList();
        }

        //Map Portal related data.
        public virtual void MapPortalData(OrderModel orderModel)
        {
            UserModel userInfo = _userService.GetUserById(orderModel.UserId, null);
            ZnodeLogging.LogMessage("orderModel with:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { UserId = orderModel?.UserId, PortalId = orderModel?.PortalId });

            if (IsNotNull(userInfo?.PublishCatalogId))
                orderModel.PortalCatalogId = userInfo.PublishCatalogId.Value;
            else
                orderModel.PortalCatalogId = (_portalCatalogRepository.Table.FirstOrDefault(x => x.PortalId == orderModel.PortalId)?.PublishCatalogId).GetValueOrDefault();

            orderModel.StoreName = _portalRepository.Table?.FirstOrDefault(x => x.PortalId == orderModel.PortalId)?.StoreName;
        }

        //Map ShoppingCart related data.
        public virtual void MapShoppingCartData(OrderModel orderModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            orderModel.ShoppingCartModel = GetShoppingCartByOrderId(orderModel.OmsOrderId, orderModel.PortalId, orderModel.UserId, orderModel.PortalCatalogId);

            if (IsNotNull(orderModel.ShoppingCartModel))
            {
                orderModel.ShoppingCartModel.ShippingId = orderModel.ShippingId;
                orderModel.ShoppingCartModel.ShippingAddress = orderModel.ShippingAddress;
                orderModel.ShoppingCartModel.BillingAddress = orderModel.BillingAddress;
                orderModel.ShoppingCartModel.Shipping.ShippingId = orderModel.ShippingId;
                orderModel.ShoppingCartModel.Shipping.ShippingCountryCode = orderModel.ShippingAddress?.CountryName;
                orderModel.PortalCatalogId = orderModel.ShoppingCartModel.PublishedCatalogId;
                if (orderModel.IsTaxCostEdited)
                    orderModel.ShoppingCartModel.CustomTaxCost = orderModel.TaxCost;
                if (orderModel.IsShippingCostEdited)
                    orderModel.ShoppingCartModel.CustomShippingCost = orderModel.ShippingCost;
            }

            List<string> downloadableProductkeys = GetDownloadableProductKeyList(orderModel.ShoppingCartModel.ShoppingCartItems?.Select(x => x.SKU)?.Distinct()?.ToList());
            if (downloadableProductkeys?.Count > 0)
            {
                foreach (ShoppingCartItemModel lineItem in orderModel.ShoppingCartModel.ShoppingCartItems)
                {
                    bool IsDownloadableSKU = downloadableProductkeys.Any(x => x == lineItem.SKU);
                    if (IsDownloadableSKU)
                    {
                        int? parentOmsOrderLineItemsId = orderModel.OrderLineItems.FirstOrDefault(x => x.OmsOrderLineItemsId == lineItem.OmsOrderLineItemsId).ParentOmsOrderLineItemsId;
                        List<OrderLineItemModel> downloadableItemlist = orderModel.OrderLineItems.Where(x => x.OmsOrderLineItemsId == parentOmsOrderLineItemsId).ToList();
                        if (downloadableItemlist.Count > 0)
                            foreach (OrderLineItemModel item in downloadableItemlist)
                                lineItem.DownloadableProductKey = GetProductKey(item.Sku, item.OmsOrderLineItemsId, downloadableProductkeys);
                    }
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //to save order after performing validation and calculation in database
        public virtual OrderModel SaveOrder(ShoppingCartModel model, SubmitOrderModel updateordermodel = null)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging log = new ZnodeLogging();

            bool isUpdateAction = model.OmsOrderId > 0;
            bool isEnableBcc = false;
            model.OrderAttribute = DefaultGlobalConfigSettingHelper.DefaultOrderAttribute;
            if (model?.PublishStateId < 1)
                model.PublishStateId = PublishStateId;

            SetStateCode(model);

            UserAddressModel userDetails = SetUserDetails(model);
            //Get refunded line item for CCH tax.
            GetRefundedLineItemForCCH(model, updateordermodel);
            IZnodeCheckout checkout = SetCheckoutData(userDetails, model, log);
            // Perform validation and start the timer
            ValidateCheckout(checkout);
            log.LogActivityTimerStart();

            // Instantiate the order fullfillment
            ZnodeOrderFulfillment order;
            string isInventoryInStockMessage = string.Empty;
            Dictionary<int, string> minMaxSelectableQuantity = new Dictionary<int, string>();
            try
            {
                // Do pre-submit processing
                bool preSubmitOrderSuccess = checkout.ShoppingCart.PreSubmitOrderProcess(out isInventoryInStockMessage, out minMaxSelectableQuantity);
                if (preSubmitOrderSuccess)
                {
                    checkout.ShoppingCart.ReturnOrderLineItem(model);

                    order = checkout.SubmitOrder(updateordermodel, model);

                    //Save in Quote if it is quote to order.
                    SaveInQuote(model, order);
                }
                else
                {
                    log.LogActivityTimerEnd((int)ZnodeLogging.ErrorNum.OrderSubmissionFailed, null);
                    throw new ZnodeException(ErrorCodes.ProcessingFailed, Admin_Resources.ErrorPresubmitOrderProcessing);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                if (!string.IsNullOrEmpty(isInventoryInStockMessage))
                    throw new ZnodeException(ErrorCodes.OutOfStockException, Admin_Resources.ErrorPlaceOrder);

                if (IsNotNull(minMaxSelectableQuantity) && minMaxSelectableQuantity.Count > 0 && minMaxSelectableQuantity.ContainsKey(ErrorCodes.MinAndMaxSelectedQuantityError))
                    throw new ZnodeException(ErrorCodes.MinAndMaxSelectedQuantityError, minMaxSelectableQuantity[ErrorCodes.MinAndMaxSelectedQuantityError]);

                throw ;
            }

            // If checkout successful then do post-submit processing
            if (checkout.IsSuccess)
            {
                // Do post submit processing
                PostSubmitOrder(order, checkout, model.FeedbackUrl);
                log.LogActivityTimerEnd((int)ZnodeLogging.ErrorNum.OrderSubmissionSuccess, order.OrderID.ToString());
            }
            else
            {
                log.LogActivityTimerEnd((int)ZnodeLogging.ErrorNum.OrderSubmissionFailed, null, null, null, null, checkout.PaymentResponseText);
                throw new Exception(checkout.PaymentResponseText);
            }
            OrderModel orderModel = BindOrderData(order, model);
            ZnodeLogging.LogMessage("ShippingName and ShippingID to get ShippingName from repository: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose,new { ShippingName = checkout?.ShoppingCart?.Shipping?.ShippingName, ShippingID = checkout?.ShoppingCart?.Shipping?.ShippingID });
            checkout.ShoppingCart.Shipping.ShippingName = GetShippingName(checkout.ShoppingCart.Shipping.ShippingName, checkout.ShoppingCart.Shipping.ShippingID);
            if (!isUpdateAction)
            {
                // And finally attach the receipt HTML to the order and return.
                orderModel.ReceiptHtml = GetOrderReceipt(order, checkout, model.FeedbackUrl, model.LocaleId, isUpdateAction, out isEnableBcc);

                if (!string.IsNullOrEmpty(orderModel.ReceiptHtml))
                    orderModel.IsEmailSend = SendOrderReceipt(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, $"{Admin_Resources.TitleOrderReceipt} - {orderModel.OrderNumber}", ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeConfigManager.SiteConfig.AdminEmail, orderModel.ReceiptHtml, isEnableBcc);
            }
            //Send purchased product details Email To Vendor.
            SendEmailToVendor(order, checkout, model.FeedbackUrl, model.LocaleId, isEnableBcc);

            //Send order alert email for store notification
            SendEmailNotification(model, isUpdateAction, checkout, order, orderModel, isEnableBcc);

            //to send line item state change receipt to user.
            SendLineItemStateChangeEmail(order, isEnableBcc);
            ERPInitializer<OrderModel> _erpInc = new ERPInitializer<OrderModel>(orderModel, "CreateOrder");
            if (orderModel?.OrderLineItems.Count > 0)
            {
                List<OrderLineItemModel> downLoadablelineItems = new List<OrderLineItemModel>();
                foreach (OrderLineItemModel item in orderModel.OrderLineItems)
                {
                    var status = (item.OrderLineItemCollection?.Where(x => x.IsDownloadableSKU).ToList());
                    if (status?.Count() > 0)
                        downLoadablelineItems.AddRange(status);
                }
                if (downLoadablelineItems.Any())
                    //Save Downloadable product key to database
                    SaveDownloadableProductKey(model, isUpdateAction, isEnableBcc, checkout, order, orderModel, downLoadablelineItems);

            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderModel;
        }

        public void SaveDownloadableProductKey(ShoppingCartModel model, bool isUpdateAction, bool isEnableBcc, IZnodeCheckout checkout, ZnodeOrderFulfillment order, OrderModel orderModel, List<OrderLineItemModel> downLoadablelineItems)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            List<Tuple<int, string, decimal>> productDetails = SetDownloadableProductKeyDetails(downLoadablelineItems.ToList());
            DataTable orderData = ConvertOrderDataToDataTable(productDetails);

            DataTable keysData = DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled ?
                orderHelper.SaveDownloadableProductKeyWithJSON(orderData, model.UserId.GetValueOrDefault()) :
                orderHelper.SaveDownloadableProductKey(orderData, model.UserId.GetValueOrDefault());
            downLoadablelineItems.ForEach(m =>
            {
                if (m.OmsOrderLineItemsId == Convert.ToInt32(keysData.Rows[0]["OmsOrderLineItemsId"]))
                {
                    m.DownloadableProductKey = Convert.ToString(keysData.Rows[0]["DownloadableProductKey"]);
                }
            });

            //Send order receipt for downloadable product keys.
            SendOrderReceiptForProductKeys(model, isUpdateAction, checkout, order, orderModel, keysData, isEnableBcc);
        }

        //Keep Product key visible during order update
        private void UpdateProductKey(ShoppingCartModel model, OrderModel orderModel)
        {
            int oldOmsOrderLineItemsId = model.ShoppingCartItems.FirstOrDefault().OmsOrderLineItemsId;
            var productKeyDetail = _omsDownloadableProductKey.Table.Where(x => x.OmsOrderLineItemsId == oldOmsOrderLineItemsId)?.ToList();
            int newOmsOrderLineItemsId = orderModel.OrderLineItems.FirstOrDefault().OmsOrderLineItemsId;
            if (IsNotNull(productKeyDetail))
            {
                productKeyDetail.ForEach(x => x.OmsOrderLineItemsId = newOmsOrderLineItemsId);
                productKeyDetail.ForEach(x => _omsDownloadableProductKey.Update(x));
            }
        }

        //Send order receipt for downloadable product keys.
        public void SendOrderReceiptForProductKeys(ShoppingCartModel model, bool isUpdateAction, IZnodeCheckout checkout, ZnodeOrderFulfillment order, OrderModel orderModel, DataTable keysData, bool isEnableBcc = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            DownloadableProductKeyListModel keys;
            keys = ConvertToParamList(keysData);
            if (keys?.DownloadableProductKeys?.Count > 0)
            {
                // And finally attach the receipt HTML to the order and return.
                orderModel.KeyReceiptHtml = GetDownloadableProductOrderReceipt(order, checkout, model.FeedbackUrl, model.LocaleId, keys, isUpdateAction, out isEnableBcc);

                if (!string.IsNullOrEmpty(orderModel.KeyReceiptHtml))
                    SendOrderReceipt(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, $"{Admin_Resources.TitleOrderReceipt} - {orderModel.OrderNumber}", ZnodeConfigManager.SiteConfig.AdminEmail, ZnodeConfigManager.SiteConfig.AdminEmail, orderModel.KeyReceiptHtml, isEnableBcc);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //This method will convert the datable to List model.
        public DownloadableProductKeyListModel ConvertToParamList(DataTable dataTable)
        {
            DownloadableProductKeyListModel model = new DownloadableProductKeyListModel();
            model.DownloadableProductKeys = new List<DownloadableProductKeyModel>();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                foreach (DataRow dr in dataTable.Rows)
                    model.DownloadableProductKeys.Add(new DownloadableProductKeyModel { SKU = dr["SKU"].ToString(), DownloadableProductKey = dr["DownloadableProductKey"].ToString(), OmsOrderLineItemsId = Convert.ToInt32(dr["OmsOrderLineItemsId"].ToString()), DownloadableProductURL = dr["DownloadableProductURL"].ToString(), ProductName = dr["ProductName"].ToString() });
            }
            ZnodeLogging.LogMessage("DownloadableProductKeys list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { DownloadableProductKeysListCount = model?.DownloadableProductKeys?.Count });
            return model;
        }

        //Set Downloadable Product Key Details to tuples
        public List<Tuple<int, string, decimal>> SetDownloadableProductKeyDetails(List<OrderLineItemModel> OrderLineItem)
        {
            ZnodeLogging.LogMessage("OrderLineItem list count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OrderLineItemListCount = OrderLineItem?.Count });
            List<Tuple<int, string, decimal>> productKeyDetails = new List<Tuple<int, string, decimal>>();

            foreach (OrderLineItemModel product in OrderLineItem)
            {
                int orderLineItemId = product.OmsOrderLineItemsId;
                string sku = product.Sku;
                decimal quantity = product.Quantity;

                productKeyDetails.Add(new Tuple<int, string, decimal>(orderLineItemId, sku, quantity));
            }
            ZnodeLogging.LogMessage("productKeyDetails list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { productKeyDetailsListCount = productKeyDetails?.Count });
            return productKeyDetails;
        }

        // te data table  and item into it.
        public DataTable ConvertOrderDataToDataTable(List<Tuple<int, string, decimal>> productDetails)
        {
            DataTable table = new DataTable("OMSDownloadableProduct");
            table.Columns.Add("OmsOrderLineItemsId", typeof(int));
            table.Columns.Add("SKU", typeof(string));
            table.Columns.Add("Quantity", typeof(decimal));

            foreach (Tuple<int, string, decimal> item in productDetails)
                table.Rows.Add(item.Item1, item.Item2, item.Item3);

            return table;
        }

        //Get refunded line item for cch tax.
        public virtual void GetRefundedLineItemForCCH(ShoppingCartModel model, SubmitOrderModel updateordermodel)
        {
            if (updateordermodel?.ReturnOrderLineItems?.ReturnItemList?.Count > 0)
            {
                string[] refundedSkusList = updateordermodel.RefundedSkus.Split(',');
                model.ReturnItemList = IsNotNull(refundedSkusList)
                    ? updateordermodel?.ReturnOrderLineItems?.ReturnItemList.Where(c => refundedSkusList.Contains(Convert.ToString(c.OmsOrderLineItemsId))).ToList()
                    : updateordermodel.ReturnOrderLineItems.ReturnItemList;
                model.IsCchCalculate = true;
                ZnodeLogging.LogMessage("ReturnItemList Count: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { ReturnItemListCount = model?.ReturnItemList });
            }
        }

        // Get shipping name.
        public virtual string GetShippingName(string shippingName, int shippingId) => !string.IsNullOrEmpty(shippingName) ? shippingName : Convert.ToString((_shippingRepository.Table.Where(w => w.ShippingId == shippingId).Select(s => s.Description).FirstOrDefault()));

        //to get shipping address
        public virtual string GetOrderShipmentAddress(OrderShipmentModel orderShipment)
        {
            if (IsNotNull(orderShipment))
            {
                ZnodeLogging.LogMessage("AddressId to get shipping company name",ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { AddressId = orderShipment.AddressId });
                string ShippingcompanyName = _addressRepository.Table.FirstOrDefault(x => x.AddressId == orderShipment.AddressId)?.CompanyName;

                string street1 = string.IsNullOrEmpty(orderShipment.ShipToStreet2) ? string.Empty : "<br />" + orderShipment.ShipToStreet2;
                orderShipment.ShipToCompanyName = IsNotNull(orderShipment?.ShipToCompanyName) ? $"{orderShipment?.ShipToCompanyName}{"<br />"}" : ShippingcompanyName;
                return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{"<br />"}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{"<br />"}{orderShipment.ShipToStateCode}{"<br />"}{orderShipment.ShipToPostalCode}{"<br />"}{orderShipment.ShipToCountry}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}{"<br />"}{WebStore_Resources.TitleEmail}{" : "}{orderShipment.ShipToEmailId}";
            }
            return string.Empty;
        }

        //to get shipping address
        protected virtual string GetOrderShipmentAddress(OrderShipmentModel orderShipment, List<ZnodeAddress> addressList)
        {
            if (IsNotNull(orderShipment) && addressList?.Count > 0)
            {
                string ShippingcompanyName = addressList.Where(x => x.AddressId == orderShipment.AddressId)?.FirstOrDefault()?.CompanyName;

                string street1 = string.IsNullOrEmpty(orderShipment.ShipToStreet2) ? string.Empty : "<br />" + orderShipment.ShipToStreet2;
                orderShipment.ShipToCompanyName = IsNotNull(orderShipment?.ShipToCompanyName) ? $"{orderShipment?.ShipToCompanyName}{"<br />"}" : ShippingcompanyName;
                return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{"<br />"}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{"<br />"}{orderShipment.ShipToStateCode}{"<br />"}{orderShipment.ShipToPostalCode}{"<br />"}{orderShipment.ShipToCountry}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}{"<br />"}{WebStore_Resources.TitleEmail}{" : "}{orderShipment.ShipToEmailId}";
            }
            return string.Empty;
        }

        //to get shipping address
        public virtual string GetOrderBillingAddress(OrderModel orderBilling)
        {
            if (IsNotNull(orderBilling))
            {
                string street1 = string.IsNullOrEmpty(orderBilling.BillingAddress.Address2) ? string.Empty : "<br />" + orderBilling.BillingAddress.Address2;
                return $"{orderBilling?.BillingAddress.FirstName}{" "}{orderBilling?.BillingAddress.LastName}{"<br />"}{orderBilling?.BillingAddress.CompanyName}{"<br />"}{orderBilling.BillingAddress.Address1}{street1}{"<br />"}{ orderBilling.BillingAddress.CityName}{"<br />"}{(string.IsNullOrEmpty(orderBilling.BillingAddress.StateCode) ? orderBilling.BillingAddress.StateName : orderBilling.BillingAddress.StateCode)}{"<br />"}{orderBilling.BillingAddress.PostalCode}{"<br />"}{orderBilling.BillingAddress.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderBilling.BillingAddress.PhoneNumber}";
            }
            return string.Empty;
        }

        //Get expands and add them to navigation properties
        public virtual List<string> GetExpands(NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            List<string> navigationProperties = new List<string>();
            ZnodeLogging.LogMessage("Expands details:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, expands);
            if (expands?.HasKeys() ?? false)
            {
                foreach (string key in expands.Keys)
                {
                    //Add expand keys
                    if (Equals(key.ToLower(), ZnodeOmsOrderEnum.ZnodeOmsOrderDetails.ToString().ToLower())) SetExpands(ZnodeOmsOrderEnum.ZnodeOmsOrderDetails.ToString(), navigationProperties);
                    if (Equals(key.ToLower(), ZnodeOmsOrderLineItemEnum.ZnodeOmsOrderShipment.ToString().ToLower())) SetExpands(ZnodeOmsOrderLineItemEnum.ZnodeOmsOrderShipment.ToString(), navigationProperties);
                    if (Equals(key.ToLower(), ZnodeOmsOrderLineItemEnum.ZnodeOmsOrderAttributes.ToString().ToLower())) SetExpands(ZnodeOmsOrderLineItemEnum.ZnodeOmsOrderAttributes.ToString(), navigationProperties);
                    if (Equals(key, ZnodeOmsOrderDetailEnum.ZnodeOmsPaymentState.ToString().ToLower())) SetExpands(ZnodeOmsOrderDetailEnum.ZnodeOmsPaymentState.ToString(), navigationProperties);
                    if (Equals(key, ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString().ToLower())) SetExpands(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString(), navigationProperties);
                    if (Equals(key, ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString().ToLower())) SetExpands(ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString(), navigationProperties);
                    if (Equals(key, ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower())) SetExpands(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString(), navigationProperties);
                    if (Equals(key, ExpandKeys.ZnodeUser)) SetExpands(ExpandKeys.ZnodeUser, navigationProperties);
                    if (Equals(key, ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString().ToLower())) SetExpands(ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString(), navigationProperties);
                    if (Equals(key, ExpandKeys.ZnodeShipping)) SetExpands(ExpandKeys.ZnodeShipping, navigationProperties);
                    if (Equals(key, ExpandKeys.ZnodeOmsOrderLinePersonalize)) SetExpands(ExpandKeys.ZnodeOmsOrderLinePersonalize, navigationProperties);
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return navigationProperties;
        }

        //To set shopping cart data to checkout object
        public virtual IZnodeCheckout SetShoppingCartDataToCheckout(IZnodeCheckout checkout, ShoppingCartModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            checkout.ShoppingCart.LocalId = model.LocaleId;
            checkout.ShoppingCart.PublishedCatalogId = model.PublishedCatalogId;
            checkout.ShoppingCart.OrderDate = model.OrderDate;
            checkout.ShoppingCart.GiftCardAmount = model.GiftCardAmount;
            checkout.ShoppingCart.GiftCardMessage = model.GiftCardMessage;
            checkout.ShoppingCart.GiftCardNumber = model.GiftCardNumber;
            checkout.ShoppingCart.IsGiftCardApplied = model.GiftCardApplied;
            checkout.ShoppingCart.IsGiftCardValid = model.GiftCardValid;
            checkout.ShoppingCart.CreditCardNumber = model.CreditCardNumber;
            checkout.ShoppingCart.CSRDiscountAmount = model.CSRDiscountAmount;
            checkout.ShoppingCart.CSRDiscountDescription = model.CSRDiscountDescription;
            checkout.ShoppingCart.CSRDiscountApplied = model.CSRDiscountApplied;
            checkout.ShoppingCart.CSRDiscountMessage = model.CSRDiscountMessage;
            checkout.ShoppingCart.CustomShippingCost = model.CustomShippingCost;
            checkout.ShoppingCart.CustomTaxCost = model.CustomTaxCost;
            checkout.ShoppingCart.OrderAttribute = model.OrderAttribute;
            checkout.ShoppingCart.CurrencyCode = model.CurrencyCode;
            checkout.ShoppingCart.CultureCode = model.CultureCode;
            checkout.ShoppingCart.UserId = model.UserId;
            checkout.ShoppingCart.ExternalId = model.ExternalId;
            checkout.ShoppingCart.CardType = model.CardType;
            checkout.ShoppingCart.CreditCardExpMonth = model.CreditCardExpMonth;
            checkout.ShoppingCart.CreditCardExpYear = model.CreditCardExpYear;
            checkout.ShoppingCart.LoginUserName = model.UserDetails?.LoginName ?? string.Empty;
            checkout.ShoppingCart.IsLineItemReturned = model.IsLineItemReturned;
            if (IsNotNull(model?.Coupons))
            {
                foreach (CouponModel coupon in model.Coupons)
                    checkout.ShoppingCart.Coupons.Add(CouponMap.ToZnodeCoupon(coupon));
            }
            checkout.ShoppingCart.PortalID = model.PortalId;
            checkout.ShoppingCart.VAT = model.Vat.GetValueOrDefault();
            checkout.ShoppingCart.HST = model.Hst.GetValueOrDefault();
            checkout.ShoppingCart.GST = model.Gst.GetValueOrDefault();
            checkout.ShoppingCart.PST = model.Pst.GetValueOrDefault();
            checkout.AdditionalInstructions = model.AdditionalInstructions;
            checkout.PurchaseOrderNumber = model.PurchaseOrderNumber;
            checkout.PoDocument = model.PODocumentName;
            checkout.PortalID = model.PortalId;
            checkout.ShoppingCart.Payment = PaymentMap.ToZnodePayment(model.Payment);
            checkout.ShoppingCart.Shipping = ShippingMap.ToZnodeShipping(model.Shipping);
            checkout.ShippingID = checkout.ShoppingCart.Shipping.ShippingID;
            checkout.ShoppingCart.Payment.PaymentSettingId = model.Payment?.PaymentSetting == null || model.Payment?.PaymentSetting.PaymentSettingId == 0 ? null : model.Payment?.PaymentSetting.PaymentSettingId;
            checkout.ShoppingCart.ReturnItemList = model.ReturnItemList;
            checkout.ShoppingCart.IsCchCalculate = model.IsCchCalculate;
            checkout.ShoppingCart.IsAllowWithOtherPromotionsAndCoupons = model.IsAllowWithOtherPromotionsAndCoupons;
            checkout.ShoppingCart.EstimateShippingCost = model.EstimateShippingCost;
            checkout.ShoppingCart.Custom1 = model.Custom1;
            checkout.ShoppingCart.Custom2 = model.Custom2;
            checkout.ShoppingCart.Custom3 = model.Custom3;
            checkout.ShoppingCart.Custom4 = model.Custom4;
            checkout.ShoppingCart.Custom5 = model.Custom5;
            if (IsNotNull(model.OrderShipment))
            {
                // Do the cart calculation
                checkout.ShoppingCart.AddressCarts.ForEach(x =>
                {
                    x.Shipping = string.IsNullOrEmpty(x.Shipping.ShippingName) ? new Libraries.ECommerce.Entities.ZnodeShipping
                    {
                        ShippingID = checkout.ShoppingCart.Shipping.ShippingID,
                        ShippingName = checkout.ShoppingCart.Shipping.ShippingName,
                        ShippingCountryCode = checkout?.ShoppingCart?.Shipping.ShippingCountryCode
                    } : x.Shipping;
                    var address = _addressRepository.GetById(x.AddressID);
                    checkout.ShoppingCart.Payment = PaymentMap.ToZnodePayment(model.Payment, address.ToModel<AddressModel>());
                    x.Payment = checkout.ShoppingCart.Payment;
                    x.PortalId = checkout.PortalID;
                    x.UserId = checkout.ShoppingCart.UserId;
                    x.CurrencyCode = checkout.ShoppingCart.CurrencyCode;
                    x.CultureCode = checkout.ShoppingCart.CultureCode;
                    x.Coupons = checkout.ShoppingCart.Coupons;
                    x.PublishStateId = checkout.ShoppingCart.PublishStateId;
                    x.IsAllowWithOtherPromotionsAndCoupons = checkout.ShoppingCart.IsAllowWithOtherPromotionsAndCoupons;
                    x.Calculate();
                });
            }
            else
            {
                // Do the cart calculation
                checkout.ShoppingCart.AddressCarts.ForEach(x =>
                {
                    x.Shipping = string.IsNullOrEmpty(x.Shipping.ShippingName) ? new Libraries.ECommerce.Entities.ZnodeShipping
                    {
                        ShippingID = checkout.ShoppingCart.Shipping.ShippingID,
                        ShippingName = checkout.ShoppingCart.Shipping.ShippingName,
                        ShippingCountryCode = checkout?.ShoppingCart?.Shipping.ShippingCountryCode
                    } : x.Shipping;
                    x.Payment = checkout.ShoppingCart.Payment;
                    x.PortalId = checkout.PortalID;
                    x.UserId = checkout.ShoppingCart.UserId;
                    x.CurrencyCode = checkout.ShoppingCart.CurrencyCode;
                    x.CultureCode = checkout.ShoppingCart.CultureCode;
                    x.OrderId = checkout.ShoppingCart.OrderId;
                    x.IsCchCalculate = checkout.ShoppingCart.IsCchCalculate;
                    x.ReturnItemList = checkout.ShoppingCart.ReturnItemList;
                    x.OrderDate = checkout.ShoppingCart.OrderDate;
                    x.Coupons = checkout.ShoppingCart.Coupons;
                    x.PublishStateId = checkout.ShoppingCart.PublishStateId;
                    x.IsAllowWithOtherPromotionsAndCoupons = checkout.ShoppingCart.IsAllowWithOtherPromotionsAndCoupons;
                    //x.Calculate();
                });
            }

            checkout.ShoppingCart.Calculate();

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return checkout;
        }

        //To revert product inventory for updating existion order .
        public virtual bool RevertOrderInventory(int orderId, int? userId, string omsOrderLineitemIds = "", int isRevertAll = 0)
        {
            if (orderId > 0)
            {
                ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId, userId = userId, omsOrderLineitemIds = omsOrderLineitemIds, isRevertAll = isRevertAll });
                int? omsOrderdetailId = _orderDetailsRepository.Table.Where(x => x.OmsOrderId == orderId && x.IsActive)?.FirstOrDefault().OmsOrderDetailsId;

                //SP call to revert order inventory, update this code once dba provide the sp.
                IZnodeViewRepository<View_ReturnBoolean> objStoredProc = new ZnodeViewRepository<View_ReturnBoolean>();
                objStoredProc.SetParameter(ZnodeOmsOrderDetailEnum.OmsOrderDetailsId.ToString(), omsOrderdetailId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("OmsOrderLineItemIds", omsOrderLineitemIds, ParameterDirection.Input, DbType.String);
                objStoredProc.SetParameter(View_ReturnBooleanEnum.Status.ToString(), null, ParameterDirection.Output, DbType.Int32);
                objStoredProc.SetParameter(ZnodeUserEnum.UserId.ToString(), userId, ParameterDirection.Input, DbType.Int32);
                objStoredProc.SetParameter("IsRevertAll", isRevertAll, ParameterDirection.Input, DbType.Int32);
                int status = 0;
                objStoredProc.ExecuteStoredProcedureList("Znode_RevertOrderInventory @OmsOrderDetailsId, @OmsOrderLineItemIds, @Status OUT, @UserId,@IsRevertAll", 2, out status);

                if (status == 1)
                    ZnodeLogging.LogMessage(string.Format(Admin_Resources.SuccessOrderInventoryRevert, orderId), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                else
                    ZnodeLogging.LogMessage(string.Format(Admin_Resources.ErrorOrderInventoryRevert, orderId), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                return status == 1;
            }
            return false;
        }

        //Get order details by using expands.
        public virtual void GetExpands(NameValueCollection expands, OrderModel order)
        {
            if (expands.HasKeys())
            {
                ExpandOrderLineItems(expands, order);
                ExpandStore(expands, order);
                ExpandPaymentType(expands, order);
                ExpandOmsOrderState(expands, order);
                ExpandShoppingCart(expands, order);
                ExpandPaymentState(expands, order);
                ExpandShippingType(expands, order);
                ExpandOmsOrderNote(expands, order);
                ExpandUserDetails(expands, order);
                ExpandPaymentStatusList(expands, order);
            }
        }

        //Set shopping cart items by order line item details.
        public virtual void ExpandShoppingCart(NameValueCollection expands, OrderModel order)
        {
            ZnodeLogging.LogMessage("expands details:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, expands);
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ShoppingCart)))
            {
                //Get order details as shopping cart by order id.
                ShoppingCartModel shoppingCart = GetShoppingCartByOrderId(order.OmsOrderId, order.PortalId, order.UserId, order.PortalCatalogId);

                if (IsNotNull(shoppingCart))
                    order.ShoppingCartModel = shoppingCart;
            }
        }

        //Get order details as shopping cart by order id.
        public virtual ShoppingCartModel GetShoppingCartByOrderId(int orderId, int portalId, int userId, int portalCatalogId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage(" Input parameters OrderId, portalId, userId, portalCatalogId :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new object[] { orderId, portalId, userId, portalCatalogId });
            //Get catalog list by portal id.
            if (portalCatalogId <= 0)
                portalCatalogId = (_portalCatalogRepository.Table.FirstOrDefault(x => x.PortalId == portalId)?.PublishCatalogId).GetValueOrDefault();

            IShoppingCartService _shoppingCartService = GetService<IShoppingCartService>();

            //Get shopping cart model by using orderId.
            ShoppingCartModel shoppingCart = _shoppingCartService.GetShoppingCart(new CartParameterModel
            {
                LocaleId = GetLocaleIdFromHeader(),
                PortalId = portalId,
                UserId = userId,
                PublishedCatalogId = portalCatalogId > 0 ? portalCatalogId : 0,
                OmsOrderId = orderId
            });
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return shoppingCart;
        }

        //Get order line item by order id.
        public virtual void ExpandOrderLineItems(NameValueCollection expands, OrderModel order)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.OrderLineItems)))
            {
                //Expand for oms order shipment.
                expands.Add(ZnodeOmsOrderLineItemEnum.ZnodeOmsOrderShipment.ToString(), ZnodeOmsOrderLineItemEnum.ZnodeOmsOrderShipment.ToString());
                //Expand for oms order attributes.
                expands.Add(ZnodeOmsOrderLineItemEnum.ZnodeOmsOrderAttributes.ToString(), ZnodeOmsOrderLineItemEnum.ZnodeOmsOrderAttributes.ToString());

                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodeOmsOrderLineItemEnum.OmsOrderDetailsId.ToString(), FilterOperators.In, order.OmsOrderDetailsId.ToString()));
                string whereClause = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause;
                ZnodeLogging.LogMessage("WhereClause for GetEntityList:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, whereClause);
                List<ZnodeOmsOrderLineItem> orderLineItemList = _orderLineItemRepository.GetEntityList(whereClause, GetExpands(expands))?.ToList();
                //Map the Order Line Item to Order Model.
                if (IsNotNull(orderLineItemList))
                {
                    if (IsNull(order.OrderLineItems))
                        order.OrderLineItems = new List<OrderLineItemModel>();

                    foreach (ZnodeOmsOrderLineItem orderLineItem in orderLineItemList)
                        order.OrderLineItems.Add(orderLineItem.ToModel<OrderLineItemModel>());
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //Get order shipment details by OmsOrderShipmentId.
        public virtual void ExpandOrderShipment(NameValueCollection expands, OrderLineItemModel orderLineItemModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            if (!string.IsNullOrEmpty(expands.Get(ExpandKeys.OrderShipment)))
            {
                List<ZnodeOmsOrderShipment> orderShipmentList = _orderShipmentRepository.Table.Where(w => w.OmsOrderShipmentId == orderLineItemModel.OmsOrderShipmentId).ToList();
                ZnodeLogging.LogMessage("orderShipmentList count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, orderShipmentList?.Count);

                //Map the Order Line Item to Order Model.
                if (IsNotNull(orderShipmentList))
                {
                    foreach (ZnodeOmsOrderShipment orderShipment in orderShipmentList)
                        orderLineItemModel.ZnodeOmsOrderShipment = orderShipment.ToModel<OrderShipmentModel>();
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //Get store name by portal id.
        public virtual void ExpandStore(NameValueCollection expands, OrderModel order)
        {
            ZnodeLogging.LogMessage("OrderModel with PortalId :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, order?.PortalId);

            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Store)))
            {
                ZnodePortal portal = _portalRepository.GetById(order.PortalId);

                //Map the Store name to order model.
                if (IsNotNull(portal))
                    order.StoreName = portal.StoreName;
            }
        }

        //Get Payment state name by Payment Status Id.
        public virtual void ExpandPaymentState(NameValueCollection expands, OrderModel order)
        {

            if (!string.IsNullOrEmpty(expands.Get(ExpandKeys.OmsPaymentState)))
            {
                IZnodeRepository<ZnodeOmsPaymentState> _paymentStateRepository = new ZnodeRepository<ZnodeOmsPaymentState>();
                ZnodeOmsPaymentState portal = _paymentStateRepository.Table.Where(x => x.OmsPaymentStateId == order.OmsPaymentStateId)?.FirstOrDefault();

                //Map the Paymenet status name to order model.
                if (IsNotNull(portal))
                    order.PaymentStatus = portal.Name;
            }
        }

        //Get payment type name by payment type id.
        public virtual void ExpandPaymentType(NameValueCollection expands, OrderModel order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.PaymentType)))
            {
                IZnodeRepository<ZnodePaymentType> _paymentTypeRepository = new ZnodeRepository<ZnodePaymentType>();
                ZnodePaymentType paymentType = _paymentTypeRepository.Table.Where(w => w.PaymentTypeId == order.PaymentTypeId)?.FirstOrDefault();

                //Map the payment type name to order model.
                if (IsNotNull(paymentType))
                    order.PaymentType = paymentType.Name;
            }
        }

        //Get username, user first and last name by payment type id.
        public virtual void ExpandUserDetails(NameValueCollection expands, OrderModel order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.UserDetails)))
                GetUserDetails(order.UserId, order);
        }

        //Get user deatils by id.
        public virtual void GetUserDetails(int userId, OrderModel order)
        {
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { userId = userId });

            IZnodeRepository<ZnodeUser> _userRepository = new ZnodeRepository<ZnodeUser>();

            ZnodeLogging.LogMessage("UserId to get user details: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { UserId = order?.UserId });
            UserModel userDetails = GetUserNameByUserId(order.UserId);
            if (IsNotNull(order) && IsNotNull(userDetails))
            {
                order.FirstName = userDetails.FirstName;
                order.LastName = userDetails.LastName;
                order.UserName = userDetails.UserName;
                order.CreatedByName = GetUserNameByUserId(order.CreatedBy)?.UserName ?? order.UserName;
            }
            else
            {
                order.UserName = GetUserNameByUserId(order.CreatedBy)?.UserName ?? _userRepository.Table.FirstOrDefault(x => x.UserId == order.UserId)?.Email;
                order.CreatedByName = order.UserName;
            }
        }

        //Get payment status list.
        public virtual void ExpandPaymentStatusList(NameValueCollection expands, OrderModel order)
        {
            if (!string.IsNullOrEmpty(expands.Get(ExpandKeys.PaymentStateList)))
            {
                IZnodeRepository<ZnodeOmsPaymentState> _omsPaymentStateRepository = new ZnodeRepository<ZnodeOmsPaymentState>();
                order.OrderPaymentStateModelList = _omsPaymentStateRepository.GetEntityList(string.Empty).ToModel<OrderPaymentStateModel>().ToList();
            }
        }

        //Get shipping type by shipping type id.
        public virtual void ExpandShippingType(NameValueCollection expands, OrderModel order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ShippingType)) && order.ShippingId > 0)
                order.ShippingTypeName = _shippingRepository.Table.FirstOrDefault(w => w.ShippingId == order.ShippingId)?.Description;
        }

        public virtual void ExpandOmsOrderState(NameValueCollection expands, OrderModel order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.OmsOrderState)))
            {
                IZnodeRepository<ZnodeOmsOrderState> _omsOrderStateTypeRepository = new ZnodeRepository<ZnodeOmsOrderState>();
                ZnodeOmsOrderState omsOrderState = _omsOrderStateTypeRepository.Table.FirstOrDefault(w => w.OmsOrderStateId == order.OmsOrderStateId);

                //Map the order state name to order model.
                if (IsNotNull(omsOrderState))
                    order.OrderState = omsOrderState.OrderStateName;
            }
        }

        //Get Oms Order Note details.
        public virtual void ExpandOmsOrderNote(NameValueCollection expands, OrderModel order)
           => order.OrderNotes = !String.IsNullOrEmpty(expands.Get(ExpandKeys.OrderNotes)) ?
                GetOrderNoteDetails(order.OmsOrderId, 0) : new List<OrderNotesModel>();

        //Assign UserId for Shipping Billing Address.
        public virtual void AssignShippingBillingAddressUserId(UserAddressModel userAddressModel, int userId)
        {
            userAddressModel.BillingAddress.UserId = userId;
            userAddressModel.ShippingAddress.UserId = userId;
        }

        //Set Shipping Billing Address of User.
        public virtual AddressListModel SetShippingBillingAddress(UserAddressModel userAddressModel)
        {
            AddressListModel addressList = new AddressListModel { AddressList = new List<AddressModel>() };

            if (userAddressModel.UseSameAsBillingAddress)
                addressList.AddressList.Add(userAddressModel.ShippingAddress);
            else
            {
                //add shipping/billing address to address list.
                addressList.AddressList.Add(userAddressModel.BillingAddress);
                addressList.AddressList.Add(userAddressModel.ShippingAddress);
            }

            foreach (var address in addressList.AddressList)
            {
                //Sets the IsDefaultBilling and IsDefaultShipping flag to false in database for Customer.
                AddressHelper.SetAddressFlagsToFalse(address);
            }
            return addressList;
        }

        //Create new user.
        public virtual ZnodeUser CreateNewRegisteredUser(ZnodeUser user, UserAddressModel userAddressModel)
        {
            ZnodeLogging.LogMessage("New User with id:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, user?.UserId);

            IZnodeRepository<ZnodeUser> _userRepository = new ZnodeRepository<ZnodeUser>();
            user = _userRepository.Insert(user);
            ZnodeLogging.LogMessage(Admin_Resources.SuccessNewRegisteredUserCreate, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            AssignShippingBillingAddressUserId(userAddressModel, user.UserId);
            return user;
        }

        //Insert/Update shippin/billing address of user.
        public virtual List<ZnodeAddress> InsertUpdateUserAddress(UserAddressModel userAddressModel)
        {
            //Create a list of addresses of user.
            AddressListModel addressList = SetShippingBillingAddress(userAddressModel);

            ZnodeLogging.LogMessage("AddressList count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, addressList?.AddressList?.Count);

            if (addressList?.AddressList?.Count > 0)
            {
                //Create the object of userAddress and store the shipping/billing address of user
                List<ZnodeAddress> userAddress = new List<ZnodeAddress>();

                if (IsNotNull(userAddressModel))
                {
                    // update Shipping address only.
                    if (userAddressModel.ShippingAddress?.AddressId > 0)
                        userAddress = UpdateUserAddress(userAddressModel.ShippingAddress, addressList, userAddress);

                    //Update shipping address only.
                    if (userAddressModel.BillingAddress?.AddressId > 0 && !Convert.ToBoolean(userAddressModel?.UseSameAsBillingAddress))
                        userAddress = UpdateUserAddress(userAddressModel.BillingAddress, addressList, userAddress);

                    //insert shipping or billing address.
                    if (userAddressModel.ShippingAddress?.AddressId == 0 || userAddressModel?.BillingAddress?.AddressId == 0)
                        userAddress = InserUserAddress(userAddressModel, addressList);
                }
                return userAddress;
            }
            else

                return new List<ZnodeAddress>();
        }

        //Insert new address for user.
        public virtual List<ZnodeAddress> InserUserAddress(UserAddressModel userAddressModel, AddressListModel addressList)
        {
            List<ZnodeAddress> userAddress = _addressRepository.Insert(addressList.AddressList.Where(w => w.AddressId == 0).ToEntity<ZnodeAddress>().ToList())?.ToList();
            //Set mapping of User and its address.
            InsertUserAddressMapping(userAddress, userAddressModel);
            ZnodeLogging.LogMessage(Admin_Resources.SuccessNewAddressCreate, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return userAddress;
        }

        //Update address for user.
        public virtual List<ZnodeAddress> UpdateUserAddress(AddressModel addressModel, AddressListModel addressList, List<ZnodeAddress> userAddress)
        {
            // update selected address to address table.
            bool status = false;
            if (addressList.AddressList.Any(w => w.AddressId == addressModel.AddressId))
                status = _addressRepository.Update(addressModel.ToEntity<ZnodeAddress>());
            userAddress.Add(addressModel.ToEntity<ZnodeAddress>());
            ZnodeLogging.LogMessage(status ? Admin_Resources.SuccessUpdateAccountAddress : Admin_Resources.ErrorUpdateAccountAddress, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return userAddress;
        }

        //Set mapping of User and its address.
        public virtual void InsertUserAddressMapping(List<ZnodeAddress> userAddress, UserAddressModel user)
        {
            List<ZnodeUserAddress> userAddressList = new List<ZnodeUserAddress>();

            //Get newly created shipping/billing address of user.
            if (IsNotNull(userAddress))
            {
                foreach (ZnodeAddress item in userAddress)
                    userAddressList.Add(new ZnodeUserAddress { UserId = user.UserId, AddressId = item.AddressId });
            }

            IZnodeRepository<ZnodeUserAddress> _userAddressRepository = new ZnodeRepository<ZnodeUserAddress>();
            userAddressList = _userAddressRepository.Insert(userAddressList)?.ToList();
            ZnodeLogging.LogMessage(Admin_Resources.SuccessNewAddressCreate, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        // To set user details from model with the login or anonymous users
        public virtual UserAddressModel SetUserDetails(ShoppingCartModel model)
        {
            if (IsNull(model.UserDetails))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.UserModelNotNull);

            UserAddressModel userAddress = model.UserDetails.ToModel<UserAddressModel, UserModel>();

            //Set billing address in user details.
            if (model.Payment?.BillingAddress?.AddressId > 0)
                userAddress.BillingAddress = model.Payment?.BillingAddress;
            else if (model.BillingAddress?.AddressId > 0)
                userAddress.BillingAddress = model.BillingAddress;
            else
                userAddress.BillingAddress = model.Payment?.BillingAddress ?? model.BillingAddress;

            userAddress.ShippingAddress = model?.ShippingAddress;

            if (IsNotNull(model?.Payment?.ShippingAddress))
                model.Payment.ShippingAddress.StateCode = model.ShippingAddress.StateCode;

            ZnodeLogging.LogMessage("BillingAddressId and ShippingAddressId of UserAddressModel: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { BillingAddressId = userAddress?.BillingAddress?.AddressId, ShippingAddressId = userAddress?.ShippingAddress?.AddressId });
            return userAddress;
        }

        //to set ShoppingCart to ZNodeCheckout
        public virtual IZnodeCheckout SetCheckoutData(UserAddressModel userAddress, ShoppingCartModel model, ZnodeLogging log)
        {

            var znodeShoppingCart = _shoppingCartMap.ToZnodeShoppingCart(model, userAddress);

            // Create the checkout object

            IZnodeCheckout checkout = CheckoutMap.ToZnodeCheckout(userAddress, znodeShoppingCart);
            if (IsNull(checkout?.ShoppingCart))
            {
                log.LogActivityTimerEnd((int)ZnodeLogging.ErrorNum.OrderSubmissionFailed, null);
                throw new ZnodeException(ErrorCodes.ProcessingFailed, Admin_Resources.UnableToProcessOrder);
            }

            return SetShoppingCartDataToCheckout(checkout, model);
        }

        //to Validate Checkout object
        public virtual void ValidateCheckout(IZnodeCheckout checkout)
        {
            if (IsNotNull(checkout))
            {
                if (IsNull(checkout?.ShoppingCart))
                    throw new ZnodeException(ErrorCodes.InvalidData,Admin_Resources.ShoppingCartNotNull);

                if (checkout?.ShoppingCart?.ShoppingCartItems?.Count < 1)
                    throw new ZnodeException(ErrorCodes.InvalidData,Admin_Resources.ErrorShoppingCartEmpty);

                //Check profile has active payment options
                if (!ProfileHasPaymentOptions(checkout))
                    throw new ZnodeException(ErrorCodes.InvalidData,Admin_Resources.NoPaymentOptionsForProfile);

                if (IsNull(checkout.ShoppingCart.Payment))
                    throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorShoppingCartPaymentNull);
            }
        }

        //Return true if Profile Has Payment Options.
        public virtual bool ProfileHasPaymentOptions(IZnodeCheckout checkout)
        {
            if (IsNotNull(checkout?.UserAccount))
            {
                //Set filters for payment list.
                FilterCollection filters = new FilterCollection();
                filters.Add(Constants.FilterKeys.IsActive.ToString(), FilterOperators.In, "1");
                if (IsNotNull(checkout))
                {
                    filters.Add(ZnodeUserEnum.UserId.ToString(), FilterOperators.In, checkout.UserAccount.UserId.ToString());
                    filters.Add(ZnodePortalEnum.PortalId.ToString(), FilterOperators.In, checkout.PortalID.ToString());
                    filters.Add(ZnodeProfileEnum.ProfileId.ToString(), FilterOperators.In, checkout.UserAccount.ProfileId.ToString());
                }

                //Get payment list
                PaymentSettingListModel paymentList = _paymentSettingService.GetPaymentSettingList(null, filters, null, null);

                return paymentList?.PaymentSettings?.Count > 0;
            }
            return false;
        }

        // to do PostSubmitOrder
        public virtual void PostSubmitOrder(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl)
          // Remove all saved cart items.
          => GetService<IShoppingCartService>().RemoveSavedCartItems(order.UserID, checkout.ShoppingCart.CookieMappingId);

        //to generate order receipt
        public virtual string GetOrderReceipt(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, bool isUpdate, out bool isEnableBcc)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            foreach (OrderLineItemModel item in order.OrderLineItems)
            {
                if (item.PersonaliseValueList != null)
                    item.PersonaliseValueList.Remove("AllocatedLineItems");

                if (item.PersonaliseValuesDetail != null)
                    item.PersonaliseValuesDetail.RemoveAll(pv => pv.PersonalizeCode == "AllocatedLineItems");
            }

            IZnodeOrderReceipt receipt = GetOrderReceiptInstance(order, checkout.ShoppingCart);

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("OrderReceipt", (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
            if (HelperUtility.IsNotNull(emailTemplateMapperModel))
            {
                string receiptContent = ShowOrderAdditionalDetails(emailTemplateMapperModel.Descriptions, order.Custom1);
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderReceiptHtml(receiptContent));
            }
            isEnableBcc = false;

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return string.Empty;
        }

        //to generate order receipt
        public virtual string GetDownloadableProductOrderReceipt(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, DownloadableProductKeyListModel key, bool isUpdate, out bool isEnableBcc)
        {
            IZnodeOrderReceipt receipt = GetOrderReceiptInstance(order, checkout.ShoppingCart);

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ProductKeyOrderReceipt", (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetProductKeysOrderReceiptHtml(emailTemplateMapperModel.Descriptions, key));
            }
            isEnableBcc = false;
            return string.Empty;
        }

        //Get purchased product order Receipt html For Email.
        public virtual string GetHtmlVendorForEmail(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, string vendorCode, int localeId)
        {
            IZnodeOrderReceipt receipt = GetOrderReceiptInstance(order, checkout.ShoppingCart);
            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("VendorReceipt", (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
            if (IsNotNull(emailTemplateMapperModel))
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetVendorProductOrderReceiptHtml(emailTemplateMapperModel.Descriptions, vendorCode));

            return string.Empty;
        }

        //to generate shipping status receipt
        //Get Html Resend Receipt For Email.
        public virtual string GetShippingReceiptForEmail(OrderModel orderModel, out bool isEnableBcc)
        {
            IZnodeOrderReceipt receipt = GetOrderReceiptInstance(orderModel);
            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ShippingReceipt", (orderModel.PortalId > 0) ? orderModel.PortalId : PortalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderResendReceiptHtml(emailTemplateMapperModel.Descriptions));
            }
            isEnableBcc = false;
            return string.Empty;
        }
        public virtual string GetCancelledOrderReceiptForEmail(OrderModel orderModel, out bool isEnableBcc)
        {
            IZnodeOrderReceipt receipt = GetOrderReceiptInstance(orderModel);
            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("CancelledOrderReceipt", (orderModel.PortalId > 0) ? orderModel.PortalId : PortalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderResendReceiptHtml(emailTemplateMapperModel.Descriptions));
            }
            isEnableBcc = false;
            return string.Empty;
        }
        //to send order receipt to customer
        public virtual bool SendOrderReceipt(string userEmailId, string subject, string senderEmail, string bccEmailId, string receiptHtml)
        {
            bool isSuccess = false;

            //This method is used to send an email.
            ZnodeEmail.SendEmail(userEmailId, senderEmail, bccEmailId, subject, receiptHtml, true);
            isSuccess = true;
            return isSuccess;
        }

        //to send order receipt to customer
        public virtual bool SendOrderReceipt(int portalId, string userEmailId, string subject, string senderEmail, string bccEmailId, string receiptHtml, bool isEnableBcc = false)
        {
            bool isSuccess = false;

            //This method is used to send an email.          
            ZnodeEmail.SendEmail(portalId, userEmailId, senderEmail, ZnodeEmail.GetBccEmail(isEnableBcc, portalId, bccEmailId), subject, receiptHtml, true);
            isSuccess = true;
            return isSuccess;
        }

        //Get Refund types from database and map into dictonary
        public virtual Dictionary<string, int> GetRefundPaymentType()
        {
            List<ZnodeOmsRefundType> refundPaymentTypes = new ZnodeRepository<ZnodeOmsRefundType>().Table.ToList();
            ZnodeLogging.LogMessage("refundPaymentTypes count.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, new { refundPaymentTypesCount = refundPaymentTypes?.Count});

            Dictionary<string, int> refundTypeDictonary = new Dictionary<string, int>();

            //Map Refundtype into dictonary
            foreach (ZnodeOmsRefundType refundtype in refundPaymentTypes)
                refundTypeDictonary.Add(refundtype.RefundType.ToUpper(), refundtype.OmsRefundTypeId);

            return refundTypeDictonary;
        }

        //Map Refund Total order Details
        public virtual RefundPaymentModel MapRefundTotalDetails(int OmsRefundTypeId, OrderItemsRefundModel orderItemsRefundModel, ZnodeOmsOrderDetail orderDetails)
        {
            return new RefundPaymentModel
            {
                OmsOrderDetailsId = orderDetails.OmsOrderDetailsId,
                OmsRefundTypeId = OmsRefundTypeId,
                RefundableAmountLeft = (orderItemsRefundModel.RefundOrderLineitems.Sum(x => x.RefundableAmountLeft)
                                       + (orderItemsRefundModel?.ShippingRefundDetails?.RefundableAmountLeft ?? 0.00m))
                                       - (_omsPaymentRefundRepository.Table.Where(x => x.OmsOrderDetailsId == orderDetails.OmsOrderDetailsId
                                       && x.OmsRefundTypeId == OmsRefundTypeId)?.ToList()?.Sum(x => x == null ? 0.00m : x.RefundAmount) ?? 0.00m),
                TotalAmount = Convert.ToDecimal(orderDetails.Total),
                RefundType = ZnodeConstant.TotalRefund,
            };
        }

        //Map Shipping Refund  Details
        public virtual RefundPaymentModel MapRefundShippingDetails(int OmsRefundTypeId, ZnodeOmsOrderDetail orderDetails)
         => new RefundPaymentModel
         {
             OmsOrderDetailsId = orderDetails.OmsOrderDetailsId,
             OmsRefundTypeId = OmsRefundTypeId,
             RefundableAmountLeft = (orderDetails.ShippingCost ?? 0.00m) - (_omsPaymentRefundRepository.Table.Where(x => x.OmsOrderDetailsId == orderDetails.OmsOrderDetailsId && x.OmsRefundTypeId == OmsRefundTypeId)?.ToList()?.Sum(x => x == null ? 0.00m : x.RefundAmount) ?? 0.00m),
             TotalAmount = Convert.ToDecimal(orderDetails.ShippingCost),
             RefundType = ZnodeConstant.ShippingRefund,
         };

        // Map ZnodeOmsOrderDetail to OrderItemsRefundModel
        public virtual OrderItemsRefundModel MapToOrderItemsRefundModel(ZnodeOmsOrderDetail orderDetail)
        => new OrderItemsRefundModel
        {
            TransactionId = orderDetail.PaymentTransactionToken,
            OmsOrderDetailsId = orderDetail.OmsOrderDetailsId,
            OmsOrderId = orderDetail.OmsOrderId,
        };

        //Get Order Line Items with Refund Details
        private List<RefundPaymentModel> GetRefundOrderLineItems(int orderDetailsId, int OmsRefundTypeId)
        //Join OrderLineItemRepository and OmsPaymentRefundRepository and get RefundableAmountLeft
        => (from orderlineItems in _orderLineItemRepository.Table
            join OmsPaymentRefund in _omsPaymentRefundRepository.Table on orderlineItems.OmsOrderLineItemsId equals OmsPaymentRefund.OmsOrderLineItemsId into j1
            from j2 in j1.DefaultIfEmpty()
            where orderlineItems.OmsOrderDetailsId == orderDetailsId
            group j2 by new
            {
                OmsOrderDetailsId = orderlineItems.OmsOrderDetailsId,
                OmsOrderLineItemsId = orderlineItems.OmsOrderLineItemsId,
                ProductName = orderlineItems.ProductName,
                Unitprice = orderlineItems.Price,
                DiscountAmount = orderlineItems.DiscountAmount ?? 0.00m,
                Quantity = orderlineItems.Quantity ?? 0.00m,
            }
                         into grouped
            select new RefundPaymentModel
            {
                OmsRefundTypeId = OmsRefundTypeId,
                RefundType = ZnodeConstant.PartialRefund,
                ProductName = grouped.Key.ProductName,
                OmsOrderDetailsId = grouped.Key.OmsOrderDetailsId,
                OmsOrderLineItemsId = grouped.Key.OmsOrderLineItemsId,
                RefundableAmountLeft = ((grouped.Key.Unitprice * grouped.Key.Quantity) - (grouped.Key.DiscountAmount + grouped.Sum(d => d == null ? 0 : d.RefundAmount)))
            })?.ToList();

        #region Update order status

        //Send the order status email to customer.
        public virtual string SendShippingStatusEmailReceipt(OrderModel model, string receiptHtml)
        {
            if (string.IsNullOrEmpty(receiptHtml))
                return receiptHtml;

            //order to bind shipping details in data tabel
            DataTable shippingStatusTable = SetShippingStatusData(model);
            ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(receiptHtml);

            // Parse order table
            receiptHelper.Parse(shippingStatusTable.CreateDataReader());

            // Return the HTML output
            return receiptHelper.Output;
        }

        //to set order details
        public virtual DataTable SetShippingStatusData(OrderModel Order)
        {
            // Create new row
            DataTable orderTable = CreateShippingTable();
            DataRow orderRow = orderTable.NewRow();

            // Additional info
            orderRow["BillingFirstName"] = Order.BillingAddress.FirstName;
            orderRow["BillingLastName"] = Order.BillingAddress.LastName;

            if (!string.IsNullOrEmpty(Order.TrackingNumber))
            {
                orderRow["TrackingMessage"] = Equals(Order.OrderState, ZnodeOrderStatusEnum.SHIPPED.ToString()) ? Admin_Resources.ShippingTrackingNoMessage + SetTrackingUrl(Order.TrackingNumber, Order.TrackingUrl) : string.Empty;
                orderRow["Message"] = string.Format(Admin_Resources.ShippingStatusMessage, Order.OrderState.ToLower()) + (Equals(Order.OrderState, ZnodeOrderStatusEnum.SHIPPED.ToString()) ? Admin_Resources.TrackingPackageMessage : string.Empty);
            }
            else
            {
                orderRow["TrackingMessage"] = string.Empty;
                orderRow["Message"] = string.Format(Admin_Resources.ShippingStatusMessage, Order.OrderState.ToLower());
            }

            // Add rows to order table
            orderTable.Rows.Add(orderRow);
            return orderTable;
        }

        public virtual DataTable CreateShippingTable()
        {
            DataTable shippingTable = new DataTable();
            // Additional info
            shippingTable.Columns.Add("BillingFirstName");
            shippingTable.Columns.Add("BillingLastName");
            shippingTable.Columns.Add("TrackingMessage");
            shippingTable.Columns.Add("Message");

            return shippingTable;
        }

        #endregion Update order status private

        //Get customer name by userId.
        public virtual void GetCustomerName(int? userId, OrdersListModel orderListModel)
        {
            if (IsNotNull(userId))
            {
                ZnodeLogging.LogMessage("UserId to get customer name", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { userId = userId });
                IZnodeRepository<ZnodeUser> _userRepository = new ZnodeRepository<ZnodeUser>();
                orderListModel.CustomerName = _userRepository.Table.Where(x => x.UserId == userId).Select(x => x.FirstName + " " + x.LastName)?.FirstOrDefault();
            }
        }

        //Get Order Note Details.
        public virtual List<OrderNotesModel> GetOrderNoteDetails(int omsOrderId, int omsQuoteId)
        {
            ZnodeLogging.LogMessage("Input parameters : ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { omsOrderId = omsOrderId, omsQuoteId = omsQuoteId });

            IZnodeRepository<View_GetOmsOrderNotes> _viewOmsOrderNoteList = new ZnodeRepository<View_GetOmsOrderNotes>();
            FilterCollection filters = new FilterCollection();
            if (omsOrderId > 0)
                filters.Add(new FilterTuple(ZnodeOmsOrderDetailEnum.OmsOrderId.ToString(), FilterOperators.Equals, omsOrderId.ToString()));

            if (omsQuoteId > 0)
                filters.Add(new FilterTuple(ZnodeOmsQuoteEnum.OmsQuoteId.ToString(), FilterOperators.Equals, omsQuoteId.ToString()));

            if (filters.Count > 0)
            {
                EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                ZnodeLogging.LogMessage("Where clause in GetOrderNoteDetails method get data", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, whereClauseModel.WhereClause);
                return _viewOmsOrderNoteList.GetEntityList(whereClauseModel.WhereClause)?.ToList().ToModel<OrderNotesModel>()?.ToList();
            }
            return new List<OrderNotesModel>();
        }

        //Create Single Order Line Item if order is having group product.
        public virtual List<OrderLineItemModel> CreateSingleOrderLineItem(OrderModel orderModel, bool isResendEmail = false)
        {
            if (orderModel?.OrderLineItems.Count > 0 && isResendEmail)
                orderModel.OrderLineItems = orderModel.OrderLineItems.Where(q => q.OrderLineItemStateId != 30).ToList();

            return orderHelper.FormalizeOrderLineItems(orderModel);
        }

        //For Getting personalize attribute.       
        public virtual string GetPersonaliseAttributes(Dictionary<string, object> personaliseValueList)
        {
            personaliseValueList.Remove("AllocatedLineItems");
            string personaliseAttibuteHtml = string.Empty;
            if (IsNotNull(personaliseValueList))
            {
                foreach (var personaliseAttibute in personaliseValueList)
                    personaliseAttibuteHtml += $"{"<p>"} { personaliseAttibute.Key}{" : "}{personaliseAttibute.Value}{"</p>"}";

                return personaliseAttibuteHtml;
            }
            return string.Empty;
        }

        // To set order discount amount
        public virtual void SetOrderDiscount(OrderModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            List<ZnodeOmsOrderDiscount> discountList = orderHelper.GetOrderDiscountAmount(model.OmsOrderDetailsId);
            if (discountList?.Count > 0)
            {
                decimal csrDiscount = (discountList?.FirstOrDefault(x => x.OmsDiscountTypeId == (int)OrderDiscountTypeEnum.CSRDISCOUNT)?.DiscountAmount).GetValueOrDefault();
                if (csrDiscount > 0)
                {
                    model.CSRDiscountAmount = csrDiscount;
                    model.DiscountAmount = (model.DiscountAmount - csrDiscount);
                }

                decimal giftCardDiscount = (discountList?.FirstOrDefault(x => x.OmsDiscountTypeId == (int)OrderDiscountTypeEnum.GIFTCARD)?.DiscountAmount).GetValueOrDefault();
                if (giftCardDiscount > 0)
                    model.GiftCardAmount = giftCardDiscount;

                decimal couponDiscount = (discountList?.FirstOrDefault(x => x.OmsDiscountTypeId == (int)OrderDiscountTypeEnum.COUPONCODE)?.DiscountAmount).GetValueOrDefault();
                if (couponDiscount > 0)
                    model.DiscountAmount = couponDiscount;

            }
        }

        //Save in Quote.
        public virtual void SaveInQuote(ShoppingCartModel model, ZnodeOrderFulfillment order)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            //If the order is from quote.
            ZnodeLogging.LogMessage("Order ID while saving order as a quote:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, order?.OrderID);
            //
            if (order.OrderID > 0 && model.IsQuoteOrder && model.OmsQuoteId > 0)
            {
                ZnodeOmsOrder createdOrder = new ZnodeRepository<ZnodeOmsOrder>().Table.Where(x => x.OmsOrderId == order.OrderID).FirstOrDefault();
                ZnodeLogging.LogMessage("Created order in SaveInQuote method with OmsOrderId", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose,createdOrder?.OmsOrderId);

                if (IsNotNull(createdOrder))
                {
                    //Set IsQuoteOrder to true.
                    createdOrder.IsQuoteOrder = true;
                    createdOrder.OMSQuoteId = model.OmsQuoteId;
                    ZnodeLogging.LogMessage(_omsOrderRepository.Update(createdOrder) ? Admin_Resources.SuccessOrderUpdate : Admin_Resources.ErrorOrderUpdate, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

                    //Convert quote addtional notes to order notes.
                    ToOrderNotes(model, order);
                    order.Order.IsQuoteOrder = true;
                    ZnodeOmsQuote quote = new ZnodeRepository<ZnodeOmsQuote>().Table.Where(x => x.OmsQuoteId == model.OmsQuoteId).FirstOrDefault();
                    if (IsNotNull(quote))
                    {
                        //Set IsQuoteOrder to true.
                        quote.IsConvertedToOrder = true;
                        ZnodeLogging.LogMessage(_znodeOmsQuote.Update(quote) ? Admin_Resources.SuccessQuotePendingOrderUpdate : Admin_Resources.ErrorQuotePendingOrderUpdate, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                    }
                }
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //to set shipping state code
        public virtual void SetStateCode(ShoppingCartModel model)
        {
            IShoppingCartService _shoppingCartService = GetService<IShoppingCartService>();
            _shoppingCartService.SetShippingStateCode(model);
        }

        //Convert quote additional notes to order notes.
        public virtual void ToOrderNotes(ShoppingCartModel model, ZnodeOrderFulfillment order)
        {
            //Get oms additional notes for given quote id.
            List<ZnodeOmsNote> quoteNotes = _omsNoteRepository.Table.Where(x => x.OmsQuoteId == model.OmsQuoteId)?.ToList();
            ZnodeLogging.LogMessage("Quote notes list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose,quoteNotes?.Count());
            if (quoteNotes?.Count() > 0)
            {
                //Convert quote additional notes to order additional notes.
                foreach (ZnodeOmsNote note in quoteNotes)
                {
                    note.OmsQuoteId = null;
                    note.OmsOrderDetailsId = order?.Order?.OmsOrderDetailsId;
                    _omsNoteRepository.Update(note);

                }
            }
        }

        //Send purchased product details Email To Vendor.
        public virtual void SendEmailToVendor(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, bool isEnableBcc)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            //Get order line item which having vendors.
            List<OrderLineItemModel> vendorOrderLineItems = order.OrderLineItems?.Where(w => w.Vendor != string.Empty && w.Vendor != null).ToList();
            ZnodeLogging.LogMessage("Vendor orderLineItems list count:",ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose,vendorOrderLineItems?.Count());
            if (vendorOrderLineItems?.Count > 0)
            {
                IZnodeRepository<ZnodePimVendor> _pimVendorRepository = new ZnodeRepository<ZnodePimVendor>();

                //Get distinct vendor codes from order line item.
                List<string> distinctVendorCodes = vendorOrderLineItems.Select(x => x.Vendor).Distinct().ToList();
                ZnodeLogging.LogMessage("distinctVendorCodes count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, distinctVendorCodes?.Count());

                //Get Email id and Vendor code together by using vendor code.
                var vendorDetails = _pimVendorRepository.Table.Where(x => distinctVendorCodes.Contains(x.VendorCode) && x.IsActive == true)?.Select(s => new { Email = s.Email, Code = s.VendorCode }).ToList();

                if (IsNotNull(vendorDetails))
                {
                    //Send purchased product details to respective vendor.
                    foreach (var vendorInfo in vendorDetails)
                    {
                        //Create purchased product receipt HTML to the order.
                        string VendorReceiptHtml = GetHtmlVendorForEmail(order, checkout, feedbackUrl, vendorInfo.Code, localeId);

                        //Send order receipt to respective vendor.
                        if (!string.IsNullOrEmpty(VendorReceiptHtml))
                            SendOrderReceipt(order.PortalId, vendorInfo.Email, Admin_Resources.TitleOrderReceipt, ZnodeConfigManager.SiteConfig.AdminEmail, string.Empty, VendorReceiptHtml, isEnableBcc);
                    }
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //to send line item state change email to user
        public virtual void SendLineItemStateChangeEmail(ZnodeOrderFulfillment order, bool isEnableBcc)
        {
            if (IsNotNull(order?.Order) && !string.IsNullOrEmpty(order?.Order.SendEmailLineItemIds))
            {
                int orderId = order.OrderID;
                ZnodeLogging.LogMessage("orderId :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, orderId);
                string lineItemId = order.Order.SendEmailLineItemIds;
                ResendOrderLineItemConfirmationEmail(orderId, lineItemId, new NameValueCollection(), isEnableBcc);
            }
        }

        //Replace sort key name
        public virtual void ReplaceSortKeys(ref NameValueCollection sorts)
        {
            foreach (string key in sorts.Keys)
            {
                if (string.Equals(key, Constants.FilterKeys.OrderTotalWithCurrency, StringComparison.OrdinalIgnoreCase)) { ReplaceSortKeyName(ref sorts, Constants.FilterKeys.OrderTotalWithCurrency.ToLower(), Constants.FilterKeys.Total); }
                if (string.Equals(key, Constants.FilterKeys.OrderDateWithTime, StringComparison.OrdinalIgnoreCase)) { ReplaceSortKeyName(ref sorts, Constants.FilterKeys.OrderDateWithTime.ToLower(), Constants.FilterKeys.OrderDate); }
            }
        }

        protected virtual void ReplaceFilterKeys(ref FilterCollection filters)
        {
            foreach (FilterTuple tuple in filters)
            {
                if (tuple.Item1 == Constants.FilterKeys.OrderDateWithTime) { ReplaceFilterKeyName(ref filters, Constants.FilterKeys.OrderDateWithTime, Constants.FilterKeys.OrderDate); }
            }
        }

        //Get user id from filters.
        public virtual void GetUserIdFromFilters(FilterCollection filters, ref int userId)
        {
            userId = Convert.ToInt32(filters?.Find(x => string.Equals(x.FilterName, ZnodeUserEnum.UserId.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Item3);
            filters?.RemoveAll(x => string.Equals(x.FilterName, ZnodeUserEnum.UserId.ToString(), StringComparison.CurrentCultureIgnoreCase));
        }

        //to check order is allowed to edit
        public virtual bool IsOrderEditable(OrderModel model)
        => _omsOrderStateRepository.GetById(model.OmsOrderStateId).IsEdit;

        //to update order state
        public virtual bool UpdateOrderState(OrderModel model)
        {
            OrderStateParameterModel orderStateModel = new OrderStateParameterModel();
            orderStateModel.OmsOrderId = model.OmsOrderId;
            orderStateModel.OmsOrderDetailsId = model.OmsOrderDetailsId;
            orderStateModel.TrackingNumber = model.TrackingNumber;
            orderStateModel.OmsOrderStateId = model.OmsOrderStateId;
            orderStateModel.CreatedBy = model.CreatedBy;
            orderStateModel.ModifiedBy = model.ModifiedBy;
            return UpdateOrderStatus(orderStateModel);
        }

        //to update order state
        public virtual bool UpdateReturnedOrderState(OrderModel model)
        {
            OrderStateParameterModel orderStateModel = new OrderStateParameterModel();
            orderStateModel.OmsOrderId = model.OmsOrderId;
            orderStateModel.OmsOrderDetailsId = model.OmsOrderDetailsId;
            orderStateModel.TrackingNumber = model.TrackingNumber;
            orderStateModel.OmsOrderStateId = Convert.ToInt32(_orderStateRepository.Table.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.CANCELLED.ToString())?.OmsOrderStateId); ;
            orderStateModel.CreatedBy = model.CreatedBy;
            orderStateModel.ModifiedBy = model.ModifiedBy;
            return UpdateReturnedOrderStatus(orderStateModel);
        }

        //to update order status.
        public virtual bool UpdateOrderTrackingNumber(OrderModel model)
        {

            ZnodeOmsOrderDetail order = _orderDetailsRepository.Table.Where(w => w.OmsOrderId == model.OmsOrderId && w.IsActive).FirstOrDefault();
            ZnodeLogging.LogMessage("OmsOrderDetailsId for ZnodeOmsOrderDetail model :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, order?.OmsOrderDetailsId);
            if (IsNotNull(order))
            {
                order.TrackingNumber = model.TrackingNumber;
                order.ModifiedDate = GetDateTime();
                order.CreatedBy = model.CreatedBy;
                order.ModifiedBy = model.ModifiedBy;
                return _orderDetailsRepository.Update(order);
            }

            return false;
        }

        //to update order status.
        private void UpdatePurchaseOrderNumber(OrderModel model)
        {
            ZnodeOmsOrderDetail order = _orderDetailsRepository.Table.Where(w => w.OmsOrderId == model.OmsOrderId && w.IsActive).FirstOrDefault();
            if (IsNotNull(order))
            {
                order.PurchaseOrderNumber = model.PurchaseOrderNumber;
                order.ModifiedDate = GetDateTime();
                order.CreatedBy = model.CreatedBy;
                order.ModifiedBy = model.ModifiedBy;
                _orderDetailsRepository.Update(order);
            }
        }

        // Update Order Shipping Billing Address
        public virtual bool UpdateOrderAddress(AddressModel model)
        {
            bool updated = false;

            ZnodeOmsOrderDetail order = _orderDetailsRepository.Table.Where(w => w.OmsOrderId == model.omsOrderId && w.IsActive).FirstOrDefault();
            ZnodeOmsOrderShipment shipment = _orderShipmentRepository.Table.Where(w => w.OmsOrderShipmentId == model.omsOrderShipmentId).FirstOrDefault();

            if (IsNotNull(order) && model.FromBillingShipping == "billing")
            {
                BillingMapping(order, model);
                updated = _orderDetailsRepository.Update(order);
            }
            if (IsNotNull(shipment) && model.FromBillingShipping == "shipping")
            {
                ShippingMapping(shipment, model);
                updated = _orderShipmentRepository.Update(shipment);
            }
            return updated;
        }

        //to check update history for provided key
        public virtual bool ExistUpdateHistory(string key, OrderModel model)
            => model?.OrderHistory?.Keys?.Contains(key) ?? false;

        //to check get history for provided key
        public virtual string GetHistoryMessageByKey(string key, OrderModel model)
        {
            string val = string.Empty;
            model.OrderHistory.TryGetValue(key, out val);

            switch (key)
            {
                case ZnodeConstant.OrderTax:
                    return string.Format(Admin_Resources.OrderTaxExempted, val);

                case ZnodeConstant.OrderBillingAddress:
                    return $" {val}";

                case ZnodeConstant.OrderShippingAddress:
                    return $" {val}";

                case ZnodeConstant.OrderShippingCost:
                    return string.Format(Admin_Resources.OrderHistoryShippingAmount, model.OrderOldValue.ShippingAmount, val);

                case ZnodeConstant.OrderShippingType:
                    return string.Format(Admin_Resources.OrderHistoryShippingType, _shippingRepository.Table.Where(w => w.ShippingId == model.OrderOldValue.ShippingId)?.Select(s => s.Description)?.FirstOrDefault(), val);

                case ZnodeConstant.OrderCSRDiscount:
                    return string.Format(Admin_Resources.CSRDiscountApplied, val);

                case ZnodeConstant.OrderGiftCard:
                    {
                        decimal giftcardAmount = model?.ShoppingCartModel?.GiftCardAmount ?? 0;
                        string formatedAmount = ZnodeCurrencyManager.FormatPriceWithCurrency(giftcardAmount, model.CultureCode, string.Empty);
                        return string.Format(Admin_Resources.GiftCardHistory, val, formatedAmount);
                    }

                case ZnodeConstant.OrderCoupon:
                    return string.Format(Admin_Resources.CouponCodeApplied, val);

                case ZnodeConstant.OrderUpdatedStatus:
                    return string.Format(Admin_Resources.OrderHistoryUpdatedStatus, model.OrderOldValue.OrderState, val);

                default:
                    return string.Format(Admin_Resources.OrderHistory, key, val);
            }
        }

        //to save order and payment state
        public virtual void SaveHistoryAndUpdateOrderState(OrderModel orderModel, bool updateExistingOrder = false)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            string orderHistory = string.Empty;
            int notesId = 0;
            //to check payment status is updated then update in existing order
            if (ExistUpdateHistory(ZnodeConstant.OrderPaymentState, orderModel))
            {
                UpdateOrderPaymentStatus(orderModel.OmsOrderId, orderModel.PaymentStatus, orderModel.OmsPaymentStateId, orderModel.CreatedBy, orderModel.ModifiedBy);
                orderHistory = GetConsolidatedHistoryMessage(GetHistoryMessageByKey(ZnodeConstant.OrderPaymentState, orderModel), orderHistory);
            }

            //to check payment status is updated then update in existing order
            if (ExistUpdateHistory(ZnodeConstant.OrderBillingAddress, orderModel))
            {
                UpdateBillingAddress(orderModel.OmsOrderId, orderModel.BillingAddress);
                orderHistory = GetConsolidatedHistoryMessage(GetHistoryMessageByKey(ZnodeConstant.OrderBillingAddress, orderModel), orderHistory);
            }

            //to check order status is updated then update in existing order
            if (ExistUpdateHistory(ZnodeConstant.OrderTrackingNumber, orderModel))
            {
                UpdateOrderTrackingNumber(orderModel);
                orderHistory = GetConsolidatedHistoryMessage(GetHistoryMessageByKey(ZnodeConstant.OrderTrackingNumber, orderModel), orderHistory);
            }

            //to check order status is updated then update in existing order
            if (ExistUpdateHistory("PurchaseOrderNumber", orderModel))
            {
                UpdatePurchaseOrderNumber(orderModel);
                orderHistory = GetConsolidatedHistoryMessage(GetHistoryMessageByKey("PurchaseOrderNumber", orderModel), orderHistory);
            }

            if (!string.IsNullOrEmpty(orderModel.AdditionalInstructions))
            {
                OrderNotesModel notesModel = new OrderNotesModel() { Notes = orderModel.AdditionalInstructions, OmsOrderDetailsId = orderModel.OmsOrderDetailsId, CreatedBy = orderModel.CreatedBy, ModifiedBy = orderModel.ModifiedBy };
                AddOrderNote(notesModel);
                notesId = notesModel.OmsNotesId;
            }

            UpdateExtenalId(orderModel);

            //to check order status is updated then update in existing order
            if (ExistUpdateHistory(ZnodeConstant.OrderUpdatedStatus, orderModel))
            {
                bool isOrderStateUpdated = UpdateOrderState(orderModel);

                if (isOrderStateUpdated && orderModel.OmsOrderStateId == _omsOrderStateRepository.Table.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.CANCELLED.ToString())?.OmsOrderStateId)
                    RevertOrderInventory(orderModel.OmsOrderId, orderModel.UserId);

                orderHistory = GetConsolidatedHistoryMessage(GetHistoryMessageByKey(ZnodeConstant.OrderUpdatedStatus, orderModel), orderHistory);
                //if update Existing Order then no need to save other history of order.
                if (updateExistingOrder)
                {
                    if (IsNotNull(orderModel.OmsOrderStateId))
                        orderModel.OrderState = _omsOrderStateRepository.GetById(orderModel.OmsOrderStateId).OrderStateName;

                    CreateOrderHistory(new OrderHistoryModel { OmsOrderDetailsId = orderModel.OmsOrderDetailsId, Message = orderHistory, OmsNotesId = notesId, CreatedBy = orderModel.CreatedBy, ModifiedBy = orderModel.ModifiedBy });
                    return;
                }
            }
            if (ExistUpdateHistory(ZnodeConstant.OrderReturnAllAndCancelStatus, orderModel))
            {
                bool isOrderStateUpdated = UpdateReturnedOrderState(orderModel);
                if (isOrderStateUpdated && orderModel.OrderHistory.ContainsKey(ZnodeConstant.OrderReturnAllAndCancelStatus)) orderModel.OrderHistory.Remove(ZnodeConstant.OrderReturnAllAndCancelStatus);
            }

            if (IsNotNull(orderModel?.OrderHistory))
            {
                foreach (var history in orderModel?.OrderHistory)
                {
                    if (!string.IsNullOrEmpty(history.Key) &&
                        history.Key != ZnodeConstant.OrderPaymentState &&
                        history.Key != ZnodeConstant.OrderTrackingNumber &&
                        history.Key != ZnodeConstant.OrderUpdatedStatus &&
                        history.Key != ZnodeConstant.OrderBillingAddress
                        && history.Key != "PurchaseOrderNumber")
                    {
                        orderHistory = GetConsolidatedHistoryMessage(GetHistoryMessageByKey(history.Key, orderModel), orderHistory);
                    }
                }
            }

            decimal orderAmount = orderModel.OverDueAmount;

            if (HelperUtility.IsNotNull(orderModel?.OrderLineItemHistory) && orderModel?.OrderLineItemHistory.Count > 0)
            {
                foreach (var item in orderModel?.OrderLineItemHistory)
                {
                    orderHistory = GenerateOrderLineItemHistory(orderHistory, item.Key, item.Value, orderModel.OrderOldValue.OrderLineItems, orderModel.CultureCode);

                    orderAmount = BindOrderAmount(orderAmount, item);
                }
            }

            if (!string.IsNullOrEmpty(orderHistory))
                CreateOrderHistory(new OrderHistoryModel { OmsOrderDetailsId = orderModel.OmsOrderDetailsId, Message = orderHistory, OmsNotesId = notesId, OrderAmount = orderAmount, CreatedBy = orderModel.CreatedBy, ModifiedBy = orderModel.ModifiedBy });
        }

        //Bind order amount 
        protected virtual decimal BindOrderAmount(decimal orderAmount, KeyValuePair<string, OrderLineItemHistoryModel> item)
        {
            //if return shipping amount contain value
            if (string.IsNullOrEmpty(item.Value.ReturnShippingAmount))
                orderAmount = string.Equals(item.Value.OrderUpdatedStatus, ZnodeOrderStatusEnum.RETURNED.ToString(), StringComparison.InvariantCultureIgnoreCase)
                    ? (item.Value.SubTotal + item.Value.TaxCost)
                    : orderAmount;
            else
                orderAmount = string.Equals(item.Value.OrderUpdatedStatus, ZnodeOrderStatusEnum.RETURNED.ToString(), StringComparison.InvariantCultureIgnoreCase)
                    ? (item.Value.SubTotal + item.Value.TaxCost + Convert.ToDecimal(item.Value.ReturnShippingAmount))
                    : orderAmount;
            return orderAmount;
        }

        //to save order line item changes to history table
        public virtual string GenerateOrderLineItemHistory(string history, string sku, OrderLineItemHistoryModel skuHistory, List<OrderLineItemModel> oldValue, string cultureCode)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNotNull(skuHistory))
            {
                if (oldValue?.Where(w => w.Sku == sku)?.Count() == 0)
                    sku = skuHistory.SKU;

                string oldQuantity = Convert.ToString(oldValue.Where(w => w.Sku == sku).Select(s => s.Quantity).FirstOrDefault());
                string oldStatus = Convert.ToString(oldValue.Where(w => w.Sku == sku).Select(s => s.OrderLineItemState).FirstOrDefault());
                string oldUnitPrice = ZnodeCurrencyManager.FormatPriceWithCurrency(oldValue.Where(w => w.Sku == sku).Select(s => s.Price).FirstOrDefault(), cultureCode, string.Empty);

                string productName = skuHistory.ProductName;
                string qty = Convert.ToString(skuHistory.Quantity);

                if (!string.IsNullOrEmpty(skuHistory.OrderLineAdd))
                    history = GetConsolidatedHistoryMessage(string.Format(Admin_Resources.LineItemAdded, skuHistory.SKU, skuHistory.OrderLineAdd, qty), history);

                if (!string.IsNullOrEmpty(skuHistory.OrderLineDelete))
                    history = GetConsolidatedHistoryMessage(string.Format(Admin_Resources.LineItemDeleted, skuHistory.SKU, skuHistory.OrderLineDelete, qty), history);

                if (!string.IsNullOrEmpty(skuHistory.OrderUpdatedStatus) && !(string.Equals(skuHistory.OrderUpdatedStatus, OrderDiscountTypeEnum.PARTIALREFUND.ToString(), StringComparison.OrdinalIgnoreCase)))
                    history = GetConsolidatedHistoryMessage(string.Format(Admin_Resources.LineItemStateChanged, !string.IsNullOrEmpty(skuHistory.SKU) ? skuHistory.SKU : sku, productName, qty, oldStatus, skuHistory.OrderUpdatedStatus), history);

                if (!string.IsNullOrEmpty(skuHistory.OrderLineQuantity) && !Equals(skuHistory.OrderLineQuantity, "0"))
                    history = GetConsolidatedHistoryMessage(string.Format(Admin_Resources.LineItemQuantityChanged, sku, productName, oldQuantity, skuHistory.OrderLineQuantity), history);

                if (!string.IsNullOrEmpty(skuHistory.OrderLineUnitPrice))
                    history = GetConsolidatedHistoryMessage(string.Format(Admin_Resources.LineItemPriceChanged, sku, productName, oldUnitPrice, skuHistory.OrderLineUnitPrice), history);

                if (!string.IsNullOrEmpty(skuHistory.OrderTrackingNumber))
                    history = GetConsolidatedHistoryMessage(string.Format(Admin_Resources.LineItemTrackingNumberChanged, sku, productName, qty, skuHistory.OrderTrackingNumber), history);

                if (!string.IsNullOrEmpty(skuHistory.ReturnShippingAmount))
                    history = GetConsolidatedHistoryMessage(string.Format(Admin_Resources.LineItemRetrunShipping, sku, productName, skuHistory.ReturnShippingAmount), history);

                if (!string.IsNullOrEmpty(skuHistory.PartialRefundAmount))
                    history = GetConsolidatedHistoryMessage(string.Format(Admin_Resources.LineItemPartialRefund, sku, productName, skuHistory.PartialRefundAmount), history);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return history;
        }

        //to check that do we need to update existing order data return true else return false (in case submit new order with same order number)
        public virtual bool IsExistingOrderUpdated(OrderModel model)
        {
            bool isExistingOrderUpdated = false;
            if (IsNotNull(model?.OrderHistory) || IsNotNull(model?.OrderLineItemHistory)
                && model.OrderHistory.Count > 0 || model.OrderLineItemHistory.Count > 0)
            {
                isExistingOrderUpdated = !IsOrderHistoryExceptPaymentAndOrderState(model);
            }
            return isExistingOrderUpdated;
        }

        //to check return shipping amount only
        public virtual bool IsReturnShipping(OrderModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            bool isreturnshipping = false;
            int historyCount = 0;
            if (IsNotNull(model?.OrderHistory) || IsNotNull(model?.OrderLineItemHistory)
                && model.OrderHistory.Count == 0 || model.OrderLineItemHistory.Count > 0)
            {
                foreach (var item in model?.OrderLineItemHistory)
                {
                    if (!string.IsNullOrEmpty(item.Value.ReturnShippingAmount) && (model.ReturnItemList?.ReturnItemList?.Count == 0 || item.Value.IsShippingReturn))
                    {
                        isreturnshipping = true;
                    }
                    else if (!string.IsNullOrEmpty(item.Value.OrderLineAdd) ||
                        !string.IsNullOrEmpty(item.Value.OrderLineDelete) ||
                        !string.IsNullOrEmpty(item.Value.OrderUpdatedStatus) ||
                        !(!string.IsNullOrEmpty(item.Value.OrderLineQuantity) || !Equals(item.Value.OrderLineQuantity, "0")) ||
                        !string.IsNullOrEmpty(item.Value.OrderLineUnitPrice) ||
                        !string.IsNullOrEmpty(item.Value.OrderTrackingNumber))
                    {
                        historyCount += 1;
                    }
                }

                if (isreturnshipping && historyCount == 0)
                    return true;
                else
                    return false;
            }
            return isreturnshipping;
        }

        //to check order data is updated
        public virtual bool IsOrderDataUpdated(OrderModel model)
        {
            bool isOrderDataUpdated = true;
            if (IsNotNull(model))
            {
                if (IsNull(model.OrderHistory) && IsNull(model.OrderLineItemHistory)
                || (model.OrderHistory.Count == 0 && model.OrderLineItemHistory.Count == 0))
                    isOrderDataUpdated = false;

                if (!string.IsNullOrEmpty(model.AdditionalInstructions))
                    isOrderDataUpdated = true;

                if (!string.IsNullOrEmpty(model.ExternalId))
                    isOrderDataUpdated = true;
            }
            return isOrderDataUpdated;
        }

        //to check order history except payment and order state exist return true/false
        public virtual bool IsOrderHistoryExceptPaymentAndOrderState(OrderModel model)
        {
            int count = 0;
            foreach (var history in model.OrderHistory)
            {
                if (history.Key != ZnodeConstant.OrderUpdatedStatus
                    && history.Key != ZnodeConstant.OrderPaymentState
                    && history.Key != ZnodeConstant.OrderTrackingNumber
                    && history.Key != ZnodeConstant.OrderBillingAddress
                    && history.Key != "PurchaseOrderNumber")
                {
                    count++;
                }

                if (history.Key == ZnodeConstant.OrderBillingAddress && Equals(model?.BillingAddress?.AddressId, model?.ShippingAddress?.AddressId))
                    count++;
            }

            if (count == 0)
            {
                return model?.OrderLineItemHistory.Count > 0;
            }

            return count > 0;
        }

        //to get consolidated history message
        public virtual string GetConsolidatedHistoryMessage(string message, string mergedMessage)
        {
            if (!string.IsNullOrEmpty(mergedMessage))
                mergedMessage += $"<br/>{ message}";
            else
                mergedMessage = message;

            return mergedMessage;
        }

        // Get order details for returned line items.
        public virtual ZnodeOmsOrderDetail GetOrderDetailsForLineItem(int orderId, NameValueCollection expands, FilterCollection filters)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            string stateName = filters.Where(s => s.FilterName.ToLower() == Constants.FilterKeys.OmsOrderStateName.ToLower()).Select(s => s.FilterValue).FirstOrDefault();

            ZnodeLogging.LogMessage("Input parameter OrderId:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, orderId);

            int orderStateId = 0;
            if (!string.IsNullOrEmpty(stateName))
            {
                orderStateId = _omsOrderStateRepository.Table.Where(w => w.OrderStateName.ToLower() == stateName.ToLower()).Select(s => s.OmsOrderStateId).FirstOrDefault();
                filters.RemoveAll(x => x.Item1.Equals(Constants.FilterKeys.OmsOrderStateName, StringComparison.InvariantCultureIgnoreCase));
            }

            ZnodeLogging.LogMessage("orderStateId :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, orderStateId );
            if (orderStateId > 0)
            {
                ZnodeOmsOrderDetail orderDetails = _orderDetailsRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection())?.WhereClause, GetExpands(expands));

                orderDetails.ZnodeOmsOrderLineItems = orderDetails.ZnodeOmsOrderLineItems.Where(w => w.OrderLineItemStateId == orderStateId && w.IsActive).ToList();

                decimal lineItemTax = 0m;
                decimal lineItemShippingCost = 0m;
                decimal subTotal = 0m;

                foreach (ZnodeOmsOrderLineItem omsOrderLineItem in orderDetails?.ZnodeOmsOrderLineItems)
                {
                    ZnodeOmsTaxOrderLineDetail omsTaxOrderLineDetail = _omsTaxOrderLineDetailRepository.Table.FirstOrDefault(w => w.OmsOrderLineItemsId == omsOrderLineItem.OmsOrderLineItemsId);

                    lineItemTax = lineItemTax + Convert.ToDecimal(omsTaxOrderLineDetail?.GST) + Convert.ToDecimal(omsTaxOrderLineDetail?.HST) + Convert.ToDecimal(omsTaxOrderLineDetail?.PST) + Convert.ToDecimal(omsTaxOrderLineDetail?.SalesTax) + Convert.ToDecimal(omsTaxOrderLineDetail?.VAT);
                    subTotal = subTotal + (omsOrderLineItem.Price * Convert.ToDecimal(omsOrderLineItem.Quantity));
                }

                orderDetails.ShippingCost = 0m;
                orderDetails.TaxCost = lineItemTax;
                orderDetails.Total = subTotal + lineItemShippingCost + lineItemTax;
                orderDetails.SubTotal = subTotal;
                orderDetails.DiscountAmount = 0m;

                ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                return orderDetails;
            }
            else
            {
                return new ZnodeOmsOrderDetail();
            }
        }

        //Calculate cartItem to send mail
        public virtual void CalculateCartItemForResendMail(OrderModel orderModel, string omsOrderLineId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter omsOrderLineId :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, omsOrderLineId);
            string[] omsOrderLineArray = omsOrderLineId.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            orderModel.OrderLineItems = CreateSingleOrderLineItem(orderModel, true).Where(lineItemId => omsOrderLineArray.Contains(lineItemId.ParentOmsOrderLineItemsId.ToString())).ToList();
            decimal lineItemTax = 0m;
            decimal lineItemShippingCost = 0m;
            decimal subTotal = 0m;
            foreach (OrderLineItemModel omsOrderLineItem in orderModel.OrderLineItems)
            {
                ZnodeOmsTaxOrderLineDetail omsTaxOrderLineDetail = _omsTaxOrderLineDetailRepository.Table.FirstOrDefault(w => w.OmsOrderLineItemsId == omsOrderLineItem.OmsOrderLineItemsId);

                lineItemTax = lineItemTax + Convert.ToDecimal(omsTaxOrderLineDetail?.GST) + Convert.ToDecimal(omsTaxOrderLineDetail?.HST) + Convert.ToDecimal(omsTaxOrderLineDetail?.PST) + Convert.ToDecimal(omsTaxOrderLineDetail?.SalesTax) + Convert.ToDecimal(omsTaxOrderLineDetail?.VAT);
                lineItemShippingCost = lineItemShippingCost + Convert.ToDecimal(omsOrderLineItem.ShippingCost);

                subTotal = subTotal + (omsOrderLineItem.Price * Convert.ToDecimal(omsOrderLineItem.Quantity));
            }
            orderModel.GiftCardAmount = 0;
            orderModel.CSRDiscountAmount = 0;
            orderModel.ShippingCost = lineItemShippingCost;
            orderModel.TaxCost = lineItemTax;
            orderModel.Total = subTotal + lineItemShippingCost + lineItemTax;
            orderModel.SubTotal = subTotal;
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //to set update order data
        public virtual SubmitOrderModel SetUpdateOrderData(OrderModel model)
        {
            SubmitOrderModel updateModel = new SubmitOrderModel
            {
                OrderId = model.OmsOrderId,
                OmsOrderDetailsId = model.OmsOrderDetailsId,
                OrderStateId = model.OmsOrderStateId,
                PaymentStateId = model.OmsPaymentStateId,
                TrackingNumber = model.TrackingNumber,
                OrderNumber = model.OrderNumber,
                ReturnOrderLineItems = model.ReturnItemList,
                IsLineItemReturned = model.IsLineItemReturned,
                LineItemReturnAmount = model.ShoppingCartModel?.OverDueAmount,
                CreatedBy = model.CreatedBy,
                ModifiedBy = model.ModifiedBy,
                RefundedSkus = string.Join(",", model?.OrderLineItemHistory?.Where(w => w.Value.OrderUpdatedStatus == ZnodeOrderStatusEnum.RETURNED.ToString())?.Select(p => p.Value.OmsOrderLineItemsId.ToString()))
            };
            return updateModel;
        }

        //to bind order details that need to retunes
        public virtual OrderModel BindOrderData(ZnodeOrderFulfillment order, ShoppingCartModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            OrderModel orderModel = new OrderModel();
            if (IsNotNull(order))
            {
                orderModel.OmsOrderId = order.OrderID;
                orderModel.BillingAddress.EmailAddress = string.IsNullOrEmpty(order.Email) ? order.BillingAddress.EmailAddress : order.Email;
                orderModel.OrderNumber = order.Order?.OrderNumber;
                orderModel.PortalId = model.PortalId;
                order.OrderDateWithTime = order.OrderDate.ToString(DefaultGlobalConfigSettingHelper.DefaultDateFormat) + " " + order.OrderDate.ToString("HH:mm");
                orderModel.OverDueAmount = order.OrderOverDueAmount;
                orderModel.OrderLineItems = order.OrderLineItems;
                orderModel.BillingAddress.CompanyName = order.BillingAddress.CompanyName;
                orderModel.PaymentDisplayName = model.Payment.PaymentDisplayName;
                orderModel.CreditCardNumber = order?.CreditCardNumber;
                orderModel.ShippingCost = order.ShippingCost;
                orderModel.TaxCost = order.TaxCost;
                orderModel.DiscountAmount = order.DiscountAmount;
                orderModel.Total = order.Total;
                orderModel.OrderDate = order.OrderDate;
                orderModel.ShippingTypeName = order.ShippingName;
                foreach (OrderLineItemModel item in orderModel.OrderLineItems)
                {
                    item.ShippingAddressHtml = GetOrderShipmentAddress(item.ZnodeOmsOrderShipment);
                    item.OrderLineItemState = GetOrderStatusForCustomer(item);
                    item.Price = (item.Price == 0) ? order.OrderLineItems.FirstOrDefault(x => x.OmsOrderLineItemsId == item.OmsOrderLineItemsId).OrderLineItemCollection.FirstOrDefault().Price : item.Price;
                    item.Quantity = Convert.ToDecimal(ServiceHelper.ToInventoryRoundOff(item.Quantity));
                    item.OrderLineItemCollection?.ForEach(x =>
                    {
                        x.Quantity = Convert.ToDecimal(ServiceHelper.ToInventoryRoundOff(x.Quantity));
                        x.IsDownloadableSKU = _pimDownloadableProduct.Table.Any(y => y.SKU == item.Sku);
                    });
                    item.IsDownloadableSKU = _pimDownloadableProduct.Table.Any(x => x.SKU == item.Sku);
                }
                if (IsNotNull(order.Order))
                {
                    string street1 = string.IsNullOrEmpty(order.Order.BillingAddress.Address2) ? string.Empty : "<br />" + order.Order.BillingAddress.Address2;
                    orderModel.BillingAddressHtml = $"{order.Order?.BillingAddress.FirstName}{" "}{order.Order?.BillingAddress.LastName}{"<br />"}{order.BillingAddress.CompanyName}{"<br />"}{order.Order.BillingAddress.Address1}{street1}{"<br />"}{ order.Order.BillingAddress.CityName}{"<br />"}{(string.IsNullOrEmpty(order.Order.BillingAddress.StateName) ? order.Order.BillingAddress.StateCode : order.Order.BillingAddress.StateName)}{"<br />"}{order.Order.BillingAddress.PostalCode}{"<br />"}{order.Order.BillingAddress.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{order.Order.BillingAddress.PhoneNumber}";
                }
            }

            if (IsNotNull(model))
            {
                orderModel.PortalId = model.PortalId;
                orderModel.PaymentDisplayName = model.Payment?.PaymentDisplayName;
                orderModel.IsQuoteOrder = model.IsQuoteOrder;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return orderModel;
        }

        //Expands necessary to get Order for Resend Mail.
        public virtual NameValueCollection GetOrderExpandForResendMail()
        {
            NameValueCollection expands = new NameValueCollection();
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrder.ToString().ToLower());
            return expands;
        }

        // Map returned order amount.
        public virtual void MapReturnedTotal(OrderModel orderModel, OrderModel returnedOrderModel)
        {
            orderModel.ReturnTaxCost = returnedOrderModel.TaxCost;
            orderModel.ReturnShippingCost = returnedOrderModel.ShippingCost;
            orderModel.ReturnSubTotal = returnedOrderModel.SubTotal;
            orderModel.ReturnTotal = returnedOrderModel.Total;
        }

        //Map Order data to Shopping cart.
        public virtual void MapOrderModelToShoppingCart(OrderModel orderModel)
        {
            orderModel.ShoppingCartModel.AdditionalInstructions = !string.IsNullOrEmpty(orderModel.AdditionalInstructions) ? orderModel.AdditionalInstructions : orderModel.ShoppingCartModel.AdditionalInstructions;
            orderModel.ShoppingCartModel.PurchaseOrderNumber = orderModel.PurchaseOrderNumber;
            orderModel.ShoppingCartModel.PODocumentName = orderModel.PoDocument;
            orderModel.ShoppingCartModel.Token = orderModel.ShoppingCartModel.Token ?? orderModel.PaymentTransactionToken;
            orderModel.ShoppingCartModel.CreditCardNumber = orderModel.ShoppingCartModel.CreditCardNumber ?? orderModel.CreditCardNumber;
            orderModel.ShoppingCartModel.CardType = orderModel.ShoppingCartModel.CardType ?? orderModel.CardType;
            orderModel.ShoppingCartModel.CreditCardExpMonth = orderModel.ShoppingCartModel.CreditCardExpMonth ?? orderModel.CreditCardExpMonth;
            orderModel.ShoppingCartModel.CreditCardExpYear = orderModel.ShoppingCartModel.CreditCardExpYear ?? orderModel.CreditCardExpYear;
            orderModel.ShoppingCartModel.ExternalId = orderModel.ShoppingCartModel.ExternalId ?? orderModel.ExternalId;
            SetOrderState(orderModel);
        }

        //Get Tracking Url by ShippingId.
        public virtual string GetTrackingUrlByShippingId(int shippingId)
         => _shippingRepository.GetById(shippingId)?.TrackingUrl;

        //Set Tracking Url.
        public virtual string SetTrackingUrl(string trackingNo, string trackingUrl)
        {
            ZnodeLogging.LogMessage("Input parameter trackingNo and trackingUrl:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new object[] { trackingNo, trackingUrl });
            return IsNotNull(trackingUrl) ? $"<a target=_blank href={ trackingUrl + trackingNo }>{trackingNo} </ a >" : trackingNo;
        }

        // Check for allowed territories.
        public virtual bool IsAllowedTerritories(ShoppingCartModel model) => model.ShoppingCartItems.Where(w => w.IsAllowedTerritories == false).ToList().Count > 0;

        //Get expands and add them to navigation properties
        public virtual List<string> GetExpandsForOrderLineItem(NameValueCollection expands)
        {
            List<string> navigationProperties = new List<string>();
            if (expands?.HasKeys() ?? false)
            {
                foreach (string key in expands.Keys)
                    //Add expand keys
                    if (Equals(key, ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower())) SetExpands(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString(), navigationProperties);
            }
            return navigationProperties;
        }

        //Cancel tax transaction.
        private static void CancelTaxTransaction(bool updated, ZnodeOmsOrderDetail order, List<ZnodeOmsOrderState> orderStateList)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            bool isCancelledOrder = order.OmsOrderStateId.Equals(orderStateList?.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.CANCELLED.ToString())?.OmsOrderStateId);
            if (updated && isCancelledOrder)
            {
                ZnodeTaxHelper taxHelper = new ZnodeTaxHelper();
                int? omsLineItemId = order?.ZnodeOmsOrderLineItems?.FirstOrDefault()?.OmsOrderLineItemsId;
                if (IsNotNull(omsLineItemId))
                    taxHelper.CancelTaxTransaction(omsLineItemId.Value, order?.PortalId);
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //Filters to get order data.
        protected virtual FilterCollection CreatFiltersForOrder(OrderStateParameterModel model)
        {
            FilterCollection filter = new FilterCollection();
            filter.Add(new FilterTuple(Constants.FilterKeys.OmsOrderId, FilterOperators.Equals, model.OmsOrderId.ToString()));
            filter.Add(new FilterTuple(Constants.FilterKeys.IsActive, FilterOperators.Equals, Convert.ToString(true)));
            ZnodeLogging.LogMessage("Filter details:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, filter);
            return filter;
        }

        //Expand for order line item.
        protected virtual NameValueCollection GetOrderLineItemExpands()
        {
            NameValueCollection expands = new NameValueCollection();
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower(), ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString().ToLower());
            return expands;
        }

        //Method for subtracting days from date.
        public virtual void GetAndMapRmaDetails(OrderModel orderModel)
        {
            ZnodeRmaConfiguration rmaConfiguration = _rmaConfigurationRepository.GetEntity(string.Empty);
            DateTime lastDateOfReturn = orderModel.OrderDate.Subtract(TimeSpan.FromDays(Convert.ToDouble(-rmaConfiguration?.MaxDays ?? 90)));

            if (IsNotNull(rmaConfiguration))
                orderModel.IsEmailNotificationForRma = rmaConfiguration.IsEmailNotification;
            orderModel.IsValidForRma = CompareTwoDates(DateTime.UtcNow, lastDateOfReturn);
        }

        //For Comparing two dates and get result.
        public virtual bool CompareTwoDates(DateTime orderCreatedDate, DateTime lastDateOfReturn)
        {
            int result = DateTime.Compare(orderCreatedDate, lastDateOfReturn);
            return (result <= 0);
        }

        //to set over due amount for return all items in the cart
        public virtual void SetOverDueAmountForReturnAllItem(OrderModel model)
        {
            if (IsNotNull(model))
                model.OverDueAmount = (model.Total - (model.ShippingCost + model.ShippingDifference)) * -1;

            decimal returnShippingCost = 0;
            int returnCount = model?.ReturnItemList?.ReturnItemList?.Where(w => w.IsAlreadyReturned == true).Count() ?? 0;

            returnShippingCost = returnCount > 0 ? model?.ReturnItemList?.ReturnItemList?.Where(x => x.IsShippingReturn == true && x.IsAlreadyReturned == true)?.Sum(x => x.ShippingCost) ?? 0 : model?.ReturnItemList?.ReturnItemList?.Where(x => x.IsShippingReturn == true)?.Sum(x => x.ShippingCost) ?? 0;

            model.OverDueAmount += (returnShippingCost * -1);
        }

        //to update line item state Id as per order stateId
        public virtual void UpdateLineItemState(int orderId, int previousOrderStateId, int currentOrderStateId, DateTime? shipDate)
        {
            ZnodeLogging.LogMessage("Input parameters orderId, previousOrderStateId and currentOrderStateId:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new object[] { orderId, previousOrderStateId, currentOrderStateId });
            if (previousOrderStateId == currentOrderStateId)
            {
                previousOrderStateId = _previousOrderStateId;
            }

            List<ZnodeOmsOrderLineItem> lineitemtoupdate = (from _dtls in _orderDetailsRepository.Table
                                                            join _lineitem in _orderLineItemRepository.Table on _dtls.OmsOrderDetailsId equals _lineitem.OmsOrderDetailsId
                                                            where _dtls.OmsOrderId == orderId &&
                                                           _dtls.IsActive == true &&
                                                           _lineitem.OrderLineItemStateId == previousOrderStateId
                                                            select _lineitem).ToList();

            ZnodeLogging.LogMessage("lineitemtoupdate count :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new object[] { lineitemtoupdate?.Count });

            foreach (ZnodeOmsOrderLineItem item in lineitemtoupdate)
            {
                item.OrderLineItemStateId = currentOrderStateId;
                item.ShipDate = currentOrderStateId == 20 ? shipDate : null;
                _orderLineItemRepository.Update(item);
            }
        }

        //to set order state
        public virtual void SetOrderState(OrderModel model)
        {
            if (ExistUpdateHistory(ZnodeConstant.OrderUpdatedStatus, model))
            {
                _previousOrderStateId = _orderDetailsRepository.Table.Where(x => x.OmsOrderId == model.OmsOrderId && x.IsActive == true).FirstOrDefault().OmsOrderStateId;
            }
        }

        //to set over due amount for return all items in the cart
        public virtual void CancleOrderAmount(OrderModel model)
        {
            if (IsNotNull(model) && !string.IsNullOrEmpty(model.OrderState) &&
                String.Equals(model.OrderState, ZnodeOrderStatusEnum.CANCELLED.ToString(), StringComparison.OrdinalIgnoreCase) &&
                model?.ShoppingCartModel?.ShoppingCartItems?.Count < 1 &&
                model.ReturnItemList?.ReturnItemList?.Count > 0)
            {
                model.Total = (model.Total - model.ReturnItemList.Total);
            }
        }

        //If the external id is set for an order then update the external id in order detail table.
        public virtual void UpdateExtenalId(OrderModel orderModel)
        {
            ZnodeLogging.LogMessage("ExternalId for order:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new object[] { orderModel?.ExternalId });

            //If the external id is set for an order then update the external id in order detail table.
            if (!string.IsNullOrEmpty(orderModel?.ExternalId))
            {
                ZnodeOmsOrderDetail orderDetail = _orderDetailsRepository.Table.FirstOrDefault(x => x.OmsOrderDetailsId == orderModel.OmsOrderDetailsId);
                if (IsNotNull(orderDetail) && !Equals(orderDetail.ExternalId, orderModel.ExternalId))
                {
                    orderDetail.ExternalId = orderModel.ExternalId;
                    ZnodeLogging.LogMessage(_orderDetailsRepository.Update(orderDetail) ? string.Format(Admin_Resources.UpdateExternalId, orderModel.OmsOrderId) : string.Format(Admin_Resources.FailToUpdateExternalId, orderModel.OmsOrderId), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                }
            }
        }

        //to return shipping amount of already returned item but but shipping is not returned at the time of return line item
        public virtual bool ReturnShippingAmount(OrderModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            bool isSuccess = false;
            List<int> lineitemIds = new List<int>();
            foreach (var item in model?.OrderLineItemHistory)
            {
                if (!string.IsNullOrEmpty(item.Value.ReturnShippingAmount))
                {
                    lineitemIds.Add(item.Value.OmsOrderLineItemsId);
                }
            }
            decimal retrunShippingCost = GetShippingCostForReturnedItem(lineitemIds);
            ZnodeLogging.LogMessage("retrunShippingCost returned from GetShippingCostForReturnedItem:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new object[] { retrunShippingCost });

            if (HelperUtility.IsNotNull(retrunShippingCost) && retrunShippingCost > 0)
            {
                UpdateOrderShippingCost(model.OmsOrderId, retrunShippingCost);
                model.OverDueAmount = retrunShippingCost * -1;
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return isSuccess;
        }

        //to update shipping cost of existing order by OrderId
        public virtual void UpdateOrderShippingCost(int orderId, decimal returnShipping)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters orderId:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new object[] { orderId });

            ZnodeOmsOrderDetail order = _orderDetailsRepository.Table.FirstOrDefault(x => x.OmsOrderId == orderId && x.IsActive == true);
            if (HelperUtility.IsNotNull(order) && returnShipping > 0)
            {
                if (HelperUtility.IsNotNull(order.ShippingDifference) && order.ShippingDifference > 0 && order.ShippingDifference >= returnShipping)
                {
                    order.ShippingDifference = Convert.ToDecimal(order.ShippingDifference - returnShipping);
                }
                else if (HelperUtility.IsNotNull(order.ShippingCost) && order.ShippingCost > 0)
                {
                    order.ShippingCost = Convert.ToDecimal(order.ShippingCost - returnShipping);
                }
                order.Total = Convert.ToDecimal(order.Total - returnShipping) > 0 ? Convert.ToDecimal(order.Total - returnShipping) : 0;

                _orderDetailsRepository.Update(order);
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        // to get shipping cost for returned item by line item IDs
        public virtual decimal GetShippingCostForReturnedItem(List<int> lineitemIds)
        {
            decimal totalRetrunShippingCost = 0;
            ZnodeLogging.LogMessage("lineitemIds count:",ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, lineitemIds?.Count);
            List<ZnodeOmsOrderLineItem> items = _orderLineItemRepository.Table.Where(x => lineitemIds.Contains(x.OmsOrderLineItemsId)).ToList();
            foreach (ZnodeOmsOrderLineItem item in items)
            {
                totalRetrunShippingCost += HelperUtility.IsNotNull(item.ShippingCost) ? item.ShippingCost.GetValueOrDefault() : 0;
            }

            foreach (ZnodeOmsOrderLineItem item in items)
            {
                item.IsShippingReturn = true;
                _orderLineItemRepository.Update(item);
            }
            return totalRetrunShippingCost;
        }

        // Map Customer Shipping.
        public virtual void CustomerShipping(OrderModel orderModel)
        {
            var shipping = (from customerShipping in _omsCustomerShippingRepository.Table
                            join shippingType in _shippingTypeRepository.Table on customerShipping.ShippingTypeId equals shippingType.ShippingTypeId
                            where customerShipping.OmsOrderDetailsId == orderModel.OmsOrderDetailsId
                            select new { AccountNumber = customerShipping.AccountNumber, ShippingMethod = customerShipping.ShippingMethod, ShippingTypeClassName = shippingType.ClassName })?.FirstOrDefault();
            if (IsNotNull(shipping))
            {
                orderModel.AccountNumber = shipping.AccountNumber;
                orderModel.ShippingMethod = shipping.ShippingMethod;
                orderModel.ShippingTypeClassName = shipping.ShippingTypeClassName;
            }
        }

        //Get portal payment display name
        public virtual string GetPortalPaymentDisplayName(int paymentSettingId, int portalId)
        {
            ZnodeLogging.LogMessage("Input parameters:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { paymentSettingId = paymentSettingId, portalId = portalId });
            return _portalPaymentSettingRepository.Table.Where(x => x.PaymentSettingId == paymentSettingId && x.PortalId == portalId).FirstOrDefault()?.PaymentDisplayName ?? string.Empty;
        }

        private static void SetCustomerShipping(OrderModel model)
        {
            model.ShoppingCartModel.Shipping.AccountNumber = model?.AccountNumber;
            ZnodeLogging.LogMessage("AccountNumber in CustomerShipping:",ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose,model.ShoppingCartModel.Shipping.AccountNumber);
            model.ShoppingCartModel.Shipping.ShippingMethod = model?.ShippingMethod;
            ZnodeLogging.LogMessage("CustomerShipping details in CustomerShipping:",ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, model.ShoppingCartModel.Shipping.ShippingMethod);
        }

        //to check shipping cost for the line item that has been already returned
        public virtual void IsShippingCostReturned(int orderId, List<ReturnOrderLineItemModel> itemstoreturn)
        {
            ZnodeLogging.LogMessage("orderId and itemstoreturn count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId, itemstoreturnCount = itemstoreturn?.Count });

            if (itemstoreturn?.Count > 0)
            {
                string ids = string.Join(",", itemstoreturn.Select(x => x.OmsOrderLineItemsId));
                var order = (from dtl in _orderDetailsRepository.Table
                             join itm in _orderLineItemRepository.Table on dtl.OmsOrderDetailsId equals itm.OmsOrderDetailsId
                             where dtl.OmsOrderDetailsId == itm.OmsOrderDetailsId &&
                             dtl.OmsOrderId == orderId &&
                             dtl.IsActive == true &&
                             itm.IsActive == true &&
                             itm.IsShippingReturn == true &&
                             ids.Contains(itm.OmsOrderLineItemsId.ToString())
                             select itm).ToList();

                if (IsNotNull(order))
                {
                    itemstoreturn.ForEach(d =>
                    {
                        var si = order
                                    .FirstOrDefault(s => s.OmsOrderLineItemsId == d.OmsOrderLineItemsId);
                        d.IsAlreadyReturned = si != null ? si.IsShippingReturn.GetValueOrDefault() : false;
                    });
                }
            }
        }

        //Get the ZnodeCheckout model.
        public virtual IZnodeCheckout GetZnodeCheeckoutModel(OrderModel model)
        {
            ZnodeLogging log = new ZnodeLogging();
            UserAddressModel userDetails = SetUserDetails(model.ShoppingCartModel);
            IZnodeCheckout checkout = SetCheckoutData(userDetails, model.ShoppingCartModel, log);
            return checkout;
        }

        //Update order line item details.
        public virtual OrderLineItemStatusListModel UpdateOrderLineItems(OrderLineItemDataListModel orderDetailListModel)
        {

            if (IsNull(orderDetailListModel) || IsNull(orderDetailListModel?.OrderLineItemDetails))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ModelCanNotBeNull);

            if (string.IsNullOrEmpty(orderDetailListModel?.OrderNumber))
                throw new ZnodeException(ErrorCodes.NotFound, Admin_Resources.ErrorOrderNumberRequired);

            return UpdateOrderLineItemsDetails(orderDetailListModel);
        }

        //Update order line item details.
        public virtual OrderLineItemStatusListModel UpdateOrderLineItemsDetails(OrderLineItemDataListModel orderDataListModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            OrderLineItemStatusListModel listResponse = new OrderLineItemStatusListModel();

            //Get Order Id based on the Order Number
            int? orderId = _omsOrderRepository.Table.Where(x => x.OrderNumber == orderDataListModel.OrderNumber)?.FirstOrDefault()?.OmsOrderId;
            ZnodeLogging.LogMessage("orderId :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId });

            if (IsNull(orderId) || orderId == 0)
                throw new ZnodeException(ErrorCodes.NotFound, Admin_Resources.OrderNotFound);

            int? orderDetailsId = _orderDetailsRepository.Table.Where(x => x.OmsOrderId == orderId.Value && x.IsActive == true)?.FirstOrDefault()?.OmsOrderDetailsId;

            if (IsNull(orderDetailsId) || orderDetailsId == 0)
                throw new ZnodeException(ErrorCodes.NotFound, Admin_Resources.OrderDetailsNotFound);

            bool status = true;

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(ZnodeOmsOrderStateEnum.IsOrderLineItemState.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue));
            string whereClause = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection()).WhereClause;
            ZnodeLogging.LogMessage("Where condition in UpdateOrderLineItemsDetails method:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, whereClause);
            List<ZnodeOmsOrderState> orderStateList = _omsOrderStateRepository.GetEntityList(whereClause)?.ToList();

            //get the current state of line item with their ids
            List<Tuple<int, int, int>> ordLineIdStateAndDisplayOrder = GetOrderLineCurrentStateWithDisplayOrder(orderDataListModel.OrderLineItemDetails);

            //Update all the order line item under the specified order number.And returns list of failed line item ids if any.
            foreach (OrderLineItemDataModel orderDetailsModel in orderDataListModel.OrderLineItemDetails)
            {
                OrderLineItemStatusModel itemStatus = new OrderLineItemStatusModel();
                try
                {
                    status = orderId > 0 && orderDetailsModel.OmsOrderLineItemsId > 0 ?
                         UpdateLineItemStatusAndTrackingNumber(orderDetailsId, orderStateList, orderDetailsModel) : false;

                    if (status)
                    {
                        itemStatus.OrderLineItemsId = orderDetailsModel.OmsOrderLineItemsId;
                        itemStatus.Message = Admin_Resources.SuccessUpdate;
                        itemStatus.Status = true;
                        listResponse.OrderLineItemStatusList.Add(itemStatus);

                        AddOrderLineNotes(orderDetailsModel.OmsOrderLineItemsId, orderDetailsId.GetValueOrDefault());
                    }
                }
                catch (Exception ex)
                {
                    itemStatus.OrderLineItemsId = orderDetailsModel.OmsOrderLineItemsId;
                    itemStatus.Message = ex.Message;
                    itemStatus.Status = false;
                    listResponse.OrderLineItemStatusList.Add(itemStatus);
                }
            }

            orderStateList = null;

            if (status)
                UpdateOrderByLineItemState(orderId.Value, orderDataListModel, ordLineIdStateAndDisplayOrder);

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return listResponse;
        }

        //Update line item status and tracking number
        public virtual bool UpdateLineItemStatusAndTrackingNumber(int? orderDetailsId, List<ZnodeOmsOrderState> orderStateList, OrderLineItemDataModel orderDetailsModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("orderDetailsId while updating line item:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, orderDetailsId);
            bool status;
            //Get the order state id on the basis of entered state code.
            orderDetailsModel.OrderLineItemStateId = !string.IsNullOrEmpty(orderDetailsModel.OrderLineItemState) ?
                orderStateList?.FirstOrDefault(x => x.OrderStateName.ToLower() == orderDetailsModel.OrderLineItemState.ToLower())?.OmsOrderStateId : orderStateList?.FirstOrDefault(x => x.OmsOrderStateId == orderDetailsModel.OrderLineItemStateId)?.OmsOrderStateId;

            if (IsNull(orderDetailsModel.OrderLineItemStateId) || orderDetailsModel.OrderLineItemStateId == 0)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidOrderStatus);

            // Update line item status & tracking number.
            status = orderDetailsModel.OrderLineItemStateId > 0 ? UpdateOrderLineItem(orderDetailsModel, orderDetailsId.Value) : false;
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return status;
        }


        //Get order line item and update.
        public virtual bool UpdateOrderLineItem(OrderLineItemDataModel orderLineItemModel, int omsOrderDetailsId)
        {
            ZnodeLogging.LogMessage("Input parameters :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { omsOrderDetailsId = omsOrderDetailsId });

            bool status = true;

            //Get the Order Line Item Details based on the OrderLineItem & OrderDetailsId
            ZnodeOmsOrderLineItem model = _orderLineItemRepository.Table.FirstOrDefault(x => x.OmsOrderLineItemsId == orderLineItemModel.OmsOrderLineItemsId && x.OmsOrderDetailsId == omsOrderDetailsId);
            if (IsNotNull(model))
            {
                if (!IsPartialShipping(orderLineItemModel))
                {
                    UpdateExistingItem(model, orderLineItemModel);
                }
                else
                {
                    status = ShipItemPartially(model, orderLineItemModel);
                }
            }
            else
            {
                status = false;
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.InvalidOrderNo);
            }

            return status;
        }

        //Update order line items.
        public virtual void UpdateOrderLineItem(ZnodeOmsOrderLineItem model, OrderLineItemDataModel orderLineItemDetailModel)
        {
            model.TrackingNumber = (!string.IsNullOrEmpty(Convert.ToString(orderLineItemDetailModel.TrackingNumber))) ? orderLineItemDetailModel.TrackingNumber : model.TrackingNumber;
            model.OrderLineItemStateId = (orderLineItemDetailModel.OrderLineItemStateId > 0) ? orderLineItemDetailModel.OrderLineItemStateId : model.OrderLineItemStateId;
            _orderLineItemRepository.Update(model);
        }

        public virtual void SendEmailNotification(ShoppingCartModel model, bool isUpdateAction, IZnodeCheckout checkout, ZnodeOrderFulfillment order, OrderModel orderModel, bool isEnableBcc)
        {
            string storeReceiptHtml = string.Empty;
            //Attach the store receipt HTML to the order and return.
            if (!string.IsNullOrEmpty(orderModel.ReceiptHtml))
                storeReceiptHtml = orderModel.ReceiptHtml;
            else
                storeReceiptHtml = GetOrderReceipt(order, checkout, model.FeedbackUrl, model.LocaleId, isUpdateAction, out isEnableBcc);
            //Send order amount notification email.
            SendOrderAlertStoreNotification(order.Total, orderModel, storeReceiptHtml, isEnableBcc);
        }

        //Send order alert notification email.
        public virtual void SendOrderAlertStoreNotification(decimal orderAmount, OrderModel orderModel, string storeReceiptHtml, bool isEnableBcc)
        {
            if (IsNotNull(orderModel.PortalId))
            {
                //Get portal information by portalId
                ZnodePortal portal = _portalRepository.GetById(orderModel.PortalId);
                if (IsNotNull(portal?.OrderAmount))
                    if (orderAmount >= portal.OrderAmount)
                        if (!string.IsNullOrEmpty(storeReceiptHtml))
                            SendOrderReceipt(orderModel.PortalId, portal.Email, $"{Admin_Resources.TitleStoreOrderReceipt} - {orderModel.OrderNumber}", null, null, storeReceiptHtml, isEnableBcc);
            }
        }

        //to generate order receipt
        public virtual string GetOrderReceipt(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, bool isUpdate = false, string emailTemplate = "OrderReceipt")
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            foreach (OrderLineItemModel item in order.OrderLineItems)
            {
                if (item.PersonaliseValueList != null)
                    item.PersonaliseValueList.Remove("AllocatedLineItems");

                if (item.PersonaliseValuesDetail != null)
                    item.PersonaliseValuesDetail.RemoveAll(pv => pv.PersonalizeCode == "AllocatedLineItems");
            }

            IZnodeOrderReceipt receipt = GetOrderReceiptInstance(order, checkout.ShoppingCart);

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("OrderReceipt", (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
            if (HelperUtility.IsNotNull(emailTemplateMapperModel))
            {
                string receiptContent = ShowOrderAdditionalDetails(emailTemplateMapperModel.Descriptions, order.Custom1);
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderReceiptHtml(receiptContent));
            }
            return string.Empty;
        }

        //to check Order State by stateId and state name
        public virtual bool IsOrderStateExist(int stateId, string stateName)
        {
            ZnodeLogging.LogMessage("Input parameters :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { stateId = stateId, stateName = stateName });

            ZnodeOmsOrderState state = _omsOrderStateRepository.Table.Where(x => x.OmsOrderStateId == stateId && x.OrderStateName.ToLower() == stateName.ToLower())?.FirstOrDefault() ?? null;
            return IsNull(state) ? false : state?.OmsOrderStateId > 0;
        }
        //to check whether the line item process for partial shipping
        public virtual bool IsPartialShipping(OrderLineItemDataModel model)
        => IsOrderStateExist(model.OrderLineItemStateId.GetValueOrDefault(), ZnodeOrderStatusEnum.SHIPPED.ToString()) && IsNotNull(model.Quantity) && (model?.Quantity > 0);

        //to ship item partially by Line item id and quantity
        public virtual bool ShipItemPartially(ZnodeOmsOrderLineItem model, OrderLineItemDataModel lineItemModel)
        {
            bool isSuccess = true;
            if (IsNotNull(model))
            {
                //if total quantity greater than partial shipping quantity
                if (model.Quantity > lineItemModel.Quantity)
                {
                    isSuccess = AddPartialShippedItems(lineItemModel);
                }
                else
                {
                    UpdateExistingItem(model, lineItemModel);
                }
            }
            return isSuccess;
        }

        //to update order state by line item state
        public virtual void UpdateOrderByLineItemState(int orderId, OrderLineItemDataListModel model, List<Tuple<int, int, int>> ordLineIdStateAndDisplayOrder)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeOmsOrderDetail order = _orderDetailsRepository.Table.FirstOrDefault(x => x.OmsOrderId == orderId && x.IsActive);
            if (IsOrderStateUpdate(orderId))
            {
                int lineItemStatusId = model?.OrderLineItemDetails?.Select(x => x.OrderLineItemStateId)?.FirstOrDefault() ?? 0;
                if (lineItemStatusId > 0)
                {
                    bool orderStatus = _omsOrderStateRepository.Table.FirstOrDefault(x => x.OmsOrderStateId == lineItemStatusId)?.IsOrderState ?? false;
                    if (orderStatus)
                    {
                        if (IsNotNull(order) && order.OmsOrderStateId != lineItemStatusId)
                        {
                            order.OmsOrderStateId = lineItemStatusId;
                            _orderDetailsRepository.Update(order);

                            AddOrderNotes(orderId, order?.OmsOrderDetailsId);
                        }
                    }
                    else
                    {
                        //Update order state to InProgress
                        UpdateOrderStateToInProgress(orderId);
                        AddOrderNotes(orderId, order?.OmsOrderDetailsId);
                    }
                }
            }
            else
            {
                foreach (var lstItem in ordLineIdStateAndDisplayOrder)
                {
                    var lineItm = _orderLineItemRepository.GetById(lstItem.Item1);
                    var stateDisplayOrder = _omsOrderStateRepository.GetById(lineItm.OrderLineItemStateId.GetValueOrDefault())?.DisplayOrder;

                    if (stateDisplayOrder < lstItem.Item3)
                    {
                        int orderLineStateId = lineItm.OrderLineItemStateId.GetValueOrDefault();
                        bool orderStatus = _omsOrderStateRepository.Table.FirstOrDefault(x => x.OmsOrderStateId == orderLineStateId)?.IsOrderState ?? false;
                        if (orderStatus)
                        {
                            if (IsNotNull(order))
                            {
                                order.OmsOrderStateId = lineItm.OrderLineItemStateId.GetValueOrDefault();
                                _orderDetailsRepository.Update(order);
                                AddOrderNotes(orderId, order?.OmsOrderDetailsId);
                            }
                        }
                        else
                        {
                            UpdateOrderStateToInProgress(orderId);
                            AddOrderNotes(orderId, order?.OmsOrderDetailsId);
                        }
                    }
                    else if (stateDisplayOrder > lstItem.Item3)
                    {
                        UpdateOrderStateToInProgress(orderId);
                        AddOrderNotes(orderId, order?.OmsOrderDetailsId);
                    }
                }
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }

        //to check all items in order are updated 
        public virtual bool IsOrderStateUpdate(int orderId)
        {
            ZnodeLogging.LogMessage("Input parameters :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId });

            bool isSuccess = false;

            List<int?> lineStateId = (from dtls in _orderDetailsRepository.Table
                                      join lineitems in _orderLineItemRepository.Table on dtls.OmsOrderDetailsId equals lineitems.OmsOrderDetailsId
                                      where dtls.IsActive && dtls.OmsOrderId == orderId
                                      select lineitems.OrderLineItemStateId)?.Distinct()?.ToList() ?? null;

            if (lineStateId?.Count > 0)
            {
                return lineStateId?.Count == 1;
            }
            return isSuccess;
        }

        //to update existing item details
        public virtual void UpdateExistingItem(ZnodeOmsOrderLineItem model, OrderLineItemDataModel orderLineItemModel)
        {
            UpdateOrderLineItem(model, orderLineItemModel);
            if (IsNotNull(model.ParentOmsOrderLineItemsId))
            {
                //Get the Parent Order Line Item Details.
                model = _orderLineItemRepository.Table.FirstOrDefault(x => x.OmsOrderLineItemsId == model.ParentOmsOrderLineItemsId);
                if (IsNotNull(model))
                    UpdateOrderLineItem(model, orderLineItemModel);
            }
        }

        //to add partial shipped items to database
        public virtual bool AddPartialShippedItems(OrderLineItemDataModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            int status = 0;
            IZnodeViewRepository<SEODetailsModel> objStoredProc = new ZnodeViewRepository<SEODetailsModel>();
            objStoredProc.SetParameter("@LineItemId", model.OmsOrderLineItemsId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@LineItemStateId", model.OrderLineItemStateId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@Quantity", model.Quantity, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@TrackingNumber", string.IsNullOrEmpty(model.TrackingNumber) ? string.Empty : model.TrackingNumber, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@status", null, ParameterDirection.Output, DbType.Int32);
            objStoredProc.ExecuteStoredProcedureList("Znode_InsertPartialShippedItems @LineItemId,@LineItemStateId,@Quantity,@TrackingNumber,@status OUT", 4, out status);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return status == 1;
        }

        //Update Order Paypal Payment TransactionId
        public virtual bool UpdateOrderTransactionId(int orderId, string transactionId, int createdBy = 0, int modifiedBy = 0)
        {
            ZnodeLogging.LogMessage("Input parameters orderId and transactionId:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId, transactionId = transactionId });

            if (orderId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.OrderIdLessThanOne);

            if (string.IsNullOrEmpty(transactionId))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorTransactionIdNull);

            ZnodeOmsOrderDetail orderDetails = _orderDetailsRepository.Table.Where(x => x.OmsOrderId == orderId && x.IsActive)?.FirstOrDefault();
            ZnodeLogging.LogMessage("Order details in UpdateOrderTransactionId method:",ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose,orderDetails);
            if (IsNotNull(orderDetails))
            {
                orderDetails.TransactionId = transactionId;
                orderDetails.CreatedBy = createdBy;
                orderDetails.ModifiedBy = modifiedBy;
                return _orderDetailsRepository.Update(orderDetails);
            }
            return false;
        }


        public virtual bool ReorderCompleteOrder(int orderId, int portalId, int userId = 0, int omsOrderLineItemsId = 0)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId, portalId = portalId, omsOrderLineItemsId = omsOrderLineItemsId });

            if (orderId < 1 && omsOrderLineItemsId < 1)
                throw new ZnodeException(ErrorCodes.IdLessThanOne, Admin_Resources.OrderIdLessThanOne);

            int savedCartId = GetSavedCartId(portalId, userId);

            int status = 0;
            IZnodeViewRepository<SEODetailsModel> objStoredProc = new ZnodeViewRepository<SEODetailsModel>();
            objStoredProc.SetParameter("@OmsOrderId", orderId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@OmsSavedCartId", savedCartId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@UserId", userId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@OmsOrderLineItemsId", omsOrderLineItemsId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@status", null, ParameterDirection.Output, DbType.Int32);
            objStoredProc.ExecuteStoredProcedureList("Znode_InsertSaveCartLineItemsForReOrder @OmsOrderId,@OmsSavedCartId,@UserId,@OmsOrderLineItemsId,@status OUT", 4, out status);

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return status == 1;
        }

        //to get savedcartId by cookieMappingId
        private int GetSavedCartId(int portalId = 0, int? userId = 0)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            int cookieMappingId = Convert.ToInt32(_cookieMappingRepository.Table.FirstOrDefault(x => x.UserId == userId)?.OmsCookieMappingId);
            ZnodeLogging.LogMessage("Input parameters PortalId and UserId :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { userId = userId, portalId = portalId });

            if (cookieMappingId > 0)
            {
                IZnodeRepository<ZnodeOmsSavedCart> _savedCartRepository = new ZnodeRepository<ZnodeOmsSavedCart>();
                ZnodeOmsSavedCart savedCart = _savedCartRepository.Table.FirstOrDefault(x => x.OmsCookieMappingId == cookieMappingId);
                if (HelperUtility.IsNull(savedCart))
                {
                    ZnodeOmsCookieMapping cookieMapping = _cookieMappingRepository.Table.FirstOrDefault(x => x.OmsCookieMappingId == cookieMappingId);
                    if (HelperUtility.IsNull(cookieMapping) || cookieMapping.OmsCookieMappingId <= 0)
                    {
                        cookieMappingId = CreateCookieMappingId(userId, portalId);
                    }
                    //savedcartId not exist for cookieMappingId then create new.
                    ZnodeOmsSavedCart newCart = _savedCartRepository.Insert(new ZnodeOmsSavedCart()
                    {
                        OmsCookieMappingId = cookieMappingId,
                        CreatedDate = HelperUtility.GetDateTime(),
                        ModifiedDate = HelperUtility.GetDateTime()
                    });
                    return newCart.OmsSavedCartId;
                }
                return Convert.ToInt32(savedCart.OmsSavedCartId);
            }
            return 0;
        }

        //create new cookiemappingid for cart
        protected virtual int CreateCookieMappingId(int? userId, int portalId)
        {
            ZnodeLogging.LogMessage("Input parameters userId and portalId :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose,new  { userId = userId, portalId = portalId });
            ZnodeOmsCookieMapping cookieMapping = _cookieMappingRepository.Insert(new ZnodeOmsCookieMapping()
            {
                UserId = userId == 0 ? null : userId,
                CreatedDate = HelperUtility.GetDateTime(),
                ModifiedDate = HelperUtility.GetDateTime(),
                PortalId = portalId
            });
            return Convert.ToInt32(cookieMapping?.OmsCookieMappingId);
        }

        // Convert the quote to order
        public virtual OrderModel ConvertToOrder(AccountQuoteModel accountQuoteModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            int isUpdated = 0;
            if (IsNull(accountQuoteModel))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ModelNotNull);

            if (accountQuoteModel.OmsQuoteId <= 0)
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorQuoteIdGreaterThanZero);
            int approvedOrderId = _omsOrderStateRepository.Table.FirstOrDefault(x => x.OrderStateName == ZnodeOrderStatusEnum.APPROVED.ToString()).OmsOrderStateId;
            IAccountQuoteService _accountQuoteService = GetService<IAccountQuoteService>();
            _accountQuoteService.UpdateQuoteStatus(approvedOrderId, accountQuoteModel.OmsQuoteId.ToString(), "Ordered,Draft", out isUpdated);

            accountQuoteModel.IsUpdated = isUpdated == 1;

            InsertApproverComments(accountQuoteModel);

            ZnodeLogging.LogMessage("Input parameter OmsQuoteId for getting quote details :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new object[] { accountQuoteModel?.OmsQuoteId });
            ZnodeOmsQuote quoteDetails = _znodeOmsQuote.GetById(accountQuoteModel.OmsQuoteId);

            if (IsNull(quoteDetails))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.DetailsNotFound);

            ShoppingCartModel model = GetShoppingCartModel(quoteDetails, accountQuoteModel);

            if (IsAllowedTerritories(model))
                throw new ZnodeException(ErrorCodes.AllowedTerritories, Admin_Resources.AllowedTerritoriesError);

            SubmitOrderModel submitOrderModel = new SubmitOrderModel();

            ParameterModel portalId = new ParameterModel() { Ids = Convert.ToString(model.PortalId) };

            //Get generated unique order number on basis of current date.
            submitOrderModel.OrderNumber = GenerateOrderNumber(submitOrderModel, portalId);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return SaveOrder(model, submitOrderModel);
        }
        #endregion Public Methods

        #region Protected Methods

        //Add the order notes in case of order status updated by API.
        protected virtual void AddOrderNotes(int orderId, int? omsOrderDetailsId)
        {
            ZnodeLogging.LogMessage("Input parameters OrderId and omsOrderDetailsId :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose,new object[] { orderId, omsOrderDetailsId });
            if (omsOrderDetailsId.GetValueOrDefault() > 0)
            {
                ZnodeOmsNote ordNote = new ZnodeOmsNote();
                ordNote.OmsOrderDetailsId = omsOrderDetailsId;
                ordNote.Notes = string.Format(Admin_Resources.SuccessOrderStatusUpdate, orderId);
                _omsNoteRepository.Insert(ordNote);
            }
        }

        //Add the order notes in case of line item updated by API.
        protected virtual void AddOrderLineNotes(int omsOrderLineItemsId, int orderDetailsId)
        {
            ZnodeLogging.LogMessage("Input parameters omsOrderLineItemsId and orderDetailsId:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose,new object[] { omsOrderLineItemsId, orderDetailsId } );
            ZnodeOmsNote lineNote = new ZnodeOmsNote();
            lineNote.OmsOrderDetailsId = orderDetailsId;
            lineNote.Notes = string.Format(Admin_Resources.SuccessOrderLineItemStatusUpdate, omsOrderLineItemsId);
            _omsNoteRepository.Insert(lineNote);
        }

        //Updates the order status to In Progress if available else keep the stateus as is
        protected virtual void UpdateOrderStateToInProgress(int orderId)
        {
            ZnodeLogging.LogMessage("OrderId:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, orderId);
            ZnodeOmsOrderDetail order = _orderDetailsRepository.Table.FirstOrDefault(x => x.OmsOrderId == orderId && x.IsActive);
            if (IsNotNull(order))
            {
                int? stateId = _omsOrderStateRepository.Table.FirstOrDefault(x => x.OrderStateName.ToUpper() == InProgressOrderState)?.OmsOrderStateId;
                if (IsNotNull(stateId) && stateId.GetValueOrDefault() > 0)
                {
                    order.OmsOrderStateId = stateId.GetValueOrDefault();
                    _orderDetailsRepository.Update(order);
                }
            }
        }

        //Add the current order state and other things in tuple.
        protected virtual List<Tuple<int, int, int>> GetOrderLineCurrentStateWithDisplayOrder(List<OrderLineItemDataModel> orderLineItemDetails)
        {
            List<Tuple<int, int, int>> ordLineIdStateAndDisplayOrderList = new List<Tuple<int, int, int>>();

            foreach (OrderLineItemDataModel lineItem in orderLineItemDetails)
            {
                var lineItm = _orderLineItemRepository.GetById(lineItem.OmsOrderLineItemsId);
                if (IsNull(lineItm))
                    throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorInvalidOrderLineItemId);

                var stateDisplayOrder = _omsOrderStateRepository.GetById(lineItm.OrderLineItemStateId.GetValueOrDefault())?.DisplayOrder;
                ordLineIdStateAndDisplayOrderList.Add(new Tuple<int, int, int>(lineItm.OmsOrderLineItemsId, lineItm.OrderLineItemStateId.GetValueOrDefault(), stateDisplayOrder.GetValueOrDefault()));
            }
            return ordLineIdStateAndDisplayOrderList;
        }

        //Get order receipt instance.
        protected virtual IZnodeOrderReceipt GetOrderReceiptInstance(OrderModel order)
        {
            var objZnodeOrderReceipt = GetService<IZnodeOrderReceipt>();
            objZnodeOrderReceipt.FromApi = true;
            objZnodeOrderReceipt.OrderModel = order;
            return objZnodeOrderReceipt;
        }

        //Get order receipt instance.
        protected virtual IZnodeOrderReceipt GetOrderReceiptInstance(ZnodeOrderFulfillment order, ZnodeShoppingCart shoppingCart)
        {
            return GetService<IZnodeOrderReceipt>(new ZnodeNamedParameter("order", order),new ZnodeNamedParameter("shoppingCart", shoppingCart));
        }
        #endregion

        #region Private Methods

        //Get shopping cart model using quote details
        private ShoppingCartModel GetShoppingCartModel(ZnodeOmsQuote quoteDetails, AccountQuoteModel accountQuoteModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            //Bind cart parameter model.
            CartParameterModel cartParameterModel = BindCartParameterModel(quoteDetails, accountQuoteModel);
            IShoppingCartService _shoppingCartService = GetService<IShoppingCartService>();
            ShoppingCartModel model = _shoppingCartService.GetShoppingCartDetails(cartParameterModel);
            model.UserDetails = _userService.GetUserById(quoteDetails.UserId, null);

            AddressModel shippingAddress;
            AddressModel billingAddress;

            //Bind shipping billing address.
            BindShippingBillingAddress(quoteDetails, out shippingAddress, out billingAddress);

            //Bind shopping cart model.
            BindShoppingCartModel(quoteDetails, accountQuoteModel, model, shippingAddress, billingAddress);
            PaymentSettingModel paymentSetting = _paymentSettingService.GetPaymentSetting(quoteDetails.PaymentSettingId.GetValueOrDefault(), null, model.PortalId);
            model.Payment = new PaymentModel { BillingAddress = billingAddress, ShippingAddress = shippingAddress, PaymentSetting = paymentSetting, PaymentDisplayName = paymentSetting.PaymentDisplayName };
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return model;
        }

        protected virtual int GetOrderStateIdByName(string orderStateName = "")
            => _omsOrderStateRepository.Table.FirstOrDefault(x => x.OrderStateName.ToUpper() == orderStateName.ToUpper())?.OmsOrderStateId ?? 0;

        //get publish catalog id
        private int GetPublishCatalogId(int portalId)
        {
            ZnodeLogging.LogMessage("Get publish catalogId by using portal Id:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, portalId);
            int? portalCatalogId = _portalCatalogRepository.Table.Where(x => x.PortalId == portalId)?.FirstOrDefault()?.PublishCatalogId;
            if (portalCatalogId > 0)
                portalCatalogId = portalCatalogId.GetValueOrDefault();

            return portalCatalogId.GetValueOrDefault();
        }

        //Set Expand parameters for Order List.
        private void SetOrderListWithExpands(DataSet dataSet, List<OrderModel> entites, PageListModel pageListModel)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (!HelperUtility.IsNull(dataSet) && dataSet.Tables.Count > 0)
            {
                DataTable dataTable = dataSet.Tables[0];

                DateTime datetimeStart = DateTime.UtcNow;
                //Convert Dataset to entity
                foreach (DataRow row in dataTable.Rows)
                    entites.Add(JsonConvert.DeserializeObject<OrderModel>(Convert.ToString(row["OrderJSON"])));
                ZnodeLogging.LogMessage("Order entities list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OrderEntitesCount = entites });
                foreach (OrderModel orderModel in entites)
                {
                    if (IsNotNull(orderModel?.OrderLineItems) && orderModel.OrderLineItems.Count > 0)
                    {
                        orderModel.OrderItem = orderModel.OrderLineItems.FirstOrDefault().ProductName;
                        orderModel.ItemCount = orderModel.OrderLineItems.Where(x => x.ParentOmsOrderLineItemsId == null).ToList().Count;
                    }
                }
                DateTime datetimeStop = DateTime.UtcNow;

                ZnodeLogging.LogMessage(string.Format(Admin_Resources.ExecutionTimeDataBinding, Convert.ToString((datetimeStop - datetimeStart).TotalSeconds)), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                pageListModel.TotalRowCount = dataSet.Tables[0].Rows.Count > 0 ? Convert.ToInt32(dataSet.Tables[0].Rows[0]["RowsCount"]) : 0;
            }
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        }
        //Update external Id against Order number.
        private bool UpdateOMSExternalId(string orderNumber, string externalId)
        {
            ZnodeOmsOrderDetail orderDetail = GetOrderDetailsByOMSOrderNumber(orderNumber);
            return UpdateOMSOrderDetailsExternalId(orderDetail, externalId);
        }

        //Get Order Details by Order Number.
        private ZnodeOmsOrderDetail GetOrderDetailsByOMSOrderNumber(string orderNumber)
        {
            ZnodeLogging.LogMessage("Order Number:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, orderNumber);
            ZnodeOmsOrderDetail orderDetail = (from znodeorder in _omsOrderRepository.Table
                                               join znodeorderdetail in _orderDetailsRepository.Table on znodeorder.OmsOrderId equals znodeorderdetail.OmsOrderId
                                               where znodeorder.OrderNumber == orderNumber && znodeorderdetail.IsActive
                                               select znodeorderdetail)?.FirstOrDefault();
            return orderDetail;
        }

        //Update external Id against Order Details.
        private bool UpdateOMSOrderDetailsExternalId(ZnodeOmsOrderDetail orderDetail, string externalId)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNotNull(orderDetail) && !string.IsNullOrEmpty(externalId))
                orderDetail.ExternalId = externalId;

            OrderModel orderModel = orderDetail.ToModel<OrderModel>();

            ZnodeLogging.LogMessage("External Id:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, externalId);
            bool isSavedExternalId = false;
            if (IsNotNull(orderDetail))
            {
                isSavedExternalId = _orderDetailsRepository.Update(orderDetail);
                ZnodeLogging.LogMessage(isSavedExternalId ? string.Format(Admin_Resources.UpdateExternalId, orderModel.OmsOrderId) : string.Format(Admin_Resources.FailToUpdateExternalId, orderModel.OmsOrderId), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Warning);
            }

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return isSavedExternalId;
        }

        private string ShowOrderAdditionalDetails(string receiptContent, string customData)
        {
            if (!string.IsNullOrEmpty(customData))
                return receiptContent.Replace("#FeedBack#", GenerateOrderAdditionalInfoTemplate(customData));
            else return receiptContent;
        }

        //to generate order additional information template
        private string GenerateOrderAdditionalInfoTemplate(string customData)
        {
            string template = string.Empty;
            var CustomDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(customData);
            if (HelperUtility.IsNotNull(CustomDict))
            {
                template = ("<b>Additional Information</b>");

                if (CustomDict.ContainsKey("ProductName"))
                    template += $" <br />Product will be used by  { CustomDict["ProductName"]}";

                if (CustomDict.ContainsKey("RecipientName"))
                    template += $" <br />Recipient of the product {CustomDict["RecipientName"]}";

                if (CustomDict.ContainsKey("ApproverManager"))
                    template += $" <br />Approving Manager {CustomDict["ApproverManager"]}";

                if (CustomDict.ContainsKey("ProjectName"))
                    template += $" <br />Project Name {CustomDict["ProjectName"]}";

                if (CustomDict.ContainsKey("EventDate"))
                    template += $" <br />Event Date {CustomDict["EventDate"]}";

                if (CustomDict.ContainsKey("InHandsDate"))
                    template += $" <br />In Hands Date {CustomDict["InHandsDate"]}";
            }

            return template;
        }

        //get the order details by order id or order number.
        protected virtual OrderModel GetOrderByOrderDetails(int orderId, string orderNumber = "", FilterCollection filters = null, NameValueCollection expands = null)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { orderId = orderId, orderNumber = orderNumber });
            ZnodeOmsOrder order = null;

            //Variable to check method call from receipt or from other recource.
            bool isFromOrderReceipt = !string.IsNullOrEmpty(expands.Get(ExpandKeys.IsFromOrderReceipt));
            bool isFromReOrder = string.IsNullOrEmpty(expands.Get(ExpandKeys.IsFromReOrder));
            bool isOrderHistory = !string.IsNullOrEmpty(expands.Get(ZnodeOmsOrderDetailEnum.ZnodeOmsHistories.ToString()));

            if (IsNull(filters))
                filters = new FilterCollection();

            orderNumber = string.IsNullOrEmpty(orderNumber) ? filters.Find(x => string.Equals(x.FilterName, ZnodeOmsOrderEnum.OrderNumber.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Item3 : orderNumber;
            string emailAddress = filters.Find(x => string.Equals(x.FilterName, ZnodeUserEnum.Email.ToString(), StringComparison.CurrentCultureIgnoreCase))?.Item3;

            int portalId;
            int.TryParse(filters.FirstOrDefault(x => x.FilterName.Equals(ZnodePortalEnum.PortalId.ToString(), StringComparison.InvariantCultureIgnoreCase))?.FilterValue, out portalId);

            //check if OrderNumber and EmailAddress available and based on it we will show the order history
            if (!string.IsNullOrEmpty(orderNumber) && !string.IsNullOrEmpty(emailAddress))
            {
                ZnodeLogging.LogMessage("OrderNumber and EmailAddress to show order history: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { OrderNumber = orderNumber, EmailAddress = emailAddress });
                IZnodeRepository<ZnodeUser> _userRepository = new ZnodeRepository<ZnodeUser>();

                filters.RemoveAll(x => string.Equals(x.FilterName, ZnodeOmsOrderEnum.OrderNumber.ToString(), StringComparison.CurrentCultureIgnoreCase));
                filters.RemoveAll(x => string.Equals(x.FilterName, ZnodeUserEnum.Email.ToString(), StringComparison.CurrentCultureIgnoreCase));
                order = (from znodeorder in _omsOrderRepository.Table
                         join znodeorderdetail in _orderDetailsRepository.Table on znodeorder.OmsOrderId equals znodeorderdetail.OmsOrderId
                         join znodeUser in _userRepository.Table on znodeorderdetail.UserId equals znodeUser.UserId
                         where znodeorderdetail.PortalId == portalId && znodeorder.OrderNumber == orderNumber && znodeUser.Email == emailAddress
                         select znodeorder)?.FirstOrDefault();

            }

            else if (!string.IsNullOrEmpty(orderNumber))
                //Get active order by order number.
                order = _omsOrderRepository.Table.FirstOrDefault(x => x.OrderNumber == orderNumber);
            else
                //Get active order by order id.
                order = _omsOrderRepository.Table.FirstOrDefault(x => x.OmsOrderId == orderId);

            filters.Add(new FilterTuple(Constants.FilterKeys.OmsOrderId, FilterOperators.Equals, orderId > 0 ? orderId.ToString() : order?.OmsOrderId.ToString()));
            filters.Add(new FilterTuple(Constants.FilterKeys.IsActive, FilterOperators.Equals, ZnodeConstant.TrueValue));

            ZnodeOmsOrderDetail orderDetails = null;

            ZnodeLogging.LogMessage("Filters to get order details: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { Filters = filters });
            if (orderId > 0 || (IsNotNull(order) && order?.OmsOrderId > 0))
                orderDetails = _orderDetailsRepository.GetEntity(DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection())?.WhereClause, GetExpands(expands));

            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

            return GetOrderDetails(order, orderDetails, isFromOrderReceipt, isOrderHistory, isFromReOrder, expands);
        }

        //Bind shipping billing address.
        private void BindShippingBillingAddress(ZnodeOmsQuote quoteDetails, out AddressModel shippingAddress, out AddressModel billingAddress)
        {
            shippingAddress = _addressRepository.GetById(quoteDetails.ShippingAddressId.GetValueOrDefault())?.ToModel<AddressModel>();
            billingAddress = _addressRepository.GetById(quoteDetails.BillingAddressId.GetValueOrDefault())?.ToModel<AddressModel>();
        }

        //Bind shopping cart model.
        private void BindShoppingCartModel(ZnodeOmsQuote quoteDetails, AccountQuoteModel accountQuoteModel, ShoppingCartModel model, AddressModel shippingAddress, AddressModel billingAddress)
        {
            model.ShippingAddress = shippingAddress;
            model.BillingAddress = billingAddress;
            model.IsQuoteOrder = true;
            model.UserId = quoteDetails.UserId;
            model.PortalId = quoteDetails.PortalId;
            model.OmsQuoteId = quoteDetails.OmsQuoteId;
            model.CurrencyCode = accountQuoteModel.CurrencyCode;
            model.CultureCode = accountQuoteModel.CultureCode;
            model.CardType = quoteDetails.CardType;
            model.CreditCardNumber = quoteDetails.CreditCardNumber;
            model.CreditCardExpMonth = quoteDetails.CreditCardExpMonth;
            model.CreditCardExpYear = quoteDetails.CreditCardExpYear;
            model.Token = quoteDetails.PaymentTransactionToken;
            model.PODocumentName = quoteDetails.PoDocument;
            model.PurchaseOrderNumber = quoteDetails.PurchaseOrderNumber;
            model.OrderDate = DateTime.Now;
            model.OmsOrderStatusId = GetOrderStateIdByName(ZnodeOrderStatusEnum.SUBMITTED.ToString());
            model.CustomTaxCost = quoteDetails.TaxCost;
            model.CustomShippingCost = quoteDetails.ShippingCost;
        }

        //Bind cart parameter model.
        private CartParameterModel BindCartParameterModel(ZnodeOmsQuote quoteDetails, AccountQuoteModel accountQuoteModel)
        {
            return new CartParameterModel
            {
                OmsQuoteId = quoteDetails.OmsQuoteId,
                ShippingId = quoteDetails.ShippingId,
                PublishedCatalogId = GetPublishCatalogId(quoteDetails.PortalId),
                LocaleId = accountQuoteModel.LocaleId.Equals(0) ? GetDefaultLocaleId() : accountQuoteModel.LocaleId,
                UserId = quoteDetails.UserId,
                PortalId = quoteDetails.PortalId
            };
        }
        //Insert approver comments.
        private void InsertApproverComments(AccountQuoteModel accountQuoteModel)
        {
            //Update comments for the approver.
            if (accountQuoteModel.IsUpdated && IsNotNull(accountQuoteModel.Comments))
            {
                int quoteId = Convert.ToInt32(accountQuoteModel.OmsQuoteId);
                ZnodeOmsQuoteComment quoteComment = _omsQuoteComment.Insert(new ZnodeOmsQuoteComment() { OmsQuoteId = quoteId, Comments = accountQuoteModel.Comments });
                if (quoteComment?.OmsQuoteCommentId > 0)
                {
                    int approverUserId = GetLoginUserId();
                    ZnodeOMSQuoteApproval quoteApproval = _omsQuoteApproval.Table.FirstOrDefault(x => x.OmsQuoteId == quoteId && x.ApproverUserId == approverUserId);
                    if (IsNotNull(quoteApproval))
                    {
                        quoteApproval.OmsQuoteCommentId = quoteComment?.OmsQuoteCommentId;
                        bool quoteApprovalCommentUpdated = _omsQuoteApproval.Update(quoteApproval);
                        ZnodeLogging.LogMessage(quoteApprovalCommentUpdated ? "Comment has been successfully updated against this approver." : "Failed to update comments for the current user.", string.Empty, TraceLevel.Info);
                    }
                }
            }
        }
        private void BillingMapping(ZnodeOmsOrderDetail order, AddressModel model)
        {
            order.DisplayName = model.DisplayName;
            order.BillingCity = model.CityName;
            order.BillingCompanyName = model.CompanyName;
            order.BillingFirstName = model.FirstName;
            order.BillingLastName = model.LastName;
            order.BillingPhoneNumber = model.PhoneNumber;
            order.BillingPostalCode = model.PostalCode;
            order.BillingStreet1 = model.Address1;
            order.BillingStreet2 = model.Address2;
            order.BillingCountry = model.CountryName;
            order.BillingStateCode = model.StateName;
            order.AddressId = model.AddressId;
        }
        private void ShippingMapping(ZnodeOmsOrderShipment shipment, AddressModel model)
        {
            shipment.ShipToCity = model.CityName;
            shipment.ShipToCompanyName = model.CompanyName;
            shipment.ShipToFirstName = model.FirstName;
            shipment.ShipToLastName = model.LastName;
            shipment.ShipToPhoneNumber = model.PhoneNumber;
            shipment.ShipToPostalCode = model.PostalCode;
            shipment.ShipToStreet1 = model.Address1;
            shipment.ShipToStreet2 = model.Address2;
            shipment.ShipToCountry = model.CountryName;
            shipment.ShipToStateCode = model.StateName;
            shipment.DisplayName = model.DisplayName;
            shipment.AddressId = model.AddressId;
        }
        //Set is IsBilling, IsShipping as per data in addressViewModel and addressType
        private void SetBillingShippingFlags(int AddressId)
        {  //Get entity from address repository where AddressId equals with addressModel.AddressId
            ZnodeAddress addressEntity = _addressRepository.Table?.FirstOrDefault(x => x.AddressId == AddressId);
            addressEntity.IsShipping = true;
            addressEntity.IsBilling = true;

            _addressRepository.Update(addressEntity);

        }

        //Get the Address List based on Order Shipment Address Ids.
        private List<ZnodeAddress> GetOrderShipmentAddressList(List<OrderShipmentModel> orderShipments)
        {
            List<ZnodeAddress> addressesList = null;
            if (orderShipments?.Count > 0)
            {
                List<int> address = orderShipments.Where(y => IsNotNull(y)).Select(x => x.AddressId)?.Distinct()?.ToList();
                if (address?.Count > 0)
                    addressesList = _addressRepository.Table.Where(x => address.Contains(x.AddressId)).ToList();
            }
            return addressesList;
        }

        //Get the Order State to Customer List based on the Order Line Status Ids.
        private List<ZnodeOmsOrderStateShowToCustomer> GetOrderStatusForCustomerList(List<OrderLineItemModel> lineItems)
        {
            List<ZnodeOmsOrderStateShowToCustomer> orderStatusList = null;
            if (lineItems?.Count > 0)
            {
                List<int?> statusList = lineItems.Select(x => x.OrderLineItemStateId)?.Distinct()?.ToList();
                if (statusList?.Count > 0)
                {
                    IZnodeRepository<ZnodeOmsOrderStateShowToCustomer> _orderStateShowToCustomerRepository = new ZnodeRepository<ZnodeOmsOrderStateShowToCustomer>();
                    orderStatusList = _orderStateShowToCustomerRepository.Table.Where(x => statusList.Contains(x.OmsOrderStateId)).ToList();
                }

            }
            return orderStatusList;
        }

        //Get the Order State to Customer List based on the Order Line Status Ids.
        private List<ZnodeOmsOrderShipment> GetOrderShipmentList(List<OrderLineItemModel> lineItems)
        {
            List<ZnodeOmsOrderShipment> orderShipmentList = null;
            if (lineItems?.Count > 0)
            {
                List<int> statusList = lineItems.Select(x => x.OmsOrderShipmentId)?.Distinct()?.ToList();
                if (statusList?.Count > 0)
                    orderShipmentList = _orderShipmentRepository.Table.Where(x => statusList.Contains(x.OmsOrderShipmentId)).ToList();
            }
            return orderShipmentList;
        }

        private List<ZnodeOmsPersonalizeItem> GetPersonalisedValueOrderLineItemList(List<OrderLineItemModel> lineItems)
        {
            List<ZnodeOmsPersonalizeItem> orderPersonlizeItemList = null;
            if (lineItems?.Count > 0)
            {
                List<int?> statusList = lineItems.Select(x => Convert.ToInt32(x.ParentOmsOrderLineItemsId) > 0 ? x.ParentOmsOrderLineItemsId : x.OmsOrderLineItemsId)?.Distinct()?.ToList();
                if (statusList?.Count > 0)
                    orderPersonlizeItemList = new ZnodeRepository<ZnodeOmsPersonalizeItem>().Table.Where(x => statusList.Contains(x.OmsOrderLineItemsId)).ToList();
            }
            return orderPersonlizeItemList;
        }

        private Dictionary<string, object> GetPersonalisedValueOrderLineItem(int orderLineItemId, List<ZnodeOmsPersonalizeItem> personalizeItems)
        {
            Dictionary<string, object> personaliseItem = new Dictionary<string, object>();
            if (orderLineItemId > 0 && personalizeItems?.Count > 0)
            {
                foreach (KeyValuePair<string, string> personaliseAttr in personalizeItems.Where(x => x.OmsOrderLineItemsId == orderLineItemId)?.ToDictionary(x => x.PersonalizeCode, x => x.PersonalizeValue))
                    personaliseItem.Add(personaliseAttr.Key, (object)personaliseAttr.Value);
            }

            return personaliseItem;
        }
        private List<string> GetDownloadableProductKeyList(List<string> lineItemSKU)
        {
            List<string> orderProductKeySKUList = null;
            if (lineItemSKU?.Count > 0)
                orderProductKeySKUList = _pimDownloadableProduct.Table.Where(x => lineItemSKU.Contains(x.SKU)).Select(x => x.SKU).ToList();

            return orderProductKeySKUList;
        }
        private List<ZnodeOmsOrderLineItemsAdditionalCost> GetAdditionalCostList(List<OrderLineItemModel> lineItems)
        {
            List<ZnodeOmsOrderLineItemsAdditionalCost> orderPersonlizeItemList = null;
            if (lineItems?.Count > 0)
            {
                List<int?> statusList = lineItems.Select(x => (int?)x.OmsOrderLineItemsId)?.Distinct()?.ToList();
                if (statusList?.Count > 0)
                    orderPersonlizeItemList = new ZnodeRepository<ZnodeOmsOrderLineItemsAdditionalCost>().Table.Where(x => statusList.Contains(x.OmsOrderLineItemsId)).ToList();
            }
            return orderPersonlizeItemList;
        }

        #endregion
    }
}