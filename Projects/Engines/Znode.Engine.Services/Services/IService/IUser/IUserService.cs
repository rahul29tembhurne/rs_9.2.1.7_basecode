﻿using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services
{
    public interface IUserService
    {
        /// <summary>
        /// This method is used to logged in to the site
        /// </summary>
        /// <param name="portalId">integer Portal ID</param>
        /// <param name="model">AccountModel model</param>
        /// <param name="errorCode">Out param string error code</param>
        /// <param name="expand">Expand collection</param>
        /// <returns>Returns the data for the logged in user.</returns>
        UserModel Login(int portalId, UserModel model, out int? errorCode, NameValueCollection expand);
        /// <summary>
        /// This method is used to change/Reset the password.
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>Returns changed data in AccountModel format</returns>
        UserModel ChangePassword(int portalId, UserModel model);

        /// <summary>
        /// This method will send an email containing password reset link.
        /// </summary>
        /// <param name="accountIds">Parameter Model containing string of Account Ids.</param>
        /// <returns>Returns true/false.</returns>
        bool BulkResetPassword(ParameterModel accountIds);

        /// <summary>
        /// This method will send an email containing password reset link.
        /// </summary>
        /// <param name="portalId">integer Portal ID</param>
        /// <param name="model">AccountModel model</param>
        /// <returns>returns all the account model data</returns>
        UserModel ForgotPassword(int portalId, UserModel model,bool isUserCreateFromAdmin=false,bool isAdminUser=false);

        /// <summary>
        /// This function will verify the Reset Password Link current status.
        /// </summary>
        /// <param name="portalId">integer Portal ID</param>
        /// <param name="model">UserModel.</param>
        /// <returns>Returns Reset Password Link Status i.e. Error codes</returns>
        int? VerifyResetPasswordLinkStatus(int portalId, UserModel model);

        /// <summary>
        /// This method will enable/disable the admin account.
        /// </summary>
        /// <param name="userIds">Account Ids to enable/disable user account.</param> 
        /// <param name="lockUser">To lock or unlock account.</param>
        /// <returns>Returns true if user account deleted successfully, else return false.</returns>
        bool EnableDisableUser(ParameterModel userIds, bool lockUser);

        /// <summary>
        /// This method will Get the accound details based on Username
        /// </summary>
        /// <param name="username">Username to get user account.</param>
        /// <param name="portalId">Portal Id.</param>
        /// <param name="isSocialLogin">Is social login user.</param>
        /// <returns>Returns the Account details in AccountModel format</returns>
        UserModel GetUserByUsername(string username, int portalId, bool isSocialLogin = false);

        /// <summary>
        /// Create Admin User account.
        /// </summary>
        /// <param name="model">User Model</param>
        /// <returns>Returns newly created account model.</returns>
        UserModel CreateAdminUser(UserModel model);

        /// <summary>
        /// Get user account details by account id.
        /// </summary>
        /// <param name="userId">User Id to get user account data.</param>
        /// <param name="expands">Expands collection.</param>
        /// <param name="portalId">portalId.</param>
        /// <returns>Returns AccountModel.</returns>
        UserModel GetUserById(int userId, NameValueCollection expands, int portalId = 0);

        /// <summary>
        /// Update user account details.
        /// </summary>
        /// <param name="accountModel">User model.</param>
        /// <param name="webstoreUser">Flag to identify a webstore user.</param>
        /// <returns>Bool value according the status of update operation.</returns>
        bool UpdateUserData(UserModel userModel, bool webStoreUser);

        /// <summary>
        /// Get user account list.
        /// </summary>
        /// <param name="loggedUserAccountId">loggedUserAccountId</param>
        /// <param name="expands">Expands for Account.</param>
        /// <param name="filters">Filters for Account.</param>
        /// <param name="sorts">Sorts for Account.</param>
        /// <param name="page">Page.</param>
        /// <returns>Returns AccountListModel.</returns>
        UserListModel GetUserList(int loggedUserAccountId, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Delete user account.
        /// </summary>
        /// <param name="accountId">Account Ids to be deleted.</param>
        /// <returns>Returns true if record deleted successfully, else return false.</returns>
        bool DeleteUser(ParameterModel accountIds);

        /// <summary>
        /// Create customer account.
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="model">User Model</param>
        /// <returns>Returns created customer account.</returns>
        UserModel CreateCustomer(int portalId, UserModel model);

        /// <summary>
        /// Update customer account.
        /// </summary>
        /// <param name="userModel">User Model.</param>
        /// <returns>Returns true if record updated sucessfully, else return false.</returns>
        bool UpdateCustomer(UserModel userModel);

        /// <summary>
        /// Gets the assigned portals to user.
        /// </summary>
        /// <param name="aspNetUserId">User id.</param>
        /// <returns>Returns assigned portals to user.</returns>
        UserPortalModel GetPortalIds(string aspNetUserId);

        /// <summary>
        /// Save portal ids againt the user.
        /// </summary>
        /// <param name="userPortalModel">Model to save portal ids againt the user.</param>
        /// <returns>Returns true if saved successfully.</returns>
        bool SavePortalsIds(UserPortalModel userPortalModel);

        /// <summary>
        /// Sign up for news letter.
        /// </summary>
        /// <param name="model">NewsLetterSignUpModel containing email address.</param>
        /// <returns>Returns true if email successfully subscribed for news letter else false.</returns>
        bool SignUpForNewsLetter(NewsLetterSignUpModel model);

        /// <summary>
        /// Check default admin password reset.
        /// </summary>        
        /// <returns>true/false</returns>
        bool IsDefaultAdminPasswordReset();

        #region Social Login
        /// <summary>
        /// Login to the 3rd party like facebook, google etc.
        /// </summary>
        /// <param name="model">SocialLoginModel</param>
        /// <returns>Returns SocialLoginModel.</returns>
        UserModel SocialLogin(SocialLoginModel model);

        /// <summary>
        /// Get the social login providers.
        /// </summary>
        /// <returns>Returns list of social login providers.</returns>
        List<SocialDomainModel> GetLoginProviders();
        #endregion

        /// <summary>
        /// Get user access permission list
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Returns user access permission code</returns>
        UserModel GetUserAccessPermission(int userId);

        /// <summary>
        /// Get customer profile
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="portalId">portalId</param>
        /// <returns>Returns customer profile by particular userID</returns>
        List<ProfileModel> GetCustomerProfile(int userId, int portalId);

        /// <summary>
        /// This method will convert shoper to admin.
        /// </summary>
        /// <param name="portalId">integer Portal ID</param>
        /// <param name="model">AccountModel model</param>
        /// <returns>returns all the account model data</returns>
        UserModel ConvertShopperToAdmin(UserModel model);
    }
}
