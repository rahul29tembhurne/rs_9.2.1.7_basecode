﻿using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services
{
    public interface IGlobalMessageService
    {
        #region Manage ManageMessages 
        /// <summary>
        /// Publish the global message
        /// </summary>
        /// <param name="cmsMessageKeyId"></param>
        /// <param name="portalId"></param>
        /// <param name="localeId"></param>
        /// <param name="targetPublishState"></param>
        /// <param name="takeFromDraftFirst"></param>
        /// <returns>boolean value true/false</returns>
        PublishedModel PublishManageMessage(string cmsMessageKeyId, int localeId = 0, string targetPublishState = null, bool takeFromDraftFirst = false);
                
        #endregion
    }
}
