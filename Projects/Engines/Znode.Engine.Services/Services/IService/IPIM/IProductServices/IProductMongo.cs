﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Engine.Services
{
    public interface IProductMongo
    {
        /// <summary>
        ///  Method Gets all publish products satisfying the query
        /// </summary>
        /// <param name="query">Query to get products</param>
        /// <returns>ProductEntity List</returns>
        List<ProductEntity> GetPublishProducts(IMongoQuery query);

        /// <summary>
        ///  Method Gets Publish Product by publish Category Id and locale Id 
        /// </summary>
        /// <param name="categoryId">Publish Category Id to get Publish Product</param>
        /// <param name="localeId">Locale Id to get Publish Product</param>
        /// <returns></returns>
        List<ProductEntity> GetProductsByCategoryId(int categoryId, int localeId);

        /// <summary>
        ///  Method Gets Publish Product Details
        /// </summary>
        /// <param name="productId">Product Id to get Product</param>
        /// <param name="localeId">Locale Id to get locale specific Product</param>
        /// <returns>ProductEntity</returns>
        ProductEntity GetProduct(int productId, int localeId, int? catalogVersionId = null);

        /// <summary>
        /// Get Publish Product Name by ProductId and Locale Id
        /// </summary>
        /// <param name="productId">Product Id to get Product</param>
        /// <param name="localeId">Locale Id to get locale specific Product</param>
        /// <returns>Publish product Name</returns>
        string GetProductName(int productId, int localeId);
    }
}
