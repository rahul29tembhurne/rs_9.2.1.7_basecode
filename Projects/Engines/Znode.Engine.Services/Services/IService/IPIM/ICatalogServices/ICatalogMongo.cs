﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Engine.Services
{
    public interface ICatalogMongo
    {
        /// <summary>
        ///  Method Gets Publish catalog by publish catalog Id
        /// </summary>
        /// <param name="catalogId">Publish Catalog Id to get Publish Catalog</param>
        /// <returns>CatalogEntity</returns>
        CatalogEntity GetCatalogByPublishId(int catalogId);
    }
}
