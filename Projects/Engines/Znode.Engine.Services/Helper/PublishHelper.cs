﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.Observer;
using Znode.Libraries.Search;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Engine.Services
{
    public static class PublishHelper
    {
        /// <summary>
        /// Save Product in Mongo
        /// </summary>
        /// <param name="dataSet">dataset</param>
        public static List<ProductEntity> SaveProductInMongo(IEnumerable<string> productXmlList, string revisionType, bool isTempVersion = false, long publishStartTime = 0)
        {
            List<ProductEntity> productEntities = new List<ProductEntity>();
            List<ProductEntity> productEntityList = ConvertListOfXMLStringToListModelForProducts<ProductEntity>(productXmlList);

            foreach (ProductEntity productEntity in productEntityList)
            {
                if (IsNotNull(productEntity))
                {
                    productEntity.ProfileIds = !string.IsNullOrEmpty(productEntity.TempProfileIds) ? ((productEntity.TempProfileIds)?.Split(',').Select<string, int>(int.Parse)?.ToArray()) : null;
                    productEntity.IndexId = $"{productEntity.ZnodeProductId}{productEntity.ZnodeCategoryIds}{productEntity.ZnodeCatalogId}{productEntity.LocaleId}";
                    productEntity.Name = productEntity.Name.Contains("&amp;") ? productEntity.Name.Replace("&amp;", "&") : productEntity.Name;
                    productEntity.revisionType = revisionType;

                    //Nivi Code
                    //if (Convert.ToBoolean(productEntity.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.CallForPricing)?.AttributeValues))
                    //{
                    //    productEntity.SalesPrice = null;
                    //    productEntity.RetailPrice = null;
                    //}

                    //Bind temp version id only in the case of category publish from catalog.
                    if (isTempVersion)
                    {
                        productEntity.VersionId = ZnodeConstant.TemporaryVersionId;
                        productEntity.PublishStartTime = publishStartTime;
                    }
                    productEntities.Add(productEntity);
                }
            }

            if (productEntities?.Count > 0)
                new MongoRepository<ProductEntity>().Create(productEntities);

            return productEntities;
        }

        // Save products in mongo with temporary version.
        public static List<ProductEntity> SaveProductInMongoWithTempVersion(IEnumerable<string> productXmlList, string revisionType, bool isTempVersion = false, long publishStartTime = 0)
          =>  SaveProductInMongo(productXmlList, revisionType, isTempVersion, publishStartTime);


        /// <summary>
        /// Delete product from mongo db
        /// </summary>
        /// <param name="productXmlList">List of product xml</param>
        /// <returns></returns>
        public static bool RemoveProductFromMongo(int productId) 
            => (new MongoRepository<ProductEntity>()).DeleteByQuery(Query.And(Query<ProductEntity>.Where(ga => ga.ZnodeProductId.Equals(productId))));

        
        public static string GetIndexName(string catalogName)
        {
            if (!string.IsNullOrEmpty(catalogName))
            {
                catalogName = RegxQuery(catalogName);
                return $"{catalogName.ToLower()}index";
            }
            return string.Empty;
        }

        private static string RegxQuery(string catalogName)
        {
            catalogName = Regex.Replace(catalogName, @"\s+", ""); // Regx to remove spaces 
            catalogName = Regex.Replace(catalogName, @"[^a-zA-Z0-9_]{1,255}\+", string.Empty); //Regx to have alphabet and numbers upto limit 255.
            catalogName = Regex.Replace(catalogName, @"[?!^#\/*?<>..|.]", string.Empty);//Regx to remove the special character that are mentioned .
            catalogName = Regex.Replace(catalogName, @"(?i)([?!^(+_)])", string.Empty); //Regex to remove the special character if they are at starting postion .
            catalogName = catalogName.StartsWith("-") ? catalogName.Replace('-', ' ') : catalogName; //To remove '-'.
            return catalogName;
        }

        public static string GetIndexName(int publishCatalogId)
        {
            if (publishCatalogId > 0)
            {
                ISearchService searchService = GetService<ISearchService>();
                PortalIndexModel portalIndex = searchService.GetCatalogIndexData(null, new FilterCollection() { new FilterTuple(ZnodeCatalogIndexEnum.PublishCatalogId.ToString(), FilterOperators.Equals, publishCatalogId.ToString()) });
                if (portalIndex?.CatalogIndexId > 0)
                    return portalIndex.IndexName;
                else
                {
                    IZnodeRepository<ZnodePublishCatalog> _publishCatalogRepository = new ZnodeRepository<ZnodePublishCatalog>();
                    return GetIndexName(_publishCatalogRepository.GetById(publishCatalogId)?.CatalogName);
                }
            }
            return string.Empty;
        }

       

        //Get Locale Name by LocaleId.
        public static string GetLocaleById(int localeId)
         => new ZnodeRepository<ZnodeLocale>().Table.FirstOrDefault(x => x.LocaleId == localeId && x.IsActive)?.Name;

        //Save category in mongo db.
        public static List<CategoryEntity> SaveCategoryInMongo(List<CategoryEntity> categoryEntities, long publishStartTime = 0, bool isTempVersion = false)
        {
            foreach (CategoryEntity category in categoryEntities)
            {
                if (IsNotNull(category))
                {
                    category.ZnodeParentCategoryIds = !string.IsNullOrEmpty(category.TempZnodeParentCategoryIds) ? ((category.TempZnodeParentCategoryIds)?.Split(',').Select<string, int>(int.Parse)?.ToArray()) : null;
                    category.ProductIds = !string.IsNullOrEmpty(category.TempProductIds) ? ((category.TempProductIds)?.Split(',').Select<string, int>(int.Parse)?.ToArray()) : null;
                    category.ProfileIds = !string.IsNullOrEmpty(category.TempProfileIds) ? ((category.TempProfileIds)?.Split(',').Select<string, int>(int.Parse)?.ToArray()) : null;
                    category.Id = ObjectId.GenerateNewId();

                    if (isTempVersion)
                    {
                        category.VersionId = ZnodeConstant.TemporaryVersionId;
                        category.PublishStartTime = publishStartTime;
                    }
                }
            }

            if (categoryEntities?.Count > 0)
              new MongoRepository<CategoryEntity>().Create(categoryEntities);

            return categoryEntities;
        }

        //Save category in mongo db for temp version.
        public static List<CategoryEntity> SaveCategoryInMongoForTempVersion(List<CategoryEntity> categoryEntities, long publishStartTime = 0, bool isTempVersion = false)
           => SaveCategoryInMongo(categoryEntities, publishStartTime, isTempVersion);

        /// <summary>
        /// Publish Associated Products like (Addons, Group Products,Bundle Products and Configurable Products) Configuration
        /// </summary>
        /// <param name="id">Id (catalog/Product) </param>
        /// <param name="parameterName">parameter Name</param>
        /// <param name="userId">User Id</param>
        /// <param name="versionId">Version Id</param>
        public static void PublishAssociatedProductsConfiguration(int id, string parameterName, int userId, ZnodePublishStatesEnum ZnodePublishState, List<int> localeIds = null)
        {
            //Publish Addons for multiple products
            PublishAddons(id, parameterName, userId, ZnodePublishState, localeIds);

            //Publish Group Products for multiple products 
            PublishGroupProducts(id, parameterName, userId, ZnodePublishState);

            //Publish Bundle Products for multiple products
            PublishBundleProducts(id, parameterName, userId, ZnodePublishState);

            //Publish Configurable Products for multiple products
            PublishConfigurableProducts(id, parameterName, userId, ZnodePublishState);
        }

        /// <summary>
        /// Convert Dataset to Entity and save in Mongo
        /// </summary>
        /// <typeparam name="T">type of mongo Entity</typeparam>
        /// <param name="resultDataSet">dataset</param>
        /// <param name="mongoRepository">Mongorepository object</param>
        /// <param name="versionId">VersionId</param>
        public static void SaveToMongo<T>(DataSet resultDataSet, IMongoRepository<T> mongoRepository, int versionId) where T : MongoEntity
        {
            DataTable dataTable = resultDataSet.Tables[0];
            List<T> entites = new List<T>();

            //Convert Dataset to entity
            foreach (DataRow row in dataTable.Rows)
                entites.Add(ConvertXMLStringToModel<T>(Convert.ToString(row["ReturnXML"])));

            if (entites?.Count > 0)
                mongoRepository.Create(entites.Select(e => { e.VersionId = versionId; return e; }).ToList());
        }

        /// <summary>
        /// Execute SP and get result
        /// </summary>
        /// <param name="id">Id (catalog/Product)</param>
        /// <param name="parameterName1">parameter Name</param>
        /// <param name="userId">User Id</param>
        /// <param name="spName">Name of SP</param>
        /// <param name="versionId">Version Id</param>
        /// <param name="parameterName2">Parameter name</param>
        /// <param name="associategProductTypeName">Product Type</param>
        /// <returns>Dataset</returns>
        public static DataSet ExecuteSP(int id, string parameterName1, int userId, string spName, int? versionId = null, string parameterName2 = "", string associategProductTypeName = "", List<int> publishProductIds = null)
        {
            if (IsNull(publishProductIds))
                publishProductIds = new List<int>() { id };

            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter(parameterName1, id, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);
            
            if (versionId > 0)
                executeSpHelper.GetParameter("@VersionId", versionId, ParameterDirection.Input, SqlDbType.Int);

            if (!string.IsNullOrEmpty(parameterName2) && !string.IsNullOrEmpty(associategProductTypeName))
                executeSpHelper.GetParameter(parameterName2, associategProductTypeName, ParameterDirection.Input, SqlDbType.Text);
            return executeSpHelper.GetSPResultInDataSet(spName);
        }

        /// <summary>
        /// Execute SP and get result
        /// </summary>
        /// <param name="id">Id (catalog/Product)</param>
        /// <param name="parameterName1">parameter Name</param>
        /// <param name="userId">User Id</param>
        /// <param name="spName">Name of SP</param>
        /// <param name="versionId">Version Id</param>
        /// <param name="parameterName2">Parameter name</param>
        /// <param name="associategProductTypeName">Product Type</param>
        /// <param name="publishProductIds">Publish Product Ids</param>
        /// <returns>Dataset</returns>
        public static DataSet ExecuteSPWithDataTableParameter(int id, string parameterName1, int userId, string spName, ZnodePublishStatesEnum revisionType, string parameterName2 = "", string associategProductTypeName = "", List<int> LocaleIds = null)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.SetTableValueParameter(parameterName1, ConvertKeywordListToDataTable(new List<int>() { id }), ParameterDirection.Input, SqlDbType.Structured, "dbo.TransferId");

            if (LocaleIds?.Count > 0)
                executeSpHelper.SetTableValueParameter("@LocaleId", ConvertKeywordListToDataTable(LocaleIds), ParameterDirection.Input, SqlDbType.Structured, "dbo.TransferId");

            executeSpHelper.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);

            executeSpHelper.GetParameter("@PublishStateId", (byte)revisionType, ParameterDirection.Input, SqlDbType.Int);

            if (!string.IsNullOrEmpty(parameterName2) && !string.IsNullOrEmpty(associategProductTypeName))
                executeSpHelper.GetParameter(parameterName2, associategProductTypeName, ParameterDirection.Input, SqlDbType.Text);
            DataSet ds = executeSpHelper.GetSPResultInDataSet(spName);

            return ds;
        }

        /// <summary>
        /// Execute SP with Json parameters and get result
        /// </summary>
        /// <param name="id">Id (catalog/Product)</param>
        /// <param name="parameterName1">parameter Name</param>
        /// <param name="userId">User Id</param>
        /// <param name="spName">Name of SP</param>
        /// <param name="versionId">Version Id</param>
        /// <param name="parameterName2">Parameter name</param>
        /// <param name="associategProductTypeName">Product Type</param>
        /// <param name="publishProductIds">Publish Product Ids</param>
        /// <returns>Dataset</returns>
        public static DataSet ExecuteSPWithJSONParameter(int id, string parameterName1, int userId, string spName, ZnodePublishStatesEnum revisionType, string parameterName2 = "", string associategProductTypeName = "", List<int> LocaleIds = null)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter(parameterName1, ConvertKeywordListToDataTable(new List<int>() { id })?.ToJson(), ParameterDirection.Input, SqlDbType.NVarChar);

            if (LocaleIds?.Count > 0)
                executeSpHelper.GetParameter("@LocaleId", ConvertKeywordListToDataTable(LocaleIds)?.ToJson(), ParameterDirection.Input, SqlDbType.NVarChar);

            executeSpHelper.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);

            executeSpHelper.GetParameter("@PublishStateId", (byte)revisionType, ParameterDirection.Input, SqlDbType.Int);

            if (!string.IsNullOrEmpty(parameterName2) && !string.IsNullOrEmpty(associategProductTypeName))
                executeSpHelper.GetParameter(parameterName2, associategProductTypeName, ParameterDirection.Input, SqlDbType.Text);
            DataSet ds = executeSpHelper.GetSPResultInDataSet(spName);

            return ds;
        }

        /// <summary>
        /// Execute SP and get result
        /// </summary>
        /// <param name="id">Id (catalog/Product)</param>
        /// <param name="parameterName1">parameter Name</param>
        /// <param name="userId">User Id</param>
        /// <param name="spName">Name of SP</param>
        /// <param name="versionId">Version Id</param>
        /// <param name="parameterName2">Parameter name</param>
        /// <param name="associategProductTypeName">Product Type</param>
        /// <returns>Dataset</returns>
        public static DataSet ExecuteSP(int id, string parameterName1, int userId, string spName, int? versionId = null, string parameterName2 = "", int associategProductTypeName = 0, string parameterName3 = "", int localeId = 0)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter(parameterName1, id, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);

            if (versionId > 0)
                executeSpHelper.GetParameter("@VersionId", versionId, ParameterDirection.Input, SqlDbType.Int);

            if (!string.IsNullOrEmpty(parameterName2) && associategProductTypeName > 0)
                executeSpHelper.GetParameter(parameterName2, associategProductTypeName, ParameterDirection.Input, SqlDbType.Int);
            if (!string.IsNullOrEmpty(parameterName3) && localeId > 0)
                executeSpHelper.GetParameter(parameterName3, localeId, ParameterDirection.Input, SqlDbType.Int);
            return executeSpHelper.GetSPResultInDataSet(spName);
        }

        //Add Filter for Activation and Expiration date of category.  
        public static IMongoQuery AddFilterForDate(IMongoQuery query)
        {
            List<IMongoQuery> query1 = new List<IMongoQuery>();
            query1.Add(Query<CategoryEntity>.LTE(d => d.ActivationDate, GetDateTime()));
            query1.Add(Query<CategoryEntity>.EQ(d => d.ActivationDate, null));

            List<IMongoQuery> query2 = new List<IMongoQuery>();
            query2.Add(Query<CategoryEntity>.GTE(d => d.ExpirationDate, GetDateTime()));
            query2.Add(Query<CategoryEntity>.EQ(d => d.ExpirationDate, null));

            IMongoQuery query3 = Query.And(query, Query.Or(query1), Query.Or(query2));
            return query3;
        }

        //Add Filter for Activation and Expiration date of content page.  
        public static IMongoQuery DateFilterForContentPage(IMongoQuery query)
        {
            List<IMongoQuery> query1 = new List<IMongoQuery>();
            query1.Add(Query<CategoryEntity>.LTE(d => d.ActivationDate, GetDateTime()));
            query1.Add(Query<CategoryEntity>.EQ(d => d.ActivationDate, null));

            List<IMongoQuery> query2 = new List<IMongoQuery>();
            query2.Add(Query<CategoryEntity>.GTE(d => d.ExpirationDate, GetDateTime()));
            query2.Add(Query<CategoryEntity>.EQ(d => d.ExpirationDate, null));

            IMongoQuery query3 = Query.Or(query, Query.Or(query1), Query.Or(query2));
            return query3;
        }

        //Publish Addons for multiple products
        private static void PublishAddons(int id, string parameterName, int userId, ZnodePublishStatesEnum revisionType, List<int> localeIds)
        {
            if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
                SaveAssociatedProductsToMongo<AddonEntity>(ExecuteSPWithJSONParameter(id, parameterName, userId, "Znode_GetPublishAssociatedAddonsWithJSON", revisionType, "", "", localeIds), new MongoRepository<AddonEntity>());
            else
                SaveAssociatedProductsToMongo<AddonEntity>(ExecuteSPWithDataTableParameter(id, parameterName, userId, "Znode_GetPublishAssociatedAddons", revisionType, "", "", localeIds), new MongoRepository<AddonEntity>());
        }

        //Publish Group Products for multiple products
        private static void PublishGroupProducts(int id, string parameterName, int userId, ZnodePublishStatesEnum revisionType)
        {
            if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
                SaveAssociatedProductsToMongo<GroupProductEntity>(ExecuteSPWithJSONParameter(id, parameterName, userId, "Znode_GetPublishAssociatedProductsWithJSON", revisionType, "@ProductType", "GroupedProduct"), new MongoRepository<GroupProductEntity>());
            else
                SaveAssociatedProductsToMongo<GroupProductEntity>(ExecuteSPWithDataTableParameter(id, parameterName, userId, "Znode_GetPublishAssociatedProducts", revisionType, "@ProductType", "GroupedProduct"), new MongoRepository<GroupProductEntity>());
        }

        //Publish Bundle Products for multiple products
        private static void PublishBundleProducts(int id, string parameterName, int userId, ZnodePublishStatesEnum revisionType)
        {
            if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
                SaveAssociatedProductsToMongo<BundleProductEntity>(ExecuteSPWithJSONParameter(id, parameterName, userId, "Znode_GetPublishAssociatedProductsWithJSON", revisionType, "@ProductType", "BundleProduct"), new MongoRepository<BundleProductEntity>());
            else
                SaveAssociatedProductsToMongo<BundleProductEntity>(ExecuteSPWithDataTableParameter(id, parameterName, userId, "Znode_GetPublishAssociatedProducts", revisionType, "@ProductType", "BundleProduct"), new MongoRepository<BundleProductEntity>());
        }

        //Publish Configurable Products for multiple products
        private static void PublishConfigurableProducts(int id, string parameterName, int userId, ZnodePublishStatesEnum revisionType)
        {
            if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
                SaveAssociatedProductsToMongo<ConfigurableProductEntity>(ExecuteSPWithJSONParameter(id, parameterName, userId, "Znode_GetPublishAssociatedProductsWithJSON", revisionType, "@ProductType", "ConfigurableProduct"), new MongoRepository<ConfigurableProductEntity>());
            else
                SaveAssociatedProductsToMongo<ConfigurableProductEntity>(ExecuteSPWithDataTableParameter(id, parameterName, userId, "Znode_GetPublishAssociatedProducts", revisionType, "@ProductType", "ConfigurableProduct"), new MongoRepository<ConfigurableProductEntity>());
        }


        /// <summary>
        /// This is overload method for publish Associated Products like (Addons, Group Products,Bundle Products and Configurable Products) Configuration
        /// </summary>
        /// <param name="id">Id (catalog) </param>
        /// <param name="parameterName">parameter Name</param>
        /// <param name="id2">Id2 (category) </param>
        /// <param name="parameterName2">parameter Name</param>
        /// <param name="userId">User Id</param>
        /// <param name="versionId">Version Id</param>
        /// <param name="publishStartTime">Publish start time</param>
        public static void PublishAssociatedProductsConfiguration(int id, string parameterName, int id2, string parameterName2, int userId, int? versionId = null, ZnodePublishStatesEnum revisionType = ZnodePublishStatesEnum.PREVIEW, long publishStartTime = 0)
        {
            Parallel.Invoke(new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },
                   //Publish Addons
                   () => { PublishAddons(id, parameterName, id2, parameterName2, userId, versionId, revisionType, publishStartTime, true); },
                   
                   //Publish Group Products 
                   () => { PublishGroupProducts(id, parameterName, id2, parameterName2, userId, versionId, revisionType, publishStartTime, true); },
                   
                   //Publish Bundle Products
                   () => { PublishBundleProducts(id, parameterName, id2, parameterName2, userId, versionId, revisionType, publishStartTime, true); },
                   
                   //Publish Configurable Products
                   () => { PublishConfigurableProducts(id, parameterName, id2, parameterName2, userId, versionId, revisionType, publishStartTime, true); });
        }

        //Following are the overload method for publish Associated Products like (Addons, Group Products,Bundle Products and Configurable Products) Configuration
        //Publish Addons
        private static void PublishAddons(int id, string parameterName, int id2, string parameterName2, int userId, int? versionId = null, ZnodePublishStatesEnum revisionType = ZnodePublishStatesEnum.PREVIEW, long publishStartTime = 0, bool isTempVersion = false)
           => SaveAssociatedCatalogCategoryProductsToMongo<AddonEntity>(ExecuteSP(id, parameterName, id2, parameterName2, userId, "Znode_GetPublishAssociatedAddons", versionId, null, null, revisionType), new MongoRepository<AddonEntity>(), publishStartTime, isTempVersion ? ZnodeConstant.TemporaryVersionId : Convert.ToInt32(versionId));

        //Publish Group Products 
        private static void PublishGroupProducts(int id, string parameterName, int id2, string parameterName2, int userId, int? versionId = null, ZnodePublishStatesEnum revisionType = ZnodePublishStatesEnum.PREVIEW, long publishStartTime = 0, bool isTempVersion = false)
           => SaveAssociatedCatalogCategoryProductsToMongo<GroupProductEntity>(ExecuteSP(id, parameterName, id2, parameterName2, userId, "Znode_GetPublishAssociatedProducts", versionId, "@ProductType", "GroupedProduct", revisionType), new MongoRepository<GroupProductEntity>(), publishStartTime, isTempVersion ? ZnodeConstant.TemporaryVersionId : Convert.ToInt32(versionId));

        //Publish Bundle Products
        private static void PublishBundleProducts(int id, string parameterName, int id2, string parameterName2, int userId, int? versionId = null, ZnodePublishStatesEnum revisionType = ZnodePublishStatesEnum.PREVIEW, long publishStartTime = 0, bool isTempVersion = false)
           => SaveAssociatedCatalogCategoryProductsToMongo<BundleProductEntity>(ExecuteSP(id, parameterName, id2, parameterName2, userId, "Znode_GetPublishAssociatedProducts", versionId, "@ProductType", "BundleProduct", revisionType), new MongoRepository<BundleProductEntity>(), publishStartTime, isTempVersion ? ZnodeConstant.TemporaryVersionId : Convert.ToInt32(versionId));

        //Publish Configurable Products
        private static void PublishConfigurableProducts(int id, string parameterName, int id2, string parameterName2, int userId, int? versionId = null, ZnodePublishStatesEnum revisionType = ZnodePublishStatesEnum.PREVIEW, long publishStartTime = 0, bool isTempVersion = false)
           => SaveAssociatedCatalogCategoryProductsToMongo<ConfigurableProductEntity>(ExecuteSP(id, parameterName, id2, parameterName2, userId, "Znode_GetPublishAssociatedProducts", versionId, "@ProductType", "ConfigurableProduct", revisionType), new MongoRepository<ConfigurableProductEntity>(), publishStartTime, isTempVersion ? ZnodeConstant.TemporaryVersionId : Convert.ToInt32(versionId));

        public static DataSet ExecuteSP(int id, string parameterName, int id2, string parameterName2, int userId, string spName, int? versionId = null, string parameterName3 = "", string associategProductTypeName = "", ZnodePublishStatesEnum revisionType = ZnodePublishStatesEnum.PREVIEW)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter(parameterName, id, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter(parameterName2, id2, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@PublishStateId", (byte)revisionType, ParameterDirection.Input, SqlDbType.Int);

            if (versionId > 0)
                executeSpHelper.GetParameter("@VersionId", versionId, ParameterDirection.Input, SqlDbType.Int);

            if (!string.IsNullOrEmpty(parameterName3) && !string.IsNullOrEmpty(associategProductTypeName))
                executeSpHelper.GetParameter(parameterName3, associategProductTypeName, ParameterDirection.Input, SqlDbType.Text);
            return executeSpHelper.GetSPResultInDataSet(spName);
        }

        // Convert Dataset to Entity and save in Mongo
        private static void SaveAssociatedProductsToMongo<T>(DataSet resultDataSet, IMongoRepository<T> mongoRepository) where T : AssociatedProductEntity
        {
            DataTable dataTable = resultDataSet.Tables[0];
            List<T> entites = new List<T>();

            //Convert Dataset to entity
            foreach (DataRow row in dataTable.Rows)
            {
                entites.Add(ConvertXMLStringToModel<T>(Convert.ToString(row["ReturnXML"])));
            }

            if (entites?.Count > 0)
                mongoRepository.Create(entites);
        }

        // Convert Dataset to Entity and save in Mongo
        private static void SaveAssociatedCatalogCategoryProductsToMongo<T>(DataSet resultDataSet, IMongoRepository<T> mongoRepository, long publishStartTime = 0, int versionId = 0) where T : AssociatedProductEntity
        {
            DataTable dataTable = resultDataSet.Tables[0];
            List<T> entites = new List<T>();

            if (!dataTable.Columns.Contains("ReturnXML"))
                return;

                //Convert Dataset to entity
            foreach (DataRow row in dataTable.Rows)
                entites.Add(ConvertXMLStringToModel<T>(Convert.ToString(row["ReturnXML"])));

            if (entites?.Count > 0)
            {
                entites.ForEach(x => { x.VersionId = versionId; x.PublishStartTime = publishStartTime; });
                mongoRepository.Create(entites);
            }
        }

        public static void SaveInSearch(List<ProductEntity> productEntities, string revisionType)
        {
            if (productEntities?.Count > 0)
            {
                ISearchService searchService = GetService<ISearchService>();             

                List<ProductEntity> productEntity = productEntities.GroupBy(x => new { x.ZnodeCatalogId, x.LocaleId }).Select(x => x.FirstOrDefault()).ToList();
                string indexName = string.Empty;
                ProductAttributeMapper.MergeSimpleProductAttributes(productEntity);
                productEntities.ForEach(x => x.revisionType = revisionType);
                foreach (var item in productEntity)
                {
                    IEnumerable<object> productIds = productEntities.Where(x => x.ZnodeCatalogId == item.ZnodeCatalogId).Select(x => x.ZnodeProductId).Distinct().Cast<object>();
                    indexName = GetIndexName(item.ZnodeCatalogId);
                    searchService.DeleteProduct(indexName, productIds, revisionType, Convert.ToString(item.VersionId));
                    searchService.CreateProduct(indexName, productEntities.FindAll(x => x.ZnodeCatalogId == item.ZnodeCatalogId && x.ZnodeCategoryIds > 0 && x.LocaleId == item.LocaleId));
                }
            }
        }
     
        public static bool SaveInSearchForCategoryName(List<ProductEntity> productEntities)
        {
            bool searchStatus = false;
            if (productEntities?.Count > 0)
            {
                ISearchService searchService = GetService<ISearchService>();
                IEnumerable<ProductEntity> catalogs = productEntities.GroupBy(x => x.ZnodeCatalogId).Select(x => x.FirstOrDefault());
                string indexName = string.Empty;
                foreach (var item in catalogs)
                {
                    indexName = GetIndexName(item.ZnodeCatalogId);
                    searchService.DeleteCategoryForGivenIndex(indexName, item.ZnodeCategoryIds);
                    searchService.CreateProduct(indexName, productEntities.FindAll(x => x.ZnodeCatalogId == item.ZnodeCatalogId && x.ZnodeCategoryIds > 0));
                }
                searchStatus = true;

                //Clear cache call
                var clearCacheInitializer = new ZnodeEventNotifier<IEnumerable<ProductEntity>>(catalogs);
            }
            return searchStatus;
        }

        public static bool SaveInSearchForCatalogCategoryProducts(List<ProductEntity> productEntities, int publishCatalogId, List<int> publishCategoryIds, string revisionType)
        {
            bool searchStatus = false;
            if (productEntities?.Count > 0)
            {
                ISearchService searchService = GetService<ISearchService>();
                string indexName = string.Empty;
                indexName = GetIndexName(publishCatalogId);

                productEntities.ForEach(x => x.revisionType = revisionType);

                searchStatus = searchService.CreateIndexForCategoryProducts(new CategoryProductSearchParameterModel {
                    IndexName = indexName,
                    CatalogId = publishCatalogId,
                    RevisionType = revisionType,
                    PublishCategoryIds=publishCategoryIds,
                    VersionId = productEntities.Any() ? productEntities.FirstOrDefault().VersionId : 0,
                    ProductEntities = productEntities.FindAll(x => x.ZnodeCatalogId == publishCatalogId && x.ZnodeCategoryIds > 0)});
            }
            return searchStatus;
        }
        public static void SaveToMongo<T>(DataSet resultDataSet, IMongoRepository<T> mongoRepository) where T : MongoEntity
        {
            DataTable dataTable = resultDataSet.Tables[0];
            List<T> entites = new List<T>();
            ZnodeRepository<ZnodePublishPortalLog> publishPortalLogRepository = new ZnodeRepository<ZnodePublishPortalLog>();
            //Convert Dataset to entity
            foreach (DataRow row in dataTable.Rows)
            {
                T entity = ConvertXMLStringToModel<T>(Convert.ToString(row["ReturnXML"]));
                int mappingId = Convert.ToInt32(entity.GetDynamicProperty("MappingId"));
                int? versionId = publishPortalLogRepository.Table.OrderByDescending(x => x.PublishPortalLogId).FirstOrDefault(x => x.IsPortalPublished == true && x.PortalId == mappingId)?.PublishPortalLogId;
                if (IsNotNull(versionId))
                {
                    entity.VersionId = versionId.Value;
                    entites.Add(entity);
                }
            }
            if (entites?.Count > 0)
                mongoRepository.Create(entites);
        }

        //Converts Searchable Attributes List to Data Table
        public static DataTable ConvertKeywordListToDataTable(List<int> publishProductIds)
        {
            DataTable table = new DataTable("@PimProductId");
            table.Columns.Add("Id", typeof(int));

            foreach (int model in publishProductIds)
                table.Rows.Add(model);
            return table;
        }

        public static void RemoveInSearch(List<ProductEntity> productEntities)
        {
            if (productEntities?.Count > 0)
            {
                ISearchService searchService = GetService<ISearchService>();
                IEnumerable<ProductEntity> catalogs = productEntities.GroupBy(x => x.ZnodeCatalogId).Select(x => x.FirstOrDefault());
                string indexName = string.Empty;
                foreach (var item in catalogs)
                {
                    indexName = GetIndexName(item.ZnodeCatalogId);
                    searchService.DeleteProduct(indexName, item.SKU, ZnodePublishStatesEnum.PREVIEW.ToString());
                }
            }
        }
    }
}