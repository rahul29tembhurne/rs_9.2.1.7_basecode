﻿using System;
using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Libraries.Observer;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.MongoDB.Data;
using System.Linq;
using static Znode.Engine.Services.Helper.ClearCacheHelper;
using System.Web;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Services
{
    public class ZnodeEventObserver
    {
        #region Private Variables
        Connector<MessageEntity> myMessageToken;
        Connector<IEnumerable<ProductEntity>> productToken;
        Connector<List<int>> categoryToken;
        Connector<List<WidgetSliderBannerEntity>> sliderToken;
        Connector<ContentPageConfigEntity> pagesToken;
        Connector<DefaultGlobalConfigListModel> cacheKey;
        Connector<PortalModel> webstorePortalKey;
        readonly EventAggregator eventAggregator;
        Connector<SeoEntity> mySEOToken;
        Connector<ZnodePortalCountry> myZnodePortalCountryToken;
        Connector<ZnodePromotionCoupon> myZnodePromotionCouponToken;
        Connector<PriceSKUModel> myPriceSKUModelToken;
        Connector<InventorySKUModel> myInventorySKUModelToken;
        Connector<PromotionModel> myPromotionModelToken;
        #endregion

        #region Constructor
        public ZnodeEventObserver(EventAggregator eve)
        {
            eventAggregator = eve;

            eve.Attach<MessageEntity>(this.OnPublishContent); //1
            eve.Attach <GlobalMessageEntity>(this.OnPublishGlobalContent);
            eve.Attach<IEnumerable<ProductEntity>>(this.OnPublishProduct); //1
            eve.Attach<List<int>>(this.OnPublishCategory);//1
            eve.Attach<List<WidgetSliderBannerEntity>>(this.OnPublishSlider);//1
            eve.Attach<ContentPageConfigEntity>(this.OnPublishPages);//1
            eve.Attach<Dictionary<string, string>>(this.OnLoggingConfigurationUpdate);
            eve.Attach<PortalModel>(this.OnPortalUpdate);
            eve.Attach<SeoEntity>(this.OnPublishSEO);
            eve.Attach<ZnodePortalCountry>(this.OnChangeCountryAssociation);
            eve.Attach<ZnodePromotionCoupon>(this.OnCreatePromotion);

            eve.Attach<PriceSKUModel>(this.OnUpdatePrice); 
            eve.Attach<InventorySKUModel>(this.OnUpdateInventory);            
            eve.Attach<PromotionModel>(this.OnUpdatePromotion);

            //SEO => Product - >Content -> Category -
        }
        #endregion

        /// <summary>
        /// Delete Webstore cache on updating pricing in product.
        /// </summary>
        /// <param name="model"></param>
        private void OnUpdatePrice(PriceSKUModel model)
        {
            ClearWebstoreCache(string.Join(",", Convert.ToInt32(model.PortalId)), "OnUpdatePrice");

            eventAggregator.Detach(myPriceSKUModelToken);
        }

        /// <summary>
        /// Delete Webstore cache on updating inventory in product.
        /// </summary>
        /// <param name="model"></param>
        private void OnUpdateInventory(InventorySKUModel model)
        {
            ClearWebstoreCache(string.Join(",", Convert.ToInt32(model.PortalId)), "OnUpdateInventory");

            eventAggregator.Detach(myInventorySKUModelToken);
        }

        /// <summary>
        /// Delete Webstore cache on updating promotion in product.
        /// </summary>
        /// <param name="model"></param>
        private void OnUpdatePromotion(PromotionModel model)
        {
            ClearWebstoreCache(string.Join(",", Convert.ToInt32(model.PortalId)), "OnUpdatePromotion");

            eventAggregator.Detach(myPromotionModelToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnCreatePromotion(ZnodePromotionCoupon model)
        {
            HttpRuntime.Cache.Remove("AllPromotionCache");
           
            eventAggregator.Detach(myMessageToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnPublishContent(MessageEntity model)
        {
            //Clear API cache 
            ClearApiCache("MessageKey_");

            ClearWebstoreCache(string.Join(",", model.PortalId), "MessageKey_");

            eventAggregator.Detach(myMessageToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnPublishGlobalContent(GlobalMessageEntity model)
        {
            //Clear API cache 
            ClearApiCache("MessageKey_");

            ClearWebstoreCache(string.Join(",", GetAllPortalIds()), "MessageKey_");

            eventAggregator.Detach(myMessageToken);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnPublishSEO(SeoEntity model)
        {
            //Clear API cache 
            ClearApiCache("SEODetails_");

            ClearWebstoreCache(string.Join(",", model.SEOId), "SEODetails_");

            eventAggregator.Detach(mySEOToken);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnPortalUpdate(PortalModel model)
        {
            //Clear API cache 
            ClearApiCache("webstoreportal");

            ClearWebstoreCache(string.Join(",", model.PortalId), "webstoreportal");

            eventAggregator.Detach(webstorePortalKey);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnPublishProduct(IEnumerable<ProductEntity> model)
        {
            //get portal id
            var catalogid = model.Select(x => x.ZnodeCatalogId).AsEnumerable().Distinct();

            //Clear API cache 
            ClearApiCache("ProductListKey_");
            ClearApiCache("fulltextsearch");
            ClearApiCache("webstoreproduct");
            ClearWebstoreCache(string.Join(",", GetPortalIds(catalogid)), "ProductListKey_");

            eventAggregator.Detach(productToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnPublishCategory(List<int> model)
        {
            //Clear API cache 
            ClearApiCache("CategoryListKey_");
            ClearApiCache("categorydetails");
            ClearWebstoreCache(string.Join(",", GetPortalIds(model)), "CategoryListKey_");

            eventAggregator.Detach(categoryToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnPublishSlider(List<WidgetSliderBannerEntity> model)
        {
            //get portal id
            var portalids = model.Select(x => x.PortalId).AsEnumerable().Distinct();

            //Clear API cache 
            ClearApiCache("SliderBannerKey_");

            ClearWebstoreCache(string.Join(",", portalids), "SliderBannerKey_");

            eventAggregator.Detach(sliderToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnPublishPages(ContentPageConfigEntity model)
        {
            //Clear API cache 
            ClearApiCache("TextWidgetKey_");

            ClearWebstoreCache(string.Join(",", model.PortalId), "TextWidgetKey_");

            eventAggregator.Detach(pagesToken);
        }

        private void OnLoggingConfigurationUpdate(Dictionary<string, string> model)
        {
            ClearWebstoreCache(string.Join(",", GetAllPortalIds()), "DefaultLoggingConfigCache");

            HttpContext.Current.Cache.Remove("DefaultLoggingConfigCache");

            ZnodeCacheDependencyManager.Remove("DefaultLoggingConfigCache");

            ReInitializeWebstoreCacheForLogConfiguration(string.Join(",", GetAllPortalIds()), "DefaultLoggingConfigCache");

            eventAggregator.Detach(cacheKey);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        private void OnChangeCountryAssociation(ZnodePortalCountry model)
        {
            ClearWebstoreCache(Convert.ToString(model.PortalId), string.Concat("CountriesList", Convert.ToString(model.PortalId)));

            eventAggregator.Detach(myZnodePortalCountryToken);
        }

        private int[] GetPortalIds(IEnumerable<int> catalogids)
        {
            //get portal id
            IZnodeRepository<ZnodePortalCatalog> portalCatalog = new ZnodeRepository<ZnodePortalCatalog>();

            var portal = (from p in portalCatalog.Table
                          where catalogids.Contains(p.PublishCatalogId)
                          select new
                          {
                              p.PortalId
                          }).ToArray();
            int[] terms = new int[portal.Count()];
            int i = 0;
            foreach (var item in portal)
            {
                terms[i] = Convert.ToInt32(item.PortalId);
                i++;
            }

            return terms;
        }

        private int[] GetAllPortalIds()
        {
            //get portal id
            IZnodeRepository<ZnodePortal> portal = new ZnodeRepository<ZnodePortal>();
            var portalids = (from p in portal.Table
                             select new
                             {
                                 p.PortalId
                             }).ToArray();
            int[] terms = new int[portalids.Count()];
            int i = 0;
            foreach (var item in portalids)
            {
                terms[i] = Convert.ToInt32(item.PortalId);
                i++;
            }

            return terms;
        }

    }
}
