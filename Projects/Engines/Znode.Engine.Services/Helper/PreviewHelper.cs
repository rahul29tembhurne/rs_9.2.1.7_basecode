﻿using EntityFramework.Extensions;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.MongoDB.Data.DataModel;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.Engine.Services
{
    public static class PreviewHelper
    {
        /// <summary>
        /// Delete records from Mongo. [DeleteAndForget: It does not log anything before deletion.]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="repository"></param>
        /// <param name="query"></param>
        public static void DeleteRecords<T>(IMongoRepository<T> repository, int versionToDelete, IMongoQuery additionalQuery = null) where T : MongoEntity
        {
            IMongoQuery finalQuery = IsNotNull(additionalQuery) ?
                Query.And(Query<T>.EQ(x => x.VersionId, versionToDelete), additionalQuery) :
                Query<T>.EQ(x => x.VersionId, versionToDelete);

            repository.DeleteByQuery(finalQuery, true);
        }

        /// <summary>
        /// Delete records from Mongo. [DeleteAndForget: It does not log anything before deletion.]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="repository"></param>
        /// <param name="query"></param>
        public static void DeleteAllPreviousRecords<T>(IMongoRepository<T> repository, int versionToDelete, IMongoQuery additionalQuery = null) where T : MongoEntity
        {
            IMongoQuery finalQuery = IsNotNull(additionalQuery) ?
                Query.And(Query<T>.LTE(x => x.VersionId, versionToDelete), additionalQuery) :
                Query<T>.LTE(x => x.VersionId, versionToDelete);

            repository.DeleteByQuery(finalQuery, true);
        }

        /// <summary>
        /// Delete records from Mongo. [DeleteButRemember: It saves data into log before deletion.]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="repository"></param>
        /// <param name="logRepository"></param>
        /// <param name="query"></param>
        public static void DeleteAllPreviousRecords<T, Log>(IMongoRepository<T> repository, IMongoRepository<Log> logRepository, int versionToDelete, IMongoQuery additionalQuery = null) where T : MongoEntity where Log : MongoEntity
        {
            IMongoQuery finalQuery = IsNotNull(additionalQuery) ?
                Query.And(Query<T>.LTE(x => x.VersionId, versionToDelete), additionalQuery) :
                Query<T>.LTE(x => x.VersionId, versionToDelete);

            //Inserting the entries to the log collection before deleting.
            var logEntities = MapLogEntities<T, Log>(repository.GetEntityList(finalQuery));
            if (IsNotNull(logEntities) && logEntities.Any())
                logRepository.Create(logEntities);

            repository.DeleteByQuery(finalQuery, true);
        }

        /// <summary>
        /// Delete records from Mongo. [DeleteButRemember: It saves data into log before deletion.]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="repository"></param>
        /// <param name="logRepository"></param>
        /// <param name="query"></param>
        public static void DeleteRecords<T, Log>(IMongoRepository<T> repository, IMongoRepository<Log> logRepository, int versionToDelete, IMongoQuery additionalQuery = null,bool isDelete = false) where T : MongoEntity where Log : MongoEntity
        {
            IMongoQuery finalQuery = (versionToDelete <= 0 && isDelete) ? additionalQuery : 
                IsNotNull(additionalQuery) ?
                Query.And(Query<T>.EQ(x => x.VersionId, versionToDelete), additionalQuery) :
                Query<T>.EQ(x => x.VersionId, versionToDelete);

            //Inserting the entries to the log collection before deleting.
            var logEntities = MapLogEntities<T, Log>(repository.GetEntityList(finalQuery));
            if (IsNotNull(logEntities) && logEntities.Any())
                logRepository.Create(logEntities);

            repository.DeleteByQuery(finalQuery, true);
        }

        public static void IncreamentVersionIdForAllStoreEntities(int previousVersionId, int newVersionId)
        {
            IMongoRepository<ProductPageEntity> _cmsProductPageMongoRepository = new MongoRepository<ProductPageEntity>();
            IMongoRepository<WebStoreEntity> _cmsWebStoreMongoRepository = new MongoRepository<WebStoreEntity>();
            IMongoRepository<WidgetCategoryEntity> _cmsWidgetCategoryMongoRepository = new MongoRepository<WidgetCategoryEntity>();
            IMongoRepository<WidgetProductEntity> _cmsWidgetProductMongoRepository = new MongoRepository<WidgetProductEntity>();
            IMongoRepository<WidgetSliderBannerEntity> _cmsWidgetSliderBannerMongoRepository = new MongoRepository<WidgetSliderBannerEntity>();
            IMongoRepository<WidgetTitleEntity> _cmsWidgetTitleMongoRepository = new MongoRepository<WidgetTitleEntity>();
            IMongoRepository<TextWidgetEntity> _cmsTextWidgetMongoRepository = new MongoRepository<TextWidgetEntity>();
            IMongoRepository<ContentPageConfigEntity> _cmsContentPageMongoRepository = new MongoRepository<ContentPageConfigEntity>();
            IMongoRepository<MessageEntity> _cmsMessageMongoRepository = new MongoRepository<MessageEntity>();
            IMongoRepository<WidgetBrandEntity> _cmsWidgetBrandMongoRepository = new MongoRepository<WidgetBrandEntity>();
            IMongoRepository<BlogNewsEntity> _blogNewsMongoRepository = new MongoRepository<BlogNewsEntity>();
            IMongoRepository<PortalGlobalAttributeEntity> _portalAttributMongoRepository = new MongoRepository<PortalGlobalAttributeEntity>();
            IMongoRepository<SeoEntity> _seoMongoRepository = new MongoRepository<SeoEntity>();
            IMongoRepository<SearchWidgetEntity> _cmsSearchWidgetMongoRepository = new MongoRepository<SearchWidgetEntity>();

            //Update CMS Portal Product Page
            UpdateEntityVersion(_cmsProductPageMongoRepository, previousVersionId, newVersionId);

            //Update CMS Widget Category
            UpdateEntityVersion(_cmsWidgetCategoryMongoRepository, previousVersionId, newVersionId);

            //Update CMS Widget Product
            UpdateEntityVersion(_cmsWidgetProductMongoRepository, previousVersionId, newVersionId);

            //Update CMS Widget Slider Banner
            UpdateEntityVersion(_cmsWidgetSliderBannerMongoRepository, previousVersionId, newVersionId);

            //Update CMS Title Widget
            UpdateEntityVersion(_cmsWidgetTitleMongoRepository, previousVersionId, newVersionId);

            //Update CMS Text Widget
            UpdateEntityVersion(_cmsTextWidgetMongoRepository, previousVersionId, newVersionId);

            //Update CMS Search Widget
            UpdateEntityVersion(_cmsSearchWidgetMongoRepository, previousVersionId, newVersionId);

            //Update CMS Message Configuration
            UpdateEntityVersion(_cmsMessageMongoRepository, previousVersionId, newVersionId);

            //Update CMS Content Page Configuration
            UpdateEntityVersion(_cmsContentPageMongoRepository, previousVersionId, newVersionId);

            //Update CMS Widget Brand
            UpdateEntityVersion(_cmsWidgetBrandMongoRepository, previousVersionId, newVersionId);

            //Update Blog/News.
            UpdateEntityVersion(_blogNewsMongoRepository, previousVersionId, newVersionId);

            //Update Portal Associated Attribute Group/ Attrribute Details.
            UpdateEntityVersion(_portalAttributMongoRepository, previousVersionId, newVersionId);

            //Update SEO Settings.
            UpdateEntityVersion(_seoMongoRepository, previousVersionId, newVersionId);

            //Update Webstore configuration.
            UpdateEntityVersion(_cmsWebStoreMongoRepository, previousVersionId, newVersionId);
        }

        //Check if the preview version exists on Mongo
        public static WebStoreEntity GetWebstore(int portalId, ZnodePublishStatesEnum publishState, int? localeId = null)
        {
            IMongoRepository<WebStoreEntity> _webstoreEntity = new MongoRepository<WebStoreEntity>();

            IMongoQuery query = Query.And(
                Query<WebStoreEntity>.EQ(d => d.PortalId, portalId),
                Query<WebStoreEntity>.EQ(d => d.PublishState, publishState.ToString())
                );

            query = localeId.HasValue ? Query.And(query, Query<WebStoreEntity>.EQ(x => x.LocaleId, localeId.Value)) : query;

            return _webstoreEntity.GetEntity(query);
        }

        private static void UpdateEntityVersion<T>(IMongoRepository<T> repository, int previousVersionId, int newVersionId) where T : MongoEntity
        {
            List<T> entities = repository.GetEntityList(Query<MongoEntity>.EQ(x => x.VersionId, previousVersionId), true);
            if (IsNotNull(entities) && entities.Count > 0)
            {
                foreach (T entity in entities)
                {
                    entity.VersionId = newVersionId;
                    repository.UpdateEntity(Query<T>.EQ(x => x.Id, entity.Id), entity);
                }
            }
        }

        //Update temporary version id with actual version id.
        public static void UpdateTempEntityVersion<T>(IMongoRepository<T> repository, long publishStartTime, int actualVersionId, int publishCatalogId, List<int> publishCategoryIds, List<int> publishProductIds, bool isAllowTempVersionIdToNull = false) where T : MongoEntity
        {
            List<T> entities = new List<T>();

            if (!isAllowTempVersionIdToNull)
            {
                entities = repository.GetEntityList(Query.And(Query<ProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId), Query<MongoEntity>.EQ(x => x.VersionId, ZnodeConstant.TemporaryVersionId), Query<MongoEntity>.EQ(x => x.PublishStartTime, publishStartTime)), true);
            }
            else {
                entities = repository.GetEntityList(Query.And(Query<ProductEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId), Query<MongoEntity>.EQ(x => x.VersionId, actualVersionId), Query<MongoEntity>.EQ(x => x.PublishStartTime, publishStartTime)), true);
            }

            if (IsNotNull(entities) && entities.Count > 0)
            {
                List<List<T>> publishedEntityListInChunks = SplitCollectionIntoChunks(entities, !string.IsNullOrEmpty(ZnodeApiSettings.ProductPublishChunkSize) ? Convert.ToInt32(ZnodeApiSettings.ProductPublishChunkSize) : ZnodeConstant.ProductPublishChunk);

                Parallel.For(0, publishedEntityListInChunks.Count(), new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, index =>
                {
                    foreach (T entity in publishedEntityListInChunks[index])
                    {
                        if (!isAllowTempVersionIdToNull)
                            entity.VersionId = actualVersionId;
                        else
                            entity.PublishStartTime = null;

                        repository.UpdateEntity(Query<T>.EQ(x => x.Id, entity.Id), entity);
                    }
                });
            }
        }

        //Create Entity Version.
        private static void CreateEntityVersion<T>(IMongoRepository<T> repository, int previousVersionId, int newVersionId) where T : MongoEntity
        {
            List<T> entities = repository.GetEntityList(Query<MongoEntity>.EQ(x => x.VersionId, previousVersionId), true);
            if (IsNotNull(entities) && entities.Count > 0)
            {
                foreach (T entity in entities)
                {
                    entity.Id = ObjectId.GenerateNewId();
                    entity.VersionId = newVersionId;
                }
                repository.Create(entities);
            }
        }

        public static void CopyWebstoreToState(IMongoRepository<WebStoreEntity> repository, int previewedVersionId, int currentPublishedVersion, int portalId, ZnodePublishStatesEnum targetPublishState)
        {
            List<WebStoreEntity> entities = repository.GetEntityList(Query.And(Query<WebStoreEntity>.EQ(x => x.PortalId, portalId), Query<MongoEntity>.EQ(x => x.VersionId, previewedVersionId)), true);

            if (IsNotNull(entities) && entities.Count > 0)
            {
                foreach (WebStoreEntity entity in entities)
                {
                    entity.VersionId = currentPublishedVersion;
                    entity.PublishState = targetPublishState.ToString();
                    entity.Id = ObjectId.GenerateNewId();
                }

                repository.Create(entities);
            }
        }

        public static void CopyEntityFromVersion<T>(IMongoRepository<T> repository, int previewedVersionId, int currentPublishedVersion, int portalId, IMongoQuery additionalQuery = null) where T : MongoEntity
        {
            IMongoQuery finalQuery = Query.And(Query<WebStoreEntity>.EQ(x => x.PortalId, portalId), Query<MongoEntity>.EQ(x => x.VersionId, previewedVersionId));

            if (IsNotNull(additionalQuery))
                finalQuery = Query.And(finalQuery, additionalQuery);

            List<T> entities = repository.GetEntityList(finalQuery, true);

            if (IsNotNull(entities) && entities.Count > 0)
            {
                foreach (T entity in entities)
                {
                    entity.VersionId = currentPublishedVersion;
                    entity.Id = ObjectId.GenerateNewId();
                }

                repository.Create(entities);
            }
        }

        /// <summary>
        /// Copy Entities For Preview.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="versionId"></param>
        /// <param name="query"></param>
        /// <param name="repository"></param>
        public static void CopyEntitiesForPreview<T, Log>(int versionId, IMongoQuery query, IMongoRepository<T> repository) where T : MongoEntity where Log : MongoEntity, IDisposable, new()
        {
            List<T> entites;
            entites = repository.GetEntityList(query, true);

            var logEntities = MapLogEntities<T, Log>(entites);

            Parallel.ForEach(entites, (obj) => { obj.VersionId = versionId; obj.Id = ObjectId.GenerateNewId(); });

            if (entites?.Count() > 0)
                repository.Create(entites);

            if (logEntities.Any())
                new MongoRepository<Log>().Create(logEntities);
        }

        //Get Product count of published product. 
        public static int GetProductCountByQuery(IMongoQuery query)
        {
            List<ProductEntity> entityList = new MongoRepository<ProductEntity>().GetEntityList(query);
            List<int> productIds = entityList.Select(x => x.ZnodeProductId).ToList();
            if (productIds.Count > 0)
                return productIds.Distinct().Count();
            else
                return 0;
        }

        //Get Product count of published Category.
        public static int GetCategoryCountByQuery(IMongoQuery query)
        {
            List<CategoryEntity> entityList = new MongoRepository<CategoryEntity>().GetEntityList(query);
            List<int> productIds = entityList.Select(x => x.ZnodeCategoryId).ToList();
            if (productIds.Count > 0)
                return productIds.Distinct().Count();
            else
                return 0;
        }

        // Delete publish Catalog along with associated category and products
        public static void DeletePublishCatalogFromMongo(IMongoQuery query, int PreviousVersionId = 0, bool isDelete = false)
        {
              Parallel.Invoke(new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },

                    //Delete Publish Products associated with given catalog
                    () => { DeleteRecords(new MongoRepository<CatalogEntity>(), new MongoRepository<_LogCatalogEntity>(), PreviousVersionId, query, isDelete); },

                    //Delete Publish Products associated with given catalog
                    () => { DeleteRecords(new MongoRepository<CatalogAttributeEntity>(), new MongoRepository<_LogCatalogAttributeEntity>(), PreviousVersionId, query, isDelete); },

                    //Delete Publish category associated with given catalog
                    () => { DeleteRecords(new MongoRepository<CategoryEntity>(), new MongoRepository<_LogCategoryEntity>(), PreviousVersionId, query, isDelete); },

                    //Delete Publish catalog 
                    () => { DeleteRecords(new MongoRepository<ProductEntity>(), new MongoRepository<_LogProductEntity>(), PreviousVersionId, query, isDelete); },

                    //Delete Publish Addons
                    () => { DeleteRecords(new MongoRepository<AddonEntity>(), new MongoRepository<_LogAddonEntity>(), PreviousVersionId, query, isDelete); },

                    //Delete Publish Group Products
                    () => { DeleteRecords(new MongoRepository<GroupProductEntity>(), new MongoRepository<_LogGroupProductEntity>(), PreviousVersionId, query, isDelete); },

                    //Delete Publish Configurable Products
                    () => { DeleteRecords(new MongoRepository<ConfigurableProductEntity>(), new MongoRepository<_LogConfigurableProductEntity>(), PreviousVersionId, query, isDelete); },

                    //Delete Publish Bundle Products
                    () => { DeleteRecords(new MongoRepository<BundleProductEntity>(), new MongoRepository<_LogBundleProductEntity>(), PreviousVersionId, query, isDelete); },

                    //Delete previous version from versionEntity.
                    () => { DeleteRecords(new MongoRepository<VersionEntity>(), new MongoRepository<_LogVersionEntity>(), PreviousVersionId, query, isDelete); });
        }

        // Delete publish Catalog along with associated category and products
        public static void DeletePublishProductFromMongo(IMongoQuery query)
        {
            //Delete Publish catalog
            new MongoRepository<ProductEntity>().DeleteByQuery(query);

            //Delete Publish Addons
            new MongoRepository<AddonEntity>().DeleteByQuery(query);

            //Delete Publish Group Products
            new MongoRepository<GroupProductEntity>().DeleteByQuery(query);

            //Delete Publish Configurable Products
            new MongoRepository<ConfigurableProductEntity>().DeleteByQuery(query);

            //Delete Publish Bundle Products
            new MongoRepository<BundleProductEntity>().DeleteByQuery(query);

        }

        /// <summary>
        /// Copy Entities For Preview.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="versionId"></param>
        /// <param name="query"></param>
        /// <param name="repository"></param>
        public static IEnumerable<D> MapLogEntities<S, D>(List<S> resultSet) where D : MongoEntity where S : MongoEntity
        {
            foreach (S obj in resultSet)
                obj.Id = ObjectId.GenerateNewId();

            return resultSet.ToModel<D>();
        }

        /// <summary>
        /// Copy Associated Products To Mongo.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="versionId"></param>
        /// <param name="query"></param>
        /// <param name="mongoRepository"></param>
        public static void CopyAssociatedProductsToMongo<T>(int versionId, IMongoQuery query, IMongoRepository<T> mongoRepository) where T : AssociatedProductEntity
        {
            List<T> entites;

            entites = mongoRepository.GetEntityList(query, true);

            foreach (T obj in entites)
            {
                obj.VersionId = versionId;
                obj.Id = ObjectId.GenerateNewId();
                mongoRepository.Create(obj);
            }
        }

        /// <summary>
        /// Get version by revisionType.
        /// </summary>
        /// <param name="publishCatalogId"></param>
        /// <param name="revisionType"></param>
        /// <returns></returns>
        public static int GetVersionId(int publishCatalogId, string revisionType, int localeId = 0)
        {
            List<VersionEntity> versionEntity = new MongoRepository<VersionEntity>().GetEntityList(Query.And(
            Query<VersionEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
            Query<VersionEntity>.EQ(x => x.RevisionType, revisionType),
            Query<VersionEntity>.EQ(x => x.LocaleId, localeId),
            Query<VersionEntity>.EQ(x => x.IsPublishSuccess, true)))?.OrderByDescending(c => c.VersionId)?.ToList();

            return versionEntity.Count > 0 ? versionEntity.FirstOrDefault().VersionId : 0;
        }


        // Get latest version by revisionType.
        public static int GetLatestVersionId(int publishCatalogId, string revisionType, int localeId = 0)
        {
            VersionEntity versionEntity = new MongoRepository<VersionEntity>().GetEntityList(Query.And(
            Query<VersionEntity>.EQ(x => x.ZnodeCatalogId, publishCatalogId),
            Query<VersionEntity>.EQ(x => x.RevisionType, revisionType),
            Query<VersionEntity>.EQ(x => x.LocaleId, localeId),
            Query<VersionEntity>.EQ(x => x.IsPublishSuccess, true))).OrderByDescending(x => x.VersionId).FirstOrDefault();
            return versionEntity != null ? versionEntity.VersionId : 0;
        }

        //In this method we swap version id , here we swap Temporary version with our actual version. This is performd perticularly for category publish from catalog.
        public static void SwapVersionIdInMongoEntities(long publishStartTime, int actualVersionId, int publishCatalogId, List<int> publishCategoryIds, List<int> publishProductIds, bool isAllowTempVersionIdToNull = false)
        {
             IMongoRepository<CategoryEntity> _categoryMongoRepository = new MongoRepository<CategoryEntity>();
             IMongoRepository<ProductEntity> _productMongoRepository = new MongoRepository<ProductEntity>();
             IMongoRepository<AddonEntity> _addonMongoRepository = new MongoRepository<AddonEntity>();
             IMongoRepository<GroupProductEntity> _groupProductMongoRepository = new MongoRepository<GroupProductEntity>();
             IMongoRepository<ConfigurableProductEntity> _configurableProductMongoRepository = new MongoRepository<ConfigurableProductEntity>();
             IMongoRepository<BundleProductEntity> _bundleProductMongoRepository = new MongoRepository<BundleProductEntity>();

            Parallel.Invoke(new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount },
                   //Update Category Entity
                   () => { UpdateTempEntityVersion(_categoryMongoRepository, publishStartTime, actualVersionId, publishCatalogId, publishCategoryIds, publishProductIds, isAllowTempVersionIdToNull);},
                   //Update Product Entity
                   () => { UpdateTempEntityVersion(_productMongoRepository, publishStartTime, actualVersionId, publishCatalogId, publishCategoryIds, publishProductIds, isAllowTempVersionIdToNull); },
                   //Update Addon Entity
                   () => { UpdateTempEntityVersion(_addonMongoRepository, publishStartTime, actualVersionId, publishCatalogId, publishCategoryIds, publishProductIds, isAllowTempVersionIdToNull);   },
                   //Update Group Product Entity
                   () => { UpdateTempEntityVersion(_groupProductMongoRepository, publishStartTime, actualVersionId, publishCatalogId, publishCategoryIds, publishProductIds, isAllowTempVersionIdToNull); },
                   //Update Configurable Product Entity
                   () => { UpdateTempEntityVersion(_configurableProductMongoRepository, publishStartTime, actualVersionId, publishCatalogId, publishCategoryIds, publishProductIds, isAllowTempVersionIdToNull); },
                   //Update Bundle Product Entity
                   () => { UpdateTempEntityVersion(_bundleProductMongoRepository, publishStartTime, actualVersionId, publishCatalogId, publishCategoryIds, publishProductIds, isAllowTempVersionIdToNull); });
        }

        // Delete data from mongo.
        public static void DeleteLogFromMongo(int versionId)
        {

            //Delete Publish Product associated with given catalog
            new MongoRepository<_LogProductEntity>().DeleteByQuery(Query.And(
                    Query<_LogProductEntity>.EQ(x => x.VersionId, versionId)));

            //Delete Category catalog
            new MongoRepository<_LogCategoryEntity>().DeleteByQuery(Query.And(
                    Query<_LogCategoryEntity>.EQ(x => x.VersionId, versionId)));

            //Delete Publish Catalog 
            new MongoRepository<_LogCatalogEntity>().DeleteByQuery(Query.And(
                    Query<_LogCatalogEntity>.EQ(x => x.VersionId, versionId)));

            //Write your delete logic here.

            //Delete Publish Catalog 
            new MongoRepository<PublishPreviewLogEntity>().DeleteByQuery(Query.And(
                    Query<PublishPreviewLogEntity>.EQ(x => x.VersionId, versionId)));
        }

        public static void CreatePreviewLog(string publishState, int versionId, int preVersionId, string entityId, string entity, int localeId, string localeName)
          => new MongoRepository<PublishPreviewLogEntity>().Create(
                        new PublishPreviewLogEntity()
                        {
                            EntityId = entityId,
                            EntityType = entity,
                            LogCreatedDate = DateTime.UtcNow,
                            LogMessage = $"{entity} has been published successfully.",
                            SourcePublishState = publishState,
                            VersionId = versionId,
                            PreviousVersionId = preVersionId,
                            LocaleId = localeId,
                            LocaleDisplayValue = localeName
                        });

        public static void CreatePreviewLog(string publishState, int versionId, int preVersionId, int entityId, string entity, int localeId, string localeName)
         => CreatePreviewLog(publishState, versionId, preVersionId, entityId.ToString(), entity, localeId, localeName);

        public static void CopyAllCMSEntitiesToVersion(int sourceVersionId, int targetVersionId, int portalId, ZnodePublishStatesEnum publishState)
        {
            IMongoRepository<WebStoreEntity> _cmsWebStoreMongoRepository = new MongoRepository<WebStoreEntity>();
            IMongoRepository<WidgetCategoryEntity> _cmsWidgetCategoryMongoRepository = new MongoRepository<WidgetCategoryEntity>();
            IMongoRepository<WidgetProductEntity> _cmsWidgetProductMongoRepository = new MongoRepository<WidgetProductEntity>();
            IMongoRepository<WidgetSliderBannerEntity> _cmsWidgetSliderBannerMongoRepository = new MongoRepository<WidgetSliderBannerEntity>();
            IMongoRepository<WidgetTitleEntity> _cmsWidgetTitleMongoRepository = new MongoRepository<WidgetTitleEntity>();
            IMongoRepository<TextWidgetEntity> _cmsTextWidgetMongoRepository = new MongoRepository<TextWidgetEntity>();
            IMongoRepository<ContentPageConfigEntity> _cmsContentPageMongoRepository = new MongoRepository<ContentPageConfigEntity>();
            IMongoRepository<MessageEntity> _cmsMessageMongoRepository = new MongoRepository<MessageEntity>();
            IMongoRepository<WidgetBrandEntity> _cmsWidgetBrandMongoRepository = new MongoRepository<WidgetBrandEntity>();
            IMongoRepository<BlogNewsEntity> _blogNewsMongoRepository = new MongoRepository<BlogNewsEntity>();
            IMongoRepository<PortalGlobalAttributeEntity> _portalAttributMongoRepository = new MongoRepository<PortalGlobalAttributeEntity>();
            IMongoRepository<SeoEntity> _seoMongoRepository = new MongoRepository<SeoEntity>();
            IMongoRepository<SearchWidgetEntity> _cmsSearchWidgetMongoRepository = new MongoRepository<SearchWidgetEntity>();
            IMongoRepository<ProductPageEntity> _cmsProductPageMongoRepository = new MongoRepository<ProductPageEntity>();

            CopyWebstoreToState(_cmsWebStoreMongoRepository, sourceVersionId, targetVersionId, portalId, publishState);

            CopyEntityFromVersion(_cmsProductPageMongoRepository, sourceVersionId, targetVersionId, portalId);
            CopyEntityFromVersion(_cmsWidgetCategoryMongoRepository, sourceVersionId, targetVersionId, portalId);
            CopyEntityFromVersion(_cmsWidgetProductMongoRepository, sourceVersionId, targetVersionId, portalId);
            CopyEntityFromVersion(_cmsWidgetSliderBannerMongoRepository, sourceVersionId, targetVersionId, portalId);
            CopyEntityFromVersion(_cmsWidgetTitleMongoRepository, sourceVersionId, targetVersionId, portalId);
            CopyEntityFromVersion(_cmsTextWidgetMongoRepository, sourceVersionId, targetVersionId, portalId);
            CopyEntityFromVersion(_cmsContentPageMongoRepository, sourceVersionId, targetVersionId, portalId);
            CopyEntityFromVersion(_cmsMessageMongoRepository, sourceVersionId, targetVersionId, portalId);
            CopyEntityFromVersion(_cmsWidgetBrandMongoRepository, sourceVersionId, targetVersionId, portalId);
            CopyEntityFromVersion(_blogNewsMongoRepository, sourceVersionId, targetVersionId, portalId);
            CopyEntityFromVersion(_portalAttributMongoRepository, sourceVersionId, targetVersionId, portalId);
            CopyEntityFromVersion(_seoMongoRepository, sourceVersionId, targetVersionId, portalId);
            CopyEntityFromVersion(_cmsSearchWidgetMongoRepository, sourceVersionId, targetVersionId, portalId);
        }

        public static void DeleteAllCMSEntitiesFromMongo(int versionToDelete, IMongoQuery additionalQuery = null, bool keepLog = false)
        {
            IMongoRepository<WebStoreEntity> _cmsWebStoreMongoRepository = new MongoRepository<WebStoreEntity>();
            IMongoRepository<WidgetCategoryEntity> _cmsWidgetCategoryMongoRepository = new MongoRepository<WidgetCategoryEntity>();
            IMongoRepository<WidgetProductEntity> _cmsWidgetProductMongoRepository = new MongoRepository<WidgetProductEntity>();
            IMongoRepository<WidgetSliderBannerEntity> _cmsWidgetSliderBannerMongoRepository = new MongoRepository<WidgetSliderBannerEntity>();
            IMongoRepository<WidgetTitleEntity> _cmsWidgetTitleMongoRepository = new MongoRepository<WidgetTitleEntity>();
            IMongoRepository<TextWidgetEntity> _cmsTextWidgetMongoRepository = new MongoRepository<TextWidgetEntity>();
            IMongoRepository<ContentPageConfigEntity> _cmsContentPageMongoRepository = new MongoRepository<ContentPageConfigEntity>();
            IMongoRepository<MessageEntity> _cmsMessageMongoRepository = new MongoRepository<MessageEntity>();
            IMongoRepository<WidgetBrandEntity> _cmsWidgetBrandMongoRepository = new MongoRepository<WidgetBrandEntity>();
            IMongoRepository<BlogNewsEntity> _blogNewsMongoRepository = new MongoRepository<BlogNewsEntity>();
            IMongoRepository<PortalGlobalAttributeEntity> _portalAttributMongoRepository = new MongoRepository<PortalGlobalAttributeEntity>();
            IMongoRepository<SeoEntity> _seoMongoRepository = new MongoRepository<SeoEntity>();
            IMongoRepository<SearchWidgetEntity> _cmsSearchWidgetMongoRepository = new MongoRepository<SearchWidgetEntity>();
            IMongoRepository<ProductPageEntity> _cmsProductPageMongoRepository = new MongoRepository<ProductPageEntity>();

            IMongoRepository<_LogWebStoreEntity> log_cmsWebStoreMongoRepository = new MongoRepository<_LogWebStoreEntity>();
            IMongoRepository<_LogWidgetCategoryEntity> log_cmsWidgetCategoryMongoRepository = new MongoRepository<_LogWidgetCategoryEntity>();
            IMongoRepository<_LogWidgetProductEntity> log_cmsWidgetProductMongoRepository = new MongoRepository<_LogWidgetProductEntity>();
            IMongoRepository<_LogWidgetSliderBannerEntity> log_cmsWidgetSliderBannerMongoRepository = new MongoRepository<_LogWidgetSliderBannerEntity>();
            IMongoRepository<_LogWidgetTitleEntity> log_cmsWidgetTitleMongoRepository = new MongoRepository<_LogWidgetTitleEntity>();
            IMongoRepository<_LogTextWidgetEntity> log_cmsTextWidgetMongoRepository = new MongoRepository<_LogTextWidgetEntity>();
            IMongoRepository<_LogContentPageConfigEntity> log_cmsContentPageMongoRepository = new MongoRepository<_LogContentPageConfigEntity>();
            IMongoRepository<_LogMessageEntity> log_cmsMessageMongoRepository = new MongoRepository<_LogMessageEntity>();
            IMongoRepository<_LogWidgetBrandEntity> log_cmsWidgetBrandMongoRepository = new MongoRepository<_LogWidgetBrandEntity>();
            IMongoRepository<_LogBlogNewsEntity> log_blogNewsMongoRepository = new MongoRepository<_LogBlogNewsEntity>();
            IMongoRepository<_LogPortalGlobalAttributeEntity> log_portalAttributMongoRepository = new MongoRepository<_LogPortalGlobalAttributeEntity>();
            IMongoRepository<_LogSeoEntity> log_seoMongoRepository = new MongoRepository<_LogSeoEntity>();
            IMongoRepository<_LogSearchWidgetEntity> log_cmsSearchWidgetMongoRepository = new MongoRepository<_LogSearchWidgetEntity>();
            IMongoRepository<_LogProductPageEntity> log_cmsProductPageMongoRepository = new MongoRepository<_LogProductPageEntity>();

            if (keepLog)
            {
                //Delete CMS Portal Product Page
                DeleteAllPreviousRecords(_cmsProductPageMongoRepository, log_cmsProductPageMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Widget Category
                DeleteRecords(_cmsWidgetCategoryMongoRepository, log_cmsWidgetCategoryMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Widget Product
                DeleteRecords(_cmsWidgetProductMongoRepository, log_cmsWidgetProductMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Widget Slider Banner

                DeleteAllPreviousRecords(_cmsWidgetSliderBannerMongoRepository, log_cmsWidgetSliderBannerMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Title Widget
                DeleteRecords(_cmsWidgetTitleMongoRepository, log_cmsWidgetTitleMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Text Widget
                DeleteRecords(_cmsTextWidgetMongoRepository, log_cmsTextWidgetMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Search Widget
                DeleteRecords(_cmsSearchWidgetMongoRepository, log_cmsSearchWidgetMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Message Configuration
                DeleteRecords(_cmsMessageMongoRepository, log_cmsMessageMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Content Page Configuration
                DeleteRecords(_cmsContentPageMongoRepository, log_cmsContentPageMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Widget Brand
                DeleteRecords(_cmsWidgetBrandMongoRepository, log_cmsWidgetBrandMongoRepository, versionToDelete, additionalQuery);

                //Delete Blog/News.
                DeleteRecords(_blogNewsMongoRepository, log_blogNewsMongoRepository, versionToDelete, additionalQuery);

                //Delete Portal Associated Attribute Group/ Attrribute Details.
                DeleteRecords(_portalAttributMongoRepository, log_portalAttributMongoRepository, versionToDelete, additionalQuery);

                //Delete SEO configuration.
                DeleteRecords(_seoMongoRepository, log_seoMongoRepository, versionToDelete, additionalQuery);

                //Delete Webstore configuration.
                DeleteRecords(_cmsWebStoreMongoRepository, log_cmsWebStoreMongoRepository, versionToDelete, additionalQuery);
            }
            else
            {
                //Delete CMS Portal Product Page
                DeleteRecords(_cmsProductPageMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Widget Category
                DeleteRecords(_cmsWidgetCategoryMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Widget Product
                DeleteRecords(_cmsWidgetProductMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Widget Slider Banner
                DeleteRecords(_cmsWidgetSliderBannerMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Title Widget
                DeleteRecords(_cmsWidgetTitleMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Text Widget
                DeleteRecords(_cmsTextWidgetMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Search Widget
                DeleteRecords(_cmsSearchWidgetMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Message Configuration
                DeleteRecords(_cmsMessageMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Content Page Configuration
                DeleteRecords(_cmsContentPageMongoRepository, versionToDelete, additionalQuery);

                //Delete CMS Widget Brand
                DeleteRecords(_cmsWidgetBrandMongoRepository, versionToDelete, additionalQuery);

                //Delete Blog/News.
                DeleteRecords(_blogNewsMongoRepository, versionToDelete, additionalQuery);

                //Delete Portal Associated Attribute Group/ Attrribute Details.
                DeleteRecords(_portalAttributMongoRepository, versionToDelete, additionalQuery);

                //Delete SEO configuration.
                DeleteRecords(_seoMongoRepository, versionToDelete, additionalQuery);

                //Delete Webstore configuration.
                DeleteRecords(_cmsWebStoreMongoRepository, versionToDelete, additionalQuery);
            }
        }

        /// <summary>
        /// Publish Associated Products like (Addons, Group Products,Bundle Products and Configurable Products) Configuration
        /// </summary>
        /// <param name="id">Id (catalog/Product) </param>
        /// <param name="parameterName">parameter Name</param>
        /// <param name="userId">User Id</param>
        /// <param name="versionId">Version Id</param>
        public static void PublishAssociatedProductsConfiguration(int id, string parameterName, int userId, int? versionId = null, List<int> localeIds = null, ZnodePublishStatesEnum znodePublishStateEnum = ZnodePublishStatesEnum.PREVIEW)
        {
            //Publish Addons for multiple products
            PublishAddons(id, parameterName, userId, versionId, localeIds, znodePublishStateEnum);

            //Publish Group Products for multiple products 
            PublishGroupProducts(id, parameterName, userId, versionId, znodePublishStateEnum);

            //Publish Bundle Products for multiple products
            PublishBundleProducts(id, parameterName, userId, versionId, znodePublishStateEnum);

            //Publish Configurable Products for multiple products
            PublishConfigurableProducts(id, parameterName, userId, versionId, znodePublishStateEnum);
        }

        //Update Znode Publish Catalog Log with status of catalog
        public static void SetPublishStatus(int versionId, bool status, string revisionType = "", string type = "", List<int> typeIds = null, bool isCategoryPublishCall = false)
        {
            try
            {
                IZnodeRepository<ZnodePublishCatalogLog> _publishCatalogLogRepository = new ZnodeRepository<ZnodePublishCatalogLog>();

                ZnodePublishCatalogLog publishCatalogLog = _publishCatalogLogRepository.GetById(versionId);
                if (IsNotNull(publishCatalogLog))
                {
                    if (!isCategoryPublishCall)
                    {
                        publishCatalogLog.IsCatalogPublished = status;
                        publishCatalogLog.IsCategoryPublished = status;
                        publishCatalogLog.IsProductPublished = status;
                        publishCatalogLog.PublishStateId = GetPublishStatusByCode(revisionType)?.PublishStateId;
                    }
                    else
                    {
                        if (revisionType == ZnodePublishStatesEnum.PREVIEW.ToString())
                        {
                            publishCatalogLog.IsCatalogPublished = status;
                            publishCatalogLog.PublishStateId = GetPublishStatusByCode(revisionType)?.PublishStateId;
                        }
                    }

                    if (revisionType == ZnodePublishStatesEnum.PRODUCTION.ToString())
                    {
                        publishCatalogLog.PublishCategoryId = Convert.ToString(GetCategoryCountByQuery(Query<CategoryEntity>.EQ(pr => pr.VersionId, versionId)));
                        publishCatalogLog.PublishProductId = Convert.ToString(GetProductCountByQuery(Query<ProductEntity>.EQ(pr => pr.VersionId, versionId)));
                        GetPimCategorysByCatalogIdAndSetStatus(publishCatalogLog.PimCatalogId, publishCatalogLog.PublishStateId, 1);
                    }
                    _publishCatalogLogRepository.Update(publishCatalogLog);

                    if (type == SEODetailsEnum.Product.ToString())
                        SetProductPublishStatus(typeIds, publishCatalogLog);

                    if (type == SEODetailsEnum.Category.ToString())
                        SetCategoryPublishStatus(typeIds, publishCatalogLog);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            }
        }

        public static void SetPimProductsByCatalogIdAndSetStatus(List<int> productIds, string revisionType, byte isProductPublish)
        {
            byte? publishStateId = GetPublishStatusByCode(revisionType)?.PublishStateId;
            foreach (int productId in productIds)
            {
                GetPublishProductsByCatalogIdAndSetStatus(productId, publishStateId, isProductPublish);
            }
        }

        public static bool SetPimCategoryByCatalogIdAndSetStatus(int categoryId, string revisionType, byte isProductPublish)
        {
            byte? publishStateId = GetPublishStatusByCode(revisionType)?.PublishStateId;
            return GetPimCategorysByCatalogIdAndSetStatus(0, publishStateId, isProductPublish, categoryId);
        }

        //Update SEO publish status.
        public static void UpdateSeoPublishStatusBySEOCode(string seoCodes, string revisionType)
        {
            byte publishStateId = GetPublishStatusByCode(revisionType).PublishStateId;
            IZnodeRepository<ZnodeCMSSEODetail> _seoDetailRepository = new ZnodeRepository<ZnodeCMSSEODetail>();
            List<ZnodeCMSSEODetail> cMSSEODetailList = _seoDetailRepository.Table.Where(x => x.SEOCode == seoCodes)?.ToList();
            foreach (var cMSSEODetail in cMSSEODetailList)
            {
                cMSSEODetail.IsPublish = true;
                cMSSEODetail.PublishStateId = publishStateId;               
            }
            try
            {
                 _seoDetailRepository.BatchUpdate(cMSSEODetailList);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            }
        }

        public static bool GetPimCategorysByCatalogIdAndSetStatus(int catalogId, byte? publishStateId, byte isCategoryPublish, int publishCategoryId = 0)
        {
            IZnodeRepository<View_GetCatalogCategory> _publishCatalogProductRepository = new ZnodeRepository<View_GetCatalogCategory>();
            IZnodeRepository<ZnodePimCategory> _pimProductRepository = new ZnodeRepository<ZnodePimCategory>();
            IZnodeRepository<ZnodePublishCategory> _publishCategoryRepository = new ZnodeRepository<ZnodePublishCategory>();
            List<ZnodePimCategory> znodePimCategory = new List<ZnodePimCategory>();

            if (publishCategoryId > 0)
            {
                ZnodePublishCategory model = _publishCategoryRepository.Table.FirstOrDefault(x => x.PublishCategoryId == publishCategoryId);
                znodePimCategory = IsNotNull(model) ? _publishCatalogProductRepository.Table.Where(x => x.PimCategoryId == model.PimCategoryId).ToModel<ZnodePimCategory>().ToList() : null;
            }

            if (catalogId > 0)
                znodePimCategory = _publishCatalogProductRepository.Table.Where(x => x.PimCatalogId == catalogId).ToModel<ZnodePimCategory>().ToList();

            if(IsNotNull(znodePimCategory))
                Parallel.ForEach(znodePimCategory,(category) => { category.PublishStateId = publishStateId; category.IsCategoryPublish = isCategoryPublish; });

            try
            {
                return _pimProductRepository.BatchUpdate(znodePimCategory);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                return false;
            }
        }

        public static bool GetPublishProductsByCatalogIdAndSetStatus(int publishProductId, byte? publishStateId, byte isProductPublish, int pimCatalogId = 0)
        {
            IZnodeRepository<ZnodePublishProduct> _publishCatalogProductRepository = new ZnodeRepository<ZnodePublishProduct>();

            List<string> _productId = new List<string>();

            try
            {

                if (publishProductId > 0)
                    _productId = _publishCatalogProductRepository.Table.Where(x => x.PublishProductId == publishProductId).Select(x => x.PimProductId.ToString()).ToList();

                HelperMethods.CurrentContext.ZnodePimProducts.Where(x => _productId.Contains(x.PimProductId.ToString())).Update(y => new ZnodePimProduct() { PublishStateId = publishStateId, IsProductPublish = isProductPublish });

                return true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                return false;
            }
        }

        public static bool GetPimProductsByCatalogIdAndSetStatus(int pimProductId, byte? publishStateId, byte isProductPublish, int pimCatalogId = 0)
        {
            IZnodeRepository<View_GetCatalogProduct> _publishCatalogProductRepository = new ZnodeRepository<View_GetCatalogProduct>();

            List<string> _productId = new List<string>();

            try
            {

                if (pimProductId > 0)
                    _productId = _publishCatalogProductRepository.Table.Where(x => x.PimProductId == pimProductId).Select(x => x.PimProductId.ToString()).ToList();

                if (pimCatalogId > 0)
                    _productId = _publishCatalogProductRepository.Table.Where(x => x.PimCatalogId == pimCatalogId).Select(x => x.PimProductId.ToString()).ToList();

                HelperMethods.CurrentContext.ZnodePimProducts.Where(x => _productId.Contains(x.PimProductId.ToString())).Update(y => new ZnodePimProduct() { PublishStateId = publishStateId, IsProductPublish = isProductPublish });

                return true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                return false;
            }
        }

        public static void ManageMongoVersionByQuery(IMongoQuery query, VersionEntity model)
        {
            //Add new version to version entity
            new MongoRepository<VersionEntity>().Create(model);

            new MongoRepository<VersionEntity>().DeleteByQuery(query);
        }

        //Publish Addons 
        private static void PublishAddons(int id, string parameterName, int userId, int? versionId = null, List<int> localeIds = null, ZnodePublishStatesEnum znodePublishStateEnum = ZnodePublishStatesEnum.PREVIEW)
        {
            if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
                SaveAssociatedProductsToMongo<AddonEntity>(ExecuteSPWithJSONParameters(id, parameterName, userId, "Znode_GetPublishAssociatedAddonsWithJSON", versionId, "", "", localeIds, znodePublishStateEnum), new MongoRepository<AddonEntity>());
            else
                SaveAssociatedProductsToMongo<AddonEntity>(ExecuteSP(id, parameterName, userId, "Znode_GetPublishAssociatedAddons", versionId, "", "", localeIds, znodePublishStateEnum), new MongoRepository<AddonEntity>());
        }

        //Publish Group Products 
        private static void PublishGroupProducts(int id, string parameterName, int userId, int? versionId = null, ZnodePublishStatesEnum znodePublishStateEnum = ZnodePublishStatesEnum.PREVIEW)
        {
            if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
                SaveAssociatedProductsToMongo<GroupProductEntity>(ExecuteSPWithJSONParameters(id, parameterName, userId, "Znode_GetPublishAssociatedProductsWithJSON", versionId, "@ProductType", "GroupedProduct", new List<int>(), znodePublishStateEnum), new MongoRepository<GroupProductEntity>());
            else
                SaveAssociatedProductsToMongo<GroupProductEntity>(ExecuteSP(id, parameterName, userId, "Znode_GetPublishAssociatedProducts", versionId, "@ProductType", "GroupedProduct", new List<int>(), znodePublishStateEnum), new MongoRepository<GroupProductEntity>());
        }

        //Publish Bundle Products
        private static void PublishBundleProducts(int id, string parameterName, int userId, int? versionId = null, ZnodePublishStatesEnum znodePublishStateEnum = ZnodePublishStatesEnum.PREVIEW)
        {
            if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
                SaveAssociatedProductsToMongo<BundleProductEntity>(ExecuteSPWithJSONParameters(id, parameterName, userId, "Znode_GetPublishAssociatedProductsWithJSON", versionId, "@ProductType", "BundleProduct", new List<int>(), znodePublishStateEnum), new MongoRepository<BundleProductEntity>());
            else
                SaveAssociatedProductsToMongo<BundleProductEntity>(ExecuteSP(id, parameterName, userId, "Znode_GetPublishAssociatedProducts", versionId, "@ProductType", "BundleProduct", new List<int>(), znodePublishStateEnum), new MongoRepository<BundleProductEntity>());
        }

        //Publish Configurable Products
        private static void PublishConfigurableProducts(int id, string parameterName, int userId, int? versionId = null, ZnodePublishStatesEnum znodePublishStateEnum = ZnodePublishStatesEnum.PREVIEW)
        {
            if (DefaultGlobalConfigSettingHelper.IsColumnEncryptionSettingEnabled)
                SaveAssociatedProductsToMongo<ConfigurableProductEntity>(ExecuteSPWithJSONParameters(id, parameterName, userId, "Znode_GetPublishAssociatedProductsWithJSON", versionId, "@ProductType", "ConfigurableProduct", new List<int>(), znodePublishStateEnum), new MongoRepository<ConfigurableProductEntity>());
            else
                SaveAssociatedProductsToMongo<ConfigurableProductEntity>(ExecuteSP(id, parameterName, userId, "Znode_GetPublishAssociatedProducts", versionId, "@ProductType", "ConfigurableProduct", new List<int>(), znodePublishStateEnum), new MongoRepository<ConfigurableProductEntity>());
        }

        // Convert Dataset to Entity and save in Mongo
        private static void SaveAssociatedProductsToMongo<T>(DataSet resultDataSet, IMongoRepository<T> mongoRepository) where T : AssociatedProductEntity
        {
            DataTable dataTable = resultDataSet.Tables[0];
            List<T> entites = new List<T>();

            if (!dataTable.Columns.Contains("ReturnXML"))
                return;

            //Convert Dataset to entity
            foreach (DataRow row in dataTable.Rows)
                entites.Add(ConvertXMLStringToModel<T>(Convert.ToString(row["ReturnXML"])));

            if (entites?.Count > 0)
                mongoRepository.Create(entites);
        }

        public static ZnodePublishState GetPublishStatusByCode(string PublishStateCode)
        {
            ZnodeRepository<ZnodePublishState> _publishStateRepository = new ZnodeRepository<ZnodePublishState>();
            return _publishStateRepository.Table.FirstOrDefault(x => x.PublishStateCode == PublishStateCode);
        }

        private static void SetProductPublishStatus(List<int> typeIds, ZnodePublishCatalogLog publishCatalogLog)
        {
            IZnodeRepository<ZnodePimProduct> _pimProductRepository = new ZnodeRepository<ZnodePimProduct>();

            int pimCategoryId = typeIds.FirstOrDefault();
            if (pimCategoryId > 0)
            {
                ZnodePimProduct PimProduct = _pimProductRepository.Table.FirstOrDefault(x => x.PimProductId == pimCategoryId);
                PimProduct.PublishStateId = publishCatalogLog.PublishStateId;
                _pimProductRepository.Update(PimProduct);
            }
        }

        private static void SetCategoryPublishStatus(List<int> typeIds, ZnodePublishCatalogLog publishCatalogLog)
        {
            IZnodeRepository<ZnodePimCategory> _pimCategoryRepository = new ZnodeRepository<ZnodePimCategory>();
            IZnodeRepository<ZnodePublishCategory> _publishCategoryRepository = new ZnodeRepository<ZnodePublishCategory>();

            ZnodePublishCategory publishCategory = _publishCategoryRepository.Table.FirstOrDefault(x => x.PublishCategoryId == typeIds.FirstOrDefault());
            int pimCategoryId = publishCategory.PimCategoryId.HasValue ? publishCategory.PimCategoryId.Value : 0;
            if (pimCategoryId > 0)
            {
                ZnodePimCategory model = _pimCategoryRepository.GetById(pimCategoryId);
                model.PublishStateId = publishCatalogLog.PublishStateId;
                _pimCategoryRepository.Update(model);
            }
        }

        /// <summary>
        /// Execute SP and get result
        /// </summary>
        /// <param name="id">Id (catalog/Product)</param>
        /// <param name="parameterName1">parameter Name</param>
        /// <param name="userId">User Id</param>
        /// <param name="spName">Name of SP</param>
        /// <param name="versionId">Version Id</param>
        /// <param name="parameterName2">Parameter name</param>
        /// <param name="associategProductTypeName">Product Type</param>
        /// <returns>Dataset</returns>
        public static DataSet ExecuteSP(int id, string parameterName1, int userId, string spName, int? versionId = null, string parameterName2 = "", string associategProductTypeName = "", List<int> LocaleIds = null, ZnodePublishStatesEnum revisionType = ZnodePublishStatesEnum.PREVIEW)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter(parameterName1, id, ParameterDirection.Input, SqlDbType.Int);

            if (versionId > 0)
                executeSpHelper.GetParameter("@VersionId", versionId, ParameterDirection.Input, SqlDbType.Int);

            executeSpHelper.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@PublishStateId", (byte)revisionType, ParameterDirection.Input, SqlDbType.Int);

            if (LocaleIds?.Count > 0)
                executeSpHelper.SetTableValueParameter("@LocaleId", PublishHelper.ConvertKeywordListToDataTable(LocaleIds), ParameterDirection.Input, SqlDbType.Structured, "dbo.TransferId");

            if (!string.IsNullOrEmpty(parameterName2) && !string.IsNullOrEmpty(associategProductTypeName))
                executeSpHelper.GetParameter(parameterName2, associategProductTypeName, ParameterDirection.Input, SqlDbType.Text);
            return executeSpHelper.GetSPResultInDataSet(spName);
        }

        /// <summary>
        /// Execute SP with Json Parameters and get result.
        /// </summary>
        /// <param name="id">Id (catalog/Product)</param>
        /// <param name="parameterName1">parameter Name</param>
        /// <param name="userId">User Id</param>
        /// <param name="spName">Name of SP</param>
        /// <param name="versionId">Version Id</param>
        /// <param name="parameterName2">Parameter name</param>
        /// <param name="associategProductTypeName">Product Type</param>
        /// <returns>Dataset</returns>
        public static DataSet ExecuteSPWithJSONParameters(int id, string parameterName1, int userId, string spName, int? versionId = null, string parameterName2 = "", string associategProductTypeName = "", List<int> LocaleIds = null, ZnodePublishStatesEnum revisionType = ZnodePublishStatesEnum.PREVIEW)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter(parameterName1, id, ParameterDirection.Input, SqlDbType.Int);

            if (versionId > 0)
                executeSpHelper.GetParameter("@VersionId", versionId, ParameterDirection.Input, SqlDbType.Int);

            executeSpHelper.GetParameter("@UserId", userId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@PublishStateId", (byte)revisionType, ParameterDirection.Input, SqlDbType.Int);

            if (LocaleIds?.Count > 0)
                executeSpHelper.GetParameter("@LocaleId", PublishHelper.ConvertKeywordListToDataTable(LocaleIds)?.ToJson(), ParameterDirection.Input, SqlDbType.NVarChar);

            if (!string.IsNullOrEmpty(parameterName2) && !string.IsNullOrEmpty(associategProductTypeName))
                executeSpHelper.GetParameter(parameterName2, associategProductTypeName, ParameterDirection.Input, SqlDbType.Text);
            return executeSpHelper.GetSPResultInDataSet(spName);
        }

        public static void LogProgress(Guid jobId, string jobName, int progressMark, bool isCompleted = false, bool isFailed = false, Exception ex = null)
        {
            IMongoRepository<__ProgressNotifierEntity> progressNotificationRepository = new MongoRepository<__ProgressNotifierEntity>();
            IMongoQuery query = Query<__ProgressNotifierEntity>.EQ(x => x.JobId, jobId);

            __ProgressNotifierEntity progressNotifierEntity = progressNotificationRepository.GetEntity(query, true);

            if (IsNotNull(progressNotifierEntity))
            {
                progressNotifierEntity.JobName = jobName;
                progressNotifierEntity.ProgressMark = progressMark;
                progressNotifierEntity.IsCompleted = isCompleted;
                progressNotifierEntity.IsFailed = isFailed;
                progressNotifierEntity.ExceptionMessage = IsNotNull(ex) ? ex.GetBaseException().Message : null;

                progressNotificationRepository.UpdateEntity(query, progressNotifierEntity, true);
            }
            else
            {
                int userId = GetLoginUserId();
                UserModel user = GetUserDetailsByUserId(userId);

                progressNotificationRepository.Create(
                 new __ProgressNotifierEntity()
                 {
                     Id = ObjectId.GenerateNewId(),
                     JobId = jobId,
                     JobName = jobName,
                     ProgressMark = progressMark,
                     IsCompleted = isCompleted,
                     IsFailed = isFailed,
                     ExceptionMessage = IsNotNull(ex) ? ex.GetBaseException().Message : null,
                     StartedBy = userId,
                     StartedByFriendlyName = user?.UserName
                 });
            }

            //Delete older and suspended notifications if any.
            progressNotificationRepository.DeleteByQuery(Query<__ProgressNotifierEntity>.Where(x => x.JobName == jobName && x.JobId != jobId));
        }

        public static void DeleteHistoricalProgressNotifications(IMongoRepository<__ProgressNotifierEntity> _ProgressNotifierMongoRepository)
        {
            _ProgressNotifierMongoRepository.DeleteByQuery(Query.Or(
                    Query<__ProgressNotifierEntity>.EQ(x => x.IsFailed, true),
                    Query<__ProgressNotifierEntity>.EQ(x => x.IsCompleted, true)), true);
        }

        /// <summary>
        /// Copy Entities For Preview for production on category publish with temp version.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="versionId">Current version id</param>
        /// <param name="query">Query with all required filters</param>
        /// <param name="repository">Repository name or Entity name</param>
        public static void CopyEntitiesForPreviewAndProductionOnCategoryPublishWithTempVersion<T>(long publishStartTime, IMongoQuery query, IMongoRepository<T> repository) where T : MongoEntity , IDisposable, new()
        {
            List<T> entites = repository.GetEntityList(query, true);

            Parallel.ForEach(entites, (obj) => { obj.VersionId = ZnodeConstant.TemporaryVersionId; obj.PublishStartTime = publishStartTime ; obj.Id = ObjectId.GenerateNewId(); });

            if (entites?.Count() > 0)
                repository.Create(entites);
        }

        private static int GetLoginUserId()
        {
            const string headerUserId = "Znode-UserId";
            int userId = 0;
            var headers = HttpContext.Current.Request.Headers;
            int.TryParse(headers[headerUserId], out userId);
            return userId;
        }

        private static UserModel GetUserDetailsByUserId(int userId)
        {
            if (userId > 0)
            {
                IZnodeRepository<ZnodeUser> _userRepository = new ZnodeRepository<ZnodeUser>();
                IZnodeRepository<AspNetUser> _aspNetUserRepository = new ZnodeRepository<AspNetUser>();
                IZnodeRepository<AspNetZnodeUser> _aspNetZnodeUserRepository = new ZnodeRepository<AspNetZnodeUser>();

                return (from user in _userRepository.Table
                        join aspNetUser in _aspNetUserRepository.Table on user.AspNetUserId equals aspNetUser.Id
                        join aspNetZnodeUser in _aspNetZnodeUserRepository.Table on aspNetUser.UserName equals aspNetZnodeUser.AspNetZnodeUserId
                        where user.UserId == userId
                        select new UserModel
                        {
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            UserName = aspNetZnodeUser.UserName
                        })?.FirstOrDefault();
            }
            return new UserModel();
        }
    }

    public class PublishProcessor
    {
        private readonly Guid _JobId;

        public PublishProcessor()
        {
            _JobId = Guid.NewGuid();
        }

        public delegate bool DelegateStoreCopyWithinMongo(WebStoreEntity existingWebstoreCopy, ZnodePublishStatesEnum targetPublishState, List<string> publishContent);
        public delegate bool DelegateStoreCopyFromSQLToMongo(int portalId, int localeId, ZnodePublishStatesEnum targetPublishState, List<string> publishContent);

        public delegate bool DelegateCopyIndividualEntityWithinMongo(string id, WebStoreEntity existingWebstoreCopy, ZnodePublishStatesEnum targetPublishState, int additionalParameter = 0);
        public delegate bool DelegateCopyIndividualEntityFromSQLToMongo(string id, int portalId, int localeId, ZnodePublishStatesEnum targetPublishState, ref bool breakWithSuccess, int additionalParameter = 0);

        public bool PublishCompleteWebstore(int portalId, DelegateStoreCopyWithinMongo copyWithinMongoDelegate, DelegateStoreCopyFromSQLToMongo copyFromSQLToMongoDelegate, string targetPublishState = null, string publishContent = null, bool takeFromDraftFirst = false, bool asyncFlag = false)
        {
            string storeName = new ZnodeRepository<ZnodePortal>().Table?.FirstOrDefault(x => x.PortalId == portalId)?.StoreName;

            string storePublishProgressMessage = IsNotNull(storeName) ? storeName + " Store" : "Store";

            PreviewHelper.LogProgress(_JobId, storePublishProgressMessage, 5);

            List<bool> result = new List<bool>();

            List<string> publishContentList = EnumerateCommaSeparatedString(publishContent);

            List<int> activeLocaleIds = GetAvailableLocalesForPortal(portalId);

            bool previewEnabled = IsWebstorePreviewEnabled();

            int localeCounter = 0;

            foreach (int localeId in activeLocaleIds)
            {
                try
                {
                    if (previewEnabled)
                    {
                        ZnodePublishStatesEnum targetStateEnum;

                        if (string.IsNullOrEmpty(targetPublishState) || GetDefaultPublishState().ToString() == targetPublishState || !Enum.TryParse(targetPublishState, out targetStateEnum))
                        {
                            //Get the Non-Default State.
                            ZnodePublishStatesEnum nonDefaultPublishState;

                            if (NonDefaultPublishStateExists(out nonDefaultPublishState))
                                result.Add(PublishWebstoreToDefault(portalId, localeId, copyWithinMongoDelegate, copyFromSQLToMongoDelegate, nonDefaultPublishState, publishContentList, takeFromDraftFirst));
                            else
                                result.Add(copyFromSQLToMongoDelegate(portalId, localeId, GetDefaultPublishState(), publishContentList));
                        }
                        else
                        {
                            result.Add(copyFromSQLToMongoDelegate(portalId, localeId, targetStateEnum, publishContentList));
                        }
                    }
                    else
                    {
                        //Push to Mongo (GetDefaultPublishState())
                        result.Add(copyFromSQLToMongoDelegate(portalId, localeId, GetDefaultPublishState(), publishContentList));
                    }

                    localeCounter += 1;

                    PreviewHelper.LogProgress(_JobId, storePublishProgressMessage, localeCounter * 100 / activeLocaleIds.Count);
                }
                catch (Exception ex)
                {
                    if (asyncFlag)
                    {
                        PreviewHelper.LogProgress(_JobId, storePublishProgressMessage, 100, false, true, ex);
                        result.Add(false);
                    }
                    else
                        throw;
                }
            }

            if (result.Contains(false))
            {
                return false;
            }
            else
            {
                PreviewHelper.LogProgress(_JobId, storePublishProgressMessage, localeCounter * 100 / activeLocaleIds.Count, true);
                return true;
            }

        }
        public bool PublishIndividualCMSEntity(string id, int portalId, DelegateCopyIndividualEntityWithinMongo copyWithinMongoDelegate, DelegateCopyIndividualEntityFromSQLToMongo copyFromSQLToMongoDelegate, int localeId = 0, string targetPublishState = null, bool takeFromDraftFirst = false, int additionalParameter = 0)
        {
            List<bool> result = new List<bool>();

            List<int> activeLocaleIds = localeId > 0 ? new List<int>() { localeId } : GetAvailableLocalesForPortal(portalId);
            
            bool previewEnabled = IsWebstorePreviewEnabled();
            
            bool breakWithSuccess = false;

            foreach (int currentLocaleId in activeLocaleIds)
            {
                if (previewEnabled)
                {
                    ZnodePublishStatesEnum targetStateEnum;

                    if (string.IsNullOrEmpty(targetPublishState) || GetDefaultPublishState().ToString() == targetPublishState || !Enum.TryParse(targetPublishState, out targetStateEnum))
                    {
                        //Get the Non-Default State.
                        ZnodePublishStatesEnum nonDefaultPublishState;

                        if (NonDefaultPublishStateExists(out nonDefaultPublishState))
                        {
                            result.Add(PublishIndividualCMSEntityToDefault(portalId, currentLocaleId, id, copyWithinMongoDelegate, copyFromSQLToMongoDelegate, nonDefaultPublishState, takeFromDraftFirst, additionalParameter));
                        }
                        else
                        {
                            result.Add(copyFromSQLToMongoDelegate(id, portalId, currentLocaleId, GetDefaultPublishState(), ref breakWithSuccess, additionalParameter));
                        }
                    }
                    else
                    {
                        result.Add(copyFromSQLToMongoDelegate(id, portalId, currentLocaleId, targetStateEnum, ref breakWithSuccess, additionalParameter));
                    }
                }
                else
                {
                    //Push to Mongo (GetDefaultPublishState())
                    result.Add(copyFromSQLToMongoDelegate(id, portalId, currentLocaleId, GetDefaultPublishState(), ref breakWithSuccess, additionalParameter));
                }
            }

            if (result.Contains(false))
                return false;
            else
                return true;
        }

        #region Private Methods

        // Publish the webstore and all the CMS content to default publish state (PRODUCTION in usual usage).
        private bool PublishWebstoreToDefault(int portalId, int localeId, DelegateStoreCopyWithinMongo copyWithinMongoDelegate, DelegateStoreCopyFromSQLToMongo copyFromSQLToMongoDelegate, ZnodePublishStatesEnum sourceContentState, List<string> publishContent, bool publishFromDraft = false)
        {
            //Publish from any other content state to default publish state (PRODUCTION in most cases).

            bool publishResult = false;

            ZnodePublishStatesEnum defaultPublishState = GetDefaultPublishState();

            WebStoreEntity existingWebstoreCopy = PreviewHelper.GetWebstore(portalId, sourceContentState, localeId);

            if (publishFromDraft || IsNull(existingWebstoreCopy))
            {
                //If source does not exist. Publish to source first.
                bool isPublished = copyFromSQLToMongoDelegate(portalId, localeId, sourceContentState, publishContent);
                existingWebstoreCopy = isPublished ? PreviewHelper.GetWebstore(portalId, sourceContentState, localeId) : null;
            }

            if (IsNotNull(existingWebstoreCopy))
            {
                //By this point it is sure that the source has already been published. Now copy the data from source to default state.
                publishResult = copyWithinMongoDelegate(existingWebstoreCopy, defaultPublishState, publishContent);
            }

            return publishResult;
        }

        private bool PublishIndividualCMSEntityToDefault(int portalId, int localeId, string entityId, DelegateCopyIndividualEntityWithinMongo copyWithinMongoDelegate, DelegateCopyIndividualEntityFromSQLToMongo copyFromSQLToMongoDelegate, ZnodePublishStatesEnum sourceContentState, bool publishFromDraft = false, int additionalParameter = 0)
        {
            //Publish from any other content state to default publish state (PRODUCTION in most cases).

            bool publishResult = false;
            bool breakWithSuccess = false;
            ZnodePublishStatesEnum defaultPublishState = GetDefaultPublishState();

            WebStoreEntity existingWebstoreCopy = PreviewHelper.GetWebstore(portalId, sourceContentState, localeId);

            if (publishFromDraft || IsNull(existingWebstoreCopy))
            {
                //If source does not exist. Publish to source first.
                bool isPublished = copyFromSQLToMongoDelegate(entityId, portalId, localeId, sourceContentState, ref breakWithSuccess, additionalParameter);
                existingWebstoreCopy = isPublished ? PreviewHelper.GetWebstore(portalId, sourceContentState, localeId) : null;
            }

            if (breakWithSuccess)
            {
                //This block is only for the cases where the portal exists in the Mongo but the individual entity has not been associated to the portal.
                //Hence, it should skip/break all further steps gracefully with status true.
                publishResult = true;
            }
            else
            {
                if (IsNotNull(existingWebstoreCopy))
                {
                    publishResult = copyWithinMongoDelegate(entityId, existingWebstoreCopy, defaultPublishState, additionalParameter);
                }
            }

            return publishResult;
        }

        private List<string> EnumerateCommaSeparatedString(string commaSeparatedString)
        {
            return !string.IsNullOrEmpty(commaSeparatedString) ? commaSeparatedString.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : null;
        }

        private List<int> GetAvailableLocalesForPortal(int portalId)
        {
            return new ZnodeRepository<ZnodePortalLocale>()?.Table?.Where(x => x.PortalId == portalId)?.Select(x => x.LocaleId)?.ToList();
        }

        private List<PublishStateMappingModel> GetAvailablePublishStateMappings()
        {
            IZnodeRepository<ZnodePublishStateApplicationTypeMapping> _publishStateMappingRepository = new ZnodeRepository<ZnodePublishStateApplicationTypeMapping>();
            IZnodeRepository<ZnodePublishState> _publishStateRepository = new ZnodeRepository<ZnodePublishState>();

            List<PublishStateMappingModel> publishStateMappings = (from PSATM in _publishStateMappingRepository.Table
                                                                   join PS in _publishStateRepository.Table on PSATM.PublishStateId equals PS.PublishStateId
                                                                   where PSATM.IsActive
                                                                   select new PublishStateMappingModel
                                                                   {
                                                                       PublishStateMappingId = PSATM.PublishStateMappingId,
                                                                       ApplicationType = PSATM.ApplicationType,
                                                                       PublishStateCode = PS.PublishStateCode,
                                                                       Description = PSATM.Description,
                                                                       IsDefault = PS.IsDefaultContentState,
                                                                       IsEnabled = PSATM.IsEnabled,
                                                                       PublishStateId = PSATM.PublishStateId,
                                                                       PublishState = PS.PublishStateCode
                                                                   }).ToList();

           
            return publishStateMappings;
        }

        private bool IsWebstorePreviewEnabled()
        {
            return GetAvailablePublishStateMappings()?.Count(x => !x.IsDefault && x.IsEnabled) > 0;
        }

        private bool NonDefaultPublishStateExists(out ZnodePublishStatesEnum publishState)
        {
            string nonDefaultPublishState = GetAvailablePublishStateMappings()?.Where(x => !x.IsDefault && x.IsEnabled)?.FirstOrDefault()?.PublishStateCode;
            
            if (!string.IsNullOrEmpty(nonDefaultPublishState))
                return Enum.TryParse(nonDefaultPublishState, out publishState);

            publishState = 0;
            return false;
        }

        private ZnodePublishStatesEnum GetDefaultPublishState()
        {
            IZnodeRepository<ZnodePublishState> _publishStateRepository = new ZnodeRepository<ZnodePublishState>();
            string publishStateCode = _publishStateRepository.Table.Where(x => x.IsContentState && x.IsDefaultContentState)?.FirstOrDefault()?.PublishStateCode;
           
            ZnodePublishStatesEnum publishState;

            if (!string.IsNullOrEmpty(publishStateCode) && Enum.TryParse(publishStateCode, true, out publishState))
                return publishState;
            else
                return ZnodePublishStatesEnum.PRODUCTION;
        }

        #endregion Private Methods
    }
}