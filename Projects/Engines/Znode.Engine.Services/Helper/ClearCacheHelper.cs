﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Engine.Services.Helper
{
    public static class ClearCacheHelper
    {
        public static CacheConfiguration CacheConfig { get; set;}
        #region Public Method
        //Clear API and Webstore Cache
        public static void ClearCacheAfterPublish(string portalId)
        {
            FilterCollection filter = new FilterCollection();
            if (!string.IsNullOrEmpty(portalId))
                filter.Add(ZnodeDomainEnum.PortalId.ToString(), FilterOperators.In, portalId);
            filter.Add(ZnodeDomainEnum.IsActive.ToString(), FilterOperators.Equals, "True");

            //Clears API cache
            ClearApiCache();

            //Clear webstore cache
            ClearCache(filter);
        }

        //Clears API cache
        public static void ClearApiCache()
        {
            List<string> cacheItemsNotToRemove = CacheItemNotToRemove();
            //Get API Cache
            IDictionaryEnumerator cacheEnumerator = HttpRuntime.Cache.GetEnumerator();
            //Clear all cached items.
            try
            {
                while (cacheEnumerator.MoveNext())
                {
                    if (cacheEnumerator.Key != null && !cacheItemsNotToRemove.Contains(cacheEnumerator.Key.ToString()))
                        HttpRuntime.Cache.Remove(cacheEnumerator.Key.ToString());
                }
                //Insert cache refresh time into database after refreshing API cache
                InsertCacheRefreshTimeIntoDatabase(ApplicationCacheTypeEnum.ApiCache.ToString());

                //Call the Clear Cache API Using Load balancer Domain.
                CallClearCacheOnAPILoadBalancerDomain();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"Failed to clear api cache", "CacheLog");
                ZnodeLogging.LogMessage(ex.Message, "CacheLog", TraceLevel.Error, ex);
            }
        }

        public static void ClearApiCache(string key)
        {
            List<string> cacheItemsNotToRemove = CacheItemNotToRemove();
            //Get API Cache
            IDictionaryEnumerator cacheEnumerator = HttpRuntime.Cache.GetEnumerator();

            CacheConfig = (CacheConfiguration)ConfigurationManager.GetSection("ZnodeApiCache");

            string template = GetTemplate(key);

            //Clear  cached items.
            try
            {
                while (cacheEnumerator.MoveNext())
                {
                    if (cacheEnumerator.Key != null && (!cacheItemsNotToRemove.Contains(cacheEnumerator.Key.ToString())) &&(cacheEnumerator.Key.ToString().Contains(template)))
                    {
                        HttpRuntime.Cache.Remove(cacheEnumerator.Key.ToString());
                    }
                }

                //Call the Clear Cache API Using Load balancer Domain.
                CallClearCacheOnAPILoadBalancerDomain(key);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"Failed to clear api cache", "CacheLog");
                ZnodeLogging.LogMessage(ex.Message, "CacheLog", TraceLevel.Error, ex);
            }
        }

        public static string GetTemplate(string key)
        {
            string template = CacheConfig.CacheRoutes.FirstOrDefault(x => x.Key == key)?.Template;
            if (template != null)
                return template.LastIndexOf('{') > 0 ? template?.Substring(0, template.LastIndexOf('{')) : template;

            return string.Empty;

        }


        //Clear webstore cache.
        public static void ClearWebstoreCache(string domainIds)
        {
            if (string.IsNullOrEmpty(domainIds)) return;
            FilterCollection filter = new FilterCollection
            {
                {ZnodeDomainEnum.IsActive.ToString(), FilterOperators.Equals, "True"},
                {ZnodeDomainEnum.DomainId.ToString(), FilterOperators.In, domainIds}
            };

            //Clear webstore cache
            ClearCache(filter);
        }



        //Clear webstore cache.
        public static void ClearWebstoreCache(string portalIds, string key)
        {
            if (!string.IsNullOrEmpty(portalIds))
            {
                FilterCollection filter = new FilterCollection
                {
                    {ZnodeDomainEnum.IsActive.ToString(), FilterOperators.Equals, "True"},
                    {ZnodeDomainEnum.PortalId.ToString(), FilterOperators.In, portalIds}
                };

                //Clear webstore cache
                ClearCache(filter, key);
            }
        }

        public static void ReInitializeWebstoreCacheForLogConfiguration(string portalIds, string key)
        {
            if (!string.IsNullOrEmpty(portalIds))
            {
                FilterCollection filter = new FilterCollection
                {
                    {ZnodeDomainEnum.IsActive.ToString(), FilterOperators.Equals, "True"},
                    {ZnodeDomainEnum.PortalId.ToString(), FilterOperators.In, portalIds}
                };

                //Clear webstore cache
                ReInitializeCacheForLogConfiguration(filter, key);
            }
        }
        #endregion

        #region Private method
        //Clear webstore cache
        private static void ClearCache(FilterCollection filter)
        {
            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection());

            IZnodeRepository<ZnodeDomain> domainRepository = new ZnodeRepository<ZnodeDomain>();
            List<string> domainList = domainRepository.GetEntityList(whereClauseModel.WhereClause, whereClauseModel.FilterValues)?.Where(x => x.ApplicationType.Equals(ApplicationTypesEnum.WebStore.ToString(), StringComparison.InvariantCultureIgnoreCase) || x.ApplicationType.Equals(ApplicationTypesEnum.WebstorePreview.ToString(), StringComparison.InvariantCultureIgnoreCase))?.Select(x => x.DomainName)?.ToList();

            if (domainList?.Count <= 0) return;
            var urlList = new List<string>();

            //Set the Webstore Load Balancer node Ips based on the Global Setting.
            SetWebStoreLoadBalancerDomain(domainList);

            if (HelperUtility.IsNotNull(domainList))
            {
                foreach (string domain in domainList)
                {
                    urlList.Add($"http://{domain}/Home/ClearCacheAfterPublish");
                    urlList.Add($"https://{domain}/Home/ClearCacheAfterPublish");
                }
            }

            //Call the Clear Cache based on the url.
            CallClearCacheByUrl(urlList);

            //Insert cache refresh time into database after refreshing webstore cache
            InsertCacheRefreshTimeIntoDatabase(ApplicationCacheTypeEnum.FullPageCache.ToString());
        }

        //Insert cache refresh time into database after refreshing respective cache
        public static void InsertCacheRefreshTimeIntoDatabase(string cacheType)
        {
            IZnodeRepository<ZnodeApplicationCache> _applicationCacheRepository = new ZnodeRepository<ZnodeApplicationCache>();
            ZnodeApplicationCache znodeApplicationCache = _applicationCacheRepository.Table.FirstOrDefault(x => x.ApplicationType == cacheType);
            if (HelperUtility.IsNotNull(znodeApplicationCache))
            {
                znodeApplicationCache.StartDate = DateTime.Now;
                ZnodeLogging.LogMessage(_applicationCacheRepository.Update(znodeApplicationCache) ? "Cache succesfully updated." : "Failed to update cache.", string.Empty, TraceLevel.Info);
            }
        }
        private static void ClearCache(FilterCollection filter, string key)
        {
            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection());

            IZnodeRepository<ZnodeDomain> domainRepository = new ZnodeRepository<ZnodeDomain>();
            List<string> domainList = domainRepository.GetEntityList(whereClauseModel.WhereClause, whereClauseModel.FilterValues)?.Where(x => x.ApplicationType.Equals(ApplicationTypesEnum.WebStore.ToString(), StringComparison.InvariantCultureIgnoreCase) || x.ApplicationType.Equals(ApplicationTypesEnum.WebstorePreview.ToString(), StringComparison.InvariantCultureIgnoreCase))?.Select(x => x.DomainName)?.ToList();

            if (domainList?.Count <= 0) return;
            var urlList = new List<string>();

            //Set the Webstore Load Balancer node Ips based on the Global Setting.
            SetWebStoreLoadBalancerDomain(domainList);

            if (HelperUtility.IsNotNull(domainList))
            {
                foreach (string domain in domainList)
                {
                    urlList.Add($"http://{domain}/Home/ClearPartialCache?key={key}");
                    urlList.Add($"https://{domain}/Home/ClearPartialCache?key={key}");
                }
            }

            //Call the Clear Cache based on the url.
            CallClearCacheByUrl(urlList);
        }

        private static void ReInitializeCacheForLogConfiguration(FilterCollection filter, string key)
        {
            EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filter.ToFilterDataCollection());

            IZnodeRepository<ZnodeDomain> domainRepository = new ZnodeRepository<ZnodeDomain>();
            var domainList = domainRepository.GetEntityList(whereClauseModel.WhereClause, whereClauseModel.FilterValues)?.Where(x => x.ApplicationType.Equals(ApplicationTypesEnum.WebStore.ToString(), StringComparison.InvariantCultureIgnoreCase) || x.ApplicationType.Equals(ApplicationTypesEnum.WebstorePreview.ToString(), StringComparison.InvariantCultureIgnoreCase))?.Select(x => x.DomainName)?.ToList();

            if (domainList?.Count <= 0) return;
            var urlList = new List<string>();

            //Set the Webstore Load Balancer node Ips based on the Global Setting.
            SetWebStoreLoadBalancerDomain(domainList);

            if (HelperUtility.IsNotNull(domainList))
            {
                foreach (string domain in domainList)
                {
                    urlList.Add($"http://{domain}/Home/ReInitializePartialCacheForLogConfiguration?key={key}");
                    urlList.Add($"https://{domain}/Home/ReInitializePartialCacheForLogConfiguration?key={key}");
                }
            }

            //Call the Clear Cache based on the url.
            CallClearCacheByUrl(urlList);
        }

        //This item will not be remove from cache.
        public static List<string> CacheItemNotToRemove()
        {
            var cacheItemsNotToRemove = new List<string>
            {
                "CartPromotionCache",
                "PricePromotionCache",
                "ProductPromotionCache",
                "PromotionTypesCache",
                "ShippingTypesCache",
                "TaxTypesCache",
                "DefaultGlobalConfigCache",
                "AllPromotionCache"
            };

            return cacheItemsNotToRemove;
        }

        //Call the Clear Cache call on respective domain url based on the domain url list.
        private static void CallClearCacheByUrl(List<string> urlList)
        {
            //When the "ClearOnlyHttpsDomainCache" global setting in True, then pass only Https domain url for clear cache.
            //If setting is False, then it will pass both Http & Https domain url as per existing behaviour.
            if (DefaultGlobalConfigSettingHelper.IsClearOnlyHttpsDomainCache && urlList?.Count > 0)
            {
                urlList = urlList.Where(x => x.ToLower().Contains("https://")).ToList();
            }

            if (IsNotNull(urlList))
                {
                foreach (string url in urlList)
                {
                    HttpWebResponse response = null;

                    try
                    {
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                        response = (HttpWebResponse)request.GetResponse();
                    }
                    catch (WebException ex)
                    {
                        ZnodeLogging.LogMessage(ex.Message, "CacheLog", TraceLevel.Warning, ex);
                        ZnodeLogging.LogMessage($"Failed to clear cache for store {url}", "CacheLog");
                        /* A WebException will be thrown if the status of the response is not `200 OK` */
                    }
                    catch (Exception ex)
                    {
                        ZnodeLogging.LogMessage(ex.Message, "CacheLog", TraceLevel.Error, ex);
                        ZnodeLogging.LogMessage($"Failed to clear cache for store {url}", "CacheLog");
                        /* A WebException will be thrown if the status of the response is not `200 OK` */
                    }
                    finally
                    {
                        // Don't forget to close your response.
                        if (HelperUtility.IsNotNull(response))
                            response?.Close();
                    }
                }
            }
        }


        //Set the Webstore Load Balancer Domain from Global Setting i.e."ClearWebStoreLoadBalancerCacheIPs".
        //Need to set the Private IPs of all the nodes within the WebStore Load Balancing envirnment.
        //Required to Clear the Webstore cache in Load Balancing envirnment.
        private static void SetWebStoreLoadBalancerDomain(List<string> domains)
        {
            GlobalSettingValues featureSubValues = DefaultGlobalConfigSettingHelper.DefaultClearLoadBalancerWebStoreCacheIPs;
            if (domains?.Count > 0 && !string.IsNullOrEmpty(featureSubValues?.Value1))
            {
                foreach (string domain in featureSubValues.Value1.Split('~'))
                {
                    if (!string.IsNullOrEmpty(domain))
                        domains.Add(domain);
                }
            }
        }

        //Call the Clear Cache call on all the nodes of Load Balancer envirnement.
        //Call the Clear Cache call on all Load Balancer nodes from Global Setting i.e."ClearLoadBalancerAPICacheIPs".
        //Need to set the Private IPs of all the nodes within the API Load Balancing envirnment.
        //Required to Clear the API cache in Load Balancing envirnment.
        private static void CallClearCacheOnAPILoadBalancerDomain(string key = "")
        {
            GlobalSettingValues featureSubValues = DefaultGlobalConfigSettingHelper.DefaultClearLoadBalancerAPICacheIPs;
            if (!string.IsNullOrEmpty(featureSubValues?.Value1))
            {
                List<string> domains = new List<string>();
                foreach (string domain in featureSubValues.Value1.Split('~'))
                {
                    if (!string.IsNullOrEmpty(domain))
                        domains.Add(domain);
                }

                if (domains?.Count > 0)
                {
                    List<string> urlList = new List<string>();
                    foreach (string domain in domains)
                    {
                        if (!string.IsNullOrEmpty(key))
                        {
                            urlList.Add($"http://{domain}/Home/ClearApiCacheByKey?key={key}");
                            urlList.Add($"https://{domain}/Home/ClearApiCacheByKey?key={key}");
                        }
                        else
                        {
                            urlList.Add($"http://{domain}/Home/ClearApiCache");
                            urlList.Add($"https://{domain}/Home/ClearApiCache");
                        }
                    }
                    //Call the Clear Cache based on the url.
                    CallClearCacheByUrl(urlList);
                }
            }

        }
            #endregion
    }
}
