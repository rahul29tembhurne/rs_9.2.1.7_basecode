﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Znode.Engine.Api.Models;

namespace Znode.Engine.TaskScheduler
{
    public class ProductFeedHelper : BaseScheduler, ISchdulerProviders
    {
        #region MyRegion
        private const string SchedulerName = "ProductFeedScheduler";
        private string TokenBasedAuthToken = string.Empty;
        #endregion
        public void InvokeMethod(string[] args)
        {
            ProductFeedModel model = GetModelFromArguments(args[1]);
            if (!Equals(model, null))
            {
                CallProductFeedAPI(model);
            }
        }

        private void CallProductFeedAPI(ProductFeedModel model)
        {
            string data = JsonConvert.SerializeObject(model);
            var dataBytes = Encoding.UTF8.GetBytes(data);
            string jsonString = string.Empty;
            string message = string.Empty;
            string requestPath = $"{model.SuccessXMLGenerationMessage}/productfeed";
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestPath);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Timeout = base.requesttimeout;
                request.ContentLength = dataBytes.Length;
                request.Headers.Add($"Znode-UserId: {model.UserId}");
                request.Headers.Add($"Authorization: {"Basic " + model.Token}");
                if (!string.IsNullOrEmpty(TokenBasedAuthToken) && TokenBasedAuthToken != "0")
                    request.Headers.Add($"Token: {TokenBasedAuthToken}");
                using (var reqStream = request.GetRequestStream())
                {
                    reqStream.Write(dataBytes, 0, dataBytes.Length);
                }
                using (HttpWebResponse responce = (HttpWebResponse)request.GetResponse())
                {
                    Stream datastream = responce.GetResponseStream();
                    StreamReader reader = new StreamReader(datastream);
                    jsonString = reader.ReadToEnd();
                    reader.Close();
                    datastream.Close();
                    LogMessage("API Call Successfully.", SchedulerName);
                    LogMessage($"Product feed data {jsonString}", SchedulerName);
                }
            }
            catch (WebException webex)
            {
                if (CheckTokenIsInvalid(webex))
                {
                    TokenBasedAuthToken = GetToken(model.SuccessXMLGenerationMessage, model.Token);
                    CallProductFeedAPI(model);
                }
                else
                    LogMessage($"Failed : {webex.Message}, stack: {webex.StackTrace}", SchedulerName);
            }
            catch (Exception ex)
            {
                LogMessage($"Failed : {ex.Message}, stack: {ex.StackTrace}", SchedulerName);
            }
        }

        private ProductFeedModel GetModelFromArguments(string arguments)
        {
            if (!string.IsNullOrEmpty(arguments))
            {
                ProductFeedModel model = new ProductFeedModel();
                string[] argsArray = arguments.Split('#');

                model.ProductFeedId = int.Parse(argsArray[0]);
                model.LocaleId = int.Parse(argsArray[1]);
                model.ProductFeedTimeStampName = Convert.ToString(argsArray[2]);
                model.ProductFeedPriority = Convert.ToDecimal(argsArray[3]);
                model.ProductFeedSiteMapTypeCode = Convert.ToString(argsArray[4]);
                model.ProductFeedTypeCode = Convert.ToString(argsArray[5]);
                model.Title = Convert.ToString(argsArray[6]);
                model.Link = Convert.ToString(argsArray[7]);
                model.Description = Convert.ToString(argsArray[8]);
                model.FileName = Convert.ToString(argsArray[9]);
                model.PortalId = argsArray[10].Split(',').Select(int.Parse).ToArray();
                model.SuccessXMLGenerationMessage = Convert.ToString(argsArray[11]);
                model.IsFromScheduler = true;
                model.UserId = int.Parse(argsArray[13]);
                model.Token = argsArray[14];
                if (argsArray.Length > 15)
                    TokenBasedAuthToken = argsArray[15];
                // args[16] contains request timeout value.
                if (argsArray.Length > 16 && !string.IsNullOrEmpty(argsArray[16]))
                    base.requesttimeout = int.Parse(argsArray[16]);
                return model;
            }
            else
                return null;
        }
    }
}
