﻿namespace Znode.Engine.Api.Client.Endpoints
{
    public class GeneralSettingEndpoint : BaseEndpoint
    {
        //Endpoint to get lists of General Settings.
        public static string List() => $"{ApiRoot}/generalsetting/list";

        //Endpoint update the existing General Setting.
        public static string Update() => $"{ApiRoot}/generalsetting/update";

        //Endpoint to get cache management data.
        public static string CacheData() => $"{ApiRoot}/generalsetting/getcachedata";

        //Endpoint to create/update cache data.
        public static string CreateUpdateCache() => $"{ApiRoot}/generalsetting/createupdatecache";

        //Endpoint to refresh cache data.
        public static string RefreshCacheData() => $"{ApiRoot}/generalsetting/refreshcache";

        //Endpoint to get lists of all publish states.
        public static string PublishStateMappingList() => $"{ApiRoot}/publishstatemapping/list";

        //Endpoint to enable/disable publish state to application type mapping.
        public static string EnableDisablePublishStateMapping(bool isEnabled) => $"{ApiRoot}/publishstatemapping/enabledisable/{isEnabled}";
    }
}
