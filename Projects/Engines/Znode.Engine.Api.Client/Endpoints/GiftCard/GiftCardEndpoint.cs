﻿namespace Znode.Engine.Api.Client.Endpoints
{
    public class GiftCardEndpoint : BaseEndpoint
    {
        //Get GiftCard list Endpoint.
        public static string GetGiftCardList() => $"{ApiRoot}/giftcard/list";

        //Get RandomGiftCardNumber Endpoint.
        public static string GetRandomGiftCardNumber() => $"{ApiRoot}/giftcard/getrandomgiftcardnumber";

        //Create GiftCard Endpoint.
        public static string CreateGiftCard() => $"{ApiRoot}/giftcard/create";

        //Get GiftCard on the basis of GiftCard id Endpoint.
        public static string GetGiftCard(int giftCardId) => $"{ApiRoot}/giftcard/{giftCardId}";

        //Update GiftCard Endpoint.
        public static string UpdateGiftCard() => $"{ApiRoot}/giftcard/update";

        //Delete GiftCard Endpoint.
        public static string DeleteGiftCard() => $"{ApiRoot}/giftcard/delete";

        //Get gift card history for a user.
        public static string GetGiftCardHistoryList() => $"{ApiRoot}/giftcard/getgiftcardhistorylist";
    }
}
