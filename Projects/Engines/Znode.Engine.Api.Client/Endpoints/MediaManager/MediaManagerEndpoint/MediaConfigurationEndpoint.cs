﻿namespace Znode.Engine.Api.Client.Endpoints
{
    public class MediaConfigurationEndpoint : BaseEndpoint
    {
        //Endpoint to get media server list.
        public static string GetMediaServerList() => $"{ApiRoot}/getmediaserver/list";

        //Endpoint to update existing media configuration setting.
        public static string Update() => $"{ApiRoot}/mediaconfiguration/update";

        //Endpoint to get media configuration setting.
        public static string GetMediaConfiguration() => $"{ApiRoot}/mediaconfiguration";

        //Endpoint to create new media configuration setting.
        public static string Create() => $"{ApiRoot}/mediaconfiguration/create";

        //Endpoin to get default media configuration setting.
        public static string GetDefaultMediaConfiguration() => $"{ApiRoot}/defaultmediaconfiguration";

        //Endpoint to sync new media.
        public static string SyncMedia(string folderName) => $"{ApiRoot}/mediaconfiguration/syncmedia/{folderName}";
    }
}
