﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Client
{
    public class SiteMapClient : BaseClient, ISiteMapClient
    {
        #region Public Method.
        // Gets the list of Publish Product List.
        public virtual PublishProductListModel GetPublishProductList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            //Get Endpoint.
            string endpoint = PublishProductEndpoint.GetPublishProductForSiteMap();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            //Get response
            ApiStatus status = new ApiStatus();
            PublishProductListResponse response = GetResourceFromEndpoint<PublishProductListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            PublishProductListModel list = new PublishProductListModel { PublishProducts = response?.PublishProducts.Select(x => new PublishProductModel { Name = x.Name, PublishProductId = x.PublishProductId, CategoryName = x.CategoryName, SEOUrl = x.SEOUrl }).ToList() };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #endregion
    }
}
