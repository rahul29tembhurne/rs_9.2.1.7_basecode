﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Client
{
    public class GeneralSettingClient : BaseClient, IGeneralSettingClient
    {
        //Method to get list of all GeneralSettings.
        public virtual GeneralSettingModel list()
        {
            //Get Endpoint.
            string endpoint = GeneralSettingEndpoint.List();

            //Get response
            ApiStatus status = new ApiStatus();
            GeneralSettingResponse response = GetResourceFromEndpoint<GeneralSettingResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };

            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response.GeneralSetting;
        }

        //Method to update existing GeneralSettings.
        public virtual bool Update(GeneralSettingModel generalSettingModel)
        {
            //Get Endpoint
            string endpoint = GeneralSettingEndpoint.Update();

            //Get Serialize object as a response.
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PutResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(generalSettingModel), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.IsSuccess;
        }

        public virtual PublishStateMappingListModel GetPublishStateMappingList(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            //Create Endpoint to get the list of tax class.
            string endpoint = GeneralSettingEndpoint.PublishStateMappingList();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            ApiStatus status = new ApiStatus();
            PublishStateMappingListResponse response = GetResourceFromEndpoint<PublishStateMappingListResponse>(endpoint, status);

            //check the status of response of type tax class.
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            PublishStateMappingListModel list = new PublishStateMappingListModel { PublishStateMappingList = response?.PublishStateMappings };
            list.MapPagingDataFromResponse(response);
            return list;
        }

        //Method to enable/disable publish state to application type mapping.
        public virtual bool EnableDisablePublishStateMapping(int publishStateMappingId, bool isEnabled)
        {
            //Get Endpoint.
            string endpoint = GeneralSettingEndpoint.EnableDisablePublishStateMapping(isEnabled);

            //Get response.
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PutResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(publishStateMappingId), status);

            //Check status
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response.IsSuccess;
        }

        #region Cache Management
        //Method to get Cache Management data
        public virtual CacheListModel GetCacheData()
        {
            //Get Endpoint
            string endpoint = GeneralSettingEndpoint.CacheData();

            //Get Serialize object as a response.
            ApiStatus status = new ApiStatus();
            GeneralSettingResponse response = GetResourceFromEndpoint<GeneralSettingResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };

            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response.CacheData;
        }

        public virtual bool CreateUpdateCacheData(CacheListModel cacheListModel)
        {
            //Get Endpoint
            string endpoint = GeneralSettingEndpoint.CreateUpdateCache();

            //Get serialized object as a response.
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(cacheListModel), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);
            return response.IsSuccess;
        }

        public virtual CacheModel RefreshCacheData(CacheModel cacheModel)
        {
            //Get Endpoint
            string endpoint = GeneralSettingEndpoint.RefreshCacheData();

            //Get serialized object as a response.
            ApiStatus status = new ApiStatus();
            GeneralSettingResponse response = PostResourceToEndpoint<GeneralSettingResponse>(endpoint, JsonConvert.SerializeObject(cacheModel), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);
            return response?.Cache;
        }
        #endregion      
    }
}
