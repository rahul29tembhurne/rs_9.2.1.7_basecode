﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Client
{
    public class UserClient : BaseClient, IUserClient
    {
        public virtual UserModel Login(UserModel model, ExpandCollection expands)
        {
            string endpoint = UsersEndpoint.Login();
            endpoint += BuildEndpointQueryString(expands);

            ApiStatus status = new ApiStatus();
            UserResponse response = PostResourceToEndpoint<UserResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.Unauthorized };
            CheckStatusAndThrow<ZnodeUnauthorizedException>(status, expectedStatusCodes);
            return response?.User;
        }

        public virtual UserModel ChangePassword(UserModel model)
        {
            string endpoint = UsersEndpoint.ChangePassword();
            ApiStatus status = new ApiStatus();

            UserResponse response = PostResourceToEndpoint<UserResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response?.User;
        }

        public virtual bool BulkResetPassword(ParameterModel accountId)
        {
            string endpoint = UsersEndpoint.BulkResetPassword();
            //Get response.
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(accountId), status);

            //Check status
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response.IsSuccess;
        }

        public virtual UserModel ForgotPassword(UserModel model)
        {
            string endpoint = UsersEndpoint.ForgotPassword();
            ApiStatus status = new ApiStatus();

            UserResponse response = PostResourceToEndpoint<UserResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response?.User;
        }

        public virtual int? VerifyResetPasswordLinkStatus(UserModel model)
        {
            string endpoint = UsersEndpoint.VerifyResetPasswordLinkStatus();
            ApiStatus status = new ApiStatus();

            UserResponse response = PostResourceToEndpoint<UserResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response.ErrorCode;
        }

        public virtual UserModel GetAccountByUser(string username)
        {
            string endpoint = UsersEndpoint.GetByUsername();
            ApiStatus status = new ApiStatus();

            UserResponse response = PostResourceToEndpoint<UserResponse>(endpoint, JsonConvert.SerializeObject(new UserModel() { UserName = username }), status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return response?.User;
        }

        public virtual UserModel CreateUser(UserModel accountModel)
        {
            //Get Endpoint
            string endpoint = UsersEndpoint.CreateUser();

            //Get Serialize object as a response.
            ApiStatus status = new ApiStatus();
            UserResponse response = PostResourceToEndpoint<UserResponse>(endpoint, JsonConvert.SerializeObject(accountModel), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);
            return response?.User;
        }

        public virtual UserModel GetUserAccountData(int accountId, int portalId = 0) => GetUserAccountData(accountId, null, portalId);

        public virtual UserModel GetUserAccountData(int accountId, ExpandCollection expands, int portalId = 0)
        {
            //Get Endpoint
            string endpoint = UsersEndpoint.GetUserAccountData(accountId, portalId);
            endpoint += BuildEndpointQueryString(expands);

            //Get Serialize object as a response.
            ApiStatus status = new ApiStatus();
            UserResponse response = GetResourceFromEndpoint<UserResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return response?.User;
        }

        //Update User account data.
        public virtual UserModel UpdateUserAccountData(UserModel model, bool webstoreUser)
        {
            //Get Endpoint
            string endpoint = UsersEndpoint.UpdateUserAccountData(webstoreUser);

            //Get Serialize object as a response.
            ApiStatus status = new ApiStatus();
            UserResponse response = PutResourceToEndpoint<UserResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response?.User;
        }

        //Get user account list.
        public virtual UserListModel GetUserAccountList(int loggedUserAccountId, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            //Get Endpoint.
            string endpoint = UsersEndpoint.GetUserAccountList(loggedUserAccountId);
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            //Get response.
            ApiStatus status = new ApiStatus();
            UserListResponse response = GetResourceFromEndpoint<UserListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            //Attributes list.
            UserListModel list = new UserListModel { Users = response?.Users };
            list.MapPagingDataFromResponse(response);
            return list;
        }

        public virtual bool DeleteUser(ParameterModel accountId)
        {
            //Get Endpoint.
            string endpoint = UsersEndpoint.Delete();
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(accountId), status);

            //check the status of response.
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response.IsSuccess;
        }

        public virtual bool EnableDisableAccount(ParameterModel accountId, bool lockUser)
        {
            //Get Endpoint.
            string endpoint = UsersEndpoint.EnableDisableAccount(lockUser);

            //Get response.
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(accountId), status);

            //Check status
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response.IsSuccess;
        }

        public virtual UserModel CreateCustomerAccount(UserModel accountModel)
        {
            //Get Endpoint
            string endpoint = UsersEndpoint.CreateCustomerAccount();

            //Get Serialize object as a response.
            ApiStatus status = new ApiStatus();
            UserResponse response = PostResourceToEndpoint<UserResponse>(endpoint, JsonConvert.SerializeObject(accountModel), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);
            return response?.User;
        }

        public virtual UserModel UpdateCustomerAccount(UserModel model)
        {
            //Get Endpoint
            string endpoint = UsersEndpoint.UpdateCustomerAccount();

            //Get Serialize object as a response.
            ApiStatus status = new ApiStatus();
            UserResponse response = PutResourceToEndpoint<UserResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response?.User;
        }

        //Get customer account list.
        public virtual UserListModel GetCustomerAccountList(string currentUserName, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            int loggedUserAccountId = GetAccountByUser(currentUserName).UserId;
            //Get Endpoint.
            string endpoint = UsersEndpoint.GetCustomerAccountList(loggedUserAccountId);
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            //Get response.
            ApiStatus status = new ApiStatus();
            UserListResponse response = GetResourceFromEndpoint<UserListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            //customer list.
            UserListModel list = new UserListModel { Users = response?.Users };
            list.MapPagingDataFromResponse(response);
            return list;
        }

        public virtual UserListModel GetAccountList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            //Get Endpoint.
            string endpoint = UsersEndpoint.GetAccountList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            //Get response.
            ApiStatus status = new ApiStatus();
            UserListResponse response = GetResourceFromEndpoint<UserListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            //Attributes list.
            UserListModel list = new UserListModel { Users = response?.Users };
            list.MapPagingDataFromResponse(response);
            return list;
        }

        //Gets the assigned portals to user.
        public virtual UserPortalModel GetPortalIds(string aspNetUserId)
        {
            //Get Endpoint
            string endpoint = UsersEndpoint.GetPortalIds(aspNetUserId);

            //Get Serialize object as a response.
            ApiStatus status = new ApiStatus();
            UserResponse response = GetResourceFromEndpoint<UserResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return response?.UserPortal;
        }

        //Save portal ids againt the user.
        public virtual UserPortalModel SavePortalsIds(UserPortalModel model)
        {
            //Get Endpoint
            string endpoint = UsersEndpoint.SavePortalsIds();

            //Get Serialize object as a response.
            ApiStatus status = new ApiStatus();
            UserResponse response = PostResourceToEndpoint<UserResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response?.UserPortal;
        }

        //Sign up for news letter.
        public virtual bool SignUpForNewsLetter(NewsLetterSignUpModel model)
        {
            string endpoint = UsersEndpoint.SignUpForNewsLetter();
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response.IsSuccess;
        }

        public virtual bool IsDefaultAdminPasswordReset()
        {
            //Get Endpoint.
            string endpoint = UsersEndpoint.IsDefaultAdminPasswordReset();
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = GetResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

            //check the status of response.
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response.IsSuccess;
        }

        //Convert shopper to admin.
        public virtual UserModel ConvertShopperToAdmin(UserModel model)
        {
            string endpoint = UsersEndpoint.ConvertShopperToAdmin();
            ApiStatus status = new ApiStatus();

            UserResponse response = PostResourceToEndpoint<UserResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return response?.User;
        }

        #region Social login
        // Login to the 3rd party like facebook, google etc.
        public virtual UserModel SocialLogin(SocialLoginModel model)
        {
            string endpoint = UsersEndpoint.SocialLogin();

            ApiStatus status = new ApiStatus();
            UserResponse response = PostResourceToEndpoint<UserResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeUnauthorizedException>(status, new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.Unauthorized });
            return response?.User;
        }

        //Update Billing Account Number and Quote status
        public virtual EntityAttributeModel UpdateUserAndQuoteDetails(EntityAttributeModel model)
        {
            string endpoint = UsersEndpoint.UpdateUserAndQuoteDetails();

            ApiStatus status = new ApiStatus();
            EntityAttributeResponse response = PostResourceToEndpoint<EntityAttributeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return response?.EntityAttribute;
        }
        #endregion
    }
}
