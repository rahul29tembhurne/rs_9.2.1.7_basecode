using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using System;

namespace Znode.Engine.Promotions
{
    public class ZnodeCartPromotionPercentOffShipping : ZnodeCartPromotionType
    {
        #region Constructor
        public ZnodeCartPromotionPercentOffShipping()
        {
            Name = "Percent Off Shipping";
            Description = "Applies a percent off shipping for an order; affects the shopping cart.";
            AvailableForFranchise = false;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountPercent);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the percent off shipping for the order.
        /// </summary>       
        public override void Calculate(int? couponIndex, List<PromotionModel> allPromotions)
        {
            OrderBy = nameof(PromotionModel.OrderMinimum);
            ApplicablePromolist = ZnodePromotionManager.GetPromotionsByType(ZnodeConstant.PromotionClassTypeCart, ClassName, allPromotions, OrderBy, Convert.ToInt32(ShoppingCart.PortalId));

            bool isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
            }

            if (Equals(PromotionBag.Coupons, null))
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, false))
                    return;

                ApplyDiscount(isCouponValid, couponIndex);
            }
            else if (!Equals(PromotionBag.Coupons, null) && isCouponValid)
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, true))
                    return;

                foreach (CouponModel coupon in PromotionBag.Coupons)
                {
                    if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                    {
                        ApplyDiscount(isCouponValid, couponIndex, coupon.Code);
                        if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                        {
                            break;
                        }
                    }
                }
                AddPromotionMessage(couponIndex);
            }
        }
        #endregion

        #region Private Method
        //to Apply Discount
        private void ApplyDiscount(bool isCouponValid, int? couponIndex, string couponCode = "")
        {
            decimal subTotal = ShoppingCart.SubTotal;
            subTotal = GetCartSubTotal(ShoppingCart);
            decimal shippingCost = ShoppingCart.ShippingCost;

            if (PromotionBag.MinimumOrderAmount <= subTotal)
            {
                ShoppingCart.Shipping.ShippingDiscount += shippingCost * (PromotionBag.Discount / 100);
                ShoppingCart.IsAnyPromotionApplied = true;
            }
            else
            {
                ShoppingCart.IsAnyPromotionApplied = false;
            }

            if (!string.IsNullOrEmpty(couponCode) && ShoppingCart.IsAnyPromotionApplied)
            {
                SetCouponApplied(couponCode);
                ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                ShoppingCart.Shipping.ShippingDiscountDescription = couponCode;
                ShoppingCart.Shipping.ShippingDiscountType = (int)OrderDiscountTypeEnum.COUPONCODE;
            }
            else if (ShoppingCart.IsAnyPromotionApplied)
            {
                ShoppingCart.IsAnyPromotionApplied = true;
                ShoppingCart.Shipping.ShippingDiscountDescription = PromotionBag.PromoCode;
                ShoppingCart.Shipping.ShippingDiscountType = (int)OrderDiscountTypeEnum.PROMOCODE;
            }
            //to check free shipping is applied 
            ShoppingCart.Shipping.ShippingDiscountApplied = FreeShippingApplied(ShoppingCart.IsAnyPromotionApplied);
        }

        //to check percent of shipping promotion is applied and discount amount is greater than or equals to 100
        private bool FreeShippingApplied(bool promoapplied)
          => promoapplied && PromotionBag.Discount >= 100 && ShoppingCart.Shipping.ShippingID > 0;

        #endregion
    }
}
