using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Engine.Promotions
{
    /// <summary>
    /// Base class for ZnodeCartPromotionManager, ZnodePricePromotionManager, and ZnodeProductPromotionManager.
    /// </summary>
    public class ZnodePromotionManager : ZnodeBusinessBase
    {
        #region Private Variables
        #endregion

        /// <summary>
        /// Notify those that care when we dispose.
        /// </summary>
        public event System.EventHandler Disposed;

        #region Constructor
        public ZnodePromotionManager()
        {

        }

        #endregion    
        // Gets the list of active cart promotions from the application cache.
        public List<PromotionModel> CartPromotionCache
        {
            get
            {
                List<PromotionModel> promotions = new List<PromotionModel>();
                promotions = HttpRuntime.Cache["CartPromotionCache"] as List<PromotionModel>;
                if (HelperUtility.IsNull(promotions) || promotions?.Count <= 0)
                    CacheActiveCartPromotions();
                else
                {
                    promotions = HttpRuntime.Cache["CartPromotionCache"] as List<PromotionModel>;
                    if (promotions?.Count > 0)
                    {
                        promotions.OrderBy(x => x.DisplayOrder);
                    }
                }
                return promotions ?? new List<PromotionModel>();
            }
        }

        // Gets the list of active price promotions from the application cache.
        public List<PromotionModel> PricePromotionCache
        {
            get
            {
                List<PromotionModel> promotions = new List<PromotionModel>();
                promotions = HttpRuntime.Cache["PricePromotionCache"] as List<PromotionModel>;
                if (HelperUtility.IsNull(promotions) || promotions?.Count <= 0)
                    CacheActivePricePromotions();
                else
                {
                    promotions = HttpRuntime.Cache["PricePromotionCache"] as List<PromotionModel>;
                    if (promotions?.Count > 0)
                    {
                        promotions.OrderBy(x => x.DisplayOrder);
                    }
                }
                return promotions ?? new List<PromotionModel>();
            }
        }

        // Gets the list of active product promotions from the application cache.
        public List<PromotionModel> ProductPromotionCache
        {
            get
            {
                List<PromotionModel> promotions = new List<PromotionModel>();
                promotions = HttpRuntime.Cache["ProductPromotionCache"] as List<PromotionModel>;
                if (HelperUtility.IsNull(promotions) || promotions?.Count <= 0)
                    CacheActiveProductPromotions();
                else
                {
                    promotions = HttpRuntime.Cache["ProductPromotionCache"] as List<PromotionModel>;
                    if (promotions?.Count > 0)
                    {
                        promotions.OrderBy(x => x.DisplayOrder);
                    }
                }
                return promotions ?? new List<PromotionModel>();
            }
        }

        // Gets the coupon from a promotion.
        public ZnodeCoupon GetCoupon(ZnodePromotionCoupon copoun)
              => HelperUtility.IsNull(copoun) ? new ZnodeCoupon() : new ZnodeCoupon
              {
                  RequiredBrandMinimumQuantity = copoun.InitialQuantity,
                  RequiredCatalogMinimumQuantity = copoun.InitialQuantity,
                  RequiredCategoryMinimumQuantity = copoun.InitialQuantity,
                  RequiredProductMinimumQuantity = copoun.InitialQuantity,
                  CouponId = copoun.PromotionCouponId,
                  CouponCode = copoun.Code,
                  CouponQuantityAvailable = copoun.AvailableQuantity,
              };

        //Caches all active promotions in the application cache.
        public static void CacheActivePromotions()
        {
            // NOTE: For performance reasons, we split the active promotions into three caches
            // because we don't need to verify cart promotions on product/category pages.
            CacheActiveCartPromotions();
            CacheActivePricePromotions();
            CacheActiveProductPromotions();
        }

        // Caches all active cart promotions in the application cache.
        public static void CacheActiveCartPromotions()
        {
            if (HelperUtility.IsNull(HttpRuntime.Cache["CartPromotionCache"]))
            {
                List<PromotionModel> cartPromotions = GetPromotionsByType("CART");
                ZnodeCacheDependencyManager.Insert("CartPromotionCache", cartPromotions, "ZnodePromotion");
            }
        }

        //Caches all active price promotions in the application cache.
        public static void CacheActivePricePromotions()
        {
            if (HelperUtility.IsNull(HttpRuntime.Cache["PricePromotionCache"]))
            {
                List<PromotionModel> cartPromotions = GetPromotionsByType("PRICE");
                ZnodeCacheDependencyManager.Insert("PricePromotionCache", cartPromotions, "ZnodePromotion");
            }
        }

        //Caches all active product promotions in the application cache.
        public static void CacheActiveProductPromotions()
        {
            if (HelperUtility.IsNull(HttpRuntime.Cache["ProductPromotionCache"]))
            {
                List<PromotionModel> cartPromotions = GetPromotionsByType("PRODUCT");
                ZnodeCacheDependencyManager.Insert("ProductPromotionCache", cartPromotions, "ZnodePromotion");
            }
        }

        // Caches all available promotion types in the application cache.
        public static void CacheAvailablePromotionTypes()
        {
            Container container = new Container();

            List<IZnodePromotionsType> promoTypes = HttpRuntime.Cache["PromotionTypesCache"] as List<IZnodePromotionsType>;

            //Check if any promotion types available in cache or not.
            if (HelperUtility.IsNull(promoTypes) || promoTypes?.Count <= 0)
            {

                container.Configure(scanner => scanner.Scan(x =>
                {
                    x.AssembliesFromApplicationBaseDirectory();
                    x.AddAllTypesOf<IZnodePromotionsType>();
                }));
            }

            // Only cache promotion types that have a ClassName and Name; this helps avoid showing base classes in some of the dropdown lists
            promoTypes = container.GetAllInstances<IZnodePromotionsType>().Where(x => !string.IsNullOrEmpty(x.ClassName) && !string.IsNullOrEmpty(x.Name))?.ToList();
            HttpRuntime.Cache["PromotionTypesCache"] = promoTypes.ToList();
        }

        // Gets all available promotion types from the application cache.
        public static List<IZnodePromotionsType> GetAvailablePromotionTypes()
        {
            //Get Promotion types from cache.
            List<IZnodePromotionsType> list = HttpRuntime.Cache["PromotionTypesCache"] as List<IZnodePromotionsType>;

            if (HelperUtility.IsNotNull(list))
                list.Sort((promoTypeA, promoTypeB) => string.CompareOrdinal(promoTypeA.Name, promoTypeB.Name));
            else
                list = new List<IZnodePromotionsType>();

            return list;
        }


        // Clean up. Nothing here though.
        public void Dispose(List<ZnodePromotion> promotions)
        {
            this.Dispose(true);
            GC.SuppressFinalize(promotions);
        }

        // Clean up. Nothing here though.
        public void Dispose(List<PromotionModel> promotions)
        {
            this.Dispose(true);
            GC.SuppressFinalize(promotions);
        }

        // Clean up.
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                EventHandler handler = Disposed;
                if (!Equals(handler, null))
                    handler(this, EventArgs.Empty);
            }
        }

        #region Private Method

        // Gets the list of all promotions in the database.
        private static List<PromotionModel> AllPromotions
        {
            get
            {
                List<PromotionModel> list = HttpRuntime.Cache["AllPromotionCache"] as List<PromotionModel>;

                if (HelperUtility.IsNotNull(list) && list.Count > 0)
                {
                    return list;
                }

                list = new ZnodePromotionHelper().GetAllPromotions();
                return list ?? new List<PromotionModel>();
            }
        }

        //to get promotion by promotionsType
        private static List<PromotionModel> GetPromotionsByType(string promotionsType)
        {
            return GetPromotionsByType(promotionsType, AllPromotions);
        }

        //to get promotion by promotionsType
        private static List<PromotionModel> GetPromotionsByType(string promotionsType, List<PromotionModel> allPromotions)
        {
            List<PromotionModel> promotions = allPromotions?.Where(promo => (DateTime.Today.Date.AddDays(1) >= promo.StartDate && DateTime.Today.Date <= promo.EndDate)
                                                                            && promo.PromotionType.ClassType.Equals(promotionsType, StringComparison.OrdinalIgnoreCase)
                                                                            && promo.PromotionType.IsActive == true)
                                                           .OrderBy(x => x.DisplayOrder)
                                                           .ToList();

            return promotions ?? new List<PromotionModel>();
        }

        internal static List<PromotionModel> GetPromotionsByType(string promotionsType, string promotionName, List<PromotionModel> allPromotions, string orderBy = "QuantityMinimum", int portalId = 0)
        {

            List<PromotionModel> promotions = allPromotions.Where(promo => (DateTime.Today.Date >= promo.StartDate && DateTime.Today.Date <= promo.EndDate)
                                                                            && promo.PromotionType.ClassType.Equals(promotionsType, StringComparison.OrdinalIgnoreCase)
                                                                            && promo.PromotionType.ClassName.Equals(promotionName)
                                                                            && promo.PromotionType.IsActive && (promo.PortalId == portalId || promo.PortalId == null))
                                                           .OrderByDescending(x => typeof(PromotionModel).GetProperty(orderBy).GetValue(x, null))
                                                           .ToList();

            return promotions ?? new List<PromotionModel>();
        }

        protected int GetHeaderPortalId()
        {
            const string headerCartPortalId = "Znode-Cart-PortalId";
            int portalId = 0;
            var headers = HttpContext.Current.Request.Headers;
            int.TryParse(headers[headerCartPortalId], out portalId);
            return portalId > 0 ? portalId : ZnodeConfigManager.SiteConfig.PortalId;
        }
        #endregion
    }
}