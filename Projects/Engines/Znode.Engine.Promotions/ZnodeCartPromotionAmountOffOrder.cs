using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Promotions
{
    public class ZnodeCartPromotionAmountOffOrder : ZnodeCartPromotionType
    {
        #region Constructor
        public ZnodeCartPromotionAmountOffOrder()
        {
            Name = "Amount Off Order";
            Description = "Applies an amount off an entire order; affects the shopping cart.";
            AvailableForFranchise = false;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountAmount);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the amount off an order.
        /// </summary>
        public override void Calculate(int? couponIndex, List<PromotionModel> allPromotions)
        {
            decimal itemCount = GetCartItemsCount();
            if (itemCount <= 0)
                return;

            decimal subTotal = ShoppingCart.SubTotal;
            subTotal = GetCartSubTotal(ShoppingCart);

            OrderBy = nameof(PromotionModel.OrderMinimum);
            ApplicablePromolist = ZnodePromotionManager.GetPromotionsByType(ZnodeConstant.PromotionClassTypeCart, ClassName, allPromotions, OrderBy, Convert.ToInt32(ShoppingCart.PortalId));

            if (Equals(PromotionBag.Coupons, null))
            {
                if (PromotionBag.MinimumOrderAmount <= subTotal && itemCount > 0)
                {
                    List<PromotionModel> Promotionlist = ZnodePromotionHelper.GetMostApplicablePromoList(ApplicablePromolist, subTotal, false);
                    if (!ZnodePromotionHelper.IsApplicablePromotion(Promotionlist, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, false))
                        return;

                    ApplyDiscount(PromotionBag.Discount);
                    ShoppingCart.IsAnyPromotionApplied = true;
                }
            }
            else if (PromotionBag?.Coupons?.Count > 0)
            {
                bool isCouponValid = ValidateCoupon(couponIndex);
                foreach (CouponModel coupon in PromotionBag.Coupons)
                {
                    if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                    {
                        // Apply the discount
                        if (PromotionBag.MinimumOrderAmount <= subTotal && isCouponValid && itemCount > 0)
                        {
                            if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, true))
                                return;

                            ApplyDiscount(PromotionBag.Discount, coupon.Code);
                            SetCouponApplied(coupon.Code);
                            ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                        }
                        if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                        {
                            break;
                        }
                    }
                }
                AddPromotionMessage(couponIndex.Value);
            }
        }
        #endregion

        #region Private Methods
        //to get all items count in cart
        private decimal GetCartItemsCount()
        {
            decimal count = 0;

            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                decimal lineItemCount = 0;

                if (cartItem.Product.FinalPrice > 0 && (cartItem?.Product?.ZNodeGroupProductCollection?.Count == 0))
                {
                    lineItemCount += 1;
                }

                foreach (ZnodeProductBaseEntity addon in cartItem.Product.ZNodeAddonsProductCollection)
                {
                    if (addon.FinalPrice > 0.0M)
                        lineItemCount++;
                }

                if (cartItem?.Product?.ZNodeGroupProductCollection?.Count > 0)
                {
                    foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
                    {
                        if (group.FinalPrice > 0.0M)
                            lineItemCount += group.SelectedQuantity;
                    }
                }
                else
                {
                    lineItemCount = (lineItemCount * cartItem.Quantity);
                }

                count += (lineItemCount);
            }
            return count;
        }

        //apply  calculated discount to shopping cart item
        private void ApplyDiscount(decimal lineItemDiscount, string couponCode = "")
        {
            ShoppingCart.OrderLevelDiscount += lineItemDiscount;
            ShoppingCart.OrderLevelDiscountDetails = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), lineItemDiscount, GetDiscountType(couponCode), ShoppingCart.OrderLevelDiscountDetails);
        }

        //to remove discount from shopping cart
        private void RemoveDiscount(decimal lineItemDiscount)
        {
            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                if (cartItem.Product.DiscountAmount > lineItemDiscount)
                    cartItem.Product.DiscountAmount -= lineItemDiscount;

                cartItem.Product.DiscountAmount = (cartItem.Product.DiscountAmount < 0) ? 0 : cartItem.Product.DiscountAmount;

                foreach (ZnodeProductBaseEntity addon in cartItem.Product.ZNodeAddonsProductCollection)
                {
                    if (addon.FinalPrice > 0.0M && addon.DiscountAmount > lineItemDiscount)
                        addon.DiscountAmount -= lineItemDiscount;
                }

                foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
                {
                    if (group.FinalPrice > 0.0M && group.DiscountAmount > lineItemDiscount)
                        group.DiscountAmount -= lineItemDiscount;
                }

            }
        }
        #endregion
    }
}
