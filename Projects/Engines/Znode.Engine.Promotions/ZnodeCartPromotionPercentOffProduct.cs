using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Promotions
{
    public class ZnodeCartPromotionPercentOffProduct : ZnodeCartPromotionType
    {
        #region Private Variable
        private readonly ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
        #endregion

        #region Constructor
        public ZnodeCartPromotionPercentOffProduct()
        {
            Name = "Percent Off Product";
            Description = "Applies a percent off a product for an order; affects the shopping cart.";
            AvailableForFranchise = true;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountPercent);
            Controls.Add(ZnodePromotionControl.RequiredProduct);
            Controls.Add(ZnodePromotionControl.RequiredProductMinimumQuantity);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the percent off a product in the shopping cart.
        /// </summary>
        public override void Calculate(int? couponIndex, List<PromotionModel> allPromotions)
        {
            ApplicablePromolist = ZnodePromotionManager.GetPromotionsByType(ZnodeConstant.PromotionClassTypeCart, ClassName, allPromotions, OrderBy, Convert.ToInt32(ShoppingCart.PortalId));
            bool isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
            }

            //to get all product of promotion by PromotionId
            List<ProductModel> promotionsProduct = promotionHelper.GetPromotionProducts(PromotionBag.PromotionId);

            // Loop through each cart Item
            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                if (CheckProductPromotion(promotionsProduct, cartItem.Product.ProductID)
                    && cartItem.Product.ZNodeConfigurableProductCollection.Count == 0
                    && cartItem.Product.ZNodeGroupProductCollection.Count == 0)
                {
                    ApplyDiscount(isCouponValid, couponIndex, cartItem);
                }

                //to apply promotion for addon product
                if (cartItem.Product.ZNodeAddonsProductCollection.Count > 0)
                {
                    foreach (ZnodeProductBaseEntity addon in cartItem.Product.ZNodeAddonsProductCollection)
                    {
                        ApplyLineItemDiscount(cartItem.Quantity, isCouponValid, couponIndex, addon, promotionsProduct);
                    }
                }

                //to apply promotion for configurable product
                if (cartItem.Product.ZNodeConfigurableProductCollection.Count > 0)
                {
                    if (CheckProductPromotion(promotionsProduct, cartItem.ParentProductId))
                        ApplyLineItemDiscount(cartItem.Quantity, isCouponValid, couponIndex, cartItem.Product, promotionsProduct, cartItem.Product.ZNodeConfigurableProductCollection[0], true);
                    else
                    {
                        foreach (ZnodeProductBaseEntity configurable in cartItem.Product.ZNodeConfigurableProductCollection)
                        {
                            ApplyLineItemDiscount(cartItem.Quantity, isCouponValid, couponIndex, configurable, promotionsProduct);
                        }
                    }
                }

                //to apply promotion for group product
                if (cartItem.Product.ZNodeGroupProductCollection.Count > 0)
                {
                    foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
                    {
                        if (CheckProductPromotion(promotionsProduct, cartItem.Product.ProductID))
                            ApplyLineItemDiscount(group.SelectedQuantity, isCouponValid, couponIndex, group, promotionsProduct, group);
                        else
                            ApplyLineItemDiscount(group.SelectedQuantity, isCouponValid, couponIndex, group, promotionsProduct);
                    }
                }
            }
            AddPromotionMessage(couponIndex);
        }
        #endregion

        #region Private Method
        //to apply discount amount
        private void ApplyDiscount(bool isCouponValid, int? couponIndex, ZnodeShoppingCartItem cartItem)
        {


            decimal subTotal = ShoppingCart.SubTotal;
            subTotal = GetCartSubTotal(ShoppingCart);
            decimal qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);

            ZnodePricePromotionManager pricePromoManager = new ZnodePricePromotionManager();
            decimal basePrice = pricePromoManager.PromotionalPrice(cartItem.Product, cartItem.TieredPricing);

            if (Equals(PromotionBag.Coupons, null))
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, cartItem.Quantity, ShoppingCart.SubTotal, false))
                    return;

                if (PromotionBag.RequiredProductMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
                {
                    decimal discount = cartItem.PromotionalPrice * (PromotionBag.Discount / 100);
                    cartItem.Product.DiscountAmount += discount;
                    cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(PromotionBag.PromoCode, discount, OrderDiscountTypeEnum.PROMOCODE, cartItem.Product.OrdersDiscount);
                    ShoppingCart.IsAnyPromotionApplied = true;
                    SetPromotionalPriceAndDiscount(cartItem, discount);
                }
            }
            else if (!Equals(PromotionBag.Coupons, null) && isCouponValid)
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, cartItem.Quantity, ShoppingCart.SubTotal, true))
                    return;

                foreach (CouponModel coupon in PromotionBag.Coupons)
                {
                    if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                    {
                        if (PromotionBag.RequiredProductMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
                        {
                            decimal discount = cartItem.PromotionalPrice * (PromotionBag.Discount / 100);
                            cartItem.Product.DiscountAmount += discount;
                            cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(coupon.Code, discount, OrderDiscountTypeEnum.COUPONCODE, cartItem.Product.OrdersDiscount);
                            SetPromotionalPriceAndDiscount(cartItem, discount);
                            SetCouponApplied(coupon.Code);
                            ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                        }

                        if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                        {
                            break;
                        }
                    }
                }
            }
        }

        //to apply line item discount
        private void ApplyLineItemDiscount(decimal qtyOrdered, bool isCouponValid, int? couponIndex, ZnodeProductBaseEntity product, List<ProductModel> promotionsProduct, ZnodeProductBaseEntity childproduct = null, bool isconfig = false)
        {
            if (CheckProductPromotion(promotionsProduct, product.ProductID)
                || childproduct != null)
            {
                if (Equals(PromotionBag.Coupons, null))
                {
                    ApplyChildItemDiscount(qtyOrdered, isCouponValid, couponIndex, product, childproduct, string.Empty, isconfig);
                }
                else if (!Equals(PromotionBag.Coupons, null) && isCouponValid)
                {
                    foreach (CouponModel coupon in PromotionBag.Coupons)
                    {
                        ApplyChildItemDiscount(qtyOrdered, isCouponValid, couponIndex, product, childproduct, coupon.Code, isconfig);
                    }
                }
            }
        }

        //to apply child item discount
        private void ApplyChildItemDiscount(decimal qtyOrdered, bool isCouponValid, int? couponIndex, ZnodeProductBaseEntity product, ZnodeProductBaseEntity childproduct = null, string couponCode = "", bool isconfig = false)
        {
            decimal subTotal = ShoppingCart.SubTotal;
            subTotal = GetCartSubTotal(ShoppingCart);
            decimal basePrice = product.FinalPrice;
            if (basePrice > 0)
            {
                if (PromotionBag.RequiredProductMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
                {
                    decimal discount = basePrice * (PromotionBag.Discount / 100);
                    if (childproduct == null)
                    {
                        product.DiscountAmount += discount;
                        product.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), product.OrdersDiscount);
                    }
                    else
                    {
                        if (isconfig)
                            product.DiscountAmount += discount;
                        else
                            childproduct.DiscountAmount += discount;
                        childproduct.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), product.OrdersDiscount);
                    }
                }

                if (!string.IsNullOrEmpty(couponCode))
                {
                    if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, qtyOrdered, ShoppingCart.SubTotal, true))
                        return;

                    SetCouponApplied(couponCode);
                    ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                }
                else
                {
                    ShoppingCart.IsAnyPromotionApplied = true;
                }
            }
        }

        //to check product have associated promotion to it
        private bool CheckProductPromotion(List<ProductModel> promotionsProduct, int productId)
        => promotionsProduct.Where(x => x.ProductId == productId)?.FirstOrDefault() == null ? false : true;

        #endregion
    }
}
