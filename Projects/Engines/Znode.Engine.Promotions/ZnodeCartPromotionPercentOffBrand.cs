using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Promotions
{
    public class ZnodeCartPromotionPercentOffBrand : ZnodeCartPromotionType
    {
        #region Private Variable
        private readonly ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
        private List<PromotionCartItemQuantity> promobrandSkus;
        #endregion

        #region Constructor
        public ZnodeCartPromotionPercentOffBrand()
        {
            Name = "Percent Off Brand";
            Description = "Applies a percent off products for a particular brand or manufacturer; affects the shopping cart.";
            AvailableForFranchise = false;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountPercent);
            Controls.Add(ZnodePromotionControl.RequiredBrand);
            Controls.Add(ZnodePromotionControl.RequiredBrandMinimumQuantity);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the percent off products for a particular brand or manufacturer in the shopping cart.
        /// </summary>
        public override void Calculate(int? couponIndex, List<PromotionModel> allPromotions)
        {
            ApplicablePromolist = ZnodePromotionManager.GetPromotionsByType(ZnodeConstant.PromotionClassTypeCart, ClassName, allPromotions, OrderBy, Convert.ToInt32(ShoppingCart.PortalId));

            bool isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
            }

            //to get all product of promotion by PromotionId
            List<BrandModel> promotionsBrand = promotionHelper.GetPromotionBrands(PromotionBag.PromotionId);

            //to set all promotions brand wise sku to calculate each sku quantity
            promobrandSkus = promotionHelper.SetPromotionBrandSKUQuantity(promotionsBrand, ShoppingCart);

            // Loop through each cart Item
            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                //to apply promotion for configurable product
                if (cartItem.Product.ZNodeConfigurableProductCollection.Count > 0)
                {
                    foreach (ZnodeProductBaseEntity configurable in cartItem.Product.ZNodeConfigurableProductCollection)
                    {
                        ApplyLineItemDiscount(cartItem.Quantity, isCouponValid, couponIndex, cartItem.Product, promotionsBrand, configurable, true);
                    }
                }
                //to apply promotion for group product
                else if (cartItem.Product.ZNodeGroupProductCollection.Count > 0)
                {
                    foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
                    {
                        if (!ApplyLineItemDiscount(group.SelectedQuantity, isCouponValid, couponIndex, cartItem.Product, promotionsBrand, group))
                            ApplyLineItemDiscount(group.SelectedQuantity, isCouponValid, couponIndex, group, promotionsBrand);
                    }
                }
                else
                {
                    foreach (BrandModel brand in promotionsBrand)
                    {
                        if (string.Equals(brand.BrandCode, cartItem.Product.BrandCode, StringComparison.OrdinalIgnoreCase))
                        {
                            ApplyDiscount(isCouponValid, couponIndex, cartItem);
                            break;
                        }
                    }
                }                
            }
            AddPromotionMessage(couponIndex);
        }
        #endregion

        #region Private Method
        //to apply discount
        private void ApplyDiscount(bool isCouponValid, int? couponIndex, ZnodeShoppingCartItem cartItem)
        {
            

            decimal subTotal = ShoppingCart.SubTotal;
            subTotal = GetCartSubTotal(ShoppingCart);

            decimal qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);

            ZnodePricePromotionManager pricePromoManager = new ZnodePricePromotionManager();
            decimal basePrice = pricePromoManager.PromotionalPrice(cartItem.Product, cartItem.TieredPricing);

            if (Equals(PromotionBag.Coupons, null))
            {
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, cartItem.Quantity, ShoppingCart.SubTotal, false))
                    return;

                ApplyProductDiscount(isCouponValid, couponIndex, subTotal, qtyOrdered, cartItem, basePrice);
            }
            else if (!Equals(PromotionBag.Coupons, null) && isCouponValid)
            {
                decimal cartQty = promobrandSkus.Sum(x => x.Quantity);
                if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, cartQty, ShoppingCart.SubTotal, true))
                    return;

                foreach (CouponModel coupon in PromotionBag.Coupons)
                {
                    if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                    {
                        ApplyProductDiscount(isCouponValid, couponIndex, subTotal, qtyOrdered, cartItem, basePrice, coupon.Code);
                        if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                        {
                            break;
                        }
                    }
                }
            }
        }

        //to apply product discount
        private bool ApplyProductDiscount(bool isCouponValid, int? couponIndex, decimal subTotal, decimal qtyOrdered, ZnodeShoppingCartItem cartItem, decimal basePrice, string couponCode = "")
        {
            bool discountApplied = false;
            if (PromotionBag.MinimumOrderAmount <= subTotal
                 && cartItem.Product.ZNodeConfigurableProductCollection.Count == 0
                     && cartItem.Product.ZNodeGroupProductCollection.Count == 0
                     && IsRequiredMinimumQuantity(cartItem.Product.SKU))
            {
                decimal discount = cartItem.PromotionalPrice * (PromotionBag.Discount / 100);
                cartItem.Product.DiscountAmount += discount;
                cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), cartItem.Product.OrdersDiscount);
                SetPromotionalPriceAndDiscount(cartItem, discount);
                discountApplied = true;
            }
            else if (cartItem.Product?.ZNodeConfigurableProductCollection.Count == 0
                && cartItem.Product?.ZNodeGroupProductCollection.Count == 0)
            {
                if (cartItem.Product.DiscountAmount > basePrice * (PromotionBag.Discount / 100))
                {
                    cartItem.Product.DiscountAmount -= basePrice * (PromotionBag.Discount / 100);
                    cartItem.Product.DiscountAmount = cartItem.Product.DiscountAmount < 0 ? 0 : cartItem.Product.DiscountAmount;
                    cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), cartItem.Product.DiscountAmount, GetDiscountType(couponCode), cartItem.Product.OrdersDiscount);
                    discountApplied = true;
                }
            }

            if (!string.IsNullOrEmpty(couponCode) && discountApplied)
            {
                SetCouponApplied(couponCode);
                ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
            }
            else
            {
                ShoppingCart.IsAnyPromotionApplied = discountApplied;
            }

            return discountApplied;
        }
        //to apply line item discount
        private bool ApplyLineItemDiscount(decimal qtyOrdered, bool isCouponValid, int? couponIndex, ZnodeProductBaseEntity product, List<BrandModel> promotionsBrand, ZnodeProductBaseEntity childproduct = null, bool isconfig = false)
        {
            bool discountApplied = false;
            decimal subTotal = ShoppingCart.SubTotal;
            subTotal = GetCartSubTotal(ShoppingCart);
            foreach (BrandModel promotion in promotionsBrand)
            {
                if (string.Equals(product.BrandCode, promotion.BrandCode, StringComparison.OrdinalIgnoreCase))
                {
                    if (Equals(PromotionBag.Coupons, null))
                    {
                        discountApplied = ApplyChildItemDiscount(qtyOrdered, isCouponValid, couponIndex, subTotal, product, childproduct, string.Empty, isconfig);
                    }
                    else
                    {
                        foreach (CouponModel coupon in PromotionBag.Coupons)
                        {
                            if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                            {
                                discountApplied = ApplyChildItemDiscount(qtyOrdered, isCouponValid, couponIndex, subTotal, product, childproduct, coupon.Code, isconfig);
                                if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return discountApplied;
        }

        //to apply child item discount
        private bool ApplyChildItemDiscount(decimal qtyOrdered, bool isCouponValid, int? couponIndex, decimal subTotal, ZnodeProductBaseEntity product, ZnodeProductBaseEntity childproduct = null, string couponCode = "", bool isconfig = false)
        {
            bool discountApplied = false;

            decimal basePrice = product.FinalPrice;
            if (basePrice > 0)
            {
                if (IsRequiredMinimumQuantity(product.SKU) && PromotionBag.MinimumOrderAmount <= subTotal)
                {
                    decimal discount = basePrice * (PromotionBag.Discount / 100);
                    if (childproduct == null)
                    {
                        product.DiscountAmount += discount;
                        product.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), product.OrdersDiscount);
                    }
                    else
                    {
                        if (isconfig)
                            product.DiscountAmount += discount;
                        else
                            childproduct.DiscountAmount += discount;
                        childproduct.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), discount, GetDiscountType(couponCode), product.OrdersDiscount);
                    }

                    discountApplied = true;
                    ShoppingCart.IsAnyPromotionApplied = true;
                }

                if (!string.IsNullOrEmpty(couponCode))
                {
                    SetCouponApplied(couponCode);
                    ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                    discountApplied = true;
                }
            }
            return discountApplied;
        }

        //to check minimum quantity of promotion in the shopping cart item
        private bool IsRequiredMinimumQuantity(string sku)
        {
            bool result = false;
            if (promobrandSkus?.Count > 0)
            {
                decimal cartQty = promobrandSkus.Sum(x => x.Quantity);
                if (cartQty >= PromotionBag.RequiredBrandMinimumQuantity)
                {
                    result = promobrandSkus.Any(x => x.SKU.Contains(sku));
                }
            }
            return result;
        }
        #endregion
    }
}
