using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

namespace Znode.Engine.Promotions
{
    /// <summary>
    /// Helps manage cart promotions.
    /// </summary>
    public class ZnodeCartPromotionManager : ZnodePromotionManager
    {
        #region Private Variables      
        private ZnodeShoppingCart _shoppingCart;
        private ZnodeGenericCollection<IZnodeCartPromotionType> _cartPromotions;
        private readonly ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
        List<PromotionModel> newPromotionsFromCache;
        #endregion

        #region Constructor
        /// <summary>
        /// Throws a NotImplementedException because this class requires a shopping cart to work.
        /// </summary>
        public ZnodeCartPromotionManager()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Instantiates all the promotions that are required for the current shopping cart.
        /// </summary>
        /// <param name="shoppingCart">The current shopping cart.</param>
        /// <param name="profileId">The current profile ID, if any.</param>
        public ZnodeCartPromotionManager(ZnodeShoppingCart shoppingCart, int? profileId)
        {
            _shoppingCart = shoppingCart;
            _cartPromotions = new ZnodeGenericCollection<IZnodeCartPromotionType>();

            //Get the current portal.
            int currentPortalId = GetHeaderPortalId();

            //check if shoppingcart is null.
            if (HelperUtility.IsNotNull(_shoppingCart) && HelperUtility.IsNotNull(_shoppingCart.PortalId))
               currentPortalId = _shoppingCart.PortalId.GetValueOrDefault();
            
            //Get the current profile.
            if (!profileId.HasValue)
            {
                ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
                ProfileModel profile = promotionHelper.GetProfileCache();

                if (HelperUtility.IsNotNull(profile))                
                    profileId = profile.ProfileId;                
            }
            if (CartPromotionCache?.Count <= 0)
                CacheActiveCartPromotions();

            // Check for default portal

            List<PromotionModel> cartPromotionsFromCache = CartPromotionCache;
            cartPromotionsFromCache = ApplyPromotionsFilter(cartPromotionsFromCache, currentPortalId, profileId, true);
            //Check if cached promotion are null and bind data to ZNodePromotionBag. 
            if (HelperUtility.IsNotNull(newPromotionsFromCache))
            {
                BuildPromotionsList(newPromotionsFromCache, currentPortalId, profileId);
                Dispose(newPromotionsFromCache);
            }
            else
            {
                BuildPromotionsList(cartPromotionsFromCache, currentPortalId, profileId);
                Dispose(cartPromotionsFromCache);
            }
        }

        /// <summary>
        /// Calculates the promotion and updates the shopping cart.
        /// </summary>
        public void Calculate()
        {
            // No sense in continuing without a shopping cart
            if (HelperUtility.IsNull(_shoppingCart))            
                return;            

            _shoppingCart.OrderLevelDiscount = 0;
            _shoppingCart.Shipping.ShippingDiscount = 0;
            _shoppingCart.ClearPreviousErrorMessges();
            ClearAllPromotionsAndCoupons();
            CalculateEachPromotion();
            if (HelperUtility.IsNotNull(_shoppingCart.Coupons))
            {
                for (int couponIndex = 0; couponIndex < _shoppingCart.Coupons.Count; couponIndex++)
                    CheckForInvalidCoupon(couponIndex);
                }
            }

        /// <summary>
        /// Process anything that must be done before the order is submitted.
        /// </summary>
        /// <returns>True if everything is good for submitting the order; otherwise, false.</returns>
        public bool PreSubmitOrderProcess()
        {
            bool allPreConditionsOk = true;

            foreach (ZnodeCartPromotionType promo in _cartPromotions)
                // Make sure all pre-conditions are good before letting the customer check out
                allPreConditionsOk &= promo.PreSubmitOrderProcess();
            return allPreConditionsOk;
        }

        /// <summary>
        /// Process anything that must be done after the order is submitted.
        /// </summary>
        public void PostSubmitOrderProcess()
        {
            foreach (ZnodeCartPromotionType promo in _cartPromotions)
                promo.PostSubmitOrderProcess();
        }
        #endregion

        #region Private Methods
        //to clear all promotions and coupons applied to shopping cart
        private void ClearAllPromotionsAndCoupons()
        {
            ZnodePricePromotionManager pricePromoManager = new ZnodePricePromotionManager();

            foreach (ZnodeShoppingCartItem cartItem in _shoppingCart.ShoppingCartItems)
            {
                cartItem.PromotionalPrice = pricePromoManager.PromotionalPrice(cartItem.Product, cartItem.TieredPricing);
                cartItem.ExtendedPriceDiscount = 0;
                cartItem.Product.DiscountAmount = 0;
                cartItem.Product.OrdersDiscount = null;

                foreach (ZnodeProductBaseEntity addOn in cartItem.Product?.ZNodeAddonsProductCollection)
                {
                    addOn.DiscountAmount = 0;
                    addOn.OrdersDiscount = null;
                }

                foreach (ZnodeProductBaseEntity bundle in cartItem.Product?.ZNodeBundleProductCollection)
                {
                    bundle.DiscountAmount = 0;
                    bundle.OrdersDiscount = null;
                }

                foreach (ZnodeProductBaseEntity configurable in cartItem.Product?.ZNodeConfigurableProductCollection)
                {
                    configurable.DiscountAmount = 0;
                    configurable.OrdersDiscount = null;
                }

                foreach (ZnodeProductBaseEntity group in cartItem.Product?.ZNodeGroupProductCollection)
                {
                    group.DiscountAmount = 0;
                    group.OrdersDiscount = null;
                }
            }

            //to clear all promotions and coupons applied 
            if (_shoppingCart?.Coupons?.Count > 0)
            {
                foreach (Libraries.ECommerce.Entities.ZnodeCoupon coupon in _shoppingCart.Coupons)
                {
                    coupon.CouponApplied = false;
                    coupon.CouponValid = false;
                    coupon.CouponMessage = string.Empty;
                }
            }
        }

        //Calculate each and every promotion applied on cart.
        private void CalculateEachPromotion()
        {
            List<PromotionModel> AllPromotions = promotionHelper.GetAllPromotions().Where(promo => (DateTime.Today.Date.AddDays(1) >= promo.StartDate && DateTime.Today.Date <= promo.EndDate)).ToList();
            bool state = _shoppingCart.IsAllowWithOtherPromotionsAndCoupons;
            foreach (PromotionModel item in AllPromotions?.Where(x => x.IsCouponRequired != true))
            {
                item.IsAllowWithOtherPromotionsAndCoupons = state;
            }

            AllPromotions = (!_shoppingCart.IsAllowWithOtherPromotionsAndCoupons) ? ZnodePromotionHelper.IsAllowWithPromotionsAndCoupons(AllPromotions) : AllPromotions;

            for (int cartPromoIndex = 0, cartPromoCouponIndex = 0; cartPromoIndex < _cartPromotions.Count; cartPromoIndex++)
            {
                if (_cartPromotions[cartPromoIndex].IsPromoCouponAvailable)
                {
                    if (Equals(cartPromoCouponIndex, 0))
                    {
                        if (!_shoppingCart.IsAllowWithOtherPromotionsAndCoupons && AllPromotions.Where(a => a.PromotionType.ClassName == _cartPromotions[cartPromoIndex].ClassName && !a.IsAllowWithOtherPromotionsAndCoupons).ToList()?.Count > 0)
                            RemoveDiscount();
                        CalculateCouponPromotion(cartPromoIndex, cartPromoCouponIndex, AllPromotions);
                    }
                    else
                    {
                        //Check if multiple coupons are applied or not and set the value of coupons of cart accordingly.
                        if (_shoppingCart.CartAllowsMultiCoupon)
                        {
                            if (_shoppingCart.Coupons[cartPromoCouponIndex].AllowsMultipleCoupon)
                                CalculateCouponPromotion(cartPromoIndex, cartPromoCouponIndex, AllPromotions);
                            else
                            {
                                _shoppingCart.Coupons[cartPromoCouponIndex].CouponApplied = false;
                                _shoppingCart.Coupons[cartPromoCouponIndex].CouponValid = false;
                                _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage = string.IsNullOrEmpty(_shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage) ? Api_Resources.CouponMessage : _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage;
                            }
                        }
                        else
                        {
                            _shoppingCart.Coupons[cartPromoCouponIndex].CouponApplied = false;
                            _shoppingCart.Coupons[cartPromoCouponIndex].CouponValid = false;
                            _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage = string.IsNullOrEmpty(_shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage) ? Api_Resources.CouponMessage : _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage;
                        }
                    }
                    cartPromoCouponIndex++;
                }
                else
                    _cartPromotions[cartPromoIndex].Calculate(null, AllPromotions);
            }
        }

        private void RemoveDiscount()
        {
			_shoppingCart.OrderLevelDiscount = 0;
			foreach (ZnodeShoppingCartItem cartItem in _shoppingCart.ShoppingCartItems)
            {
                cartItem.Product.DiscountAmount = 0;		

				foreach (ZnodeProductBaseEntity addon in cartItem.Product.ZNodeAddonsProductCollection){
                        addon.DiscountAmount = 0;
                }
                foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection){
                    group.DiscountAmount = 0;
                }
            }
        }

        //Calculate each and every Coupon promotion applied on cart.
        private void CalculateCouponPromotion(int cartPromoIndex, int cartPromoCouponIndex, List<PromotionModel> AllPromotions)
        {
            _cartPromotions[cartPromoIndex].Calculate(cartPromoCouponIndex, AllPromotions);

            if (!Equals(_shoppingCart.Coupons, null) && _shoppingCart.Coupons.Count > 0 && !Equals(_shoppingCart.Coupons[cartPromoCouponIndex], null) && _shoppingCart.Coupons[cartPromoCouponIndex].CouponApplied)
            {
                if (cartPromoCouponIndex == 0)
                    _shoppingCart.ClearPreviousErrorMessges();

                _shoppingCart.AddPromoDescription += _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage;
            }
            else
            {
                _shoppingCart.Coupons[cartPromoCouponIndex].CouponValid = false;
                _shoppingCart.Coupons[cartPromoCouponIndex].CouponApplied = false;
                _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage = string.IsNullOrEmpty(_shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage) ? "Invalid Coupon Code." : _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage;
            }
        }

        //check for invalid coupon is applied on cart.
        private void CheckForInvalidCoupon(int couponIndex)
        {
            if (!_shoppingCart.Coupons[couponIndex].CouponApplied && !_shoppingCart.Coupons[couponIndex].CouponValid && !string.IsNullOrEmpty(_shoppingCart.Coupons[couponIndex].Coupon))
            {
                _shoppingCart.AddPromoDescription = GlobalSetting_Resources.MessageInvalidCoupon;
                _shoppingCart.Coupons[couponIndex].CouponMessage = !string.IsNullOrEmpty(_shoppingCart.Coupons[couponIndex].CouponMessage) ? _shoppingCart.Coupons[couponIndex].CouponMessage : GlobalSetting_Resources.MessageInvalidCoupon;
            }
        }

        //to Apply Promotions Filter
        private List<PromotionModel> ApplyPromotionsFilter(List<PromotionModel> promotionsFromCache, int? currentPortalId, int? currentProfileId, bool isDefaultPortal)
        {
            //Check if no coupons applied for cuurent shopping cart.
            if (HelperUtility.IsNull(_shoppingCart.Coupons) || _shoppingCart.Coupons.Count <= 0)
            {
                promotionsFromCache = promotionsFromCache.Where(promotion => (DateTime.Today.Date >= promotion.StartDate && DateTime.Today.Date <= promotion.EndDate) &&
                            (promotion.ProfileId == currentProfileId || promotion.ProfileId == null) &&
                            (promotion.PortalId == currentPortalId || promotion.PortalId == null) &&
                            (promotion.IsCouponRequired == false) &&
                            promotion.PromotionType.ClassType.Equals("CART", StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.DisplayOrder).ToList();
            }
            else
            {
                List<PromotionModel> cartPromotionsFromCache = promotionsFromCache;

                promotionsFromCache = promotionsFromCache?.Where(promotion => (DateTime.Today.Date >= promotion.StartDate && DateTime.Today.Date <= promotion.EndDate) &&
                                (promotion.ProfileId == currentProfileId || promotion.ProfileId == null) &&
                                (promotion.PortalId == currentPortalId || promotion.PortalId == null) &&
                                (promotion.IsCouponRequired == false) &&
                                promotion.PromotionType.ClassType.Equals("CART", StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.DisplayOrder).ToList() ?? new List<PromotionModel>();

                newPromotionsFromCache = new List<PromotionModel>();

                //to add all promotions without coupons
                foreach (PromotionModel promo in promotionsFromCache ?? new List<PromotionModel>())
                {
                    if (!promo.IsCouponRequired.GetValueOrDefault())
                        newPromotionsFromCache.Add(promo);
                    }

                string couponCodes = string.Join(",", _shoppingCart.Coupons.Select(coupon => coupon.Coupon));
                string[] couponList = couponCodes.Split(',');

                foreach (Libraries.ECommerce.Entities.ZnodeCoupon coupon in _shoppingCart.Coupons)
                {
                    if (!string.IsNullOrEmpty(coupon.Coupon))
                    {
                        PromotionModel promotionsWithCoupon = promotionHelper.GetCouponsPromotion(cartPromotionsFromCache, coupon.Coupon, currentPortalId, currentProfileId, _shoppingCart.OrderId);
                        if (HelperUtility.IsNotNull(promotionsWithCoupon))
                        {
                            newPromotionsFromCache.Add(promotionsWithCoupon);
                        }
                    }
                }

                foreach (Libraries.ECommerce.Entities.ZnodeCoupon coupon in _shoppingCart.Coupons)
                {
                    coupon.AllowsMultipleCoupon = promotionHelper.AllowsMultipleCoupon(coupon.Coupon, currentPortalId, currentProfileId);
                }
                _shoppingCart.CartAllowsMultiCoupon = Convert.ToBoolean(_shoppingCart?.Coupons?.FirstOrDefault()?.AllowsMultipleCoupon);
                promotionsFromCache = newPromotionsFromCache;
            }
            return promotionsFromCache;
        }

        //Bind cached promotion if any.
        private void BuildPromotionsList(List<PromotionModel> promotionsFromCache, int? currentPortalId, int? currentProfileId)
        {
            foreach (PromotionModel promotion in promotionsFromCache ?? new List<PromotionModel>())
            {
                ZnodePromotionBag promoBag = BuildPromotionBag(promotion, currentPortalId, currentProfileId);
                AddPromotionType(promotion, promoBag);
            }
        }

        //Bind ZnodePromotion data to ZnodePromotionBag.
        private ZnodePromotionBag BuildPromotionBag(PromotionModel promotion, int? currentPortalId, int? currentProfileId)
        {
            ZnodePromotionBag promotionBag = new ZnodePromotionBag();
            promotionBag.PromotionId = promotion.PromotionId;
            promotionBag.PortalId = currentPortalId;
            promotionBag.ProfileId = currentProfileId;
            promotionBag.Discount = promotion.Discount.GetValueOrDefault();
            promotionBag.MinimumOrderAmount = promotion.OrderMinimum.GetValueOrDefault(0);
            promotionBag.RequiredProductMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
            promotionBag.RequiredProductId = promotion.ReferralPublishProductId.GetValueOrDefault(0);
            promotionBag.DiscountedProductQuantity = promotion.PromotionProductQuantity.GetValueOrDefault(1);
            promotionBag.RequiredBrandMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
            promotionBag.RequiredCatalogMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
            promotionBag.RequiredCategoryMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
            promotionBag.IsCouponAllowedWithOtherCoupons = promotion.IsAllowedWithOtherCoupons;
            promotionBag.PromoCode = promotion.PromoCode;
            promotionBag.IsUnique = promotion.IsUnique;

            if (promotion.IsCouponRequired.GetValueOrDefault())
            {
                promotionBag.Coupons = promotionHelper.GetPromotionCoupons(promotion.PromotionId, GetCartCoupons());
                promotionBag.PromotionMessage = promotion.PromotionMessage;
            }
            return promotionBag;
        }

        //Add Promotion Type in promotionBag
        private void AddPromotionType(PromotionModel promotion, ZnodePromotionBag promotionBag)
        {
            List<IZnodePromotionsType> availablePromoTypes = GetAvailablePromotionTypes();
            foreach (IZnodePromotionsType promotionType in availablePromoTypes.Where(t => t.ClassName == promotion.PromotionType?.ClassName))
            {
                try
                {
                    Assembly promoTypeAssembly = Assembly.GetAssembly(promotionType.GetType());
                    IZnodeCartPromotionType cartPromo = (IZnodeCartPromotionType)promoTypeAssembly.CreateInstance(promotionType.ToString());

                    if (HelperUtility.IsNotNull(cartPromo))
                    {
                        cartPromo.Precedence = promotion.DisplayOrder.GetValueOrDefault();

                        if (HelperUtility.IsNotNull(promotionBag.Coupons) && promotionBag.Coupons.Count > 0)
                            cartPromo.IsPromoCouponAvailable = true;

                        cartPromo.Bind(_shoppingCart, promotionBag);
                        _cartPromotions.Add(cartPromo);
                    }
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage("Error while instantiating promotion type: " + promotionType);
                    ZnodeLogging.LogMessage(ex.ToString());
                }
            }
        }

        //to get cart coupons
        private string GetCartCoupons()
        {
            return string.Join(",", _shoppingCart.Coupons.Select(coupon => coupon.Coupon));
        }

        #endregion
    }
}