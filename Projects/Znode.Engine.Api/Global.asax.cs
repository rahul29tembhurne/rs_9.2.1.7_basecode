﻿using log4net.Config;
using MongoDB.Bson;
using StructureMap;
using System;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Znode.Api.Core;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

namespace Znode.Engine.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static bool StartupCompleted = false;
        private readonly string logComponentName = "Diagnostics";

        protected void Application_Start()
        {
            ZnodeLogging.LogMessage("Znode API is starting up", logComponentName, TraceLevel.Verbose);
            BsonDefaults.GuidRepresentation = GuidRepresentation.Standard;
            ObjectFactory.Initialize(scanner => scanner.Scan(x =>
            {
                x.AssembliesFromApplicationBaseDirectory(
                    assembly => assembly.FullName.Contains("Znode."));
                x.WithDefaultConventions();
            }));
            PerformRegistrations();
            EnsureStartupCompleted();
        }

        private void PerformRegistrations()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
           RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            XmlConfigurator.Configure();
            AutoMapperConfig.Execute();
            ZnodeLogging.LogMessage("Initial registrations and configuration has been performed.", logComponentName, TraceLevel.Verbose);
        }

        private bool EnsureStartupCompleted()
        {
            if (!StartupCompleted)
            {
                ZnodeDiagnostics.RunDiagnostics();
                DefaultCacheBuilder.TryBuildCaches();
                StartupCompleted = true;
                ZnodeLogging.LogMessage("API startup has completed successfully.", logComponentName, TraceLevel.Info);
            }
            return StartupCompleted;
        }

        private void WriteResponseErrorMessage(ZnodeException exception)
        {
            HttpContext.Current.Response.StatusCode = 404;

            if (HttpContext.Current.Request.Headers.AllKeys.Contains("Znode-DomainName") && !string.IsNullOrEmpty(HttpContext.Current.Request.Headers["Znode-DomainName"]))
            {
                BaseResponse responseBody = new BaseResponse
                {
                    ErrorCode = exception.ErrorCode,
                    ErrorMessage = Api_Resources.ApiNotAvailableMessage,
                    HasError = true
                };

                HttpContext.Current.Response.Write(HelperUtility.ToJSON(responseBody));
            }
            else
            {
                HttpContext.Current.Response.Write($"<p style=\"text-align:center;font-size:30;\">{exception.ErrorMessage}</p>");
            }

            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            try
            {
                EnsureStartupCompleted();
            }
            catch (ZnodeException ex)
            {
                WriteResponseErrorMessage(ex);
                return;
            }

            FirstRequestInitialisation.Initialise();
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");

                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Authorization-Token, X-HTTP-Method-Override,Znode-UserId,Znode-DomainName,Authorization,Token");
                HttpContext.Current.Response.End();
            }
            SetSiteConfig();
        }

        protected void Application_AcquireRequestState()
        {
            ProfessionalDomainIdentifier.SetApiMandate();
        }

        private void SetSiteConfig()
        {
            var domainName = GetDomainNameFromAuthHeader();

            //Set Domain Config & Site Config details based on domain name.
            SetDomainAndSiteConfigDetails(domainName);

            //Set the API Domain & Site Configuration.
            SetAPISiteConfig();
        }

        private void SetDomainAndSiteConfigDetails(string domainName)
        {
            if (!String.IsNullOrEmpty(domainName))
            {
                if (!ZnodeConfigManager.CheckSiteConfigCache(domainName) || ZnodeConfigManager.GetDomainConfig(domainName) == null)
                {
                    var cache = new DomainCache(new DomainService());
                    var domainConfig = cache.GetDomain(domainName);

                    if (Equals(domainConfig, null) || !domainConfig.IsActive)
                    {
                        ZnodeLogging.LogMessage($"Domain {domainName} has not been configured to work with Znode.", string.Empty, System.Diagnostics.TraceLevel.Warning);

                        // The URL was not found in our config, send out a 404 error
                        HttpContext.Current.Response.StatusCode = 404;

                        BaseResponse responseBody = new BaseResponse
                        {
                            ErrorCode = ErrorCodes.InvalidDomainConfiguration,
                            ErrorMessage = Api_Resources.InvalidDomainConfigurationMessage,
                            HasError = true
                        };

                        HttpContext.Current.Response.Write(HelperUtility.ToJSON(responseBody));
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        return;
                    }

                    ZnodeConfigManager.SetDomainConfig(domainName, domainConfig);

                    //Set below call from Portal Cache.
                    IZnodeRepository<ZnodePortal> _portalRepository = new ZnodeRepository<ZnodePortal>();
                    ZnodePortal portal = _portalRepository.GetById(domainConfig.PortalId);
                    ZnodeConfigManager.SetSiteConfig(domainName, portal);
                }
            }
        }

        private void SetAPISiteConfig()
        {
            var domainName = GetAPIDomainName();

            //Set Domain Config & Site Config details based on domain name.
            SetDomainAndSiteConfigDetails(domainName);
        }
        private string GetAPIDomainName()
            => Convert.ToBoolean(ZnodeApiSettings.ValidateAuthHeader) ? HttpContext.Current.Request.ServerVariables.Get("HTTP_HOST").Trim() : string.Empty;

        private string GetDomainNameFromAuthHeader()
        {
            var headers = HttpContext.Current.Request.Headers;

            const string domainHeader = "Znode-DomainName";

            string domain = string.IsNullOrEmpty(headers[domainHeader]) ? HttpContext.Current.Request.ServerVariables.Get("HTTP_HOST").Trim() : headers[domainHeader].ToString();

            return domain;
        }

        private string DecodeBase64(string encodedValue)
        {
            var encodedValueAsBytes = Convert.FromBase64String(encodedValue);
            return Encoding.UTF8.GetString(encodedValueAsBytes);
        }

        protected void Application_EndRequest(Object sender, EventArgs e)
        {
            DisposeContext();
        }


        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception exception = null;
            exception = Server.GetLastError().GetBaseException();
            LogMessage(exception);

            DisposeContext();
            RedirectToErrorPage(sender, exception);
        }

        private void RedirectToErrorPage(Object sender, Exception exception)
        {
            var controller = new Znode.Engine.Api.Controllers.HomeController();
            var httpContext = HttpContext.Current;

            var routeData = new RouteData();
            httpContext.ClearError();
            httpContext.Response.Clear();

            SetErrorPage(exception, routeData);

            ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
        }

        private void SetErrorPage(Exception exception, RouteData routeData)
        {
            routeData.Values.Add("controller", "home");
            routeData.Values.Add("action", "index");
        }

        private void DisposeContext()
        {
            //Dispose Entity Framework Context.
            RemoveCurrentContextItems("ocm_");

            //Disposet Mongo DB Context.
            RemoveCurrentContextItems("mongo_");

            //Dispose Mongo Log DB Context
            RemoveCurrentContextItems("mongologdb_");
        }

        //Remove the Curent Context Items based on the key.
        private void RemoveCurrentContextItems(string key)
        {
            string objectContextKey = key + HttpContext.Current.GetHashCode().ToString("x");
            var context = HttpContext.Current.Items[objectContextKey];

            if (!Equals(context, null))
            {
                HttpContext.Current.Items.Remove(objectContextKey);
            }
        }

        //Log error message.
        private void LogMessage(Exception exception)
        {
               ZnodeLogging.LogMessage(exception, "ApiApplicationError", TraceLevel.Error);
        }
    }
}
