﻿using Autofac.Integration.WebApi;
using Autofac;
using Microsoft.Owin;
using Owin;
using Swashbuckle.Application;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;
using Autofac.Integration.Mvc;
using System.Web.Mvc;

[assembly: OwinStartup(typeof(Znode.Engine.Api.Startup))]

namespace Znode.Engine.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            string buildVersion = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["SwaggerBuildVersion"]) ? $"Znode {ConfigurationManager.AppSettings["SwaggerBuildVersion"]}" : "Znode";
            ConfigureAuth(app);
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
            config.EnableSwagger(c =>
            {
                c.SingleApiVersion(buildVersion, Api_Resources.APITitle);
                c.IncludeXmlComments(GetXmlCommentsPath());
                c.ResolveConflictingActions(x => x.First());
            }).EnableSwaggerUi(c => { c.DisableValidator(); });

            //Register the Dependencies for the API
            var container = StartUpTasks.RegisterDependencies();

            // Create an assign a dependency resolver for Web API to use.
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            app.UseWebApi(config);
        }
        protected static string GetXmlCommentsPath()
        {
            return System.String.Format(@"{0}\bin\Znode.Api.Core.XML",
                    System.AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}
